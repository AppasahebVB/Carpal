package cors.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;


/**
 * provides cors origin access  
 * 
 * @author Gopala krishna reddy 
 *
 */
public class CORSFilter implements Filter{
		//public static JdbcTemplate jdbcTemplate;
		
		@Override
	    public void init(FilterConfig arg0) throws ServletException {}
	    @Override
	    public void doFilter(ServletRequest req, ServletResponse resp,
	            FilterChain chain) throws IOException, ServletException {
	    	 HttpServletRequest request = null;
	    
	    	
	        // TODO Auto-generated method stub
	        HttpServletResponse response=(HttpServletResponse) resp;
	        request=(HttpServletRequest) req;
	        
	        response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	        response.setHeader("Access-Control-Max-Age", "3600"); 
	        response.setHeader("Access-Control-Allow-Headers", "*");
	        
	        String path = request.getRequestURI().substring(request.getContextPath().length());
	      // System.out.println("path.length() - "+ path.length());
	       
	       if(StringUtils.isNotEmpty(path)) {
	       	if (path.length() > 2) {
	            chain.doFilter(request, response); // Goes to default servlet.
	        } else {
	            request.getRequestDispatcher( request.getRequestURI()+ "/resources/views/login.html").forward(request, response);
	        }
	       }else {
	    	   chain.doFilter(request, response); // Goes to default servlet.
	       }
	 
			//chain.doFilter(request, response);
	    }
	 
	    @Override
	    public void destroy() {}
	    
	   
}
