package trac.customexception.customexceptionhandling;

import java.util.HashMap;



public class CommonExceptionHandlingClass extends Exception {
	
	private static final long serialVersionUID = 1L;
 
	private Integer errorCode;
	private String errorMsg;
 
	
 	/**
	 * @return the errorCode
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public HashMap<String,Object> customGenericExceptionJson(Integer errorCode, String errorMsg) {
		HashMap<String,Object> exceptionResult = new HashMap<String,Object>();
		exceptionResult.put("errorCode", errorCode);
		exceptionResult.put("errorMsg", errorMsg);
		//String responseJson = new Gson().toJson(exceptionResult);
		return exceptionResult;
		
		
	}

}
