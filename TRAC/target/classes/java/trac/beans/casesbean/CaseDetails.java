package trac.beans.casesbean;

import java.io.Serializable;

public class CaseDetails implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7419015169378678900L;
	
	private int userId;
	private String email;
	private String caseName;
	private String caseId;
	private String caseStatus;
	private String caseRegisteredTime;
	private String caseServiceLocation;
	private String caseReturnedServiceCount;
	private String caseRegisteredFrom;
	private String caseRegisteredTo;
	private int serviceRequiredTypeId;
	private String serviceRequiredTime;
	private String serviceLocationLatitude;
	private String serviceLocationLongitude;
	private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
	private String vehicleId;
	private String caseNotes;
	private int deviceTyepId;
	private int locationId;
	
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the caseName
	 */
	public synchronized String getCaseName() {
		return caseName;
	}
	/**
	 * @param caseName the caseName to set
	 */
	public synchronized void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	/**
	 * @return the caseId
	 */
	public synchronized String getCaseId() {
		return caseId;
	}
	/**
	 * @param caseId the caseId to set
	 */
	public synchronized void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	/**
	 * @return the caseStatus
	 */
	public synchronized String getCaseStatus() {
		return caseStatus;
	}
	/**
	 * @param caseStatus the caseStatus to set
	 */
	public synchronized void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	/**
	 * @return the caseRegisteredTime
	 */
	public synchronized String getCaseRegisteredTime() {
		return caseRegisteredTime;
	}
	/**
	 * @param caseRegisteredTime the caseRegisteredTime to set
	 */
	public synchronized void setCaseRegisteredTime(String caseRegisteredTime) {
		this.caseRegisteredTime = caseRegisteredTime;
	}
	/**
	 * @return the caseServiceLocation
	 */
	public synchronized String getCaseServiceLocation() {
		return caseServiceLocation;
	}
	/**
	 * @param caseServiceLocation the caseServiceLocation to set
	 */
	public synchronized void setCaseServiceLocation(String caseServiceLocation) {
		this.caseServiceLocation = caseServiceLocation;
	}
	/**
	 * @return the caseReturnedServiceCount
	 */
	public synchronized String getCaseReturnedServiceCount() {
		return caseReturnedServiceCount;
	}
	/**
	 * @param caseReturnedServiceCount the caseReturnedServiceCount to set
	 */
	public synchronized void setCaseReturnedServiceCount(
			String caseReturnedServiceCount) {
		this.caseReturnedServiceCount = caseReturnedServiceCount;
	}
	/**
	 * @return the caseRegisteredFrom
	 */
	public synchronized String getCaseRegisteredFrom() {
		return caseRegisteredFrom;
	}
	/**
	 * @param caseRegisteredFrom the caseRegisteredFrom to set
	 */
	public synchronized void setCaseRegisteredFrom(String caseRegisteredFrom) {
		this.caseRegisteredFrom = caseRegisteredFrom;
	}
	/**
	 * @return the caseRegisteredTo
	 */
	public synchronized String getCaseRegisteredTo() {
		return caseRegisteredTo;
	}
	/**
	 * @param caseRegisteredTo the caseRegisteredTo to set
	 */
	public synchronized void setCaseRegisteredTo(String caseRegisteredTo) {
		this.caseRegisteredTo = caseRegisteredTo;
	}
	/**
	 * @return the serviceLocationLatitude
	 */
	public synchronized String getServiceLocationLatitude() {
		return serviceLocationLatitude;
	}
	/**
	 * @param serviceLocationLatitude the serviceLocationLatitude to set
	 */
	public synchronized void setServiceLocationLatitude(
			String serviceLocationLatitude) {
		this.serviceLocationLatitude = serviceLocationLatitude;
	}
	/**
	 * @return the serviceLocationLongitude
	 */
	public synchronized String getServiceLocationLongitude() {
		return serviceLocationLongitude;
	}
	/**
	 * @param serviceLocationLongitude the serviceLocationLongitude to set
	 */
	public synchronized void setServiceLocationLongitude(
			String serviceLocationLongitude) {
		this.serviceLocationLongitude = serviceLocationLongitude;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String isServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public int getDeviceTyepId() {
		return deviceTyepId;
	}
	public void setDeviceTyepId(int deviceTyepId) {
		this.deviceTyepId = deviceTyepId;
	}
	/**
	 * @return the caseNotes
	 */
	public String getCaseNotes() {
		return caseNotes;
	}
	/**
	 * @param caseNotes the caseNotes to set
	 */
	public void setCaseNotes(String caseNotes) {
		this.caseNotes = caseNotes;
	}
	/**
	 * @return the serviceRequiredType
	 */
	public int getServiceRequiredTypeId() {
		return serviceRequiredTypeId;
	}
	/**
	 * @param serviceRequiredType the serviceRequiredType to set
	 */
	public void setServiceRequiredTypeId(int serviceRequiredTypeId) {
		this.serviceRequiredTypeId = serviceRequiredTypeId;
	}
	/**
	 * @return the serviceRequiredTime
	 */
	public String getServiceRequiredTime() {
		return serviceRequiredTime;
	}
	/**
	 * @param serviceRequiredTime the serviceRequiredTime to set
	 */
	public void setServiceRequiredTime(String serviceRequiredTime) {
		this.serviceRequiredTime = serviceRequiredTime;
	}
	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	
	
	
	

}
