package trac.beans.vehcilebean;

import java.io.Serializable;

public class Vehicle implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2107587111242700731L;
	
	private int userId;
	private int vehicleId;
	private String date;
	private String isFavorite;
	private String vin;
	private Long vehicleBrandId;
	
	private Long vehicleModelId;
	private String vehicleName;
	private String vehicleModelYear;
	private String vehicleRegisterNo;
	private String vehicleBrandName;
	private String vehicleModelName;
	private String email;
	private String vehiclePic;
	private Object vehicleList;
	private Object vehicleBrandsList;
	private Object vehicleModelsList;
	private Object isSuccess;
	private String serviceMessage;
	private String isImageChanged;
	private String vehicleFileNameOnCloud;
	
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the vehicleId
	 */
	public int getVehicleId() {
		return vehicleId;
	}
	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	public String getIsFavorite() {
		return isFavorite;
	}
	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVehiclePic() {
		return vehiclePic;
	}
	public void setVehiclePic(String vehcilePic) {
		this.vehiclePic = vehcilePic;
	}
	public Object isSuccess() {
		return isSuccess;
	}
	public void setSuccess(Object isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}
	/**
	 * @param vin the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}
	

	public Object getVehicleList() {
		return vehicleList;
	}
	public void setVehicleList(Object vehicleList) {
		this.vehicleList = vehicleList;
	}
	public Object getVehicleBrandsList() {
		return vehicleBrandsList;
	}
	public void setVehicleBrandsList(Object vehicleBrandsList) {
		this.vehicleBrandsList = vehicleBrandsList;
	}
	public Object getVehicleModelsList() {
		return vehicleModelsList;
	}
	public void setVehicleModelsList(Object vehicleModelsList) {
		this.vehicleModelsList = vehicleModelsList;
	}

	
	public Long getVehicleBrandId() {
		return vehicleBrandId;
	}
	public void setVehicleBrandId(Long vehicleBrandId) {
		this.vehicleBrandId = vehicleBrandId;
	}
	public Long getVehicleModelId() {
		return vehicleModelId;
	}
	public void setVehicleModelId(Long vehicleModelId) {
		this.vehicleModelId = vehicleModelId;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getVehicleBrandName() {
		return vehicleBrandName;
	}
	public void setVehicleBrandName(String vehicleBrandName) {
		this.vehicleBrandName = vehicleBrandName;
	}
	public String getVehicleModelName() {
		return vehicleModelName;
	}
	public void setVehicleModelName(String vehicleModelName) {
		this.vehicleModelName = vehicleModelName;
	}
	/**
	 * @return the isImageChanged
	 */
	public String getIsImageChanged() {
		return isImageChanged;
	}
	/**
	 * @param isImageChanged the isImageChanged to set
	 */
	public void setIsImageChanged(String isImageChanged) {
		this.isImageChanged = isImageChanged;
	}
	/**
	 * @return the vehicleFileNameOnCloud
	 */
	public String getVehicleFileNameOnCloud() {
		return vehicleFileNameOnCloud;
	}
	/**
	 * @param vehicleFileNameOnCloud the vehicleFileNameOnCloud to set
	 */
	public void setVehicleFileNameOnCloud(String vehicleFileNameOnCloud) {
		this.vehicleFileNameOnCloud = vehicleFileNameOnCloud;
	}
	/**
	 * @return the vehicleModelYear
	 */
	public String getVehicleModelYear() {
		return vehicleModelYear;
	}
	/**
	 * @param vehicleModelYear the vehicleModelYear to set
	 */
	public void setVehicleModelYear(String vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}
	
	/**
	 * @return the vehicleRegisterNo
	 */
	public String getVehicleRegisterNo() {
		return vehicleRegisterNo;
	}
	/**
	 * @param vehicleRegisterNo the vehicleRegisterNo to set
	 */
	public void setVehicleRegisterNo(String vehicleRegisterNo) {
		this.vehicleRegisterNo = vehicleRegisterNo;
	}
	
	
}
