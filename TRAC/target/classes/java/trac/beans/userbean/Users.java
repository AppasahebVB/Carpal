package trac.beans.userbean;

import java.io.Serializable;

public class Users implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2107587111242700730L;
	
	private int userId;
	private String loginId;
	private String userName;
	private String displayName;
	private String password;
	private String newPassword;
	private String hashedPassword;
	private String phone;
	private String email;
	private String securityQuestion;
	private int securityQuestionId;
	private String securityAnswer;
	private boolean isValidCredentials = false;
	private boolean isImageChanged;
	private int isdCode;
	private int statusCode;
	private int deviceTypeId;
	private String applicationVersionNo;
	private String profilePicURL;
	private int roleId;
	private String createdBy;
	private String accessToken;
	private int expiresIn;
	private int retryCount;
	private String clientId;
	private String clientSecret;
	private boolean isVersionChanged;
	private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
	private String lastUpdatedBy;
	private String[] getUserObjectArray;

	

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the userId
	 */
	public synchronized int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public synchronized void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the userName
	 */
	public synchronized String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public synchronized void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the password
	 */
	public synchronized String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public synchronized void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}
	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/**
	 * @return the hashedPassword
	 */
	public synchronized String getHashedPassword() {
		return hashedPassword;
	}
	/**
	 * @param hashedPassword the hashedPassword to set
	 */
	public synchronized void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
	/**
	 * @return the phone
	 */
	public synchronized String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public synchronized void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the isdCode
	 */
	public int getIsdCode() {
		return isdCode;
	}
	/**
	 * @param isdCode the isdCode to set
	 */
	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the securityQuestion
	 */
	public synchronized String getSecurityQuestion() {
		return securityQuestion;
	}
	/**
	 * @param securityQuestion the securityQuestion to set
	 */
	public synchronized void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	/**
	 * @return the securityQuestionId
	 */
	public synchronized int getSecurityQuestionId() {
		return securityQuestionId;
	}
	/**
	 * @param securityQuestionId the securityQuestionId to set
	 */
	public synchronized void setSecurityQuestionId(int securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}
	/**
	 * @return the securityAnswer
	 */
	public synchronized String getSecurityAnswer() {
		return securityAnswer;
	}
	/**
	 * @param securityAnswer the securityAnswer to set
	 */
	public synchronized void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	/**
	 * @return the isValidCredentials
	 */
	public synchronized boolean isValidCredentials() {
		return isValidCredentials;
	}
	/**
	 * @param isValidCredentials the isValidCredentials to set
	 */
	public synchronized void setValidCredentials(boolean isValidCredentials) {
		this.isValidCredentials = isValidCredentials;
	}
	/**
	 * @return the statusCode
	 */
	public synchronized int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public synchronized void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the deviceTypeId
	 */
	public synchronized int getDeviceTypeId() {
		return deviceTypeId;
	}
	/**
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public synchronized void setDeviceTypeId(int deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * @return the profilePicURL
	 */
	public synchronized String getProfilePicURL() {
		return profilePicURL;
	}
	/**
	 * @param profilePicURL the profilePicURL to set
	 */
	public synchronized void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}
	/**
	 * @return the roleId
	 */
	public synchronized int getRoleId() {
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public synchronized void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the createdBy
	 */
	public synchronized String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public synchronized void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the accessToken
	 */
	public synchronized String getAccessToken() {
		return accessToken;
	}
	/**
	 * @param accessToken the accessToken to set
	 */
	public synchronized void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	/**
	 * @return the expiresIn
	 */
	public synchronized int getExpiresIn() {
		return expiresIn;
	}
	/**
	 * @param expiresIn the expiresIn to set
	 */
	public synchronized void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
	/**
	 * @return the retryCount
	 */
	public synchronized int getRetryCount() {
		return retryCount;
	}
	/**
	 * @param retryCount the retryCount to set
	 */
	public synchronized void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	/**
	 * @return the isVersionChanged
	 */
	public boolean isVersionChanged() {
		return isVersionChanged;
	}
	/**
	 * @param isVersionChanged the isVersionChanged to set
	 */
	public void setVersionChanged(boolean isVersionChanged) {
		this.isVersionChanged = isVersionChanged;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public synchronized int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public synchronized void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	
	public String getApplicationVersionNo() {
		return applicationVersionNo;
	}
	public void setApplicationVersionNo(String applicationVersionNo) {
		this.applicationVersionNo = applicationVersionNo;
	}
	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
	/**
	 * @param clientSecret the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * @return the isImageChnaged
	 */
	public boolean isImageChanged() {
		return isImageChanged;
	}
	/**
	 * @param isImageChnaged the isImageChnaged to set
	 */
	public void setImageChanged(boolean isImageChanged) {
		this.isImageChanged = isImageChanged;
	}
	/**
	 * @return the getUserObjectArray
	 */
	public String[] getGetUserObjectArray() {
		return getUserObjectArray;
	}
	/**
	 * @param getUserObjectArray the getUserObjectArray to set
	 */
	public void setGetUserObjectArray(String[] getUserObjectArray) {
		this.getUserObjectArray = getUserObjectArray;
	}
	
	
	

}
