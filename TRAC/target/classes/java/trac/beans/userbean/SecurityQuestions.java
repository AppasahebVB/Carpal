package trac.beans.userbean;

import java.util.List;
import java.util.Map;

public class SecurityQuestions {
	
	private int securityQuestionId;
	private String securityQuestionAnswer;
	private List<Map<String, Object>> securityQuestionList;
	
	/**
	 * @return the securityQuestionId
	 */
	public int getSecurityQuestionId() {
		return securityQuestionId;
	}
	/**
	 * @param securityQuestionId the securityQuestionId to set
	 */
	public void setSecurityQuestionId(int securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}
	/**
	 * @return the securityQuestionAnswer
	 */
	public String getSecurityQuestionAnswer() {
		return securityQuestionAnswer;
	}
	/**
	 * @param securityQuestionAnswer the securityQuestionAnswer to set
	 */
	public void setSecurityQuestionAnswer(String securityQuestionAnswer) {
		this.securityQuestionAnswer = securityQuestionAnswer;
	}
	/**
	 * @return the securityQuestionList
	 */
	public List<Map<String, Object>> getSecurityQuestionList() {
		return securityQuestionList;
	}
	/**
	 * @param securityQuestionList the securityQuestionList to set
	 */
	public void setSecurityQuestionList(List<Map<String, Object>> securityQuestionList) {
		this.securityQuestionList = securityQuestionList;
	}


}
