package trac.beans.driverbean;

import java.io.Serializable;
import java.sql.Timestamp;

public class Driver implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2353849425765123756L;

	
	private int driverID;
	private String driverName;
	private String phone;
    private String email;
    private String notes;
    private String dateOfJoin;
    private String licenseNo;
    private String licenseNoExpiredAt;
    private String internationalLicenseNo;
    private String internationalLicenseExpiredAt;
    private String passportNo;
    private String passportExpiredAt;
    private String visaNo;
    private String visaIssuedAt;
    private String visaExpiredAt;
    private Boolean isAvailable;
    private Timestamp createdAt;
    private String createdBy;
    private Timestamp updatedAt;
    private String updatedBy;
    private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
    
	
	/**
	 * @return the driverID
	 */
	public synchronized int getDriverID() {
		return driverID;
	}
	/**
	 * @param driverID the driverID to set
	 */
	public synchronized void setDriverID(int driverID) {
		this.driverID = driverID;
	}
	/**
	 * @return the driverName
	 */
	public synchronized String getDriverName() {
		return driverName;
	}
	/**
	 * @param driverName the driverName to set
	 */
	public synchronized void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	/**
	 * @return the phone
	 */
	public synchronized String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public synchronized void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the notes
	 */
	public synchronized String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public synchronized void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the dateOfJoin
	 */
	public synchronized String getDateOfJoin() {
		return dateOfJoin;
	}
	/**
	 * @param dateOfJoin the dateOfJoin to set
	 */
	public synchronized void setDateOfJoin(String dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}
	/**
	 * @return the licenseNo
	 */
	public synchronized String getLicenseNo() {
		return licenseNo;
	}
	/**
	 * @param licenseNo the licenseNo to set
	 */
	public synchronized void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	/**
	 * @return the licenseNoExpiredAt
	 */
	public synchronized String getLicenseNoExpiredAt() {
		return licenseNoExpiredAt;
	}
	/**
	 * @param licenseNoExpiredAt the licenseNoExpiredAt to set
	 */
	public synchronized void setLicenseNoExpiredAt(String licenseNoExpiredAt) {
		this.licenseNoExpiredAt = licenseNoExpiredAt;
	}
	/**
	 * @return the internationalLicenseNo
	 */
	public synchronized String getInternationalLicenseNo() {
		return internationalLicenseNo;
	}
	/**
	 * @param internationalLicenseNo the internationalLicenseNo to set
	 */
	public synchronized void setInternationalLicenseNo(String internationalLicenseNo) {
		this.internationalLicenseNo = internationalLicenseNo;
	}
	/**
	 * @return the internationalLicenseExpiredAt
	 */
	public synchronized String getInternationalLicenseExpiredAt() {
		return internationalLicenseExpiredAt;
	}
	/**
	 * @param internationalLicenseExpiredAt the internationalLicenseExpiredAt to set
	 */
	public synchronized void setInternationalLicenseExpiredAt(
			String internationalLicenseExpiredAt) {
		this.internationalLicenseExpiredAt = internationalLicenseExpiredAt;
	}
	/**
	 * @return the passportNo
	 */
	public synchronized String getPassportNo() {
		return passportNo;
	}
	/**
	 * @param passportNo the passportNo to set
	 */
	public synchronized void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	/**
	 * @return the passportExpiredAt
	 */
	public synchronized String getPassportExpiredAt() {
		return passportExpiredAt;
	}
	/**
	 * @param passportExpiredAt the passportExpiredAt to set
	 */
	public synchronized void setPassportExpiredAt(String passportExpiredAt) {
		this.passportExpiredAt = passportExpiredAt;
	}
	/**
	 * @return the visaNo
	 */
	public synchronized String getVisaNo() {
		return visaNo;
	}
	/**
	 * @param visaNo the visaNo to set
	 */
	public synchronized void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}
	/**
	 * @return the visaIssuedAt
	 */
	public synchronized String getVisaIssuedAt() {
		return visaIssuedAt;
	}
	/**
	 * @param visaIssuedAt the visaIssuedAt to set
	 */
	public synchronized void setVisaIssuedAt(String visaIssuedAt) {
		this.visaIssuedAt = visaIssuedAt;
	}
	/**
	 * @return the visaExpiredAt
	 */
	public synchronized String getVisaExpiredAt() {
		return visaExpiredAt;
	}
	/**
	 * @param visaExpiredAt the visaExpiredAt to set
	 */
	public synchronized void setVisaExpiredAt(String visaExpiredAt) {
		this.visaExpiredAt = visaExpiredAt;
	}
	/**
	 * @return the isAvailable
	 */
	public synchronized Boolean getIsAvailable() {
		return isAvailable;
	}
	/**
	 * @param isAvailable the isAvailable to set
	 */
	public synchronized void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	/**
	 * @return the createdAt
	 */
	public synchronized Timestamp getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public synchronized void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the createdBy
	 */
	public synchronized String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public synchronized void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the updatedAt
	 */
	public synchronized Timestamp getUpdatedAt() {
		return updatedAt;
	}
	/**
	 * @param updatedAt the updatedAt to set
	 */
	public synchronized void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	/**
	 * @return the updatedBy
	 */
	public synchronized String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public synchronized void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public synchronized int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public synchronized void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
    
   
}
