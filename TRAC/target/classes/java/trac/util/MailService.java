package trac.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailService {
	@Autowired
	public static MailSender mailSender;
	@Autowired
	public static String fromEmail;
	
	@Autowired
	public static String password;
	
	public void setMailSender(MailSender mailSender) {
		MailService.mailSender = mailSender;
	}
	
	
	public void setFromEmail(String fromEmail) {
		MailService.fromEmail = fromEmail;
	}
	
	public void setPassword(String password) {
		MailService.password = password;
	}
	
	
	public static void sendMail(String to, String subject, String msg) {
	
		SimpleMailMessage message = new SimpleMailMessage();
	//	Session mailSession = createSession();
	//	if(mailSession != null){
			message.setFrom(fromEmail);
			message.setTo(to);
			message.setSubject(subject);
			message.setText(msg);
			mailSender.send(message);
		//}
	}
	
	
	/**
	   * Create mail session.
	   * 
	   * @return mail session, may not be null.
	   */
	public static Session createSession()
	  {
	    Properties props = new Properties();
	    props.put("mail.smtps.host", 587);
	    props.put("mail.smtps.auth", "true");

	    Authenticator auth = null;
	    if (password != null && fromEmail != null)
	    {
	      auth = new Authenticator()
	      {
	        protected PasswordAuthentication getPasswordAuthentication()
	        {
	          return new PasswordAuthentication(fromEmail, password);
	        }
	      };
	    }
	    Session session = Session.getInstance(props, auth);
	    
	    return session;
	  }
}
