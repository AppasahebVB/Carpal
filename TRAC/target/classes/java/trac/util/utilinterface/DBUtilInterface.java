package trac.util.utilinterface;

import java.util.Map;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;

public interface DBUtilInterface {
	public Map<String,Object> executeStoredProcedure(String[] inParamaters,Object[] inParamValues,SimpleJdbcCall simpleJdbcCall);
}
