package trac.util;


import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class UtilFunctions {
	  // create random object
    private Random random = new Random();

	public String getUserIdService(String caseIntialName, int serviceTypeId,
			String userId) {

		StringBuilder buff = new StringBuilder();
		buff.append(caseIntialName);
		buff.append("-");
		buff.append(serviceTypeId);
		buff.append("-");
		buff.append(userId);
		buff.append("-");
		buff.append(UtilFunctions.getDate());
		System.out.println("CaseDetails.....  " + buff.toString());
		return buff.toString();
	 }


	public static String getTime() {
		java.util.Date date = new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		return new Timestamp(date.getTime()).toString();
	}
	
	public static String getDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	   Date date = new Date();
	   System.out.println(dateFormat.format(date));
		return dateFormat.format(date).toString();
	
	}
	
	
	/**
	 *  Generate a CAPTCHA String consisting of random lowercase & uppercase letters, and numbers.
	 */
	public String generateCaptchaString() {
		int length = 7 + (Math.abs(random.nextInt()) % 3);
		StringBuffer captchaStringBuffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int baseCharNumber = Math.abs(random.nextInt()) % 62;
			int charNumber = 0;
			if (baseCharNumber < 26) {
				charNumber = 65 + baseCharNumber;
			}
			else if (baseCharNumber < 52){
				charNumber = 97 + (baseCharNumber - 26);
			}
			else {
				charNumber = 48 + (baseCharNumber - 52);
			}
			captchaStringBuffer.append((char)charNumber);
		}

		return captchaStringBuffer.toString();
	}

}
