package trac.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import trac.beans.userbean.Users;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;


public class CustomHttpURLConnection {

	

	// HTTP GET request
	public void sendGet(String urlToGET) throws Exception {

		
		URL obj = new URL(urlToGET);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + urlToGET);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

	// HTTP POST request
	public Users sendPost(String urlToPost) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		ObjectMapper mapper = new ObjectMapper();
		Users tempUser = new Users();
		URL obj = new URL(urlToPost);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		  String message = URLEncoder.encode("my message", "UTF-8");

		// add reuqest header
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
	
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
        writer.write("message=" + message);
        writer.close();

        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	 BufferedReader reader = new BufferedReader(new InputStreamReader(obj.openStream()));
             //StringBuffer res = new StringBuffer();
             String jsonString =  reader.readLine();
            /* while ((line = reader.readLine()) != null) {
            	 System.out.println("line = "+line);
                 res.append(line);
             }*/
           //convert JSON string to Map
     		map = mapper.readValue(jsonString, 
     		    new TypeReference<HashMap<String,Object>>(){});
     		tempUser.setAccessToken((String)map.get("access_token"));
     		tempUser.setExpiresIn((int)map.get("expires_in"));
      
             reader.close();


        } 

        writer.close();
        return tempUser;

	}

}
