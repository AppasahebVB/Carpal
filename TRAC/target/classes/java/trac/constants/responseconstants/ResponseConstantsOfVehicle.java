package trac.constants.responseconstants;

public class ResponseConstantsOfVehicle {
	

	public static final String VIN = "vin";
	public static final String VEHCICLEBRANDID = "vehicleBrandId";
	public static final String VEHCICLEMODELID = "vehicleModelId";
	public static final String VEHCILEPIC = "vehiclePic";
	public static final String VEHCICLEBRANDNAME = "vehicleBrandName";
	public static final String VEHCICLEMODELNAME = "vehicleModelName";
	public static final String VEHCICLENAME = "vehicleName";
	public static final String FAVORITE = "isFavorite";
	public static final String VEHICLELIST = "vehicleList";
	public static final String VEHICLEBRANDSLIST = "vehicleBrandsList";
	public static final String VEHICLEMODELSLIST = "vehicleModelsList";
	public static final String VEHICLE_PIC_URL = "vehiclePicUrl";
	public static final String VEHICLE_FILENAME = "vehicleFileNameOnCloud";
	public static final String VEHICLEREGISTERNO = "vehicleRegisterNo";
	public static final String VEHICLEMODELYEAR = "vehicleModelYear";
	
}
