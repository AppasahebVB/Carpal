package trac.constants.responseconstants;

public class ResponseConstantsOfUserLocation {
	
	
	public static final String USERLOCATIONNAME = "userLocationName";
	public static final String DETAILADDRESS = "detailAddress";
	public static final String USERLOCATIONLONGITUDE = "userLocationLongitude";
	public static final String USERLOCATIONLATITUDE = "userLocationLatitude";
	public static final String USERLOCATIONLIST = "userLocationList";
	public static final String LOCATIONID = "locationId";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String COUNTRYNAME = "countryName";

}
