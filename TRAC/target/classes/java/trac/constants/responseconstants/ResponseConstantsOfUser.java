package trac.constants.responseconstants;

public class ResponseConstantsOfUser {

	
	public static final String DISPLAYNAME ="displayName";
	   public static final String ISVALIDCREDENTIALS ="isValidCredentials";
	   public static final String PROFILEPICURL ="profilePicURL";
	   public static final String BLOB_KEY ="blobKey";
	   public static final String HASHEDPASSWORD ="hashedPassword";
	 ;public static final String CLOUD_PIC_FILENAME ="userFileNameOnCloud";
	   public static final String SECURITYQUESTION ="securityQuestion";
}
