package trac.constants.responseconstants;

public class ResponseConstantsOfCaseDetails {
	public static final String SERVICE_TYPE = "serviceType";
	public static final String CASESTATUS = "caseStatus";
	public static final String SERVICELOCATIONLATITUDE = "serviceLocationLatitude";
	public static final String SERVICELOCATIONLONGITUDE = "serviceLocationLongitude";
	public static final String CASENOTES = "caseNotes";
	public static final String CASEID = "caseId";
	public static final String CASEREGISTEREDTIME = "caseRegisteredTime";
	public static final String SERVICEHISTORY = "serviceHistory";
	public static final String SERVICE_REQUIRED_TIME  = "serviceRequiredTime";
	public static final String SERVICE_REQUIRED_TYPE  = "serviceRequiredType";
	public static final String VEHICLE_NAME  = "vehicleName";
	public static final String LOCATION_NAME  = "locationName";
	
	
}
