package trac.constants.sqlqueryconstants;

public class UserSqlQueryConstants {
	
	 public static final String SELECT_USER = "SELECT * FROM users WHERE email=? or phone = ? and passwordRetryCount < 3";
	 public static final String SELECT_USER_BY_ID = "SELECT * FROM users WHERE userID=? and passwordRetryCount < 3";
	 public static final String GET_SEC_QUESTIONS =  "SELECT SecurityQuestionID,Description from lkp_securityquestions_loc where  LocaleID IS NOT NULL and LocaleID = 1 and Flags != 32";
	 public static final String GET_SEC_ANSWER =  "SELECT * FROM users WHERE email=? or phone=?";
	 public static final String UPDATE_LOGIN_ATTEMPTS =  "UPDATE users SET passwordRetryCount = passwordRetryCount + 1 WHERE userID=? and passwordRetryCount < 3";
	 
	 public static final String UNIQUE_USERNAME =  "SELECT count(*) FROM users WHERE deviceTypeID = ?";
	 public static final String UNIQUE_EMAIL =  "SELECT count(*) FROM users WHERE email=?  and passwordRetryCount < 3";
	 public static final String UPDATE_SEC_CHECK =  "UPDATE users SET  isSecurityPassed = ? WHERE email = ? or phone = ?";
	 
	 public static final String UPDATE_PROFILE_PIC =  "UPDATE users SET profilePicData = ?,userFileNameOnCloud = ? WHERE userID = ?";
	 
	 public static final String SELECT_USER_COUNT = "SELECT count(*) FROM users WHERE userID=?";
	 

}
