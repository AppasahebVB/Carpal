package trac.constants.storedprocedureconstants;

public class SPNameConstants {
	
	//user related sp names
	public static final String CHANGE_PASSWORD_SPNAME = "usp_Users_Chnage_Password";
	public static final String RESET_PASSWORD_SPNAME = "usp_Users_Password_Update";
	public static final String USER_REGISTRATION_SPNAME = "usp_Registration_Users_Insert";
	public static final String USER_UPDATE_SPNAME = "usp_Registration_Users_Update";
	public static final String USER_SECURITYQUESTION_SPNAME = "usp_Registration_SecurityQuestion_Users_Select";
	
	//user location related sp names
	public static final String USERLOC_REGISTRATION_SPNAME = "usp_Location_Locations_Insert";
	public static final String USERLOC_UPDATE_SPNAME = "usp_Location_Locations_Update";
	public static final String USERLOC_FETCH_SPNAME = "usp_Location_Locations_Select";
	
	//vehicle related sp names
	public static final String VEHICLE_REGISTRATION_SPNAME = "usp_Vehicle_Vehicles_Insert";
	public static final String VEHICLE_UPDATE_SPNAME = "usp_Vehicle_Vehicles_Update";
	public static final String VEHICLE_FETCH_SPNAME = "usp_Vehicle_Vehicles_Select";
	public static final String GET_VEHICLE_BRAND_FETCH_SPNAME = "usp_vehicles_vehicleBrands_select";
	
	//case related sp names
	public static final String CASE_REGISTRATION_SPNAME = "usp_RoadAssassinate_cases_Insert";
	public static final String CASE_HISTORY_FETCH_SPNAME = "usp_ServiceHistoryAPI_Select";
	
	

}
