package trac.constants.resturlconstants;

public class RestUrlConstantsOfDriver {
	   public static final String SAVE_DRIVER_INFO = "/driver/postdetails";
	   public static final String GET_DRIVER_INFO = "/driver/getdriverdetails";
	   public static final String CREATE_DRIVER_TABLE = "/driver/create";
}
