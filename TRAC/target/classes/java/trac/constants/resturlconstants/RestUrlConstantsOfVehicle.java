package trac.constants.resturlconstants;


/**
 * This Class is Used to Place the all RestUrlConstantsOf VehicleBean
 * @author gatla
 *
 */
public class RestUrlConstantsOfVehicle {

	public static final String REGISTER_VEHICLE_INFO = "/vehicle/register";
	public static final String UPDATE_VEHICLE_INFO= "/vehicle/update";
	public static final String GET_VEHICLE_INFO= "/vehicle/getvehicleinfo";
	public static final String GET_VEHICLE_BRANDS= "/vehicle/getvehiclebrands";

	
}
