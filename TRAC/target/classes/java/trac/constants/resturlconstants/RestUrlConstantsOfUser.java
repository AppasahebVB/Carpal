package trac.constants.resturlconstants;


/**
 * This Class is used to place the All RestUrlConstantsOf UserBean
 * @author gatla
 *
 */
public class RestUrlConstantsOfUser {
	
   public static final String CREATE_USER = "/user/register";
   public static final String VALIDATE_USER = "/user/validate";
   public static final String UNIQUE_USER = "/user/unique";
   public static final String EDIT_USER = "/userprofile/update";
   
   public static final String SECURITY_CHECK1 = "/user/checkemail";
   public static final String SECURITY_CHECK2 = "/user/checksecurity";
   public static final String SECURITY_QUESTIONS = "/user/getsecurityinfo";
   
   public static final String GET_CAPTCHA = "/user/securitycheck";
   public static final String CHANGE_PASSWORD = "/userprofile/changepassword";
   public static final String RESET_PASSWORD = "/user/resetpassword";


}
