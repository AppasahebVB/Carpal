package trac.constants.resturlconstants;

public class RestUrlConstantsOfRoadAssistance {

	/**
	 * This Class is used to place the All RestUrlConstantsOf casesBean
	 * @author gatla
	 *
	 */
	public static final String REGISTER_CASE = "/roadassistance/registercase";
	public static final String FETCH_CASE_INFO = "/roadassistance/fetchcase";
	public static final String FETCH_LIST_OF_SERVICES = "/roadassistance/listofservices";

}
