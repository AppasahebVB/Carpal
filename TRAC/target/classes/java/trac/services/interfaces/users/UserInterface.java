package trac.services.interfaces.users;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gmr.web.multipart.GMultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.userbean.SecurityQuestions;
import trac.beans.userbean.Users;

public interface UserInterface {
	  //public @ResponseBody Object registerUserDetails(@RequestBody Users userDetails);
	  public @ResponseBody Object userRegistration(@RequestParam("file") GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response);
	  //public @ResponseBody Object updateUserDetails(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> fetchUserDetails(@RequestBody Users userDetails,@RequestHeader ("host") String hostName) throws Exception;
	  public @ResponseBody HashMap<String, Object> checkForUnique(@RequestBody Users userDetails);
	  public @ResponseBody Object getSecurityInfo(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> validateSecInfo(@RequestBody Users userDetails);
	  public @ResponseBody SecurityQuestions getSecurityQuestionsList();
	  public @ResponseBody boolean sendMail();
	  public @ResponseBody Object validateRegistration(); 
	  public @ResponseBody Object changePassword(Users userDetails);
	  public @ResponseBody Object updateUserDetails(GMultipartFile gMultiPartData, Users userDetails,HttpServletRequest request, HttpServletResponse response);
	  public @ResponseBody Object resetPassword(@RequestBody Users userDetails);
	  
}
