package trac.services.interfaces.vehicle;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gmr.web.multipart.GMultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.vehcilebean.Vehicle;

public interface VehicleInterface {
	 public @ResponseBody HashMap<String, Object> registerVehicleDetails(@RequestParam("file") GMultipartFile gMultiPartData,@RequestBody Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response);
	 public @ResponseBody Object updateVehicleDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response);
	 public @ResponseBody Object fetchVehicleDetails(@RequestBody Vehicle vechicleDetails);
	 public @ResponseBody Object fetchVehicleBrandsDetails();
	
}
