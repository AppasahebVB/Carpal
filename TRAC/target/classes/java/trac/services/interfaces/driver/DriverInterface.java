package trac.services.interfaces.driver;

import trac.beans.driverbean.Driver;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface DriverInterface {
	 public @ResponseBody Driver saveDriverDetails(@RequestBody Driver driverInfo);
	 public @ResponseBody Driver getDriverDetails();

}
