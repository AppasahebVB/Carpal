package trac.services.interfaces.roadassistance;

import java.util.List;
import java.util.Map;

import trac.beans.casesbean.CaseDetails;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface CaseRegisterInterface {
	
	 public @ResponseBody Object registerCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody Object fetchCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody List<Map<String, Object>> fetchServicesList();

}
