package trac.services.controllers.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
 
import trac.beans.driverbean.Driver;
import trac.constants.resturlconstants.RestUrlConstantsOfDriver;
import trac.dao.driverdao.DriverSqlOperations;
import trac.services.interfaces.driver.DriverInterface;
 
/**
 * Handles requests for the Driver service.
 */
@Controller
public class DriverController implements DriverInterface{
     
    private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
    public DriverSqlOperations daoOperations = new DriverSqlOperations();
    
     @Override
    @RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVER_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
    public @ResponseBody Driver saveDriverDetails(@RequestBody Driver driverInfo) {
        logger.info("Start create Driver.");
       
       System.out.println("in "+ RestUrlConstantsOfDriver.SAVE_DRIVER_INFO +" service");
       System.out.println(daoOperations.addDriverDetails(driverInfo.getDriverName(), driverInfo.getEmail()));
        return driverInfo;
    }
    
     @Override
    @RequestMapping(value = RestUrlConstantsOfDriver.GET_DRIVER_INFO, method = RequestMethod.GET)
    public @ResponseBody Driver getDriverDetails() {
        logger.info("get driver details.");
       
        System.out.println("in "+ RestUrlConstantsOfDriver.GET_DRIVER_INFO +" service");
        Driver driverInfo = daoOperations.getDriverDetails("Saeed");
        return driverInfo;
    }
    
}
