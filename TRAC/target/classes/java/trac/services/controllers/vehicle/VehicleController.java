package trac.services.controllers.vehicle;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import trac.beans.vehcilebean.Vehicle;
import trac.constants.cloudconstants.CloudConstantsInfo;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.resturlconstants.RestUrlConstantsOfVehicle;
import trac.dao.vehicledao.VehicleDaoOperations;
import trac.services.interfaces.vehicle.VehicleInterface;
import trac.util.CloudStorageService;
import trac.util.UtilFunctions;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.gmr.web.multipart.GMultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VehicleController implements VehicleInterface{
	private static final Logger logger = LoggerFactory.getLogger(VehicleController.class);
	private VehicleDaoOperations sqlOperations = new VehicleDaoOperations();
	private CloudStorageService cloudService = new CloudStorageService(); // object that have cloud related info
	/**
	 * This Function is Used To Register Vehicle Details of of Registered User.
	 * URL : /vehicle/register
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.REGISTER_VEHICLE_INFO, method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object> registerVehicleDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.REGISTER_VEHICLE_INFO + " service");
		
		String vehcilePicUrl = null;
		String vehicleNameOnCloud = vechicleDetails.getVehicleName() + UtilFunctions.getDate();
		vechicleDetails.setVehicleFileNameOnCloud(vehicleNameOnCloud); 
		
		//registering vehicle details in TRAC DB
		HashMap<String, Object> returnedUserDetails = sqlOperations.registerVehcileInfo(vechicleDetails);
		
		//check whether is there any multipart content exist 
		if((boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS) && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null){
						if (gMultiPartData.getSize() != 0) {
							Integer vehicleId = new Integer((int)returnedUserDetails.get(CommonResponseConstants.VEHICLEID));
							//Blob file = new Blob(gMultiPartData.getBytes());
							vehcilePicUrl = cloudService.uploadImagesToCloudBucket(gMultiPartData,vechicleDetails.getVehicleFileNameOnCloud(),null,response,CloudConstantsInfo.VEHICLE_BUCKETNAME);
							
							int updateCount =sqlOperations.updateVehcilePic(vehicleId,vehcilePicUrl,vechicleDetails.getVehicleFileNameOnCloud());
							 //userDetails.setProfilePicURL(profilePicURL);
							if(updateCount == 1)
								returnedUserDetails.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,vehcilePicUrl);
							else
								returnedUserDetails.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,null);
						}
		}
		
		return returnedUserDetails;
	}
	/**
	 * This Function is Used To Update Vehicle Details Of Registered User.
	 * URL : /vehicle/update
	 * 
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.UPDATE_VEHICLE_INFO, method = RequestMethod.POST)
		public @ResponseBody Object updateVehicleDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.UPDATE_VEHICLE_INFO + " service");
		
		String vehcilePicUrl = null;
		String  isImageChanged = request.getParameter("isImageChanged");
		String vehicleNameOnCloud = vechicleDetails.getVehicleName() + UtilFunctions.getDate();
		vechicleDetails.setVehicleFileNameOnCloud(vehicleNameOnCloud); 
		
		//registering vehicle details in TRAC DB
		Object returnJson = sqlOperations.editVehcileInfo(vechicleDetails); 
		
		//registering vehicle details in TRAC DB
				@SuppressWarnings("unchecked")
				HashMap<String, Object> returnedUserDetails = (HashMap<String, Object>)returnJson;//getting return object
				
				//check whether is there any multipart content exist 
				if(isImageChanged.equals("true") && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null && returnedUserDetails != null){//checking for multipart content
								if (gMultiPartData.getSize() != 0) {
									Integer vehicleId = new Integer(vechicleDetails.getVehicleId());
									//Blob file = new Blob(gMultiPartData.getBytes());
									vehcilePicUrl = cloudService.uploadImagesToCloudBucket(gMultiPartData,vechicleDetails.getVehicleFileNameOnCloud(),(String)returnedUserDetails.get(ResponseConstantsOfVehicle.VEHICLE_FILENAME),response,CloudConstantsInfo.VEHICLE_BUCKETNAME);
									
									int updateCount =sqlOperations.updateVehcilePic(vehicleId,vehcilePicUrl,vechicleDetails.getVehicleFileNameOnCloud());//updating vehicle pic url and vehicle pic name
									
									if(updateCount == 1) //if updated successfully
										returnedUserDetails.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,vehcilePicUrl);
									else //if not updated
										returnedUserDetails.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,null);
								}
				}else if(!(boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS)){//if nothing to edit
					System.out.println("IN ELSE");
					returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "Nothing to update");
					returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
				}
				
				if(returnedUserDetails.containsKey(ResponseConstantsOfVehicle.VEHICLE_FILENAME))
					returnedUserDetails.remove(ResponseConstantsOfVehicle.VEHICLE_FILENAME);//removing parameter from map
				
				return returnedUserDetails;
	}
	/**
	 * This Function is Used To Get All Vehicle Details Of Registered Vehicle.
	 * URL : /vehicle/getvehicleinfo
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.GET_VEHICLE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object fetchVehicleDetails(@RequestBody Vehicle vechicleDetails) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.GET_VEHICLE_INFO + " service");
		//registering vehicle details in TRAC DB
		@SuppressWarnings("unchecked")
		ArrayList<Object> vehicleList = (ArrayList<Object>) sqlOperations.getVehcileInfo(vechicleDetails);
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if(vehicleList.size()>0){
			returnObject.put(CommonResponseConstants.USERID, vechicleDetails.getUserId());
			returnObject.put(ResponseConstantsOfVehicle.VEHICLELIST, vehicleList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicles not Found");

		}
		return returnObject;

	}
	/**
	 * This Function is Used To Get All Vehicle Brands Of Corresponding Model .
	 * URL : /vehicle/getvehiclebrands
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.GET_VEHICLE_BRANDS, method = RequestMethod.GET,consumes="application/json",produces="application/json")
	public @ResponseBody Object fetchVehicleBrandsDetails() {
		logger.info("IN " + RestUrlConstantsOfVehicle.GET_VEHICLE_BRANDS+ " service");
		// registering vehicle details in TRAC DB
		ArrayList<Object> vehicleBrandModelsList = (ArrayList<Object>) sqlOperations
				.getVehicleBrands();
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if (vehicleBrandModelsList != null && vehicleBrandModelsList.size() > 0) {

			String brandName = (String) ((HashMap<String, Object>) vehicleBrandModelsList.get(0)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME);

			ArrayList<Object> brandsList = new ArrayList<Object>();
			HashMap<String, Object> modelsMap = new HashMap<String, Object>();

			ArrayList<Object> subModelsList = new ArrayList<Object>();

			int j = 0;
			int brandsSize = vehicleBrandModelsList.size();
			int i = 0;
			for (i = 0; i < brandsSize; i++) {
				brandName = (String) ((HashMap<String, Object>) vehicleBrandModelsList.get(i)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME);
				if (i != brandsSize - 1
						&& brandName.equalsIgnoreCase((String) ((HashMap<String, Object>) vehicleBrandModelsList.get(i + 1)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME))) {
					subModelsList.add(j, vehicleBrandModelsList.get(i));
					j++;
				} else {
					subModelsList.add(j, vehicleBrandModelsList.get(i));
					modelsMap = new HashMap<String, Object>();
					modelsMap.put("brandModels", subModelsList);
					modelsMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME,brandName);
					modelsMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID,((HashMap<String, Object>) vehicleBrandModelsList.get(i)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDID));
					brandsList.add(modelsMap);
					subModelsList = new ArrayList<Object>();
					j = 0;
				}
			}

			returnObject.put(ResponseConstantsOfVehicle.VEHICLEBRANDSLIST,brandsList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		} else {
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Vehicle Brands not Found");

		}
		return returnObject;

	}
}
