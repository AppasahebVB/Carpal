package trac.services.controllers.users;


import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.userbean.UserLocation;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.resturlconstants.RestUrlConstantsOfUserLocation;
import trac.dao.userdao.UserLocationDaoOperations;
import trac.services.interfaces.users.UserLocationInterface;

@Controller
public class UserLocationController implements UserLocationInterface {
	
	 private static final Logger logger = LoggerFactory.getLogger(UserLocationController.class);
	 private UserLocationDaoOperations sqlOperations = new UserLocationDaoOperations();
	 /**
	  * This Function is used to Register location details of registered user.
	  * URL: /userlocation/register
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.REGISTER_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> registerUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		 logger.info("IN "+ RestUrlConstantsOfUserLocation.REGISTER_USER_LOC + " service");
		 HashMap<String, Object> returnJson =  sqlOperations.addUserLocation(userLocationDetails);
		// TODO Auto-generated method stub
		return returnJson;
	}
	/**
	  * This Function is used to Edit location details of registered user.
	  * URL: /userlocation/update
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.EDIT_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> updateUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		 logger.info("IN "+ RestUrlConstantsOfUserLocation.EDIT_USER_LOC + " service");
		 HashMap<String, Object> returnJson =   sqlOperations.updateUserLocation(userLocationDetails);
		// TODO Auto-generated method stub
		return returnJson;
	}
	/**
	  * This Function is used to get registered location details of registered user.
	  * URL: /userlocation/fetch
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.GET_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		 logger.info("IN "+ RestUrlConstantsOfUserLocation.GET_USER_LOC + " service");
		 HashMap<String, Object> returnObject = new HashMap<String, Object>();
		 ArrayList<Object> resultObject = (ArrayList<Object>) sqlOperations.getUserLocation(userLocationDetails);
		 //condition to check objectsize is not null then return userid and location object
		 if(resultObject.size()>0){
		 returnObject.put(CommonResponseConstants.USERID, userLocationDetails.getUserId());
		 returnObject.put(ResponseConstantsOfUserLocation.USERLOCATIONLIST, resultObject);
		 returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		 }
		 else{
			 returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			 returnObject.put(CommonResponseConstants.SERVICEMESSAGE, "User Location not Found.");
			 
		 }
		 
		return returnObject;
	}
	
	
}
