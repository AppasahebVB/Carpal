package trac.services.controllers.users;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import trac.beans.userbean.SecurityQuestions;
import trac.beans.userbean.Users;
import trac.constants.cloudconstants.CloudConstantsInfo;
import trac.constants.resturlconstants.RestUrlConstantsOfUser;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUser;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.dao.userdao.UserSqlOperations;
import trac.services.interfaces.users.UserInterface;
import trac.util.CloudStorageService;
import trac.util.CustomHashing;
import trac.util.CustomHttpURLConnection;
import trac.util.MailService;
import trac.util.UtilFunctions;
import trac.util.utilinterface.SpringByCryptoHashing;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.gmr.web.multipart.GMultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController implements UserInterface{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private UserSqlOperations sqlOperations = new UserSqlOperations();
	private CloudStorageService cloudService = new CloudStorageService(); // object that have cloud related info
	private SpringByCryptoHashing hashing = new CustomHashing();
	private UtilFunctions utility = new UtilFunctions();
	private	Map<String, Object> returnJson = null;

	@Override
	@RequestMapping(value = "/user/sendmail", method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody boolean sendMail() {
		logger.info("send mail.");
		MailService.sendMail("agkreddy@gmail.com","hii","hiiiiiiiiiii");
		return true;
	}
	
	/**
	 * This Function is used to upload the Multipart image file and register user 
	 * 
	 * @param gMultiPartData
	 * @param userDetails
	 * @param request
	 * @serviceurl /upload/photo
	 * @return success and failure
	 */

	@RequestMapping(value = RestUrlConstantsOfUser.CREATE_USER, method = RequestMethod.POST)
	public @ResponseBody Object userRegistration(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response) {
		
		logger.info("create user.");
		System.out.println("in "+ RestUrlConstantsOfUser.CREATE_USER +" service");
		
		Object returnJson; //return json variable
		String profilePicURL = null; //to hold image url of cloud
		String userFileNameOnCloud = null;
		int updateCount = 0;
		
		
		//encrypting the password
		userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
		
		//Condition to check User Registered Throw Mobile or web   (1= mobile,2= web)		
		if(userDetails.getDeviceTypeId() == 2)
			userDetails.setRoleId(6);
		
		
		if(  	userDetails.getPassword()!=null && userDetails.getPassword().length()>0
				&&
				userDetails.getEmail()!=null && userDetails.getEmail().length()>0
				&&
				userDetails.getPhone()!=null && userDetails.getPhone().length()>0){
			
			
			
			//calling addUserDetails function to save user details
			returnJson = sqlOperations.addUserDetails(userDetails);
		
			@SuppressWarnings("unchecked")
			HashMap<String, Object> returnedUserDetails = (HashMap<String, Object>) returnJson;
			
			//check whether is there any multipart content exist 
			if((boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS) && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null){
					if (gMultiPartData.getSize() != 0) {
						userFileNameOnCloud = new Integer((int)returnedUserDetails.get(CommonResponseConstants.USERID)).toString() + UtilFunctions.getDate();
						//Blob file = new Blob(gMultiPartData.getBytes());
						profilePicURL = cloudService.uploadImagesToCloudBucket(gMultiPartData,userFileNameOnCloud,null,response,CloudConstantsInfo.USER_BUCKETNAME);
						if(profilePicURL.length() > 0)
							updateCount = sqlOperations.updateProfilePic((int)returnedUserDetails.get(CommonResponseConstants.USERID),profilePicURL,userFileNameOnCloud);
						 //userDetails.setProfilePicURL(profilePicURL);
						if(updateCount == 1)
							returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,profilePicURL);
						else
							returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,null);
					}
			}
		}
		else{

			HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Email/Phone doesn't Exist.");

			returnJson = responseMap;
		}
		
	
		return returnJson;
	}
	
	/**
	 * This Function is Used To Edit UserDetails of Registered User. 
	 * URL: /user/update
	 * Method: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.EDIT_USER, method = RequestMethod.POST)
	public @ResponseBody Object updateUserDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("edit user.");
		String profilePicURL = null;
		String oldFileName = null;
		boolean isLoginExist = false;
		String userFileNameOnCloud = new Integer(userDetails.getUserId()).toString() + UtilFunctions.getDate();
		Object returnJson;
		
		HashMap<String, Object> returnedUserDetails = null;
		String  isImageChanged = request.getParameter("isImageChanged");

		//user update if details are not null
		if(userDetails.getEmail()!=null && userDetails.getEmail().length()>0
				||
		   userDetails.getPhone()!=null && userDetails.getPhone().length()>0){
			
			//password encyption if password not null
			if(userDetails.getPassword()!=null && userDetails.getPassword().length()>0 )
				userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
			
			//updating the details by calling update dao function
			returnJson = sqlOperations.editUserDetails(userDetails);
			
        	returnedUserDetails = (HashMap<String, Object>) returnJson;
	    }else{
	    	returnedUserDetails = new HashMap<String, Object>();
	    	returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
	    	
	    }
		
		//is email/phone already exist
		isLoginExist = (boolean)returnedUserDetails.get(CommonResponseConstants.ISLOGINEXIST);
		
		//check whether is there any multipart content exist 
		if(isImageChanged.equals("true") && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null && !isLoginExist){
			
				if (gMultiPartData.getSize() != 0) {
					
					if(returnedUserDetails.containsKey(ResponseConstantsOfUser.CLOUD_PIC_FILENAME) && (String)returnedUserDetails.get(ResponseConstantsOfUser.CLOUD_PIC_FILENAME) != null) //condition to check whether the key exist in map
						oldFileName = returnedUserDetails.get(ResponseConstantsOfUser.CLOUD_PIC_FILENAME).toString();
					
					profilePicURL = cloudService.uploadImagesToCloudBucket(gMultiPartData,userFileNameOnCloud,oldFileName,response,CloudConstantsInfo.USER_BUCKETNAME);
					
					int updateCount =sqlOperations.updateProfilePic(userDetails.getUserId(),profilePicURL,userFileNameOnCloud);
					 //userDetails.setProfilePicURL(profilePicURL);
					if(updateCount == 1){
						returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, true);
						returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "User updated successfully");
						returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,profilePicURL);
					}else{
						returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "Problem updating image");
						returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
						returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,null);
					}
				}else{
					returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, returnedUserDetails.get(CommonResponseConstants.SERVICEMESSAGE));
					returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
				}
				
		}else if(!(boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS)){
			
			if(isLoginExist)
				returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, returnedUserDetails.get(CommonResponseConstants.SERVICEMESSAGE));
			else
				returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "Nothing to update");
			
			returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
		}
		return returnedUserDetails;
	}

	/**
	 * This Function is Used To Validate User Details of Registered User By Using Email And Phone.
	 * URL: /user/validate
	 * Method: POST
	 * @return Token , Service Message
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.VALIDATE_USER, method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object> fetchUserDetails(@RequestBody Users userDetails,@RequestHeader ("host") String hostName) throws Exception {
		logger.info("in "+ RestUrlConstantsOfUser.VALIDATE_USER +" service");

		//http url connection to get accesstoken 
		CustomHttpURLConnection httpUrlConnection = new CustomHttpURLConnection();
		Users returnUserDetails = null;
		
		//fetching userdetails if exists
		if((userDetails.getLoginId()!=null && userDetails.getLoginId().length()>0) && userDetails.getPassword()!=null && userDetails.getPassword().length()>0 ){
			 returnUserDetails = sqlOperations.getUserDetails(userDetails);
		}
		int rowUpdatedCount = 0;


		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(returnUserDetails != null ){
			System.out.println("RetryCount - "+returnUserDetails.getRetryCount());
			if(returnUserDetails.getRetryCount() < 3){
				responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS, hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword()));
			}else{
				responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS, false);
			}
			
			
			//Generate Token If Credentials is Valid
			if((boolean) responseMap.get(ResponseConstantsOfUser.ISVALIDCREDENTIALS)){
				Users tempUserDetails = httpUrlConnection.sendPost("http://"+hostName+"/oauth/token?username="+returnUserDetails.getUserId()+"&password="+userDetails.getPassword()+"&client_id=mysupplycompany&client_secret=mycompanykey&grant_type=password");
				responseMap.put(CommonResponseConstants.EMAIL,returnUserDetails.getEmail());
				responseMap.put(CommonResponseConstants.PHONE,returnUserDetails.getPhone());
				responseMap.put(ResponseConstantsOfUser.HASHEDPASSWORD,"");
				responseMap.put(CommonResponseConstants.ACCESSTOKEN,tempUserDetails.getAccessToken());
				responseMap.put(CommonResponseConstants.EXPIRESIN,tempUserDetails.getExpiresIn());
				responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,returnUserDetails.getProfilePicURL());
				responseMap.put(CommonResponseConstants.SERVICECODE,600);
				responseMap.put(CommonResponseConstants.ISSUCCESS,true);
				responseMap.put(CommonResponseConstants.USERID, returnUserDetails.getUserId());
				responseMap.put(ResponseConstantsOfUser.DISPLAYNAME, returnUserDetails.getDisplayName());
				
				if(returnUserDetails.getApplicationVersionNo()!=null && !returnUserDetails.getApplicationVersionNo().equalsIgnoreCase(userDetails.getApplicationVersionNo())){
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"App version changed,please update");
				}

			}else{

				if(returnUserDetails.getRetryCount() < 3)
					rowUpdatedCount = sqlOperations.updateUserLoginAttempts(returnUserDetails.getUserId());
				
				//Condition to check If User login multiple times then display message
				if(rowUpdatedCount == 0 && returnUserDetails.getRetryCount() >= 3){
					System.out.println("if isMultipleLoginFailed - ");
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Multiple logins failed, please conatct admin");
					responseMap.put(CommonResponseConstants.SERVICECODE,602);
				}else{
					System.out.println("else isMultipleLoginFailed - ");

					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Invalid User/Password");
					responseMap.put(CommonResponseConstants.SERVICECODE,601);
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				}
				responseMap.put(ResponseConstantsOfUser.HASHEDPASSWORD,"");

			}
		}else{


			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Invalid User/Password");
			responseMap.put(CommonResponseConstants.SERVICECODE,601);
			responseMap.put(CommonResponseConstants.ISSUCCESS,false);
		}

		return responseMap;
	}
	/**
	 * This Function is Used To Check the Uniqueness of user along the application
	 * URL: /user/unique
	 * METHOD: POST
	 * @return 
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.UNIQUE_USER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody  HashMap<String, Object> checkForUnique(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		boolean isSuccess =  sqlOperations.checkForUnique(userDetails.getUserName(),userDetails.getEmail());

		responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

		return responseMap;
	}
	/**
	 * This Function is Used To Fetch SecurityQuestion When the user forget password.
	 * URL: /user/checkemail
	 * @return secQuestion
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_CHECK1, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object getSecurityInfo(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.SECURITY_CHECK1 +" service");
		
		Object secQuestion;
		
		//condition to check if email and phone number is exit then return security question else email/phone does't exist.
		if(userDetails.getLoginId()!=null && userDetails.getLoginId().length()>0){
			secQuestion =  sqlOperations.fetchSecurityQuestion(userDetails.getLoginId(),userDetails.getLoginId());
		}
		else{
			HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Email/Phone doesn't Exist.");
			secQuestion = responseMap;
		}
		return secQuestion;

	}
	/**
	 * This Function is Used To validate security Answer to provided by user when he tries option to get forget password
	 * URL: /user/checksecurity
	 * @return userID
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_CHECK2, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> validateSecInfo(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.SECURITY_CHECK2 +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		HashMap<String, Object> responseMap =  sqlOperations.validateSecurityAnswer(userDetails);

		return responseMap;
	}
	/**
	 * This Function is used to get the list of security questions of list
	 * URL: /user/getsecurityinfo
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_QUESTIONS, method = RequestMethod.GET,consumes="application/json",produces="application/json")
	public @ResponseBody SecurityQuestions getSecurityQuestionsList() {

		logger.info("in "+ RestUrlConstantsOfUser.SECURITY_QUESTIONS +" service");


		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		List<Map<String, Object>> securityQuestionMap =  sqlOperations.fetchSecurityInfoList();
		SecurityQuestions secQuestions = new SecurityQuestions();

		secQuestions.setSecurityQuestionList(securityQuestionMap);
		return secQuestions;

	}
	
	/**
	 * This Function is used to get Verification code before completion of registration. 
	 * URL: /user/securitycheck
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_CAPTCHA, method = RequestMethod.GET,consumes="application/json",produces="application/json")
	public @ResponseBody Object validateRegistration() {
		logger.info("get captcha");
		Map<String,Object> returnJson = new HashMap<String,Object>();
		//Generate captcha string
		String randomCaptcha = utility.generateCaptchaString();
		returnJson.put("captchaCode", randomCaptcha);
		return returnJson;
	}
	/**
	 * This Function is used to get the password of user.
	 * URL: /user/changepassword
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.RESET_PASSWORD, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object changePassword(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.RESET_PASSWORD +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.RESET_PASSWORD +" service");

		userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
		Object returnJson =  sqlOperations.updatePassword(userDetails,SPNameConstants.RESET_PASSWORD_SPNAME);

		return returnJson;

	}
	
	/**
	 * This Function is used to get the password of user.
	 * URL: /user/changepassword
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.CHANGE_PASSWORD, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object resetPassword(@RequestBody Users userDetails) {
		returnJson = new HashMap<String,Object>();
		//Object returnJson = null;
		logger.info("in "+ RestUrlConstantsOfUser.CHANGE_PASSWORD +" service");
		Users returnUserDetails = sqlOperations.getUserDetailsById(userDetails.getUserId());
		
		boolean isPasswordCorrect = hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword());
		
		if(isPasswordCorrect){
			userDetails.setHashedPassword(hashing.encryption(userDetails.getNewPassword()));
			returnJson  =  (Map<String, Object>) sqlOperations.updatePassword(userDetails,SPNameConstants.CHANGE_PASSWORD_SPNAME);
		}else{
			returnJson.put(CommonResponseConstants.ISSUCCESS, false);
			returnJson.put(CommonResponseConstants.SERVICEMESSAGE, "Password not matching");
		}

		return returnJson;

	}


}
