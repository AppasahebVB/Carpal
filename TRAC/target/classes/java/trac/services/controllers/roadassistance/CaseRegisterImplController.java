package trac.services.controllers.roadassistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.resturlconstants.RestUrlConstantsOfRoadAssistance;
import trac.dao.casesdao.CaseRelatedSqlOperations;
import trac.services.interfaces.roadassistance.CaseRegisterInterface;

@Controller
public class CaseRegisterImplController implements CaseRegisterInterface {

	private static final Logger logger = LoggerFactory.getLogger(CaseRegisterImplController.class);
	private CaseRelatedSqlOperations sqlOperations = new CaseRelatedSqlOperations();


	/**
	 * This Function is Used To Registered case Details of Registered User.
	 * URL: /roadassistance/registercase
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.REGISTER_CASE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object registerCaseDetails(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.REGISTER_CASE + " service");
		Object returnJson = sqlOperations.registerCaseInfo(caseDetails);
		// TODO Auto-generated method stub
		return returnJson;
	}

	/**
	 * This Function is Used To Fetch case Details of Registered Vehicle.
	 * URL: /roadassistance/fetchcase
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CASE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	Object fetchCaseDetails(@RequestBody CaseDetails caseDetails) {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_CASE_INFO + " service");
		ArrayList<HashMap<String, Object>> returnJson = (ArrayList<HashMap<String, Object>>) sqlOperations.getCaseInfo(caseDetails);
		HashMap<String, Object> caseMap= new HashMap<String, Object>();

		if((returnJson).size()==0){
			caseMap.put(CommonResponseConstants.ISSUCCESS, true);
			caseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"No Cases Registered");
		}else{
			caseMap.put(ResponseConstantsOfCaseDetails.SERVICEHISTORY,returnJson); 
			caseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}

		return caseMap;
	}

	/**
	 * This Function is Used To Fetch list of services .
	 * URL: /roadassistance/listofservices
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_LIST_OF_SERVICES, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody List<Map<String, Object>> fetchServicesList() {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_LIST_OF_SERVICES + " service");
		List<Map<String, Object>> servicesList = sqlOperations.getServiceList();
		// TODO Auto-generated method stub
		return servicesList;
	}
}
