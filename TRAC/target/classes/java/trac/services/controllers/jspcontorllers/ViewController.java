package trac.services.controllers.jspcontorllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class ViewController {
	
	@RequestMapping(value = "/views/index",method = RequestMethod.POST)
	public String viewRedirect() {
		System.out.println("IN trac");
		//model.addAttribute("message", "Spring 3 MVC Hello World");
		return "index";
 
	}

}
