package trac.dao.driverdao;

import trac.beans.driverbean.Driver;
import trac.beans.jdbcteamplatebean.JDBCConnection;

import org.springframework.jdbc.core.JdbcTemplate;

public class DriverSqlOperations {

	JDBCConnection jdbcConnection = new JDBCConnection();
	public static JdbcTemplate jdbcTemplate;

	private static final String SQL_INSERT = "INSERT INTO drivers (DriverName, Email)"
			+ " VALUES (?, ?)";

	private static final String SQL_SELECT = "SELECT * FROM drivers WHERE DriverName=?";

	public String addDriverDetails(String driverName, String driverEmail) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		jdbcTemplate.update(SQL_INSERT, driverName, driverEmail);
		return driverName + " " + driverEmail;
	}

	public Driver getDriverDetails(String driverName) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		Driver driverInfo = (Driver) jdbcTemplate.queryForObject(SQL_SELECT,
				Driver.class, driverName);
		return driverInfo;
	}

}
