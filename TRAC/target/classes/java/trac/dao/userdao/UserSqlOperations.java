package trac.dao.userdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.userbean.Users;

import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUser;
import trac.constants.sqlqueryconstants.UserSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;

import trac.util.CustomHashing;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;
import trac.util.utilinterface.SpringByCryptoHashing;

public class UserSqlOperations {

	public static JdbcTemplate jdbcTemplate;
	private SpringByCryptoHashing hashing = new CustomHashing();
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	
	/**
	 * This method is used to register the details of first time user it returns
	 * user object contains all related info of user
	 * 
	 * @param userName
	 * @spname usp_Registration_Users_Insert
	 * @return Users plain java object contains registered user info
	 * @author rpenta
	 */
	public Object addUserDetails(Users userDetails) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_REGISTRATION_SPNAME)
				.returningResultSet("userRegisterStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum){
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									if (rs.getBoolean("issuccess")) {
										responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
								
										responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
										responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
										responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
										responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
										responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,true);
										responseMap.put(CommonResponseConstants.ISSUCCESS,true);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User Registered Successfully.");
									} else {
										responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,false);
										responseMap.put(CommonResponseConstants.ISSUCCESS,false);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User already Registered/Invalid Data");
									}
	
									return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"imobileNumber","iemail","ipwdHash","iuserName","ideviceTypeID","iapplicationVersion","isecurityQuestionID","isecurityQuestion","isecurityQuestionHash","iRoleID","icreatedBy","iDisplayName"};
		Object[] inParamaterValues = {userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getUserName(),userDetails.getDeviceTypeId(),userDetails.getApplicationVersionNo(),userDetails.getSecurityQuestionId(),userDetails.getSecurityQuestion(),hashing.encryption(userDetails.getSecurityAnswer()),userDetails.getRoleId(),userDetails.getUserName(),userDetails.getDisplayName()};
		
	    // executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userRegisterStatus");
		
		return returnjson.get(0);
	}

	/**
	 * This method is used to fetch the details of user it returns user object
	 * contains all related info of user
	 * 
	 * @param userDetails
	 * @return Users plain java object contains fetched user info
	 * @see
	 */
	public Users getUserDetails(Users userDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		Users tempUserDetails = new Users();
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				UserSqlQueryConstants.SELECT_USER, userDetails.getLoginId(),
				userDetails.getLoginId());
		
		for (Map<String, Object> row : rows) {
			tempUserDetails.setUserId((int) row.get("userID"));
			tempUserDetails.setEmail((String) row.get("email"));
			tempUserDetails.setPhone((String) row.get("phone"));

			tempUserDetails.setHashedPassword((String) row.get("pwdHash"));
			tempUserDetails.setRetryCount((int) row.get("passwordRetryCount"));
			tempUserDetails.setApplicationVersionNo((String) row.get("applicationVersionNo"));
			tempUserDetails.setProfilePicURL((String) row.get("profilePicData"));
			tempUserDetails.setDisplayName((String) row.get("displayName"));
		}
		// Condition to Check if record is available return userdetails
		if (rows.size() > 0 && rows != null)
			return tempUserDetails;
		else
			return null;
	}
	
	/**
	 * This method is used to fetch the details of user it returns user object
	 * contains all related info of user
	 * 
	 * @param userName
	 * @return Users plain java object contains fetched user info
	 * @see
	 */
	public Users getUserDetailsById(int userId) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		Users tempUserDetails = new Users();
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				UserSqlQueryConstants.SELECT_USER_BY_ID, userId);
		
		for (Map<String, Object> row : rows) {
						tempUserDetails.setHashedPassword((String) row.get("pwdHash"));
		}
		// Condition to Check if record is available return userdetails
		if (rows.size() > 0 && rows != null)
			return tempUserDetails;
		else
			return null;
	}

	/**
	 * This method is used to check for uniqueness of user along the application
	 * it returns true if user doesn't exists else false
	 * 
	 * @param userName
	 * @param emailId
	 * @return true/false
	 * @see
	 */
	public boolean checkForUnique(String userName, String emailId) {

		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.UNIQUE_USERNAME;
		int rowCount = 0;
		boolean isUserNameExists = false;

		if (userName != null && userName.length() > 0) {
			dynamicSqlQuery = dynamicSqlQuery + " and userName = ? ";
			isUserNameExists = true;
		}
		if (emailId != null && emailId.length() > 0) {
			dynamicSqlQuery = dynamicSqlQuery + " and email = ?";

			if (isUserNameExists) {
				rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, userName,
						emailId);
			} else {
				rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, emailId);
			}
		} else {
			rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, userName);
		}

		if (rowCount > 0)
			return true;
		else
			return false;
	}

	/**
	 * This method is used to edit the details of user it returns 0/1 based on
	 * updation
	 * 
	 * @param Users
	 *            user plain java object
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */
	public Object editUserDetails(Users userDetails) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_UPDATE_SPNAME)
				.returningResultSet("userUpdateStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum)  {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									if (rs.getBoolean("issuccess")) {
										responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
										responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
										responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
										responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
										responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
										responseMap.put(ResponseConstantsOfUser.CLOUD_PIC_FILENAME,rs.getString("userFileNameOnCloud"));
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
										responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
										responseMap.put(CommonResponseConstants.ISSUCCESS,true);
									} else {
										responseMap.put(CommonResponseConstants.ISSUCCESS,false);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
										responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
									}
									return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});
		
		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","imobileNumber","iemail","ipwdHash","iDisplayName"};
		Object[] inParamaterValues = {userDetails.getUserId(),userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getDisplayName()};
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

			System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		
		@SuppressWarnings("unchecked")
		List<Users> returnjson = (List<Users>) simpleJdbcCallResult.get("userUpdateStatus");

		return returnjson.get(0);

	}

	/**
	 * This method is used to fetch the security question of user when he tries
	 * option to retrieve forgot password using email option it returns security
	 * question if email matches or else returns null
	 * 
	 * @param email
	 *            email of user
	 * @spname usp_Registration_SecurityQuestion_Users_Select
	 * @return security question
	 * @see
	 */
	public Object fetchSecurityQuestion(String email, String phone) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_SECURITYQUESTION_SPNAME)
				.returningResultSet("userUpdateStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									System.out.println("count" + rowNum);
									System.out.println("Description"
											+ rs.getString("Description"));
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									// Condition To Check Description is not null then return security question
									if (rs.getString("Description") != null) {
										responseMap.put(ResponseConstantsOfUser.SECURITYQUESTION,
														rs.getString("Description"));
										responseMap.put(CommonResponseConstants.ISSUCCESS,
												true);
									} else
										responseMap.put(CommonResponseConstants.ISSUCCESS,
												false);
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});

		// IN parameters for stored procedure
		
		String[] inParamaters = {"iemail","iphone"};
		Object[] inParamaterValues = {email,phone};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("userUpdateStatus");
		if (returnjson.size() == 0) {
			HashMap<String, Object> responseMap = new HashMap<String, Object>();

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Email/Phone doesn't Exist.");
			returnjson.add(responseMap);
		}
		return returnjson.get(0);
	}

	/**
	 * This method is used to validate the security answer provided by user when
	 * he tries option to retrieve forgot password it returns true if securtiy
	 * answer matches or else returns false
	 * 
	 * @param Users
	 *            user plain java object
	 * @return userid , true (or) false
	 * @see
	 */

	public HashMap<String, Object> validateSecurityAnswer(Users userDetails) {
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		// get JDBC Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		String hashedSecAnswer = null;
		String userId = null;
		boolean isSuccess = false;
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				UserSqlQueryConstants.GET_SEC_ANSWER, userDetails.getLoginId(),
				userDetails.getLoginId());

		for (Map<String, Object> row : rows) {
			hashedSecAnswer = row.get("securityQuestionHash").toString();
			userId = row.get("userID").toString();
		}

		if (rows.size() > 0) {
			isSuccess = hashing.decryption(userDetails.getSecurityAnswer(),hashedSecAnswer);
			int updateCount = jdbcTemplate.update(
					UserSqlQueryConstants.UPDATE_SEC_CHECK, 1,
					userDetails.getLoginId(), userDetails.getLoginId());
			System.out.println("updateCount - " + updateCount);
		}

		if (rows.size() == 0) {
			isSuccess = false;
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Invalid email id/phone");
		}

		if (isSuccess) {
			responseMap.put(CommonResponseConstants.USERID, userId);
		} else {
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Invalid security answer");
		}

		responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

		return responseMap;

	}

	/**
	 * This method is used to update the retry count if user provides incorrect
	 * password for multiple times it returns 1 if passwordRetryCount column
	 * updates successfully or else returns 0
	 * 
	 * @param userName
	 *            unique user name to search for record to update
	 * @return 1 (or) 0
	 * @see
	 */
	public int updateUserLoginAttempts(int userID) {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.UPDATE_LOGIN_ATTEMPTS;
		return jdbcTemplate.update(dynamicSqlQuery, userID);

	}

	/**
	 * This method is used to update the profile pic of user it returns 1 if
	 * profile pic column updates successfully or else returns 0
	 * 
	 * @param userID
	 *            unique userID to search for record to update
	 * @param profilePicURL
	 *            profile pic to be updated in user table
	 * @return 1 (or) 0
	 * @see
	 */
	public int updateProfilePic(int userID, String profilePicURL,String userFileNameOnCloud) {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		String dynamicSqlQuery = UserSqlQueryConstants.UPDATE_PROFILE_PIC;
		return jdbcTemplate.update(dynamicSqlQuery, profilePicURL,userFileNameOnCloud, userID);

	}

	/**
	 * This method is used to get the list of security questions list it returns
	 * list of securityQuestions
	 * 
	 * @param
	 * @return list of securityQuestions
	 * @see
	 */
	public List<Map<String, Object>> fetchSecurityInfoList() {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.GET_SEC_QUESTIONS;
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList(dynamicSqlQuery);

		return rows;
	}

	/**
	 * This method is used to update the password of user it returns true/false
	 * 
	 * @param userName
	 * @spname usp_Users_Password_Update
	 * @return Users plain java object contains registered user info
	 * @see
	 */

	public Object updatePassword(Users userDetails,String spName) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(spName)
				.returningResultSet("passwordUpdateStatus",
						new RowMapper<Map<String, Object>>() {
							@Override
							public Map<String, Object> mapRow(ResultSet rs,int rowNum) {
								try{
									Map<String, Object> returnJson = new HashMap<String, Object>();
	
									if (rs.getBoolean(CommonResponseConstants.ISSUCCESS)) {
										returnJson.put(CommonResponseConstants.ISSUCCESS, true);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,"Password changed successfully");
									} else {
										returnJson.put(CommonResponseConstants.ISSUCCESS, false);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,"Problem updating password");
									}
									return returnJson;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});
		

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ipwdHash"};
		Object[] inParamaterValues = {userDetails.getUserId(),userDetails.getHashedPassword()};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult.get("passwordUpdateStatus"));

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("passwordUpdateStatus");
		
		if(returnjson !=  null)
			return returnjson.get(0);
		else
			return null;
	}

}