package trac.dao.casesdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.casesbean.CaseDetails;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.sqlqueryconstants.ServicesSqlContants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.DBUtilInterface;

public class CaseRelatedSqlOperations {
	public static JdbcTemplate jdbcTemplate;
	public static UtilFunctions utility = new UtilFunctions();
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	/**
	 * This Method is used to registered caseDetails of Existing user.
	 * @spname usp_RoadAssassinate_cases_Insert
	 * @param caseDetails
	 * @return if true returns casedetails plain java object,or returns false
	 */

	public HashMap<String, Object> registerCaseInfo(CaseDetails caseDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		String deviceName = null;

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_REGISTRATION_SPNAME)
		.returningResultSet("caseRegisterStatus",
				new RowMapper<Map<String, Object>>() {
			@Override
			public Map<String, Object> mapRow(ResultSet rs,int rowNum){
					try{
						Boolean isSuccess = rs.getBoolean("isSuccess");
						Map<String, Object> returnResult = new HashMap<String, Object>();
		
						if (isSuccess) {
							returnResult.put(ResponseConstantsOfCaseDetails.CASEID,rs.getString("icaseNumber"));
							returnResult.put(CommonResponseConstants.SERVICECODE,rs.getString("vserviceType"));
							returnResult.put(CommonResponseConstants.VEHICLEID,rs.getString("iVehicleID"));
							returnResult.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLATITUDE,rs.getString("ilatitude"));
							returnResult.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLONGITUDE,rs.getString("ilongitude"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTEREDTIME,rs.getString("itime"));
							returnResult.put("caseNotes",rs.getString("inotes"));
							returnResult.put(CommonResponseConstants.ISSUCCESS, true);
							returnResult.put(CommonResponseConstants.SERVICEMESSAGE,"Case registered successfully");
						} else {
							returnResult.put(CommonResponseConstants.ISSUCCESS, false);
							returnResult.put(CommonResponseConstants.SERVICEMESSAGE,"Previous case not yet closed still");
						}
	
					return returnResult;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});
		//Condition to check user Registered Case Throw Mobile or web.
		if (caseDetails.getDeviceTyepId() == 2
				|| caseDetails.getDeviceTyepId() == 1)
			deviceName = "MBCASE";
		else
			deviceName = "WEBCASE";

		String caseId = utility.getUserIdService(deviceName,
				caseDetails.getServiceCode(), ""+caseDetails.getUserId());
		// IN parameters for stored procedure
		String[] inParamaters = {"icaseNumber","iuserID","iserviceTypeID","iVehicleID","iLocID","ilatitude","ilongitude","itime","iStatus","inotes","iServiceRequiredTypeId","iServiceRequiredTime"};
		Object[] inParamaterValues = {caseId,caseDetails.getUserId(),caseDetails.getServiceCode(),caseDetails.getVehicleId(),caseDetails.getLocationId(),caseDetails.getServiceLocationLatitude(),caseDetails.getServiceLocationLongitude(),caseDetails.getCaseRegisteredTime(),caseDetails.getCaseStatus(),caseDetails.getCaseNotes(),caseDetails.getServiceRequiredTypeId(),caseDetails.getServiceRequiredTime()};
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseRegisterStatus");
		//return Json
		return returnjson.get(0);

	}

	/**
	 * This method is used to get the case details of registered user of registered vehicle.
	 * 
	 * @param userID
	 * @spname usp_ServiceHistoryAPI_Select
	 * @return casedetails plain java object
	 */
	public Object getCaseInfo(CaseDetails caseDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_HISTORY_FETCH_SPNAME)
		.returningResultSet("caseDetailsList",
				new RowMapper<Map<String, Object>>() {
			@Override
			public Map<String, Object> mapRow(ResultSet rs,
					int rowNum) {
				try{
					Map<String, Object> returnResult = new HashMap<String, Object>();
					returnResult.put(ResponseConstantsOfCaseDetails.SERVICE_TYPE,
							rs.getString("serviceType"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASEID,
							rs.getString("caseNumber"));
					returnResult.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME,
							rs.getString("serviceRequestedAt"));
					returnResult.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE,
							rs.getString("serviceType"));
					returnResult.put(ResponseConstantsOfCaseDetails.VEHICLE_NAME,
							rs.getString("vehicleName"));
					returnResult.put(ResponseConstantsOfCaseDetails.LOCATION_NAME,
							rs.getString("locationName"));
					returnResult.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLATITUDE,
							rs.getString("GPSLOCationLat"));
					returnResult.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLONGITUDE,
							rs.getString("GPSLOCationLong"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS,
							rs.getString("statusID"));
					return returnResult;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {caseDetails.getUserId()};
		
			// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		Object returnJson =  simpleJdbcCallResult.get("caseDetailsList");

		return returnJson;

	}
	/**
	 * This method is used to display the list of registered services.
	 * @return
	 */
	public List<Map<String, Object>> getServiceList() {
		//get JDBC Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = ServicesSqlContants.GET_LIST_OF_SERVICES;
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList(dynamicSqlQuery);

		return rows;
		// TODO Auto-generated method stub

	}

}
