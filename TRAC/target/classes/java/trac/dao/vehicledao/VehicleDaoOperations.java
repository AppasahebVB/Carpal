package trac.dao.vehicledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import trac.beans.jdbcteamplatebean.JDBCConnection;

//import trac.beans.userbean.UserLocation;
import trac.beans.vehcilebean.Vehicle;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;

import trac.constants.sqlqueryconstants.VehicleSqlConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


public class VehicleDaoOperations {


	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	
	/**
	 * This method is used to register the details of first time user 
	 * it returns user object contains all related info  of user
	 *
	 * @param  userName 
	 * @spname  usp_Vehicle_Vehicles_Insert
	 * @return Users plain java object contains registered user info
	 * @author rpenta     
	 */	
	public HashMap<String, Object> registerVehcileInfo(Vehicle vehicleDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_REGISTRATION_SPNAME).returningResultSet("userStatus",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
						HashMap<String, Object> responseMap = new HashMap<String, Object>();
		
						if(rs.getBoolean("issuccess")){
		
							responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
							responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL, rs.getString("vehiclePicURL"));
							
							if(rs.getInt("isFavorite") == 1)
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
							else
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle registred successfully.");
		
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle already registered");
							responseMap.put(CommonResponseConstants.ISSUCCESS, false);
						}
						return responseMap;
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
			});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ivehicleName","iChassis","iBrandModelID","ifavorite","ivehicleModelID","ivehicleRegisterNo","ivehicleModelYear"};
		Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getVehicleName(),vehicleDetails.getVin(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getIsFavorite(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getVehicleModelYear()};
		
	    // executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userStatus");
		return  returnjson.get(0);
	}


	/**
	 * This method is used to edit the vehicleDetails of Existing User
	 * it returns user object contains all related info  of user
	 * 
	 * @param  userId,vehicleDetails
	 * @spname  usp_Vehicle_Vehicles_Update
	 * @return true or false
	 */

	public Object editVehcileInfo(Vehicle vehicleDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_UPDATE_SPNAME).returningResultSet("updateStatus",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
	
					System.out.println("Vehicle Update IsSuccess = "+rs.getBoolean(1));
					if(rs.getBoolean(1)){
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						responseMap.put(ResponseConstantsOfVehicle.VEHICLE_FILENAME, rs.getString("vehicleFileNameOnCloud"));
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, rs.getBoolean("isFavorite"));
						responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
						responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
	
	
					return responseMap;
				}catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
			});

				System.out.println("Vehicle UVIN = "+vehicleDetails.getVin());
								
				//IN parameters for stored procedure
				String[] inParamaters = {"iuserID","ivehicleName","iVehicleId","iChassis","iBrandModelID","ifavorite","ivehicleModelID","ivehicleRegisterNo","ivehicleModelYear"};
				Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getVehicleName(),vehicleDetails.getVehicleId(),vehicleDetails.getVin(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getIsFavorite(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getVehicleModelYear()};
				
				//executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				@SuppressWarnings("unchecked")
				List<Vehicle> vehiclesList = (List<Vehicle>) simpleJdbcCallResult.get("updateStatus");
			
				if(vehiclesList.size() > 0)
					return vehiclesList.get(0);
				else
					return null;
	}


	/**
	 * This method is used to get the vehicleDetails of Existing User
	 * it returns user object contains all related info  of user
	 * 
	 * @param  userId
	 * @spname  usp_Vehicle_Vehicles_Select
	 * @return userId And registered vehicle details
	 * @author rpenta
	 */
	public Object getVehcileInfo(Vehicle vehicleDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_FETCH_SPNAME)
		.returningResultSet("vehicleDetailsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					
					responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
					if(rs.getInt("isFavorite") == 1)
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
					else
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
	
	
	
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {vehicleDetails.getUserId()};
		
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		//executing stored procedure
		System.out.println("vehicle details list - "+simpleJdbcCallResult.get("vehicleDetailsList"));
		return simpleJdbcCallResult.get("vehicleDetailsList");
	}

	/**
	 *This method is used to get the all vehicle brands list of Corresponding Model.
	 *
	 *@param userid
	 *@spname usp_vehicles_vehicleBrands_select
	 * @return userid, List of Vehicle Brands
	 */
	public Object getVehicleBrands() {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.GET_VEHICLE_BRAND_FETCH_SPNAME)
		.returningResultSet("vehicleBrandsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
	
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getLong("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName"));
	
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
			});

		//IN parameters for stored procedure
		Map<String, Object> inParamMap = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(in);
		
		
		System.out.println("vehicle brands list - "+simpleJdbcCallResult.get("vehicleBrandsList"));
		return simpleJdbcCallResult.get("vehicleBrandsList");
	}

	/**
	 *This method is used to update the vehicle image that was uploaded.
	 *
	 *@param vehicleId,vehcilePicUrl
	 *@spname 
	 * @return 1 if updated or else 0 if not updated
	 */
	public int updateVehcilePic(int vehicleId, String vehcilePicUrl, String vehicleFileNameOnCloud) {
		       //Jdbc Connection
				jdbcTemplate = JDBCConnection.getJdbcTemplate();
				System.out.println("jdbcTemplate - "+jdbcTemplate);
				
				String dynamicSqlQuery = VehicleSqlConstants.UPDATE_VEHICLE_PIC;
				return jdbcTemplate.update(dynamicSqlQuery,vehcilePicUrl,vehicleFileNameOnCloud,vehicleId);
				
				

	}
}
