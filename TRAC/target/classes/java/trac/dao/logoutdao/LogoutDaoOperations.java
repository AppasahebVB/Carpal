package trac.dao.logoutdao;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.sqlqueryconstants.LogoutSqlQueryConstants;

public class LogoutDaoOperations {

	public JdbcTemplate jdbcTemplate;
	

	/**
	 * This Method is used to generate Token Id Corresponding UserID.
	 * 
	 * @param 
	 * @return true or false
	 */
	public boolean accessTokenValidation(String userId) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		List<Map<String, Object>> rows =jdbcTemplate.queryForList(LogoutSqlQueryConstants.GET_USERIN_AUTH_TABLE,userId);
		//Condition To check Token is available return true else false
		if(rows.size() > 0 && rows != null)
			return true;
		else
			return false;
	}
	
	
}
