package trac.twiliosms;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;

import trac.twiliosms.smsinterface.SMSInterface;

public class SMS implements SMSInterface{
	private static final Logger logger = LoggerFactory.getLogger(SMS.class);
	 /* Find your sid and token at twilio.com/user/account */
	public static final String ACCOUNT_SID = "AC123";
	public static final String AUTH_TOKEN = "456bef";
	
	public void sendSmsNotification(){
		//Create a Twilio REST client
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
		Account account = client.getAccount();
		//Use the API to send a text message
		SmsFactory smsFactory = account.getSmsFactory();
		Map<String, String> smsParams = new HashMap<String, String>();
		smsParams.put("To", "+14105551234");
		smsParams.put("From", "(410) 555-6789"); // Replace with a Twilio phone number in your account
		smsParams.put("Body", "Where's Wallace?");
		
		try {
			Sms sms = smsFactory.create(smsParams);
		} catch (TwilioRestException exception) {
			// TODO Auto-generated catch block
			logger.info("exception message " + exception.getMessage());
			exception.printStackTrace();
		}
		
	}

}
