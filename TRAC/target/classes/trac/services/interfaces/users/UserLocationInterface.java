package trac.services.interfaces.users;

import java.util.HashMap;

import trac.beans.userbean.UserLocation;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface UserLocationInterface {
	
	 public @ResponseBody HashMap<String, Object> registerUserLocationDetails(@RequestBody UserLocation userLocationDetails);
	 public @ResponseBody HashMap<String, Object> updateUserLocationDetails(@RequestBody UserLocation userLocationDetails);
	 public @ResponseBody HashMap<String, Object> fetchUserLocationDetails(@RequestBody UserLocation userLocationDetails);
	 public @ResponseBody HashMap<String, Object> unregisterUserLocationDetails(@RequestBody UserLocation userLocationDetails);

}
