package trac.services.controllers.shift;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.shiftbean.Shift;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfShift;
import trac.constants.resturlconstants.RestUrlConstantsOfShift;
import trac.dao.shiftdao.ShiftDaoOperations;
import trac.services.interfaces.shift.ShiftInterface;

@Controller
public class ShiftController implements ShiftInterface{
	private static final Logger logger = LoggerFactory.getLogger(ShiftController.class);
	private ShiftDaoOperations sqlOperations = new ShiftDaoOperations();

	/**
	 * This method is to save shift timing
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/insertShiftTimmings
	 * @return success  (or) failure
	 */
			
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.INSERT_SHIFT_TIMMINGS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerShiftTimings(@RequestBody HashMap<String, Object> requestBody) {
		logger.info("IN "+ RestUrlConstantsOfShift.INSERT_SHIFT_TIMMINGS + " service");
		System.out.println("in "+  RestUrlConstantsOfShift.INSERT_SHIFT_TIMMINGS  +" service");
		List<HashMap<String, Object>> responseListMap = null ;
		System.out.print(requestBody.containsKey("userID"));
		System.out.print(requestBody.containsKey("shiftDayId"));
		System.out.print(requestBody.containsKey("shifttimingsList"));
		if(requestBody.containsKey("userID") && requestBody.containsKey("shiftDayId") && requestBody.containsKey("shifttimingsList")){
		 responseListMap = sqlOperations.insertShiftInfo((int)requestBody.get("userID"),(int)requestBody.get("shiftDayId"),(ArrayList<LinkedHashMap<String, Object>>)requestBody.get("shifttimingsList"));
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfShift.SHIFT_DETAILS_SAVE_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is to update Shift Timing
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/editShiftTimmings
	 * @return success  (or) failure
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.UPDATE_SHIFT_TIMMINGS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateShiftTimings(@RequestBody Shift shiftDetails) {
		logger.info("IN "+ RestUrlConstantsOfShift.UPDATE_SHIFT_TIMMINGS + " service");
		List<HashMap<String, Object>> responseListMap = sqlOperations.updateShiftInfo(shiftDetails);
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfShift.SHIFT_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is to delete Shift Timing
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/deleteShiftTimmings
	 * @return success (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.DELETE_SHIFT_TIMMINGS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> deleteShiftTimings(@RequestBody Shift shiftDetails) {
		logger.info("IN "+ RestUrlConstantsOfShift.DELETE_SHIFT_TIMMINGS + " service");
		List<HashMap<String, Object>> responseListMap = sqlOperations.deleteShiftInfo(shiftDetails.getShiftId());
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfShift.SHIFT_DELETION_FAILED);
		}
		return resultMap;
	}

	/**
	 * This method is to get all Shift Timings
	 * 
	 * @serviceurl /shift/getShifts
	 * @return success with shift timings List (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.GET_LIST_OF_SHIFTS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> getListOfShifts(@RequestBody Shift shiftDetails) {
		logger.info("IN "+ RestUrlConstantsOfShift.GET_LIST_OF_SHIFTS + " service");

		List<Map<String, Object>> shiftsList = (List<Map<String, Object>>) sqlOperations.getShiftInfoByDay(shiftDetails.getShiftDayId());
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if(shiftsList!=null && shiftsList.size()>0){
			returnObject.put(ResponseConstantsOfShift.SHIFTLIST, shiftsList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_NOT_FOUND);

		}
		return returnObject;

	}
	
	/**
	 * This method is to get all Shift Timings
	 * 
	 * @serviceurl /shiftTimmings/getShiftTimmings
	 * @return success with shift timings List (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.GET_SHIFT_TIMMINGS, method = RequestMethod.GET,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchShiftTimings() {
		logger.info("IN "+ RestUrlConstantsOfShift.GET_SHIFT_TIMMINGS + " service");
		HashMap<String, Object> returnObject = new HashMap<String, Object>();
		List<Shift> shiftsList = (List<Shift>) new ShiftDaoOperations().getListOfShifts();
		if(shiftsList!=null && shiftsList.size()>0){
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			returnObject.put(ResponseConstantsOfShift.SHIFTLIST, shiftsList);
		}else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_NOT_FOUND);
		}
		return returnObject;

	}
	/**
	 * This method is to save shift timing
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/insertShiftTimmings
	 * @return success  (or) failure
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.SHIFT_SCHEDULE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> getShiftScheduleInfo(@RequestBody Shift shift) {
		logger.info("IN "+ RestUrlConstantsOfShift.SHIFT_SCHEDULE + " service");
		System.out.println("in "+  RestUrlConstantsOfShift.SHIFT_SCHEDULE  +" service");
		List<Object>  responseList = null ;
		List<HashMap<String, Object>> driverscheduleInfo = null;
		List<HashMap<String, Object>> shiftlist = null;
		HashMap<String, Object> dateAndDayList = null;
		List<HashMap<String, Object>> dayWiseShiftCountList = null;
		List<String> dateList = null;
		List<Integer> dayList = null;
		List<HashMap<String, Object>> differentShiftList = null;
		
		HashMap<String, Object> responseMap =new HashMap<String, Object>();
		
		try {

			 responseList =  sqlOperations.getScheduleOfDrivers(shift);
		} catch (Exception e) {
			e.printStackTrace();
		}


			driverscheduleInfo = (List<HashMap<String, Object>>)responseList.get(0);
			shiftlist = (List<HashMap<String, Object>>)responseList.get(1);
			dateAndDayList =  (HashMap<String, Object>) responseList.get(2);
			dayWiseShiftCountList = (List<HashMap<String, Object>>)responseList.get(3);
			dateList = (List<String>) dateAndDayList.get("datesList");
			differentShiftList = (List<HashMap<String, Object>>) responseList.get(4);
			dayList = (List<Integer>) dateAndDayList.get("daysList");
			
			if(driverscheduleInfo.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Driver schedule not found");
			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}

			responseMap.put("startDate",dateList.get(0));
		responseMap.put("endDate",dateList.get(dateList.size() - 1));
		responseMap.put("totalNoOfShifts",shiftlist.size());
		responseMap.put("dayWiseShiftCountList",dayWiseShiftCountList);
		responseMap.put("driverScheduleList",driverscheduleInfo);
		responseMap.put("shiftlist",shiftlist);
		responseMap.put("differentShiftList", differentShiftList);
		responseMap.put("daysList", dayList);
		//responseMap.put("isMonthStartingDay", responseList.get(6));
		responseMap.putAll((Map<String,Object>) responseList.get(5));
		
		return responseMap;
	}

	
	/**
	 * This method is to save shift timing
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/insertShiftTimmings
	 * @return success  (or) failure
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.GENARATE_SHIFT_SCHEDULE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> genarateShiftSchedule(@RequestBody Shift shift) {
		logger.info("IN "+ RestUrlConstantsOfShift.GENARATE_SHIFT_SCHEDULE + " service");
		System.out.println("in "+  RestUrlConstantsOfShift.INSERT_SHIFT_TIMMINGS  +" service");
		
		HashMap<String, Object> responseMap =new HashMap<String, Object>();
		
		try {
			responseMap =  sqlOperations.genarateScheduleOfDrivers(shift);
		} catch (Exception e) {
			e.printStackTrace();
		}


		
		return responseMap;
	}
	
	/**
	 * This method is to update Shift and dayOff  of Driver
	 * 
	 * @param shiftDetails
	 * @serviceurl /shiftTimmings/editShiftTimmings
	 * @return success  (or) failure
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfShift.UPDATE_SCHEDULE_OF_DRIVER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateDriverSchedule(@RequestBody Shift shiftDetails) {
		logger.info("IN "+ RestUrlConstantsOfShift.UPDATE_SCHEDULE_OF_DRIVER + " service");
		List<Object>  responseList = null ;
		List<HashMap<String, Object>> driverscheduleInfo = null;
		List<HashMap<String, Object>> shiftlist = null;
		HashMap<String, Object> dateAndDayList = null;
		List<HashMap<String, Object>> dayWiseShiftCountList = null;
		List<String> dateList = null;
		List<Integer> dayList = null;
		List<HashMap<String, Object>> differentShiftList = null;
		
		HashMap<String, Object> responseMap =new HashMap<String, Object>();
		List<HashMap<String, Object>> responseListMap = sqlOperations.updateDriverSchedule(shiftDetails);
		//HashMap<String, Object> resultMap = null;
	    if(responseListMap!=null && responseListMap.size()>0){
			//resultMap = responseListMap.get(0);
			
			try {

				 responseList =  sqlOperations.getScheduleOfDrivers(shiftDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}


				driverscheduleInfo = (List<HashMap<String, Object>>)responseList.get(0);
				shiftlist = (List<HashMap<String, Object>>)responseList.get(1);
				dateAndDayList =  (HashMap<String, Object>) responseList.get(2);
				dayWiseShiftCountList = (List<HashMap<String, Object>>)responseList.get(3);
				dateList = (List<String>) dateAndDayList.get("datesList");
				differentShiftList = (List<HashMap<String, Object>>) responseList.get(4);
				dayList = (List<Integer>) dateAndDayList.get("daysList");
				
				if(driverscheduleInfo.isEmpty()){
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Driver schedule not found");
				}else{
					responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

				responseMap.put("startDate",dateList.get(0));
			responseMap.put("endDate",dateList.get(dateList.size() - 1));
			responseMap.put("totalNoOfShifts",shiftlist.size());
			responseMap.put("dayWiseShiftCountList",dayWiseShiftCountList);
			responseMap.put("driverScheduleList",driverscheduleInfo);
			responseMap.put("shiftlist",shiftlist);
			responseMap.put("differentShiftList", differentShiftList);
			responseMap.put("daysList", dayList);
			//responseMap.put("isMonthStartingDay", responseList.get(6));
			responseMap.putAll((Map<String,Object>) responseList.get(5));
			
			return responseMap;
		}else{
			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfShift.SHIFT_DETAILS_UPDATE_FAILED);
		}

		return responseMap;
	}
	

}
