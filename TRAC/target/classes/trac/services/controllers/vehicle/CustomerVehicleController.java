package trac.services.controllers.vehicle;


import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.vehcilebean.Vehicle;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.resturlconstants.RestUrlConstantsOfVehicle;
import trac.dao.vehicledao.VehicleDaoOperations;
import trac.services.interfaces.vehicle.CustomerVehicleInterface;

@Controller
public class CustomerVehicleController implements CustomerVehicleInterface{
	private static final Logger logger = LoggerFactory.getLogger(CustomerVehicleController.class);
	private VehicleDaoOperations sqlOperations = new VehicleDaoOperations();
	/**
	 * This Function is Used To Register Vehicle Details of of Registered User.
	 * URL : /vehicle/register
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.REGISTER_CUSTOMER_VEHICLE_INFO,method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.REGISTER_CUSTOMER_VEHICLE_INFO + " service");
		
		
		List<Object> responseList =null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();//getting return object


			//editing vehicle details in TRAC DB
			try {
				responseList = sqlOperations.registerVehcileInfo(vechicleDetails);
			} catch (Exception e) {
				e.printStackTrace();
			} 

			
			if(responseList!=null && responseList.size()>0){

				HashMap<String, Object> vehicleResult = ((List<HashMap<String, Object>>)responseList.get(0)).get(0);
				List<HashMap<String, Object>> locationList =  (List<HashMap<String, Object>>) responseList.get(1);
				if(vehicleResult.isEmpty()){
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,vehicleResult.get(CommonResponseConstants.SERVICEMESSAGE));
				}else if(vehicleResult.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) vehicleResult.get(CommonResponseConstants.ISSUCCESS)){
					responseMap = vehicleResult;
				}
				else{
				responseMap.put("vehicle",vehicleResult);
				responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLIST, locationList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer Vehicle Registration Fialed.");
			}
		return responseMap;
		
	}
	/**
	 * This Function is Used To Update Vehicle Details Of Registered User.
	 * URL : /vehicle/update
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.EDIT_CUSTOMER_VEHICLE_INFO,method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.EDIT_CUSTOMER_VEHICLE_INFO + " service");
		List<Object> responseList =null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();//getting return object


			//editing vehicle details in TRAC DB
			try {
				responseList = sqlOperations.editVehcileInfo(vechicleDetails);
			} catch (Exception e) {
				e.printStackTrace();
			} 

			
			if(responseList!=null && responseList.size()>0){

				HashMap<String, Object> vehicleResult = ((List<HashMap<String, Object>>)responseList.get(0)).get(0);
				List<HashMap<String, Object>> locationList =  (List<HashMap<String, Object>>) responseList.get(1);
				if(vehicleResult.isEmpty()){
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,vehicleResult.get(CommonResponseConstants.SERVICEMESSAGE));
				}else if(vehicleResult.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) vehicleResult.get(CommonResponseConstants.ISSUCCESS)){
					responseMap = vehicleResult;
				}
				else{
				responseMap.put("vehicle",vehicleResult);
				responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLIST, locationList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer Vehicle Updation Fialed.");
			}
		return responseMap;
	}
	/**
	 * This Function is Used To Get All Vehicle Details Of Registered Vehicle.
	 * URL : /vehicle/getvehicleinfo
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.FETCH_CUSTOMER_VEHICLE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.FETCH_CUSTOMER_VEHICLE_INFO + " service");
		//registering vehicle details in TRAC DB
		List<HashMap<String, Object>> responseList =null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();

		 try {
			responseList = (List<HashMap<String, Object>>) sqlOperations.getVehcileInfo(vechicleDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(responseList!=null && responseList.size()>0){
			responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID, vechicleDetails.getCustomerId());
			responseMap.put(ResponseConstantsOfVehicle.VEHICLELIST, responseList);
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicles not Found");

		}
		return responseMap;

	}

}
