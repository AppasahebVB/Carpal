package trac.services.controllers.vehicle;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.gmr.web.multipart.GMultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.vehcilebean.Vehicle;
import trac.constants.cloudconstants.CloudConstantsInfo;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.resturlconstants.RestUrlConstantsOfVehicle;
import trac.dao.vehicledao.VehicleDaoOperations;
import trac.services.interfaces.vehicle.VehicleInterface;
import trac.util.CloudStorageService;
import trac.util.CommonUtil;
import trac.util.UtilFunctions;

@Controller
public class VehicleController implements VehicleInterface{
	private static final Logger logger = LoggerFactory.getLogger(VehicleController.class);
	private VehicleDaoOperations sqlOperations = new VehicleDaoOperations();
	private CloudStorageService cloudService = new CloudStorageService(); // object that have cloud related info
	/**
	 * This method is Used To Register Vehicle Details of of Registered User.
	 * URL : /vehicle/register
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.REGISTER_VEHICLE_INFO, method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object> registerVehicleDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.REGISTER_VEHICLE_INFO + " service");

		//registering vehicle details in TRAC DB
		List<Object> returnedUserDetails =null;
		 try {
			returnedUserDetails = sqlOperations.registerVehcileInfo(vechicleDetails);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HashMap<String, Object> responseMap;

		if(returnedUserDetails!=null && returnedUserDetails.size()>0){
			responseMap = ((List<HashMap<String, Object>>)returnedUserDetails.get(0)).get(0);
			
			if(responseMap.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,responseMap.get(CommonResponseConstants.SERVICEMESSAGE));
			}
			else if((responseMap.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) responseMap.get(CommonResponseConstants.ISSUCCESS))){
				
			}
			else{
			
				responseMap.put(CommonResponseConstants.ISSUCCESS,true);
			String vehcilePicUrl = null;
			String vehicleNameOnCloud = vechicleDetails.getVehicleName() + UtilFunctions.getDate();
			vechicleDetails.setVehicleFileNameOnCloud(vehicleNameOnCloud); 

			//check whether is there any multipart content exist 
			if((boolean)responseMap.get(CommonResponseConstants.ISSUCCESS) && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null){
				if (gMultiPartData.getSize() != 0) {
					Integer vehicleId = new Integer((int)responseMap.get(CommonResponseConstants.VEHICLEID));
					//Blob file = new Blob(gMultiPartData.getBytes());
					vehcilePicUrl = cloudService.uploadImagesToCloudBucket(gMultiPartData,vechicleDetails.getVehicleFileNameOnCloud(),null,response,CloudConstantsInfo.VEHICLE_BUCKETNAME);

					int updateCount =sqlOperations.updateVehcilePic(vehicleId,vehcilePicUrl,vechicleDetails.getVehicleFileNameOnCloud());
					//userDetails.setProfilePicURL(profilePicURL);
					if(updateCount == 1)
						responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,vehcilePicUrl);
					else
						responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,null);
				}
			}
			}
		}else{
			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle Registration Fialed.");
		}
		return responseMap;
	}
	/**
	 * This method is Used To Update Vehicle Details Of Registered User.
	 * URL : /vehicle/update
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.UPDATE_VEHICLE_INFO, method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object> updateVehicleDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Vehicle vechicleDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.UPDATE_VEHICLE_INFO + " service");


		//editing vehicle details in TRAC DB
		List<Object> returnJson = null;
		try {
			returnJson = sqlOperations.editVehcileInfo(vechicleDetails);
		} catch (Exception e) {
			e.printStackTrace();
		} 

		
		
		HashMap<String, Object> responseMap;//getting return object

		if(returnJson!=null && returnJson.size()>0){

			responseMap = ((List<HashMap<String, Object>>)returnJson.get(0)).get(0);
			

			if(responseMap.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle Updation Fialed.");
			}
			else if((responseMap.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) responseMap.get(CommonResponseConstants.ISSUCCESS))){
				
			}
			else{

				String vehcilePicUrl = null;
				String  isImageChanged = request.getParameter("isImageChanged");
				String vehicleNameOnCloud = vechicleDetails.getVehicleName() + UtilFunctions.getDate();
				vechicleDetails.setVehicleFileNameOnCloud(vehicleNameOnCloud); 


				//check whether is there any multipart content exist 
				if(isImageChanged.equals("true") && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null && responseMap != null){//checking for multipart content
					if (gMultiPartData.getSize() != 0) {
						Integer vehicleId = new Integer(vechicleDetails.getVehicleId());
						//Blob file = new Blob(gMultiPartData.getBytes());
						vehcilePicUrl = cloudService.uploadImagesToCloudBucket(gMultiPartData,vechicleDetails.getVehicleFileNameOnCloud(),(String)responseMap.get(ResponseConstantsOfVehicle.VEHICLE_FILENAME),response,CloudConstantsInfo.VEHICLE_BUCKETNAME);

						int updateCount =sqlOperations.updateVehcilePic(vehicleId,vehcilePicUrl,vechicleDetails.getVehicleFileNameOnCloud());//updating vehicle pic url and vehicle pic name

						if(updateCount == 1) //if updated successfully
							responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,vehcilePicUrl);
						else //if not updated
							responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL,null);
					}
				}else if(!(boolean)responseMap.get(CommonResponseConstants.ISSUCCESS)){//if nothing to edit
					System.out.println("IN ELSE");
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Nothing to update");
					responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				}

				if(responseMap.containsKey(ResponseConstantsOfVehicle.VEHICLE_FILENAME))
					responseMap.remove(ResponseConstantsOfVehicle.VEHICLE_FILENAME);//removing parameter from map

			}}else{
				responseMap = new HashMap<String, Object>();
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle Updation Fialed.");
			}
		return responseMap;
	}
	/**
	 * This method is Used To Get All Vehicle Details Of Registered Vehicle.
	 * URL : /vehicle/getvehicleinfo
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.GET_VEHICLE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchVehicleDetails(@RequestBody Vehicle vechicleDetails) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.GET_VEHICLE_INFO + " service");
		//registering vehicle details in TRAC DB
	
		List<HashMap<String, Object>> responseList = (List<HashMap<String, Object>>) sqlOperations.getVehcileInfo(vechicleDetails);
		HashMap<String, Object> returnObject = new HashMap<String, Object>();


		if(responseList!=null && responseList.size()>0){
			returnObject.put(CommonResponseConstants.USERID, vechicleDetails.getUserId());
			returnObject.put(ResponseConstantsOfVehicle.VEHICLELIST, responseList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicles not Found");

		}
		return returnObject;

	}


	/**
	 * Unregister Vehicle
	 * URL : /vehicle/unregistervehicleinfo
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.UNREGISTER_VEHICLE_INFO, method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody HashMap<String, Object> unregisterVehicleDetails(@RequestBody Vehicle vehicleDetails) {
		logger.info("IN " + RestUrlConstantsOfVehicle.UNREGISTER_VEHICLE_INFO+ " service");
		List<HashMap<String, Object>> responseList = null;
		try {
			responseList = sqlOperations.deleteVehilceInfo(vehicleDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if (responseList != null && responseList.size() > 0) {
			returnObject = responseList.get(0);
		} else {
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Vehicle Unregistration Failed.");

		}
		return returnObject;

	}

	/**
	 * This method is Used To Get All Vehicle Brands Of Corresponding Model .
	 * URL : /vehicle/getvehiclebrands
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.GET_VEHICLE_BRANDS, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchVehicleBrandsDetails() {
		logger.info("IN " + RestUrlConstantsOfVehicle.GET_VEHICLE_BRANDS+ " service");
		// registering vehicle details in TRAC DB
		List<Object> resultList = (List<Object>) sqlOperations
				.getVehicleBrands();
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if (resultList != null && resultList.size() > 0) {

			try {
				ArrayList<Object> brandsList = new CommonUtil().processVehicleBrands((ArrayList<Object>) resultList.get(0));
				returnObject.put(ResponseConstantsOfVehicle.VEHICLEBRANDSLIST,brandsList);
				returnObject.put("vinmodelsList",resultList.get(1));
				returnObject.put("vinmodelyearList",resultList.get(2));
				returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		} else {
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Vehicle Brands not Found");

		}
		return returnObject;

	}
	
	/**
	 * This method is Used To update vehicle Display Order
	 * URL : /vehicle/editDisplayorder
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfVehicle.EDIT_VEHICLE_DISPLAY_ORDER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editVehicleDisplayOrder(@RequestBody ArrayList<LinkedHashMap<String, Object>> vechicles) {
		logger.info("IN "+ RestUrlConstantsOfVehicle.GET_VEHICLE_INFO + " service");
		//registering vehicle details in TRAC DB
		List<HashMap<String, Object>> responseList = (List<HashMap<String, Object>>) sqlOperations.updateVehcileDisplayOrder(vechicles);
		HashMap<String, Object> returnObject = new HashMap<String, Object>();


		if(responseList!=null && responseList.size()>0){
			
			returnObject = responseList.get(0);
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle Display Order Updation Failed.");

		}
		return returnObject;

	}
	
}
