package trac.services.controllers.users;


import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.userbean.UserLocation;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.resturlconstants.RestUrlConstantsOfUserLocation;
import trac.dao.userdao.UserLocationDaoOperations;
import trac.services.interfaces.users.UserLocationInterface;

@Controller
public class UserLocationController implements UserLocationInterface {
	
	 private static final Logger logger = LoggerFactory.getLogger(UserLocationController.class);
	 private UserLocationDaoOperations sqlOperations = new UserLocationDaoOperations();
	 /**
	  * This Function is used to Register location details of registered user.
	  * URL: /userlocation/register
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.REGISTER_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> registerUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		 logger.info("IN "+ RestUrlConstantsOfUserLocation.REGISTER_USER_LOC + " service");
		 HashMap<String, Object> responseMap;
		 List<HashMap<String, Object>> returnList = null;
		 try {
			returnList =  sqlOperations.addUserLocation(userLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 if(returnList !=null && returnList.size()>0)
		 {
			 responseMap = returnList.get(0);
		 }else{
			 responseMap = new HashMap<String, Object>();
			 responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			 responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving User Location Failed.");
		 }
		return responseMap;
	}
	/**
	  * This Function is used to Edit location details of registered user.
	  * URL: /userlocation/update
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.EDIT_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> updateUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		
		logger.info("IN "+ RestUrlConstantsOfUserLocation.EDIT_USER_LOC + " service");
		 HashMap<String, Object> responseMap;
		 List<HashMap<String, Object>> returnList = null;
		 try {
			returnList =  sqlOperations.updateUserLocation(userLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		 if(returnList !=null && returnList.size()>0)
		 {
			 responseMap = returnList.get(0);
		 }else{
			 responseMap = new HashMap<String, Object>();
			 responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			 responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving User Location Failed.");
		 }
		return responseMap;
	}
	/**
	  * This Function is used to get registered location details of registered user.
	  * URL: /userlocation/fetch
	  */
	@Override
	 @RequestMapping(value = RestUrlConstantsOfUserLocation.GET_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchUserLocationDetails(
			@RequestBody UserLocation userLocationDetails) {
		 logger.info("IN "+ RestUrlConstantsOfUserLocation.GET_USER_LOC + " service");
		 
		 HashMap<String, Object> responseMap;
		 
		 List<HashMap<String, Object>> returnList = null;
		 
		 try {
			returnList = (List<HashMap<String, Object>>) sqlOperations.getUserLocation(userLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		 responseMap = new HashMap<String, Object>();
		 //condition to check objectsize is not null then return userid and location object
		 if(returnList!=null && returnList.size()>0){
			 responseMap.put(CommonResponseConstants.USERID, userLocationDetails.getUserId());
			 responseMap.put(ResponseConstantsOfUserLocation.USERLOCATIONLIST, returnList);
			 responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		 }
		 else{
			 responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			 responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User Location not Found.");
			 
		 }
		 
		return responseMap;
	}
	
	/**
	 *Unregister location registered by user.
	 * URL: /userlocation/register
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUserLocation.UNREGISTER_USER_LOC, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> unregisterUserLocationDetails(
		@RequestBody UserLocation userLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfUserLocation.UNREGISTER_USER_LOC + " service");
		HashMap<String, Object> responseMap;
		List<HashMap<String, Object>> returnList = null;
			try {
				returnList =  sqlOperations.deleteLocationInfo(userLocationDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(returnList !=null && returnList.size()>0)
			{
				responseMap = returnList.get(0);
				}else{
				responseMap = new HashMap<String, Object>();
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Unregistering User Location Failed.");
			}
		return responseMap;
	}
}
