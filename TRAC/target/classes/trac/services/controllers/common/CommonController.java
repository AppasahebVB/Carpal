package trac.services.controllers.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.commonbean.Common;
import trac.constants.resturlconstants.RestUrlConstantsOfCommonController;
import trac.dao.commondao.CommonSqlOperations;
import trac.services.controllers.users.UserController;
import trac.services.interfaces.common.CommonInterface;

@Controller
public class CommonController implements CommonInterface{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private final CommonSqlOperations commonSqlOperations = new CommonSqlOperations(); 

	/**
	 * This method is used to get list of Countries 
	 * @param commonBean
	 * @serviceurl /countries/fetch
	 * @return success and failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfCommonController.GET_COUNTRIES, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody Common getListOfCountries(Common commonBean) {
		logger.info("IN "+ RestUrlConstantsOfCommonController.GET_COUNTRIES + " METHOD");

		return commonSqlOperations.getListOfCountries(commonBean);
	}

}
