package trac.util;


import trac.util.utilinterface.SpringByCryptoHashing;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CustomHashing implements SpringByCryptoHashing{
	
	/*
	 * method to encrypt password
	 */
	 public String encryption(String stringToBeHashed) {
		 String hashedPassword = null;
		 try{
			if(StringUtils.isNotEmpty(stringToBeHashed)) {
		        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		        hashedPassword = passwordEncoder.encode(stringToBeHashed);
			}
		 }catch(IllegalArgumentException ex){
			 return "Invalid salt version";
		 }
	       // userDetails.setPassword(hashedPassword);
	        return hashedPassword;
	 }
	 
	 /*
		 * method to decrypt password
		 */
	 
	 public boolean decryption(String actualString,String hashedString) {
		 try{
	        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	       return passwordEncoder.matches(actualString, hashedString);
	       // users.setPassword(hashedPassword);
	     }catch(IllegalArgumentException ex){
			 return false;
		 }
	 
	 }
}
