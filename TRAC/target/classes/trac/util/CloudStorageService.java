package trac.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import trac.constants.cloudconstants.CloudConstantsInfo;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

public class CloudStorageService {

	 private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	 	/**
		 * This method is Used to upload images to cloud Bucket
		 *
		 */
		 public  String uploadImagesToCloudBucket(MultipartFile gMultiPartData,String currentImageFileName,String oldImageFileName,HttpServletResponse response,String bucketName) {
			
				
				 String imageUrl = null;
			try {
			GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	        .initialRetryDelayMillis(10)
	        .retryMaxAttempts(10)
	        .totalRetryPeriodMillis(15000)
	        .build()); //runtime parameters setting
			
			GcsFilename filename = new GcsFilename(bucketName, currentImageFileName.toString()); //setting cloud info like bucket name and file name 
			
			GcsFileOptions options = new GcsFileOptions.Builder().mimeType("image/png").acl("public-read").build();//setting meta dat of image
			
			if(oldImageFileName != null && oldImageFileName.length() > 0){
				//System.out.println("In IF of oldImageFileName");
				deleteImageFromGCS(oldImageFileName,gcsService);
			}
				
			GcsOutputChannel writeChannel = gcsService.createOrReplace(filename,options);           
			//PrintWriter out = new PrintWriter(Channels.newWriter(writeChannel, "UTF8"));
				writeChannel.waitForOutstandingWrites(); //wait streaming if another streaming in progress
				writeChannel.write(ByteBuffer.wrap(gMultiPartData.getBytes())); //writing image to bucket
				writeChannel.close();//closing the streamer
				
				BlobKey blobKey = blobstoreService.createGsBlobKey("/gs/"+ bucketName  + "/" + currentImageFileName); 
				
				System.out.println("blobkey = "+blobKey.toString());
				
				// Get the image serving URL (in https:// format)
					imageUrl =  ImagesServiceFactory.getImagesService().getServingUrl(
					    ServingUrlOptions.Builder.withBlobKey(blobKey).secureUrl(true));
					System.out.println("imageUrl = "+imageUrl);
			} catch (ClosedByInterruptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return imageUrl;
	
	}	// end of uploadVehicleImageToCloudBucket
		 
		 /**
			 * delete images directly from server
			 *
			 */
		 
		 public  boolean deleteImageFromGCS(String oldImageFileName,GcsService gcsService){
			 GcsFilename filename = new GcsFilename(CloudConstantsInfo.VEHICLE_BUCKETNAME, oldImageFileName.toString()); //setting cloud info like bucket name and file name 
			Boolean isDeleted = false;					
				
			try {
				isDeleted = gcsService.delete(filename);
				System.out.println("isDeleted -  "+ isDeleted);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			return isDeleted;
			 
		 }
		 
		 
}
