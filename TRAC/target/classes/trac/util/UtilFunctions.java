package trac.util;


import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class UtilFunctions {
	// create random object
	private Random random = new Random();
	private static final Random RANDOM = new SecureRandom();
	/** Length of password. @see #generateRandomPassword() */
	public static final int PASSWORD_LENGTH = 6;

	//generting caseId from the userid
	public String getUserIdService(String caseIntialName, int serviceTypeId,
			String userId) {

		StringBuilder buff = new StringBuilder();
		buff.append(caseIntialName);
		buff.append("-");
		buff.append(serviceTypeId);
		buff.append("-");
		buff.append(userId);
		buff.append("-");
		buff.append(UtilFunctions.getDate());
		System.out.println("CaseDetails.....  " + buff.toString());
		return buff.toString();
	}

	//get current time
	public static String getTime() {
		java.util.Date date = new java.util.Date();
		System.out.println(new Timestamp(date.getTime()));
		return new Timestamp(date.getTime()).toString();
	}

	//get current date
	public static String getDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		return dateFormat.format(date).toString();

	}

	//formatting date and compare
	public static boolean formatDateAndCompare(String dateInString1,String dateInString2){
		String[] formatteddate1 = dateInString1.split(" ")[0].split("-");
		Calendar dateCal1 = Calendar.getInstance();
		dateCal1.set(new Integer(formatteddate1[0]), new Integer(formatteddate1[1]), new Integer(formatteddate1[2]));

		String[] formatteddate2 = dateInString2.split(" ")[0].split("-");
		Calendar dateCal2 = Calendar.getInstance();
		dateCal2.set(new Integer(formatteddate2[0]), new Integer(formatteddate2[1]), new Integer(formatteddate2[2]));

		Date date1 = dateCal1.getTime();
		Date date2 = dateCal2.getTime();
		if(date1.after(date2)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 *  Generate a CAPTCHA String consisting of random lowercase & uppercase letters, and numbers.
	 */
	public String generateCaptchaString() {
		//int length = 4 + (Math.abs(random.nextInt()) % 3);
		int length = 4;
		StringBuffer captchaStringBuffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int baseCharNumber = Math.abs(random.nextInt()) % 62;
			int charNumber = 0;
			if (baseCharNumber < 26) {
				charNumber = 65 + baseCharNumber;
			}
			else if (baseCharNumber < 52){
				charNumber = 97 + (baseCharNumber - 26);
			}
			else {
				charNumber = 48 + (baseCharNumber - 52);
			}
			captchaStringBuffer.append((char)charNumber);
		}

		return captchaStringBuffer.toString();
	}

	//get difference milliseconds with isExceeded current time
	public static Object[] getDifferenceMilliseconds(String expectedarrivalTime,String assignedTime){
		long diffmilliseconds =0;
		long expectedarrivalmilliseconds =0;
		boolean isExceed;
		Object[] differenceArray = new Object[2];
		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
			date = formatter.parse(expectedarrivalTime);

			expectedarrivalmilliseconds = date.getTime();
			long currentmilliseconds = (new Date()).getTime();
			if(currentmilliseconds>expectedarrivalmilliseconds){
				diffmilliseconds = expectedarrivalmilliseconds;
				isExceed = true;
			}else{
				diffmilliseconds = (expectedarrivalmilliseconds - currentmilliseconds)/1000;
				isExceed = false;
			}

			differenceArray[0] = diffmilliseconds;
			differenceArray[1] = isExceed;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//long millisecondsFromNow = milliseconds - (new Date()).getTime();
		return differenceArray;
	}

	//get current formated date
	public static String getCurrentDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		return dateFormat.format(date).toString();
	}

	//check is Today
	public static boolean isToday(String dateString){
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
		dayFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Date date;

		try {
			date = dayFormat.parse(dateString);
			Date todayDate = new Date();
			if(dayFormat.format(date).equalsIgnoreCase(dayFormat.format(todayDate)))
			{
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}


	//checek date is before today date
	public static boolean isBeforeToday(String dateString){
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Date date;

		try {
			Date todayDate = new Date();
			date = dateFormat.parse(dateString);

			if(date.getTime()<=todayDate.getTime())
			{
				return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	//get no of days in month
	public static int getNoOfDaysInMonth(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		// Get the number of days in that month
		int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH); 
		return daysInMonth;
	}

	//get no of days in current month
	public static int getNoOfDaysInCurrentMonth(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH,0);
		// Get the number of days in that month
		int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH); 
		return daysInMonth;
	}

	//get no of days left a in month
	public int getNoOfDaysLeftInMonth(Calendar currentDateCal ){
		/*Date date = new Date();
		    Calendar currentDateCal = Calendar.getInstance();
		    currentDateCal.setTime(date);*/
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(new Date());  
		endDateCal.add(Calendar.MONTH, 1);
		endDateCal.set(Calendar.DATE, 1);
		endDateCal.add(Calendar.DATE, -1);
		Date currentDate = currentDateCal.getTime();
		Date endDate = endDateCal.getTime();
		//in milliseconds
		long diff = endDate.getTime() - currentDate.getTime();
		return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;

	}

	//get no of days left a in month of specified date
	public int getNoOfDaysLeftInMonth(String fromdate){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar currentDateCal = Calendar.getInstance();
		currentDateCal = UtilFunctions.getDateOfCalender(dateFormat,fromdate,0,true);
		// currentDateCal.setTime(date);
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime( new Date());  
		endDateCal.add(Calendar.MONTH, 1);
		endDateCal.set(Calendar.DATE, 1);
		endDateCal.add(Calendar.DATE, -1);
		// Date currentDate = currentDateCal.getTime();
		//Date endDate = endDateCal.getTime();
		//in milliseconds
		// long diff = endDate.getTime() - currentDate.getTime();
		//int diffDays = (int) diff / (24 * 60 * 60 * 1000);
		int numDays = currentDateCal.getActualMaximum(Calendar.DATE) + 1;
		return numDays;
		//return diffDays;
	}

	//get start date of last week
	public String startDateOfLastWeek(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DATE, -i - 7);
		Date startDate = c.getTime();
		c.add(Calendar.DATE, 6);
		return dateFormat.format(startDate); 
	}

	//get start date of week
	public String startDateOfWeek(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		Date startDate = c.getTime();
		return dateFormat.format(startDate); 
	}

	//get start date of month
	public String startDateOfAMonth(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();

		// substract one month
		c.add(Calendar.MONTH, -1);

		// get to the lowest day of that month
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		Date startDate = c.getTime();
		return dateFormat.format(startDate);
	}

	//get start date of current month
	public String startDateOfCurrentMonth(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();

		// substract one month
		c.add(Calendar.MONTH, 0);

		// get to the lowest day of that month
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		Date startDate = c.getTime();
		return dateFormat.format(startDate);
	}

	//check is month starting day
	public boolean isMonthStartingDay(){
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		// substract one month
		cal1.add(Calendar.MONTH, 0);

		// get to the lowest day of that month
		cal1.set(Calendar.DAY_OF_MONTH, cal1.getActualMinimum(Calendar.DAY_OF_MONTH));

		return cal1.equals(cal2);
	}

	//check start date of last month
	public String startDateOfLastMonth(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();

		// substract one month
		c.add(Calendar.MONTH, -1);

		// get to the lowest day of that month
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		Date startDate = c.getTime();
		return dateFormat.format(startDate);
	}

    //check start and end date of previous week
	public List<String> startAndEndDateOfPreviousWeek(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		List<String> dates = new ArrayList<String>(); 
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DATE, -i - 7);
		Date startDate = c.getTime();
		c.add(Calendar.DATE, 6);
		Date endDate = c.getTime();
		dates.add(dateFormat.format(startDate));
		dates.add(dateFormat.format(endDate));
		dates.add(dateFormat1.format(startDate));
		dates.add(dateFormat1.format(endDate));
		return dates; 
	}
	
	//check start and end date of current week
	public List<String> startAndEndDateOfCurrentWeek(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");

		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		List<String> dates = new ArrayList<String>(); 
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DATE, -dayOfWeek);
		Date startDate = c.getTime();
		c.add(Calendar.DATE, 6);
		Date endDate = c.getTime();
		dates.add(dateFormat.format(startDate));
		dates.add(dateFormat.format(endDate));
		dates.add(dateFormat1.format(startDate));
		dates.add(dateFormat1.format(endDate));
		return dates; 
	}

	//get start and end date from current day
	public List<String> startAndEndDatesFromCurrentDay(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		List<String> dates = new ArrayList<String>(); 
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		Date startDate = c.getTime();
		c.add(Calendar.DATE, 6);
		Date endDate = c.getTime();
		dates.add(dateFormat.format(startDate));
		dates.add(dateFormat.format(endDate));
		dates.add(dateFormat1.format(startDate));
		dates.add(dateFormat1.format(endDate));
		return dates; 
	}
	
	//get start and end date of month
	public List<String> startAndEndDateOfAMonth(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();
		List<String> dates = new ArrayList<String>(); 
		// substract one month
		c.add(Calendar.MONTH, -1);

		// get to the lowest day of that month
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		Date startDate = c.getTime();

		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime( new Date());  
		endDateCal.add(Calendar.MONTH, 1);
		endDateCal.set(Calendar.DATE, 1);
		endDateCal.add(Calendar.DATE, -1);

		Date endDate = endDateCal.getTime();

		dates.add(dateFormat.format(startDate));
		dates.add(dateFormat.format(endDate));
		dates.add(dateFormat1.format(startDate));
		dates.add(dateFormat1.format(endDate));

		return dates;
	}


	//get instance Of StartDay Of Month
	public Calendar instanceOfStartDayOfMonth(){

		Calendar cal = Calendar.getInstance();

		// get to the lowest day of that month
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));

		return cal;
	}

	//start And End Date Of CurrentMonth
	public String startAndEndDateOfCurrentMonth(){
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Calendar c = Calendar.getInstance();

		// get to the lowest day of that month
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		Date startDate = c.getTime();



		return dateFormat1.format(startDate);
	}

	//check today is Starting Day Of Month
	public static boolean isTodayStartingDayOfAMonth(){

		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		// substract one month
		cal2.add(Calendar.MONTH, -1);

		// get to the lowest day of that month
		cal2.set(Calendar.DAY_OF_MONTH, cal2.getActualMinimum(Calendar.DAY_OF_MONTH));

		// lowest possible time in that day
		cal2.set(Calendar.HOUR_OF_DAY, cal2.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal2.set(Calendar.MINUTE, cal2.getActualMinimum(Calendar.MINUTE));
		cal2.set(Calendar.SECOND, cal2.getActualMinimum(Calendar.SECOND));


		if (cal1.equals(cal2))
			return true;
		else
			return false;
	}


	//get Next and  before SevenDays of the date
	public Map<String,Object> getNextPrevSevenDays(String startDate,String endDate, int whatDatesToFetch) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Map<String,Object> datesMap = new HashMap<String,Object>();
		List<String> datesList = new ArrayList<String>();
		List<String> startAndEndDateList = new ArrayList<String>();
		List<Integer> daysList = new ArrayList<Integer>();
		String currentDate = null;
		String futureDates = null;
		String formattedStartDate = null;
		String formattedEndDate = null;
		int prev = -7;
		int next = 1;
		Calendar cal = Calendar.getInstance();

		if(whatDatesToFetch == 1){//to get previous week dates
			cal = UtilFunctions.getDateOfCalender(dateFormat,startDate,prev,true);
			currentDate = dateFormat.format(cal.getTime());
			formattedStartDate = dateFormat1.format(cal.getTime());
		}else if(whatDatesToFetch == 2){//to get next week dates
			cal = UtilFunctions.getDateOfCalender(dateFormat,endDate,1,true);
			currentDate = dateFormat.format(cal.getTime()); 
			formattedStartDate = dateFormat1.format(cal.getTime());
		}else if(whatDatesToFetch == 3){//to get current week dates
			cal = UtilFunctions.getDateOfCalender(dateFormat,startDate,1,true);
			currentDate = dateFormat.format(cal.getTime()); 
			formattedStartDate = dateFormat1.format(cal.getTime());
		}else if(whatDatesToFetch == 5){//to get last week dates
			cal = UtilFunctions.getDateOfCalender(dateFormat,startDate,1,true);
			currentDate = dateFormat.format(cal.getTime()); 
			formattedStartDate = dateFormat1.format(cal.getTime());
		}else{
			currentDate =dateFormat.format(cal.getTime());
			formattedStartDate = dateFormat1.format(cal.getTime());
		}


		datesList.add(currentDate); //adding to list
		daysList.add(getDay(cal));//adding to list

		prev = prev + 1; //incrementing to get previous dates
		next = next + 1;//incrementing to get next dates
		Calendar cal2 = null;

		for(int i =1; i< 7; i++,prev++,next++){
			Calendar cal1 = null;
			int k;
			if(whatDatesToFetch == 1){
				k = prev;
				cal1 = UtilFunctions.getDateOfCalender(dateFormat,startDate,k,false);
			}else if(whatDatesToFetch == 2){
				k = next;
				cal1 = UtilFunctions.getDateOfCalender(dateFormat,endDate,k,false);
			}else if(whatDatesToFetch == 3){
				k = next;
				cal1 = UtilFunctions.getDateOfCalender(dateFormat,startDate,k,false);
			}else{
				k = i;
				cal1 = Calendar.getInstance();
			}

			cal1.add(Calendar.DATE, k);
			futureDates = dateFormat.format(cal1.getTime());
			datesList.add(futureDates);
			daysList.add(getDay(cal1));
			futureDates = null;
			cal2 = cal1;
		}
		// cal2.add(Calendar.DATE, 1);
		formattedEndDate= dateFormat1.format(cal2.getTime());

		startAndEndDateList.add(formattedStartDate);
		startAndEndDateList.add(formattedEndDate);

		datesMap.put("datesList", datesList);
		datesMap.put("formatteddatesList", datesList);
		datesMap.put("daysList", daysList);
		datesMap.put("startAndEndDateList", startAndEndDateList);
		return datesMap;
	}

	//get Previous month info
	public Map<String,Object> getPreviousMonthInfo(String startDate) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Map<String,Object> datesMap = new HashMap<String,Object>();
		List<String> datesList = new ArrayList<String>();
		List<Integer> daysList = new ArrayList<Integer>();
		List<String> startAndEndDateList = new ArrayList<String>();
		String futureDates = null;
		String formattedStartDate = null;
		String formattedEndDate = null;
		int noOfDays = UtilFunctions.getNoOfDaysInMonth();
		Calendar cal1 = null;
		for(int i =0; i< noOfDays; i++){


			cal1 = UtilFunctions.getDateOfCalender(dateFormat,startDate,i,false);
			if(i == 0){
				formattedStartDate = dateFormat1.format(cal1.getTime());
			}

			/*if( i == (noOfDays - 1)){
	    			 formattedEndDate= dateFormat1.format(cal1.getTime());
	    		}
			 */
			cal1.add(Calendar.DATE, i);
			futureDates = dateFormat.format(cal1.getTime());
			datesList.add(futureDates);
			daysList.add(getDay(cal1));
			futureDates = null;
		}

		//cal1.add(Calendar.DATE, 1);
		formattedEndDate= dateFormat1.format(cal1.getTime());

		startAndEndDateList.add(formattedStartDate);
		startAndEndDateList.add(formattedEndDate);
		datesMap.put("startAndEndDateList", startAndEndDateList);
		datesMap.put("datesList", datesList);
		datesMap.put("daysList", daysList);
		return datesMap;
	}


	public Map<String,Object> getCurrentMonthInfo(String startDate) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd EEEE");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		Map<String,Object> datesMap = new HashMap<String,Object>();
		List<String> datesList = new ArrayList<String>();
		List<Integer> daysList = new ArrayList<Integer>();
		List<String> startAndEndDateList = new ArrayList<String>();
		String futureDates = null;
		String formattedStartDate = null;
		String formattedEndDate = null;
		Calendar cal2 = null;
		int noOfDays = UtilFunctions.getNoOfDaysInCurrentMonth();

		for(int i =0; i< noOfDays; i++){
			Calendar cal1 = null;

			cal1 = UtilFunctions.getDateOfCalender(dateFormat,startDate,i,false);
			if(i == 0){
				formattedStartDate = dateFormat1.format(cal1.getTime());
			}


			cal1.add(Calendar.DATE, i);
			futureDates = dateFormat.format(cal1.getTime());
			datesList.add(futureDates);
			daysList.add(getDay(cal1));
			futureDates = null;

			cal2 = cal1;

		}
		//cal2.add(Calendar.DATE, 1);
		formattedEndDate= dateFormat1.format(cal2.getTime());


		startAndEndDateList.add(formattedStartDate);
		startAndEndDateList.add(formattedEndDate);
		datesMap.put("startAndEndDateList", startAndEndDateList);
		datesMap.put("datesList", datesList);
		datesMap.put("daysList", daysList);
		return datesMap;
	}

	public static Calendar getDateOfCalender(DateFormat dateFormat,String startDate,int day,boolean isNextPREV){
		Calendar cal1 = Calendar.getInstance();
		Date dateObj;
		try {
			dateObj = dateFormat.parse(startDate);
			cal1.setTime(dateObj);

			if(isNextPREV)
				cal1.add(cal1.DATE, day);

			return cal1;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
	}

	public int getDay(Calendar cal){
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			return 1;
		}else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
			return 2;
		}else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
			return 3;
		}else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
			return 4;
		}else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
			return 5;
		}else if(cal.get(Calendar.DAY_OF_WEEK)== Calendar.FRIDAY){
			return 6;
		}else{
			return 7;
		}
	}
	public static String[] dateToHoursMin(String dateString){
		String hourMin[] = new String[2];
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		try {
			Calendar cal1 = Calendar.getInstance();
			Date dateToCompare = sdf.parse(dateString.substring(0,dateString.indexOf(".")>0?dateString.lastIndexOf("."):dateString.length()));
			cal1.setTime(dateToCompare);

			hourMin[0] = cal1.get(Calendar.HOUR) <9 ? "0"+cal1.get(Calendar.HOUR) :""+cal1.get(Calendar.HOUR);
			hourMin[1] = cal1.get(Calendar.MINUTE) <9 ? "0"+cal1.get(Calendar.MINUTE) :""+cal1.get(Calendar.MINUTE);

			System.out.print("Hours:"+hourMin[0]);
			System.out.print("Minutes:"+hourMin[1]);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return hourMin;

	}

	public static String[] convertToHoursMin(String dateString){
		String hourMin[] = new String[2];
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		hourMin[0]  = "";
		hourMin[1]  = "";
		try {
			Calendar cal1 = Calendar.getInstance();
			Date dateToCompare = sdf.parse(dateString);
			cal1.setTime(dateToCompare);;

			hourMin[0] = dateFormat.format(dateToCompare);
			String hours = cal1.get(Calendar.HOUR) <9 ? "0"+cal1.get(Calendar.HOUR) :""+cal1.get(Calendar.HOUR);
			String min  = cal1.get(Calendar.MINUTE) <9 ? "0"+cal1.get(Calendar.MINUTE) :""+cal1.get(Calendar.MINUTE);

			hourMin[1] = hours +":"+min;

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return hourMin;

	}


	/**
	 * Generate a random String suitable for use as a temporary password.
	 *
	 * @return String suitable for use as a temporary password
	 * @since 2.4
	 */
	public static String generateRandomPassword()
	{
		// Pick from some letters that won't be easily mistaken for each
		// other. So, for example, omit o O and 0, 1 l and L.
		//String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
		String letters = "1234567890";

		String pw = "";
		for (int i=0; i<PASSWORD_LENGTH; i++)
		{
			int index = (int)(RANDOM.nextDouble()*letters.length());
			pw += letters.substring(index, index+1);
		}
		return pw;
	}



	public static long getDifferenceMilliseconds(String statusTime){
		long countDownMilliSeconds =0;
		long statuUpdatedMilliSeconds =0;

		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
			date = formatter.parse(statusTime);

			statuUpdatedMilliSeconds = date.getTime();
			long currentmilliseconds = (new Date()).getTime();
			if(currentmilliseconds<statuUpdatedMilliSeconds){
				countDownMilliSeconds = (statuUpdatedMilliSeconds - currentmilliseconds)/1000;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return countDownMilliSeconds;
	}


	public static long getElapsedMilliSeconds(String statusTime){

		long statuUpdatedMilliSeconds =0;

		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.ENGLISH);
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
			date = formatter.parse(statusTime);

			statuUpdatedMilliSeconds = date.getTime();


		} catch (ParseException e) {
			e.printStackTrace();
		}
		return statuUpdatedMilliSeconds;
	}



	public static String convertScriptDate(String dateString){
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		String formatted = null;
		try {
			Calendar cal1 = Calendar.getInstance();
			Date dateToCompare = sdf.parse(dateString);
			cal1.setTime(dateToCompare);
			formatted = dateFormat.format(cal1.getTime());
			System.out.println(formatted);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatted;
	}

	public static long getCurrentDateByTimeZone(){
		//Date currentDate = new Date();
		TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Dubai");
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeZone(timeZone1);
		long currentmilliseconds = calendar.getTimeInMillis();
		return currentmilliseconds;
	}

	public static long getDifferenceArrivalMilliseconds(String expectedarrivalTime){

		long expectedarrivalmilliseconds =0;

		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));

			date = formatter.parse(expectedarrivalTime);
			expectedarrivalmilliseconds =  date.getTime();

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return expectedarrivalmilliseconds;
	}


	public static Calendar getCalenderObject(String dateToConvert){
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Calendar cal  = Calendar.getInstance();
		try {
			cal.setTime(df.parse(dateToConvert));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cal;
	}

}
