package trac.constants.resturlconstants;


/**
 * This Class is Used to Place the all RestUrlConstantsOf UserLocationBean
 * @author gatla
 *
 */
public class RestUrlConstantsOfUserLocation {

	public static final String REGISTER_USER_LOC = "/userlocation/register";
	public static final String EDIT_USER_LOC = "/userlocation/update";
	public static final String GET_USER_LOC = "/userlocation/fetch";
	public static final String UNREGISTER_USER_LOC = "/userlocation/unregister";

}
