package trac.constants.resturlconstants;


/**
 * This Class is used to place the All RestUrlConstantsOf UserBean
 * @author gatla
 *
 */
public class RestUrlConstantsOfUser {
	
   public static final String CREATE_USER = "/tracuser/register";
   public static final String VALIDATE_USER = "/user/validate";
   public static final String UNIQUE_USER = "/user/unique";
   public static final String EDIT_USER = "/userprofile/update";
   public static final String USERS_LIST = "/user/list";
   public static final String USERS_ACTIVATION = "/user/activation";
   public static final String USERS_REMOVE = "/user/remove";

  
   public static final String SECURITY_CHECK1 = "/user/checkemail";
   public static final String SECURITY_CHECK2 = "/user/checksecurity";
   public static final String SECURITY_QUESTIONS = "/user/getsecurityinfo";
   public static final String GET_LIST_OF_ROLES = "/user/getroles";
   public static final String GET_ROLES_INFO = "/user/getroleinfo";
   
   public static final String GET_CAPTCHA = "/user/securitycheck";
   public static final String CHANGE_PASSWORD = "/userprofile/changepassword";
   public static final String UPDATE_PASSWORD = "/password/update";
   public static final String RESET_PASSWORD = "/user/resetpassword";
   
   public static final String CREATE_ROLE = "/user/role";
   public static final String UPDATE_ROLE = "/user/updaterole";
   public static final String GET_PERMISSIONS = "/user/permissions";
   public static final String GET_LIST_OF_MODULES = "/user/getmodules";
   
/*   public static final String SAVE_USER_CONTACT_DETAILS = "/contactDetailsOfCustomer/saveCustomerContactDetails";
   public static final String EDIT_USER_CONTACT_DETAILS = "/contactDetailsOfCustomer/editCustomerContactDetails";*/
   
   public static final String GET_LIST_OF_MANAGERS = "/user/getmanagers";
   public static final String GET_USER_MODULES = "/user/modules";
   public static final String USER_ACCESS = "/user/access";
   public static final String UPDATE_ROLE_STATUS = "/user/updateRoleStatus";


}
