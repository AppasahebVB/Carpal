package trac.constants.sqlqueryconstants;

/**
 *  Shift Sql Query Constants are placed in this class
 *
 */

public class ShiftSqlQueryConstants {
	public static final String LIST_OF_SHIFTS = "Select shift.ID,shift.ShiftID,shift.shiftDay as  shiftDayId,shiftdays.Description as shiftDay,shift.StartTime,shift.EndTime,shift.VehiclesNeeded,shift.DriversNeeded from shifts shift "+ 
												"join lkp_sys_shift_days shiftdays on shiftdays.ID = shift.shiftDay "+ 
												"join lkp_sys_workshifts workshifts on workshifts.ID= shift.ShiftID order by shift.shiftDay,workshifts.id";
	public static final String SELECT_SHIFTS_BY_DAY = "SELECT shift.Id,workshifts.id as shiftId,workshifts.description as shiftName,shift.shiftId,shift.startTime,shift.endTime,shift.vehiclesNeeded,shift.driversNeeded,2 as shiftRWD FROM "+ 
												"shifts shift right join lkp_sys_workshifts workshifts on "+ 
												"shift.shiftId = workshifts.id where shift.shiftDay = ? and shift.isActive = 1";
	
	public static final String SELECT_SHIFTS = "SELECT * FROM lkp_sys_workshifts"; 
			
}
