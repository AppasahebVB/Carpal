package trac.constants.sqlqueryconstants;

/**
 *  User Sql Query Constants are placed in this class
 *
 */

public class UserSqlQueryConstants {
	
	 public static final String MOBILE_SELECT_USER = "SELECT user.*,userroles.active,userroles.roleID FROM users user JOIN usersroles userroles on user.userID = userroles.userID " +
	 						"WHERE email=? or phone = ? and passwordRetryCount <= 3 and userroles.roleID = 6";
	 public static final String WEB_SELECT_USER = "SELECT user.*,userroles.active,userroles.roleID FROM users user JOIN usersroles userroles on user.userID = userroles.userID " +
				"WHERE email=? or phone = ? and passwordRetryCount <= 3  and userroles.roleID <> 6";
	 public static final String SELECT_USER_BY_ID = "SELECT * FROM users WHERE userID=? and passwordRetryCount < 3";
	 public static final String GET_SEC_QUESTIONS =  "SELECT SecurityQuestionID,Description from lkp_securityquestions_loc where  LocaleID IS NOT NULL and LocaleID = 1 and Flags != 32";
	 public static final String GET_COUNTRY_ISD_LIST =  "SELECT isdCode.ID,isdCode.CountryIsoCode,isdCode.CountryIsdCode,countryName.Name from lkp_countries_isd_code_loc as isdCode join lkp_countries_loc as countryName where countryName.ISOCode = isdCode.CountryIsoCode and LocaleID = 1";
	 public static final String GET_SEC_ANSWER =  "SELECT * FROM users WHERE email=? or phone=?";
	 
	 public static final String UPDATE_LOGIN_ATTEMPTS =  "UPDATE users SET passwordRetryCount = passwordRetryCount + 1 WHERE userID=? and passwordRetryCount < 3";
	 
	 public static final String UNIQUE_USERNAME =  "SELECT count(*) FROM users WHERE deviceTypeID = ?";
	 public static final String UNIQUE_EMAIL =  "SELECT count(*) FROM users WHERE email=?  and passwordRetryCount < 3";
	 public static final String UPDATE_SEC_CHECK =  "UPDATE users SET  isSecurityPassed = ? WHERE email = ? or phone = ?";
	 
	 public static final String UPDATE_PROFILE_PIC =  "UPDATE users SET profilePicData = ?,userFileNameOnCloud = ? WHERE userID = ?";
	 
	 public static final String SELECT_USER_COUNT = "SELECT count(*) FROM users WHERE userID=?";
	 public static final String GET_ROLES = "select * from lkp_secroles_loc where RoleID != 0 and active = 1";
	 
	 public static final String GET_USERS_LIST = "SELECT u.phone,u.email,u.userID, u.isActive,u.isActive, IF (u.firstName IS NULL , IFNULL(u.displayName,"+"''"+"),  CONCAT(IFNULL(u.firstName,"+"''"+"),"+"' '"+ ",IFNULL(u.lastName,"+"''"+"))) AS username,ur.roleID,"+
	 "lroles.DisplayName as roleName from users u LEFT JOIN usersroles ur ON  u.userID = ur.userID  LEFT JOIN lkp_secroles_loc lroles ON ur.roleID = lroles.RoleID WHERE ur.roleID <> 6 and u.isDisabled = 1";

	 
	 //public static final String SELECT_SHIFTS = "Select * from shifts shift join lkp_sys_shift_days shiftdays on shiftdays.ID = shift.shiftDay group by shift.ShiftName";
	 public static final String SELECT_SHIFTS = "SELECT * FROM shifts";
	/* public static final String GET_MANAGERS = "Select * from usersroles INNER JOIN users on usersroles.userID = users.userID  where roleID = 7";*/
	 
	 public static final String GET_MANAGERS = "SELECT users.userID,users.displayName,users.firstName,users.lastName from usersroles INNER JOIN users on usersroles.userID = users.userID where roleID = 7";
	 
	 public static final String GET_MODULES = "SELECT * from lkp_modules";
	 

}
