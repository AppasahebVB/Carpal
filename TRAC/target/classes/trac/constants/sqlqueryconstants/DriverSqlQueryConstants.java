package trac.constants.sqlqueryconstants;

/**
 *  Driver Sql Query Constants are placed in this class
 *
 */


public class DriverSqlQueryConstants {
	 public static final String GET_FULL_DRIVER_INFO_QUERY =  "SELECT d.driverID as driverId,d.driverFirstName,d.driverLastName,d.licenseNo,d.licenseNoExpiredAt,"+
"d.passportNo,d.passportExpiredAt,d.passportIssuedAt,d.governmentIdNumber,d.nationality,"+
"d.visaNo,d.visaIssuedAt,d.visaExpiredAt,d.insuranceCardID as healthInsuranceCardNo,d.insuranceExpiredAt as healthInsuranceCardExpiryAt,"+
"dusers.phone,dusers.email,ddetails.area,ddetails.street,ddetails.building,ddetails.landmark,ddetails.city,ddetails.poBox as postBox,ddetails.countryISO,lcountries.Name as country,"+
"ddetails.eContactName,ddetails.ePhone,ddetails.eEmail,ddetails.eArea,ddetails.eStreet,ddetails.eBuilding,ddetails.eLandmark,ddetails.eCity,ddetails.ePOBox as ePostBox,ddetails.eCountryISO,elcountries.Name as eCountry,"+
"dexp.exprerienceID,dexp.totalExperience,dexp.relatedExpirence,dexp.companyName,dexp.designation,dexp.doj,dexp.dor,"+
"d.dateOfJoin as presentDateOfJoining,emp.employeeId,d.designation as presentdesignation,"+
"s.description as preferedShiftTimings,dworkdetails.workingShiftID as preferedWorkTimings,dworkdetails.preferredDayOffID as preferedDayOff,CONCAT(reporting.firstName,'',reporting.lastName) AS reportingManager ,reporting.userID as reportingTo, user.userName,dworkdetails.reportingTo as reportingUserId,dworkdetails.shirtSize,dworkdetails.shoeSize,dworkdetails.trouserSize,"+
"dworkdetails.shoesReceived as noOfShoeReceived,dworkdetails.shirtsReceived as noOfShirtsReceived,dworkdetails.trousersReceived as noOfTrousersReceived,t.TruckCode as preferedTruck,dworkdetails.preferedVehicleID as preferedTruckId,dworkdetails.truckDriverId"+
" FROM drivers d"+
" JOIN driverdetails ddetails"+ 
" ON d.driverID = ddetails.driverID"+ 
" JOIN driverexperiences dexp"+
" ON dexp.driverID = d.driverID"+
" JOIN users dusers"+
" ON dusers.userID = d.userID"+
" JOIN driverworkdetails dworkdetails"+
" ON dworkdetails.driverID= d.driverID"+
" JOIN employee emp"+
" ON d.employeeId= emp.ID"+
" JOIN lkp_countries_loc lcountries"+
" ON ddetails.countryISO = lcountries.ISOCode"+
" INNER JOIN lkp_countries_loc elcountries"+
" ON ddetails.eCountryISO = elcountries.ISOCode"+ 
" INNER JOIN lkp_sys_workshifts s"+
" ON s.id  = dworkdetails.workingShiftID"+
" INNER JOIN users reporting"+
" ON reporting.userID = dworkdetails.reportingTo"+
" JOIN users user"+
" ON user.userID = d.userID"+
" INNER JOIN trucks t"+
" ON t.TruckID = dworkdetails.preferedVehicleID"+
" WHERE  lcountries.LocaleID = 1 AND elcountries.LocaleID = 1 AND d.driverID = ? group by d.driverID ";
}
