package trac.constants.responseconstants;

/**
 *  Constants for customer response 
 *
 */

public class ResponseConstantsOfCustomer {

	   public static final String CUSTOMERID ="customerId";
	   public static final String CUSTOMERMOBILE ="mobileNumber";
	   public static final String CUSTOMEREMAILID ="emailId";
	   public static final String CUSTOMERLOCATIONNAME = "locationName";
	   public static final String CUSTOMERLOCATIONLONGITUDE = "locationLongitude";
	   public static final String CUSTOMERLOCATIONLATITUDE = "locationLatitude";
	   
	   public static final String CUSTOMERLOCATIONLIST = "customerLocationList";
	   public static final String DESTLOCATIONLIST = "destinationLocationList";
	   
	   public static final String CUSTOMER_CASEID ="caseId";
	   public static final String CUSTOMER_CASE_NUMBER ="caseNumber";
	   public static final String CUSTOMER_SUBCASEID ="subcaseId";
	   public static final String CUSTOMER_SUBCASE_NUMBER ="subcaseNumber";
	   public static final String CASEREGISTEREDFAULTID ="caseRegisteredFaultID";
	   public static final String CASEREGISTEREDSERVICEID ="caseRegisteredServiceID";
	   public static final String FAULTDESCRIPTION ="faultDescription";
	   public static final String FAULTID ="faultId";
	   public static final String SERVICEDESCRIPTION ="serviceDescription";
	   public static final String CASECOMMENTS ="caseComments";
	   public static final String CREATEDBY ="createdBy";
	   public static final String CREATEDAT ="createdAt";
	   
	   public static final String ISSERVICELOCATION ="isServiceLocation";
	   public static final String ISDESTINATIONLOCATION ="isDestinationLocation";
	   
	   
	   
	   
}
