package trac.beans.commonbean;

import java.util.List;
import java.util.Map;

public class Common {
	
	private List<Map<String, Object>> tableResultList;
	private String serviceMessage;
	private boolean isSuccess = false;
	
	/**
	 * @return the tableResultList
	 */
	public List<Map<String, Object>> getTableResultList() {
		return tableResultList;
	}
	/**
	 * @param tableResultList the tableResultList to set
	 */
	public void setTableResultList(List<Map<String, Object>> tableResultList) {
		this.tableResultList = tableResultList;
	}
	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	

}
