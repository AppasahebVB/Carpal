package trac.beans.driverbean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;


/** Stores the Driver related information
 * 
 */

public class Driver implements Serializable{

	private static final long serialVersionUID = -2353849426512354666L;

	//Personal Details
	private String userId;
	private String driverFirstName;
	private String driverLastName;
	private String driverName;
	
    private String licenseNo;
    private String licenseNoExpiredAt;
    /*private String internationalLicenseNo;
    private String internationalLicenseExpiredAt;*/
    private String nationality;
    private String governmentIdNumber;
    private String passportNo;
    private String passportIssuedAt;
    private String passportExpiredAt;
    private String visaNo;
    private String visaIssuedAt;
    private String visaExpiredAt;
    private String healthInsuranceCardNo;
    private String healthInsuranceCardExpiryAt;
    
    //contact Details/EmergencyContactDetails
    private String eContactName;
    private String phone;
    private String email;
    private String area;
    private String street;
    private String building;
    private String landmark;
    private String city;
    private String postBox;
    private String country;
    
    //workExperience
    private int experienceId;
    private String totalWorkExperience;
    private String relatedWorkExperience;
    private String companyName;
    private String previousdesignation;
    private String dateOfJoining;
    private String formattedDateOfJoining;
    private String dateOfRelieving;
    private String isNew;
    private String isDeleted;
    
    //presentWorkDetails
    private String employeeId;
    private String userName;
    private String userType;
    private int driverId;
    private String presentdesignation;
    private String preferedWorkTimings;
    private String preferedDayOff;
    private String reportingTo;
    private String shirtSize;
    private String trouserSize;
    private int shoeSize;
    private int noOfShirtsReceived;
    private int noOfTrousersReceived;
    private int noOfShoeReceived;
    private int preferedTruckId;
    
    private int truckId;
    private int shiftId;
    private int punchId;
    private String shiftTimings;
    private String punchInTime;
    private String punchOutTime;
    
    private int shiftScheduleId;
    private String scheduledOn;
    private int scheduleTypeID;
    private int scheduleStatusID;
    private String scheduleEndOn;
    
    private String notes;
    private Boolean isAvailable;
    private Timestamp createdAt;
    private int createdBy;
    private Timestamp updatedAt;
    private String updatedBy;
    
    private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
	
	private List<Map<String, Object>> driverInfo;
    
    
    
    public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getShiftScheduleId() {
		return shiftScheduleId;
	}
	public void setShiftScheduleId(int shiftScheduleId) {
		this.shiftScheduleId = shiftScheduleId;
	}
	
	
	public String getDriverFirstName() {
		return driverFirstName;
	}
	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}
	public String getDriverLastName() {
		return driverLastName;
	}
	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}
	
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getLicenseNoExpiredAt() {
		return licenseNoExpiredAt;
	}
	public void setLicenseNoExpiredAt(String licenseNoExpiredAt) {
		this.licenseNoExpiredAt = licenseNoExpiredAt;
	}
/*	public String getInternationalLicenseNo() {
		return internationalLicenseNo;
	}
	public void setInternationalLicenseNo(String internationalLicenseNo) {
		this.internationalLicenseNo = internationalLicenseNo;
	}
	public String getInternationalLicenseExpiredAt() {
		return internationalLicenseExpiredAt;
	}
	public void setInternationalLicenseExpiredAt(
			String internationalLicenseExpiredAt) {
		this.internationalLicenseExpiredAt = internationalLicenseExpiredAt;
	}*/
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getPassportIssuedAt() {
		return passportIssuedAt;
	}
	public void setPassportIssuedAt(String passportIssuedAt) {
		this.passportIssuedAt = passportIssuedAt;
	}
	public String getPassportExpiredAt() {
		return passportExpiredAt;
	}
	public void setPassportExpiredAt(String passportExpiredAt) {
		this.passportExpiredAt = passportExpiredAt;
	}
	public String getVisaNo() {
		return visaNo;
	}
	public void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}
	public String getVisaIssuedAt() {
		return visaIssuedAt;
	}
	public void setVisaIssuedAt(String visaIssuedAt) {
		this.visaIssuedAt = visaIssuedAt;
	}
	public String getVisaExpiredAt() {
		return visaExpiredAt;
	}
	public void setVisaExpiredAt(String visaExpiredAt) {
		this.visaExpiredAt = visaExpiredAt;
	}
	public String getHealthInsuranceCardNo() {
		return healthInsuranceCardNo;
	}
	public void setHealthInsuranceCardNo(String healthInsuranceCardNo) {
		this.healthInsuranceCardNo = healthInsuranceCardNo;
	}
	public String getHealthInsuranceCardExpiryAt() {
		return healthInsuranceCardExpiryAt;
	}
	public void setHealthInsuranceCardExpiryAt(String healthInsuranceCardExpiryAt) {
		this.healthInsuranceCardExpiryAt = healthInsuranceCardExpiryAt;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostBox() {
		return postBox;
	}
	public void setPostBox(String postBox) {
		this.postBox = postBox;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getTotalWorkExperience() {
		return totalWorkExperience;
	}
	public void setTotalWorkExperience(String totalWorkExperience) {
		this.totalWorkExperience = totalWorkExperience;
	}
	public String getRelatedWorkExperience() {
		return relatedWorkExperience;
	}
	public void setRelatedWorkExperience(String relatedWorkExperience) {
		this.relatedWorkExperience = relatedWorkExperience;
	}
	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return the governmentIdNumber
	 */
	public String getGovernmentIdNumber() {
		return governmentIdNumber;
	}
	/**
	 * @param governmentIdNumber the governmentIdNumber to set
	 */
	public void setGovernmentIdNumber(String governmentIdNumber) {
		this.governmentIdNumber = governmentIdNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPreviousdesignation() {
		return previousdesignation;
	}
	public void setPreviousdesignation(String previousdesignation) {
		this.previousdesignation = previousdesignation;
	}
	public String getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	/**
	 * @return the formattedDateOfJoining
	 */
	public String getFormattedDateOfJoining() {
		return formattedDateOfJoining;
	}
	/**
	 * @param formattedDateOfJoining the formattedDateOfJoining to set
	 */
	public void setFormattedDateOfJoining(String formattedDateOfJoining) {
		this.formattedDateOfJoining = formattedDateOfJoining;
	}
	public String getDateOfRelieving() {
		return dateOfRelieving;
	}
	public void setDateOfRelieving(String dateOfRelieving) {
		this.dateOfRelieving = dateOfRelieving;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public String getPresentdesignation() {
		return presentdesignation;
	}
	public void setPresentdesignation(String presentdesignation) {
		this.presentdesignation = presentdesignation;
	}
	public String getPreferedWorkTimings() {
		return preferedWorkTimings;
	}
	public void setPreferedWorkTimings(String preferedWorkTimings) {
		this.preferedWorkTimings = preferedWorkTimings;
	}
	public String getPreferedDayOff() {
		return preferedDayOff;
	}
	public void setPreferedDayOff(String preferedDayOff) {
		this.preferedDayOff = preferedDayOff;
	}
	public String getReportingTo() {
		return reportingTo;
	}
	public void setReportingTo(String reportingTo) {
		this.reportingTo = reportingTo;
	}
	public String getShirtSize() {
		return shirtSize;
	}
	public void setShirtSize(String shirtSize) {
		this.shirtSize = shirtSize;
	}
	public String getTrouserSize() {
		return trouserSize;
	}
	public void setTrouserSize(String trouserSize) {
		this.trouserSize = trouserSize;
	}
	public int getShoeSize() {
		return shoeSize;
	}
	public void setShoeSize(int shoeSize) {
		this.shoeSize = shoeSize;
	}
	public int getNoOfShirtsReceived() {
		return noOfShirtsReceived;
	}
	public void setNoOfShirtsReceived(int noOfShirtsReceived) {
		this.noOfShirtsReceived = noOfShirtsReceived;
	}
	public int getNoOfTrousersReceived() {
		return noOfTrousersReceived;
	}
	public void setNoOfTrousersReceived(int noOfTrousersReceived) {
		this.noOfTrousersReceived = noOfTrousersReceived;
	}
	public int getNoOfShoeReceived() {
		return noOfShoeReceived;
	}
	public void setNoOfShoeReceived(int noOfShoeReceived) {
		this.noOfShoeReceived = noOfShoeReceived;
	}
	
	public String getShiftTimings() {
		return shiftTimings;
	}
	public void setShiftTimings(String shiftTimings) {
		this.shiftTimings = shiftTimings;
	}
	public String getPunchInTime() {
		return punchInTime;
	}
	public void setPunchInTime(String punchInTime) {
		this.punchInTime = punchInTime;
	}
	public String getPunchOutTime() {
		return punchOutTime;
	}
	public void setPunchOutTime(String punchOutTime) {
		this.punchOutTime = punchOutTime;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Boolean getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getServiceMessage() {
		return serviceMessage;
	}
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	public int getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String geteContactName() {
		return eContactName;
	}
	public void seteContactName(String eContactName) {
		this.eContactName = eContactName;
	}
	public int getPreferedTruckId() {
		return preferedTruckId;
	}
	public void setPreferedTruckId(int preferedTruckId) {
		this.preferedTruckId = preferedTruckId;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public int getTruckId() {
		return truckId;
	}
	public void setTruckId(int truckId) {
		this.truckId = truckId;
	}
	public String getScheduledOn() {
		return scheduledOn;
	}
	public void setScheduledOn(String scheduledOn) {
		this.scheduledOn = scheduledOn;
	}
	public int getScheduleTypeID() {
		return scheduleTypeID;
	}
	public void setScheduleTypeID(int scheduleTypeID) {
		this.scheduleTypeID = scheduleTypeID;
	}
	public int getScheduleStatusID() {
		return scheduleStatusID;
	}
	public void setScheduleStatusID(int scheduleStatusID) {
		this.scheduleStatusID = scheduleStatusID;
	}
	public int getPunchId() {
		return punchId;
	}
	public void setPunchId(int punchId) {
		this.punchId = punchId;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getScheduleEndOn() {
		return scheduleEndOn;
	}
	public void setScheduleEndOn(String scheduleEndOn) {
		this.scheduleEndOn = scheduleEndOn;
	}
	/**
	 * @return the experienceId
	 */
	public int getExperienceId() {
		return experienceId;
	}
	/**
	 * @param experienceId the experienceId to set
	 */
	public void setExperienceId(int experienceId) {
		this.experienceId = experienceId;
	}
	/**
	 * @return the isNew
	 */
	public String getIsNew() {
		return isNew;
	}
	/**
	 * @param isNew the isNew to set
	 */
	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}
	/**
	 * @return the driverInfo
	 */
	public List<Map<String, Object>> getDriverInfo() {
		return driverInfo;
	}
	/**
	 * @param driverInfo the driverInfo to set
	 */
	public void setDriverInfo(List<Map<String, Object>> driverInfo) {
		this.driverInfo = driverInfo;
	}
	/**
	 * @return the isDeleted
	 */
	public String getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	
}
