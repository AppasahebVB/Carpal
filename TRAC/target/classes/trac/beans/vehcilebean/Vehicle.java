package trac.beans.vehcilebean;

import java.io.Serializable;

/** Stores the Vehicle related information
 * 
 */
public class Vehicle implements Serializable{
	
	
	private static final long serialVersionUID = -2107587111242700731L;
	
	private int userId;
	private int vehicleId;
	private String date;
	private String isFavorite;
	private String vin;
	private Long vehicleBrandId;
	
	private Long vehicleModelId;
	private String vehicleName;
	private String vehicleModelYear;
	private String vehicleRegisterNo;
	private String vehicleBrandName;
	private String vehicleModelName;
	private String email;
	private String vehiclePic;
	private Object vehicleList;
	private Object vehicleBrandsList;
	private Object vehicleModelsList;
	private String customerId;
	private Boolean isCustomer;
	private Object isSuccess;
	private String serviceMessage;
	private String isImageChanged;
	private String vehicleFileNameOnCloud;
	
	private int roleId;
	private String memberShipStart;
	private String memberShipEnd;
	private String countryISO;
	private String city;
	private String plateCode;
	private int memberShipTypeId;
	private String color;
	private String dateOfPurchase;
	private String registrationDate;
	private String mileage;
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the vehicleId
	 */
	public int getVehicleId() {
		return vehicleId;
	}
	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	public String getIsFavorite() {
		return isFavorite;
	}
	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVehiclePic() {
		return vehiclePic;
	}
	public void setVehiclePic(String vehcilePic) {
		this.vehiclePic = vehcilePic;
	}
	public Object isSuccess() {
		return isSuccess;
	}
	public void setSuccess(Object isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}
	/**
	 * @param vin the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}
	

	public Object getVehicleList() {
		return vehicleList;
	}
	public void setVehicleList(Object vehicleList) {
		this.vehicleList = vehicleList;
	}
	public Object getVehicleBrandsList() {
		return vehicleBrandsList;
	}
	public void setVehicleBrandsList(Object vehicleBrandsList) {
		this.vehicleBrandsList = vehicleBrandsList;
	}
	public Object getVehicleModelsList() {
		return vehicleModelsList;
	}
	public void setVehicleModelsList(Object vehicleModelsList) {
		this.vehicleModelsList = vehicleModelsList;
	}

	
	public Long getVehicleBrandId() {
		return vehicleBrandId;
	}
	public void setVehicleBrandId(Long vehicleBrandId) {
		this.vehicleBrandId = vehicleBrandId;
	}
	public Long getVehicleModelId() {
		return vehicleModelId;
	}
	public void setVehicleModelId(Long vehicleModelId) {
		this.vehicleModelId = vehicleModelId;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getVehicleBrandName() {
		return vehicleBrandName;
	}
	public void setVehicleBrandName(String vehicleBrandName) {
		this.vehicleBrandName = vehicleBrandName;
	}
	public String getVehicleModelName() {
		return vehicleModelName;
	}
	public void setVehicleModelName(String vehicleModelName) {
		this.vehicleModelName = vehicleModelName;
	}
	/**
	 * @return the isImageChanged
	 */
	public String getIsImageChanged() {
		return isImageChanged;
	}
	/**
	 * @param isImageChanged the isImageChanged to set
	 */
	public void setIsImageChanged(String isImageChanged) {
		this.isImageChanged = isImageChanged;
	}
	/**
	 * @return the vehicleFileNameOnCloud
	 */
	public String getVehicleFileNameOnCloud() {
		return vehicleFileNameOnCloud;
	}
	/**
	 * @param vehicleFileNameOnCloud the vehicleFileNameOnCloud to set
	 */
	public void setVehicleFileNameOnCloud(String vehicleFileNameOnCloud) {
		this.vehicleFileNameOnCloud = vehicleFileNameOnCloud;
	}
	/**
	 * @return the vehicleModelYear
	 */
	public String getVehicleModelYear() {
		return vehicleModelYear;
	}
	/**
	 * @param vehicleModelYear the vehicleModelYear to set
	 */
	public void setVehicleModelYear(String vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}
	
	/**
	 * @return the vehicleRegisterNo
	 */
	public String getVehicleRegisterNo() {
		return vehicleRegisterNo;
	}
	/**
	 * @param vehicleRegisterNo the vehicleRegisterNo to set
	 */
	public void setVehicleRegisterNo(String vehicleRegisterNo) {
		this.vehicleRegisterNo = vehicleRegisterNo;
	}
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the isSuccess
	 */
	public Object getIsSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setIsSuccess(Object isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the memberShipStart
	 */
	public String getMemberShipStart() {
		return memberShipStart;
	}
	/**
	 * @param memberShipStart the memberShipStart to set
	 */
	public void setMemberShipStart(String memberShipStart) {
		this.memberShipStart = memberShipStart;
	}
	/**
	 * @return the memberShipEnd
	 */
	public String getMemberShipEnd() {
		return memberShipEnd;
	}
	/**
	 * @param memberShipEnd the memberShipEnd to set
	 */
	public void setMemberShipEnd(String memberShipEnd) {
		this.memberShipEnd = memberShipEnd;
	}
	/**
	 * @return the countryISO
	 */
	public String getCountryISO() {
		return countryISO;
	}
	/**
	 * @param countryISO the countryISO to set
	 */
	public void setCountryISO(String countryISO) {
		this.countryISO = countryISO;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the plateCode
	 */
	public String getPlateCode() {
		return plateCode;
	}
	/**
	 * @param plateCode the plateCode to set
	 */
	public void setPlateCode(String plateCode) {
		this.plateCode = plateCode;
	}
	/**
	 * @return the memberShipTypeId
	 */
	public int getMemberShipTypeId() {
		return memberShipTypeId;
	}
	/**
	 * @param memberShipTypeId the memberShipTypeId to set
	 */
	public void setMemberShipTypeId(int memberShipTypeId) {
		this.memberShipTypeId = memberShipTypeId;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the dateOfPurchase
	 */
	public String getDateOfPurchase() {
		return dateOfPurchase;
	}
	/**
	 * @param dateOfPurchase the dateOfPurchase to set
	 */
	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}
	/**
	 * @return the registrationDate
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	/**
	 * @return the mileage
	 */
	public String getMileage() {
		return mileage;
	}
	/**
	 * @param mileage the mileage to set
	 */
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	/**
	 * @return the isCustomer
	 */
	public Boolean getIsCustomer() {
		return isCustomer;
	}
	/**
	 * @param isCustomer the isCustomer to set
	 */
	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}
	
	
}
