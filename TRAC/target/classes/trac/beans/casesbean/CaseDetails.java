package trac.beans.casesbean;

import java.io.Serializable;

import trac.beans.customerbean.Customer;

/** Stores Case related information
 * 
 */
public class CaseDetails extends Customer implements Serializable{
	

	private static final long serialVersionUID = -7419015169378678900L;
	private int userId;
	private String email;
	private String caseName;
	private int subcaseId;
	private String caseStatusId;
	private String caseRegisteredTime;
	private String caseServiceLocation;
	private String caseReturnedServiceCount;
	private String caseRegisteredFrom;
	private String caseRegisteredTo;
	private int serviceRequiredTypeId;
	private String serviceRequiredTime;
	private String serviceLocationLatitude;
	private String serviceLocationLongitude;
	private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
	private int serviceDescription;
	private int vehicleId;
	private String caseNotes;
	private String caseComments;
	private int deviceTyepId;
//	private int locationId;
	private int caseTermsAgreed;
	
	private int truckDriverId;
	private String caseNumber;
	private int priorityId;
	
	private int caseFaultId;
	private String caseFaultDescription;
	private int caseServiceId;
	private String caseServiceDescription;
	
	private String isVehicleAvailable;
	private String vehicleAvailableComments;
	private String isServiceAccepted;
	private String serviceComments;
	private String serviceAcceptedComments;
	private String isReported;
	private String isReportedSameIssue;
	private String reportedComments;
	private String isAttendedSolved;
	private String isEmergency;
	private String emergencyComments;
    
	private String isNoDentsScratches;
	private String dentscomments;
	private String isDriverAbletoResolve;
	private String driverComments;
	private String isRequiredServiceProvided;
	private String dispatchedTimeFromLoc;
	private String isTowRequested;
	private String arrivalTimeAtLoc;
	
	private String durationValue;
	
	private int typeId;
	
	private String area;
	private String street;
	private String state;
	private String city;
	private String poBox;
	private String countryCode;
	
	private boolean isCustomer;
	
	private int startIndex;
	private int endIndex;
	
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}

	public boolean getIsCustomer() { 
		return isCustomer; 
	} 
	
	/** * @param isCustomer the isCustomer to set */
	public void setIsCustomer(boolean isCustomer) { 
		this.isCustomer = isCustomer; 
	}
	/**
	 * @return the caseName
	 */
	public synchronized String getCaseName() {
		return caseName;
	}
	/**
	 * @param caseName the caseName to set
	 */
	public synchronized void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	
	/**
	 * @return the caseStatus
	 */
	public synchronized String getCaseStatusId() {
		return caseStatusId;
	}
	/**
	 * @param caseStatus the caseStatus to set
	 */
	public synchronized void setCaseStatus(String caseStatusId) {
		this.caseStatusId = caseStatusId;
	}
	/**
	 * @return the caseRegisteredTime
	 */
	public synchronized String getCaseRegisteredTime() {
		return caseRegisteredTime;
	}
	/**
	 * @param caseRegisteredTime the caseRegisteredTime to set
	 */
	public synchronized void setCaseRegisteredTime(String caseRegisteredTime) {
		this.caseRegisteredTime = caseRegisteredTime;
	}
	/**
	 * @return the caseServiceLocation
	 */
	public synchronized String getCaseServiceLocation() {
		return caseServiceLocation;
	}
	/**
	 * @param caseServiceLocation the caseServiceLocation to set
	 */
	public synchronized void setCaseServiceLocation(String caseServiceLocation) {
		this.caseServiceLocation = caseServiceLocation;
	}
	/**
	 * @return the caseReturnedServiceCount
	 */
	public synchronized String getCaseReturnedServiceCount() {
		return caseReturnedServiceCount;
	}
	/**
	 * @param caseReturnedServiceCount the caseReturnedServiceCount to set
	 */
	public synchronized void setCaseReturnedServiceCount(
			String caseReturnedServiceCount) {
		this.caseReturnedServiceCount = caseReturnedServiceCount;
	}
	/**
	 * @return the caseRegisteredFrom
	 */
	public synchronized String getCaseRegisteredFrom() {
		return caseRegisteredFrom;
	}
	/**
	 * @param caseRegisteredFrom the caseRegisteredFrom to set
	 */
	public synchronized void setCaseRegisteredFrom(String caseRegisteredFrom) {
		this.caseRegisteredFrom = caseRegisteredFrom;
	}
	/**
	 * @return the caseRegisteredTo
	 */
	public synchronized String getCaseRegisteredTo() {
		return caseRegisteredTo;
	}
	/**
	 * @param caseRegisteredTo the caseRegisteredTo to set
	 */
	public synchronized void setCaseRegisteredTo(String caseRegisteredTo) {
		this.caseRegisteredTo = caseRegisteredTo;
	}
	/**
	 * @return the serviceLocationLatitude
	 */
	public synchronized String getServiceLocationLatitude() {
		return serviceLocationLatitude;
	}
	/**
	 * @param serviceLocationLatitude the serviceLocationLatitude to set
	 */
	public synchronized void setServiceLocationLatitude(
			String serviceLocationLatitude) {
		this.serviceLocationLatitude = serviceLocationLatitude;
	}
	/**
	 * @return the serviceLocationLongitude
	 */
	public synchronized String getServiceLocationLongitude() {
		return serviceLocationLongitude;
	}
	/**
	 * @param serviceLocationLongitude the serviceLocationLongitude to set
	 */
	public synchronized void setServiceLocationLongitude(
			String serviceLocationLongitude) {
		this.serviceLocationLongitude = serviceLocationLongitude;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String isServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	/**
	 * @return the serviceDescription
	 */
	public int getServiceDescription() {
		return serviceDescription;
	}
	/**
	 * @param serviceDescription the serviceDescription to set
	 */
	public void setServiceDescription(int serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public int getDeviceTyepId() {
		return deviceTyepId;
	}
	public void setDeviceTyepId(int deviceTyepId) {
		this.deviceTyepId = deviceTyepId;
	}
	/**
	 * @return the caseNotes
	 */
	public String getCaseNotes() {
		return caseNotes;
	}
	/**
	 * @param caseNotes the caseNotes to set
	 */
	public void setCaseNotes(String caseNotes) {
		this.caseNotes = caseNotes;
	}
	/**
	 * @return the serviceRequiredType
	 */
	public int getServiceRequiredTypeId() {
		return serviceRequiredTypeId;
	}
	/**
	 * @param serviceRequiredType the serviceRequiredType to set
	 */
	public void setServiceRequiredTypeId(int serviceRequiredTypeId) {
		this.serviceRequiredTypeId = serviceRequiredTypeId;
	}
	/**
	 * @return the serviceRequiredTime
	 */
	public String getServiceRequiredTime() {
		return serviceRequiredTime;
	}
	/**
	 * @param serviceRequiredTime the serviceRequiredTime to set
	 */
	public void setServiceRequiredTime(String serviceRequiredTime) {
		this.serviceRequiredTime = serviceRequiredTime;
	}
	/**
	 * @return the locationId
	 *//*
	public int getLocationId() {
		return locationId;
	}
	*//**
	 * @param locationId the locationId to set
	 *//*
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}*/
	/**
	 * @return the isCaseTermsAgreed
	 */
	public int getCaseTermsAgreed() {
		return caseTermsAgreed;
	}
	/**
	 * @param isCaseTermsAgreed the isCaseTermsAgreed to set
	 */
	public void setCaseTermsAgreed(int caseTermsAgreed) {
		this.caseTermsAgreed = caseTermsAgreed;
	}
	/**
	 * @return the subcaseId
	 */
	public int getSubcaseId() {
		return subcaseId;
	}
	/**
	 * @param subcaseId the subcaseId to set
	 */
	public void setSubcaseId(int subcaseId) {
		this.subcaseId = subcaseId;
	}
	/**
	 * @return the truckDriverId
	 */
	public int getTruckDriverId() {
		return truckDriverId;
	}
	/**
	 * @param truckDriverId the truckDriverId to set
	 */
	public void setTruckDriverId(int truckDriverId) {
		this.truckDriverId = truckDriverId;
	}
	/**
	 * @return the caseNumber
	 */
	public String getCaseNumber() {
		return caseNumber;
	}
	/**
	 * @param caseNumber the caseNumber to set
	 */
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	/**
	 * @return the priorityId
	 */
	public int getPriorityId() {
		return priorityId;
	}
	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(int priorityId) {
		this.priorityId = priorityId;
	}
	/**
	 * @return the caseFaultId
	 */
	public int getCaseFaultId() {
		return caseFaultId;
	}
	/**
	 * @param caseFaultId the caseFaultId to set
	 */
	public void setCaseFaultId(int caseFaultId) {
		this.caseFaultId = caseFaultId;
	}
	/**
	 * @return the caseFaultDescription
	 */
	public String getCaseFaultDescription() {
		return caseFaultDescription;
	}
	/**
	 * @param caseFaultDescription the caseFaultDescription to set
	 */
	public void setCaseFaultDescription(String caseFaultDescription) {
		this.caseFaultDescription = caseFaultDescription;
	}
	/**
	 * @return the caseServiceId
	 */
	public int getCaseServiceId() {
		return caseServiceId;
	}
	/**
	 * @param caseServiceId the caseServiceId to set
	 */
	public void setCaseServiceId(int caseServiceId) {
		this.caseServiceId = caseServiceId;
	}
	/**
	 * @return the caseServiceDescription
	 */
	public String getCaseServiceDescription() {
		return caseServiceDescription;
	}
	/**
	 * @param caseServiceDescription the caseServiceDescription to set
	 */
	public void setCaseServiceDescription(String caseServiceDescription) {
		this.caseServiceDescription = caseServiceDescription;
	}
	/**
	 * @return the isVehicleAvailable
	 */
	public String getIsVehicleAvailable() {
		return isVehicleAvailable;
	}
	/**
	 * @param isVehicleAvailable the isVehicleAvailable to set
	 */
	public void setIsVehicleAvailable(String isVehicleAvailable) {
		this.isVehicleAvailable = isVehicleAvailable;
	}
	/**
	 * @return the vehicleAvailableComments
	 */
	public String getVehicleAvailableComments() {
		return vehicleAvailableComments;
	}
	/**
	 * @param vehicleAvailableComments the vehicleAvailableComments to set
	 */
	public void setVehicleAvailableComments(String vehicleAvailableComments) {
		this.vehicleAvailableComments = vehicleAvailableComments;
	}
	/**
	 * @return the isServiceAccepted
	 */
	public String getIsServiceAccepted() {
		return isServiceAccepted;
	}
	/**
	 * @param isServiceAccepted the isServiceAccepted to set
	 */
	public void setIsServiceAccepted(String isServiceAccepted) {
		this.isServiceAccepted = isServiceAccepted;
	}
	/**
	 * @return the serviceAcceptedComments
	 */
	public String getServiceAcceptedComments() {
		return serviceAcceptedComments;
	}
	/**
	 * @param serviceAcceptedComments the serviceAcceptedComments to set
	 */
	public void setServiceAcceptedComments(String serviceAcceptedComments) {
		this.serviceAcceptedComments = serviceAcceptedComments;
	}
	/**
	 * @return the isReported
	 */
	public String getIsReported() {
		return isReported;
	}
	/**
	 * @param isReported the isReported to set
	 */
	public void setIsReported(String isReported) {
		this.isReported = isReported;
	}
	
	/**
	 * @return the reportedComments
	 */
	public String getReportedComments() {
		return reportedComments;
	}
	/**
	 * @param reportedComments the reportedComments to set
	 */
	public void setReportedComments(String reportedComments) {
		this.reportedComments = reportedComments;
	}
	/**
	 * @return the isAttendedSolved
	 */
	public String getIsAttendedSolved() {
		return isAttendedSolved;
	}
	/**
	 * @param isAttendedSolved the isAttendedSolved to set
	 */
	public void setIsAttendedSolved(String isAttendedSolved) {
		this.isAttendedSolved = isAttendedSolved;
	}
	/**
	 * @return the isEmergency
	 */
	public String getIsEmergency() {
		return isEmergency;
	}
	/**
	 * @param isEmergency the isEmergency to set
	 */
	public void setIsEmergency(String isEmergency) {
		this.isEmergency = isEmergency;
	}
	/**
	 * @return the emergencyComments
	 */
	public String getEmergencyComments() {
		return emergencyComments;
	}
	/**
	 * @param emergencyComments the emergencyComments to set
	 */
	public void setEmergencyComments(String emergencyComments) {
		this.emergencyComments = emergencyComments;
	}
	
	/**
	 * @return the dentscomments
	 */
	public String getDentscomments() {
		return dentscomments;
	}
	/**
	 * @param dentscomments the dentscomments to set
	 */
	public void setDentscomments(String dentscomments) {
		this.dentscomments = dentscomments;
	}
	/**
	 * @return the isDriverAbletoResolve
	 */
	public String getIsDriverAbletoResolve() {
		return isDriverAbletoResolve;
	}
	/**
	 * @param isDriverAbletoResolve the isDriverAbletoResolve to set
	 */
	public void setIsDriverAbletoResolve(String isDriverAbletoResolve) {
		this.isDriverAbletoResolve = isDriverAbletoResolve;
	}
	/**
	 * @return the driverComments
	 */
	public String getDriverComments() {
		return driverComments;
	}
	/**
	 * @param driverComments the driverComments to set
	 */
	public void setDriverComments(String driverComments) {
		this.driverComments = driverComments;
	}
	/**
	 * @return the isRequiredServiceProvided
	 */
	public String getIsRequiredServiceProvided() {
		return isRequiredServiceProvided;
	}
	/**
	 * @param isRequiredServiceProvided the isRequiredServiceProvided to set
	 */
	public void setIsRequiredServiceProvided(String isRequiredServiceProvided) {
		this.isRequiredServiceProvided = isRequiredServiceProvided;
	}
	/**
	 * @return the dispatchedTimeFromLoc
	 */
	public String getDispatchedTimeFromLoc() {
		return dispatchedTimeFromLoc;
	}
	/**
	 * @param dispatchedTimeFromLoc the dispatchedTimeFromLoc to set
	 */
	public void setDispatchedTimeFromLoc(String dispatchedTimeFromLoc) {
		this.dispatchedTimeFromLoc = dispatchedTimeFromLoc;
	}
	/**
	 * @return the isTowRequested
	 */
	public String getIsTowRequested() {
		return isTowRequested;
	}
	/**
	 * @param isTowRequested the isTowRequested to set
	 */
	public void setIsTowRequested(String isTowRequested) {
		this.isTowRequested = isTowRequested;
	}
	/**
	 * @return the arrivalTimeAtLoc
	 */
	public String getArrivalTimeAtLoc() {
		return arrivalTimeAtLoc;
	}
	/**
	 * @param arrivalTimeAtLoc the arrivalTimeAtLoc to set
	 */
	public void setArrivalTimeAtLoc(String arrivalTimeAtLoc) {
		this.arrivalTimeAtLoc = arrivalTimeAtLoc;
	}
	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param caseStatusId the caseStatusId to set
	 */
	public void setCaseStatusId(String caseStatusId) {
		this.caseStatusId = caseStatusId;
	}
	/**
	 * @return the serviceComments
	 */
	public String getServiceComments() {
		return serviceComments;
	}
	/**
	 * @param serviceComments the serviceComments to set
	 */
	public void setServiceComments(String serviceComments) {
		this.serviceComments = serviceComments;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the durationValue
	 */
	public String getDurationValue() {
		return durationValue;
	}
	/**
	 * @param durationValue the durationValue to set
	 */
	public void setDurationValue(String durationValue) {
		this.durationValue = durationValue;
	}
	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the poBox
	 */
	public String getPoBox() {
		return poBox;
	}
	/**
	 * @param poBox the poBox to set
	 */
	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the isReportedSameIssue
	 */
	public String getIsReportedSameIssue() {
		return isReportedSameIssue;
	}
	/**
	 * @param isReportedSameIssue the isReportedSameIssue to set
	 */
	public void setIsReportedSameIssue(String isReportedSameIssue) {
		this.isReportedSameIssue = isReportedSameIssue;
	}
	/**
	 * @return the isNoDentsScratches
	 */
	public String getIsNoDentsScratches() {
		return isNoDentsScratches;
	}
	/**
	 * @param isNoDentsScratches the isNoDentsScratches to set
	 */
	public void setIsNoDentsScratches(String isNoDentsScratches) {
		this.isNoDentsScratches = isNoDentsScratches;
	}
	/**
	 * @return the caseComments
	 */
	public String getCaseComments() {
		return caseComments;
	}
	/**
	 * @param caseComments the caseComments to set
	 */
	public void setCaseComments(String caseComments) {
		this.caseComments = caseComments;
	}
	/**
	 * @return the startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}
	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	/**
	 * @return the endIndex
	 */
	public int getEndIndex() {
		return endIndex;
	}
	/**
	 * @param endIndex the endIndex to set
	 */
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	
	
	
}
