package trac.dao.reportsdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.userbean.Users;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;

public class ReportsSqlOperations {

	public static JdbcTemplate jdbcTemplate = null;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();


	/*
	 * *
	 * get All operations related to cases
	 * @param userid
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> fetchOpertaionsData(Users userDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_OPERATIONS).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKNICKNAME, rs.getString("trucknickname"));
						responseMap.put("dispatcherName", rs.getString("dispatcherName"));
						responseMap.put("sourceTypeName", rs.getString("sourceTypeName"));
						responseMap.put("customerName", rs.getString("customerName"));
						responseMap.put("mobileNumber", rs.getString("mobileNumber"));
						responseMap.put("locationName", rs.getString("locationName"));
						responseMap.put("Area", rs.getString("Area"));
						responseMap.put("Street", rs.getString("Street"));
						responseMap.put("Building", rs.getString("Building"));
						responseMap.put("City", rs.getString("City"));
						responseMap.put("CountryISO", rs.getString("CountryISO"));
						responseMap.put("vehicleName", rs.getString("vehicleName"));
						responseMap.put("plateCode", rs.getString("plateCode"));
						responseMap.put("vin", rs.getString("vin"));
						responseMap.put("vehiclebrandName", rs.getString("vehiclebrandName"));
						responseMap.put("vehicleModelName", rs.getString("vehicleModelName"));
						responseMap.put("modelYear", rs.getString("modelYear"));
						responseMap.put("color", rs.getString("color"));
						responseMap.put("calledTime", rs.getString("calledTime"));
						responseMap.put("assignedTime", rs.getString("assignedTime"));
						responseMap.put("dispatchedTime", rs.getString("dispatchedTime"));
						responseMap.put("CheckedInTime", rs.getString("CheckedInTime"));
						responseMap.put("completedTime", rs.getString("completedTime"));
						responseMap.put("caseReference", rs.getString("caseReference"));
						responseMap.put("faultNames", rs.getString("faultNames"));
						responseMap.put("serviceNames", rs.getString("serviceNames"));
						responseMap.put("casestatus", rs.getString("casestatus"));
						responseMap.put("dlocationName", rs.getString("dlocationName"));
						responseMap.put("Area", rs.getString("Area"));
						responseMap.put("Street", rs.getString("Street"));
						responseMap.put("Building", rs.getString("Building"));
						responseMap.put("City", rs.getString("City"));
						responseMap.put("CountryISO", rs.getString("CountryISO"));
						responseMap.put("driverComments", rs.getString("driverComments"));
						responseMap.put("customerComments", rs.getString("customerComments"));												


					}else{

						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User not found.");
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * get All Attendance related Data
	 * @param userid
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchAttendanceData(Users userDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_ATTENDANCE).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
						responseMap.put("employeeId", rs.getString("employeeId"));
						responseMap.put("shiftName", rs.getInt("shiftName"));
						responseMap.put("dayId", rs.getInt("dayId"));
						responseMap.put("attendanceDate", rs.getString("attendanceDate"));
						responseMap.put("attendanceTime", rs.getString("attendanceTime"));
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User not found.");
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * get All Trucks related Data
	 * @param userid
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchTrucksData(Users userDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_TRUCK_INFO).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){
						responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKCODE, rs.getString("TruckCode"));
						responseMap.put(ResponseConstantsOfTruck.DATEOFPURCHASE, rs.getString("DateOfPurchase"));
						responseMap.put(ResponseConstantsOfTruck.REGISTRATIONDATE, rs.getString("TruckRegistrationDate"));
						responseMap.put(ResponseConstantsOfTruck.PLATECODE, rs.getString("plateCode"));
						responseMap.put(ResponseConstantsOfTruck.PLATENUMBER, rs.getString("TruckRegistrationNo"));
						responseMap.put(ResponseConstantsOfTruck.VIN, rs.getString("vin"));
						responseMap.put(ResponseConstantsOfTruck.DATEOFACQUIRING, rs.getString("acquiredDate"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDID, rs.getInt("TruckBrandID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDNAME, rs.getString("brand"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKMODELID, rs.getInt("TruckModelID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKMODELNAME, rs.getString("model"));
						responseMap.put(ResponseConstantsOfTruck.MODELYEAR, rs.getString("ModelYear"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKCOLOR, rs.getString("color"));
						responseMap.put(ResponseConstantsOfTruck.TOTALCOST, rs.getString("totalCost"));
						responseMap.put(ResponseConstantsOfTruck.REREGISTRATIONDUEAT, rs.getString("ReRegistrationDueAt"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKNICKNAME, rs.getString("trucknickname"));
						responseMap.put(ResponseConstantsOfTruck.AVAILABLEHOURSPERDAY, rs.getString("AvailableHours"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKPOLICYID, rs.getString("TruckPolicyID"));
						responseMap.put(ResponseConstantsOfTruck.INSCOMPANYNAME, rs.getString("insCompanyName"));
						responseMap.put(ResponseConstantsOfTruck.POLICYNUMBER, rs.getString("policyNumber"));
						responseMap.put(ResponseConstantsOfTruck.POLICYISSUEDAT, rs.getString("IssuedAt"));
						responseMap.put(ResponseConstantsOfTruck.POLICYEXPIREDAT, rs.getString("ExpiredAt"));
						responseMap.put(ResponseConstantsOfTruck.REREGISTRATIONDUEAT, rs.getString("ReRegistrationDueAt"));
						responseMap.put(ResponseConstantsOfTruck.SERVICESPROVIDED, rs.getString("servcices"));
					}else{

						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User not found.");
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


}
