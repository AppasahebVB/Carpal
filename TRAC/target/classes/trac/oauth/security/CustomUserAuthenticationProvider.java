package trac.oauth.security;

import java.util.ArrayList;
import java.util.List;

/*import trac.beans.servicebean.ServiceInfo;
import trac.beans.userbean.Users;
import trac.dao.userdao.UserSqlOperations;
import trac.util.CustomHashing;
import trac.util.utiinterface.SpringByCryptoHashing;
*/
import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;


public class CustomUserAuthenticationProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		System.out.println("username checking--------------------------- "+authentication.getPrincipal().toString());
		
		List<GrantedAuthority> grantedAuthorities =	new ArrayList<GrantedAuthority>();
		CustomUserAuthenticationToken auth = new CustomUserAuthenticationToken(authentication.getPrincipal(),
				authentication.getCredentials(), grantedAuthorities);
		return auth;
	}

	public boolean supports(Class<?> arg0) {
		return true;
	}
}
