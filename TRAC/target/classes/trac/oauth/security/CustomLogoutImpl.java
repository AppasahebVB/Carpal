package trac.oauth.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.JdbcTokenStore;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import trac.dao.logoutdao.LogoutDaoOperations;


import com.google.gson.Gson;

/**
 *  Logout implementaion is done in this class
 *  
 */

public class CustomLogoutImpl  implements LogoutSuccessHandler  {
	

	JdbcTokenStore  tokenstore;
	 private static final Logger logger = LoggerFactory.getLogger(CustomLogoutImpl.class);
	
	public JdbcTokenStore getTokenstore() {
		return tokenstore;
	}


	public void setTokenstore(JdbcTokenStore tokenstore) {
		this.tokenstore = tokenstore;
	}

	/**
	 * This Method is used to display the service message of registered user.To pass token parameter in the form of header.
	 * @param token
	 * 
	 */
	
	@Override
    public void onLogoutSuccess(HttpServletRequest paramHttpServletRequest,
            HttpServletResponse paramHttpServletResponse,
            Authentication paramAuthentication) throws IOException,
            ServletException {
        Map<String,Object> responseMap = new HashMap<String,Object>();
        try{
        boolean isSuccess =  removeaccess(paramHttpServletRequest);
	        if(isSuccess){
	            responseMap.put("isLoggedOut", true);
	            responseMap.put("serviceMessage", "Logged Out Successfully");
	            String responseJson = new Gson().toJson(responseMap);
	            paramHttpServletResponse.getOutputStream().write(responseJson.getBytes());
	            paramHttpServletResponse.setContentType("application/json");
	            paramHttpServletResponse.setCharacterEncoding("UTF-8");
	        }else{
	        	 responseMap.put("isLoggedOut", false);
	        	 responseMap.put("serviceMessage", "Logout Failed.");
	             String responseJson = new Gson().toJson(responseMap);
	             paramHttpServletResponse.getOutputStream().write(responseJson.getBytes());
	             paramHttpServletResponse.setContentType("application/json");
	             paramHttpServletResponse.setCharacterEncoding("UTF-8");
	        }
        }catch(Exception e){
        	System.out.println("exception in onLogoutSuccess method"+ e.getMessage());
        	logger.error("exception in onLogoutSuccess method", e.getMessage());
            responseMap.put("isLoggedOut", false);
            String responseJson = new Gson().toJson(responseMap);
            paramHttpServletResponse.getOutputStream().write(responseJson.getBytes());
            paramHttpServletResponse.setContentType("application/json");
            paramHttpServletResponse.setCharacterEncoding("UTF-8");
        }
        
        
    }
	
	/**
	 * This method is used to Token Authorization of register user. if user gives correct tokenID it returns true.
	   otherwise returns false.
	 * @param
	 * @return
	 */
	/*
	 * IF CORRECT NO RECORD RETURN So SUCCESS iF iNCORRECRT rECORD eXISTS RETUN
	 * FAIL
	 */

	
public boolean removeaccess(HttpServletRequest req){
		
		String tokens=req.getHeader("Authorization");
		String userId=tokens.split(" ")[2];
		boolean isTokenExist = false;
		System.out.println(tokens);
		System.out.println(tokens.indexOf(" "));
		System.out.println(tokens.split(" ")[1]);
		System.out.println(tokens.split(" ")[2]);
		String value=tokens.split(" ")[1];
		if(value.length() > 0){
			DefaultOAuth2AccessToken token= new DefaultOAuth2AccessToken(value);
			System.out.println("isExpired -"+token.isExpired());
			tokenstore.removeAccessToken(value.trim());
			isTokenExist = new LogoutDaoOperations().accessTokenValidation(userId);
			

		}
		return !isTokenExist;
		
	}



}
