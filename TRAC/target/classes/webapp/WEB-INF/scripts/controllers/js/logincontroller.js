var loginAppModule =  angular.module("TRACLOGIN", []).controller('loginController', loginController);
	
	function loginController($scope,$http) {
		$scope.isServiceValidationExist = false;
		  $scope.signinUser = function signinUser(user) {
			     
			     var isValid = $scope.userForm.$valid;
			  	console.log("user json = "+JSON.stringify(user));
			  	console.log("isValid = "+isValid);
			  	if(isValid){
			    $http.defaults.headers.post["Content-Type"] = "application/json";
					$http.post(SERVICEADDRESS+'/user/validate',JSON.stringify(user)).
					  success(function(returnJson) {
						    	
						    	if(returnJson.isSuccess){
						    		window.location.href = HOSTADDRESS+"/index.html";
						    	}else{
						    		$scope.isServiceValidationExist = true;
						    		$scope.serviceMessage = returnJson.serviceMessage;
						    		//alert(returnJson.serviceMessage);
						    	}
						    	
					  }).error(function(returnErrorJson, status, headers, config) {
					         console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
					 });//error
				};//isvalid condition
		  };//signinUser function end
}; //signinUser function end