
/**
* Building service and view url's
* 
* @author  : Appshark
* @version : 1.0
* @service : 
*/

//LOCAL-SERVER
var IPADDRESS = "localhost";
var PORT = "8888";
var APPDEFAULTURL ="resources/views";

//CLOUD-SERVER
/*var IPADDRESS = "4.trac-appshark-20141113.appspot.com";
var PORT = "";
var APPDEFAULTURL ="resources/views";*/

// for accessing static resurces
var HOSTADDRESS;
//for accessing services
var SERVICEADDRESS;

if (PORT != '') {
    HOSTADDRESS =  'http://' + IPADDRESS + ':' + PORT + '/' + APPDEFAULTURL;
    SERVICEADDRESS = 'http://' + IPADDRESS + ':' + PORT;
}
else {
    HOSTADDRESS = 'http://' + IPADDRESS + '/' + APPDEFAULTURL;
    SERVICEADDRESS = 'http://' + IPADDRESS;
  
}