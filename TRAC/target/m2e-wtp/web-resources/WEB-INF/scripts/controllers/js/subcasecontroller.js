var TRACAPP =  angular.module("TRACAPP", ['ui.bootstrap']);
TRACAPP.config(function (datepickerConfig) {
    datepickerConfig.showWeeks = false;
});

TRACAPP.run(function ($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function () {
        $templateCache.removeAll();
    });
});


TRACAPP.controller("subcasecontroller", function($scope, $http, localStorage,$filter) {
	  //common headers for all http posts
       $http.defaults.headers.post["Content-Type"] = "application/json";
       $http.defaults.headers.common['Authorization'] = "bearer " + localStorage.getData("accessToken");
	
	    angular.isUndefinedOrNull = function(val) {
	    	return angular.isUndefined(val) || val === null;
	    };

	    /**
	     * case prerequisite job details
	     */
	    
	 
		
		/**
		 * Fetch List of Cases Info
		 */
		$scope.getListOfCases = function getListOfCases(casetype){

			$scope.casesList = [];

			var casesRequest = {
					typeId:casetype,
					userId:$scope.userId
					//$scope.userId
			};	

			console.log('/cases/listAllCases'+JSON.stringify(casesRequest));
			$http.post(SERVICEADDRESS + '/cases/listAllCases', JSON.stringify(casesRequest)).
			success(function(returnJson) {

				if($scope.isFirst){
					App.init(); // Init layout and core plugins
					Plugins.init(); // Init all plugins
					FormComponents.init(); // Init all form-specific plugins
					$scope.isFirst = false;
				}
							
				if (returnJson.isSuccess) {

					$scope.casesList = returnJson.caseslist;
					console.log('Response'+JSON.stringify(returnJson));
					setTimeout(function(){ 
						
						
						//var table = $('#tabledata').dataTable().fnDestroy();
						$('#tabledata').dataTable( {
							"bDestroy": true,
							"pagingType": "full_numbers"
						});
							
					},1000);
					

				} else {
					$scope.serviceMessage = returnJson.serviceMessage;

				}

			}).error(function(returnErrorJson, status, headers, config) {
				isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.getListOfCases(casetype);
				}
	           

			}); //error


		}; //end of fetchListOfCases 
		
	
        $scope.getCaseDetailsById = function getCaseDetailsById(casesId){
            
        	var caseDetails={};
            
            caseDetails.userId = $scope.userId;
            caseDetails.caseId = casesId;
            console.log("caseDetails json " + JSON.stringify(caseDetails));
          
            $http.post(SERVICEADDRESS + '/cases/fetchCaseDetailedInfo', JSON.stringify(caseDetails)).
            success(function(returnJson) {
            
                if (returnJson.isSuccess) {
                	$scope.casesview = '/resources/views/bodytemplates/main_case_faults.html';
          		}
            }).error(function(returnErrorJson, status, headers, config) {
            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.getCaseDetailsById(casesId); 
    			}
               

            }); //error
            
           
        }; //end of getCaseDetailsById
        
        
        /**
    	 * To fetch faults and there related service 
    	 * 
    	 * 
    	 * @return selection array
    	 */

    	$scope.caseFaultServicesList =  function () {
    		
    		$scope.faults = [];
    		$scope.faultsId = [];
    		
    		$http.get(SERVICEADDRESS+'/caseRegistration/fetchcasefaultservices').
    		success(function(returnJson) {

    			if(returnJson.isSuccess){
    				//loading list of case faults and services from DB
    				$scope.faults = returnJson.caseFaultServicesList;
    			}else{
    				$scope.serviceMessage = returnJson.serviceMessage;
    			}
    			console.log(JSON.stringify(returnJson));

    		}).error(function(returnErrorJson, status, headers, config) {			
    			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
    			if(isFreshTokenGenarated){
    				$scope.caseFaultServicesList();
    			}
    		});//error
    	};//caseFaultServicesList function end
  

  $scope.getValueFromJson =  function (jsonToSearch, comparableValue, prop,valueToGet) {
		var l = jsonToSearch.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = jsonToSearch[k][prop];
			var searcValue = comparableValue;
			if (indexValue == searcValue) {
				return jsonToSearch[k][valueToGet];
			}
		}
		return false;
	};
	
	//json serach to find out location where the value resides
	$scope.getIndexOf =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop] - "+ arr[k][prop]);
			var indexValue = arr[k][prop].trim().toString();
			var searcValue = coparableValue.trim().toString();
			if (indexValue.indexOf(searcValue) >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOfFault =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop] - "+ arr[k][prop]);
			var indexValue = arr[k][prop];
			var searcValue = coparableValue;
			
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	
	//json serach to find out location where the value resides
	$scope.searchByMultipleParameters =  function (arr, comparableValue, prop1,prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop1] - "+ arr[k][prop1]);
			var indexValue1 = arr[k][prop1];
			var indexValue2 = arr[k][prop2];
			var searcValue = comparableValue;
			
			if (indexValue1 == searcValue || indexValue2 == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	
	   $scope.logoutUser = function logoutUser() {
	   	$http.defaults.headers.common['Authorization'] = "bearer "+ accessToken + " " +  userId;
			//service call
			$http.post(SERVICEADDRESS+'/logout').
			  success(function(returnJson) {
				    	
				    	if(returnJson.isLoggedOut){
				    		localStorage.clearStorage();
				    		window.location.href = HOSTADDRESS+"/login.html";
				    	
				    	}else{
				    		
				    	}
				    	
			  }).error(function(returnErrorJson, status, headers, config) {
				  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.logoutUser(); 
					}
			 });//error
			  
		  };//logoutUser function end
		  
	 
		
		$scope.initScopeVariables = function(){
		  
		    $scope.accessToken = localStorage.getData("accessToken");//accessToken
	        $scope.userId = localStorage.getData("userId");//userId
	   	   //display name or email of user
			$scope.displayName = localStorage.getData("displayName");
			$scope.isValidated = true; //scope variable to check against service message
		    //default view to be displayed
			$scope.casesview = '/resources/views/bodytemplates/list_of_cases_table.html';
			$scope.getListOfCases(1);
			 
	    	$scope.casesFilter = [{"typeID":1,"caseFilterName":"Display My Cases - Open"},
	    	                      {"typeID":2,"caseFilterName":"Display All Cases"},
	    	                      {"typeID":3,"caseFilterName":"Display All Cases - Pending"},
	    	                      {"typeID":4,"caseFilterName":"Display My Cases - Pending"},
	    	                      {"typeID":5,"caseFilterName":"Display All Cases - Closed"},
	    	                      {"typeID":6,"caseFilterName":"Display My Cases - Closed"}
	    	                      ];
			$scope.isFirst = true;
			$scope.table = '';
	    	
		};
		
		 $scope.initScopeVariables();
    
    $scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};
	
	$scope.toggleMin();

	$scope.open = function($event) {
		
		$event.preventDefault();
		$event.stopPropagation();
		console.log(" $scope.opened", $scope.opened);
		$scope.opened = true;
	};
	
	$scope.dateOptions = {
			formatYear: 'yyyy',
			startingDay: 1
	};
	
	$scope.datepickerYearOptions = {
			'formatYear': 'yyyy',
			'startingDay' : 0,
			'minMode':'year'
		    
	};//end
	
	$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy'];
	$scope.format = $scope.formats[0];
	$scope.yearformat = $scope.formats[4];
});// End Controller

