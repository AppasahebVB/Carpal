
TRACAPP.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


TRACAPP.controller("registrationController", function($scope,$http,localStorage,$location,$timeout,$route,dataService) {
	
	 $scope.displayName = localStorage.getData("displayName");
	 $scope.accessToken = localStorage.getData("accessToken"); //access token
	 $scope.userId = localStorage.getData("userId"); //user id
	 
	    angular.isUndefinedOrNull = function(val) {
			return angular.isUndefined(val) || val === null;
		};
	    
		//common headers for all http posts
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.defaults.headers.common['Authorization'] = "bearer " + $scope.accessToken;
	$scope.isServiceValidationExist = false;

	$scope.rolesList = [];
	$scope.listOfUsers = [];
	$scope.isValidated = true;
	$scope.onSuccess = false;
	
	
	//method to fetch security questions
	$scope.getRolesList = function getRolesList() {
		
		$('#displayOnSuccess').hide();
		$('#displayOnError').hide();
		$http({
	        method: 'GET',
	        url: SERVICEADDRESS + '/user/getroles'
	    }).success(function(returnJson){
	    	
		    $scope.rolesList = returnJson.userRolesList;
            
       
		}).error(function(returnErrorJson, status, headers, config) {
					console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
		});//end of http post call
	};
	
	$scope.createUser = function createUser(user) {
		
	  	var formData = new FormData(this);
	  	formData.append('firstName', user.firstName);
	  	formData.append('lastName', user.lastName);
	  	formData.append('email', user.email);
	  	formData.append('phone', user.phone);
	  	formData.append('roleIds', user.roleIds);
		formData.append('file', $scope.file);
	    console.log("formData - "+formData);
	    console.log("SERVICEADDRESS - "+SERVICEADDRESS);
	    	$.ajax({
			        url: SERVICEADDRESS + '/tracuser/register',
			        type: 'POST',
			        data:  formData,
				    mimeType:"multipart/form-data",
				    authorization : "bearer " + $scope.accessToken,
				    contentType: false,
			        cache: false,
			        processData:false,
			    success: function(data, textStatus, jqXHR){
			 		var returnJSON = $.parseJSON(data);
			 		console.log("returnJSON - "+ returnJSON);
			 		if(returnJSON.isSuccess){
			 			$('#displayOnSuccess').show();
				 		$('#displayServiceMessage').text(returnJSON.serviceMessage);
			    	}else{
			    		$('#displayOnError').show();
				 		$('#displayServiceMessage').text(returnJSON.serviceMessage);
			    	}
			    },
			     error: function(jqXHR, textStatus, errorThrown){
			     }         
			    });
			 
          e.preventDefault(); //Prevent Default action.
          return false;
	};
	
	//method to fetch security questions
	$scope.initListOfRoles = function initListOfRoles() {
	
		$http({
	        method: 'GET',
	        url: SERVICEADDRESS + '/user/list',
	        params: {userId: $scope.userId}
	    }).success(function(returnJson){
	    	if(returnJson.isSuccess){
	    		$scope.listOfUsers = returnJson.listOfUsers;
	    	}else{
	    		
	    	}
       
		}).error(function(returnErrorJson, status, headers, config) {
					console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
		});//end of http post call
	};
	
	
	$scope.removeUser = function removeUser(userObject,index) {
		$http({
	        method: 'PUT',
	        url: SERVICEADDRESS + '/user/remove',
	        params: {userId: userObject.userID}
	    }).success(function(returnJson){
	    	if(returnJson.isSuccess){
	    		$scope.isValidated = true;
	    		$scope.onSuccess = true;
	    		$scope.listOfUsers.splice(index,1);
	    		 $timeout(function(){
					 $scope.onSuccess = false;
			     }, 10000);
	    	}else{
	    		$scope.isValidated = false;
	    	}
	    	$scope.serviceMessage = returnJson.serviceMessage;
	    }).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
	    });//end of http post call
	};
	    
	    $scope.userActivation = function userActivation(userObject,index) {
			$http({
		        method: 'PUT',
		        url: SERVICEADDRESS + '/user/activation',
		        params: {userId: userObject.userID}
		    }).success(function(returnJson){
		    	if(returnJson.isSuccess){
		    		$scope.isValidated = true;
		    		$scope.onSuccess = true;
		    		$scope.listOfUsers[index].isActive = returnJson.isActive;
		    		 $timeout(function(){
						 $scope.onSuccess = false;
				     }, 10000);
		    	}else{
		    		$scope.isValidated = false;
		    	}
		    	
		    	$scope.serviceMessage = returnJson.serviceMessage;
		    }).error(function(returnErrorJson, status, headers, config) {
				console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
		    });//end of http post call
	    };
});

