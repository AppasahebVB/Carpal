TRACAPP.config(function (datepickerConfig) {
    datepickerConfig.showWeeks = false;
});



TRACAPP.controller("casecontroller", ['$scope', '$q', '$timeout','$interval','$http', 'localStorage','$filter','$location','$route','$routeParams','dataService','casedDetailsObject','$anchorScroll','$window',
                                      function ($scope,$q ,$timeout,$interval,$http, localStorage,$filter,$location,$route,$routeParams,dataService,casedDetailsObject,$anchorScroll,$window) {
	  //common headers for all http posts
       $http.defaults.headers.post["Content-Type"] = "application/json";
       $http.defaults.headers.common['Authorization'] = "bearer " + localStorage.getData("accessToken");
	
	    angular.isUndefinedOrNull = function(val) {
	    	return angular.isUndefined(val) || val === null;
	    };
	
		
	    $scope.listOfPermissions = [];

	  //the model returns a promise and THEN items
	  	 dataService.getItems().then(function(items) {
	  	        $scope.listOfPermissions=items;
	  	 }, function (status) {
	          console.log(status);
	      });
	   
	
	  	$scope.getCurrentDate = new Date();
	  	
		$scope.casesinit = function () {
			$scope.fetchListOfCases(0,0,50,false);
		};
		
		$scope.customerRatingInit = function () {
		  $scope.rate = 0;
		  $scope.max = 5;
		  $scope.isReadonly = false;
		};
		
		$scope.customerRatingInit();
		
		 $scope.range = function(min, max, step){
			    step = step || 1;
			    var input = [];
			    for (var i = min; i <= max; i += step) input.push(i);
			    return input;
		};
		
		$scope.hoveringOver = function(value) {
			    $scope.overStar = value;
			    $scope.ratingPercent = 100 * (value / $scope.max);
	     };
		
	     /**
		 * Function to call when redirecting from case schedule page to main case
		 */
	     $scope.initMainCaseDeatilsView = function(){
		   	console.log("$routeParams"+$routeParams.param1);
			 var searchParameter = $routeParams.param1;
			 if(!angular.isUndefinedOrNull(searchParameter)){
				 if(searchParameter.length > 0){
					  
					    $scope.getBrandAndModelList();
					    $scope.isCustomerRegistered = true;
	    	     		$scope.isVehicleRegistered = true;
	    	     		$scope.isServiceLocRegistered = true;
	    	     		$scope.isMainCasegistered = true;
	    	     		$scope.isFromCaseSchedule = true;
	    	     		$scope.isCustomer = ($routeParams.param2 == "true");
	    				$scope.isForScheduleView = ($routeParams.param3 == "true");
	    	     		$scope.activetab = 5;
	                 	$scope.progressBarIndex = 6;
	                 	if(!$scope.isForScheduleView)
	                 		$scope.getAllCaseDetails(searchParameter,$scope.isCustomer);
	                 	else
	                 		$scope.getCaseDetailsForView(searchParameter,$scope.isCustomer);
	                 	
	               		$scope.view = '/resources/views/bodytemplates/case_info_body.html';
	             }else{
					 $scope.view = '/resources/views/bodytemplates/case_contactdetails_body.html';
					 $scope.isCustomerRegistered = false;
				     $scope.isVehicleRegistered = false;
				     $scope.isServiceLocRegistered = false;
				     $scope.isMainCasegistered = false;
				     $scope.activetab = 0;
				     $scope.progressBarIndex = 1;
				     $scope.progressBarLoading();
				 }
			 }else{
				 $scope.view = '/resources/views/bodytemplates/case_contactdetails_body.html';
				 $scope.isCustomerRegistered = false;
			     $scope.isVehicleRegistered = false;
			     $scope.isServiceLocRegistered = false;
			     $scope.isMainCasegistered = false;
			     $scope.activetab = 0;
			     $scope.progressBarIndex = 1;
			 	 $scope.progressBarLoading();
			 }
	  		$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
	     };
	     
	     
	     /**
			 * Declaration and initialization of parameters
			 */
		$scope.initScopeVariables = function(isBrandModelsRequired){
		
		   $scope.screenTitle = "New Case";
		   $scope.datePattern=/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i;
		   $scope.accessToken = localStorage.getData("accessToken");//accessToken
	       $scope.userId = localStorage.getData("userId");//userId
	   	   
	       //display name or email of user
			$scope.displayName = localStorage.getData("displayName");
			$scope.isValidated = true; //scope variable to check against service message
			$scope.slideToTop();
			$scope.init();
			
			if(isBrandModelsRequired)
				$scope.getBrandAndModelList();
			
			 $scope.caseInfo = {};
			 $scope.caseInfo.caseView = false; 
			 $scope.caseInfo.vehicleServiceInfoView = false;
			 $scope.caseInfo.vehicleInfoView = false;
			 $scope.caseInfo.customerView = false;
			 $scope.caseInfo.showCaseview = false;
			
			 $scope.maxScheduledDate = new Date();
			$scope.searchList =  [];
			$scope.searchVehicleList = [];
			$scope.brands = [];
			$scope.brandId = [];
	        $scope.models = [];
	    	$scope.modelIds = [];
	    	$scope.subcasesList =[];
	    	$scope.vehiclesList =[];
	    	$scope.customerLocationList =[];
	    	$scope.selectedFaluts = [];
	    	$scope.selection = [];
	    	$scope.lisOfServices =[];
	    	$scope.selectedServices =[];
	    	$scope.tempSelectedServices =[];
	    	$scope.modules   = [];
	       	$scope.caseCountryNameList = [];
		    $scope.caseCountryIsoList = [];
		    $scope.destinationLocationList = [];
		    $scope.commentsList = [];
			$scope.faults = [];
			$scope.faultsId = [];
			
	    	$scope.prioritys = [{"pID":1,"pName":"High"},{"pID":2,"pName":"Medium"},{"pID":3,"pName":"Low"}];
	        $scope.MTypes = [{"typeID":1,"typeName":"Temporary"},{"typeID":2,"typeName":"Mandatory"}];
	        
	        $scope.selectedBrandId;
	    	$scope.selectedModelId;
	        $scope.isValidated = true;
	        $scope.canShowCommewnts = false;
	        $scope.isBrandValid = true;
	        $scope.isModelValid = true;
	        $scope.isCountrySelected = false;
	        $scope.isVehicleCountrySelected = false;
	        $scope.isSearchParameterValid = false;
	        $scope.isFaultValid = true;
	        $scope.isServiceAdded = false;
	        $scope.isVinValid = true;
	        $scope.isVehcileFound= true;
	        $scope.isVehcileSLFound = false;
	        $scope.isTowingRequired = false;
	        $scope.isdestinationLocSelected = false;	
	        $scope.isDestinationLocRequired = false;
	        $scope.ifNoRecordsFound = false;
	        $scope.onSuccess = false;
	        $scope.caseonSuccess = false;
	        $scope.isCaseAssigned = false;
	        $scope.processing = true;
	        $scope.showCustomerSatisfaction = false;
	        $scope.caseCloseValidated = true;
	        $scope.canAddAssistance = false;
	        $scope.canUpdateFault = false;
	        $scope.isCustomer = true;
	        $scope.isCaseClosedByDifferentIssue = false;
	        $scope.initMainCaseDeatilsView();
	        $timeout(function(){
		          $scope.canLoadNow = true;
		    }, 60000);
	       // Radio button validations(Prerequisite job details)
	 	   $scope.isVehicleAvailable = '';
	 	   $scope.Services = '';
	 	   $scope.Reported = '';
	 	   $scope.Actual = '';
	 	   $scope.attendant = '';
	 	   $scope.subCaseNumber = '';
	 	   $scope.subCaseFault = '';
	 	   $scope.subCaseFaultId ='';
	 	   $scope.servicesList = [];
	 	   // Radio button validations(Case On Job details)
	 	   $scope.Dents = '';
	 	   $scope.resolve = '';
	 	   $scope.Services = '';
	 	   $scope.Tow = '';
	 	    $scope.selectedFaults = 0;
	        $scope.brandModels = {};
	      	$scope.truckDriverId =0;
	    	$scope.selectedFaultIndex = -1;
	    	$scope.selectedFaultIndexNo = -1;
	    	$scope.maxSize = 10;
	    	$scope.maxvisiblepagesSize=5;
	    	$scope.isServiceValid = false;
	    	$scope.timerformat = 'yyyy/MM/dd h:mm:ss a';
	        $scope.dateformat = 'yyyy/MM/dd';
	        $scope.timeformat = 'h:mm:ss a';
	        $scope.timerformatedDate = '';
	    	$scope.timerformatTime = '';
	    	$scope.platenumberFocus = true;
	    
		};
		
		
		$scope.chnageMaxSizeOfList = function (maxSize) {
			$scope.maxSize = maxSize;
			$scope.indexstart = $scope.currentPage - 1;
			if($scope.totalItems<$scope.currentPage*$scope.maxSize){
				$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.totalItems);
			}else{
				$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.currentPage*$scope.maxSize);	
			}
			$scope.$apply();
		};
		
		/**
		 * progress bar div cacluations
		 */
		$scope.progressBarLoading = function () {
			// To set progress bar width 
			var timeOutVar = 0;
			if($scope.progressBarIndex == 1){
				timeOutVar = 1000;
			}
			$timeout(function(){
				var divWidth  = $('div#bar').width();
				$scope.percent = (parseInt(divWidth)/4) * $scope.progressBarIndex;
			},timeOutVar);
			
		};
		
		
		/**
		 * progress bar div cacluations
		 */
		$scope.progressBarLoadingFormainCaseView = function () {
			// To set progress bar width 
			var timeOutVar = 1000;
			//timeout function to wait and load progress bar based on div calculations
			$timeout(function(){
				var divWidth  = $('div#bar').width();
				$scope.percent = (parseInt(divWidth)/5) *  $scope.progressBarIndex;
			},timeOutVar);
			
		};
		

		/**
		 * progress bar div cacluations
		 */
		$scope.showByHideDiv = function () {
			if($scope.canShowCommewnts){
				$scope.canShowCommewnts = false;
			}else{
				$scope.canShowCommewnts = true;
			}
		};
		
		
		
		/**
		 * To initialize default parameters on case schedule page load
		 */
		$scope.initCaseScheduleDetails = function () {
		    
		    // Run function every minute to get case que count
			 var intervalPromise = $interval(function () {
				// var currentDate = $filter('date')(new Date(), 'yyyy-MM-dd mm:ss');
			    	//console.log("currentDate - "+currentDate);
			    	$scope.getCasesCount();
			 }, 60000);      
			  
			 //on page change destroy timer
			 $scope.$on('$destroy', function () {
				 $interval.cancel(intervalPromise); 
			});
			    
			 //get parameter from url
			var caseTypeId = $routeParams.param1;
			
			$scope.casesFilterList = [];
			$scope.casescheduleList = [];
			$scope.initCaseScheduleDetails = true;
			$scope.currentPage = 1;
			$scope.casesFilter = [{"typeID":0,"caseFilterName":"Last 48hrs Cases"},
			                      {"typeID":1,"caseFilterName":"My Cases - Open"},
			                      {"typeID":2,"caseFilterName":"All Cases"},
			                      {"typeID":3,"caseFilterName":"All Cases - Pending"},
			                      {"typeID":4,"caseFilterName":"My Cases - Pending"},
			                      {"typeID":5,"caseFilterName":"All Cases - Closed"},
			                      {"typeID":6,"caseFilterName":"My Cases - Closed"}
			                      ];
			//updating view
			$scope.scheduleview = '/resources/views/bodytemplates/case_schedule_body.html';
			
			$scope.initScopeVariables(false);
			
			//null condition check
			if(angular.isUndefinedOrNull(caseTypeId)){
				$scope.casetype = 0;
			}else{
				$scope.casetype = parseInt(caseTypeId);
			}
			
			
			//check and get list based on case type
			if($scope.casetype == 0 || $scope.casetype == 1  || $scope.casetype == 4)
				$scope.fetchCaseSchedule($scope.casetype);
			else
				$scope.fetchListOfCases($scope.casetype,0,50,false);
			
		};
		
		/**
		 * To initialize default parameteres on pre requisitie page load
		 */
		$scope.initPreRequisitieDetails = function () {
			//$scope.cases = {};
			$scope.cases.isVehicleAvailable = true;
			$scope.cases.isServiceAccepted = true;
			$scope.cases.isReported = true;
			$scope.cases.isAttendedSolved = true;
			$scope.cases.isReportedSameIssue = true;
			$scope.cases.isNoDentsScratches = true;
			 // set default value
	 	    $scope.isEmergency = false;
			$scope.cases.emergencyComments = "";
			$scope.cases.vehicleAvailableComments = "";
			$scope.cases.serviceAcceptedComments = "";
			$scope.cases.reportedComments = "";
			$scope.cases.dentscomments = "";
			
		};
		
		/**
		 * To initialize default parameteres on case on job page load
		 */
		$scope.initCaseOnJobDetails = function () {
			$scope.cases.dispatchedhh = 00;
			$scope.cases.dispatchedmm = 00;
			$scope.cases.arrivedhh = 00;
			$scope.cases.arrivedmm = 00;
			if(!$scope.cases.isTowRequested)
				$scope.cases.isTowRequested = false;
			$scope.cases.isDriverAbletoResolve = true;
			$scope.cases.isRequiredServiceProvided = true;
			
		};
		
		/**
		 * To scroll page to top
		 */
		$scope.scrollPageToTop = function(){
			if(!$scope.isValidated || $scope.onSuccess){
				
				$( 'html, body').animate({
				    scrollTop: 0
				  }, 1000);
			}
		};
		
		$scope.datevalidation =function(dateToBeValidated){
			
			if(!angular.isUndefinedOrNull(dateToBeValidated)){
				if(dateToBeValidated.length > 0)
					return true;
			}
			return false;
		};
		
		/**
		 * Populate country model year
		 */
		 $scope.populateCountryModelYear = function(vin){
			 if(!angular.isUndefinedOrNull(vin)){
				 $scope.cases.vehicleBrandId = "";
				 $scope.cases.vehicleModelYear = "";
				 //set brand model based on first 2(or)3 characters in vin
				 var vinmodelCode = angular.uppercase(vin.substring(0,2));
				 for(var i=0;i<$scope.vinmodelsList.length;i++){
					 var vincountryCode = $scope.vinmodelsList[i].vincountryCode;
						 if(vincountryCode.indexOf(vinmodelCode)>-1){
							 $scope.cases.vehicleBrandId = $scope.vinmodelsList[i].manufacturer;
							 break;
						 };
					 }
				  
					  vinmodelCode = vin.substring(0,3);
					 for(var i=0;i<$scope.vinmodelsList.length;i++)
					 {
					 var vincountryCode = $scope.vinmodelsList[i].vincountryCode;
					 if(vincountryCode.indexOf(vinmodelCode)>-1){
						 $scope.cases.vehicleBrandId = $scope.vinmodelsList[i].manufacturer;
						 break;
					 };
					 }
				
	              //set model year based on 10 th character in vin
					 var vinyearCode = vin.charAt(9);
					 for(var i=0;i<$scope.vinmodelyearList.length;i++)
					 {
						 var vinmodelCode = $scope.vinmodelyearList[i].vinmodelCode;
						 if(vinmodelCode.indexOf(vinyearCode)>-1){
							 $scope.cases.vehicleModelYear = $scope.vinmodelyearList[i].vinModelYear;
							 break;
						 };
					 }
					 
					 var indexLoc = parseInt($scope.getIndexOf($scope.brandModelList, $scope.cases.vehicleBrandId,"vehicleBrandName"));
	                 
	                 if(indexLoc >= 0){
	                	 $scope.isBrandValid = true;
	                	 $scope.brandModels =  $scope.brandModelList[indexLoc].brandModels;
	                	 var brandModelListLength = $scope.brandModels.length;
	                	  var selectedBrandId  =  $scope.brandModelList[indexLoc].vehicleBrandId;
	                	  $scope.cases.selectedBrandId = selectedBrandId;
	                	  $scope.cases.isBrandValid = true;
	                	  $scope.cases.selectedBrandIndex = indexLoc;
	                	 
	                 	 var noOfModels= parseInt($scope.models.length);
	         		  	  if(noOfModels > 0){
	         		  		$scope.models.splice(0);
	         		  		$scope.modelIds.splice(0);
	                      }
	         		  
	             	 		for(var i=0;i<brandModelListLength;i++){
		      					$scope.models.push($scope.brandModels[i].vehicleModelName);	
		      					$scope.modelIds.push($scope.brandModels[i].vehicleModelId);
		      				};
				 
	                 }
	                 
				 }
			    
	};
		
	/**
     * save customer Contact Details
     */
    $scope.saveCaseCustomerDetails = function saveCaseCustomerDetails(cases) {
    	
    	
    	var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
		if(!canWrite){
			$("html, body").animate({ scrollTop: 0 }, 1000);
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to add case";
			return false;
		}
		
    	var caseObject = {};
    	caseObject.firstName = cases.firstName; 
    	caseObject.lastName = cases.lastName;
    	caseObject.mobileNumber = cases.mobileNumber; 
    	caseObject.emailId = cases.emailId;
    	caseObject.customerLocationName = cases.customerLocationName; 
    	caseObject.area = cases.area;
    	caseObject.street = cases.street; 
    	caseObject.building = cases.building;
    	caseObject.customerLocationCity = cases.customerLocationCity;
    	caseObject.poBox = cases.poBox;
    	caseObject.customerLocationCountryCode = cases.customerLocationCountryCode;
    	caseObject.customerLocationLatitude = cases.customerLocationLatitude;
    	caseObject.customerLocationLongitude = cases.customerLocationLongitude;
    	caseObject.updatedBy = $scope.userId;
    	
        var isValid = $scope.tracForm.$valid;
        
        
       var enteredCountryName = cases.customerLocationCountryName;
   	   if(!angular.isUndefinedOrNull(enteredCountryName)){// country validation
   		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
   		   if(indexLoc  >= 0){
   			   $scope.isCountrySelected = false;
   		   }else{
   				$scope.isCountrySelected = true;
   				return false;
   		   }
   		}
    	
   	  
        console.log("cases json " + JSON.stringify(caseObject));
        
        if(isValid){ //if form valid
        //http call
        $http.post(SERVICEADDRESS + '/contactDetailsOfCustomer/saveCustomerContactDetails', JSON.stringify(caseObject)).success(function(returnJson) {
        	console.log("returnJson - "+ JSON.stringify(returnJson));
            if (returnJson.isSuccess) { //on success
            	$scope.isValidated = true;      
            	
            	$scope.cases.customerId = returnJson.customerId;
            	$scope.cases.customerLocationId = returnJson.locationId;
            	$scope.activetab = 1;
            	
            	if(!$scope.isCustomerRegistered){ //stopping to load on repetitive calls
                	$scope.progressBarIndex = $scope.progressBarIndex + 1;
	            	$scope.progressBarLoading();
	            }
            	$scope.isCustomerRegistered = true;
            	$scope.view = '/resources/views/bodytemplates/vehicle_info_body.html'; //loading next page
               
            } else {
            	$("html, body").animate({ scrollTop: 0 }, 1000);
            	$scope.isValidated = false;
                $scope.serviceMessage = returnJson.serviceMessage;
            }

        }).error(function(returnErrorJson, status, headers, config) {
        	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveCaseCustomerDetails(cases); 
			}

        }); //error
        
       } //if end
    
    }; //end of saveCaseCustomer Details
    
    
    /**
     * save case related comments
     */
    $scope.saveCaseComments = function saveCaseComments(cases) {
    	
    	var isValid = true;
    	var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
		if(!canWrite){
			$("html, body").animate({ scrollTop: 0 }, 1000);
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to add case";
			return false;
		}
		
    	var caseObject = {};
    	caseObject.caseId = cases.caseId; 
    	caseObject.caseComments = cases.caseComment; 
    	caseObject.userId = $scope.userId;
    	
    	var caseComments = $("#caseComment").val();
    	
    	if(!angular.isUndefinedOrNull(caseComment) && caseComments.length > 100){
	   	   isValid = false;
	  	   return false;
   		}else if(angular.isUndefinedOrNull(cases.caseComment) || cases.caseComment.length == 0){
    	   $scope.caseCloseValidated = false;  
    	   $scope.serviceMessage = "Please enter case comments";
    	   isValid = false;
    	   return false;
   		}
   	  
        console.log("cases json " + JSON.stringify(caseObject));
        
        if(isValid){ //if form valid
        //http call
        $http.post(SERVICEADDRESS + '/case/saveComment', JSON.stringify(caseObject)).success(function(returnJson) {
        	console.log("returnJson - "+ JSON.stringify(returnJson));
            if (returnJson.isSuccess) { //on success
            	$scope.isValidated = true;      
            	$scope.caseCloseValidated = true;  
            	$scope.caseonSuccess = true; 
            	 $scope.serviceMessage = "Comments saved successfully";
            	 $scope.commentsList = returnJson.commentsList;
            	 if(!angular.isUndefinedOrNull($scope.commentsList)){
             		if($scope.commentsList.length == 0){
             			$scope.ifNoRecordsFound = true;	
             		}else{
             			$scope.ifNoRecordsFound = false;
             		}
             	}else{
             		$scope.ifNoRecordsFound = true;	
             	}
            	// $scope.$apply();
            	// cases.caseComments = returnJson.caseComments;
             	 cases.caseComment = '';
            	 $timeout(function(){
					$scope.caseonSuccess = false;
			     }, 10000);
            	
            	return true;
            } else {
            	$("html, body").animate({ scrollTop: 0 }, 1000);
            	$scope.isValidated = false;
                $scope.serviceMessage = returnJson.serviceMessage;
                return false;
            }

        }).error(function(returnErrorJson, status, headers, config) {
        	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveCaseCustomerDetails(cases); 
			}

        }); //error
        
       } //if end
    
    }; //end of saveCaseCustomer Details
    $scope.brandchange = function(vehicleBrandId){
    	var enteredBrandName = vehicleBrandId;
   	    if(!angular.isUndefinedOrNull(enteredBrandName)){ //country validation
   		   var indexLoc = $scope.getIndexOfValue($scope.brands,enteredBrandName);
   		   if(indexLoc  >= 0){
   			   $scope.isBrandValid = true;
   		   }else{
   				$scope.isBrandValid = false;
   				return false;
   		   }
   		}
    };
    
    /**
     * save Vehicle Info
     */
   $scope.saveVehicleInfo = function saveVehicleInfo(cases) {
	  $scope.isBrandValid = true;
	   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
		if(!canWrite){
			
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to add vehicle info";
			$("html, body").animate({ scrollTop: 0 }, 1000);
			return false;
		}
    
    var isValid = $scope.tracForm.$valid;
    var savevehicleInfo={};
   
    $scope.errorMessage;
    $scope.isMemberShipEndValid = false;
	$scope.isRegistrationDateValid = false;
	
	var isVinNotValid = $scope.vinValidationCheck(cases.vin);
	
	if(isVinNotValid){
		isValid = false;
		return false;
	}
	
	
	var enteredCountryName = cases.vehcileCountryISO;
	   if(!angular.isUndefinedOrNull(enteredCountryName)){ //country validation
		   var indexLoc = $scope.getIndexOfInt($scope.countryJson,enteredCountryName,"Name");
		   if(indexLoc  >= 0){
			   $scope.isVehicleCountrySelected = false;
		   }else{
			   isValid = false;
				$scope.isVehicleCountrySelected = true;
				return false;
		   }
		}
	  
	   var enteredBrandName = cases.vehicleBrandId;
	   if(!angular.isUndefinedOrNull(enteredBrandName)){ //country validation
		   var indexLoc = $scope.getIndexOfValue($scope.brands,enteredBrandName);
		   if(indexLoc  >= 0){
			   $scope.isBrandValid = true;
		   }else{
			   isValid = false;
			   $scope.isBrandValid = false;
				return false;
		   }
		}
	   
    if(!angular.isUndefinedOrNull(cases) && isValid){ //if form valid and cases object is valid
    	
    	  var enteredModelName = cases.vehicleModelId;
    	var indexLoc= $scope.getIndexOfValue($scope.models,enteredModelName,"vehicleModelName");
    	
    	if(indexLoc  >= 0){ //if selected brand model is valid
    		$scope.isModelValid = true;
    	}else{
    		$scope.isModelValid = false;
    		return false;
    	}
    	
  		if(cases.memberShipEnd<cases.memberShipStart){//date validation
			$scope.isMemberShipEndValid =true;
			$scope.errorMessage = "MemberShip Start Date cannot be before MemberShip End Date.";
			$("html, body").animate({ scrollTop: 0 }, 1000);
			return false;
		}else if(registrationDate<cases.dateOfPurchase){//date validation
			$scope.isRegistrationDateValid = true;
			$scope.errorMessage = "Registration date cannot be before Purchase Date.";
			$("html, body").animate({ scrollTop: 0 }, 1000);
			return false;
		}else{//on sucess
			$scope.errorMessage = ""; 
			$scope.isMemberShipEndValid = false;
			$scope.isRegistrationDateValid = false;
		  
			savevehicleInfo.userId =  $scope.userId;
			savevehicleInfo.customerId = cases.customerId;
			savevehicleInfo.vin = cases.vin;
			savevehicleInfo.vehicleRegisterNo = cases.vehicleRegisterNo;
			savevehicleInfo.countryISO = cases.vehcileCountryISOCode;
			savevehicleInfo.city = cases.city;
			savevehicleInfo.plateCode = cases.plateCode;
			savevehicleInfo.memberShipTypeId = cases.memberShipTypeId;
			savevehicleInfo.vehicleBrandId = cases.selectedBrandId;
			savevehicleInfo.vehicleModelId = cases.selectedModelId;
			savevehicleInfo.vehicleModelYear = cases.vehicleModelYear;
			savevehicleInfo.memberShipStart  = cases.memberShipStart;
	    	savevehicleInfo.memberShipEnd    = cases.memberShipEnd;
	    	savevehicleInfo.dateOfPurchase   = cases.dateOfPurchase;
	    	savevehicleInfo.registrationDate = cases.registrationDate;
			savevehicleInfo.color = cases.color;
			savevehicleInfo.mileage = cases.mileage;
			
			 if($scope.isFromCaseSchedule){
				 savevehicleInfo.isCustomer = $scope.isCustomer;
             }else{
            	 savevehicleInfo.isCustomer = true;
             }
			
			 if(cases.plateCode.length > 0 && cases.vehicleRegisterNo.length > 0)
				 $scope.cases.plateRegNo = cases.plateCode + "," + cases.vehicleRegisterNo;
		        else if(cases.plateCode.length > 0)
		        	$scope.cases.plateRegNo = cases.plateCode;
		        else if(cases.vehicleRegisterNo.length > 0)
		        	$scope.cases.plateRegNo = cases.vehicleRegisterNo;
			
			console.log("savevehicleInfo json " + JSON.stringify(savevehicleInfo));
		
				$http.post(SERVICEADDRESS + '/vehicleInfo/registerVehicleInfoOfCustomer', JSON.stringify(savevehicleInfo)).
		        success(function(returnJson) {
		        	console.log("returnJson - "+ JSON.stringify(returnJson));
		            if (returnJson.isSuccess) { //on service success
		            	$scope.activetab = 2;
		            	$scope.isValidated = true;
		            	$scope.setServiceTime();
		            	$scope.view = '/resources/views/bodytemplates/vehicle_servicelocation_body.html';
		                $scope.cases.vehicleId = returnJson.vehicle.vehicleId;
		                
		                if(returnJson.customerLocationList.length > 0)
		                	$scope.customerLocationList = returnJson.customerLocationList;
		                
				                $scope.isVehcileFound= true;
				                
				                //broadcasting latitude and longitude to angular directives to show icon on map
				            	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.serviceLocationLongitude))
			                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
				  				
				                if(!$scope.isVehicleRegistered){
				                	$scope.cases.priorityId = $scope.prioritys[1];
				                	$scope.cases.hh = 0;
						            $scope.cases.mm = 0;
					                $scope.progressBarIndex = $scope.progressBarIndex + 1;
					            	$scope.progressBarLoading();
					             }
				            	
				            	 $scope.isVehicleRegistered = true;
	                   
				            } else {
				            					               
				            	 $scope.isValidated = false;
				                 $scope.serviceMessage = returnJson.serviceMessage;
				                 $("html, body").animate({ scrollTop: 0 }, 1000);
				            }
				
		            }).error(function(returnErrorJson, status, headers, config) {
		
		        	    var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.saveVehicleInfo(cases); 
						};
		           }); //error
						
				}; //end of if
		
		};// end of UndefinedOrNull
	
    };//end of saveVehicleInfo
    

    $scope.setServiceTime = function setServiceTime(){
    	var today = new Date();
       if(new Date($scope.cases.serviceRequiredTime).getDate()>today.getDate()){
    	   $scope.cases.minhours = 00;
           $scope.cases.minminutes = 00;  
       }else{
	       var hours = today.getHours();
	       var minutes = today.getMinutes();
	       $scope.cases.minhours = hours;
	       $scope.cases.minminutes = minutes;
       }
     };

    /**
     *save Vehicle Service Location details
     */
    $scope.saveVehicleServiceLocation = function saveVehicleServiceLocation(cases) {
    	
    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
 		if(!canWrite){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "You don't have permissions to add vehicle service location";
 			return false;
 		}
 		else if(angular.isUndefinedOrNull(cases.serviceLocationLatitude) || cases.serviceLocationLatitude.length == 0){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "Unable to retrieve Location Latitude.";
 			return false;
 		}
 		else if(angular.isUndefinedOrNull(cases.serviceLocationLongitude) || cases.serviceLocationLongitude.length == 0){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "Unable to retrieve Location Longitude.";
 			return false;
 		}
 		
 		
    	var isValid = $scope.tracForm.$valid;
        var vServiceLocationDetails={};
        
        var enteredCountryName = cases.serviceLocationCountryName;
    	   if(!angular.isUndefinedOrNull(enteredCountryName)){ //country validation
    		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
    		   if(indexLoc  >= 0){
    			   $scope.isSLCountrySelected = false;
    		   }else{
    				$scope.isSLCountrySelected = true;
    				return false;
    		   }
    		}
        $scope.cases.customerName = cases.firstName + " " + cases.lastName;
        
        $scope.cases.brandModelYear = cases.vehicleBrandId + "," + cases.vehicleModelId + "," + new Date(cases.vehicleModelYear).getFullYear();
        
            console.log("cases json " + JSON.stringify(cases));

            if(isValid){
            	if(cases.serviceRequiredTypeId == 2){
	            	scheduleDate = $('#serviceRequiredTime').val().split("/");
	            	scheduleTime =  cases.hh+":"+cases.mm;
	            	var scheduleMonth = parseInt(scheduleDate[1]) - 1;
	            	scheduleMonth = "0"+scheduleMonth;
	            	cases.serviceRequiredTime =  new Date(scheduleDate[0],scheduleMonth,scheduleDate[2], cases.hh,cases.mm,0);
            	}
            	vServiceLocationDetails.userId =  $scope.userId;
            	vServiceLocationDetails.customerId = cases.customerId; 
            	vServiceLocationDetails.vehicleId = cases.vehicleId;   
            	vServiceLocationDetails.customerLocationName = cases.serviceLocationName;         	
            	vServiceLocationDetails.area = cases.serviceLocationArea;
            	vServiceLocationDetails.building = cases.serviceLocationBuilding; 
            	vServiceLocationDetails.landmark = cases.serviceLocationLandmark;
            	vServiceLocationDetails.serviceLocationLatitude = cases.serviceLocationLatitude; 
            	vServiceLocationDetails.serviceLocationLongitude = cases.serviceLocationLongitude;
               	vServiceLocationDetails.customerLocationCity = cases.serviceLocationCity;
            	vServiceLocationDetails.customerLocationCountryCode = cases.serviceLocationCountryCode;
            	vServiceLocationDetails.priorityId = cases.priorityId;
            	vServiceLocationDetails.serviceRequiredTypeId = cases.serviceRequiredTypeId;
            	vServiceLocationDetails.street = cases.serviceLocationStreet;
            	vServiceLocationDetails.serviceRequiredTime = cases.serviceRequiredTime;
            	vServiceLocationDetails.saveForReference = cases.saveForReference;
            	vServiceLocationDetails.poBox = cases.serviceLocationpoBox;
            	//vServiceLocationDetails.isCustomer = true;
            	if($scope.isFromCaseSchedule){
            		vServiceLocationDetails.isCustomer = $scope.isCustomer;
                }else{
                	vServiceLocationDetails.isCustomer = true;
                }
            	
            	console.log("vServiceLocationDetails json " + JSON.stringify(vServiceLocationDetails));
            	
            $http.post(SERVICEADDRESS + '/serviceLocation/saveVehicleServiceLocation', JSON.stringify(vServiceLocationDetails)).            	    		
            success(function(returnJson) {
            	console.log("returnJson - "+ JSON.stringify(returnJson));
                if (returnJson.isSuccess) {
                	$scope.activetab = 3;
                	$scope.isValidated = true;
                	
                	
                	$scope.view = '/resources/views/bodytemplates/main_case_body.html';
                    $scope.cases.caseId = returnJson.location.caseId;
                    $scope.cases.locationId = returnJson.location.locationId;
                    $scope.cases.caseNumber = returnJson.location.caseNumber;
                    $scope.subcasesList = returnJson.subcasesInfo;
                    $scope.cases.isScheduledToday = returnJson.location.isScheduledToday;
                     
                    $scope.screenTitle = "Case No :"+$scope.cases.caseId;
                    
                    if(!$scope.isServiceLocRegistered){
                    	 if($scope.subcasesList.length > 0)
                         	$scope.disabledCaseFaultServicesList($scope.subcasesList);
                         else
                         	$scope.caseFaultServicesList();
                    	$scope.progressBarIndex = $scope.progressBarIndex + 1;
	                	$scope.progressBarLoading();
                    }
                	 $scope.isServiceLocRegistered = true;
                } else {
                	 $scope.isValidated = false;
                    $scope.serviceMessage = returnJson.serviceMessage;
                    $("html, body").animate({ scrollTop: 0 }, 1000);
                }

            }).error(function(returnErrorJson, status, headers, config) {
            	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.saveVehicleInfo(cases); 
    			}
            }); //error
           } // end if isValid
        }; //end of saveVehicleInfo


	/**
	 * Main Case details
	 */

    $scope.saveMainCaseDetails = function saveMainCaseDetails(cases,processing,divChange) {
    	console.log("cases - "+ JSON.stringify(cases));
    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
  		if(!canWrite){
  			$scope.isValidated = false;
  			$scope.serviceMessage = "You don't have permissions to add case details";
  			return false;
  		}
    	 
		 $scope.processing= false;
	        var isValid = $scope.tracForm.$valid;
	        var mainCaseDetails={};
	       var arrayLength = $scope.selection.length;
		       
		       if($scope.isFromCaseSchedule){
		    	  if(angular.isUndefinedOrNull(cases.caseFaultDescription)){
		    		 	   isValid = false;
		    		}else{
		    		   isValid = true;
		    	   }
		    		  
		       }
		       
		        if(isValid){
		        	  
		        	 if(!angular.isUndefinedOrNull(cases.caseFaultId) && cases.caseFaultId != 0 && arrayLength > 0){
		        		 var indexLoc= $scope.getIndexOfFault($scope.faults,cases.caseFaultId,"caseFaultId");
				           var isDisabled = $scope.faults[indexLoc].isDisabled;
				 		   if(indexLoc  >= 0){
				 			  $scope.isFaultValid = true;
					 		  $scope.faults[indexLoc].checked = false;
				 			  $scope.faults[indexLoc].isDisabled = true;
				 		   }else{
				 			  $scope.processing = true;
				 			  $scope.isFaultValid = false;
				 			 return false;
				 		   }
				 		   if(isDisabled){
				 			   return false;
				 		   }
		        	  }else{
		        		  $scope.processing = true;
		        		  $scope.isFaultValid = false;
		        		  return false;
		        	  }
		 		   
		        	   $scope.progressBarIndex = $scope.progressBarIndex + 1;
		        	 	mainCaseDetails.caseId = cases.caseId;		
						mainCaseDetails.userId = $scope.userId;	
						mainCaseDetails.vehicleId = cases.vehicleId;
						mainCaseDetails.caseFaultId = cases.caseFaultId;
						mainCaseDetails.caseFaultDescription = cases.caseFaultDescription;
						mainCaseDetails.caseNotes = cases.caseNotes;
						mainCaseDetails.caseNumber = cases.caseNumber;
									
						console.log("mainCaseDetails json " + JSON.stringify(mainCaseDetails)); 
					
					
		            $http.post(SERVICEADDRESS + '/caseRegistration/saveCaseInfo', JSON.stringify(mainCaseDetails)).            	    		
		            success(function(returnJson) {
		            	console.log("returnJson - "+ JSON.stringify(returnJson));
		            	 $scope.processing = true;
		                if (returnJson.isSuccess) {
		                	  if($scope.isFromCaseSchedule){
					 		      if(cases.caseFaultId == 6 && !$scope.cases.isDestinationLocationSet ){
				   	 					$scope.isTowingRequired = true;
				   	 					$scope.cases.isTowRequested = true;
				   	 				}
		                	  }
		                	  $scope.isValidated = true;      
		                  	$scope.caseCloseValidated = true;  
		                	 $scope.isMainCasegistered = true;
		                	 cases.caseFaultDescription = "";
		                	 cases.caseNotes = "";
		                	 cases.caseFaultId = null;
		                	 $scope.selection.splice(0);
		                	 $scope.subcasesList = returnJson.subcasesInfo;
		                	 if(divChange){
		                		$scope.addAssistanceToMainCase(false,true);
		                	 }
		                	 
		                } else {
		                	$scope.isValidated = false;
		                    $scope.serviceMessage = returnJson.serviceMessage;
		                    $("html, body").animate({ scrollTop: 0 }, 1000);
		                   
		                }
		
		            }).error(function(returnErrorJson, status, headers, config) {
		            	 $scope.processing = true;
		            	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
		    			if(isFreshTokenGenarated){
		    				$scope.saveMainCaseDetails(cases); 
		    			}
		             }); //error
		           } // end if isValid
		        
        }; //end of saveMainCaseDetails
        
        $scope.addAssistanceToMainCase= function addAssistanceToMainCase(canAddAssistance,canUpdateFalts){
        	$scope.canAddAssistance = canAddAssistance;
        	$scope.isServiceValid = true;
        	$scope.isFaultValid  = true;
        	if(canAddAssistance){
	        	for (var k = 0; k < $scope.subcasesList.length; k = k + 1) {
	        		var faultId = $scope.subcasesList[k].caseRegisteredFaultID;
	        		var indexLoc= $scope.getIndexOfFault($scope.faults,faultId,"caseFaultId");
	        		 if(indexLoc  >= 0){
			 			  $scope.faults[indexLoc].isDisabled = true;
			 		 }
	        		 $scope.faults[indexLoc].checked = false;
	    		}
        	}else{
        		 //loading list of case faults and services from DB
        		var indexLoc = -1;
        		if(canUpdateFalts){
	             	for (var i = 0; i < $scope.subcasesList.length; i++) {
	            		var  serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
	            		if(serviceID == 0){
	            			var faultId = $scope.subcasesList[i].caseRegisteredFaultID;
	            			indexLoc = $scope.getIndexOfFault($scope.faults,faultId,"caseFaultId");
		   	        		 if(indexLoc  >= 0){
		   			 			  $scope.faults[indexLoc].checked = true;
		   			 			  $scope.getServicesList(indexLoc, $scope.faults);
		   			 			  return;
		   			 		 }
	            		}
	             	}
	            	if(indexLoc  == -1){
	        			 $scope.faults[0].checked = true;
				 		 $scope.getServicesList(0, $scope.faults);
				 			  return;
	        		}
        		}
	      	}
        	
        };
        
        
        /**
    	 * on clicking new customer on cusotomer details screen
    	 */

        $scope.checkIsExists = function checkIsExists(isChecked){
        	if( (isChecked == 1 && $scope.cases.isExisting == 1) || (isChecked == 0 && $scope.cases.isExisting == 0)){
        		$scope.cases = {}; //customer details
            	$scope.cases.vehicle = -1;
            	$scope.cases.serviceLocationId = -1;
            	if(isChecked){
            		$scope.cases.isExisting = 0;
            	}else{
            		$scope.cases.isExisting = 1;
            	}
    		 	$scope.cases.serviceRequiredTypeId = 1;
            	$scope.vehiclesList.splice(0); //vehicle details
            	$scope.cases.saveForReference = true;
            	$scope.cases.searchParameter = '';
            	$scope.progressBarIndex = 1;
               	$scope.progressBarLoading();
                $scope.isCustomerRegistered = false;
    	        $scope.isVehicleRegistered = false;
    	        $scope.isServiceLocRegistered = false;
    	        $scope.isMainCasegistered = false;
    	        $scope.activetab =0;
                $scope.isSearchParameterValid = false;
                $scope.isValidated = true;
        	}
        };
   


        /**
    	 * get case details on search Parameter
    	 */
        $scope.getAllCaseDetails = function getAllCaseDetails(customerMobileNo,isCustomer){
            
        	var caseDetails={};
        	caseDetails.searchBy = customerMobileNo;
        	caseDetails.isCustomer = isCustomer;
        	
        	console.log("caseDetails json " + JSON.stringify(caseDetails));
        	 if(!angular.isUndefinedOrNull(customerMobileNo) && customerMobileNo.length > 0){
	            $http.post(SERVICEADDRESS + '/case/details', JSON.stringify(caseDetails)).
	            success(function(returnJson) {
	          
	                if (returnJson.isSuccess) {
	                	$scope.progressBarLoadingFormainCaseView(); //load progress bar to 100%
	                	$scope.isSearchParameterValid = false;
	                	$scope.cases = returnJson.customer; //customer details
	                	$scope.cases.customerLocationLatitude = $scope.cases.locationLatitude;
	                	$scope.cases.customerLocationLongitude = $scope.cases.locationLongitude;
	                	$scope.commentsList = returnJson.commentsList;
	                	if(!angular.isUndefinedOrNull($scope.commentsList)){
	                		if($scope.commentsList.length == 0){
	                			$scope.ifNoRecordsFound = true;	
	                		}
	                	}else{
	                		$scope.ifNoRecordsFound = true;	
	                	}
	                	$scope.cases.isExisting = 0;
	                	$scope.vehiclesList = returnJson.vehicles; //vehicle details
	                	
	                	$scope.customerLocationList = returnJson.vehicleservicelocations; //vehicle serice location details

	                	console.log("customerLocationList" + JSON.stringify( $scope.customerLocationList));
	                	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationName)){ //to point location in select box
		                	 var selectedServiceLocationIndex = $scope.getIndexOf($scope.customerLocationList, $scope.cases.serviceLocationName,"locationName");
		                	$scope.cases.serviceLocationId = $scope.customerLocationList[selectedServiceLocationIndex];
	                	}
	                	
	                	if(!angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){ //to point vehcilke registration no in select box
		                	var selectedVehicleIndex = $scope.getIndexOf($scope.vehiclesList, $scope.cases.vehicleRegisterNo,"vehicleRegisterNo");
			                $scope.cases.vehicle = $scope.vehiclesList[selectedVehicleIndex];
	                	}
	                	
	                	$scope.subcasesList =  returnJson.subcasesInfo; //assistance list 
	                	$scope.casestatusList =  returnJson.assistanceStatusList; //list of subcase status
	                	$scope.faults = returnJson.faultservices; //faults
	                	
	                	if(!$scope.isCustomer) //displaying address as area for mobile customer
	                		$scope.cases.serviceLocationArea = $scope.cases.detailAddress;
	                	
	                	$scope.cases.customerName = $scope.cases.firstName + " " + $scope.cases.lastName;
	                	$scope.cases.brandModelYear = $scope.cases.vehicleBrandId + "," + $scope.cases.vehicleModelId + "," + $scope.cases.vehicleModelYear;
	                	
	                    $scope.brandModelList = returnJson.vehicleBrandsList; //brand model list
	                   
		     			    var brandModelsLength = $scope.brandModelList.length;
		     				for(var i=0;i<brandModelsLength;i++){
		     					$scope.brands.push($scope.brandModelList[i].vehicleBrandName);	
		     					$scope.brandId.push($scope.brandModelList[i].vehicleBrandId);
		     				}
	                	 
	                     if(!angular.isUndefinedOrNull($scope.cases.plateCode) && !angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){
	                    	 if($scope.cases.plateCode.length > 0 && $scope.cases.vehicleRegisterNo.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.plateCode + "," + $scope.cases.vehicleRegisterNo;
	                    	 else if($scope.cases.plateCode.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.plateCode;
	                    	 else if($scope.cases.vehicleRegisterNo.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.vehicleRegisterNo;
	                     }else if(!angular.isUndefinedOrNull($scope.cases.plateCode)){
	                     	$scope.cases.plateRegNo = $scope.cases.plateCode;
	                     }else if(!angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){
	                     	$scope.cases.plateRegNo = $scope.cases.vehicleRegisterNo;
	                     }
	                     
	                	console.log("case object" + JSON.stringify( $scope.cases));
	                	
	                	//setting hours and minutes when servie required time is scheduled
	                	if(!angular.isUndefinedOrNull($scope.cases.serviceRequiredTime) && $scope.cases.serviceRequiredType == 2){
			    			var serviceRequiredTime = new Date($scope.cases.serviceRequiredTime);
			    			$scope.cases.hh = serviceRequiredTime.getHours();
			    			$scope.cases.mm = serviceRequiredTime.getMinutes();
			    		}
						
	                	console.log("sub case object" + JSON.stringify( $scope.subcasesList));
	                	
	                	if(!$scope.isFromPreRequsitie){
		                	if($scope.subcasesList.length > 0){
			   	            	 $scope.subCaseId =  $scope.subcasesList[0].subcaseId;
			   	             	 $scope.subCaseFault =  $scope.subcasesList[0].faultName;
			   	             	 $scope.subCaseFaultId =  $scope.subcasesList[0].caseRegisteredFaultID;
			   	                 $scope.subCaseServiceName =  $scope.subcasesList[0].FaultServiceName;
			   	                 $scope.isCaseClosedByDifferentIssue = $scope.subcasesList[0].isCaseClosedByDifferentIssue;
			   	             	 $scope.selectedSubCaseIndex = 0;
			   	             	
			   	             	 //loading list of case faults and services from DB
			   	             	for (var i = 0; i < $scope.subcasesList.length; i++) {
			   	             		var isCaseClosedByDifferentIssue = $scope.subcasesList[i].isCaseClosedByDifferentIssue;
		     	            		var  serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
		     	            		var faultId = $scope.subcasesList[i].faultId;
		     	            		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6){
		     	            			if(!isCaseClosedByDifferentIssue){
			     	            			$scope.isTowingRequired = true;
			     	            			if(!$scope.cases.isTowDetailsUpdated)
			     	            				$scope.cases.isTowRequested = true;
		     	            			}
		     	            		}
		     	            		if(serviceID >=  1){
		     	            			$scope.selectedSubCaseIndex++;
		     	            		}
		                		}
			   	             	 //$scope.destinationCheck($scope.subcasesList);
			   	             	
			   	             	if( $scope.selectedSubCaseIndex == $scope.subcasesList.length){
			   	             		$scope.selectedSubCaseIndex = -1;
			   	             	}
			   	             	 
			   	             	 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
			   	 				 $scope.faults[$scope.selectedFaultIndex].checked = true;
			   	 				
			   	 				 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
			   	             }
	                	}else{
	                		if(!$scope.cases.isDestinationLocationSet){
		                		for (var i = 0; i < $scope.subcasesList.length; i++) {
		     	            		$scope.subCaseFault =  $scope.subcasesList[i].faultName;
		     	            		var  serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
		     	            		var faultId =  $scope.subcasesList[i].faultId;
		     	            		 var selectedSubCaseIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
		     	            		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6){
		     	            			
		     	            			if(!$scope.cases.isTowDetailsUpdated){
		     	            				$scope.cases.isTowRequested = true;
		     	            				$scope.isTowingRequired = true;
		     	            			}else{
		     	            				$scope.cases.isTowRequested = false;
		     	            				$scope.isTowingRequired = false;
		     	            			}
		     	            		}
		     	            		 $scope.faults[selectedSubCaseIndex].isDisabled = true;
		                		}
	                		}else{
	                			$scope.cases.isTowRequested = false;
 	            				$scope.isTowingRequired = false;
	                		}
     	            	 
	                	}
	                	
	                	$scope.scrollBottom();//to scroll bottom to view assistances
	                	$scope.setVehicleBrandModelData($scope.cases); //set vehicle brand and model i their respective fields
	                	$scope.addAssistanceToMainCase(false,false); 
	                	
	                	//To point icon in map
	                	if(!angular.isUndefinedOrNull($scope.cases.locationLatitude) && !angular.isUndefinedOrNull($scope.cases.locationLongitude))
	                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.locationLatitude,"locationLongitude":$scope.cases.locationLongitude});
	                	
	                	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.serviceLocationLongitude))
	                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
	                	$scope.screenTitle = "Case No :"+$scope.cases.caseId;
	          		}return false;
	            }).error(function(returnErrorJson, status, headers, config) {
	            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	    			if(isFreshTokenGenarated){
	    				$scope.getCaseDetails(cases); 
	    			}
	               
	
	            }); //error
        	 }
        };
        
        /**
    	 * get case details on search Parameter
    	 */
        $scope.getCaseDetailsForView = function getCaseDetailsForView(caseId,isCustomer){
            
        	var caseDetails={};
        	caseDetails.searchBy = caseId;
        	caseDetails.isCustomer = isCustomer;
        	
        	console.log("caseDetails json " + JSON.stringify(caseDetails));
        	 if(!angular.isUndefinedOrNull(caseId) && caseId.length > 0){
	            $http.post(SERVICEADDRESS + '/case/detailsbyid', JSON.stringify(caseDetails)).
	            success(function(returnJson) {
	          
	                if (returnJson.isSuccess) {
	                	$scope.screenTitle = "Case No :"+caseId;
	                	$scope.commentsList = returnJson.commentsList;
	                	$scope.brandModelList = returnJson.vehicleBrandsList;
	                	if(!angular.isUndefinedOrNull($scope.commentsList)){
	                		if($scope.commentsList.length == 0){
	                			$scope.ifNoRecordsFound = true;	
	                		}
	                	}else{
	                		$scope.ifNoRecordsFound = true;	
	                	}
	                	$scope.progressBarLoadingFormainCaseView();
	                	$scope.isSearchParameterValid = false;
	                	$scope.cases = returnJson.customer; //customer details
	                	$scope.cases.customerLocationLatitude = $scope.cases.locationLatitude;
	                	$scope.cases.customerLocationLongitude = $scope.cases.locationLongitude;
	                	$scope.cases.isExisting = 0;
	                	
	                	console.log("customerLocationList" + JSON.stringify( $scope.customerLocationList));
	                	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationName)){
		                	 var selectedServiceLocationIndex = $scope.getIndexOf($scope.customerLocationList, $scope.cases.serviceLocationName,"locationName");
		                	$scope.cases.serviceLocationId = $scope.customerLocationList[selectedServiceLocationIndex];
	                	}
	                	
	                	if(!angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){
		                	var selectedVehicleIndex = $scope.getIndexOf($scope.vehiclesList, $scope.cases.vehicleRegisterNo,"vehicleRegisterNo");
			                $scope.cases.vehicle = $scope.vehiclesList[selectedVehicleIndex];
	                	}
	                	$scope.subcasesList =  returnJson.subcasesInfo; 
	                	$scope.faults = returnJson.faultservices;
	                	
	                	if(!$scope.isCustomer) //displaying address as area for mobile customer
	                		$scope.cases.serviceLocationArea = $scope.cases.detailAddress;
	                	
	                	$scope.cases.customerName = $scope.cases.firstName + " " + $scope.cases.lastName;
	                	$scope.cases.brandModelYear = $scope.cases.vehicleBrandId + "," + $scope.cases.vehicleModelId + "," + $scope.cases.vehicleModelYear;
	                	
	                   
	                     if(!angular.isUndefinedOrNull($scope.cases.plateCode) && !angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){
	                    	 if($scope.cases.plateCode.length > 0 && $scope.cases.vehicleRegisterNo.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.plateCode + "," + $scope.cases.vehicleRegisterNo;
	                    	 else if($scope.cases.plateCode.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.plateCode;
	                    	 else if($scope.cases.vehicleRegisterNo.length > 0)
	                    		 $scope.cases.plateRegNo = $scope.cases.vehicleRegisterNo;
	                     }else if(!angular.isUndefinedOrNull($scope.cases.plateCode)){
	                     	$scope.cases.plateRegNo = $scope.cases.plateCode;
	                     }else if(!angular.isUndefinedOrNull($scope.cases.vehicleRegisterNo)){
	                     	$scope.cases.plateRegNo = $scope.cases.vehicleRegisterNo;
	                     }
	                     
	                	console.log("case object" + JSON.stringify( $scope.cases));
	                	
							  
	                	if(!angular.isUndefinedOrNull($scope.cases.serviceRequiredTime) && $scope.cases.serviceRequiredType == 2){
			    			var serviceRequiredTime = new Date($scope.cases.serviceRequiredTime);
			    			$scope.cases.hh = serviceRequiredTime.getHours();
			    			$scope.cases.mm = serviceRequiredTime.getMinutes();
			    		}
						
	                	console.log("sub case object" + JSON.stringify( $scope.subcasesList));
	                
		                	if($scope.subcasesList.length > 0){
			   	            	 $scope.subCaseId =  $scope.subcasesList[0].subcaseId;
			   	             	 $scope.subCaseFault =  $scope.subcasesList[0].faultName;
			   	             	 $scope.subCaseFaultId =  $scope.subcasesList[0].caseRegisteredFaultID;
			   	              	 
			   	             	 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
			   	 				 $scope.faults[$scope.selectedFaultIndex].checked = true;
			   	 				 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
			   	             }
		                	$scope.scrollBottom();//to scroll bottom to view assistances
		                	$scope.setVehicleBrandModelData($scope.cases);
		                	$scope.addAssistanceToMainCase(false,false);
		                	
		                	if(!angular.isUndefinedOrNull($scope.cases.locationLatitude) && !angular.isUndefinedOrNull($scope.cases.locationLongitude))
		                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.locationLatitude,"locationLongitude":$scope.cases.locationLongitude});
		                	
		                	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.serviceLocationLongitude))
		                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
	                	}
	                	
	                	
	                	
	          		
	            }).error(function(returnErrorJson, status, headers, config) {
	            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	    			if(isFreshTokenGenarated){
	    				$scope.getCaseDetails(cases); 
	    			}
	               
	
	            }); //error
        	 }
        };
        
        $scope.destinationCheck = function destinationCheck(subCaseEntities){
        
	        //loading list of case faults and services from DB
	        	for (var i = 0; i < subCaseEntities.length; i++) {
	        		var isCaseClosedByDifferentIssue = subCaseEntities[i].isCaseClosedByDifferentIssue;
	     		var  serviceID =  subCaseEntities[i].caseRegisteredServiceID;
	     		var faultId = subCaseEntities[i].faultId;
	     		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6){
	     			if(!isCaseClosedByDifferentIssue){
	         			$scope.isTowingRequired = true;
	         			if(!$scope.cases.isTowDetailsUpdated)
	         				$scope.cases.isTowRequested = true;
	     			}
	     		}
	     		if(serviceID >=  1){
	     			$scope.selectedSubCaseIndex++;
	     		}
			}
	        	 
        };
        
        /**
    	 * get case details on search Parameter
    	 */
        $scope.getCaseDetails = function getCaseDetails(searchParameter,isFromSchedule){
            
        	var caseDetails={};
            
        	if(!isFromSchedule){ 
        		caseDetails.searchBy = searchParameter.split(',')[0];
        	}else{
		 		var customerMobileNo = casedDetailsObject.get('customerMobileNo');
	     		casedDetailsObject.store('customerMobileNo','');
	     		caseDetails.searchBy = customerMobileNo;
        	}
         	if((angular.isUndefinedOrNull(searchParameter) || searchParameter.length == 0) && !isFromSchedule){
            	$scope.isSearchParameterValid = true;
            	$scope.serviceMessage= "Please enter customer id / Mobile number";
            	return false;
            }
            
            console.log("caseDetails json " + JSON.stringify(caseDetails));
          
            $http.post(SERVICEADDRESS + '/contactDetailsOfCustomer/fetchCustomerContactDetails', JSON.stringify(caseDetails)).
            success(function(returnJson) {
            
                if (returnJson.isSuccess) {
                	$scope.isSearchParameterValid = false;
                	$scope.cases = returnJson.customer; //customer details
                	$scope.cases.customerLocationLatitude = $scope.cases.locationLatitude;
                	$scope.cases.customerLocationLongitude = $scope.cases.locationLongitude;
                	$scope.cases.vehicle = -1;
                	$scope.cases.serviceLocationId = -1;
                	$scope.cases.isExisting = 1;
        		 	
                	$scope.vehiclesList = returnJson.vehicles; //vehicle details
                	if(!isFromSchedule){
                		$scope.cases.serviceRequiredTypeId = 1;
	                	$scope.cases.isExisting = 0;
	                	$scope.cases.saveForReference = true;
	                	$scope.cases.searchParameter = caseDetails.searchBy;
                	}
                	if(!angular.isUndefinedOrNull($scope.cases.locationLatitude) && !angular.isUndefinedOrNull($scope.cases.locationLongitude))
                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.locationLatitude,"locationLongitude":$scope.cases.locationLongitude});
          		}else{
          			$scope.cases = {}; //customer details
          			$scope.cases.searchParameter = caseDetails.searchBy;
                	$scope.cases.vehicle = -1;
                	$scope.cases.isExisting = 0;
                	$scope.cases.serviceLocationId = -1;
                	$scope.cases.serviceRequiredTypeId = 1;
                	$scope.vehiclesList.splice(0); //vehicle details
                	$scope.cases.saveForReference = true;
                	$scope.isSearchParameterValid =  true;
          			$scope.serviceMessage= returnJson.serviceMessage;
      			}
                return false;
            }).error(function(returnErrorJson, status, headers, config) {
            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.getCaseDetails(cases,isFromSchedule); 
    			}
            }); //error
        }; //end of editCaseCustomer Details
        
        $scope.isLengthValid =function isLengthValid(value){
        	console.log("value.length - "+value.length);
        	if(value.length > 5){
        		return true;
        	}else{
        		if(!angular.isUndefinedOrNull($scope.searchList)){
        			$scope.searchList.splice(0);
        		}
        		return false;
        	}
        };
        
        $scope.isVinLengthValid =function isVinLengthValid(value){
        	console.log("value.length - "+value.length);
        	if(value.length > 3){
        		return true;
        	}else{
        		if(!angular.isUndefinedOrNull($scope.searchVehicleList)){
        			$scope.searchVehicleList.splice(0);
        		}
        		return false;
        	}
        };
        
        /**
    	 * search customer details on search Parameter
    	 */
       
        $scope.searchCustomerDetails = function searchCustomerDetails(searchParameter){
        	
        	var searchDetails={};
        	searchDetails.searchBy = searchParameter;
        	
        	 var deferred = $q.defer();
        	 console.log("caseDetails json " + JSON.stringify(searchDetails));
            
            if(!angular.isUndefinedOrNull(searchParameter)  && searchParameter.length > 2){
	           return $http.post(SERVICEADDRESS + '/customer/search', JSON.stringify(searchDetails)).
	            success(function(returnJson) {
	            
	                if (returnJson.isSuccess) {
	                	$scope.searchList.splice(0);
	                	console.log("returnJson.searchList - "+ JSON.stringify(returnJson.searchList));
	                	$scope.searchList = returnJson.searchList;
	          		}else{
	          			$scope.searchList.splice(0);
	          		}
	            }).error(function(returnErrorJson, status, headers, config) {
	            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	    			if(isFreshTokenGenarated){
	    				$scope.searchCustomerDetails(cases); 
	    			}
	            }); //error
           }else{
        	   return deferred.promise;
           }
        }; //end of searchCustomerDetails 
        
        /**
    	 * search vehicle details on search Parameter
    	 */
       
        $scope.searchVehicleDetails = function searchVehicleDetails(searchParameter){
        	
        	var searchDetails={};
        	searchDetails.searchBy = searchParameter;
        	searchDetails.customerId = $scope.cases.customerId;
        	
        	 var deferred = $q.defer();
        	 console.log("caseDetails json " + JSON.stringify(searchDetails));
            
            if(!angular.isUndefinedOrNull(searchParameter)  && searchParameter.length > 4){
	           return $http.post(SERVICEADDRESS + '/vehicle/search', JSON.stringify(searchDetails)).
	            success(function(returnJson) {
	            
	                if (returnJson.isSuccess) {
	                	$scope.searchVehicleList.splice(0);
	                	console.log("returnJson.searchVehicleList - "+ JSON.stringify(returnJson.searchVehicleList));
	                	$scope.searchVehicleList = returnJson.searchVehicleList;
	          		}else{
	          			$scope.searchVehicleList.splice(0);
	          		}
	            }).error(function(returnErrorJson, status, headers, config) {
	            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	    			if(isFreshTokenGenarated){
	    				$scope.searchCustomerDetails(cases); 
	    			}
	            }); //error
           }else{
        	   return deferred.promise;
           }
        }; //end of searchCustomerDetails 
        
        /**
    	 * get case details by case id
    	 */
        	
        $scope.getCaseDetailsById = function getCaseDetailsById(cases){
        	var caseDetails={};
            caseDetails.searchBy = cases.searchParameter;
          
            console.log("caseDetails json " + JSON.stringify(caseDetails));
          
            $http.post(SERVICEADDRESS + '/contactDetailsOfCustomer/fetchCustomerContactDetails', JSON.stringify(caseDetails)).
            success(function(returnJson) {
            console.log("returnJson - "+JSON.stringify(returnJson));
                if (returnJson.isSuccess) {
                	$scope.cases = returnJson.customer; //customer details
                }
            }).error(function(returnErrorJson, status, headers, config) {
            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.getCaseDetailsById(cases); 
    			}
               

            }); //error
        };
    
    /**
     * Edit Case Customer Details
     */
    
    $scope.editCustomerDetails = function editCustomerDetails(cases){
    	
    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
		if(!canWrite){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to upadte case details";
			return false;
		}
       
    	var isValid = $scope.tracForm.$valid;
    	var editCustomerDetails={};
    	
    	 if(angular.isUndefinedOrNull(cases.customerId)){
    		 $("html, body").animate({ scrollTop: 0 }, 1000);
 	   		$scope.isValidated = false;
 	        $scope.serviceMessage = "Invalid selection of customer option";
 	        return false;
    	}
        
    	editCustomerDetails.customerId = cases.customerId;
        editCustomerDetails.firstName = cases.firstName;
        editCustomerDetails.lastName = cases.lastName;
        editCustomerDetails.mobileNumber = cases.mobileNumber;
        editCustomerDetails.emailId = cases.emailId;
        editCustomerDetails.customerLocationName = cases.customerLocationName;
        editCustomerDetails.area = cases.area;
        editCustomerDetails.street = cases.street;
        editCustomerDetails.building = cases.building;
        editCustomerDetails.landmark = cases.landmark;
        editCustomerDetails.customerLocationCity = cases.customerLocationCity;
        editCustomerDetails.poBox = cases.poBox;
        editCustomerDetails.customerLocationCountryCode = cases.customerLocationCountryCode;
        editCustomerDetails.customerLocationLatitude = cases.customerLocationLatitude;
        editCustomerDetails.customerLocationLongitude = cases.customerLocationLongitude;
        editCustomerDetails.updatedBy = $scope.userId;
        editCustomerDetails.locationId = cases.customerLocationId;
        editCustomerDetails.isCustomer = $scope.isCustomer;
        
        console.log("editCustomerDetails json " + JSON.stringify(editCustomerDetails));
        
        if(isValid){
        	
        $http.post(SERVICEADDRESS + '/contactDetailsOfCustomer/editCustomerContactDetails', JSON.stringify(editCustomerDetails)).
        success(function(returnJson) {
        	console.log("returnJson - "+ JSON.stringify(returnJson));
            if (returnJson.isSuccess) {
            	$scope.isValidated = true;      
            	$scope.view = '/resources/views/bodytemplates/vehicle_info_body.html';
             	if(!cases.isExisting && !$scope.isCustomerRegistered){
             		$scope.activetab = 1;
	                $scope.progressBarIndex = $scope.progressBarIndex + 1;
	            	$scope.progressBarLoading();
             	}
             	$scope.isCustomerRegistered = true;
            } else {
            	$scope.isValidated = false;
                $scope.serviceMessage = returnJson.serviceMessage;
                $("html, body").animate({ scrollTop: 0 }, 1000);
            }

        }).error(function(returnErrorJson, status, headers, config) {
        	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.editCustomerDetails(cases); 
			}
       }); //error
        
       } //if end
    
    }; //end of editCaseCustomer Details
    
    /**
     * Edit Vehicle Info
     */
    $scope.editVehicleInfo = function editVehicleInfo(cases) {
    	$scope.isBrandValid = true;
    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
 		if(!canWrite){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "You don't have permissions to upadte vehicle info details";
 			return false;
 		}
 		
	    var isValid = $scope.tracForm.$valid;
	    var editVehicleInfo={};
	   
	    $scope.errorMessage;
	    $scope.isMemberShipEndValid = false;
		$scope.isRegistrationDateValid = false;
		
		var isVinNotValid = $scope.vinValidationCheck(cases.vin);
		
		if(isVinNotValid){
			return false;
		}
		
		var enteredCountryName = cases.vehcileCountryISO;
		   if(!angular.isUndefinedOrNull(enteredCountryName) && $scope.isCustomer){
			   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
			   if(indexLoc  >= 0){
				   $scope.isVehicleCountrySelected = false;
			   }else{
				   isValid = false;
					$scope.isVehicleCountrySelected = true;
					return false;
			   }
			}
	    
		  
		   var enteredBrandName = cases.vehicleBrandId;
		   if(!angular.isUndefinedOrNull(enteredBrandName)){ //country validation
			   var indexLoc = $scope.getIndexOfValue($scope.brands,enteredBrandName);
			   if(indexLoc  >= 0){
				   $scope.isBrandValid = true;
			   }else{
				   isValid = false;
					$scope.isBrandValid = false;
					return false;
			   }
			}
		   
		  if(angular.isUndefinedOrNull(cases.vehicleId)){
		   		$scope.isValidated = false;
		   		isValid = false;
		        $scope.serviceMessage = "Invalid selection of vehicle";
		        $("html, body").animate({ scrollTop: 0 }, 1000);
		        return false;
		  }
		   
	    if(!angular.isUndefinedOrNull(cases) && isValid){
	    	
/*	    	var indexLoc= $scope.getIndexOfInt($scope.brandModelList[$scope.cases.selectedBrandIndex].brandModels,cases.selectedModelId,"vehicleModelId");
	    	
	    	if(indexLoc  < 0){
	    		$scope.isModelValid = false;
	    		return false;
	    	}else{
	    		$scope.isModelValid = true;
	    	}
			*/
	    	  var enteredModelName = cases.vehicleModelId;
	      	var indexLoc= $scope.getIndexOfValue($scope.models,enteredModelName,"vehicleModelName");
	      	
	      	if(indexLoc  >= 0){ //if selected brand model is valid
	      		$scope.isModelValid = true;
	      	}else{
	      		$scope.isModelValid = false;
	      		return false;
	      	}
			if(cases.memberShipEnd<cases.memberShipStart && $scope.isCustomer)
			{
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.isMemberShipEndValid =true;
				$scope.errorMessage = "MemberShip Start Date cannot be before MemberShip End Date.";
			}
			
			else if(cases.registrationDate<cases.dateOfPurchase && $scope.isCustomer)
			{
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.isRegistrationDateValid = true;
				$scope.errorMessage = "Registration date cannot be before Purchase Date.";
			}
			else{
			
				$scope.errorMessage = ""; 
				$scope.isMemberShipEndValid = false;
				$scope.isRegistrationDateValid = false;
			  
				editVehicleInfo.vehicleId = cases.vehicleId;
				editVehicleInfo.userId =  $scope.userId;
				editVehicleInfo.customerId = cases.customerId;
				editVehicleInfo.vin = cases.vin;
				editVehicleInfo.vehicleRegisterNo = cases.vehicleRegisterNo;
				editVehicleInfo.countryISO = cases.vehcileCountryISOCode;
				editVehicleInfo.city = cases.city;
				editVehicleInfo.plateCode = cases.plateCode;
				editVehicleInfo.memberShipTypeId = cases.memberShipTypeId;
				editVehicleInfo.vehicleBrandId = cases.selectedBrandId;
				editVehicleInfo.vehicleModelId = cases.selectedModelId;
				editVehicleInfo.vehicleModelYear = cases.vehicleModelYear;
				editVehicleInfo.memberShipStart  = cases.memberShipStart;
				editVehicleInfo.memberShipEnd    = cases.memberShipEnd;
		    	editVehicleInfo.dateOfPurchase   = cases.dateOfPurchase;
		    	editVehicleInfo.registrationDate = cases.registrationDate;
		    	editVehicleInfo.color = cases.color;
		    	editVehicleInfo.mileage = cases.mileage;
		    	
		    	if(cases.plateCode.length > 0 && cases.vehicleRegisterNo.length > 0)
					 $scope.cases.plateRegNo = cases.plateCode + "," + cases.vehicleRegisterNo;
			     else if(cases.plateCode.length > 0)
			       	$scope.cases.plateRegNo = cases.plateCode;
			     else if(cases.vehicleRegisterNo.length > 0)
			       	$scope.cases.plateRegNo = cases.vehicleRegisterNo;
				
					
				$http.post(SERVICEADDRESS + '/vehicleInfo/editVehilcIeInfo', JSON.stringify(editVehicleInfo)).
			        success(function(returnJson) {
			          if (returnJson.isSuccess) {
			            	$scope.activetab = 2;
			            	$scope.isSearchParameterValid = false;
			            	$scope.isValidated = true;
			            	$scope.setServiceTime();
			            	$scope.view = '/resources/views/bodytemplates/vehicle_servicelocation_body.html';
			                $scope.cases.vehicleId = returnJson.vehicle.vehicleId;
			                
			                if(!$scope.isFromCaseSchedule){
			                	$scope.customerLocationList = returnJson.customerLocationList;
			                }
			                
			            	if(!angular.isUndefinedOrNull($scope.cases.serviceLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.serviceLocationLongitude))
		                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
	
			                
			                if(!cases.isExisting &&  !$scope.isVehicleRegistered){
			                	
			                    $scope.progressBarIndex = $scope.progressBarIndex + 1;
			                	$scope.progressBarLoading();
			                }
			                
			                $scope.isVehicleRegistered = true;
		                   
			            } else {
			            	$scope.isSearchParameterValid = false;
			                $scope.serviceMessage = "No customer details found.";
			                $("html, body").animate({ scrollTop: 0 }, 1000);
			            }
			
			            }).error(function(returnErrorJson, status, headers, config) {
			
			        	 var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.editVehicleInfo(cases); 
							};
			           }); //error
							
					}; //end of if
			
			};// end of UndefinedOrNull
	
    };//end of saveVehicleInfo
    
    
  
   	
    /**
     * Edit Vehicle Service Location details
     */
	   	    	
    $scope.editVehicleServiceLocation = function editVehicleServiceLocation(cases) {
    	
    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
  		if(!canWrite){
  			$("html, body").animate({ scrollTop: 0}, 1000);
  			$scope.isValidated = false;
  			$scope.serviceMessage = "You don't have permissions to update vehicle service location details";
  			return false;
  		}else if(angular.isUndefinedOrNull(cases.serviceLocationLatitude) || cases.serviceLocationLatitude.length == 0){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "Unable to retrieve Location Latitude.";
 			return false;
 		}
 		else if(angular.isUndefinedOrNull(cases.serviceLocationLongitude) || cases.serviceLocationLongitude.length == 0){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "Unable to retrieve Location Longitude.";
 			return false;
 		}
    	
    	var isValid = $scope.tracForm.$valid;
        var vServiceLocationDetails={};
        var scheduleDate;
        var scheduleTime;
        var scheduledDateTime;
        
        var enteredCountryName = cases.serviceLocationCountryName;
    	   if(!angular.isUndefinedOrNull(enteredCountryName)){
    		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
    		   if(indexLoc  >= 0){
    			   $scope.isSLCountrySelected = false;
    		   }else{
    				$scope.isSLCountrySelected = true;
    				return false;
    		   }
    		}
        $scope.cases.customerName = cases.firstName + " " + cases.lastName;
        
        $scope.cases.brandModelYear = cases.vehicleBrandId + "," + cases.vehicleModelId + "," + new Date(cases.vehicleModelYear).getFullYear();
        
        if(cases.plateCode.length > 0 && cases.vehicleRegisterNo.length > 0)
        	$scope.cases.plateRegNo = cases.plateCode + "," + cases.vehicleRegisterNo;
        else if(cases.plateCode.length > 0)
        	$scope.cases.plateRegNo = cases.plateCode;
        else if(cases.vehicleRegisterNo.length > 0)
        	$scope.cases.plateRegNo = cases.vehicleRegisterNo;
        
            console.log("cases json " + JSON.stringify(cases));

            if(isValid){
            	if(cases.serviceRequiredTypeId == 2){
            	
	            	scheduleDate = $('#serviceRequiredTime').val().split("/");
	            	scheduleTime =  cases.hh+":"+cases.mm;
	            	var scheduleMonth = parseInt(scheduleDate[1]) - 1;
	            	scheduleMonth = "0"+scheduleMonth;
	            	cases.serviceRequiredTime =  new Date(scheduleDate[0],scheduleMonth,scheduleDate[2], cases.hh,cases.mm,0);
            	}

            	vServiceLocationDetails.userId =  $scope.userId;
            	vServiceLocationDetails.customerId = cases.customerId; 
            	vServiceLocationDetails.vehicleId = cases.vehicleId; 
            	vServiceLocationDetails.locationId = cases.locationId;
            	vServiceLocationDetails.customerLocationName = cases.serviceLocationName;         	
            	vServiceLocationDetails.area = cases.serviceLocationArea;
            	vServiceLocationDetails.building = cases.serviceLocationBuilding; 
            	vServiceLocationDetails.landmark = cases.serviceLocationLandmark;
            	vServiceLocationDetails.customerLocationCity = cases.serviceLocationCity;
            	vServiceLocationDetails.customerLocationCountryCode = cases.serviceLocationCountryCode;
            	vServiceLocationDetails.priorityId = cases.priorityId;
            	vServiceLocationDetails.serviceRequiredTypeId = cases.serviceRequiredTypeId;
            	vServiceLocationDetails.street = cases.serviceLocationStreet;
            	vServiceLocationDetails.serviceRequiredTime = cases.serviceRequiredTime;
            	vServiceLocationDetails.saveForReference = cases.saveForReference;
            	vServiceLocationDetails.poBox = cases.serviceLocationpoBox;
            	vServiceLocationDetails.serviceLocationLatitude = cases.serviceLocationLatitude; 
            	vServiceLocationDetails.serviceLocationLongitude = cases.serviceLocationLongitude;
            	vServiceLocationDetails.isCustomer = $scope.isCustomer;
            	console.log("vServiceLocationDetails json " + JSON.stringify(vServiceLocationDetails));
            	
            $http.post(SERVICEADDRESS + '/serviceLocation/editVehicleServiceLocation', JSON.stringify(vServiceLocationDetails)).            	    		
            success(function(returnJson) {
            	console.log("returnJson - "+ JSON.stringify(returnJson));
                if (returnJson.isSuccess) {
                	
                	$scope.activetab = 3;
                	$scope.isValidated = true;
                	if(!$scope.isFromCaseSchedule){
                		  $scope.cases.caseId = returnJson.location.caseId;
                          $scope.cases.caseNumber = returnJson.location.caseNumber;
                          $scope.subcasesList= returnJson.subcasesInfo;
                          $scope.cases.isScheduledToday = returnJson.location.isScheduledToday;
                          if(!cases.isExisting && !$scope.isServiceLocRegistered){
                        	  if($scope.subcasesList.length > 0)
                              	$scope.disabledCaseFaultServicesList($scope.subcasesList);
                              else
                              	$scope.caseFaultServicesList();
                        	  
    	                    $scope.progressBarIndex = $scope.progressBarIndex + 1;
    	                	$scope.progressBarLoading();
                        }
                        $scope.isServiceLocRegistered = true;
                		$scope.view = '/resources/views/bodytemplates/main_case_body.html';
                		
                	}else{
                		$scope.view = '/resources/views/bodytemplates/case_info_body.html';
                	}
                	$scope.screenTitle = "Case No :"+$scope.cases.caseId;
                   
                } else {
                	$("html, body").animate({ scrollTop: 0 }, 1000);
                	 $scope.isValidated = false;
                    $scope.serviceMessage = returnJson.serviceMessage;
               }

            }).error(function(returnErrorJson, status, headers, config) {
            	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.saveVehicleInfo(cases); 
    			}
             }); //error
           } // end if isValid
        }; //end of savevehcielLocationdetails

        
        /**
         * save Vehicle Destination Location
         */
        $scope.saveVehicleDestinationLocation = function saveVehicleDestinationLocation(cases) {
        	
        	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
       		if(!canWrite){
       			$scope.isValidated = false;
       			$scope.serviceMessage = "You don't have permissions to add vehicle destination location details";
       			return false;
       		}
       		
        	var destinationLocObject = {};
        	destinationLocObject.userId =  $scope.userId;
        	destinationLocObject.customerId =  $scope.cases.customerId;
        	destinationLocObject.customerLocationName = cases.destinationLocationName;
        	destinationLocObject.area= cases.destinationArea;
        	destinationLocObject.street = cases.destinationStreet;
        	destinationLocObject.building = cases.destinationBuilding;
        	destinationLocObject.landmark = cases.destinationLandmark;
        	destinationLocObject.customerLocationCity = cases.destinationLocationCity;
        	destinationLocObject.poBox= cases.destinationPoBox;
        	destinationLocObject.customerLocationCountryCode= cases.destinationLocationCountryCode;
        	destinationLocObject.serviceLocationLatitude = cases.destLocLatitude;
        	destinationLocObject.serviceLocationLongitude = cases.destLocLongitude;
        	destinationLocObject.isDestinationLocation = true;
        	destinationLocObject.isCustomer = $scope.isCustomer;
        	
        	 var isValid = $scope.tracForm.$valid;
            
        	 var enteredCountryName = cases.countryName;
       	   if(!angular.isUndefinedOrNull(enteredCountryName)){
       		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
       		   if(indexLoc  >= 0){
       			   $scope.isCountrySelected = false;
       		   }else{
       				$scope.isCountrySelected = true;
       				return false;
       		   }
       		}
           console.log("cases json " + JSON.stringify(destinationLocObject));
            
           if(isValid){
            	
            $http.post(SERVICEADDRESS + '/serviceLocation/saveVehicleServiceLocation', JSON.stringify(destinationLocObject)).success(function(returnJson) {
            	console.log("returnJson - "+ JSON.stringify(returnJson));
                if (returnJson.isSuccess) {
                	$scope.isValidated = true;      
                	$scope.caseCloseValidated = true;
                	$scope.cases.isDestinationLocationSet  = true;
                	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
                	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
                } else {
                	$("html, body").animate({ scrollTop: 0 }, 1000);
                	$scope.isValidated = false;
                    $scope.serviceMessage = returnJson.serviceMessage;
                }

            }).error(function(returnErrorJson, status, headers, config) {
            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.saveCaseCustomerDetails(cases); 
    			}
            }); //error
            
           } //if end
        
        }; //end of saveCaseCustomer Details
        
        
        /**
         * Edit Vehicle destination Location details
         */
    	   	    	
        $scope.editDestinationLocation = function editDestinationLocation(cases) {
        	
        	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
        		if(!canWrite){
        			$("html, body").animate({ scrollTop: 0 }, 1000);
        			$scope.isValidated = false;
        			$scope.serviceMessage = "You don't have permissions to update vehicle destination location details";
        			return false;
        		}
        		
        	var isValid = $scope.tracForm.$valid;
            var destLocationDetails={};
           
            var enteredCountryName = cases.destinationLocationCountryName;
        	   if(!angular.isUndefinedOrNull(enteredCountryName)){
        		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
        		   if(indexLoc  >= 0){
        			   $scope.isCountrySelected = false;
        		   }else{
        				$scope.isCountrySelected = true;
        				return false;
        		   }
        		}
         
                console.log("cases json " + JSON.stringify(cases));

                if(isValid){
               	    
                	destLocationDetails.userId =  $scope.userId;
                	destLocationDetails.customerId =  cases.customerId;
                	destLocationDetails.locationId = cases.destinationlocationId;
                	destLocationDetails.customerLocationName = cases.destinationLocationName;         	
                	destLocationDetails.area = cases.destinationArea;
                	destLocationDetails.building = cases.destinationBuilding; 
                	destLocationDetails.landmark = cases.destinationLandmark;
                	destLocationDetails.customerLocationCity = cases.destinationLocationCity;
                	destLocationDetails.customerLocationCountryCode = cases.destinationLocationCountryCode;
                	destLocationDetails.street = cases.destinationStreet;
                	destLocationDetails.poBox = cases.destinationPoBox;
                	destLocationDetails.serviceLocationLatitude = cases.serviceLocationLatitude;
                	destLocationDetails.serviceLocationLongitude = cases.serviceLocationLongitude;
                	destLocationDetails.isDestinationLocation = true;
                	destLocationDetails.isCustomer = $scope.isCustomer;
                
                	
                	console.log("destLocationDetails json " + JSON.stringify(destLocationDetails));
                	
                $http.post(SERVICEADDRESS + '/serviceLocation/editVehicleServiceLocation', JSON.stringify(destLocationDetails)).            	    		
                success(function(returnJson) {
               	
                    if (returnJson.isSuccess) {
                    	$scope.isValidated = true;   
                    	$scope.caseCloseValidated = true;
                    	$scope.cases.isDestinationLocationSet  = true;
                    	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
                    	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
                    } else {
                    	 $scope.isValidated = false;
                        $scope.serviceMessage = returnJson.serviceMessage;
                        $("html, body").animate({ scrollTop: 0 }, 1000);
                    }

                }).error(function(returnErrorJson, status, headers, config) {
                	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
        			if(isFreshTokenGenarated){
        				$scope.editDestinationLocation(cases); 
        			}
               }); //error
               } // end if isValid
            }; //end of savevehcielLocationdetails

            
            $scope.redirectToAssignCaseView= function redirectToAssignCaseView(viewToBeDisplayed,faultId,subcaseId,index){
            	$scope.selectedFaultIndexNo = index;
            	 $scope.assistanceId = faultId;
            	 $scope.subcaseId = subcaseId;
            	 $scope.fetchListOfDrivers();
	    		 $scope.mainview = viewToBeDisplayed;
	    	 };
        
        
        /**
    	 * Fetch List of Drivers
    	 */
    	$scope.isApply =false;
    	$scope.fetchListOfDrivers = function fetchListOfDrivers(){
    		$scope.nearestDrivers = [];
    		var gpscoordinates = {
    				serviceLocationLatitude: $scope.cases.serviceLocationLatitude,
    				serviceLocationLongitude: $scope.cases.serviceLocationLongitude,
    				assistanceId : $scope.assistanceId,
    				caseId: $scope.cases.caseId,
    				subcaseId : $scope.subcaseId
    		};
    		
    		
    		$http.post(SERVICEADDRESS + '/assigningCase/fetchNearestAvailableDrivers', JSON.stringify(gpscoordinates)).
    		success(function(returnJson) {

    			if (returnJson.isSuccess) {
    				$scope.ifNoRecordsFound = false;
    				$scope.isCaseAssigned = true;
    				$scope.nearestDrivers = returnJson.drivers;
    			} else {
    				$scope.ifNoRecordsFound = true;
    				$scope.serviceMessage = returnJson.serviceMessage;
    				$("html, body").animate({ scrollTop: 0 }, 1000);
    			}

    		}).error(function(returnErrorJson, status, headers, config) {
    			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.fetchListOfDrivers(); 
    			}
          }); //error


    	}; //end of fetchListOfDrivers 

    	$scope.setTruckDriverId = function setTruckDriverId(truckDriverId,durationValue,entities,position){
    		$scope.truckDriverId = truckDriverId;
    		$scope.durationValue = durationValue;
    		$scope.isApply = true;
    	};

	/**
	 * Assign case to drivers
	 */
	
	$scope.assignCase = function assignCase(canChangePath){
		
		 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
 		if(!canWrite){
 			$scope.isValidated = false;
 			$scope.serviceMessage = "You don't have permissions to assign driver";
 			$("html, body").animate({ scrollTop: 0 }, 1000);
 			return false;
 		}

		var casetruckInfo = {
				subcaseId  :$scope.subcaseId,
				truckDriverId:$scope.truckDriverId,
				caseId: $scope.cases.caseId,
				userId: $scope.userId,
				durationValue:$scope.durationValue
		};
			console.log("casetruckInfo - "+ JSON.stringify(casetruckInfo));
		$http.post(SERVICEADDRESS + '/assigningCase/assigningCaseToDriver', JSON.stringify(casetruckInfo)).
		success(function(returnJson) {
			console.log("returnJson - "+ JSON.stringify(returnJson));
			if (returnJson.isSuccess) {
				$scope.isValidated = true;
				$scope.serviceMessage = returnJson.serviceMessage;
				$scope.subcasesList = returnJson.subcasesInfo;
				$scope.isCaseAssigned = false;
				if(canChangePath){
					$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
					if(!$scope.isFromCaseSchedule){
						$scope.view = '/resources/views/bodytemplates/main_case_body.html';
					}else{
						 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.assistanceId,"caseFaultId");
	   	 				 $scope.faults[$scope.selectedFaultIndex].checked = true;
	   	 				$scope.subCaseServiceName =  null;
	   	 				 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
	   	 				
						$scope.view = '/resources/views/bodytemplates/case_info_body.html';
					}
					$scope.scrollBottom();//to scroll bottom to view assistances
				}else{
					$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
					$scope.view = '/resources/views/bodytemplates/case_info_body.html';
				}
			} else {
				$scope.isValidated = false;     
				$scope.serviceMessage = returnJson.serviceMessage;
				$("html, body").animate({ scrollTop: 0 }, 1000);
			}
			$scope.isApply = false;
			console.log(returnJson.isSuccess+" "+$scope.serviceMessage);

		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.editCustomerDetails(cases); 
			}
		}); //error


	}; //end of fetchListOfDrivers 
	$scope.backToMainCase = function backToMainCase() {
		$scope.isValidated = true;
		$scope.isApply = false;
		$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
		if(!$scope.isFromCaseSchedule){
			$scope.view = '/resources/views/bodytemplates/main_case_body.html';
		}else{
			$scope.view = '/resources/views/bodytemplates/case_info_body.html';
		}
	};

   
    /** 
	 * get list of countries	 
	 **/
    $scope.getCountriesForCase = function getCountriesForCase() {
    	if(angular.isUndefinedOrNull($scope.caseCountryNameList)){
	    	$http.get(SERVICEADDRESS+'/countries/fetch').success(function(returnJson){
	    		var countryJson = returnJson.tableResultList;
	    		$scope.countryJson = returnJson.tableResultList;
	    		if($scope.caseCountryNameList.length <= 0){
				    for(var i=0;i<countryJson.length;i++){
				    	$scope.caseCountryNameList.push(countryJson[i].Name);	
						$scope.caseCountryIsoList.push(countryJson[i].ISOCode);
				    }
	    		}
			
			}).error(function(returnErrorJson, status, headers, config) {
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.getCountriesForCase(); 
				}
			});//end of http post call
    	}
    };
    
    
    /**
	 * To fetch trucks through service 
	 * 
	 * 
	 * @return selection array
	 */

	$scope.caseFaultServicesList =  function () {
	
		$http.get(SERVICEADDRESS+'/caseRegistration/fetchcasefaultservices').
		success(function(returnJson) {

			if(returnJson.isSuccess){
				$scope.faults = returnJson.caseFaultServicesList;
			}else{
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.serviceMessage = returnJson.serviceMessage;
			}
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {			
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
			if(isFreshTokenGenarated){
				$scope.caseFaultServicesList();
			}
		});//error
	};//caseFaultServicesList function end
	
    
    /**
	 * To fetch trucks through service 
	 * 
	 * 
	 * @return selection array*/
	
$scope.disabledCaseFaultServicesList =  function (subcaseList) {
		
		$http.get(SERVICEADDRESS+'/caseRegistration/fetchcasefaultservices').
		success(function(returnJson) {

			if(returnJson.isSuccess){
				$scope.faults = returnJson.caseFaultServicesList;
				 for (var i = 0; i < subcaseList.length; i++) {
	            		var subCaseFault =  subcaseList[i].faultName;
	            		 var selectedSubCaseIndex= $scope.getIndexOf($scope.faults, subCaseFault,"caseFaultDesc");
	            		$scope.faults[selectedSubCaseIndex].isDisabled = true;
	            }
			}else{
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.serviceMessage = returnJson.serviceMessage;
			}
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {			
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
			if(isFreshTokenGenarated){
				$scope.caseFaultServicesList();
			}
		});//error
	};//disabledCaseFaultServicesList function end
	

	$scope.loadCaseList = function loadCaseList(url,casetype){
		 var locationPath = url.trim()+'/'+casetype;
		 $location.path(locationPath);
		
	};
	

	/**
	 * Fetch Case Schedule Info
	 */
	
	$scope.fetchCaseSchedule = function fetchCaseSchedule(casetype){

		var casesRequest = {
				typeId:casetype,
				userId:$scope.userId
		};
		
		console.log('/caseSchedule/listAllCases'+JSON.stringify(casesRequest));
		$http.post(SERVICEADDRESS + '/caseSchedule/listAllCases', JSON.stringify(casesRequest)).
		success(function(returnJson) {

			if (returnJson.isSuccess) {
				$scope.isValidated = true;    
				$scope.casescheduleList = returnJson.caseslist;
				$scope.casestatusList = returnJson.casestatusList;
				$scope.totalItems = $scope.casescheduleList.length;
				$scope.currentPage = 1;
				if($scope.totalItems<$scope.currentPage*$scope.maxSize){
					$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.totalItems);
				}else{
					$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.currentPage*$scope.maxSize);	
				}
				$scope.showCustomerSatisfaction = false;
				
				console.log('Response'+JSON.stringify(returnJson));
			} else {
			    $scope.ifNoRecordsFound = true;
				$scope.serviceMessage = returnJson.serviceMessage;
				$("html, body").animate({ scrollTop: 0 }, 1000);
			}
			$scope.casesinquecount = returnJson.casescountresult.casesinquecount;
			$scope.pendingcasescount = returnJson.casescountresult.pendingcasescount;
			$scope.closedcasescount  = returnJson.casescountresult.closedcasescount;

		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.editCustomerDetails(cases); 
			}
  		}); //error
	}; //end of fetchCaseSchedule 
	
	$scope.isActualArrivaRequired = function isActualArrivaRequired(oldScheduleStatusId,selectedScheduleStatusId){
		if(angular.isUndefined(selectedScheduleStatusId) || selectedScheduleStatusId == oldScheduleStatusId || selectedScheduleStatusId != 2){
			return false;
		}
		return true;
			
	};
	
	/**
	 * Update Case status& arrivate time
	 */
	
	$scope.updateCaseStatus = function updateCaseStatus(caseId,caseStatusId,customerMobileNo,customerName){
		
		
		 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
	 		if(!canWrite){
	 			if($scope.isInCaseSchedule){
	 				$scope.isValidated = false;
	 			}else{
	 				$scope.caseCloseValidated = false;
	 			}
	 			$scope.serviceMessage = "You don't have permissions to update case status";
	 			return false;
	 		}
	 		
	 		if(angular.isUndefinedOrNull(caseStatusId)){
	 			$scope.isValidated = false;
	 			$scope.serviceMessage = "Invalid option selected";
	 			return false;
	 		}
	 		
		$scope.onSuccess = false;
		$scope.caseonSuccess = false;
		var caseObject = {};
		caseObject.userId = $scope.userId;
		caseObject.caseId = caseId;
		caseObject.subcaseId = '';
		caseObject.caseStatusId = caseStatusId;
		
		console.log('/caseSchedule/updateCaseArrivalTime'+angular.toJson(caseObject));
		
		$http.post(SERVICEADDRESS + '/caseSchedule/updateCaseArrivalTime', angular.toJson(caseObject)).
		success(function(returnJson) {
			console.log("returnJson - "+angular.toJson(caseObject));
			if (returnJson.isSuccess) {
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.isValidated = true;
 				$scope.caseCloseValidated = true;
 			
				if(caseStatusId == 3){
						if(!$scope.isFromCaseSchedule){
		 					$location.path("/caseschedule");
		 				}else{
		 					$scope.view = '/resources/views/bodytemplates/case_info_body.html';
		 				}
				}else if(caseStatusId == 20){
					$scope.fetchCaseSchedule(0);
				}else if(caseStatusId == 16){
						$scope.showCustomerSatisfaction = true;
						$scope.caseId = caseId;
						$scope.customerMobileNo = customerMobileNo;
						$scope.customerName = customerName;
						return false;
				}
			} else {
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
			}

		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.editCustomerDetails(cases); 
			}
    	}); //error
	}; //end of fetchListOfDrivers 

	/**
	 * Update Case status& arrivate time
	 */
	
	$scope.editCaseSchedule = function editCaseSchedule(cases,index){
		
		 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
	 		if(!canWrite){
	 			if($scope.isInCaseSchedule){
	 				$scope.isValidated = false;
	 			}else{
	 				$scope.caseCloseValidated = false;
	 			}
	 			$scope.serviceMessage = "You don't have permissions to update case status";
	 			return false;
	 		}
	 		var isValid = true;
	 		var actualDateTime;
	 	if(!angular.isUndefined(cases.dateToSet) && !angular.isUndefined(cases.timeToSet) && oldScheduleStatusId != 6){
	 		var actualDate = $filter('date')(cases.dateToSet, 'yyyy/MM/dd');
		 	var actualTime = $filter('date')(cases.timeToSet, "HH:mm");
		 	
			if(angular.isUndefinedOrNull(cases.timeToSet)){
				$("html, body").animate({ scrollTop: 0 }, 1000);
				$scope.isValidated = false;
				$scope.serviceMessage = "Please enter valid time format";
				isValid = false;
				return false;
			}
		 	
		 	actualDateTime = $filter('date')(actualDate + " " +actualTime,'yyyy/MM/dd HH:mm');
		}
		 		
	 	if(isValid){
				$scope.onSuccess = false;
				$scope.caseonSuccess = false;
				$scope.subCaseIndex = index;
				var caseObject = {};
				var arrivalTime = '';
				var casescheduleListObject = $scope.subcasesList[index];
				$scope.subcaseId = casescheduleListObject.subcaseId;
				$scope.assistanceId = cases.faultId;
				$scope.serviceId = $scope.subcasesList[index].caseRegisteredServiceID;
				
				var oldScheduleStatusId = $scope.subcasesList[index].oldScheduleStatusId;
				var scheduleStatusId = cases.schedulestatusID; 
				
				if( oldScheduleStatusId == 6){ //if case is unassigned and new status is assign to que
					
					if(!angular.isUndefinedOrNull(cases.schedulestatusID) && cases.schedulestatusID != oldScheduleStatusId){
						if($scope.isInCaseSchedule){
			 				$scope.isValidated = false;
			 			}else{
			 				$scope.caseCloseValidated = false;
			 			}
						$scope.serviceMessage = "Case is un assigneed.Please assign in order to proceed";
						
						return false;
					}else{
						if($scope.isInCaseSchedule){
			 				$scope.isValidated = true;
			 				$("html, body").animate({ scrollTop: 0 }, 1000);
			 			}else{
			 				$scope.caseCloseValidated = true;
			 			}
						$scope.isCaseAssigned = true;
						$scope.subcaseId = cases.subcaseId;
						$scope.fetchListOfDrivers();
						$scope.mainview = '/resources/views/bodytemplates/case_assign_body.html';
						
						return false;
					}
					//$("html, body").animate({ scrollTop: 0 }, 1000);
				}
				
			   if(angular.isUndefinedOrNull(cases.schedulestatusID) || cases.schedulestatusID == oldScheduleStatusId){//if do selects the current option
				   if($scope.isInCaseSchedule){
						$scope.isValidated = false;
						
					}else{
						$scope.caseCloseValidated = false;
					}
					scheduleStatusId = oldScheduleStatusId;
					//$("html, body").animate({ scrollTop: 0 }, 1000);
					
					if(cases.schedulestatusID == 4 ){
						$scope.serviceMessage = "Assigned status already updated.";	
					}else{
						var selectedStausIndex= $scope.getIndexOfInt($scope.casestatusList, cases.schedulestatusID,"schedulestatusID");
						$scope.serviceMessage = $scope.casestatusList[selectedStausIndex].schedulestatus + " status already updated.";
					}
					return false;
				}
		
				var selectedStausIndex= $scope.getIndexOfInt($scope.casestatusList, scheduleStatusId,"schedulestatusID");
				var oldStausIndex = $scope.getIndexOfInt($scope.casestatusList, oldScheduleStatusId,"schedulestatusID") + 1;
				if(selectedStausIndex != oldStausIndex && cases.schedulestatusID != 20){
					if($scope.isInCaseSchedule){
		 				$scope.isValidated = false;
		 				
		 			}else{
		 				$scope.caseCloseValidated = false;
		 			}
					$scope.serviceMessage = "Please select case status in the order in which they appear";
					//$("html, body").animate({ scrollTop: 0 }, 1000);
					return false;
				}
				
				if(cases.schedulestatusID == 15){
					if(!$scope.cases.isDestinationLocationSet && $scope.isTowingRequired){
						if($scope.isInCaseSchedule){
			 				$scope.isValidated = false;
			 				$("html, body").animate({ scrollTop: 0 }, 1000);
			 			}else{
			 				$scope.caseCloseValidated = false;
			 			}
						$scope.serviceMessage = "Please Add Destination In order to complete the case";
					}else{
						$("html, body").animate({ scrollTop: 0 }, 1000);
						$scope.mainview = '/resources/views/bodytemplates/case_onJobdetail_body.html';
					}
					return false;
				}
				
				
				
				/*if(scheduleStatusId == 9)
					casescheduleListObject.actualarrival = arrivalTime;*/
				
				casescheduleListObject.schedulestatusID = cases.schedulestatusID;
				caseObject.userId = $scope.userId;
				caseObject.caseId = casescheduleListObject.caseId;
				caseObject.subcaseId = casescheduleListObject.subcaseId;
				caseObject.caseStatusId = casescheduleListObject.schedulestatusID;
				caseObject.arrivalTimeAtLoc = actualDateTime;
				
				console.log('/caseSchedule/updateCaseArrivalTime'+angular.toJson(caseObject));
				
				$http.post(SERVICEADDRESS + '/caseSchedule/updateCaseArrivalTime', angular.toJson(caseObject)).
				success(function(returnJson) {
					console.log("returnJson - "+angular.toJson(returnJson));
					if (returnJson.isSuccess) {
						$scope.subcasesList = returnJson.subcasesInfo;
						$scope.isValidated = true;
			 			$scope.caseCloseValidated = true;
			 			$scope.isServiceValid = true;
 			        	$scope.isFaultValid  = true;
 			        	
						if( cases.schedulestatusID == 7){
							
							if($scope.isInCaseSchedule){
				 				$scope.onSuccess = true;
				 			}else{
				 				$scope.caseonSuccess = true;
				 			}
							var selectedStausIndex= $scope.getIndexOfInt($scope.casestatusList, cases.schedulestatusID,"schedulestatusID");
							$scope.serviceMessage =  $scope.casestatusList[selectedStausIndex].schedulestatus + " status updated successfully for Assistance "+ caseObject.subcaseId;
							 $timeout(function(){
									if($scope.isInCaseSchedule){
						 				$scope.onSuccess = false;
						 				//$("html, body").animate({ scrollTop: 0 }, 1000);
						 			}else{
						 				$scope.caseonSuccess = false;
						 			}
						     }, 10000);
						}
						
						if( cases.schedulestatusID == 20){
							if($scope.isInCaseSchedule){
				 				$scope.onSuccess = true;
				 				//$("html, body").animate({ scrollTop: 0 }, 1000);
				 			}else{
				 				$scope.caseonSuccess = true;
				 			}
							var selectedStausIndex= $scope.getIndexOfInt($scope.casestatusList, cases.schedulestatusID,"schedulestatusID");
							$scope.serviceMessage =  $scope.casestatusList[selectedStausIndex].schedulestatus + " status updated successfully for Assistance "+ caseObject.subcaseId;
							$scope.casesinquecount = $scope.casesinquecount + 1;
							
							 $timeout(function(){
									if($scope.isInCaseSchedule){
						 				$scope.onSuccess = false;
						 			}else{
						 				$scope.caseonSuccess = false;
						 			}
						     }, 10000);
							
						}
						
						if(cases.schedulestatusID == 9){
							$scope.canUpdateFault = false;
							$scope.subCaseId = $scope.subcasesList[index].subcaseId;
							$scope.mainview = "/resources/views/bodytemplates/case_prereqisite_body.html";
							$("html, body").animate({ scrollTop: 0 }, 1000);
						}
						
					} else {
						$scope.subcasesList[index].schedulestatusnew = oldScheduleStatusId;
						if($scope.isInCaseSchedule){
			 				$scope.isValidated = false;
			 			}else{
			 				$scope.caseCloseValidated = false;
			 			}
						$scope.serviceMessage = returnJson.serviceMessage;
					}
		
				}).error(function(returnErrorJson, status, headers, config) {
					isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.editCustomerDetails(cases,index); 
					}
		   	}); //error
	 	}//isValid
	}; //end of fetchListOfDrivers 
	
	/**
	 *get count of cases in que,pending,closed
	 */
	$scope.getCasesCount = function getCasesCount(){

		var caseDetails={};
		caseDetails.userId = $scope.userId;

		$http.post(SERVICEADDRESS + '/cases/casescount', JSON.stringify(caseDetails)).
		success(function(returnJson) {
			if (returnJson.isSuccess) {
				$scope.casesinquecount   = returnJson.casesinquecount;
				$scope.pendingcasescount = returnJson.pendingcasescount;
				$scope.closedcasescount  = returnJson.closedcasescount;
				$scope.isValidated = true;
			}else{
				$scope.isValidated = false;
			}
		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.getCasesCount(); 
			}
		}); //error

	}; //end of getCasesInQueue

	/*******
	 * to display time counter in case schedule
	 *
	*******/
	
	$scope.displayCounterTime = function(caseassignedtime){
		var assignedDate = Date.parse(caseassignedtime.replace(" ","T"));
	    var formattedDate = new Date().getTime();
        return (formattedDate-assignedDate);
	};
	
	function msToTime(s) {

		  function addZ(n) {
		    return (n<10? '0':'') + n;
		  }

		  var ms = s % 1000;
		  s = (s - ms) / 1000;
		  var secs = s % 60;
		  s = (s - secs) / 60;
		  var mins = s % 60;
		  var hrs = (s - mins) / 60;

		  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
		}
		 
	
	
    /**
	 * To prepare a list of checked services
	 * 
	 * 
	 * @return selection array
	 */

	$scope.selectedFa=[];
	/*******
	 * update checked and un checked item
	 *
	*******/
	//	toggle selection for a given service by id
	$scope.toggleSelection = function toggleSelection(serviceId) {
		var idx = $scope.selection.indexOf(serviceId);
		if (idx > -1) {// is currently selected
			$scope.selection.splice(idx, 1);
		}else { 		// is newly selected
			$scope.selection.push(serviceId);
		}
	};

	
		
		
    /*******
	 * get list of brands & models
	 *
	*******/
    
    $scope.getBrandAndModelList = function() {
    	
    	$http.get(SERVICEADDRESS+'/vehicle/getvehiclebrands').
		success(function(returnJson) {
			    $scope.brandModelList   = returnJson.vehicleBrandsList;
			    $scope.vinmodelsList    = returnJson.vinmodelsList;
			    $scope.vinmodelyearList = returnJson.vinmodelyearList;
			    var brandModelsLength = $scope.brandModelList.length;
				for(var i=0;i<brandModelsLength;i++){
					$scope.brands.push($scope.brandModelList[i].vehicleBrandName);	
					$scope.brandId.push($scope.brandModelList[i].vehicleBrandId);
				}
			  
			}).error(function(returnErrorJson, status, headers, config) {
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.getInitList();
				}
				
			});//end of http post call
		
	}; //End Brands n Models function
	
	
    /**
     *  Emergency button
     */ 
    
	   $scope.emrgencyreset = function emrgencyreset() {
		   $scope.isEmergency=!$scope.isEmergency;
	   };
	   
      /**
	     *redirect to assign a case to driver page
	     */
	  $scope.redirectToAssignCases = function redirectToAssignCases(){
		  
		  var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
	 		if(!canWrite){
	 			if($scope.isInCaseSchedule){
	 				$scope.isValidated = false;
	 			}else{
	 				$scope.caseCloseValidated = false;
	 			}
	 			$scope.serviceMessage = "You don't have permissions to Update Case Arrival Time";
	 			return false;
	 		}
	 		
			  if(!$scope.isMainCasegistered){
				  if($scope.isInCaseSchedule){
		 				$scope.isValidated = false;
		 			}else{
		 				$scope.caseCloseValidated = false;
		 			}
					$scope.serviceMessage="Please select atleast one fault";
					$("html, body").animate({ scrollTop: 0 }, 1000);
					return false;
			  }
		    
		    var caseObject = [];
			var caseStatusUpdateObject ={};
			caseStatusUpdateObject.caseId = $scope.cases.caseId;
			caseStatusUpdateObject.userId = $scope.userId;
			caseStatusUpdateObject.actualarrival = '';
			caseStatusUpdateObject.schedulestatusID = 6;
			caseObject[0] = caseStatusUpdateObject;
			
			console.log('/caseSchedule/updateCaseArrivalTime'+angular.toJson(caseObject));
			
			$http.post(SERVICEADDRESS + '/caseSchedule/updateCaseArrivalTime', angular.toJson(caseObject)).
			success(function(returnJson) {

				if (returnJson.isSuccess) {
					$scope.fetchListOfDrivers();
					if($scope.cases.isScheduledToday || $scope.cases.serviceRequiredTypeId == 1){
						$scope.mainview = '/resources/views/bodytemplates/case_assign_body.html';
					}else{
						$location.path("/caseschedule");
					}
				}

			}).error(function(returnErrorJson, status, headers, config) {
				isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.redirectToAssignCases(); 
				}
	  		}); //error
	  };
	  
		/**
		 * To fetch trucks through service 
		 * 
		 * 
		 * @return selection array
		 */

		$scope.getServicesList =  function (position, entities) {
				angular.forEach(entities, function(fault, index) {
				    if (position != index) {
				    	fault.checked = false;
				    }else{
				    	$scope.lisOfServices = entities[position].faultServicesList;
				    	
				    	if(!angular.isUndefinedOrNull($scope.subCaseServiceName) && $scope.subCaseServiceName.length>0){
					    	angular.forEach($scope.lisOfServices, function(service, index) {
					    		if($scope.lisOfServices[index].caseServiceDesc.indexOf($scope.subCaseServiceName) >= 0){
					    			service.checked = true;
					    			 $scope.isServiceAdded = true;
					    		}else{
					    			service.checked = false;
					    		}
					    	});
				    	}else{
				    		angular.forEach($scope.lisOfServices, function(service, index) {
					    			service.checked = false;
					    	});
				    	}
				    	
						$scope.cases.caseFaultId = entities[position].caseFaultId;
				    }
			  });
		
		};//getServicesList function end
		
		$scope.updateSelectedServiceId =  function (caseServiceId,subCaseId,casefaultRegserviceID,service,isChecked) {
				var subcasesInfo = {};
				$scope.isServiceAdded = false;
				var isFirst = false;
				//serach in the list if subcase and service is added already
				var indexOfSubCase = $scope.getIndexOfInt($scope.subcasesList,subCaseId,"subcaseId");
				var caseRegisteredServiceID = $scope.subcasesList[indexOfSubCase].caseRegisteredServiceID;
				
				if(!angular.isUndefinedOrNull(caseRegisteredServiceID) && isChecked){
					if(caseRegisteredServiceID >= 0){
						if(caseRegisteredServiceID == caseServiceId){
							$scope.isServiceAdded = true;
						}
						if(caseRegisteredServiceID == 0){
							isFirst = true;
						}
					}
				}
				
				
				if(!$scope.isServiceAdded && isChecked){
					if(isChecked && isFirst){ //if sub case and service is not added yet
						subcasesInfo.action = 1;
					}else{ //if sub case and service is added
						subcasesInfo.action = 2;
					}
					subcasesInfo.subCaseId = subCaseId;
					subcasesInfo.caseServiceId = caseServiceId;
					subcasesInfo.casefaultRegserviceID = $scope.subCaseFaultId;
					
					$scope.tempSelectedServices.splice(0);
					$scope.tempSelectedServices.push(subcasesInfo);	
				
					
				}else{
					$scope.tempSelectedServices.splice(0);
				}
				
				 angular.forEach($scope.lisOfServices, function(service, index) {
					    if (caseServiceId != service.caseServiceId) {
					    	service.checked = false;
					    	service.isDisabled = true;
						}
				 });
				
		};//updateSelectedServiceId function end
		
	  /**
	   *redirect to assign a case to driver page
	   */
	  $scope.getCaseDetailsByCaseId = function getCaseDetailsByCaseId(casesId){
		  var caseId = casesId;
     	
		  var caseDetails={};
          caseDetails.userId = $scope.userId;
          caseDetails.caseId = caseId;
          console.log("caseDetails json " + JSON.stringify(caseDetails));
        
          $http.post(SERVICEADDRESS + '/cases/fetchCaseDetailedInfo', JSON.stringify(caseDetails)).
          success(function(returnJson) {
          console.log("returnJson - "+JSON.stringify(returnJson));
            if (returnJson.isSuccess) {
	             $scope.subcasesList = returnJson.subcasesInfo;
	             $scope.cases = returnJson.casedetails;
	             if($scope.subcasesList.length > 0){
	            	 $scope.subCaseId =  $scope.subcasesList[0].subcaseId;
	             	 $scope.subCaseFault =  $scope.subcasesList[0].faultName;
	             	 $scope.subCaseFaultId =  $scope.subcasesList[0].caseRegisteredFaultID;
	             	 $scope.selectedSubCaseIndex = 0;
	             	 //loading list of case faults and services from DB
	 				 $scope.faults = returnJson.caseFaultServicesList;
	 				 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
	 				 $scope.faults[$scope.selectedFaultIndex].checked = true;
	 				 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
	             }
        	}
          }).error(function(returnErrorJson, status, headers, config) {
          	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
  			if(isFreshTokenGenarated){
  				$scope.getCaseDetailsById(casesId); 
  			}
          }); //error
         
      }; //end of getCaseDetailsById
      
      /*cases in queu functionality*/
      
      /**
	   *redirect to assign a case to driver page
	   */
	  $scope.getCasesInQueue= function getCasesInQueue(){
          
		  var caseDetails={};
          caseDetails.userId = $scope.userId;
          console.log("caseDetails json " + JSON.stringify(caseDetails));
        
          $http.post(SERVICEADDRESS + '/cases/listAllCasesinQue', JSON.stringify(caseDetails)).
          success(function(returnJson) {
          console.log("returnJson - "+JSON.stringify(returnJson));
            if (returnJson.isSuccess) {
            	 $scope.ifNoRecordsFound = false;
            	 $scope.casesList = returnJson.caseslist;
            	 $scope.totalItems = $scope.casesList.length;
				 $scope.currentPage = 1;
				 refreshCasesData($scope.casesList,true);
            	 $scope.isValidated = true;
	         }else{
	        	 $("html, body").animate({ scrollTop: 0 }, 1000);
	        	 $scope.ifNoRecordsFound = true;
	        	 $scope.isValidated = false;
	         }
          }).error(function(returnErrorJson, status, headers, config) {
          	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
  			if(isFreshTokenGenarated){
  				$scope.getCasesInQueue(); 
  			}
         }); //error
         
      }; //end of getCasesInQueue
      
      /**
	   *Assigning case to DO
	   */
	  $scope.assignCaseToDO = function assignCaseToDO(caseId,index){
		  
		  var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
	 		if(!canWrite){
	 			$scope.isValidated = false;
	 			$scope.serviceMessage = "You don't have permissions to accept case";
	 			return false;
	 		}
	      
		  var caseDetails={};
          caseDetails.userId = $scope.userId;
          caseDetails.caseId = caseId;
          console.log("caseDetails json " + JSON.stringify(caseDetails));
        
          $http.post(SERVICEADDRESS + '/cases/assigncasetoself', JSON.stringify(caseDetails)).
          success(function(returnJson) {
        	  console.log("returnJson - "+JSON.stringify(returnJson));
            if (returnJson.isSuccess) {
        	    $scope.isValidated = true;
        	    $location.path('/caseschedule/1');
	         }else{
	        	 $("html, body").animate({ scrollTop: 0 }, 1000);
	        	 $scope.serviceMessage = returnJson.serviceMessage;
	        	 $scope.isValidated = false;
	         }
          }).error(function(returnErrorJson, status, headers, config) {
          	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
  			if(isFreshTokenGenarated){
  				$scope.assignCaseToDO(caseId,index); 
  			}

          }); //error
         
      }; //end of assignCaseToDO
      
      /*cases in queue functionality end*/
     
     /**
  	 * to clear field value
  	 */

      $scope.clearData = function clearData(fieldValue){
    	  fieldValue = "";
      };
      
      $scope.changeStatus = function changeStatus(){
  		$scope.isCaseAssigned = false;
      };
   
      /*function to save case pre requisitie details*/
	   $scope.casePrerequisiteJobdetails = function casePrerequisiteJobdetails(cases){
		  
		   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
	 		if(!canWrite){
	 			$scope.isValidated = false;
	 			$scope.serviceMessage = "You don't have permissions to Add Case Prerequisite Job details";
	 			return false;
	 		}
		    var isValid = $scope.tracForm.$valid;
	        
	        var casePrerequisiteJobdetails = {};
	        casePrerequisiteJobdetails.userId= $scope.userId;
        	casePrerequisiteJobdetails.subcaseId =   $scope.subcaseId;
       		casePrerequisiteJobdetails.isEmergency=   $scope.isEmergency;
        	casePrerequisiteJobdetails.emergencyComments= cases.emergencyComments;
    		casePrerequisiteJobdetails.isVehicleAvailable= cases.isVehicleAvailable;
        	casePrerequisiteJobdetails.vehicleAvailableComments= cases.vehicleAvailableComments;
        	casePrerequisiteJobdetails.isServiceAccepted= cases.isServiceAccepted;
        	casePrerequisiteJobdetails.serviceAcceptedComments=  cases.serviceAcceptedComments;
        	casePrerequisiteJobdetails.isReported= cases.isReported;
        	casePrerequisiteJobdetails.isReportedSameIssue= cases.isReportedSameIssue;
        	casePrerequisiteJobdetails.reportedComments= cases.reportedComments; 
        	casePrerequisiteJobdetails.isAttendedSolved= cases.isAttendedSolved;
        	casePrerequisiteJobdetails.isNoDentsScratches= cases.isNoDentsScratches;
        	casePrerequisiteJobdetails.dentscomments= cases.dentscomments;
        	console.log("isValid - "+isValid);
	        console.log("casePrerequisiteJobdetails json " + JSON.stringify(casePrerequisiteJobdetails));
	        
	        if(isValid){
		        	$http.post(SERVICEADDRESS + '/preRequestieJobDetails/addPreRequestieJobDetails', JSON.stringify(casePrerequisiteJobdetails)).
				        success(function(returnJson) {
				            console.log("returnJson - "+JSON.stringify(returnJson));
				          
				            if (returnJson.isSuccess) {
				            	$scope.subcasesList = returnJson.subcasesInfo;
				            	$scope.isValidated = true;
				            	var isCaseClosed = returnJson.isCaseClosed;
				            	var j=0,k=0;
				            	
				            	if(!$scope.cases.isDestinationLocationSet){
				   	             	for (var i = 0; i < $scope.subcasesList.length; i++) {
				   	             		var isCaseClosedByDifferentIssue = $scope.subcasesList[i].isCaseClosedByDifferentIssue;
			     	            		var serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
			     	            		var faultId = $scope.subcasesList[i].faultId;
			     	            		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6){
			     	            			if(isCaseClosedByDifferentIssue){
			     	            				j++;
			     	            			}
			     	            			k++;
			     	            		}
			     	            		
			                		}
				   	             	
					            	if(j == k && k != 0){
					            		$scope.isTowingRequired = false;
					            		$scope.cases.isTowRequested =false;
					            	}
				            	}else{
				            		$scope.isTowingRequired = false;
				            		$scope.cases.isTowRequested =false;
				            	}
				            		
			   	             	
				            	if($scope.isEmergency || (!isCaseClosed && !cases.isAttendedSolved)){// on emergency
				            		if(!cases.isReportedSameIssue && !cases.isAttendedSolved){
				            			//$scope.canUpdateFault = true;
				            			$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
					     	            $scope.view = '/resources/views/bodytemplates/main_case_body.html';
				            		}else{
					            		$scope.isCaseAssigned = true;
					            	
					            		if(!angular.isUndefinedOrNull(returnJson.newSubCaseId) && returnJson.newSubCaseId > 0 ){
					            			$scope.subcaseId   = returnJson.newSubCaseId;
					            		}
					            	 	$scope.fetchListOfDrivers();
				            		}
				            	}else{
						            	if(!isCaseClosed){ //if not case closed
						 	 				 //if reported issue is different allow him to add sub case
					     	 				if(!cases.isVehicleAvailable){
						            			$location.path('/caseschedule');
						            			return false;
						            		}
						     	            if(!cases.isReportedSameIssue){//if different issue
						     	            	$scope.faults = returnJson.caseFaultServicesList;
						     	            	$scope.isFromPreRequsitie = true;
						     	            	if(cases.isAttendedSolved)
						     	            	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
							     	            $scope.view = '/resources/views/bodytemplates/main_case_body.html';
						     	            }else{
						     	            	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
						     	            	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
						     				    
						     	            }
						            	}else{ //if case closed
						            		if(!cases.isVehicleAvailable){
						            			$location.path('/caseschedule');
						            			return false;
						            		}else if(!cases.isReportedSameIssue){
						            			$scope.faults = returnJson.caseFaultServicesList;
						            			if(cases.isAttendedSolved){
						            				$scope.canUpdateFault = true;
						            			}
						            			$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
						     	            	$scope.view = '/resources/views/bodytemplates/main_case_body.html';
						            		}
						            		else if(!cases.isServiceAccepted){
						            			$location.path('/caseschedule');
						            		}else{
						            		  $location.path('/caseschedule');
						            		}
						            	}
				            	}
				            } else { //if service error
					            	$scope.isValidated = false;
					                $scope.serviceMessage = returnJson.serviceMessage;
					                $("html, body").animate({ scrollTop: 0 }, 1000);
				            }
				            
				            if(!cases.isReportedSameIssue){
				            	for (var k = 0; k < $scope.subcasesList.length; k = k + 1) {
				            		var subcaseId = $scope.subcasesList[k].subcaseId;
				            		var faultId = $scope.subcasesList[k].caseRegisteredFaultID;
				            		var indexLoc= $scope.getIndexOfFault($scope.faults,faultId,"caseFaultId");
				            		 if(indexLoc  >= 0){
							 			  $scope.faults[indexLoc].checked = false;
							 			  $scope.faults[indexLoc].isDisabled = true;
							 			 if(!cases.isReportedSameIssue && casePrerequisiteJobdetails.isAttendedSolved && subcaseId == casePrerequisiteJobdetails.subcaseId ){
							 				$scope.faults[indexLoc].checked = true;
					            		 }
							 		 }
				            		
				        		}
				            }
				            console.log("cases - "+JSON.stringify($scope.cases));
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.casePrerequisiteJobdetails(cases); 
							}
				        }); //Error
		        	
	       }//end isValid  
    
	    };//case prerequisite job details
	    
	    
	    $scope.onPreRequisitieCheck = function () {
			if($scope.cases.isVehicleAvailable)
				$scope.cases.vehicleAvailableComments = "";
			if($scope.cases.isServiceAccepted)
				$scope.cases.serviceAcceptedComments = "";
			if($scope.cases.isReportedSameIssue){
				$scope.cases.reportedComments = "";
				$scope.cases.isAttendedSolved = true;
			}
			if($scope.cases.isNoDentsScratches)
				$scope.cases.dentscomments = "";
		};
		
		$scope.vinValidationCheck = function (vin) {
		   var regpattern1 = /^(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
		   var regpattern2 = /^(?=.*\d).+$/;
		   var regpattern3 = /^[a-zA-Z0-9]*$/;
		  
		   if(!angular.isUndefinedOrNull(vin)){
			   if(vin.length > 0){
				   if(!regpattern3.test(vin)){
					   $scope.isVinValid = false;
					   $scope.vinValidationMessage = "Enter Only AlphaNumeric Characters";
					   return true;
				   }else if(!regpattern1.test(vin)){
					   $scope.isVinValid = false;
					   $scope.vinValidationMessage = "Please enter atleast one alphabet";
					   return true;
				   }else if(!regpattern2.test(vin)){
					   $scope.isVinValid = false;
					   $scope.vinValidationMessage = "Please enter atleast one number";
					   return true;
				   }else{
					   $scope.isVinValid = true;
					   return false;
				   }
			   }
		   }
		};

		/**
    	 * add sub Case 
    	 */
        $scope.addOneMoreSubCase = function addOneMoreSubCase(cases) {
        	
        	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
 	 		if(!canWrite){
 	 			$scope.isValidated = false;
 	 			$scope.serviceMessage = "You don't have permissions to Add Sub Case details";
 	 			return false;
 	 		}
            var isValid = $scope.tracForm.$valid;
            var mainCaseDetails={};
           var arrayLength = $scope.selection.length;
           
            if(isValid){
            	            	
            	 $scope.subCaseId = localStorage.getData('subCaseId');
            	 
            	 if(!angular.isUndefinedOrNull(cases.caseFaultId) && cases.caseFaultId != 0 && arrayLength > 0){
    		     	 var indexLoc= $scope.getIndexOfFault($scope.faults,cases.caseFaultId,"caseFaultId");
                     var subcasesListIndex = $scope.getIndexOfFault($scope.subcasesList, $scope.faults[indexLoc].caseFaultDesc,"faultName");
	                 	if(subcasesListIndex >= 0){
	                 		$scope.isValidated = false;
	                         $scope.serviceMessage = "Fault already updated for "+  $scope.caseId;
	                         $("html, body").animate({ scrollTop: 0 }, 1000);
	                 		return false;
	                 	}
            		 
    		 		   if(indexLoc  >= 0){
    		 			   $scope.faults[indexLoc].checked = false;
    		 			  $scope.faults[indexLoc].isDisabled = true;
    		 		   }else{
    		 			  $scope.isFaultValid = false;
    		 		   }
            	  }else{
            		  $scope.isFaultValid = true;
            	  }
     		    
            	 	mainCaseDetails.caseId =  $scope.caseId;		
    				mainCaseDetails.userId = $scope.userId;	
    				mainCaseDetails.vehicleId = cases.vehicleId;
    				mainCaseDetails.caseFaultId = cases.caseFaultId;
    				mainCaseDetails.caseFaultDescription = cases.caseFaultDescription;
    				mainCaseDetails.caseNotes = cases.caseNotes;
    				mainCaseDetails.caseNumber = cases.caseNumber;
    							
    				console.log("mainCaseDetails json " + JSON.stringify(mainCaseDetails)); 
    				
                $http.post(SERVICEADDRESS + '/caseRegistration/saveCaseInfo', JSON.stringify(mainCaseDetails)).            	    		
                success(function(returnJson) {
                	console.log("returnJson - "+ JSON.stringify(returnJson));
                    if (returnJson.isSuccess) {
                    	 $scope.isValidated = true;
                    	 $scope.isMainCasegistered = true;
                    	 cases.caseFaultDescription = "";
                    	 cases.caseNotes = "";
                    	 $scope.subcasesList= returnJson.subcasesInfo;
                    } else {
                    	$scope.isValidated = false;
                        $scope.serviceMessage = returnJson.serviceMessage;
                       
                    }

                }).error(function(returnErrorJson, status, headers, config) {
                	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
        			if(isFreshTokenGenarated){
        				$scope.addOneMoreSubCase(cases); 
        			}
                }); //error
               } // end if isValid
            }; //end of addOneMoreSubCase
	     
            /**
          	 * redirect to main case
          	 */
	       $scope.redirectToAssignCaseServices = function redirectToAssignCaseServices(){
	    	   
	    	   if($scope.isMainCasegistered){
	    		     $scope.isValidated = true;
	    		   	 $scope.getCountriesForCase();
	            	 $scope.subCaseId =  $scope.subcasesList[0].subcaseId;
	             	 $scope.subCaseFault =  $scope.subcasesList[0].faultName;
	             	 $scope.subCaseFaultId =  $scope.subcasesList[0].caseRegisteredFaultID;
	             	 $scope.selectedSubCaseIndex = 0;
	             	 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
					 $scope.faults[$scope.selectedFaultIndex].checked = true;
					 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
					 $scope.casescheduleview = '/resources/views/bodytemplates/main_case_faults.html';
	             }else{
	            	 $scope.isValidated = false;
                     $scope.serviceMessage = "Please add sub case in order to proceed";
                     $("html, body").animate({ scrollTop: 0 }, 1000);
	             }
	        };
	    	
	    	 /**
          	 * redirect to another view
          	 */
	    	 $scope.redirectToView= function redirectToView(viewToBeDisplayed){
	    		 $scope.mainview = viewToBeDisplayed;
	    	 };
	    	 
	    	 /**
	          	 * redirect to another view
	          	 */
		    $scope.redirectToScheduleView= function redirectToView(){
		    		 $scope.showCustomerSatisfaction = false;
		     };
		    	 
	    	 
	    	 /**
          	 * initializing parameters for updating fault service
          	 */
	    $scope.updateFaultService =  function (subCaseId,faultName,caseFaultId,index) {
	    	 $scope.isValidated = true;
	    	 $scope.isServiceValid =true;
	    	 $scope.isServiceAdded = false;
	    	 $scope.tempSelectedServices.splice(0);
        	 $scope.cases.serviceDescription = null;
	    	 $scope.subCaseId =  subCaseId;
         	 $scope.subCaseFault =  faultName;
         	 $scope.subCaseFaultId =  caseFaultId;
         	 $scope.isCaseClosedByDifferentIssue =  $scope.subcasesList[index].isCaseClosedByDifferentIssue;
         	 $scope.selectedSubCaseIndex = index;
         	 $scope.cases.serviceDescription =   angular.isUndefined($scope.subcasesList[index].serviceDescription) ? '' : $scope.subcasesList[index].serviceDescription;
     		 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
			 $scope.faults[$scope.selectedFaultIndex].checked = true;
			 $scope.subCaseServiceName =  $scope.subcasesList[index].FaultServiceName;
			 $scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
			
		};//updateFaultService function end
		
		 /**
      	 * initializing parameters for updating fault
      	 */
	    $scope.updateFault =  function (subCaseId,faultName,caseFaultId,index) {
	    		for(var i =0;i< $scope.faults.length;i++){
	    			 $scope.faults[i].checked = false;
	    		}
	    		 $scope.canUpdateFault =  true;
	    		 $scope.selectedFaultId = caseFaultId;
		    	 $scope.subCaseId =  subCaseId;
		     	 $scope.subCaseFault =  faultName;
		     	 $scope.subCaseFaultId =  caseFaultId;
		     	 $scope.selectedSubCaseIndex = index;
		     	 $scope.cases.caseFaultDescription = $scope.subcasesList[index].faultDescription;
		     	 $scope.cases.serviceDescription =   angular.isUndefined($scope.subcasesList[index].serviceDescription) ? '' : $scope.subcasesList[index].serviceDescription;
		 		 $scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
				 $scope.faults[$scope.selectedFaultIndex].checked = true;
				 $scope.cases.caseFaultId = caseFaultId;
			
		};//updateFaultService function end
		

	    /*******
		 * update checked and un checked item
		 *
		*******/
		/*$scope.updateSelection = function(position, entities,isChecked) {
			//if(!isChecked){
			  angular.forEach(entities, function(fault, index) {
				    if (position != index) {
				    	fault.checked = false;
				    	
				    }else{
				    	
				    	if(isChecked){
					    	$scope.cases.caseFaultId = entities[position].caseFaultId;
					    	$scope.selection.push(entities[position].caseFaultId);
				    	}else{
				    		var idx = $scope.selection.indexOf(entities[position].caseFaultId);
					    	if(!isChecked){
						    	$scope.cases.caseFaultId = '';
						    	$scope.selection.splice(idx,1);
						    }
				    	}
				    }
			  });
			
		};*/
		
		$scope.updateSelection = function(position, entities,isChecked) {
			  angular.forEach(entities, function(fault, index) {
			    if (position != index){
			    	fault.checked = false;
			    }else{
			    	if(isChecked){
				    	$scope.cases.caseFaultId = entities[position].caseFaultId;
				    	$scope.selection.splice(0);
				    	$scope.selection.push($scope.cases.caseFaultId);
			    	}else{
			    		var idx = $scope.selection.indexOf(entities[position].caseFaultId);
				    	if(!isChecked){
					    	$scope.cases.caseFaultId = '';
					    	$scope.selection.splice(idx,1);
					    }
			    	}
			    }
			  });
		};
		
		
		
		 /**
      	 * Service to update the fault for assistance
      	 */
	    $scope.updateFaultForAssistance =  function updateFaultForAssistance(caseId) {
	    	var isValid = true;
	    	 var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
	 	 		if(!canWrite){
	 	 			$scope.isValidated = false;
	 	 			$scope.serviceMessage = "You don't have permissions to Update Fault";
	 	 			return false;
	 	 		}
	 	 		var faultId = $scope.cases.caseFaultId;
	 	 		 var arrayLength = $scope.selection.length;
	 	 		if(angular.isUndefinedOrNull(faultId) || faultId.length == 0 || arrayLength == 0){
	 	 			 $scope.isFaultValid = false;
	 	 			isValid = false;
	 	 			//$scope.serviceMessage = "You have already updated the selected assistance";
	 	 			return false;
	 	 		}
	 	 		
	 	 		if(angular.isUndefinedOrNull($scope.cases.caseFaultDescription)){
	 	 			isValid = false;
	 	 			return false;
	 	 		}
	 	 		
	 	 		if(!angular.isUndefinedOrNull($scope.cases.caseFaultDescription)){
	 	 			if($scope.cases.caseFaultDescription.length == 0){
	 	 				isValid = false;
	 	 				return false;
	 	 			}
	 	 		}
	 	 		if(isValid){
			    	  var inpuObject = {};
			    	  inpuObject.userId= $scope.userId;
			    	  inpuObject.subcaseId=  $scope.subCaseId;
			    	  
			    	  inpuObject.caseFaultId=  $scope.cases.caseFaultId;
			    	  inpuObject.caseId = caseId;
				       console.log("inputobject json " + JSON.stringify(inpuObject));
				       
				            	$http.post(SERVICEADDRESS + '/cases/updatefault', JSON.stringify(inpuObject)).
							        success(function(returnJson) {
							            
							            if (returnJson.isSuccess) {
							            	$scope.subcasesList= returnJson.subcasesInfo;
							            	 $scope.isFaultValid = true;
							            	if($scope.isFromCaseSchedule){
							            			
							            		 //loading list of case faults and services from DB
							            		if(!$scope.cases.isDestinationLocationSet){
							            			var j=0,k=0;
								   	             	for (var i = 0; i < $scope.subcasesList.length; i++) {
								   	             		var isCaseClosedByDifferentIssue = $scope.subcasesList[i].isCaseClosedByDifferentIssue;
							     	            		var serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
							     	            		var faultId = $scope.subcasesList[i].faultId;
							     	            		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6 ){
							     	            			if(isCaseClosedByDifferentIssue){
							     	            				j++;
							     	            			}
							     	            			k++;
							     	            		}
							     	            		
							                		}
								   	             	
									            	if(j == k && k != 0){
									            		$scope.isTowingRequired = false;
									            		$scope.cases.isTowRequested =false;
									            	}
							            		}else{
							            			$scope.isTowingRequired = false;
							            			$scope.cases.isTowRequested = false;
							            		}
							            		$scope.canUpdateFault = false;
							            		if(!$scope.cases.isReportedSameIssue && $scope.cases.isAttendedSolved){
								            		$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
							     	            	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
								            	}
						                	 }
							            	$scope.cases.caseFaultDescription = '';
							            	if(!$scope.isFromPreRequsitie){
									 		       angular.forEach($scope.faults, function(fault, index) {
									 		    	   var tempFaultId = $scope.faults[index].caseFaultId;
									 		    	  fault.checked = false;
									 				    if (tempFaultId == $scope.subCaseFaultId){
									 				    	fault.isDisabled = false;
									 				    }
									 				    if (tempFaultId == $scope.cases.caseFaultId){
									 				    	fault.isDisabled = true;
									 				    }
									 				  });
									 		       
									 		      if($scope.isFromCaseSchedule){
										 		      if($scope.cases.caseFaultId == 6 && !$scope.cases.isDestinationLocationSet ){
									   	 					$scope.isTowingRequired = true;
									            			$scope.cases.isTowRequested = true;
									   	 				}
							                	  }
									 		       
									            	$scope.canUpdateFault =  false;
									            	$scope.isValidated = true;
									            	$scope.cases.caseFaultDescription = "";
									            	$scope.cases.caseNotes = "";
									            	$scope.cases.caseFaultId = null;
									            	$scope.selection.splice(0);
									            	$scope.canUpdateFault = false;
									            	
									            	for(var i =0;i< $scope.faults.length;i++){
										    			 $scope.faults[i].checked = false;
										    		}
									           }else{
									            	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
							     	            	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
									            }
							            	
							            } else {
							            	$scope.isValidated = false;
							                $scope.serviceMessage = returnJson.serviceMessage;		
							                $("html, body").animate({ scrollTop: 0 }, 1000);
							            }
							
							        }).error(function(returnErrorJson, status, headers, config) {
							        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
										if(isFreshTokenGenarated){
											$scope.updateFaultForAssistance(cases); 
										}			            		
							        }); //Error
	 	 		}
		};//updateFaultForAssistance function end
	    
	    
		 /**
      	 * redirect to main case
      	 */
		$scope.addfaultToSubCase = function addfaultToSubCase(subCaseNumber){
			$scope.cases.subcaseId = subCaseNumber;
			 $scope.casesview = '/resources/views/bodytemplates/main_case_body.html';
		};
	    
	    /**
	     * case on job details
	     */
	   $scope.updateCaseOnJobDetails = function updateCaseOnJobDetails(cases){
		   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
	 		if(!canWrite){
	 			$scope.isValidated = false;
	 			$scope.serviceMessage = "You don't have permissions to Update Case On Job Details";
	 			return false;
	 		}
	        var isValid = $scope.tracForm.$valid;
	        var dispatchedDate;
	        var arrivedDate;
	        var arrivalTimeAtLoc;
	        var actualDispatchedTime = cases.dispatchedhh+":"+cases.dispatchedmm;
	        var actualArrivedTime = cases.arrivedhh+":"+cases.arrivedmm;
	        var dispatchedTimeFromLoc;
	        
	        if(cases.isTowRequested){
	        	dispatchedDate =$('#dispatchedTimeFromLoc').val().split("-");
	        	var dispatchedMonth = parseInt(dispatchedDate[1]) - 1;
	        	dispatchedMonth = "0"+dispatchedMonth;
	        	dispatchedTimeFromLoc =  new Date(dispatchedDate[0],dispatchedMonth,dispatchedDate[2], $scope.cases.dispatchedhh,$scope.cases.dispatchedmm,0);
	        	 arrivedDate =$('#arrivalTimeAtLoc').val().split("-");
	        	 var arrivedMonth = parseInt(arrivedDate[1]) - 1;
	        	 arrivedMonth = "0"+arrivedMonth;
		         arrivalTimeAtLoc =  new Date(arrivedDate[0],arrivedMonth,arrivedDate[2], $scope.cases.arrivedhh,$scope.cases.arrivedmm,0);
		         
	        	if(dispatchedTimeFromLoc >= arrivalTimeAtLoc){
	        		$scope.isValidated = false;
	                $scope.serviceMessage = "Arrived Time should be greater than Dispatched time";	
	                $("html, body").animate({ scrollTop: 0 }, 1000);
	                return false;
		        	/*if(parseInt(cases.arrivedhh) < parseInt(cases.dispatchedhh) || actualDispatchedTime ==  actualArrivedTime){
		        		$scope.isValidated = false;
		                $scope.serviceMessage = "Arrived Time should be greater than Dispatched time";	
		                $("html, body").animate({ scrollTop: 0 }, 1000);
		                return false;
		        	}else if(parseInt(cases.arrivedmm) < parseInt(cases.dispatchedmm)){
			        		$scope.isValidated = false;
			                $scope.serviceMessage = "Arrived Time should be greater than Dispatched time";	
			                $("html, body").animate({ scrollTop: 0 }, 1000);
			                return false;
		        	}*/
	        	}
		        
	   		}	
	        console.log("caess json " + JSON.stringify(cases));
	        
	        var caseJobDetails = {};
	        caseJobDetails.userId= $scope.userId;
	        caseJobDetails.subcaseId= $scope.subcaseId;
	        caseJobDetails.caseId= $scope.cases.caseId;
	     
	        caseJobDetails.isDriverAbletoResolve=cases.isDriverAbletoResolve;
	        caseJobDetails.driverComments=cases.driverComments;
	        caseJobDetails.isRequiredServiceProvided=cases.isRequiredServiceProvided;
	        caseJobDetails.serviceComments=cases.serviceComments;
	        caseJobDetails.dispatchedTimeFromLoc= dispatchedTimeFromLoc;
	        caseJobDetails.isTowRequested=cases.isTowRequested;
	        caseJobDetails.arrivalTimeAtLoc=arrivalTimeAtLoc;
	        	
	       console.log("caseJobDetails json " + JSON.stringify(caseJobDetails));
	        
	       if(isValid){
		        	$http.post(SERVICEADDRESS + '/caseOnJob/addOnJobDetails', JSON.stringify(caseJobDetails)).
				        success(function(returnJson) {
				            
				            if (returnJson.isSuccess) {
				            	$scope.isValidated = true;
				            	$scope.caseCloseValidated = true;
				            	$scope.subcasesList = returnJson.subcasesInfo;
				            	if(cases.isTowRequested){
				            		$scope.cases.isTowRequested = false;
				            		$scope.cases.arrivalTimeAtLoc = "";
				            		$scope.cases.dispatchedTimeFromLoc = "";
				            	}
				            	$scope.mainview = '/resources/views/bodytemplates/case_details_body.html';
		     	            	$scope.view = '/resources/views/bodytemplates/case_info_body.html';
				            
				            } else {
				            	$scope.isValidated = false;
				                $scope.serviceMessage = returnJson.serviceMessage;		
				                $("html, body").animate({ scrollTop: 0 }, 1000);
				            }
				
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.caseOnJobDetails(cases); 
							}			            		
				        }); //Error
		        	
	        }//end isvalid
	        
	    };//end of case on job details
	    
	    /**
      	 * redirect to add destination
      	 */
	    $scope.redirectToAddDestination = function redirectToAddDestination(){
	    	

	    	$scope.isServiceValid = true;//diabling previous errors
	    	
	    	var inputJson ={};
	    	inputJson.customerId=$scope.cases.customerId;
	    	
	        if($scope.isTowingRequired){
		    	$http.post(SERVICEADDRESS + '/serviceLocation/fetchVehicleDestinationLocation', JSON.stringify(inputJson)).
		        success(function(returnJson) {
		        	 if (returnJson.isSuccess) {
		        		 if($scope.isInCaseSchedule){
		 	 				$scope.isValidated = true;
		 	 			}else{
		 	 				$scope.caseCloseValidated = true;
		 	 			}
		        		$scope.destinationLocationList = returnJson.destinationLocationList;
		        	 }
		        		 $scope.mainview = '/resources/views/bodytemplates/destinationlocation_body.html';
		        		 
		        }).error(function(returnErrorJson, status, headers, config) {
		        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.caseOnJobDetails(cases); 
					}			            		
		        }); //Error
	    	}else{
	    		$location.path('/caseschedule');
	    	}
	    };
	    
	    $scope.updateCaseRegisteredfaults = function updateCaseRegisteredfaults(cases,schedulestatusID){
	    	
	    	$scope.caseCloseValidated = true;
	    	var isValid = true;
	    	  var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Edit');
		 		if(!canWrite){
		 			$scope.isValidated = false;
		 			$scope.serviceMessage = "You don't have permissions to Update Case Registered faults";
		 			return false;
		 		}
		 		
		 		
		 		if($scope.isCaseClosedByDifferentIssue){	
		 			$scope.serviceMessage = "Assistance closed due to reported issues is different";
		 			isValid = false;
		 			if($scope.isInCaseSchedule){
		 				$scope.isValidated = false;
		 			}else{
		 				$scope.caseCloseValidated = false;
		 			}
		 			//$("html, body").animate({ scrollTop: 0 }, 1000);
		 			return false;
		 		}
		 		
		 		if($scope.isServiceAdded){
		 			 $scope.isServiceValid = false;
		 			$scope.serviceMessgae = "You have already updated the service";
		 			isValid = false;
		 			return false;
		 		}
		 		
		 		 if($scope.tempSelectedServices.length <= 0 ){
		 			 $scope.isServiceValid = false;
		 			$scope.serviceMessgae = "Please choose at least one service";
		 			isValid = false;
		 					return false;
		 		 }
		 		 
		 		if(angular.isUndefinedOrNull($scope.cases.serviceDescription)){
		 			 $scope.isServiceValid = false;
			 		 $scope.serviceMessgae = "Please enter service description";
			 		isValid = false;
			 		 return false;
		 		 }
		 		
		 		if(!angular.isUndefinedOrNull($scope.cases.serviceDescription)){
		 			if($scope.cases.serviceDescription.length == 0){
			 			 $scope.isServiceValid = false;
				 		 $scope.serviceMessgae = "Please enter service description";
				 		isValid = false;
				 		 return false;
		 			}
		 		 }
		 	var servicceObject = {};
		 	servicceObject = $scope.tempSelectedServices[0];
		 	    
	        var caseServiceDetails = {};
	        caseServiceDetails.userId= parseInt($scope.userId);
	        caseServiceDetails.caseServicesList=$scope.tempSelectedServices;
	        caseServiceDetails.caseServiceDescription= cases.serviceDescription;
	        caseServiceDetails.subcaseId= $scope.subCaseId;
	        
	        console.log("caseServiceDetails json " + JSON.stringify(caseServiceDetails));
	       if(isValid){
	           	$http.post(SERVICEADDRESS + '/caseRegistration/editCaseInfo', JSON.stringify(caseServiceDetails)).
				        success(function(returnJson) {
				        	 console.log("returnJson- "+ JSON.stringify(returnJson));
				            if (returnJson.isSuccess) {
				            	 $scope.isValidated = true;
				            	 $scope.caseCloseValidated = true;
				            	 $scope.isServiceValid = true;
				            	 $scope.tempSelectedServices.splice(0);
				            	 $scope.cases.serviceDescription = null;
				            	 $scope.serviceMessgae = "";
				            	 
				            	  $scope.subcasesList = returnJson.subcasesInfo;
				            	  var towingFlag = false;
				            		for (var i = 0; i < $scope.subcasesList.length; i++) {
				   	             		var isCaseClosedByDifferentIssue = $scope.subcasesList[i].isCaseClosedByDifferentIssue;
			     	            		var  serviceID =  $scope.subcasesList[i].caseRegisteredServiceID;
			     	            		var faultId = $scope.subcasesList[i].faultId;
			     	            		if(serviceID == 5 || serviceID == 1 ||serviceID == 12 || faultId == 6){
			     	            			if(!isCaseClosedByDifferentIssue){
			     	            				towingFlag = true;
			     	            			}
			     	            		}
			                		}
				            		
				            		if(towingFlag){
				            			$scope.isTowingRequired = true;
				            			$scope.cases.isTowRequested = true;
				            		}else{
				            			$scope.isTowingRequired = false;
				            			$scope.cases.isTowRequested = false;
				            		}
				            	
				            	 //getting the subcase id where service is not yet updated 
				            	 $scope.selectedSubCaseIndex = $scope.getSubCaseIndex($scope.subcasesList,"isSelected");
				            	 
				            	//getting sub case details
				            	if($scope.selectedSubCaseIndex  != -1){
				            		$scope.subCaseId =  $scope.subcasesList[$scope.selectedSubCaseIndex].subcaseId;
					             	$scope.subCaseFault =  $scope.subcasesList[$scope.selectedSubCaseIndex].faultName;
					             	$scope.subCaseFaultId =   $scope.subcasesList[$scope.selectedSubCaseIndex].caseRegisteredFaultID;
					             	$scope.selectedFaultIndex= $scope.getIndexOf($scope.faults, $scope.subCaseFault,"caseFaultDesc");
					             	$scope.faults[$scope.selectedFaultIndex].checked = true;
					             	$scope.subCaseServiceName =  $scope.subcasesList[$scope.selectedSubCaseIndex].FaultServiceName;
					             	$scope.getServicesList($scope.selectedFaultIndex, $scope.faults);
				            	}
				            	
				            	
				             } else {
				            	 if($scope.isInCaseSchedule){
				 	 				$scope.isValidated = false;
				 	 			}else{
				 	 				$scope.caseCloseValidated = false;
				 	 			}
				                $scope.serviceMessage = returnJson.serviceMessage;	
				                $("html, body").animate({ scrollTop: 0 }, 1000);
				            }
				
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.caseOnJobDetails(cases); 
							}			            		
				        }); //Error
		        	
	       }//isvalid
	    };//end of case on job details
	    
	    /**
	     * Search json and finding the property
	     */
	    $scope.getSubCaseIndex= function getSubCaseIndex(subCaseObject,propertyToSearch){
	    	var l = subCaseObject.length;
			for (var k = 0; k < l; k = k + 1) {
				var isSelected = subCaseObject[k][propertyToSearch];
				if (!isSelected) {
					return k;
				}
			}
			return -1;
	    };
	    
	    /**
	     * Registering case on job details 
	     */
	    $scope.redirectToCaseOnJob= function caseRegisteredServices(cases){
	    	 $scope.casescheduleview = '/resources/views/bodytemplates/case_onJobdetail_body.html';
	    };
	    
	    /**
	     * Registering case on job details 
	     */
	    $scope.caseRegisteredServices = function caseRegisteredServices(cases){
	    	
	    	  var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
		 		if(!canWrite){
		 			$scope.isValidated = false;
		 			$scope.serviceMessage = "You don't have permissions to Add Case Registered Services";
		 			return false;
		 		}
		 		
		 		
	    	var isValid = $scope.tracForm.$valid;
	        
	        var caseRegisteredServicesObject = {};
	        
	        caseRegisteredServicesObject.userId= $scope.userId;
	        caseRegisteredServicesObject.subcaseId =   $scope.subcaseId;
	        caseRegisteredServicesObject.isNoDentsScratches=  cases.isNoDentsScratches;
	        caseRegisteredServicesObject.dentscomments= cases.dentscomments;
	        caseRegisteredServicesObject.isDriverAbletoResolve=  cases.isDriverAbletoResolve;
	        caseRegisteredServicesObject.driverComments=  cases.driverComments;
	        caseRegisteredServicesObject.isRequiredServiceProvided= cases.isRequiredServiceProvided;
	        caseRegisteredServicesObject.serviceComments= cases.serviceComments;
	        
	        console.log("caseJobDetails json " + JSON.stringify(caseJobDetails));
	        
	       if(isValid){
		        	$http.post(SERVICEADDRESS + '/caseOnJob/addOnJobDetails', JSON.stringify(caseJobDetails)).
				        success(function(returnJson) {
				            
				            if (returnJson.isSuccess) {
				            	$scope.isValidated = true;
				            	$location.path('/createcase');
				            } else {
				            	$scope.isValidated = false;
				                $scope.serviceMessage = returnJson.serviceMessage;			                
				            }
				
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.caseOnJobDetails(cases); 
							}			            		
				        }); //Error
		        	
	        }//end isvalid
	        
	    };//end of caseRegisteredServices
	    
	    
	    /**
	     * start Save Customer Review
	     */
	    $scope.saveCustomerReview = function saveCustomerReview(cases){
	    	  var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Cases','moduleName','Add');
		 		if(!canWrite){
		 			$scope.isValidated = false;
		 			$scope.serviceMessage = "You don't have permissions to Add Customer Review";
		 			return false;
		 		}
		 		
		 	var inputObject = {};
		 	inputObject.userId= $scope.userId;
		 	inputObject.caseId= $scope.caseId;
		 	inputObject.customerCareResponse= cases.customerCareResponse;
		 	inputObject.driverResponse= cases.driverResponse;
		 	inputObject.servicesProvided= cases.servicesProvided;
		 	inputObject.overallRating= cases.overallRating;
		 	inputObject.suggestions= cases.suggestions;
		 	
		 	if(angular.isUndefinedOrNull(cases.customerCareResponse)){
		 		$scope.isValidated = false;
                $scope.serviceMessage = "Please rate Customer Care Response";	
		 		return false;
		 	}
		 	if(angular.isUndefinedOrNull(cases.driverResponse)){
		 		$scope.isValidated = false;
                $scope.serviceMessage = "Please rate Driver Response";	
		 		return false;
		 	}
		 	if(angular.isUndefinedOrNull(cases.servicesProvided)){
		 		$scope.isValidated = false;
                $scope.serviceMessage = "Please rate  Services Provided";	
		 		return false;
		 	}
		 	if(angular.isUndefinedOrNull(cases.overallRating)){
		 		$scope.isValidated = false;
                $scope.serviceMessage = "Please rate Overall Rating";	
		 		return false;
		 	}
		 /*	if(angular.isUndefinedOrNull(cases.suggestions)){
		 		$scope.isValidated = false;
                $scope.serviceMessage = "Please enter suggestions";	
		 		return false;
		 	}
		 */
	        
	        console.log("caseJobDetails json " + JSON.stringify(inputObject));
	        
	            	$http.post(SERVICEADDRESS + '/customerStaisfaction/saveCustomerReview', JSON.stringify(inputObject)).
				        success(function(returnJson) {
				            
				            if (returnJson.isSuccess){
				            	$scope.isValidated = true;
				            	$location.path("/caseschedule/0");
				            } else{
				            	$scope.isValidated = false;
				                $scope.serviceMessage = returnJson.serviceMessage;	
				                $("html, body").animate({ scrollTop: 0 }, 1000);
				            }
				
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.saveCustomerReview(cases); 
							}			            		
				        }); //Error
		      
	    };//end of saveCustomerReview
	  
	    /**
	     * end Save Customer Review
	     */
	
    /**
     * Form State Validation
     */
    
    $scope.isFormStateValid = function isFormStateValid() {
    	$scope.customerId = localStorage.getData("customerId");
    	var customerId = $scope.customerId;
    	console.log("isUndefined - "+angular.isUndefined(customerId));
    	if(angular.isUndefined(customerId) || customerId == null){
    		
    		$scope.serviceMessage = "Please fill the customer details of cases";
    		return true;
    	}else{
    		return false;
    	}
    };
    
    /**
     * displaying views of case flow based on conitions
     */
    
	  $scope.displaySelectedView = function(viewToBeDisplayed,isValid,message,currenTab) {
		  var currentPage;
		  if(currenTab == 0 ){
				  $scope.isValidated = true;
				  
				  if(!angular.isUndefinedOrNull($scope.cases.customerLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.customerLocationLongitude))
              		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.customerLocationLatitude,"locationLongitude":$scope.cases.customerLocationLongitude});
				  
				  $timeout(function(){
					  $scope.view = viewToBeDisplayed;
			     }, 0);
				  
 		  }else if(currenTab == 1 &&  $scope.isCustomerRegistered){
				  $scope.isValidated = true; 
				  $scope.view = viewToBeDisplayed;
 		  }else if(currenTab == 2 &&  $scope.isCustomerRegistered &&  $scope.isVehicleRegistered ){
				  $scope.isValidated = true; 
					if(!angular.isUndefinedOrNull($scope.cases.serviceLocationLatitude) && !angular.isUndefinedOrNull($scope.cases.serviceLocationLongitude))
                		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
					
					$timeout(function(){
						  $scope.view = viewToBeDisplayed;
				     }, 0);

 		  }else if(currenTab == 3 &&  $scope.isCustomerRegistered &&  $scope.isVehicleRegistered &&  $scope.isServiceLocRegistered){
				  $scope.isValidated = true; 
				   $scope.view = viewToBeDisplayed;
				  
				  
 		  }else if((currenTab == 4 || currenTab == 5)&&  $scope.isCustomerRegistered &&  $scope.isVehicleRegistered &&  $scope.isServiceLocRegistered){
			  $scope.isValidated = true; 
			   $scope.view = viewToBeDisplayed;
			   $scope.caseId =  $scope.cases.caseId;
			  
 		  }else{
				  $scope.isValidated = false; 
				  $("html, body").animate({ scrollTop: 0 }, 1000);
				  if(!$scope.isCustomerRegistered){
					  currentPage = "customer";
					  $scope.serviceMessage = "Please fill the following "+currentPage+" details"; 
					  return false;
				  }
				  if(!$scope.isVehicleRegistered){
					  currentPage = "vehicle info";
					  $scope.serviceMessage = "Please fill the following "+currentPage+" details"; 
					  return false;
				  }
				  if(!$scope.isServiceLocRegistered){
					  currentPage = "vehicle service location";
					  $scope.serviceMessage = "Please fill the following "+currentPage+" details"; 
					  return false;
				  }
				  if(!$scope.isMainCasegistered){
					  currentPage = "main case";
					  $scope.serviceMessage = "Please fill the following "+currentPage+" details"; 
					  return false;
				  }
		  }  
		};
		
		 /**
	     *Dispaying all case related info
	     */
		 $scope.showCaseInfo = function(cases){
			var caseObject={};
			caseObject.caseId = cases.caseId;
			caseObject.userId = cases.userId;
            console.log("caseDetails json " + JSON.stringify(caseObject));
          
            $http.post(SERVICEADDRESS + '/cases/fetchCaseDetailedInfo', JSON.stringify(caseObject)).
            success(function(returnJson) {
            
                if (returnJson.isSuccess) {
                	 $scope.isValidated = true;
                	$scope.caseDetails = returnJson.casedetails; //customer details
                	$scope.caseInfo.caseView = true;
        			$scope.caseInfo.showCaseview = true;
        			$scope.subcasesList = returnJson.subcasesInfo;
          		}else{
          			 $scope.isValidated = false;
                     $scope.serviceMessage = returnJson.serviceMessage;
                     $("html, body").animate({ scrollTop: 0 }, 1000);
          		}
                return false;
            }).error(function(returnErrorJson, status, headers, config) {
            	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    			if(isFreshTokenGenarated){
    				$scope.getCaseDetails(cases); 
    			}
            }); //error
	};
		
		 /**
	     *Dispaying vehicle related info
	     */
		 $scope.showVehicleInfo = function(cases){
			 $scope.caseInfo.vehicleInfoView = true;
		};
		/**
	     *Dispaying all case related info
	     */
		 $scope.showVehicleServiceInfoView = function(vehiclesInfo){
			 $scope.caseInfo.vehicleServiceInfoView = true;
		};
		/**
	     *Dispaying all case related info
	     */
		 $scope.showCaseView = function(cases){
			 $scope.caseInfo.caseView = true;
		};
		
		/**
	     *Dispaying main view
	     */
		 $scope.showMainView = function(cases){
			 $scope.caseInfo.customerView = false;
			 $scope.caseInfo.vehicleInfoView = false;
			 $scope.caseInfo.vehicleServiceInfoView = false;
			 $scope.caseInfo.caseView = false;
			 $scope.caseInfo.showCaseview = false;
			 $scope.isCaseAssigned = false;
		};
		
		/**
	     *set brand model data
	     */
		$scope.setVehicleBrandModelData = function(cases){
			 	if(!angular.isUndefinedOrNull(cases)){
		 	         var selectedBrand = cases.vehicleBrandId;
		 	        if(!angular.isUndefinedOrNull(selectedBrand)){
	  				 var indexLoc = parseInt($scope.getIndexOfBrand($scope.brandModelList,selectedBrand.toString(),"vehicleBrandName"));
	  				 $scope.brandModels =  $scope.brandModelList[indexLoc].brandModels;
	  	             var brandModelListLength = $scope.brandModels.length;
	  				 $scope.cases.selectedBrandIndex = indexLoc;
	  				 for(var i=0;i<brandModelListLength;i++){
	  						$scope.models.push($scope.brandModels[i].vehicleModelName);	
	  						$scope.modelIds.push($scope.brandModels[i].vehicleModelId);
	  				 }
		 	        }
			 	}
		   };
		   
			/**
		     *set vehicle data
		     */

		 $scope.setVehicleData = function(cases){
			 var vehicle = cases.vehicle;
			 	if(!angular.isUndefinedOrNull(vehicle)){
			 		$scope.platenumberFocus = false;
			 		$scope.isVehicleRegistered = true;
		 	         var selectedBrand = vehicle.vehicleBrandName;
	  				 var indexLoc = parseInt($scope.getIndexOfBrand($scope.brandModelList,selectedBrand.toString(),"vehicleBrandName"));
	  				 $scope.brandModels =  $scope.brandModelList[indexLoc].brandModels;
	  	             var brandModelListLength = $scope.brandModels.length;
	  				 $scope.cases.selectedBrandIndex = indexLoc;
	  				 for(var i=0;i<brandModelListLength;i++){
	  						$scope.models.push($scope.brandModels[i].vehicleModelName);	
	  						$scope.modelIds.push($scope.brandModels[i].vehicleModelId);
	  				 }
	  				 if(indexLoc >= 0 ){
	  					$scope.isBrandValid = true;
	  				 	$scope.isModelValid = true;
	  				 }
	  				$scope.today();
			 	}else{
			 		$scope.isVehicleRegistered = false;
			 	}
			 	
			 	$scope.cases.searchParameterOfVehicle = '';
			 	$scope.isVehcileFound = true;
			 	
				$scope.cases.vehicleId = !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleId : '' ;
				$scope.cases.vin = !angular.isUndefinedOrNull(vehicle) ? vehicle.vin : '' ;
				$scope.cases.vehicleRegisterNo = !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleRegisterNo : '' ; 
				$scope.cases.vehcileCountryISO = !angular.isUndefinedOrNull(vehicle) ? vehicle.countryName : '' ; 
				$scope.cases.vehcileCountryISOCode =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehcileCountryISOCode : '';
				$scope.cases.city =  !angular.isUndefinedOrNull(vehicle) ? vehicle.city : '' ;
				$scope.cases.plateCode =  !angular.isUndefinedOrNull(vehicle) ? vehicle.plateCode : '' ;
				$scope.cases.memberShipTypeId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipTypeId : '' ;
				$scope.cases.vehicleBrandId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleBrandName : '' ;
				$scope.cases.vehicleModelId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelName : '' ;
				$scope.cases.selectedBrandId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleBrandId : '' ;
				$scope.cases.selectedModelId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelId : '' ;
				$scope.cases.vehicleModelYear =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelYear : '' ;
				$scope.cases.memberShipStart  =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipStart : '' ;
				$scope.cases.memberShipEnd   =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipEnd : '' ;
				$scope.cases.dateOfPurchase   =  !angular.isUndefinedOrNull(vehicle) ? vehicle.dateOfPurchase : '' ;
				$scope.cases.registrationDate =  !angular.isUndefinedOrNull(vehicle) ? vehicle.registrationDate : '' ;
				$scope.cases.color =  !angular.isUndefinedOrNull(vehicle) ? vehicle.color : '' ;
				$scope.cases.mileage =  !angular.isUndefinedOrNull(vehicle) ? vehicle.mileage : '' ; 
			 	
				
		   };
		   
			/**
		     *set vehicle data on search 
		     */
		   $scope.setVehicleDataOnsearch = function(vehicle){
			 	$scope.cases.vehicleId = !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleId : '' ;
				$scope.cases.vin = !angular.isUndefinedOrNull(vehicle) ? vehicle.vin : '' ;
				$scope.cases.vehicleRegisterNo = !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleRegisterNo : '' ; 
				$scope.cases.vehcileCountryISO = !angular.isUndefinedOrNull(vehicle) ? vehicle.countryName : '' ; 
				$scope.cases.vehcileCountryISOCode =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehcileCountryISOCode : '';
				$scope.cases.city =  !angular.isUndefinedOrNull(vehicle) ? vehicle.city : '' ;
				$scope.cases.plateCode =  !angular.isUndefinedOrNull(vehicle) ? vehicle.plateCode : '' ;
				$scope.cases.memberShipTypeId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipTypeId : '' ;
				$scope.cases.vehicleBrandId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleBrandName : '' ;
				$scope.cases.vehicleModelId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelName : '' ;
				$scope.cases.selectedBrandId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleBrandId : '' ;
				$scope.cases.selectedModelId =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelId : '' ;
				$scope.cases.vehicleModelYear =  !angular.isUndefinedOrNull(vehicle) ? vehicle.vehicleModelYear : '' ;
				$scope.cases.memberShipStart  =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipStart : '' ;
				$scope.cases.memberShipEnd   =  !angular.isUndefinedOrNull(vehicle) ? vehicle.memberShipEnd : '' ;
				$scope.cases.dateOfPurchase   =  !angular.isUndefinedOrNull(vehicle) ? vehicle.dateOfPurchase : '' ;
				$scope.cases.registrationDate =  !angular.isUndefinedOrNull(vehicle) ? vehicle.registrationDate : '' ;
				$scope.cases.color =  !angular.isUndefinedOrNull(vehicle) ? vehicle.color : '' ;
				$scope.cases.mileage =  !angular.isUndefinedOrNull(vehicle) ? vehicle.mileage : '' ; 
				
		    };
		    
			/**
		     *set vehicle location data
		     */
		    
		    $scope.setLocationData = function(location){
		    	  var location = $scope.cases.serviceLocationId;
		    		var serviceRequiredTime;
			    	if(angular.isUndefinedOrNull(location)){
			    		$scope.isVehcileSLFound = false;
			    		$scope.isServiceLocRegistered = false;
			    		 location = {};
			    		 
			    	}else{
			    		$scope.isVehcileSLFound = true;
			    	}
		    		if(!angular.isUndefinedOrNull(location.serviceRequiredTime) && location.serviceRequiredType == 2){
		    			serviceRequiredTime = new Date(location.serviceRequiredTime);
		    			$scope.cases.hh = serviceRequiredTime.getHours();
		    			$scope.cases.mm = serviceRequiredTime.getMinutes();
		    		}
		    		
			    	$scope.cases.serviceLocationName = !angular.isUndefinedOrNull(location.locationName) ? location.locationName : '' ; 
			    	$scope.cases.serviceLocationArea = !angular.isUndefinedOrNull(location.area) ? location.area : '' ;
			    	$scope.cases.serviceLocationStreet= !angular.isUndefinedOrNull(location.street) ? location.street : '' ;
			    	$scope.cases.locationId = !angular.isUndefinedOrNull(location.locationId) ? location.locationId : '' ;
			    	$scope.cases.serviceLocationBuilding= !angular.isUndefinedOrNull(location.building) ? location.building : '' ;
			    	$scope.cases.serviceLocationLandmark= !angular.isUndefinedOrNull(location.landmark) ? location.landmark : '' ;
			    	$scope.cases.serviceLocationCity= !angular.isUndefinedOrNull(location.city) ? location.city : '' ;
			    	$scope.cases.serviceLocationpoBox= !angular.isUndefinedOrNull(location.pobox) ? location.pobox : '' ;
			    	$scope.cases.serviceLocationCountryName= !angular.isUndefinedOrNull(location.countryName) ? location.countryName : '' ;
			    	$scope.cases.serviceLocationCountryCode = !angular.isUndefinedOrNull(location.serviceLocationCountryCode) ? location.serviceLocationCountryCode : '' ;
			    	$scope.cases.priorityId= !angular.isUndefinedOrNull(location.priorityId) ? location.priorityId : '' ;
			    	$scope.cases.serviceRequiredTypeId= !angular.isUndefinedOrNull(location.serviceRequiredType) ? location.serviceRequiredType : 1 ;
			    	$scope.cases.serviceRequiredTime= !angular.isUndefinedOrNull(location.serviceRequiredTime) ? location.serviceRequiredTime : '' ;
			    	$scope.cases.serviceLocationLatitude = !angular.isUndefinedOrNull(location.locationLatitude) ? location.locationLatitude : '' ; 
			    	$scope.cases.serviceLocationLongitude = !angular.isUndefinedOrNull(location.locationLongitude) ? location.locationLongitude : '' ;
			    	
			    	if(!angular.isUndefinedOrNull(location.locationLatitude) && !angular.isUndefinedOrNull(location.locationLongitude))
			    		$scope.$broadcast('place-marker',{"locationLatitude":$scope.cases.serviceLocationLatitude,"locationLongitude":$scope.cases.serviceLocationLongitude});
			    	
		    	
			    	if(!$scope.isVehcileSLFound){
			    		$scope.cases.priorityId = $scope.prioritys[1].pID;
			    	}
		    };
		    
		    
			/**
		     *set destination location data
		     */
		    $scope.setDestinationLocationData = function(locationObject){
		    	
		    	/*var location={};*/
		    	
		    	if(angular.isUndefinedOrNull(locationObject)){
		    		$scope.isdestinationLocSelected = false;	
		    	}else{
		    		$scope.isdestinationLocSelected = true;	
		    	}
			 	
		    	/*if(locationObject.length > 0)
			 		location = locationObject[0];
			 	else
			 		location = null;*/
			 	
		    	$scope.cases.destinationLocationName = !angular.isUndefinedOrNull(locationObject) ? locationObject.locationName : '' ; 
		    	$scope.cases.destinationArea = !angular.isUndefinedOrNull(locationObject) ? locationObject.area : '' ;
		    	$scope.cases.destinationStreet= !angular.isUndefinedOrNull(locationObject) ? locationObject.street : '' ;
		    	$scope.cases.destinationlocationId = !angular.isUndefinedOrNull(locationObject) ? locationObject.locationId : '' ;
		    	$scope.cases.destinationBuilding= !angular.isUndefinedOrNull(locationObject) ? locationObject.building : '' ;
		    	$scope.cases.destinationLandmark= !angular.isUndefinedOrNull(locationObject) ? locationObject.landmark : '' ;
		    	$scope.cases.destinationLocationCity= !angular.isUndefinedOrNull(locationObject) ? locationObject.city : '' ;
		    	$scope.cases.destinationPoBox= !angular.isUndefinedOrNull(locationObject) ? locationObject.pobox : '' ;
		    	$scope.cases.destinationLocationCountryName= !angular.isUndefinedOrNull(locationObject) ? locationObject.countryName : '' ;
		    	$scope.cases.destinationLocationCountryCode = !angular.isUndefinedOrNull(locationObject) ? locationObject.countryCode : '' ;
		    	
		    };
	
	
		    /**
		     *set values coming from directives
		     */
	$scope.setValuesOfDirectives = function setValuesOfDirectives(value,attribute){
  		$scope.$apply(function () {
  		
  			if(attribute == 'selectedBrandId'){
  				$scope.cases.selectedBrandId = value;
  				return false;
  			}//end 
  			if(attribute == 'selectedBrandIndex'){
  				$scope.cases.selectedBrandIndex = value;
  				return false;
  			}//end 
  			if(attribute == 'selectedModelId'){
  				$scope.cases.selectedModelId = value;
  				return false;
  			}//end 
  			if(attribute == 'isBrandValid'){
  				$scope.isBrandValid = value;
  				return false;
  			}//end
  			
  			if(attribute == 'isModelValid'){
  				$scope.isModelValid = value;
  				return false;
  			}//end
  			
  			if(attribute == 'vehcileCountryISOCode'){
  				$scope.cases.vehcileCountryISOCode = value;
  				return false;
  			}//end 
  			if(attribute == 'isVehicleCountrySelected'){
  				$scope.isVehicleCountrySelected = value;
  				return false;
  			}//end 
  			
  			if(attribute == 'customerLocationCountryCode'){
  				$scope.cases.customerLocationCountryCode = value;
  				return false;
  			}//end 
  			if(attribute == 'isCountrySelected'){
  				$scope.isCountrySelected = value;
  				return false;
  			}//end 
  			if(attribute == 'serviceLocationCountryCode'){
  				$scope.cases.serviceLocationCountryCode = value;
  				return false;
  			}
  			if(attribute == 'isSLCountrySelected'){
  				$scope.cases.serviceLocationCountryCode = value;
  				return false;
  			}
  		
  			if(attribute == 'isdestinationLocSelected'){
  				$scope.isdestinationLocSelected = value;
  				return false;
  			}
  			
  			if(attribute == 'destinationLocationCountryCode'){
  				$scope.cases.destinationLocationCountryCode = value;
  				return false;
  			}
  		 });
  		
	  };	
  			
	
	/**
	 *  set the values from google map
	 */
	
	$scope.setValues = function setValues(commonService,page){
  		$scope.$apply(function () {
  			if(page == 'contactdetails'){
  			$scope.cases.street = commonService.street;
  			$scope.cases.building = commonService.building;
  			$scope.cases.landmark = commonService.landmark;
  			$scope.cases.customerLocationCity = commonService.city;
  			$scope.cases.poBox= commonService.poBox;
  			$scope.cases.area= commonService.area;
  			$scope.cases.customerLocationCountryName= commonService.countryName;
  			$scope.cases.customerLocationCountryCode= commonService.countryCode;
  			$scope.cases.customerLocationLatitude = commonService.latitude;
  			$scope.cases.customerLocationLongitude = commonService.longitude;
  		}//end contact
  			if(page == 'destinationlocation'){
  				$scope.cases.destinationArea= commonService.area;
  	  			$scope.cases.destinationStreet = commonService.street;
  	  			$scope.cases.destinationBuilding = commonService.building;
  	  			$scope.cases.destinationLandmark = commonService.landmark;
  	  			$scope.cases.destinationLocationCity = commonService.city;
  	  			$scope.cases.destinationPoBox= commonService.poBox;
  	  			$scope.cases.destinationLocationCountryName= commonService.countryName;
  	  			$scope.cases.destinationLocationCountryCode= commonService.countryCode;
	  	  		$scope.cases.destLocLatitude = commonService.latitude;
	  			$scope.cases.destLocLongitude = commonService.longitude;
	  		
  	  	}//end destinationlocation
  			if(page == 'vehicleservicelocation'){
  				
  				$scope.cases.serviceLocationArea= commonService.area;
  	  			$scope.cases.serviceLocationStreet = commonService.street;
  	  			$scope.cases.serviceLocationBuilding = commonService.building;
  	  			$scope.cases.serviceLocationLandmark = commonService.landmark;
  	  			$scope.cases.serviceLocationCity = commonService.city;
  	  			$scope.cases.serviceLocationpoBox= commonService.poBox;
  	  			$scope.cases.serviceLocationCountryName= commonService.countryName;
  	  			$scope.cases.serviceLocationCountryCode= commonService.countryCode;
	  	  		$scope.cases.serviceLocationLatitude = commonService.latitude;
	  			$scope.cases.serviceLocationLongitude = commonService.longitude;
  	  	}//end vehicleservicelocation
  			
      });
  		
  };
  
  /**
   *to slide page view to top
   */
  $scope.slideToTop = function slideToTop(){
	  $("html, body").animate({ scrollTop: 0 }, 1000);
  };
  
  /**
   *to slide page view to bottom
   */
  $scope.scrollBottom= function scrollBottom(){
	var $target = $('html,body'); 
	var window_height = $(window).height();
	var document_height = $(document).height();
	$target.animate({scrollTop: window_height + document_height}, 2000);
  };
  
  /* * * * * * * * * * * *
	 * fucntion to serach json and returm value
	 * 
	 * * * * * * * * * * * */
  $scope.getValueFromJson =  function (jsonToSearch, comparableValue, prop,valueToGet) {
		var l = jsonToSearch.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = jsonToSearch[k][prop];
			var searcValue = comparableValue;
			if (indexValue == searcValue) {
				return jsonToSearch[k][valueToGet];
			}
		}
		return false;
	};
	
	
	//json serach to find out location where the value resides
	$scope.getIndexOfBrand =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop].trim().toString();
			var searcValue = coparableValue.trim().toString();
			if (indexValue.indexOf(searcValue) >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOf =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop].toString().trim();
			var searcValue = coparableValue.toString().trim();
			if (indexValue.indexOf(searcValue) >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOfInt =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop].toString().trim();
			var searcValue = coparableValue.toString().trim();
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOfValue =  function (arr, coparableValue) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
		
			var indexValue = arr[k].toString().trim();
			var searcValue = coparableValue.toString().trim();
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOfFault =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop] - "+ arr[k][prop]);
			var indexValue = arr[k][prop].toString().trim();
			var searcValue = coparableValue.toString().trim();
			
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to access permissions
	$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop1].trim().toLowerCase();
			var searcValue = comparableValue.trim().toLowerCase();
			if (indexValue.indexOf(searcValue) >= 0) {
				return arr[k][prop2];
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.searchByMultipleValues =  function (arr, comparableValue1, comparableValue2,prop1,prop2) {
		var l = arr.length;
		var isSubCaseFound = false;
		var isSubCaseServiceFound = false;
		var indexObject ={};
		indexObject.subCaseIndex = -1;
		indexObject.subCaseServiceIndex = -1;
		for (var k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop1] - "+ arr[k][prop1]);
			var indexValue1 = arr[k][prop1];
			var indexValue2 = arr[k][prop2];
			
			if (indexValue1 == comparableValue1 ){
				indexObject.subCaseIndex = k;
				isSubCaseFound = true;
			}
			if(indexValue1 == comparableValue1 && indexValue2 == comparableValue2) {	
				indexObject.subCaseServiceIndex = k;
				isSubCaseServiceFound = true;
			}
			
			if(isSubCaseFound && isSubCaseServiceFound)
				return indexObject;
		}
		return indexObject;
	};//end
	
	
	//json serach to find out location where the value resides
	$scope.searchByMultipleParameters =  function (arr, comparableValue, prop1,prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop1] - "+ arr[k][prop1]);
			var indexValue1 = arr[k][prop1];
			var indexValue2 = arr[k][prop2];
			var searcValue = comparableValue;
			
			if (indexValue1 == searcValue || indexValue2 == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	/**
	* Fetch List of Cases Info
	* 
	*/
	$scope.fetchListOfCases = function fetchListOfCases(casetype,startIndex,endIndex,loadNextRecords){
	
		var casesRequest = {
			typeId:casetype,
			userId:$scope.userId,
			startIndex:startIndex,
			endIndex:endIndex
		};
	
		console.log('/cases/listAllCases'+JSON.stringify(casesRequest));
			$http.post(SERVICEADDRESS + '/cases/listAllCases', JSON.stringify(casesRequest)).
			success(function(returnJson) {
				
					if (returnJson.isSuccess) {
					
						
						if(loadNextRecords){
							var casescheduleList = $scope.casescheduleList;
							console.log("before - "+$scope.casescheduleList.length);
							var newCasescheduleList = returnJson.caseslist;
							$scope.$parent.casescheduleList = casescheduleList.concat(newCasescheduleList);
							//$scope.$apply();
							console.log("after - "+$scope.casescheduleList.length);
							$scope.totalItems = $scope.casescheduleList.length;
							refreshCasesData($scope.casescheduleList);
						}else{
							$scope.ifNoRecordsFound = false;
							$scope.casescheduleList = returnJson.caseslist;
							$scope.totalItems = $scope.casescheduleList.length;
							$scope.casesinquecount   = returnJson.casesinquecount;
							$scope.pendingcasescount = returnJson.pendingcasescount;
							$scope.closedcasescount  = returnJson.closedcasescount;
							
							if($scope.totalItems<$scope.currentPage*$scope.maxSize){
								$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.totalItems);
							}else{
									$scope.casesFilterList = $scope.casescheduleList.slice(($scope.indexstart*$scope.maxSize),$scope.currentPage*$scope.maxSize);	
							}
							
							console.log('Response'+JSON.stringify(returnJson));
							$scope.currentPage = 1;
						}
						
						
				
					} else {
						$scope.ifNoRecordsFound = true;
						$scope.serviceMessage = returnJson.serviceMessage;
					
					}
		
					$scope.casesinquecount = returnJson.casescountresult.casesinquecount;
					$scope.pendingcasescount = returnJson.casescountresult.pendingcasescount;
					$scope.closedcasescount  = returnJson.casescountresult.closedcasescount;

			}).error(function(returnErrorJson, status, headers, config) {
				isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.fetchListOfCases(casetype,startIndex,endIndex,loadNextRecords); 
				}
			}); //error
	}; //end of fetchListOfCases 
	
		$scope.pageCount = function () {
		   return Math.ceil($scope.casesList.length / $scope.maxSize);
		 };
	
		$scope.pageChanged = function(listOfItems) {
			var currentPage = $scope.currentPage;
			console.log('currentPage : ' + currentPage);
			console.log('currentPage % 5 : ' + currentPage % 5);
			if(currentPage % 5 == 0 && currentPage >= 5){
				var startIndex =  (currentPage * $scope.maxSize)+ 1;
				var endIndex =  (currentPage + 5) * $scope.maxSize;
				$scope.fetchListOfCases($scope.casetype,startIndex,endIndex,true); 
			}else{
				refreshCasesData(listOfItems);
			}
			
		};
	
		function refreshCasesData(listOfItems){
				$scope.indexstart;
				$scope.indexstart = $scope.currentPage-1;
			
				if($scope.totalItems<$scope.currentPage*$scope.maxSize){
						$scope.$parent.casesFilterList = listOfItems.slice(($scope.indexstart*$scope.maxSize),$scope.totalItems);
					
				}else{
						$scope.$parent.casesFilterList = listOfItems.slice(($scope.indexstart*$scope.maxSize),$scope.currentPage*$scope.maxSize);	
					
				}
			
		} 

		
		$scope.getVehicleDetails = function getVehicleDetails(vehiclesObject,searchValue){
			
			/*if(angular.isUndefinedOrNull(searchValue) || searchValue.length == 0){
				$scope.isVehcileFound= false;
				$scope.serviceMessage= "Please enter vin/plate code";
				$("html, body").animate({ scrollTop: 0 }, 1000);
				return false;
			}
			*/
			
			if(!angular.isUndefinedOrNull(searchValue) || searchValue.length > 0){
				 searchValue = searchValue.split(",")[0];
			}
			
			 var indexLoc= $scope.searchByMultipleParameters(vehiclesObject,searchValue,"vin","vehicleRegisterNo");
		  		   if(indexLoc  >= 0){
		  			 	$scope.cases.vehicle = $scope.vehiclesList[indexLoc];
		  			 	$scope.isVehcileFound= true;
		  				$scope.setVehicleDataOnsearch(vehiclesObject[indexLoc]);
		  				 var selectedBrand = $scope.cases.vehicleBrandId;
		  				 var indexLoc = parseInt($scope.getIndexOf($scope.brandModelList,selectedBrand.toString(),"vehicleBrandName"));
		  				 $scope.brandModels =  $scope.brandModelList[indexLoc].brandModels;
		  	             var brandModelListLength = $scope.brandModels.length;
		  				 $scope.cases.selectedBrandIndex = indexLoc;
		  				 for(var i=0;i<brandModelListLength;i++){
		  						console.log("vehicleModelName - "+$scope.brandModels[i].vehicleModelName);
		  						$scope.models.push($scope.brandModels[i].vehicleModelName);	
		  						$scope.modelIds.push($scope.brandModels[i].vehicleModelId);
		  				 }
		  		   }else{
		  			   $scope.cases.vehicle = 0;
		  			    $scope.cases.vehicleId = '' ;
						$scope.cases.vin = '' ;
						$scope.cases.vehicleRegisterNo = '' ; 
						$scope.cases.vehcileCountryISO = '' ; 
						$scope.cases.vehcileCountryISOCode =  '';
						$scope.cases.city =  '' ;
						$scope.cases.plateCode =  '' ;
						$scope.cases.memberShipTypeId =  '' ;
						$scope.cases.vehicleBrandId =  '' ;
						$scope.cases.vehicleModelId =  '' ;
						$scope.cases.selectedBrandId =  '' ;
						$scope.cases.selectedModelId =   '' ;
						$scope.cases.vehicleModelYear =  '' ;
						$scope.cases.memberShipStart  =  '' ;
						$scope.cases.memberShipEnd   =  '' ;
						$scope.cases.dateOfPurchase   =  '' ;
						$scope.cases.registrationDate = '' ;
						$scope.cases.color =  '' ;
						$scope.cases.mileage =  '' ;
		  				$scope.isVehcileFound= false;
		  				$scope.serviceMessage= "No vehicle found";
		  				return false;
		  		   }
		};
		
		
		/**
		 * To compare dates
		 * it returns returnJson with success or failure data
		 * 
		 * @param  truck
		 * @return returnJson
		 */
	    
		 $scope.compareDate = function (firstDate,secondDate){
		   	 if(!angular.isUndefinedOrNull(firstDate) && !angular.isUndefinedOrNull(secondDate)){
		   		
		   		 if(new Date(firstDate) < new Date(secondDate)){
						return true;
					}
		   		 
		   	 }
		   	return false;
		   };//end
		   
		   /**
			 * To compare dates
			 * it returns returnJson with success or failure data
			 * 
			 * @param  firstDate & secondDate
			 * @return returnJson
			 */
		    
			 $scope.compareModelYearDate = function (firstDate,secondDate){
			   	 if(!angular.isUndefinedOrNull(firstDate) && !angular.isUndefinedOrNull(secondDate)){
			   		var fromattedFirstdate = new Date(firstDate);
			   		
			   		var fristDateyear = fromattedFirstdate.getFullYear();
			   		if(secondDate.length == 5){
			   		 if(fristDateyear < secondDate){
							return true;
						}
			   		}else{
			   			var fromattedSeconddate = new Date(secondDate);
			   			if( fromattedFirstdate < fromattedSeconddate){
			   				return true;
			   			}
			   		}
			   		 
			   	 }
			   	return false;
			   };//end
		   
		   
	    $scope.validateVehicle = function validateVehicle(vehicle){
	    	if(angular.isUndefinedOrNull(vehicle) || vehicle == -1){
	        	return true;
	        }
	    	return false;
	    };
	    
	    $scope.validateVehicleSL = function validateVehicleSL(vehiclesl){
	    	if(angular.isUndefinedOrNull(vehiclesl) || vehiclesl == -1){
	    			return true;
	        }
	    	return false;
	    };
	  
	    
		
		$scope.selectedCount = function() {
		    var i, selectedCount = 0;
		    for (i = 0; i < $scope.faults.length; i++) {
		      if ($scope.faults[i].checked) {
		        selectedCount += 1;
		      }
		    }
		    return selectedCount;
		  };
			
	
			  
		    /**
		     * logout
		     */
		 
		   $scope.logoutUser = function logoutUser() {
		   	$http.defaults.headers.common['Authorization'] = "bearer "+ accessToken + " " +  userId;
				//service call
				$http.post(SERVICEADDRESS+'/logout').
				  success(function(returnJson) {
					    	
					    	if(returnJson.isLoggedOut){
					    		localStorage.clearStorage();
					    		window.location.href = HOSTADDRESS+"/login.html";
					    	
					    	}else{
					    		
					    	}
					    	
				  }).error(function(returnErrorJson, status, headers, config) {
					  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.logoutUser(); 
						}
				 });//error
				  
			  };//logoutUser function end
				  
				  
		  /**
		     * redirecting on clicking the  side menu
		     */
		  
		  $scope.redirectToUrl = function(urlToBeRedirected) {
			  $location.path(urlToBeRedirected);
		  };
		  
		  $scope.redirectToMainCase = function(urlToBeRedirected) {
			  $scope.mainview = urlToBeRedirected;
		  };
		
		  $scope.redirectToCaseDetails = function(urlToBeRedirected,customerMobileNo,isFromCaseSchedule,isCustomer,isForScheduleView) {
			  casedDetailsObject.store("customerMobileNo",customerMobileNo);
			  casedDetailsObject.store("isFromCaseSchedule",isFromCaseSchedule);
			  casedDetailsObject.store("isFromPreRequsitie",!isFromCaseSchedule);
			  casedDetailsObject.store("isForScheduleView",isForScheduleView);
			  casedDetailsObject.store("isCustomer",isCustomer);
			  $window.open('http://localhost:8888/resources/views/index.html#/createcase/:'+customerMobileNo+'/:'+isFromCaseSchedule+'/:'+!isFromCaseSchedule+'/:'+isCustomer, '_blank');
		 };
		 
		 $scope.openCaseDetailsPage = function(urlToBeRedirected,customerMobileNo,isCustomer,isForScheduleView) {
		
			 var locationPath = urlToBeRedirected+'/'+customerMobileNo+'/'+isCustomer+'/'+isForScheduleView;
			 $location.path(locationPath);
				
		 };
			
		 $scope.updateScheduleTime = function(serviceRequiredTypeId) {
			
			 if(serviceRequiredTypeId == 2){
				 $scope.maxScheduledDate = $scope.addDays(new Date(), 3);
			 }
			 console.log("maxScheduledDate - "+$scope.maxScheduledDate);
			 
		  };
		
		  $scope.addDays = function(theDate, days) {
			    return new Date(theDate.getTime() + days*24*60*60*1000);
		  };

			  
		  /**
		     * Date Function
		     */
		    
		    $scope.today = function() {
				$scope.dt = new Date();
			};
			$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			// Disable weekend selection
			$scope.disabled = function(date, mode) {
				return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
			};

			$scope.toggleMin = function() {
				$scope.minDate = $scope.minDate ? null : new Date();
			};
			
			$scope.toggleMin();

			$scope.open = function($event) {
				
				$event.preventDefault();
				$event.stopPropagation();
				console.log(" $scope.opened", $scope.opened);
				$scope.opened = true;
			};
			
		
			$scope.dateOptions = {
					formatYear: 'yyyy',
					startingDay: 1
			};
			
			$scope.datepickerYearOptions = {
					'formatYear': 'yyyy',
					'startingDay' : 0,
					'minMode':'year'
				    
			};//end
			
			$scope.formats = ['dd-MM-yyyy','yyyy/MM/dd', 'yyyy-MM-dd hh: mm', 'dd.MM.yyyy', 'shortDate','yyyy','yyyy-MM-dd'];
			$scope.format = $scope.formats[6];
			$scope.timPickerformat = $scope.formats[1];
			$scope.yearformat = $scope.formats[5];
			
			
			//case schedule timer initilaize
			
			  $scope.keyPresses = 0;
			  $scope.increment = function(){
			    $scope.keyPresses++;
			  };
			  

			    /* Timer Code*/ 
				$scope.blink = true;
				$scope.timerColor = {};
				$scope.deadlineMillis = 0;
				$scope.timerRunning = false;
				$scope.timerColor.color = 'timer-green';

				$scope.stopTimer = function () {
					console.log("stopTimer");
					$scope.$broadcast('timer-stop');
					$scope.timerColor = {};
					$scope.deadlineMillis = 0;
					$scope.timerRunning = false;
				};

				$scope.$on('timer-tick', function (event, data) {
			
					if ($scope.timerRunning && data.millis >= $scope.deadlineMillis) {
						 $scope.$apply(function() {
							 $scope.timerColor.color = 'timer-green';
						 });	 
					}

				});

				$scope.startTimer = function (isExceeded,differnceTime) {
					console.log("startTimer");
					$scope.$broadcast('timer-start');
					$scope.timerRunning = true;
					if(!isExceeded){ 
						$scope.deadlineMillis += differnceTime;
					}   	
				};

				$scope.countDownDone = function () {
					console.log("count done");
					$scope.$broadcast('timer-start');
					$scope.timerRunning = true;
					$scope.deadlineMillis = 0 ;
				};
				
				 $scope.init = function(){
					    $scope.getCountriesForCase();
					  
					    if(angular.isUndefinedOrNull($scope.cases)){
						 	$scope.cases= {};
						 	$scope.cases.isExisting = 1;
						 	$scope.cases.serviceRequiredTypeId = 1;
						 	$scope.cases.vehicle = -1 ;
						 	$scope.cases.serviceLocationId = -1;
						 	$scope.cases.saveForReference = true;
					    }
					};
					
				
				/* End Timer Code*/ 
				
}]);// End Controller

TRACAPP.directive('timer', ['$compile', function ($compile) {
	return  {
		restrict: 'EA',
		replace: false,
		scope: {
			interval: '=interval',
			startTimeAttr: '=startTime',
			endTimeAttr: '=endTime',
			countdownattr: '=countdown',
			finishCallback: '&finishCallback',
			autoStart: '&autoStart',
			language: '@?',
			maxTimeUnit: '='
		},
		controller: ['$scope', '$element', '$attrs', '$timeout', 'I18nService', '$interpolate','$route', function ($scope, $element, $attrs, $timeout, I18nService, $interpolate,$route) {

			// Checking for trim function since IE8 doesn't have it
			// If not a function, create tirm with RegEx to mimic native trim
			if (typeof String.prototype.trim !== 'function') {
				String.prototype.trim = function () {
					return this.replace(/^\s+|\s+$/g, '');
				};
			}

			//angular 1.2 doesn't support attributes ending in "-start", so we're
			//supporting both "autostart" and "auto-start" as a solution for
			//backward and forward compatibility.
			$scope.autoStart = $attrs.autoStart || $attrs.autostart;


			$scope.language = $scope.language || 'en';

			//allow to change the language of the directive while already launched
			$scope.$watch('language', function() {
				i18nService.init($scope.language);
			});

			//init momentJS i18n, default english
			var i18nService = new I18nService();
			i18nService.init($scope.language);

			if ($element.html().trim().length === 0) {
				$element.append($compile('<span>' + $interpolate.startSymbol() + 'millis' + $interpolate.endSymbol() + '</span>')($scope));
			} else {
				$element.append($compile($element.contents())($scope));
			}

			$scope.startTime = null;
			$scope.endTime = null;
			$scope.timeoutId = null;
			$scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) >= 0 ? parseInt($scope.countdownattr, 10) : undefined;
			$scope.isRunning = false;

			$scope.$on('timer-start', function () {
				$scope.start();
			});

			$scope.$on('timer-resume', function () {
				$scope.resume();
			});

			$scope.$on('timer-stop', function () {
				$scope.stop();
			});

			$scope.$on('timer-clear', function () {
				$scope.clear();
			});

			$scope.$on('timer-reset', function () {
				$scope.reset();
			});

			$scope.$on('timer-set-countdown', function (e, countdown) {
				$scope.countdown = countdown;
			});

			function resetTimeout() {
				if ($scope.timeoutId) {
					clearTimeout($scope.timeoutId);
				}
			}

			$scope.$watch('startTimeAttr', function(newValue, oldValue) {
				if (newValue !== oldValue && $scope.isRunning) {
					$scope.start();
				}
			});

			$scope.start = $element[0].start = function () {
				$scope.startTime = $scope.startTimeAttr ? moment($scope.startTimeAttr) : moment();
				$scope.endTime = $scope.endTimeAttr ? moment($scope.endTimeAttr) : null;
				if (!$scope.countdown) {
					$scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) > 0 ? parseInt($scope.countdownattr, 10) : undefined;
				}
				resetTimeout();
				tick();
				$scope.isRunning = true;
			};

			$scope.resume = $element[0].resume = function () {
				resetTimeout();
				if ($scope.countdownattr) {
					$scope.countdown += 1;
				}
				$scope.startTime = moment().diff((moment($scope.stoppedTime).diff(moment($scope.startTime))));
				tick();
				$scope.isRunning = true;
			};

			$scope.stop = $scope.pause = $element[0].stop = $element[0].pause = function () {
				var timeoutId = $scope.timeoutId;
				$scope.clear();
				$scope.$emit('timer-stopped', {timeoutId: timeoutId, millis: $scope.millis, seconds: $scope.seconds, minutes: $scope.minutes, hours: $scope.hours, days: $scope.days});
			};

			$scope.clear = $element[0].clear = function () {
				// same as stop but without the event being triggered
				$scope.stoppedTime = moment();
				resetTimeout();
				$scope.timeoutId = null;
				$scope.isRunning = false;

				$scope.startTime = null;
				$scope.endTime = null;
				$scope.timeoutId = null;
				$scope.countdownattr = null;
				$scope.countdown = null;
				$scope.isRunning = false;

			};

			$scope.reset = $element[0].reset = function () {
				$scope.startTime = $scope.startTimeAttr ? moment($scope.startTimeAttr) : moment();
				$scope.endTime = $scope.endTimeAttr ? moment($scope.endTimeAttr) : null;
				$scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) > 0 ? parseInt($scope.countdownattr, 10) : undefined;
				resetTimeout();
				tick();
				$scope.isRunning = false;
				$scope.clear();
			};

			$element.bind('$destroy', function () {
				resetTimeout();
				$scope.isRunning = false;
			});


			function calculateTimeUnits() {
				var timeUnits = {}; //will contains time with units

				if ($attrs.startTime !== undefined){
					$scope.millis = moment().diff(moment($scope.startTimeAttr));
				}

				timeUnits = i18nService.getTimeUnits($scope.millis);

				// compute time values based on maxTimeUnit specification
				if (!$scope.maxTimeUnit || $scope.maxTimeUnit === 'day') {
					$scope.seconds = Math.floor(($scope.millis / 1000) % 60);
					$scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
					$scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
					$scope.days = Math.floor((($scope.millis / (3600000)) / 24));
					$scope.months = 0;
					$scope.years = 0;
				} else if ($scope.maxTimeUnit === 'second') {
					$scope.seconds = Math.floor($scope.millis / 1000);
					$scope.minutes = 0;
					$scope.hours = 0;
					$scope.days = 0;
					$scope.months = 0;
					$scope.years = 0;
				} else if ($scope.maxTimeUnit === 'minute') {
					$scope.seconds = Math.floor(($scope.millis / 1000) % 60);
					$scope.minutes = Math.floor($scope.millis / 60000);
					$scope.hours = 0;
					$scope.days = 0;
					$scope.months = 0;
					$scope.years = 0;
				} else if ($scope.maxTimeUnit === 'hour') {
					$scope.seconds = Math.floor(($scope.millis / 1000) % 60);
					$scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
					$scope.hours = Math.floor($scope.millis / 3600000);
					$scope.days = 0;
					$scope.months = 0;
					$scope.years = 0;
				} else if ($scope.maxTimeUnit === 'month') {
					$scope.seconds = Math.floor(($scope.millis / 1000) % 60);
					$scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
					$scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
					$scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
					$scope.months = Math.floor((($scope.millis / (3600000)) / 24) / 30);
					$scope.years = 0;
				} else if ($scope.maxTimeUnit === 'year') {
					$scope.seconds = Math.floor(($scope.millis / 1000) % 60);
					$scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
					$scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
					$scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
					$scope.months = Math.floor((($scope.millis / (3600000)) / 24 / 30) % 12);
					$scope.years = Math.floor(($scope.millis / (3600000)) / 24 / 365);
				}
				// plural - singular unit decision (old syntax, for backwards compatibility and English only, could be deprecated!)
				$scope.secondsS = ($scope.seconds === 1) ? '' : 's';
				$scope.minutesS = ($scope.minutes === 1) ? '' : 's';
				$scope.hoursS = ($scope.hours === 1) ? '' : 's';
				$scope.daysS = ($scope.days === 1)? '' : 's';
				$scope.monthsS = ($scope.months === 1)? '' : 's';
				$scope.yearsS = ($scope.years === 1)? '' : 's';


				// new plural-singular unit decision functions (for custom units and multilingual support)
				$scope.secondUnit = timeUnits.seconds;
				$scope.minuteUnit = timeUnits.minutes;
				$scope.hourUnit = timeUnits.hours;
				$scope.dayUnit = timeUnits.days;
				$scope.monthUnit = timeUnits.months;
				$scope.yearUnit = timeUnits.years;

				//add leading zero if number is smaller than 10
				$scope.sseconds = $scope.seconds < 10 ? '0' + $scope.seconds : $scope.seconds;
				$scope.mminutes = $scope.minutes < 10 ? '0' + $scope.minutes : $scope.minutes;
				$scope.hhours = $scope.hours < 10 ? '0' + $scope.hours : $scope.hours;
				$scope.ddays = $scope.days < 10 ? '0' + $scope.days : $scope.days;
				$scope.mmonths = $scope.months < 10 ? '0' + $scope.months : $scope.months;
				$scope.yyears = $scope.years < 10 ? '0' + $scope.years : $scope.years;

			}

			//determine initial values of time units and add AddSeconds functionality
			if ($scope.countdownattr) {
				$scope.millis = $scope.countdownattr * 1000;

				$scope.addCDSeconds = $element[0].addCDSeconds = function (extraSeconds) {
					$scope.countdown += extraSeconds;
					$scope.$digest();
					if (!$scope.isRunning) {
						$scope.start();
					}
				};

				$scope.$on('timer-add-cd-seconds', function (e, extraSeconds) {
					$timeout(function () {
						$scope.addCDSeconds(extraSeconds);
					});
				});

				$scope.$on('timer-set-countdown-seconds', function (e, countdownSeconds) {
					if (!$scope.isRunning) {
						$scope.clear();
					}

					$scope.countdown = countdownSeconds;
					$scope.millis = countdownSeconds * 1000;
					calculateTimeUnits();
				});
			} else {
				$scope.millis = 0;
			}
			calculateTimeUnits();

			var tick = function tick() {

				$scope.millis = moment().diff($scope.startTime);
				var adjustment = $scope.millis % 1000;

				if ($scope.endTimeAttr) {
					$scope.millis = moment($scope.endTime).diff(moment());
					adjustment = $scope.interval - $scope.millis % 1000;
				}

				if ($scope.countdownattr) {
					$scope.millis = $scope.countdown * 1000;
				}

				if ($scope.millis < 0) {
					$scope.stop();
					$scope.millis = 0;
					calculateTimeUnits();
					if($scope.finishCallback) {
						$scope.$eval($scope.finishCallback);
					}
					return;
				}
				calculateTimeUnits();

				//We are not using $timeout for a reason. Please read here - https://github.com/siddii/angular-timer/pull/5
				$scope.timeoutId = setTimeout(function () {
					tick();
					$scope.$digest();
				}, $scope.interval - adjustment);
				
				if(!$scope.$$phase) {
					 $timeout(function() {
						 $scope.$emit('timer-tick', {timeoutId: $scope.timeoutId, millis: $scope.millis});
					 });
				}

				if ($scope.countdown > 0) {
					$scope.countdown--;
				}
				else if ($scope.countdown <= 0) {
					$scope.stop();
					if($scope.finishCallback) {
						$scope.$eval($scope.finishCallback);
					}
				}
			};

			if ($scope.autoStart === undefined || $scope.autoStart === true) {
				$scope.start();
			}
		}]
	};
}]);

/* commonjs package manager support (eg componentjs) */
if (typeof module !== "undefined" && typeof exports !== "undefined" && module.exports === exports){
	module.exports = timerModule;
}



TRACAPP.factory('I18nService', function() {

	var I18nService = function() {};

	I18nService.prototype.language = 'en';
	I18nService.prototype.timeHumanizer = {};

	I18nService.prototype.init = function init(lang){
		this.language = lang;
		//moment init
		moment.locale(this.language); //@TODO maybe to remove, it should be handle by the user's application itself, and not inside the directive

		//human duration init, using it because momentjs does not allow accurate time (
		// momentJS: a few moment ago, human duration : 4 seconds ago
		this.timeHumanizer = humanizeDuration.humanizer({
			language: this.language,
			halfUnit:false
		});
	};

	/**
	 * get time with units from momentJS i18n
	 * @param {int} millis
	 * @returns {{millis: string, seconds: string, minutes: string, hours: string, days: string, months: string, years: string}}
	 */
	I18nService.prototype.getTimeUnits = function getTimeUnits(millis) {
		var diffFromAlarm = Math.round(millis/1000) * 1000; //time in milliseconds, get rid of the last 3 ms value to avoid 2.12 seconds display

		var time = {};

		if (typeof this.timeHumanizer != 'undefined'){
			time = {
					'millis' : this.timeHumanizer(diffFromAlarm, { units: ["milliseconds"] }),
					'seconds' : this.timeHumanizer(diffFromAlarm, { units: ["seconds"] }),
					'minutes' : this.timeHumanizer(diffFromAlarm, { units: ["minutes", "seconds"] }) ,
					'hours' : this.timeHumanizer(diffFromAlarm, { units: ["hours", "minutes", "seconds"] }) ,
					'days' : this.timeHumanizer(diffFromAlarm, { units: ["days", "hours", "minutes", "seconds"] }) ,
					'months' : this.timeHumanizer(diffFromAlarm, { units: ["months", "days", "hours", "minutes", "seconds"] }) ,
					'years' : this.timeHumanizer(diffFromAlarm, { units: ["years", "months", "days", "hours", "minutes", "seconds"] })
			};
		}
		else {
			console.error('i18nService has not been initialized. You must call i18nService.init("en") for example');
		}

		return time;
	};

	return I18nService;
});


