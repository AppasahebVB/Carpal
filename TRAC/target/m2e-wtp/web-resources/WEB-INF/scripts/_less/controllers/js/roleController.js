/*
var TRACAPP =  angular.module("TRACAPP", []);*/

TRACAPP.controller("roleController", function($scope,$http,localStorage,$location,$timeout,$route,dataService) {
	    $scope.displayName = localStorage.getData("displayName");
	    $scope.accessToken = localStorage.getData("accessToken"); //access token
	    $scope.userId = localStorage.getData("userId"); //user id
	    	
	    $scope.accessTypeList = [{"ID":1,"Description":"View"},{"ID":2,"Description":"Add"},{"ID":2,"Description":"Edit"},{"ID":2,"Description":"Delete"}];

	    
	    angular.isUndefinedOrNull = function(val) {
			return angular.isUndefined(val) || val === null;
		};
	    
		//common headers for all http posts
	    $http.defaults.headers.post["Content-Type"] = "application/json";
	    $http.defaults.headers.common['Authorization'] = "bearer " + $scope.accessToken;
	    

	    $scope.listOfPermissions = [];

	    //the model returns a promise and THEN items
	    	 dataService.getItems().then(function(items) {
	    	        $scope.listOfPermissions=items;
	    	 }, function (status) {
	            console.log(status);
	        });
	    
	    
	    $scope.init = function init() {
	    	$scope.modules = [];
	    	$scope.listOfModules = [];
	    	$scope.listOfRoles = [];
	    	$scope.isValidated = true;
	    	$scope.onSuccess = false;
	    	$scope.serviceMessage ='';
	    	$scope.canShowTable = false;
	    	 $timeout(function(){
		          $scope.canLoadNow = true;
		     }, 1000);
	    	
	    };
	
	    $scope.initListOfRoles = function initListOfRoles() {
	    	$scope.roleview = '/resources/views/bodytemplates/list_of_roles_body.html';
	    	$scope.init();
	    	$scope.getLisOfRoles();
	    };
	    
	    $scope.redirectToList =  function redirectToList() {
	    	$scope.roleview = '/resources/views/bodytemplates/list_of_roles_body.html';
	    };
	    
	    /**
        * Function to create new role
        */  
		$scope.createRole = function createRole(role) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Roles','moduleName','Add');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Create Role ";
				return false;
			}
			
			var isValid = $scope.tracForm.$valid;
			var inputObject  = {};
			inputObject.userId = parseInt($scope.userId);
			inputObject.roleName = role.roleName;
			inputObject.displayName = role.displayName;
			inputObject.roleDescription = role.roleDescription;
			inputObject.moduleList = $scope.listOfModules;
			
			console.log("role json "+angular.toJson(inputObject));
			if(isValid){
						$http.post(SERVICEADDRESS+'/user/role',angular.toJson(inputObject)).
						success(function(returnJson) {
							if(returnJson.isSuccess){
								 $scope.isValidated = true;
								 $scope.onSuccess = true;
								 $scope.serviceMessage = "Role created successfully";
								 $scope.canShowTable = true;
								 $scope.roleId = returnJson.roleId;
								 $scope.role={};
								 $scope.listOfRoles = returnJson.listOfRoles;
								 
								 for(var i =0 ; i< $scope.listOfModules.length; i++){
									 $scope.listOfModules[i].canView = false;
									 $scope.listOfModules[i].canAdd = false;
									 $scope.listOfModules[i].canEdit = false;
									 $scope.listOfModules[i].canDelete = false;
								 }
								 
								 $timeout(function(){
									 $scope.onSuccess = false;
							     }, 10000);
							}else{
								$scope.isValidated = false;
								$scope.serviceMessage = returnJson.serviceMessage;
								return false;
							}

						}).error(function(returnErrorJson, status, headers, config) {
							console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
							var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.createRole(role); 
							}
							return false;
						});//error
				
		      }//end of isValid
		};//create role function end 
		
		/*******
		 * To update the role
		 *
		*******/
	    
		$scope.updateRole = function updateRole(role) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Roles','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Update Role";
				return false;
			}
			
			var isValid = $scope.tracForm.$valid;
			var inputObject  = {};
			inputObject.userId = parseInt($scope.userId);
			inputObject.roleId = parseInt(role.roleId);
			inputObject.roleName = role.roleName;
			inputObject.displayName = role.displayName;
			inputObject.roleDescription = role.roleDescription;
			inputObject.moduleList = $scope.listOfRoles;
			
			console.log("role json "+angular.toJson(inputObject));
			if(isValid){
						$http.post(SERVICEADDRESS+'/user/updaterole',angular.toJson(inputObject)).
						success(function(returnJson) {
							if(returnJson.isSuccess){
								$scope.isValidated = true;
								$scope.onSuccess = true;
								$scope.initListOfRoles();
								
								$scope.serviceMessage = "Role updated successfully";
								$timeout(function(){
									 $scope.onSuccess = false;
							     }, 10000);
								
								
							}else{
								$scope.isValidated = false;
								$scope.serviceMessage = returnJson.serviceMessage;
								return false;
							}

						}).error(function(returnErrorJson, status, headers, config) {
							console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
							var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.updateRole(role); 
							}
							return false;
						});//error
				
		      }//end of isValid
		};//update role function end
		
		
		/*******
		 * To deactvate the role
		 *
		*******/
	    
		$scope.deactivateRole = function deactivateRole(role,index) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Roles','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Update Role";
				return false;
			}
			var isActive = (role.active == 1) ? 0 : 1;
			var isValid = $scope.tracForm.$valid;
			var inputObject  = {};
			inputObject.userId = parseInt($scope.userId);
			inputObject.roleId = parseInt(role.RoleID);
			inputObject.isActive = isActive;
			
			console.log("role json "+angular.toJson(inputObject));
			if(isValid){
						$http.post(SERVICEADDRESS+'/user/updateRoleStatus',angular.toJson(inputObject)).
						success(function(returnJson) {
							if(returnJson.isSuccess){
								$scope.isValidated = true;
								$scope.listOfRoles[index].active = isActive;
								$scope.onSuccess = true;
								if(isActive == 0 )
									$scope.serviceMessage = "Role DeActivated";
								else
									$scope.serviceMessage = "Role Activated";
								
								$timeout(function(){
									 $scope.onSuccess = false;
							     }, 10000);
								
								
							}else{
								$scope.isValidated = false;
								$scope.serviceMessage = returnJson.serviceMessage;
								return false;
							}

						}).error(function(returnErrorJson, status, headers, config) {
							console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
							var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.deactivateRole(role,index); 
							}
							return false;
						});//error
				
		      }//end of isValid
		};//deactivate role function end
		
		/*******
		 * get list of modules
		 *
		*******/
	    
	    $scope.getLisOfRoles = function getLisOfRoles() {
	    	
	    	$http.get(SERVICEADDRESS+'/user/getroles').
			success(function(returnJson) {
					$scope.listOfRoles = returnJson.userRolesList;
					console.log("getmodules" + JSON.stringify(returnJson));
				  }).error(function(returnErrorJson, status, headers, config) {
					var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.getLisOfRoles();
					}
					
				});//end of http post call
			
		}; //End getModules function
		
		
		 /*******
		 * get list of modules
		 *
		*******/
	    
	    $scope.getLisOfModules = function getLisOfModules() {
	    	
	    	$http.get(SERVICEADDRESS+'/user/getmodules').
			success(function(returnJson) {
					$scope.listOfModules = returnJson.modules;
					console.log("getmodules" + JSON.stringify(returnJson));
				  }).error(function(returnErrorJson, status, headers, config) {
					var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.getLisOfModules();
					}
					
				});//end of http post call
			
		}; //End getModules function
		
		$scope.getRoleDetails = function getRoleDetails(role) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Roles','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Update a Role ";
				return false;
			}
			$scope.role = {};
			$scope.role.roleId = role.RoleID;
			$scope.role.roleName = role.RoleName;
			$scope.role.displayName = role.DisplayName;
			$scope.role.roleDescription = role.DisplayDescription;
			
			var inputObject  = {};
			inputObject.roleId = parseInt(role.RoleID);
			
			console.log("role json "+angular.toJson(inputObject));
			$http.post(SERVICEADDRESS+'/user/getroleinfo',angular.toJson(inputObject)).
				success(function(returnJson) {
					if(returnJson.isSuccess){
						$scope.isValidated = true;
						$scope.roleview = '/resources/views/bodytemplates/role_update_body.html';
						 $scope.listOfRoles = returnJson.listOfRoles;
					
					}else{
						$scope.isValidated = false;
						$scope.serviceMessage = "No Roles found";
						return false;
					}

			}).error(function(returnErrorJson, status, headers, config) {
					console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
					var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.getRoleDetails(role); 
					}
					return false;
			});//error
				
		};//getRoleDetails
		
		$scope.selection=[];
		$scope.toggleSelection = function toggleSelection(serviceId) {
			var idx = $scope.selection.indexOf(serviceId);
			// is currently selected
			if (idx > -1) {
				$scope.selection.splice(idx, 1);
			} else {// is newly selected
				$scope.selection.push(serviceId);
			}
		};//end
		

		   //json serach to find out location where the value resides
			$scope.getIndexOf =  function (arr, coparableValue, prop) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop].trim().toString();
					var searcValue = coparableValue.trim().toString();
					if (indexValue.indexOf(searcValue) >= 0) {
						return k;
					}
				}
				return -1;
			};//end

			//json serach to find out location where the value resides
			$scope.getIndexById =  function (arr, coparableValue, prop) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop];
					var searcValue = coparableValue;
					if (indexValue == searcValue >= 0) {
						return k;
					}
				}
				return -1;
			};//end

			//json serach to access permissions
			$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop1].trim().toLowerCase();
					var searcValue = comparableValue.trim().toLowerCase();
					if (indexValue.indexOf(searcValue) >= 0) {
						//var canAccess = arr[k][prop2];
						return arr[k][prop2];
					}
				}
				return -1;
			};//end
	    	
		    $scope.init();
	    
	    /* * * * * * * * * * * *
	 	 * logout of user
	 	 * 
	 	 * * * * * * * * * * * */
	    $scope.logoutUser = function logoutUser() {
	    	$http.defaults.headers.common['Authorization'] = "bearer "+ $scope.accessToken + " " +  $scope.userId;
			
	    	//service call
			$http.post(SERVICEADDRESS+'/logout').
			  success(function(returnJson) {
				    	
				    	if(returnJson.isLoggedOut){
				    		localStorage.clearStorage();
				    		window.location.href = HOSTADDRESS+"/login.html";
				    	}
			  }).error(function(returnErrorJson, status, headers, config) {
				  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.logoutUser(); 
					}
			 });//error
			  
		  };//logoutUser function end
	      
}); //mainController function end
