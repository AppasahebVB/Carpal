TRACAPP.factory("localStorage", function($window, $rootScope) {
	
	angular.element($window).on('storage', function(event) {
	      $rootScope.$apply();
	  });

	  return {
	    setData: function(key,value) {
	      $window.localStorage && $window.localStorage.setItem(key, value);
	      return this;
	    },
	    getData: function(key) {
	      return $window.localStorage && $window.localStorage.getItem(key);
	    },
	    clearStorage: function() {
		      return $window.localStorage && $window.localStorage.clear();
		}
	  };
});

