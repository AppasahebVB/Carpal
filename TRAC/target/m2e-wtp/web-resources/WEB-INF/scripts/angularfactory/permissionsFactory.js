
TRACAPP.factory('dataService', function($q, $http,localStorage) {
  var items=[];
  var service={};
  service.getItems=function() {
     var itemsDefer=$q.defer();
     if(items.length >0) 
        itemsDefer.resolve(items);
     else
     {
    	 var inputJson = {};
	    	inputJson.userId = localStorage.getData("userId");
	         $http.post(SERVICEADDRESS + '/user/permissions', JSON.stringify(inputJson)).
	          success(function(data) {
	        	  		 items=data.permissions;
	        	  		itemsDefer.resolve(data.permissions);
	          });
     }
     return itemsDefer.promise;
  };
  return service;
});

