package trac.util;

/*import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;*/
import java.security.GeneralSecurityException;

//import javax.crypto.Cipher;
//import javax.crypto.NoSuchPaddingException;
//import javax.crypto.spec.SecretKeySpec;

import org.jasypt.util.text.BasicTextEncryptor;
//import org.springframework.core.io.ClassPathResource;

public class EncryptionUtil {

	
	public static String encrypt(BasicTextEncryptor bte,String input){ 
		String encrypted = bte.encrypt(input);
	    System.out.println("Encrypted = " + encrypted);
	    return encrypted;
		
	}
	
	
	public static String decrypt(String encrypted){
		BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();
		 String original = basicTextEncryptor.decrypt(encrypted);
	      System.out.println("Original  = " + original);
		return original;
	}
	
}


