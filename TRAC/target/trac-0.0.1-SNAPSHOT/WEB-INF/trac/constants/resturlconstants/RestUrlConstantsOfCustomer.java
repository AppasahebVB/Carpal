package trac.constants.resturlconstants;

public class RestUrlConstantsOfCustomer {

	public static final String SAVE_USER_CONTACT_DETAILS = "/contactDetailsOfCustomer/saveCustomerContactDetails";
	public static final String EDIT_USER_CONTACT_DETAILS = "/contactDetailsOfCustomer/editCustomerContactDetails";
	public static final String FETCH_USER_CONTACT_DETAILS = "/contactDetailsOfCustomer/fetchCustomerContactDetails";
	public static final String FETCH_CASE_DETAILS = "/case/details";
	public static final String FETCH_CASE_DETAILS_BY_Id = "/case/detailsbyid";
	
	public static final String REGISTER_CUSTOMER_SERVICELOCATION = "/serviceLocation/saveVehicleServiceLocation";
	public static final String EDIT_CUSTOMER_SERVICELOCATION = "/serviceLocation/editVehicleServiceLocation";
	public static final String FETCH_CUSTOMER_SERVICELOCATION = "/serviceLocation/fetchVehicleServiceLocation";
	public static final String FETCH_CUSTOMER_DESTINATIONLOCATION = "/serviceLocation/fetchVehicleDestinationLocation";
	
	public static final String SAVE_CUSTOMER_SATISFACTION = "/customerStaisfaction/saveCustomerReview"; 
	
	public static final String REGISTER_USER_DESTINATIONLOCATION = "/serviceLocation/saveUserDestinationLocation";
	public static final String EDIT_USER_DESTINATIONLOCATION = "/serviceLocation/editUserDestinationLocation";



}
