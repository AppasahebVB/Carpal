package trac.constants.resturlconstants;

public class RestUrlConstantsOfReports {
	
	public static final String OPERATIONS_REPORT_URL = "/reports/operations";
	public static final String ATTENDANCE_REPORT_URL = "/reports/attendance";
	public static final String TRUCKS_REPORT_URL = "/reports/trucks";
	public static final String EXPENSES_REPORT_URL = "/reports/expenses";

}
