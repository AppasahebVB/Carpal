package trac.constants.resturlconstants;

public class RestUrlConstantsOfShift {

	public static final String INSERT_SHIFT_TIMMINGS = "/shiftTimmings/insertShiftTimmings";
	public static final String UPDATE_SHIFT_TIMMINGS= "/shiftTimmings/editShiftTimmings";
	public static final String DELETE_SHIFT_TIMMINGS= "/shiftTimmings/deleteShiftTimmings";
	public static final String GET_SHIFT_TIMMINGS   = "/shiftTimmings/getShiftTimmings";
	public static final String GET_LIST_OF_SHIFTS   = "/shift/getShifts";
	public static final String SHIFT_SCHEDULE   = "/shift/schedule";
	public static final String GENARATE_SHIFT_SCHEDULE   = "/genarate/shiftschedule";
	public static final String UPDATE_SCHEDULE_OF_DRIVER   = "/shift/updateScheduleOfDriver";

}
