package trac.constants.resturlconstants;

public class RestUrlConstantOfDashboard {
	public static final String REPORT_INFO = "/dashboard/report";
	public static final String REPORT_METADATA = "/dashboard/reportmetadata";
	public static final String REPORT_DATA = "/dashboard/reportdata";
}
