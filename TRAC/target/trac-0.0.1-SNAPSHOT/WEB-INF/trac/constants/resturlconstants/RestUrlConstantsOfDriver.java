package trac.constants.resturlconstants;


public class RestUrlConstantsOfDriver {


	public static final String SAVE_DRIVER_INFO = "/driver/savePersonalDetailsOfDriver";
	public static final String EDIT_DRIVER_INFO = "/driver/editPersonalDetailsOfDriver";
	public static final String FETCH_DRIVER_INFO = "/driver/fetchPersonalDetailsOfDriver";

	public static final String SAVE_DRIVER_CONTACT_INFO = "/driver/saveContactDetailsOfDriver";
	public static final String EDIT_DRIVER_CONTACT_INFO = "/driver/editContactDetailsOfDriver";
	public static final String FETCH_DRIVER_CONTACT_INFO = "/driver/fetchContactDetailsOfDriver";

	public static final String SAVE_EDIT_DRIVER_EMERGENCY_CONTACT_INFO = "/driver/saveOrEditEmergencyContactDetailsOfDriver";
    //public static final String EDIT_DRIVER_EMERGENCY_CONTACT_INFO = "driver/editEmergencyContactDetailsOfDriver";
	public static final String FETCH_DRIVER_EMERGENCY_CONTACT_INFO = "/driver/fetchEmergencyContactDetailsOfDriver";

	public static final String SAVE_DRIVER_EXPERIENCE_INFO = "/driver/saveDriverWorkExperience";
	public static final String EDIT_DRIVER_EXPERIENCE_INFO = "/driver/editDriverWorkExperience";
	public static final String FETCH_DRIVER_EXPERIENCE_INFO = "/driver/fetchDriverWorkExperience";

	public static final String SAVE_DRIVER_PRESENT_EXPERIENCE_INFO = "/driver/savePresentWorkExperienceOfDriver";
	public static final String EDIT_DRIVER_PRESENT_EXPERIENCE_INFO = "/driver/editPresentWorkExperienceOfDriver";
	public static final String FETCH_DRIVER_PRESENT_EXPERIENCE_INFO = "/driver/fetchPresentWorkExperienceOfDriver";

	public static final String SERACH_DRIVERSHIFT_INFO = "/driver/serachDriverShiftInfo";
	public static final String FETCH_DRIVERSHIFT_SCHEDULES_INFO = "/driver/fetchShiftSchedule";
	public static final String SAVE_DRIVERSHIFT_INFO   = "/driver/saveDriverShiftInfo";
	public static final String EDIT_DRIVERSHIFT_INFO   = "/driver/editDriverShiftInfo";
	public static final String DELETE_DRIVERSHIFT_INFO = "/driver/deleteDriverShiftInfo";

	public static final String ASSIGN_CASE_TO_DRIVER = "/driver/assigningCaseToDriver";
	public static final String SMS_NOTIFICATION = "/driver/smsNotification";

	public static final String FETCH_TRUCK_AVAILABILITY_INFO = "/driver/fetchTruckAvailabilityInfo";
	
	
	public static final String SAVE_SHIFT_INOUT_INFO = "/driver/saveShiftINOUTTimmingsOfDriver";
	public static final String UPDATE_SHIFT_INOUT_INFO = "/driver/updateShiftINOUTTimmingsOfDriver";
	public static final String FETCH_ALLSHIFT_INOUT_TIMMINGS_OFDRIVER = "/driver/fetchShiftINOUTTimmingsOfDriver";
	public static final String FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_SHIFTID = "/driver/fetchShiftINOUTTimmingsOfDriverByShift";
	public static final String FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_DRIVER = "/driver/fetchShiftINOUTTimmingsOfDriverByDetails";
	
	public static final String FETCH_TRUCKS_SHIFTS_MANAGERS= "/driver/gettrucksandshiftsmanagers";
	
	public static final String FETCH_DRIVERS_INFO = "/driver/fetchDriversInfo";
	public static final String GET_DRIVER_DETAILS = "/driver/getFullDriverDetails";
	public static final String DELETE_DRIVER_INFO = "/driver/deleteDriverInfo";

}
