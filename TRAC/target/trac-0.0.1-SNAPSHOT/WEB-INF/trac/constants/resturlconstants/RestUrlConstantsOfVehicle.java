package trac.constants.resturlconstants;


/**
 * This Class is Used to Place the all RestUrlConstantsOf VehicleBean
 * @author gatla
 *
 */
public class RestUrlConstantsOfVehicle {

	public static final String REGISTER_VEHICLE_INFO = "/vehicle/register";
	public static final String UPDATE_VEHICLE_INFO= "/vehicle/update";
	public static final String GET_VEHICLE_INFO= "/vehicle/getvehicleinfo";
	public static final String GET_VEHICLE_BRANDS= "/vehicle/getvehiclebrands";
	public static final String UNREGISTER_VEHICLE_INFO= "/vehicle/unregistervehicleinfo";

	public static final String REGISTER_CUSTOMER_VEHICLE_INFO = "/vehicleInfo/registerVehicleInfoOfCustomer";
	public static final String EDIT_CUSTOMER_VEHICLE_INFO= "/vehicleInfo/editVehilcIeInfo";
	public static final String FETCH_CUSTOMER_VEHICLE_INFO= "/vehicleInfo/fetchVehicleInfoOfCustomer";
	public static final String EDIT_VEHICLE_DISPLAY_ORDER = "/vehicle/editDisplayorder";
	
}
