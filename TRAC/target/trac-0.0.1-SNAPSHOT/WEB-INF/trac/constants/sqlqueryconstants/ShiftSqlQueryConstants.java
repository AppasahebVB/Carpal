package trac.constants.sqlqueryconstants;

public class ShiftSqlQueryConstants {
	public static final String LIST_OF_SHIFTS = "Select shift.ID,shift.ShiftID,shift.shiftDay as  shiftDayId,shiftdays.Description as shiftDay,shift.StartTime,shift.EndTime,shift.VehiclesNeeded,shift.DriversNeeded from shifts shift "+ 
												"join lkp_sys_shift_days shiftdays on shiftdays.ID = shift.shiftDay "+ 
												"join lkp_sys_workshifts workshifts on workshifts.ID= shift.ShiftID order by shift.shiftDay,workshifts.id";
	public static final String SELECT_SHIFTS = "SELECT * FROM lkp_sys_workshifts";
}
