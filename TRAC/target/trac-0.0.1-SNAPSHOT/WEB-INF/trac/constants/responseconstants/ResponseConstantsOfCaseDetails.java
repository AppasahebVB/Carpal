package trac.constants.responseconstants;

public class ResponseConstantsOfCaseDetails {
	public static final String SERVICE_TYPE = "serviceType";
	public static final String CASESTATUS = "caseStatus";
	public static final String CASESTATUS_ID = "caseStatusId";
	public static final String SERVICELOCATIONLATITUDE = "serviceLocationLatitude";
	public static final String SERVICELOCATIONLONGITUDE = "serviceLocationLongitude";
	public static final String CASENOTES = "caseNotes";
	public static final String CASEID = "caseId";
	public static final String CASEREGISTEREDTIME = "caseRegisteredTime";
	public static final String SERVICEHISTORY = "serviceHistory";
	public static final String PRIORITY_ID  = "priorityId";
	public static final String SERVICE_REQUIRED_TIME  = "serviceRequiredTime";
	public static final String SERVICE_REQUIRED_TYPE  = "serviceRequiredType";
	public static final String VEHICLE_NAME  = "vehicleName";
	public static final String LOCATION_NAME  = "locationName";
	public static final String CASE_SCHEDULE_STATUS_ID = "schedulestatusID";
	public static final String STATUS_ID = "statusId";
	public static final String CASE_SCHEDULE_STATUS = "schedulestatus";
	
	
	public static final String CASE_FAULT_SERVICE_ID = "caseFaultServiceId";
	public static final String CASE_FAULT_ID = "caseFaultId";
	public static final String CASE_SERVICE_ID = "caseServiceId";
	public static final String CASE_FAULT_DESC = "caseFaultDesc";
	public static final String CASE_SERVICE_DESC = "caseServiceDesc";
	public static final String CASE_FAULT_SERVICES_LIST = "caseFaultServicesList";
	
	public static final String SUBCASES_INFO = "subcasesInfo";
	
	public static final String OWNER_NAME = "ownername";
	public static final String OWNER_PHONE_NO = "ownerphoneno";
	public static final String DRIVER_PHONE_NO = "driverphoneno";
	
	public static final String CASEREGISTERDTIME = "caseregisterdtime";
	public static final String CASEASSIGNEDTIME = "caseassignedtime";
	public static final String ACTUALARRIVAL = "actualarrival";
	public static final String EXPECTEDARRIVAL = "expectedarrival";
	
	public static final String CALLER_NAME = "callername";
	public static final String CREATOR_NAME = "creator";
	
	public static final String CASES_LIST = "caseslist";
	
	public static final String ISCASECLOSED = "isCaseClosed";
	
	
}
