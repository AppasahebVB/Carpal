package trac.constants.responseconstants;

public class ResponseConstantsOfUserLocation {
	
	
	public static final String USERLOCATIONNAME = "userLocationName";
	public static final String DETAILADDRESS = "detailAddress";
	public static final String USERLOCATIONLONGITUDE = "userLocationLongitude";
	public static final String USERLOCATIONLATITUDE = "userLocationLatitude";
	public static final String USERLOCATIONLIST = "userLocationList";
	public static final String LOCATIONID = "locationId";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String COUNTRYNAME = "countryName";
	public static final String COUNTRYCODE = "countryCode";
	
	public static final String ADDRESS_TITLE = "addressTitle";
	public static final String AREA = "area";
	public static final String STREET = "street";
	public static final String BUILDING = "building";
	public static final String LANDMARK = "landmark";
	public static final String POBOX = "pobox";
	public static final String COUNTRYISO = "countryiso";
	public static final String SERVICELOCATIONCOUNTRYCODE = "serviceLocationCountryCode";
	

}
