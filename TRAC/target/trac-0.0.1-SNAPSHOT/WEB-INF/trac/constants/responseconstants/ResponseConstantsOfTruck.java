package trac.constants.responseconstants;

public class ResponseConstantsOfTruck {
	
	public static final String USERID = "userId";
	public static final String TRUCKID = "truckId";
	public static final String VIN = "vin";
	public static final String TRUCKDRIVERID = "truckDriverId";
	public static final String TRUCKBRANDID = "truckBrandID";
	public static final String TRUCKMODELID = "truckModelID";
	public static final String TRUCKBRANDNAME = "truckBrandName";
	public static final String TRUCKMODELNAME = "truckModelName";
	public static final String TRUCKCODE = "truckCode";
	public static final String TRUCKBRANDLIST = "truckBrandList";
	
	public static final String DATEOFPURCHASE = "dateOfPurchase";
	public static final String REGISTRATIONDATE = "registrationDate";
	public static final String PLATECODE = "plateCode";
	public static final String PLATENUMBER = "plateNumber";
	public static final String DATEOFACQUIRING = "dateOfAcquiring";
	public static final String MODELYEAR = "modelYear";
	public static final String TRUCKCOLOR = "truckColor";
	public static final String TOTALCOST = "totalCost";
	public static final String SERVICESPROVIDED= "servicesProvided";
	public static final String AVAILABLEHOURSPERDAY = "availableHoursPerDay";
	public static final String TRUCKNICKNAME = "truckNickName";
	
	public static final String TRUCKPOLICYID = "truckPolicyID";
	public static final String POLICYNAME = "truckPolicyName";
	public static final String INSCOMPANYNAME = "insCompanyName";
	public static final String POLICYNUMBER = "truckPolicyNumber";
	public static final String POLICYISSUEDAT = "truckPolicyIssuedDate";
	public static final String POLICYEXPIREDAT = "truckPolicyExpiryDate";
	public static final String REREGISTRATIONDUEAT = "truckReRegistrationDueDate";
	
	
	public static final String TRUCKLIST = "truckList";
	
	public static final String TRUCK_DETAILS_SAVED   = "Truck Details Saved Successfully.";
	public static final String TRUCK_DETAILS_SAVE_FAILED  = "Saving Truck Details Failed.";
	
	public static final String TRUCK_DETAILS_UPDATED   = "Truck Details Updated Successfully.";
	public static final String TRUCK_DETAILS_UPDATE_FAILED  = "Updating Truck Details Failed.";
	
	public static final String TRUCK_UNREGISTERED   = "Truck UnRegistered Successfully.";
	public static final String TRUCK_UNREGISTRATION_FAILED  = "Truck UnRegistration Failed.";
	public static final String TRUCKS_NOT_FOUND = "Trucks are not Available.";
	
/*	public static final String POLICYEXPIREDEXPIRY = "policyExpiredValue";
    public static final String REREGISTRATIONDUEEXPIRY = "reRegistrationDueValue";*/
    public static final String COLOR_CODE = "colorCode";
    
    public static final String TRUCK_EXPENSE_DETAILS_SAVED   = "Truck Expense Details Saved Successfully.";
	public static final String TRUCK_EXPENSE_DETAILS_SAVE_FAILED  = "Saving Truck Expense Details Failed.";
	
	public static final String TRUCK_EXPENSE_DETAILS_UPDATED   = "Truck Expense Details Updated Successfully.";
	public static final String TRUCK_EXPENSE_DETAILS_UPDATE_FAILED  = "Updating Truck Expense Details Failed.";
	
	public static final String TRUCK_EXPENSES_NOT_FOUND = "Truck Expenses are not Available.";
}


