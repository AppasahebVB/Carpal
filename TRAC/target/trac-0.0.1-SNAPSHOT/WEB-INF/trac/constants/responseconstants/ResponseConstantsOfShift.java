package trac.constants.responseconstants;

public class ResponseConstantsOfShift {
	
	public static final String USERID = "userId";
	public static final String SHIFTID = "shiftId";
	
	public static final String DRIVERID = "driverId";
	public static final String DRIVERNAME = "driverName";
	public static final String TRUCKID = "truckId";
	public static final String TRUCKNAME = "truckName";
	
	public static final String DAYOFFNAME = "dayOffName";
	public static final String DAYOFFID = "dayOffId";
	
	public static final String SHIFTDAY = "shiftDay";
	public static final String SHIFTNAME = "shiftName";
	public static final String STARTTIME = "startTime";
	public static final String ENDTIME = "endTime";
	public static final String VEHICLESCOUNT = "vehiclesCount";
	public static final String DRIVERSCOUNT = "driversCount";
	
	public static final String SHIFTLIST = "shiftList";
	
	public static final String SHIFT_DETAILS_SAVED   = "Shift Details Saved Successfully.";
	public static final String SHIFT_DETAILS_SAVE_FAILED  = "Saving Shift Details Failed.";
	
	public static final String SHIFT_DETAILS_UPDATED   = "Shift Details Updated Successfully.";
	public static final String SHIFT_DETAILS_UPDATE_FAILED  = "Updating Shift Details Failed.";
	
	public static final String SHIFT_DELETED   = "Shift Deleted Successfully.";
	public static final String SHIFT_DELETION_FAILED  = "Shift Deletion Failed.";
	
	public static final String SHIFT_NOT_FOUND  = "No Shifts Found.";
	
}


