package trac.constants.responseconstants;

public class ResponseConstantsOfUser {

	
	   public static final String DISPLAYNAME ="displayName";
	   public static final String ISVALIDCREDENTIALS ="isValidCredentials";
	   public static final String PROFILEPICURL ="profilePicURL";
	   public static final String BLOB_KEY ="blobKey";
	   public static final String HASHEDPASSWORD ="hashedPassword";
	   public static final String CLOUD_PIC_FILENAME ="userFileNameOnCloud";
	   public static final String SECURITYQUESTION ="securityQuestion";
	   public static final String MANAGERS ="managers";
	   public static final String FIRSTNAME ="firstName";
	   public static final String LASTNAME ="lastName";
	   public static final String ROLEID = "roleId";
	   public static final String ROLENAME = "roleName";
	   public static final String ROLEDESC = "roleDescription";
	   public static final String MODULENAME = "moduleName";
	   public static final String ACCESSTYPE = "accessType";
	   public static final String ID = "id";
}
