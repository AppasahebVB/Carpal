package trac.constants.responseconstants;

public class CommonResponseConstants {
	  public static final String USERID ="userId";
	  public static final String REFRESHTOKEN ="refreshToken";
	  public static final String SERVICEMESSAGE ="serviceMessage";
	   public static final String ISSUCCESS ="isSuccess";
	   public static final String ISLOGINEXIST ="isLoginExist";
	   public static final String ACCESSTOKEN ="accessToken";
	   public static final String EXPIRESIN ="expiresIn";
	   public static final String SERVICECODE ="serviceCode";
	   public static final String VEHICLEID = "vehicleId";
	   public static final String PHONE = "phone";
	   public static final String EMAIL = "email";
	   public static final String ROLEID = "roleId";
}
