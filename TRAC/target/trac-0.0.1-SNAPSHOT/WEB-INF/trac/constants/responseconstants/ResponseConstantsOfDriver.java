package trac.constants.responseconstants;

public class ResponseConstantsOfDriver {

	
	public static final String DRIVER_INFO = "driverInfo";
	
	public static final String DRIVER_FIRSTNAME = "driverFirstName";
	public static final String DRIVER_LASTNAME = "driverLastName";
	public static final String DRIVER_NAME     = "driverName";
	
	public static final String EMPLOYEEID= "employeeId";
	public static final String LICENSE_NO= "licenseNo";
	public static final String LICENSENO_EXPIRED_AT= "licenseNoExpiredAt";
	public static final String INTERNATIONAL_LICENSE_NO= "internationalLicenseNo";
	public static final String INTERNATIONAL_LICENSE_EXPIRED_AT= "internationalLicenseExpiredAt";
	public static final String NATIONALITY = "nationality";
	public static final String GOVTIDNO= "governmentIdNumber";
	public static final String PASSPORT_NO= "passportNo";
	public static final String PASSPORT_ISSUED_AT= "passportIssuedAt";
	public static final String PASSPORT_EXPIRED_AT= "passportExpiredAt";
	public static final String VISA_NO= "visaNo";
	public static final String VISA_ISSUEDAT= "visaIssuedAt";
	public static final String VISA_EXPIREDAT= "visaExpiredAt";
	public static final String HEALTH_INSURANCE_CARD_NO= "healthInsuranceCardNo";
	public static final String HEALTH_INSURANCE_CARD_EXPIRY_AT= "healthInsuranceCardExpiryAt";

	//contact Details/EmergencyContactDetails
	public static final String CONTACT_PERSON_NAME= "contactPersonName";
	public static final String PHONE= "phone";
	public static final String EMAIL= "email";
	public static final String AREA= "area";
	public static final String STREET= "street";
	public static final String BUILDING= "building";
	public static final String LANDMARK= "landmark";
	public static final String CITY= "city";
	public static final String POSTBOX= "postBox";
	public static final String COUNTRY= "country";

	//workExperience
	public static final String TOTAL_WORKEXPERIENCE= "totalWorkExperience";
	public static final String RELATED_WORKEXPERIENCE= "relatedWorkExperience";
	public static final String COMPANY_NAME= "companyName";
	public static final String PREVIOUS_DESIGNATION= "previousdesignation";
	public static final String DATE_OF_JOINING= "dateOfJoining";
	public static final String DATE_OF_RELIEVING= "DATEOFRELIEVING";

	//PRESENTWORKDETAILS
	public static final String USERNAME= "userName";
	public static final String USERTYPE= "userType";
	public static final String DRIVERID= "driverID";
	public static final String DATEOFJOIN= "dateOfJoin";
	public static final String PRESENT_DESIGNATION= "presentdesignation";
	public static final String PREFERED_WORK_TIMINGS= "preferedWorkTimings";
	public static final String PREFERED_DAYOFF= "preferedDayOff";
	public static final String REPORTING_TO= "reportingTo";
	public static final String SHIRT_SIZE= "shirtSize";
	public static final String TROUSER_SIZE= "trouserSize";
	public static final String SHOE_SIZE= "shoeSize";
	public static final String NO_OF_SHIRTS_RECEIVED= "noOfShirtsReceived";
	public static final String NO_OF_TROUSERS_RECEIVED= "noOfTrousersReceived";
	public static final String NO_OF_SHOE_RECEIVED= "noOfShoeReceived";

	public static final String PUNCHID = "punchId";
	public static final String SHIFT_TIMINGS= "shiftTimings";
	public static final String PUNCH_IN_TIME= "punchInTime";
	public static final String PUNCH_OUT_TIME= "punchOutTime";

	public static final String NOTES= "notes";

	public static final String IS_AVAILABLE= "isAvailable";
	public static final String CREATED_AT= "createdAt";
	public static final String CREATED_BY= "createdBy";
	public static final String UPDATED_AT= "updatedAt";
	public static final String UPDATED_BY= "updatedBy";
	
	public static final String SHIFT_ID= "shiftId";
	public static final String SHIFT_NAME= "shiftName";
	public static final String SHIFT_SCHEDULED_ON= "scheduledOn";
	public static final String DRIVER_STATUS= "driverStatus";
	public static final String DRIVERS = "drivers";
	public static final String ID = "Id";
	

	public static final String LATITUDE= "latitude";
	public static final String LONGITUDE= "longitude";

	

	public static final String DRIVER_PERSONAL_DETAILS_SAVED   = "Driver Personal Details Saved Successfully.";
	public static final String DRIVER_PERSONAL_DETAILS_FAILED  = "Saving Driver Personal Details Failed.";
	public static final String DRIVER_PERSONAL_DETAILS_MISSING = "License No./Passport No. is missing.";
	public static final String DRIVER_PERSONAL_DETAILS_FETCHING_PROBLEM = "Problem fetching Driver Personal Details.";
	

	public static final String DRIVER_PERSONAL_DETAILS_UPDATED   = "Driver Personal Details Updated Successfully.";
	public static final String DRIVER_PERSONAL_DETAILS_UPDATE_FAILED  = "Updating Driver Personal Details Failed.";
	public static final String DRIVER_PERSONAL_IDDETAILS_MISSING = "DriverId/UserId missing";

	public static final String DRIVER_CONTACT_DETAILS_SAVED   = "Driver Contact Details Saved Successfully.";
	public static final String DRIVER_CONTACT_DETAILS_UPDATED = "Driver Contact Details Updated Successfully.";
	public static final String DRIVER_CONTACT_DETAILS_FAILED  = "Saving Driver Contact Details Failed.";
	public static final String DRIVER_CONTACT_DETAILS__UPDATE_FAILED  = "Updating Driver Contact Details Failed.";
	public static final String DRIVER_CONTACT_DETAILS_FETCHING_PROBLEM = "Problem fetching Driver Contact Details.";
	
	
	public static final String DRIVER_EMERGENCY_CONTACT_INFO_FAILED  = "Saving or Updating Driver Emergency Contact Details Failed.";
	public static final String DRIVER_EMERGENCY_CONTACT_INFO_FETCHING_FAILED  = "Problem fetching Driver Emergency Contact Details.";
	
	
	public static final String DRIVER_EXPERIENCE_DETAILS_SAVED   = "Driver Experience Details Saved Successfully.";
	public static final String DRIVER_EXPERIENCE_DETAILS_UPDATED = "Driver Experience Details Updated Successfully.";
	public static final String DRIVER_EXPERIENCE_DETAILS_FAILED  = "Saving Driver Experience Details Failed.";
	public static final String DRIVER_EXPERIENCE_DETAILS_UPDATE_FAILED  = "Updating Driver Experience Details Failed.";
	public static final String DRIVER_EXPERIENCE_DETAILS_FETCHING_PROBLEM = "Problem fetching Driver Experience Details.";
	
	public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_SAVED = "Driver Present Experience Details Saved Successfully.";
	public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_FAILED  = "Saving Driver Present Experience Details Failed.";
	
	public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_UPDATED = "Driver Present Experience Details Updated Successfully.";
	public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_UPDATE_FAILED  = "Updating Driver Present Experience Details Failed.";
	public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_FETCHING_PROBLEM = "Problem fetching Driver Present Experience Details.";
	
	public static final String DRIVER_NOT_FOUND  = "Driver Not Found.";
	
	public static final String DRIVER_SHIFT_DETAILS_SAVED          = "Driver Shift Details Saved Successfully.";
	public static final String DRIVER_SHIFT_DETAILS_SAVE_FAILED    = "Saving Driver Shift Details Failed.";
	
	public static final String DRIVER_SHIFT_DETAILS_UPDATED        = "Driver Shift Details Updated Successfully.";
	public static final String DRIVER_SHIFT_DETAILS_UPDATE_FAILED  = "Updating Driver Shift Details Failed.";
	
	public static final String DRIVER_SHIFT_DETAILS_DELETED        = "Driver Shift Details Deleted Successfully.";
	public static final String DRIVER_SHIFT_DETAILS_DELETE_FAILED  = "Deleting Driver Shift Details Failed.";
	
	public static final String DRIVER_SHIFT_NOT_FOUND              = "No Shifts scheduled for Drivers.";
	
	public static final String DRIVER_PUNCH_DETAILS_SAVED   = "Driver Logged In Details Saved Successfully.";
	public static final String DRIVER_PUNCH_DETAILS_FAILED  = "Saving Driver Logged In Details Failed.";
	
	public static final String DRIVER_PUNCH_DETAILS_UPDATED        = "Driver Logged In Details Updated Successfully.";
	public static final String DRIVER_PUNCH_DETAILS_UPDATE_FAILED  = "Updating Driver Logged In Details Failed.";
	
	public static final String DRIVERS_NOT_LOGGEDIN  = "No Driver is Logged In.";
	
	public static final String DRIVER_DETAILS_DELETED        = "Driver Details Deleted Successfully.";
	public static final String DRIVER_DETAILS_DELETE_FAILED  = "Deleting Driver Details Failed.";
	
	
	public static final String DRIVERS_NOT_FOUND  = "No Drivers Found.";
}


