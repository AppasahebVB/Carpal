package trac.beans.userbean;

import java.util.List;
import java.util.Map;

public class UserRoles {
	private List<Map<String, Object>> userRolesList;
	

	/**
	 * @return the userRolesList
	 */
	public List<Map<String, Object>> getUserRolesList() {
		return userRolesList;
	}

	/**
	 * @param userRolesList the userRolesList to set
	 */
	public void setUserRolesList(List<Map<String, Object>> userRolesList) {
		this.userRolesList = userRolesList;
	}
}
