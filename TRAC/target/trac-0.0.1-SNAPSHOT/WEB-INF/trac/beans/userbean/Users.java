package trac.beans.userbean;

import java.io.Serializable;


public class Users implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2107587111242700730L;
	
	private int userId;
	private String loginId;
	private String userName;
	private String firstName;
	private String lastName;
	private String displayName;
	private String password;
	private String passwordconfirm;
	private String newPassword;
	private String tempPassword;
	private String hashedPassword;
	private String phone;
	private String email;
	private String securityQuestion;
	private int securityQuestionId;
	private String securityAnswer;
	private boolean isValidCredentials = false;
	private boolean isImageChanged;
	private boolean isTermsAgreed;
	private boolean updateFlag;
	private int isdCode;
	private int statusCode;
	private int deviceTypeId;
	private String applicationVersionNo;
	private String profilePicURL;
	private int roleId;
	private String roleIds;
	private String createdBy;
	private String accessToken;
	private String refreshToken;
	private int expiresIn;
	private int retryCount;
	private String clientId;
	private String clientSecret;
	private boolean isVersionChanged;
	private boolean isSuccess;
	
	private String serviceMessage;
	private int serviceCode;
	private String lastUpdatedBy;
	private String[] getUserObjectArray;
	private boolean fromWeb;
	
	private String  roleDescription;
	private String accessType;
	private String moduleIds;
	private String htmlPage;
	private int isActive;

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the userId
	 */
	public synchronized int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public synchronized void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the userName
	 */
	public synchronized String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public synchronized void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the password
	 */
	public synchronized String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public synchronized void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the tempPassword
	 */
	public String getTempPassword() {
		return tempPassword;
	}
	/**
	 * @param tempPassword the tempPassword to set
	 */
	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}
	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}
	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/**
	 * @return the hashedPassword
	 */
	public synchronized String getHashedPassword() {
		return hashedPassword;
	}
	/**
	 * @param hashedPassword the hashedPassword to set
	 */
	public synchronized void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
	/**
	 * @return the phone
	 */
	public synchronized String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public synchronized void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the isdCode
	 */
	public int getIsdCode() {
		return isdCode;
	}
	/**
	 * @param isdCode the isdCode to set
	 */
	public void setIsdCode(int isdCode) {
		this.isdCode = isdCode;
	}
	/**
	 * @return the isTermsAgreed
	 */
	public boolean isTermsAgreed() {
		return isTermsAgreed;
	}
	/**
	 * @param isTermsAgreed the isTermsAgreed to set
	 */
	public void setTermsAgreed(boolean isTermsAgreed) {
		this.isTermsAgreed = isTermsAgreed;
	}
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the securityQuestion
	 */
	public synchronized String getSecurityQuestion() {
		return securityQuestion;
	}
	/**
	 * @param securityQuestion the securityQuestion to set
	 */
	public synchronized void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	/**
	 * @return the securityQuestionId
	 */
	public synchronized int getSecurityQuestionId() {
		return securityQuestionId;
	}
	/**
	 * @param securityQuestionId the securityQuestionId to set
	 */
	public synchronized void setSecurityQuestionId(int securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}
	/**
	 * @return the securityAnswer
	 */
	public synchronized String getSecurityAnswer() {
		return securityAnswer;
	}
	/**
	 * @param securityAnswer the securityAnswer to set
	 */
	public synchronized void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	/**
	 * @return the isValidCredentials
	 */
	public synchronized boolean isValidCredentials() {
		return isValidCredentials;
	}
	/**
	 * @param isValidCredentials the isValidCredentials to set
	 */
	public synchronized void setValidCredentials(boolean isValidCredentials) {
		this.isValidCredentials = isValidCredentials;
	}
	/**
	 * @return the statusCode
	 */
	public synchronized int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public synchronized void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the deviceTypeId
	 */
	public synchronized int getDeviceTypeId() {
		return deviceTypeId;
	}
	/**
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public synchronized void setDeviceTypeId(int deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * @return the profilePicURL
	 */
	public synchronized String getProfilePicURL() {
		return profilePicURL;
	}
	/**
	 * @param profilePicURL the profilePicURL to set
	 */
	public synchronized void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}
	/**
	 * @return the roleId
	 */
	public synchronized int getRoleId() {
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public synchronized void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the roleIds
	 */
	public String getRoleIds() {
		return roleIds;
	}
	/**
	 * @param roleIds the roleIds to set
	 */
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	/**
	 * @return the createdBy
	 */
	public synchronized String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public synchronized void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the accessToken
	 */
	public synchronized String getAccessToken() {
		return accessToken;
	}
	/**
	 * @param accessToken the accessToken to set
	 */
	public synchronized void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	/**
	 * @return the refreshToken
	 */
	public String getRefreshToken() {
		return refreshToken;
	}
	/**
	 * @param refreshToken the refreshToken to set
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	/**
	 * @return the expiresIn
	 */
	public synchronized int getExpiresIn() {
		return expiresIn;
	}
	/**
	 * @param expiresIn the expiresIn to set
	 */
	public synchronized void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
	/**
	 * @return the retryCount
	 */
	public synchronized int getRetryCount() {
		return retryCount;
	}
	/**
	 * @param retryCount the retryCount to set
	 */
	public synchronized void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	/**
	 * @return the isVersionChanged
	 */
	public boolean isVersionChanged() {
		return isVersionChanged;
	}
	/**
	 * @param isVersionChanged the isVersionChanged to set
	 */
	public void setVersionChanged(boolean isVersionChanged) {
		this.isVersionChanged = isVersionChanged;
	}
	
	/**
	 * @return the updateFlag
	 */
	public boolean isUpdateFlag() {
		return updateFlag;
	}
	/**
	 * @param updateFlag the updateFlag to set
	 */
	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public synchronized int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public synchronized void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	
	public String getApplicationVersionNo() {
		return applicationVersionNo;
	}
	public void setApplicationVersionNo(String applicationVersionNo) {
		this.applicationVersionNo = applicationVersionNo;
	}
	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
	/**
	 * @param clientSecret the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * @return the isImageChnaged
	 */
	public boolean isImageChanged() {
		return isImageChanged;
	}
	/**
	 * @param isImageChnaged the isImageChnaged to set
	 */
	public void setImageChanged(boolean isImageChanged) {
		this.isImageChanged = isImageChanged;
	}
	/**
	 * @return the getUserObjectArray
	 */
	public String[] getGetUserObjectArray() {
		return getUserObjectArray;
	}
	/**
	 * @param getUserObjectArray the getUserObjectArray to set
	 */
	public void setGetUserObjectArray(String[] getUserObjectArray) {
		this.getUserObjectArray = getUserObjectArray;
	}
	/**
	 * @return the passwordconfirm
	 */
	public String getPasswordconfirm() {
		return passwordconfirm;
	}
	/**
	 * @param passwordconfirm the passwordconfirm to set
	 */
	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}
	/**
	 * @return the fromWeb
	 */
	public boolean isFromWeb() {
		return fromWeb;
	}
	/**
	 * @param fromWeb the fromWeb to set
	 */
	public void setFromWeb(boolean fromWeb) {
		this.fromWeb = fromWeb;
	}
	/**
	 * @return the roleDescription
	 */
	public String getRoleDescription() {
		return roleDescription;
	}
	/**
	 * @param roleDescription the roleDescription to set
	 */
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	
	/**
	 * @return the accessType
	 */
	public String getAccessType() {
		return accessType;
	}
	/**
	 * @param accessType the accessType to set
	 */
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	/**
	 * @return the moduleIds
	 */
	public String getModuleIds() {
		return moduleIds;
	}
	/**
	 * @param moduleIds the moduleIds to set
	 */
	public void setModuleIds(String moduleIds) {
		this.moduleIds = moduleIds;
	}
	/**
	 * @return the htmlPage
	 */
	public String getHtmlPage() {
		return htmlPage;
	}
	/**
	 * @param htmlPage the htmlPage to set
	 */
	public void setHtmlPage(String htmlPage) {
		this.htmlPage = htmlPage;
	}
	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
	
	
	

}
