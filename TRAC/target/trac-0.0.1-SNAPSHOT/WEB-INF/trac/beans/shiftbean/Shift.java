package trac.beans.shiftbean;

public class Shift {
	int id;
	
	int userId;
	int currentShiftId;
	int currentDayId;
	int selectedShiftId;
	int isDayOff;
	int isDayOffSelected;
	int driverId;
	String createdBy;
	int shiftId;
	int dayOff;
	String shiftName;
	String shiftDay;
	int shiftDayId;
	String startTime;
	String endTime;
	int vehiclesCount;
	int driversCount;
	String startDate;
	String endDate;
	String formattedStartTime;
	String formattedEndTime;
	String shiftDate;
	int datesToFetch;
	
	/**
	 * @return the formattedStartTime
	 */
	public String getFormattedStartTime() {
		return formattedStartTime;
	}
	/**
	 * @param formattedStartTime the formattedStartTime to set
	 */
	public void setFormattedStartTime(String formattedStartTime) {
		this.formattedStartTime = formattedStartTime;
	}
	/**
	 * @return the formattedEndTime
	 */
	public String getFormattedEndTime() {
		return formattedEndTime;
	}
	/**
	 * @param formattedEndTime the formattedEndTime to set
	 */
	public void setFormattedEndTime(String formattedEndTime) {
		this.formattedEndTime = formattedEndTime;
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the shiftDay
	 */
	public String getShiftDay() {
		return shiftDay;
	}
	/**
	 * @param shiftDay the shiftDay to set
	 */
	public void setShiftDay(String shiftDay) {
		this.shiftDay = shiftDay;
	}
	/**
	 * @return the shiftDayId
	 */
	public int getShiftDayId() {
		return shiftDayId;
	}
	/**
	 * @param shiftDayId the shiftDayId to set
	 */
	public void setShiftDayId(int shiftDayId) {
		this.shiftDayId = shiftDayId;
	}
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the currentShiftId
	 */
	public int getCurrentShiftId() {
		return currentShiftId;
	}
	/**
	 * @param currentShiftId the currentShiftId to set
	 */
	public void setCurrentShiftId(int currentShiftId) {
		this.currentShiftId = currentShiftId;
	}
	/**
	 * @return the currentDayId
	 */
	public int getCurrentDayId() {
		return currentDayId;
	}
	/**
	 * @param currentDayId the currentDayId to set
	 */
	public void setCurrentDayId(int currentDayId) {
		this.currentDayId = currentDayId;
	}
	/**
	 * @return the selectedShiftId
	 */
	public int getSelectedShiftId() {
		return selectedShiftId;
	}
	/**
	 * @param selectedShiftId the selectedShiftId to set
	 */
	public void setSelectedShiftId(int selectedShiftId) {
		this.selectedShiftId = selectedShiftId;
	}
	
	
	/**
	 * @return the isDayOff
	 */
	public int getIsDayOff() {
		return isDayOff;
	}
	/**
	 * @param isDayOff the isDayOff to set
	 */
	public void setIsDayOff(int isDayOff) {
		this.isDayOff = isDayOff;
	}
	/**
	 * @return the isDayOffSelected
	 */
	public int getIsDayOffSelected() {
		return isDayOffSelected;
	}
	/**
	 * @param isDayOffSelected the isDayOffSelected to set
	 */
	public void setDayOffSelected(int isDayOffSelected) {
		this.isDayOffSelected = isDayOffSelected;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the shiftDate
	 */
	public String getShiftDate() {
		return shiftDate;
	}
	/**
	 * @param shiftDate the shiftDate to set
	 */
	public void setShiftDate(String shiftDate) {
		this.shiftDate = shiftDate;
	}
	/**
	 * @return the driverId
	 */
	public int getDriverId() {
		return driverId;
	}
	/**
	 * @param driverId the driverId to set
	 */
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getVehiclesCount() {
		return vehiclesCount;
	}
	public void setVehiclesCount(int vehiclesCount) {
		this.vehiclesCount = vehiclesCount;
	}
	/**
	 * @return the dayOff
	 */
	public int getDayOff() {
		return dayOff;
	}
	/**
	 * @param dayOff the dayOff to set
	 */
	public void setDayOff(int dayOff) {
		this.dayOff = dayOff;
	}
	public int getDriversCount() {
		return driversCount;
	}
	public void setDriversCount(int driversCount) {
		this.driversCount = driversCount;
	}
	/**
	 * @return the datesToFetch
	 */
	public int getDatesToFetch() {
		return datesToFetch;
	}
	/**
	 * @param datesToFetch the datesToFetch to set
	 */
	public void setDatesToFetch(int datesToFetch) {
		this.datesToFetch = datesToFetch;
	}

	
}
