/*
 * form_components.js
 *
 * Demo JavaScript used on form-pages.
 */

"use strict";

$(document).ready(function(){

	//===== Autocomplete =====//
	// Using typehead.js-library
	$('#autocomplete-example').typeahead({
		name: 'autocomplete-example',
		local: [
			'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Dakota','North Carolina','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'
		]
	});

});
