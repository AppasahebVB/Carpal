
TRACAPP.directive('mapCanvas', function() {
    return {
    	scope: {
            datasource: '=',
    	},
        link: function(scope, element,iAttrs) {
        	
          initialize();
          
    	  function initialize() {
    	        var infowindow = null;
    	        var myCenter = new google.maps.LatLng(24.9500, 55.3333);

    	        //setting default coordinate to dubai
    	        var mapOptions = {
    	            center: {
    	                lat: 24.9500,
    	                lng: 55.3333
    	            },
    	            zoom: 8
    	        };
    	        var map = new google.maps.Map(document.getElementById('map-canvas'),
    	            mapOptions);

    	        
    	        var marker = new google.maps.Marker({
    	            position: myCenter,
    	        });



    	        // Zoom to 9 when clicking on marker
    	        google.maps.event.addListener(marker, 'click', function() {
    	            map.setZoom(9);
    	            map.setCenter(marker.getPosition());
    	        });

    	        //Listener for adding marker on map
    	        google.maps.event.addListener(map, 'click', function(event) {
    	            marker.setMap(null);
    	            placeMarker(event.latLng);
    	        });

    	        function placeMarker(location) {


    	            marker = new google.maps.Marker({
    	                position: location

    	            });


    	            marker.setMap(map);
    	            //infowindow.open(map, marker);
    	           

    	            var geocoder = new google.maps.Geocoder();

    	            var lat = parseFloat(location.lat());
    	            var lng = parseFloat(location.lng());
    	            var latlng = new google.maps.LatLng(lat, lng);
    	            geocoder.geocode({
    	                'latLng': latlng
    	            }, function(results, status) {
    	                if (status == google.maps.GeocoderStatus.OK) {
    	                    if (results[0]) {
    	                        if (infowindow)
    	                            infowindow.close();

    	                        infowindow = new google.maps.InfoWindow({
    	                            content: 'Latitude: ' + location.lat() +
    	                                '<br>Longitude: ' + location.lng()
    	                        });
    	                        infowindow.setContent(results[0].formatted_address);
    	                        
    	                        var addressdetails = {};
    	                        
    	                        for (i = 0; i < results[0].address_components.length; i++) {
    	                            console.log(results[0].address_components[i].types[0] + " " + results[0].address_components[i].long_name);
    	                            
    	                            if(results[0].address_components[i].types[0] === 'street_address' ) 
    	                            	addressdetails.street = results[0].address_components[i].long_name;
    	                            if(results[0].address_components[i].types[0] === 'premise' || results[0].address_components[i].types[0] === 'subpremise' || results[0].address_components[i].types[0] === 'administrative_area_level_2'  ) 
    	                            	addressdetails.building = results[0].address_components[i].long_name;
    	                            if(results[0].address_components[i].types[0] === 'administrative_area_level_1') 
    	                            	addressdetails.landmark = results[0].address_components[i].long_name;
    	                            if(results[0].address_components[i].types[0] === 'locality') 
    	                            	addressdetails.customerLocationCity = results[0].address_components[i].long_name;
    	                            if(results[0].address_components[i].types[0] === 'postal_code') 
    	                            	addressdetails.poBox= results[0].address_components[i].long_name;
    	                            if(results[0].address_components[i].types[0] === 'country') 
    	                            	addressdetails.customerLocationCountryCode= results[0].address_components[i].long_name;
    	                            
    	                            if(iAttrs.datasource == 'contactdetails')
    	                                scope.$parent.setValues(addressdetails,'contactdetails');
    	                            else if(iAttrs.datasource == 'destinationlocation')
    	                            	scope.$parent.setValues(addressdetails,'destinationlocation');
    	                            else if(iAttrs.datasource == 'vehicleservicelocation')
    	                            	scope.$parent.setValues(addressdetails,'vehicleservicelocation');
    	                           
    	                        }

    	                        

    	                        infowindow.open(map, marker);
    	                    } else {
    	                        // alert('No results found');
    	                    }
    	                } else {
    	                    //  alert('Geocoder failed due to: ' + status);
    	                }
    	            });
    	        }

    	    }
    	  
    	  
    	  
        }
    };
});