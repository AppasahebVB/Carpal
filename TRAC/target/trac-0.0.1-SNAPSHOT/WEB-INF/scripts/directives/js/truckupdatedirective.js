
TRACAPP.directive('autoComplete', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	var selectedFormFiled = iAttrs.id;
                        iElement.trigger('input');
                      
                      //for truck brands
                     if(selectedFormFiled == "brandId" || selectedFormFiled.indexOf("brandId") >= 0){
                    	 var selectedBrand =  scope.truckUpdate.truckBrandID;
                    	 if(scope.selectedBrandId != null && scope.selectedBrandId != ""){
                       	  scope.$parent.models = [];
                       	  scope.$parent.modelId =  [];
                         }
	                      var indexLoc = scope.getIndexOf(scope.truckBradnModelList,selectedBrand.toString(),"truckBrandName");
	                      
	                      if(indexLoc >= 0)
	                    	  
	                    	scope.truckModelsList =  scope.truckBradnModelList[indexLoc].brandModels;
	                      scope.$parent.selectedBrandId = scope.truckBradnModelList[indexLoc].truckBrandID;
	                	    var truckModelsListLength = scope.truckModelsList.length;
	                	    scope.$apply(function(){
	                	    	scope.$parent.isBrandValid = false;
			      				for(var i=0;i<truckModelsListLength;i++){
			      					console.log("truckModelName - "+scope.truckModelsList[i].truckModelName);
			      					scope.$parent.models.push(scope.truckModelsList[i].truckModelName);	
			      					scope.$parent.modelIds.push(scope.truckModelsList[i].truckModelID);
			      				}
			      				
	                	    });
                     }
                     
                    
                     //for truck models
                     if(selectedFormFiled == "modelId" || selectedFormFiled.indexOf("modelId") >= 0){
                    	 var selectedModel =  scope.truckUpdate.truckModelID;
                    	 var indexLoc = scope.getIndexOf(scope.truckModelsList,selectedModel.toString(),"truckModelName");
                    	 scope.$parent.selectedModelId  = scope.truckModelsList[indexLoc].truckModelID;
                    	 scope.$parent.isModelValid = false;
                     }
                      
                    }, 0);
                }
            });
    };
});

TRACAPP.directive('autoCompleteTrucks', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	var selectedFormFiled = iAttrs.id;
                        iElement.trigger('input');
                        
                     
                      //for truck brands
                     if(selectedFormFiled == "truckBrandName" || selectedFormFiled.indexOf("truckBrandName") >= 0){
                    	 var selectedBrand =  scope.truckUpdate.truckBrandName;
                    	/* if(scope.selectedBrandId != null && scope.selectedBrandId != ""){
                       	  scope.$parent.models = [];
                       	  scope.$parent.modelId =  [];
                         }*/
	                      var indexLoc = scope.getIndexOf(scope.truckBradnModelList,selectedBrand.toString(),"truckBrandName");
	                      
	                      if(indexLoc >= 0)
	                    	scope.truckModelsList =  scope.truckBradnModelList[indexLoc].brandModels;
	                      scope.$parent.selectedBrandId = scope.truckBradnModelList[indexLoc].truckBrandID;
	                        var truckModelsListLength = scope.truckModelsList.length;
	                        scope.truckUpdate.truckModelName = '';
	                        scope.$apply(function(){
	                        	scope.$parent.isBrandValid = false;
		                	   	for(var i=0;i<truckModelsListLength;i++){
				      					console.log("truckModelName - "+scope.truckModelsList[i].truckModelName);
				      					scope.models.push(scope.truckModelsList[i].truckModelName);	
				      					scope.modelIds.push(scope.truckModelsList[i].truckModelID);
		                	   	}
	                        });	
	                	   
                     }
                     
                    
                     //for truck models
                     if(selectedFormFiled == "truckModelName" || selectedFormFiled.indexOf("truckModelName") >= 0){
                    	 var selectedModel =  scope.truckUpdate.truckModelName;
                    	 var indexLoc = scope.getIndexOf(scope.truckModelsList,selectedModel.toString(),"truckModelName");
                    	 scope.$parent.selectedModelId  = scope.truckModelsList[indexLoc].truckModelID;
                    	 scope.$parent.isModelValid = false;
                     }
                      
                    }, 0);
                }
            });
    };
});

var directiveFunction = function(){
    return {
        link: function(scope, element, attributes){

            console.log(attributes.anotherParam); //literally "modelObject.obj"

            //modelObject is a scope property of the parent/current scope
            scope.$watch(attributes.dojcomparisionDirective, function(value){
                console.log(value);
            });
        }
    };
}

TRACAPP.directive('restrict', function($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, ' '));
            });
        }
    }
});


