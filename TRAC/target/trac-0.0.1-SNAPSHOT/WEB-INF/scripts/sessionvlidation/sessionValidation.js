	var userId;
	var roleId;
	var accessToken;

function validateSession(localStorage){
	var isLoggedIn = localStorage.getItem("isLoggedIn");
	var currentUrl = document.URL;


	if(isLoggedIn == "true"){
		userId = localStorage.getItem("userId");
		roleId = localStorage.getItem("roleId");
		accessToken= localStorage.getItem("accessToken");
		accessToken= localStorage.getItem("accessToken");
		if(roleId >= 2 && roleId <= 6 && currentUrl.indexOf('register.html') > 0){
			window.location.href = HOSTADDRESS+"/404.html";
		}else if(currentUrl.indexOf('login.html') > 0){
			window.location.href = HOSTADDRESS+"/index.html";
		}
	}else{
		if(currentUrl.indexOf('login.html') < 0)
			window.location.href = HOSTADDRESS+"/login.html";
	}
	//return false;
		
}


function inValidateSession(httpErrorCode,errorDescription,localStorage,$http){
		if(httpErrorCode == 401){
			if(errorDescription.indexOf("Invalid access token") >= 0){
				localStorage.clearStorage();
				window.location.href = HOSTADDRESS+"/login.html";
				return false;
			}else if(errorDescription.indexOf("Access token expired") >= 0){
				return refreshTokenAfterExpire(localStorage,$http);
				//todo get fresh access token
			}
		}else if(httpErrorCode == 400){
			window.location.href = HOSTADDRESS+"/404.html";
		}else if(httpErrorCode == 500){
			window.location.href = HOSTADDRESS+"/500.html";
		}
				
}


function refreshTokenAfterExpire(localStorage,$http){
	var refreshToken = localStorage.getData("refreshToken");
	//$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.post(SERVICEADDRESS+'/oauth/token?grant_type=refresh_token&client_id=trac&client_secret=trac&refresh_token='+refreshToken).
	  success(function(returnJson) {
		  console.log("returnJson - "+ JSON.stringify(returnJson));
		  if(returnJson != null)
    		localStorage.setData("accessToken", returnJson.access_token);
		  return true;
	  }).error(function(returnErrorJson, status, headers, config) {
		 return false;
	 });//error
}