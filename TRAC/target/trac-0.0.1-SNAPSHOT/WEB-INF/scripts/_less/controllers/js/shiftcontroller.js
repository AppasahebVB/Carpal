TRACAPP.config(function (datepickerConfig) {
    datepickerConfig.showWeeks = false;
});

TRACAPP.filter('range', function() {
	  return function(val, range) {
	    range = parseInt(range);
	    for (var i=1; i<range; i++)
	      val.push(i);
	    return val;
	  };
}); 

TRACAPP.filter("isRepeateative", function(){
    return function(input, driverOffDay,isDayOff){
    	console.log("driverOffDay - "+ driverOffDay);
    	console.log("driverOffDay - "+ driverOffDay);
    	var l = input.length;
    	var prop = "shiftDay";
    	var comparableValue = driverOffDay;
    	var returnArray = [];
    	for (var k = 0,j=0; k < l; k = k + 1) {
			var indexValue = input[k][prop];
			
			if(k != 0)
			prevIndexValue = input[k - 1][prop];
			
			var searcValue = parseInt(comparableValue);
			if (indexValue == searcValue) {
				if(j == 0)
					returnArray.push(input[k]);
				
				j++;
			}else{
					returnArray.push(input[k]);
			}
			
		}
		
    	 return returnArray;
    
    };
});

TRACAPP.filter("shiftFilter", function(){
    return function(input, shiftDay){
    	var l = input.length;
    	var prop = "shiftDay";
    	var returnArray = [];
    	for (var k = 0,j=0; k < l; k = k + 1) {
    		var indexValue = input[k][prop];
			var searcValue = parseInt(shiftDay);
			if (indexValue == searcValue) {
					returnArray.push(input[k]);
			}
		}
    	return returnArray;
    };
});


TRACAPP.directive('myDatePicker', ['$timeout', function($timeout){
    return {
        restrict: 'A',      
        link: function(scope, elem, attrs){
            // timeout intervals are called once directive rendering is complete
            $timeout(function(){                    
                $(elem).datetimepicker({
                	pickDate: false,
                    format: 'hh:mm'
                });
            });
        }
    };
}]);





TRACAPP.controller("shiftcontroller", function($scope, $http, localStorage,$location,$timeout,$route,$filter,dataService) {
	
		//to vlidate against undefined and null  values
	     angular.isUndefinedOrNull = function(val) {
	    	 return angular.isUndefined(val) || val === null;
		 };


       //common headers for all http posts
       $http.defaults.headers.post["Content-Type"] = "application/json";
       $http.defaults.headers.common['Authorization'] = "bearer " + localStorage.getData("accessToken");
       
       $scope.accessToken = localStorage.getData("accessToken");//accessToken
       $scope.userId = localStorage.getData("userId");//userId
       $scope.displayName = localStorage.getData("displayName");
       $scope.modules = [];
       $scope.hstep = 1;
       $scope.mstep = 1;
       $scope.isAccessible = true;
       $scope.listOfPermissions = [];
        $scope.shiftScheduleTypes = [{"ID":3,"shiftScheduleType":"Current Week"},{"ID":4,"shiftScheduleType":"Current Month"},{"ID": 1,"shiftScheduleType":"Last Week"},{"ID":6,"shiftScheduleType":"Last Month"}];


     //the model returns a promise and THEN items
     	 dataService.getItems().then(function(items) {
     	        $scope.listOfPermissions=items;
     	 }, function (status) {
             console.log(status);
         });
       
         $timeout(function(){
	          $scope.canLoadNow = true;
	     }, 1000);
         
         /**
          *to slide page view to top
          */
         $scope.slideToTop = function slideToTop(){
       	  $("html, body").animate({ scrollTop: 0 }, 1000);
         };
       
	   $scope.init = function init(shift) {
	      
	       $scope.datePattern=/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i;
		   
		   /**
		     ng-options Values for shift timings
		    */	   
		    $scope.days = [{"ID":1,"dayName":"Sunday"},{"ID":2,"dayName":"Monday"},{"ID":3,"dayName":"Tuesday"},{"ID":4,"dayName":"Wednesday"},{"ID":5,"dayName":"Thursday"},{"ID":6,"dayName":"Friday"},{"ID":7,"dayName":"Saturday"}];   
		    
		    $scope.shiftList = [];
		    $scope.shiftListToBeDeleted = [];
		   //display name or email of user
			$scope.displayName = localStorage.getData("displayName");
			$scope.isValidated = true; //scope variable to check against service message
			$scope.onSuccess = false;
			
			if(!$scope.isAccessible){
				$scope.view = '/resources/views/bodytemplates/accessDenied.html'; 
			}else{
				//default view to be displayed
				$scope.view = '/resources/views/bodytemplates/shift_timings_body.html';
			}
			
			$scope.shiftValidators = {};
			$scope.shiftValidators.canEdit = false;
			//$scope.getShiftList();
			$scope.addNewShift();
			
	   };
       
	   //TO add new shift details on clicking add	   
	   $scope.addNewShift = function addNewShift() {

			 if($scope.onSuccess)
			  $scope.onSuccess = false;
			   
			 
		   var shiftObject = {};
		   shiftObject.Id = -1;
		   shiftObject.shiftId = -1;
		   shiftObject.shiftName = '';
		   shiftObject.startTime = new Date();
		   shiftObject.endTime = new Date();
		   shiftObject.vehiclesNeeded = 1;
		   shiftObject.driversNeeded = 1;
		   shiftObject.shiftRWD = 1;
		   $scope.shiftList.push(shiftObject);
		   
	   };
	   
	   //TO delete new shift details on clicking add
	   $scope.deleteShiftRow = function deleteShiftRow(index) {
		   console.log("index - "+index);
		   
		   var shiftRWDStatus = $scope.shiftList[index].shiftRWD;
		   
		   if(shiftRWDStatus == 2){
			   $scope.shiftList[index].shiftRWD = 3;
			   $scope.shiftListToBeDeleted.push($scope.shiftList[index]);
		   }
		  			   
		   if($scope.shiftList.length >= 1){
			   $scope.shiftList.splice(index,1);
			   
			   if($scope.shiftList.length == 1)//add new row if everything is deleted
				   $scope.addNewShift();
		   }
	   };
	   
	   /**
        * Save Shift Timing Details
        */
      
       $scope.saveShiftTimingDetails = function saveShiftTimingDetails(shift) {
    	   
    	   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Shifts','moduleName','Add');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Add Shift Timings";
				return false;
			}
       	
           var isValid = $scope.tracForm.$valid;
           console.log("shift json "+ JSON.stringify(shift));
           var shiftArray =[];
           var shiftObject = {};
           shiftObject.shiftDayId= parseInt($('#shiftDayId').val());
           shiftObject.userID=parseInt($scope.userId);
           
           if(isValid){
                   
           for(var i =0; i<shift.length;i++){ //for formatting start and end times
        	   var startTime =  shift[i].startTime;
        	   var endTime =  shift[i].endTime;
        	   var startTimeMeridian =  $filter('date')(startTime, 'a');
        	   var endTimeMeridian =  $filter('date')(endTime, 'a');
        	   if(startTimeMeridian == 'PM' && endTimeMeridian == 'AM'){
        		   var tempEndTime = new Date(endTime);
        		   tempEndTime.setDate(tempEndTime.getDate()+1);
        		   shift[i].formattedStartTime =  $filter('date')(startTime, 'yyyy/MM/dd HH:mm:ss a');
        		   shift[i].formattedEndTime =  $filter('date')(tempEndTime, 'yyyy/MM/dd HH:mm:ss a');
        	   }else{
	        	   shift[i].formattedStartTime =  $filter('date')(startTime, 'yyyy/MM/dd HH:mm:ss a');
	        	   shift[i].formattedEndTime =  $filter('date')(endTime, 'yyyy/MM/dd HH:mm:ss a');
        	   }
           }
           
           
           if(!angular.isUndefinedOrNull($scope.shiftListToBeDeleted) && $scope.shiftListToBeDeleted.length > 0){ 
        	   shiftArray = shift.concat($scope.shiftListToBeDeleted);
           }else{
        	   shiftArray = shift;
           }
           
           shiftObject.shifttimingsList=shiftArray;
           
           console.log("shiftObject json " + angular.toJson(shiftObject));
           
           if(angular.isUndefinedOrNull($scope.shift.shiftDayId) || ($scope.shift.shiftDayId.length == 0)){
        	   return false;
           }
           
        
           	
           $http.post(SERVICEADDRESS + '/shiftTimmings/insertShiftTimmings', angular.toJson(shiftObject)).
           success(function(returnJson) {
          	
               if (returnJson.isSuccess) {
            	   $scope.shift.shiftDayId = '';
            	   $scope.shiftList.splice(0);
            	   $scope.shiftListToBeDeleted.splice(0);
            	   $scope.addNewShift();
	               $scope.isValidated = true;   
	               $scope.onSuccess = true;
	               $scope.serviceMessage = returnJson.serviceMessage;
	               $timeout(function(){
				          $scope.onSuccess = false;
				     }, 10000);
               }else{
            	   $scope.isValidated = false;
                   $scope.serviceMessage = returnJson.serviceMessage;
               }

           }).error(function(returnErrorJson, status, headers, config) {
	           	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	   			if(isFreshTokenGenarated){
	   				$scope.shiftTimingDetails(shift); 
	   		}
           }); //error
           
          }else{
        	  if(angular.isUndefinedOrNull(shiftObject.shiftDayId) || shiftObject.shiftDayId.length == 0){
	        	  $scope.isValidated = false;
	              $scope.serviceMessage ="Please select day";
        	  }
          } //if end
   
       }; //end save Shift Timing Details
	   
       
	   $scope.isRepeateative = function isRepeateative(shift,driverOffDay) {
		   if(shiftDay != driverOffDay)
		    	  return input;
		      else if (shiftDay == driverOffDay && (input[index-1].shiftDay != driverOffDay || index == 0) )
		    	  return input;
	   };
	   
	   /**
        * To update schedule of the driver
        */
	   $scope.updateScheduleOfDriver = function updateScheduleOfDriver(driverScheduleList,isAvailableForShift,currentShiftId,currentDayId,selectedShiftId,dayOffId,shiftDate,parentIndex,subIndex,isDayOffSelected,isDayOff) {
		   
		   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Shift Schedule','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Update Shift Schedule Of Driver";
				return false;
			}
			
		   var shiftObject = {};
           shiftObject.userId=parseInt($scope.userId);
           shiftObject.driverId=driverScheduleList[parentIndex].driverId;
           shiftObject.currentShiftId=currentShiftId;
           shiftObject.selectedShiftId= parseInt(selectedShiftId);
           shiftObject.currentDayId=currentDayId;
           shiftObject.isDayOffSelected= isDayOffSelected;
           shiftObject.isDayOff= isDayOff;
           shiftObject.dayOff=dayOffId;
           shiftObject.shiftDate=shiftDate;
           shiftObject.datesToFetch = $scope.whatDatesToFetch;
           shiftObject.isAvailableForShift = isAvailableForShift;
	   		
           if($scope.whatDatesToFetch == 1){
	   			shiftObject.startDate = $scope.startDate;
	   			shiftObject.endDate = $scope.endDate;
	   		}else if($scope.whatDatesToFetch == 2){
				shiftObject.startDate = $scope.startDate;
	   			shiftObject.endDate = $scope.endDate;
	   		}
           console.log("shiftObject json " + angular.toJson(shiftObject));
          $http.post(SERVICEADDRESS + '/shift/updateScheduleOfDriver', angular.toJson(shiftObject)).
           success(function(returnJson) {
          	
               if (returnJson.isSuccess) {
            		$scope.driverScheduleList = returnJson.driverScheduleList;
	   				$scope.datesList = returnJson.dayWiseShiftCountList;
	   				$scope.daysList = returnJson.daysList;
	   				$scope.shiftlist = returnJson.shiftlist;
	   				$scope.startDate = returnJson.startDate;
	   				$scope.endDate = returnJson.endDate;
	   				$scope.totalNoOfShifts = returnJson.totalNoOfShifts;
	   				$scope.differentShiftList = returnJson.differentShiftList;
	   				$scope.isScheduleGenarated = returnJson.isScheduleGenarated;
	   				if($scope.driverScheduleList.length == 0)
	   					$scope.ifNoRecordFound = true;
	   				
		   			if(!returnJson.isSuccess){
		   			 	$scope.ifNoRecordFound = true;
		   				$scope.serviceMessage = returnJson.serviceMessage;
		   			}
	   			console.log(JSON.stringify(returnJson));

               }else{
            	   $scope.isValidated = false;
                   $scope.serviceMessage = returnJson.serviceMessage;
               }

           }).error(function(returnErrorJson, status, headers, config) {
	           	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
	   			if(isFreshTokenGenarated){
	   				$scope.updateScheduleOfDriver(driverScheduleList,isAvailableForShift,currentShiftId,currentDayId,selectedShiftId,dayOffId,shiftDate,parentIndex,subIndex,isDayOffSelected,isDayOff); 
	   		}
           }); //error
       
	   };
	   
	 //json serach to find out location where the value resides
		$scope.updateDayOffWithShift =  function (arr, coparableValue1,coparableValue2, prop1,prop2,parentIndex) {
			var l = arr.length,k = 0,loop = false,indexToSplit = -1;
			var shiftDate = '',shiftDay = -1,dayName = '',shiftId = -1;
			
			var shiftObjectArray = [];
			var splitArray = [];
			
			for (k = 0; k < l; k = k + 1) {
				var indexValue2 = arr[k][prop2];
				if (indexValue2 == coparableValue2) {
					if(!loop){
						shiftDate= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDate;
						shiftDay= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDay;
						dayName= $scope.driverScheduleList[parentIndex].shiftschedule[k].dayName;
						shiftId= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftId;
						indexToSplit = k + 1;
						
						for (var j = 0; j < $scope.shiftlist.length; j = j + 1) {
							var tempShiftDay =  $scope.shiftlist[j].shiftDay;
							if(tempShiftDay == shiftDay){
								var shiftObject = {};
								var tempShiftId = $scope.shiftlist[j].shiftId;
								shiftObject.shiftDate= shiftDate;
								shiftObject.shiftDay= shiftDay;
								if(tempShiftId == coparableValue1)
									shiftObject.isAvailableForShift= true;
								else
									shiftObject.isAvailableForShift= false;
								shiftObject.shiftName= $scope.shiftlist[j].shiftName;
								shiftObject.isDayOff= false;
								shiftObject.isWorkingOnthatDate= true;
								shiftObject.dayName= dayName;
								shiftObject.shiftId= tempShiftId;
								shiftObjectArray.push(shiftObject);
								
							}
						}
						loop = true;
					}
						
				}else if (indexValue2 == coparableValue2){
					$scope.driverScheduleList[parentIndex].shiftschedule[k].isAvailableForShift = false;
					$scope.driverScheduleList[parentIndex].shiftschedule[k].isDayOff = false;
				}
			}
			
			if(indexToSplit != -1){
				var shiftscheduleArray = $scope.driverScheduleList[parentIndex].shiftschedule;
				if(indexToSplit == 1){
					splitArray = $scope.driverScheduleList[parentIndex].shiftschedule.splice(indexToSplit, shiftscheduleArray.length);
					$scope.driverScheduleList[parentIndex].shiftschedule = shiftObjectArray.concat(splitArray);
				}else{
					splitArray = $scope.driverScheduleList[parentIndex].shiftschedule.splice(0, indexToSplit);
					$scope.driverScheduleList[parentIndex].shiftschedule = splitArray.concat(shiftObjectArray).concat($scope.driverScheduleList[parentIndex].shiftschedule.splice(indexToSplit, shiftscheduleArray.length));
				}
			}
			console.log("shiftschedule - "+JSON.stringify($scope.driverScheduleList[parentIndex].shiftschedule));
		};//end
		
	 //json serach to find out location where the value resides
		$scope.updateIndexOfMultiplePropertySerach =  function (arr, coparableValue,prop,parentIndex) {
			var l = arr.length,k = 0,loop = false,startPosition = -1,endPosition = -1;
			var shiftObjectArray = [];
			for (k = 0; k < l; k = k + 1) {
				var indexValue = arr[k][prop];
				if (indexValue == coparableValue) {
					shiftDate= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDate;
					shiftDay= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDay;
					dayName= $scope.driverScheduleList[parentIndex].shiftschedule[k].dayName;
					shiftId= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftId;
					if(!loop){
						startPosition = k;
						var shiftObject = {};
						shiftObject.shiftDate= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDate;
						shiftObject.shiftDay= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftDay;
						shiftObject.shiftName= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftName;
						shiftObject.isWorkingOnthatDate= true;
						shiftObject.isDayOff= true;
						shiftObject.isAvailableForShift= false;
						shiftObject.dayName= $scope.driverScheduleList[parentIndex].shiftschedule[k].dayName;
						shiftObject.shiftId= $scope.driverScheduleList[parentIndex].shiftschedule[k].shiftId;
						shiftObjectArray.push(shiftObject);
						$scope.driverScheduleList[parentIndex].shiftschedule.splice(k,2,shiftObject);
					}
					if(loop){
						$scope.driverScheduleList[parentIndex].shiftschedule.splice(k,1);
						endPosition = k;
					}
					loop = true;
				}
			}
		};//end
		
		 //json serach to find out location where the value resides
		$scope.updateIsAvailableForShiftProperty =  function (arr, coparableValue1,coparableValue2,prop1,prop2,parentIndex) {
			var l = arr.length,
			k = 0,j=0;
			
			for (k = 0; k < l; k = k + 1) {
				var indexValue1 = arr[k][prop1];
				var indexValue2 = arr[k][prop2];
				if (indexValue1 ==  coparableValue1) {
					var isAvailableForShift = $scope.driverScheduleList[parentIndex].shiftschedule[k].isAvailableForShift;
					if(j == 2){
						return;
					}
					if(indexValue2 == coparableValue2){
						$scope.driverScheduleList[parentIndex].shiftschedule[k].isAvailableForShift = true;
						j++;
					}
					if(isAvailableForShift){
						$scope.driverScheduleList[parentIndex].shiftschedule[k].isAvailableForShift = false;
						j++;
					}
				}
			}
			
		};//end
		
	 //json serach to find out location where the value resides
		$scope.getIndexOfMultiplePropertySerach =  function (arr, coparableValue1,coparableValue2, prop1,prop2 ) {
			var l = arr.length,
			k = 0;
			for (k = 0; k < l; k = k + 1) {
				
				var indexValue1 = arr[k][prop1];
				var indexValue2 = arr[k][prop2];
				if (indexValue1 == coparableValue1 && indexValue2 == coparableValue2) {
					return k;
				}
			}
			return -1;
		};//end
	  
      
	   
	   
       /**
        * Edit Shift Timing Details
        */  
       
	   
       $scope.editShiftTimingDetails = function editShiftTimingDetails(shift) {
    	   
    	   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Shifts','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Edit Shift Timings";
				return false;
			}
			
    	   var isValid = $scope.tracForm.$valid;
    	   $scope.shiftId=shift.shiftId;
    	  
              var editShiftTimings = {};
              
              editShiftTimings.userId= $scope.userId; 
              editShiftTimings.shiftId= shift.shiftId;
              editShiftTimings.shiftName=shift.shiftName;
              editShiftTimings.startTime=shift.startTime;
              editShiftTimings.endTime=shift.endTime;
              
              var startTimeMeridian =  $filter('date')(shift.startTime, 'a');
       	      var endTimeMeridian =  $filter('date')(shift.endTime, 'a');
	       	   if(startTimeMeridian == 'PM' && endTimeMeridian == 'AM'){
	       		   var tempEndTime = new Date(shift.endTime);
	       		   tempEndTime.setDate(tempEndTime.getDate()+1);
		       		editShiftTimings.formattedStartTime =  $filter('date')(shift.startTime, 'yyyy/MM/dd HH:mm:ss a');
		       		editShiftTimings.formattedEndTime =  $filter('date')(tempEndTime, 'yyyy/MM/dd HH:mm:ss a');
	       	   }else{
		       		editShiftTimings.formattedStartTime =  $filter('date')(shift.startTime, 'yyyy/MM/dd HH:mm:ss a');
		       		editShiftTimings.formattedEndTime =  $filter('date')(shift.endTime, 'yyyy/MM/dd HH:mm:ss a');
	       	   }
             
              editShiftTimings.vehiclesCount=shift.vehiclesCount;
              editShiftTimings.driversCount=shift.driversCount;
              
              console.log("editShiftTimings json " + JSON.stringify(editShiftTimings));
              
              if(isValid){
              	
              $http.post(SERVICEADDRESS + '/shiftTimmings/editShiftTimmings', JSON.stringify(editShiftTimings)).
              success(function(returnJson) {
             	
                  if (returnJson.isSuccess) {
                  	$scope.isValidated = true;  
                    $scope.onSuccess = true;
                    $scope.serviceMessage = returnJson.serviceMessage;
                  	 $timeout(function(){
				          $scope.onSuccess = false;
				     }, 10000);
                    
                     
                  } else {
                  	$scope.isValidated = false;
                      $scope.serviceMessage = returnJson.serviceMessage;
                  }

              }).error(function(returnErrorJson, status, headers, config) {
              	isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
      			if(isFreshTokenGenarated){
      				 $scope.editShiftTimingDetails(shift);
      			}
                

              }); //error
              
             }else{
            		$scope.isValidated = false;
                    $scope.serviceMessage = "Please enter correct details";
             }//if end
          
          }; //end edit Shift Timing Details
       
	   
       /**
        * Delete Shift Timing Details
        */  
	   $scope.deleteShift = function deleteShift(shift,currentRow) {
    	   
    	   var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Shifts','moduleName','Delete');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to delete Shift";
				return false;
			}
    		   var shiftJson ={};
    		   shiftJson.shiftId = shift.shiftId;
   		   
    		   $http.post(SERVICEADDRESS + '/shiftTimmings/deleteShiftTimmings', JSON.stringify(shiftJson)).
    	        success(function(returnJson) {

    	            if (returnJson.isSuccess) {
    	            	   $scope.shiftList.splice(currentRow,1);
    	            } 

    	        }).error(function(returnErrorJson, status, headers, config) {
    	        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
    				if(isFreshTokenGenarated){
    					$scope.deleteShift(shift,currentRow); 
    				}

    	        }); //error
    	     
    	   };
       
			   	
        /**
       	 * To fetch schedule of driver
       	 * 
       	 * 
       	 * @return selection array
       	 */
 		$scope.getScheduleOfDriver =  function getScheduleOfDriver(startDate,endDate,whatDatesToFetch) {
	   		$scope.isValidated = true; //scope variable to check against service message
	   		$scope.ifNoRecordFound = false;
	   		
	   		var shiftObject ={};
	   		shiftObject.userId =$scope.userId;
	   		shiftObject.datesToFetch = whatDatesToFetch;
	   		if(whatDatesToFetch == 1){
	   			shiftObject.startDate = startDate;
	   			shiftObject.endDate = endDate;
	   		}else if(whatDatesToFetch == 2){
				shiftObject.startDate = startDate;
	   			shiftObject.endDate = endDate;
	   		}
	   		
	   	   $scope.startDate = startDate;
	       $scope.endDate= endDate;
	       $scope.whatDatesToFetch = whatDatesToFetch;
	   		console.log("shiftObject - "+  JSON.stringify(shiftObject) );
	   		
	   	  $http.post(SERVICEADDRESS + '/shift/schedule', JSON.stringify(shiftObject)).
              success(function(returnJson) {
            	  	$scope.slideToTop();
            	  	$scope.driverScheduleList = returnJson.driverScheduleList;
	   				$scope.datesList = returnJson.dayWiseShiftCountList;
	   				$scope.daysList = returnJson.daysList;
	   				$scope.shiftlist = returnJson.shiftlist;
	   				$scope.startDate = returnJson.startDate;
	   				$scope.endDate = returnJson.endDate;
	   				$scope.totalNoOfShifts = returnJson.totalNoOfShifts;
	   				$scope.differentShiftList = returnJson.differentShiftList;
	   				$scope.isScheduleGenarated = returnJson.isScheduleGenarated;
	   				if($scope.driverScheduleList.length == 0)
	   					$scope.ifNoRecordFound = true;
	   				
		   			if(!returnJson.isSuccess){
		   			 	$scope.ifNoRecordFound = true;
		   				$scope.serviceMessage = returnJson.serviceMessage;
		   			}
	   			console.log(JSON.stringify(returnJson));

	   		}).error(function(returnErrorJson, status, headers, config) {
	   			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
	   			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
	   			if(isFreshTokenGenarated){
	   				$scope.getScheduleOfDriver(startDate,endDate,whatDatesToFetch);
	   			}
	   		});//error
	   	};//getScheduleOfDriver end
	   	
	   	
	   	/**
       	 * To genarate schedule of driver
       	 * 
       	 * 
       	 * @return successs
       	 */
		$scope.genarateScheduleForDrivers =  function genarateScheduleForDrivers() {
	   		
	   		var shiftObject ={};
	   		shiftObject.userId =$scope.userId;
	   		
	   		console.log("shiftObject - "+  JSON.stringify(shiftObject));
	   		
	   	  $http.post(SERVICEADDRESS + '/genarate/shiftschedule', JSON.stringify(shiftObject)).
              success(function(returnJson) {
            	  		$scope.serviceMessage = returnJson.serviceMessage;
            	  		$scope.getScheduleOfDriver('','',3);
	   		}).error(function(returnErrorJson, status, headers, config) {
	   			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
	   			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
	   			if(isFreshTokenGenarated){
	   				$scope.genarateScheduleForDrivers();
	   			}
	   		});//error
	   	};//genarateScheduleForDrivers end
    			   	
    			   	
    /**
   	 * To fetch shifts through service 
   	 * 
   	 * 
   	 * @return selection array
   	 */
    	 
   	$scope.getShiftList =  function getShiftList() {
   		$scope.isValidated = true; //scope variable to check against service message
   		$scope.ifNoRecordFound = false;
   		$scope.shifts = [];
   		
	   	 if(angular.isUndefinedOrNull($scope.shift.shiftDayId) || $scope.shift.shiftDayId.length == 0){
	   		$scope.shiftList.splice(0);
	   		$scope.addNewShift();
	  	   return false;
	     }
   		
   		var shiftObject  = {};
   		shiftObject.shiftDayId = $scope.shift.shiftDayId;
   		console.log("shiftObject "+ JSON.stringify(shiftObject));
   		$http.post(SERVICEADDRESS+'/shift/getShifts', JSON.stringify(shiftObject)).
   		success(function(returnJson) {

   			if(returnJson.isSuccess){
   				$scope.shiftList = returnJson.shiftList;
   				
   				if(angular.isUndefinedOrNull($scope.shiftList) || $scope.shiftList.length == 0){
   					$scope.shiftList = [];
   					$scope.addNewShift();
   				}
   			}else{
   			 
   				$scope.ifNoRecordFound = true;
   				$scope.serviceMessage = returnJson.serviceMessage;
   			}
   			console.log(JSON.stringify(returnJson));

   		}).error(function(returnErrorJson, status, headers, config) {
   			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
   			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage);
   			if(isFreshTokenGenarated){
   				$scope.getShiftList();
   			}
   		});//error
   	};//getShiftsList function end
   	
    /* * * * * * * * * * * *
	 * get list of shifts
	 * 
	 * * * * * * * * * * * */
    $scope.getShiftDetails = function() {
    	$scope.isValidated = true; //scope variable to check against service message
		$scope.onSuccess = false;
    	 var shiftArray = [];
			$http({
		        method: 'GET',
		        url: SERVICEADDRESS + '/shiftTimmings/getShiftTimmings'
		    }).success(function(returnJson){
		    	var listOfShifts = returnJson.shiftList;
		    	for(var i=0;i < listOfShifts.length;i++){
		    		 var shiftObject ={};
		    		 shiftObject.shiftDay = listOfShifts[i].shiftDay;
		    		 shiftObject.shiftId = listOfShifts[i].shiftId;
		    		 shiftObject.shiftName = listOfShifts[i].shiftName;
					 shiftObject.startTime = listOfShifts[i].startTime;
					 shiftObject.endTime = listOfShifts[i].endTime;
					 shiftObject.vehiclesCount = listOfShifts[i].vehiclesCount;
					 shiftObject.driversCount =  listOfShifts[i].driversCount;
					 shiftArray.push(shiftObject);
		           }
		    	$scope.shiftList = 	shiftArray;
		    	console.log("shiftList - "+ angular.toJson($scope.shiftList));
			}).error(function(returnErrorJson, status, headers, config) {
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.getShiftDetails(); 
				}
			});//end of http post call
		
	};
   	
  //json search to find out location where the value resides
	$scope.getValuefromJson =  function (arr, coparableValue, propToSerach,propToGet) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop] - "+ arr[k][prop]);
			var indexValue = arr[k][prop];
			var searcValue = coparableValue;
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexOf =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log("arr[k][prop] - "+ arr[k][prop]);
			var indexValue = arr[k][prop].trim().toString();
			var searcValue = coparableValue.trim().toString();
			if (indexValue.indexOf(searcValue) >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to access permissions
	$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop1].trim().toLowerCase();
			var searcValue = comparableValue.trim().toLowerCase();
			if (indexValue.indexOf(searcValue) >= 0) {
				//var canAccess = arr[k][prop2];
				return arr[k][prop2];
			}
		}
		return -1;
	};//end
   
	$scope.counter = 0;
	//json search to find out location where the value resides
	$scope.getDynamicColSpan =  function (arr, coparableValue,isWorkingOnthatDate, currentLoopingDay) {
		
		if(!coparableValue || !isWorkingOnthatDate){
			$scope.counter = 1;
			return 1;
		}else{
			var propToSerach = "shiftDay";
			var propToGet = "shiftCountPerDay";
			var l = arr.length,
			k = 0;
			for (k = 0; k < l; k = k + 1) {
				var indexValue = arr[k][propToSerach];
				var searcValue = currentLoopingDay;
				if (indexValue == searcValue) {
					$scope.counter = $scope.counter + 1;
					return arr[k][propToGet];
				}
			}
			return 1;
		}
	};//end
	
       /**
        * logout
        */
    
      $scope.logoutUser = function logoutUser() {
      	$http.defaults.headers.common['Authorization'] = "bearer "+ accessToken + " " +  userId;
   		//service call
   		$http.post(SERVICEADDRESS+'/logout').
   		  success(function(returnJson) {
   			    	
   			    	if(returnJson.isLoggedOut){
   			    		localStorage.clearStorage();
   			    		window.location.href = HOSTADDRESS+"/login.html";
   			    	
   			    	}else{
   			    		
   			    	}
   			    	
   		  }).error(function(returnErrorJson, status, headers, config) {
   			  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
   				if(isFreshTokenGenarated){
   					$scope.logoutUser(); 
   				}
   		 });//error
   		  
   	  };//logoutUser function end
   	  
   
     
  	

});//end of shift controller