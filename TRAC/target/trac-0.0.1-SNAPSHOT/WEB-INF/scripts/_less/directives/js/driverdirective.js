TRACAPP.directive('autoComplete', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	iElement.trigger('input');
	                        var selectedCountry =  scope.driver.country;
	                        scope.$apply(function(){
	                        	scope.$parent.isoCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
	                        	scope.$parent.isCountrySelected = false;
	                        });
	                   	 
                    }, 0);
                }
            });
    };
});


TRACAPP.directive('emergencyComplete', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                        iElement.trigger('input');
	                        var selectedCountry =  scope.driver.eCountry;
	                        scope.$apply(function(){
	                        	scope.$parent.eIsoCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
	                        	scope.$parent.isECountrySelected = false;
	                        });
	                   	 
                    }, 0);
                }
            });
    };
});


