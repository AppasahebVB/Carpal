

TRACAPP.directive('scrollUp', function () {       
    return {
    	restrict: 'A',
        link: function(scope, element, attrs, controller) {
           // element.on("click", function(e) { 
            	console.log('scrollUp');               
                element.addClass("vertical");        
          //  });
        }
    };
});


TRACAPP.directive('capitalize', function() {
	   return {
	     require: 'ngModel',
	     link: function(scope, element, attrs, modelCtrl) {
	        var capitalize = function(inputValue) {
	           if(inputValue == undefined) inputValue = '';
	           var capitalized = inputValue.toUpperCase();
	           if(capitalized !== inputValue) {
	              modelCtrl.$setViewValue(capitalized);
	              modelCtrl.$render();
	            }         
	            return capitalized;
	         };
	         modelCtrl.$parsers.push(capitalize);
	         capitalize(scope[attrs.ngModel]);  // capitalize initial value
	     }
	   };
});




TRACAPP.directive('serviceTime', function() {
    var directive = {};
    directive.restrict = 'E';
    directive.template = "{{cases.hh}}:{{cases.mm}}";
    
    directive.scope = {
    		cases : "=value"
    };
    directive.compile = function(element, attributes) {
     
       var linkFunction = function($scope, element, attributes) {
          element.html($scope.cases.hh +":"+$scope.cases.mm);
       };

       return linkFunction;
    };

    return directive;
 });
 



TRACAPP.directive('ngRepeatEndWatch', function() {
   return {
      scope: {},
      link: function(scope, element, attrs) {
         console.log(scope, element, attrs);
         if (scope.$parent.$last) {
            $('.bootstrap-timepicker').timepicker();
         }
      }
   };
});


TRACAPP.directive('autoCompleteMobileSearch', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
            	minLength:6,
            	source:function (request, response) {
            		console.log("in autoCompleteMobileSearch");
            		var serachList = scope.searchCustomerDetails(scope.cases);
            		console.log("serachList - "+serachList);
            		response(serachList);
                },
                select: function() {
                    $timeout(function() {
                    		iElement.trigger('input');
                    		scope.getCaseDetails(scope.cases);
                      }, 0);
                }
            })
    };
});


TRACAPP.directive('autoCompleteCustomerCountry', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	iElement.trigger('input');
	                        var selectedCountry =  scope.cases.customerLocationCountryName;
                        	var customerLocationCountryCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
                        	scope.setValuesOfDirectives(customerLocationCountryCode,'customerLocationCountryCode');
                        	scope.setValuesOfDirectives(false,'isCountrySelected');
                      }, 0);
                }
            });
    };
});

TRACAPP.directive('autoCompleteVehicleCountry', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
            	
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	iElement.trigger('input');
	                        var selectedCountry =  scope.cases.vehcileCountryISO;
                        	var vehcileCountryISOCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
                        	scope.setValuesOfDirectives(vehcileCountryISOCode,'vehcileCountryISOCode');
                        	scope.setValuesOfDirectives(false,'isVehicleCountrySelected');
                      }, 0);
                }
            });
    };
});

TRACAPP.directive('autoCompleteServiceLocationCountry', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	iElement.trigger('input');
	                        var selectedCountry =  scope.cases.serviceLocationCountryName;
                        	var slCountryISOCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
                        	scope.setValuesOfDirectives(slCountryISOCode,'serviceLocationCountryCode');
                        	scope.setValuesOfDirectives(false,'isSLCountrySelected');
                      }, 0);
                }
            });
    };
});

TRACAPP.directive('autoCompleteDestLocationCountry', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	iElement.trigger('input');
	                        var selectedCountry =  scope.cases.destinationLocationCountryName;
                        	var destCountryISOCode = scope.getValueFromJson(scope.countryJson,selectedCountry.toString(),"Name","ISOCode");
                        	scope.setValuesOfDirectives(destCountryISOCode,'destinationLocationCountryCode');
                        	scope.setValuesOfDirectives(false,'isdestinationLocSelected');
                      }, 0);
                }
            });
    };
});


TRACAPP.directive('autoCompleteVehicle', function($timeout) {
	
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                    	var selectedFormFiled = iAttrs.id;
                        iElement.trigger('input');
                        scope.directveModels = [];
                     	  scope.directveModelId =  [];
                      //for vehicle brands
                     if(selectedFormFiled == "vehicleBrandId" || selectedFormFiled.indexOf("vehicleBrandId") >= 0){
                    	 var selectedBrand =  scope.cases.vehicleBrandId;
                    	 var indexLoc = parseInt(scope.getIndexOf(scope.brandModelList,selectedBrand.toString(),"vehicleBrandName"));
	                      
	                      if(indexLoc >= 0){
	                        scope.$parent.isBrandValid = true;
	                     	scope.brandModels =  scope.brandModelList[indexLoc].brandModels;
	                        var brandModelListLength = scope.brandModels.length;
	                	    var selectedBrandId  =  scope.brandModelList[indexLoc].vehicleBrandId;
	                    	 scope.setValuesOfDirectives(selectedBrandId,'selectedBrandId');
	                     	 scope.setValuesOfDirectives(false,'isBrandValid');
	                     	 scope.setValuesOfDirectives(indexLoc,'selectedBrandIndex');
		                	 scope.$apply(function(){
		                		// scope.$parent.selectedBrandIndex = indexLoc;
		                		   var noOfModels= parseInt(scope.$parent.models.length);
		                		  	if(noOfModels > 0){
		                           	 	scope.$parent.models.splice(0);
		                		  		scope.$parent.modelIds.splice(0);
		                             }
		                		  
				      				for(var i=0;i<brandModelListLength;i++){
				      					console.log("vehicleModelName - "+scope.brandModels[i].vehicleModelName);
				      					scope.$parent.models.push(scope.brandModels[i].vehicleModelName);	
				      					scope.$parent.modelIds.push(scope.brandModels[i].vehicleModelId);
				      				}
				      				
				      				
				      	         });
	                      }else{
	                    	  scope.$parent.models.splice(0);
	                      }
	                      
				      			
                     		}
                     
		                     //for vehicle models
		                     if(selectedFormFiled == "vehicleModelId" || selectedFormFiled.indexOf("vehicleModelId") >= 0){
		                    	 var selectedModel =  scope.cases.vehicleModelId;
		                    	 var indexLoc = scope.getIndexOf(scope.brandModels,selectedModel.toString(),"vehicleModelName");
		                    	 var selectedModelId  = scope.brandModels[indexLoc].vehicleModelId;
		                    	 scope.setValuesOfDirectives(selectedModelId,'selectedModelId');
		                     		scope.setValuesOfDirectives(true,'isModelValid');
			                    	 
		                     }
		                      
                    }, 0);
                }
            });
    };
});




TRACAPP.directive('mapCanvas', function() {
	return {
	scope: {
		datasource: '=',
	},
	link: function(scope, element,iAttrs) {

	initialize();

	function initialize() {
	var infowindow = null;
	var myCenter = new google.maps.LatLng(24.9500, 55.3333);

	//setting default coordinate to dubai
	var mapOptions = {
		center: {
			lat: 24.9500,
			lng: 55.3333
		},
		zoom: 8
	};
	var map = new google.maps.Map(document.getElementById('map-canvas'),
	mapOptions);

	var marker = new google.maps.Marker({
		position: myCenter
	});
	
	scope.$on('place-marker', function (event,args) {
		var locationLatitude =args.locationLatitude;
		var locationLongitude =args.locationLongitude;

		var locationLatLong = new google.maps.LatLng(locationLatitude, locationLongitude);

		marker.setMap(null);
		marker = new google.maps.Marker({
			position: locationLatLong
		});
		// Zoom to 9 when clicking on marker
		google.maps.event.addListener(marker, 'click', function() {
			map.setZoom(9);
			map.setCenter(marker.getPosition());
		});
		map.panTo(marker.getPosition());
		marker.setMap(map);
		 
	});


	// Zoom to 9 when clicking on marker
	google.maps.event.addListener(marker, 'click', function() {
		map.setZoom(9);
		map.setCenter(marker.getPosition());
	});

	//Listener for adding marker on map
	google.maps.event.addListener(map, 'click', function(event) {
		marker.setMap(null);
		placeMarker(event.latLng);	
	});

	function placeMarker(location) {
		marker = new google.maps.Marker({
		position: location

	});

	marker.setMap(map);
	//infowindow.open(map, marker);


	var geocoder = new google.maps.Geocoder();

	var lat = parseFloat(location.lat());
	var lng = parseFloat(location.lng());
	var latlng = new google.maps.LatLng(lat, lng);
	geocoder.geocode({
		'latLng': latlng
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						if (infowindow)
							infowindow.close();
				
					infowindow = new google.maps.InfoWindow({
						content: 'Latitude: ' + location.lat() +
						'<br>Longitude: ' + location.lng()
					});
					infowindow.setContent(results[0].formatted_address);
				
					var addressdetails = {};
					addressdetails.latitude =  location.lat();
					addressdetails.longitude =  location.lng();
						for (var i = 0; i < results[0].address_components.length; i++) {
							console.log(results[0].address_components[i].types[0] + " " + results[0].address_components[i].long_name);
						
								if(results[0].address_components[i].types[0] === 'sublocality_level_1' )
									addressdetails.area = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'route' ) 
									addressdetails.street = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'street_address' ) 
									addressdetails.street = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'bus_station' || results[0].address_components[i].types[0] === 'premise' || results[0].address_components[i].types[0] === 'subpremise' || results[0].address_components[i].types[0] === 'administrative_area_level_2'  ) 
									addressdetails.building = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'administrative_area_level_1') 
									addressdetails.landmark = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'locality') 
									addressdetails.city = results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'postal_code') 
									addressdetails.poBox= results[0].address_components[i].long_name;
								else if(results[0].address_components[i].types[0] === 'country') {
									addressdetails.countryName= results[0].address_components[i].long_name;
									addressdetails.countryCode= results[0].address_components[i].short_name;
								}
				
						if(iAttrs.datasource == 'contactdetails')
							scope.$parent.setValues(addressdetails,'contactdetails');
						else if(iAttrs.datasource == 'destinationlocation')
							scope.$parent.setValues(addressdetails,'destinationlocation');
						else if(iAttrs.datasource == 'vehicleservicelocation')
							scope.$parent.setValues(addressdetails,'vehicleservicelocation');
					
						}
				
				
					infowindow.open(map, marker);
			} else {
		// alert('No results found');
		  }
	   }
	});
	}

	}

}
};
});


TRACAPP.directive('mapCanvas2', function() {
	return {
	scope: {
		datasource: '=',
	},
	link: function(scope, element,iAttrs) {

	initialize();

	function initialize() {
	var infowindow = null;
	var myCenter = new google.maps.LatLng(24.9500, 55.3333);

	//setting default coordinate to dubai
	var mapOptions = {
		center: {
			lat: 24.9500,
			lng: 55.3333
		},
		zoom: 8
	};
	var map = new google.maps.Map(document.getElementById('map-canvas2'),
	mapOptions);

	var marker = new google.maps.Marker({
		position: myCenter
	});
	
	
		
	var markersDataArray = [];
	var markersArray = [];
	
	var totalCount = 0;
	scope.$on('total-casecount', function (event,args) {
	    totalCount = args.totalCaseCount;
		 infowindow = new google.maps.InfoWindow({
				content:''+args.totalCaseCount
			});
		 
			//infowindow.setContent(totalCount);
			
			infowindow.open(map, marker);
			
			marker.setMap(map);
			
	});
	scope.$on('place-marker2', function (event,args) {
		var locationLatitude =args.locationLatitude;
		var locationLongitude =args.locationLongitude;
		var casecount =args.casecount;
		var CaseId =args.CaseId;
		markersDataArray.push({"locationLatitude":locationLatitude,"locationLongitude":locationLongitude,"casecount":casecount});
		

  		var locationLatLong = new google.maps.LatLng(locationLatitude, locationLongitude);
  		
  		
		var marker = new google.maps.Marker({
  			position: locationLatLong

  		});
		
		markersArray.push(marker);
		
	});

	 
	// Zoom to 9 when clicking on marker
	google.maps.event.addListener(marker, 'click', function() {
		map.setZoom(10);
		map.setCenter(marker.getPosition());
	});

	
	var zoomLevel = 1;

	google.maps.event.addListener(map, 'zoom_changed', function() {
	  var i, prevZoomLevel;

	  prevZoomLevel = zoomLevel;

	  map.getZoom() < 9 ? zoomLevel = 1 : zoomLevel = 2;

	  if (prevZoomLevel !== zoomLevel) {
		 // marker.setMap(null); 
	    for (i = 0; i < markersDataArray.length; i++) {
	    	 var marker     = markersArray[i];
	    	 var markerData = markersDataArray[i];
	      if (zoomLevel === 2) {
	    	  
	    	  
	    	  
	    	  infowindow = new google.maps.InfoWindow({
	  			content: 'Latitude: ' + markerData.locationLatitude +
	  			'<br>Longitude: ' + markerData.locationLongitude
	  		});
	  		infowindow.setContent(markerData.casecount);
	  		
	  		infowindow.open(map, marker);
	  		
	  		marker.setMap(map);
			
	      }
	      else {
	    	  var locationLatLong = new google.maps.LatLng(markerData.locationLatitude, markerData.locationLongitude);
	    		
	    	  if(map.getBounds().contains(locationLatLong)){
	    		  marker.setMap(null);  
	    	  }
	      }
	    }
	 }
	
	});
	
	function placeMarker(location) {
		marker = new google.maps.Marker({
		position: location

	});
 
	}

	}

}
};
});

