TRACAPP.config(function($routeProvider) {
    $routeProvider

	     // route for the home page
	    .when('/*', {
	        templateUrl : '/resources/views/bodytemplates/case_schedule_main.html',
	        controller  : 'mainController'
	    })
	    
	    .when('/createuser', {
	        templateUrl : '/resources/views/register.html',
	        controller  : 'registrationController'
	    })
	    .when('/changepassword', {
	        templateUrl : '/resources/views/bodytemplates/changepassword.html',
	        controller  : 'mainController'
	    })
        .when('/reports', {
            templateUrl : '/resources/views/bodytemplates/reports.html',
            controller  : 'mainController'
        })
        .when('/reports/:param1', {
            templateUrl : '/resources/views/bodytemplates/reports.html',
            controller  : 'mainController'
        })
        .when('/listofsavedreports', {
            templateUrl : '/resources/views/bodytemplates/reportslist.html',
            controller  : 'mainController'
        })
        .when('/servicelocationreport', {
            templateUrl : '/resources/views/bodytemplates/reportservicelocationslist.html',
            controller  : 'mainController'
        })
        
        //role module
        .when('/createrole', {
            templateUrl : '/resources/views/bodytemplates/roles_body.html',
            controller  : 'roleController'
        })
        //role module
        .when('/listofroles', {
            templateUrl : '/resources/views/bodytemplates/list_of_roles_main.html',
            controller  : 'roleController'
        })
        
        
         //expenses module
        .when('/truckexpenses', {
            templateUrl : '/resources/views/bodytemplates/truck_expenses_body.html',
            controller  : 'expensesController'
        })
        .when('/listoftruckexpenses', {
            templateUrl : '/resources/views/bodytemplates/list_of_truck_expenses.html',
            controller  : 'expensesController'
        })
        
        
        //shift module
        .when('/shifttimings', {
            templateUrl : '/resources/views/bodytemplates/shift_timings_body.html',
            controller  : 'shiftcontroller'
        })
        .when('/shiftschedule', {
            templateUrl : '/resources/views/bodytemplates/shift_schedule_body.html',
            controller  : 'shiftcontroller'
        })
        
        //case module
        .when('/createcase', {
            templateUrl : '/resources/views/bodytemplates/main_case.html',
            controller  : 'casecontroller'
        })
        .when('/createcase/:param1/:param2/:param3', {
            templateUrl : '/resources/views/bodytemplates/main_case.html',
            controller  : 'casecontroller'
        })
        .when('/assigncase', {
            templateUrl : '/resources/views/bodytemplates/case_assign_body.html',
            controller  : 'casecontroller'
        })
        .when('/caseprereqisite', {
            templateUrl : '/resources/views/bodytemplates/case_flow_main.html',
            controller  : 'casecontroller'
        })
        .when('/caseonjobdetails', {
            templateUrl : '/resources/views/bodytemplates/case_onJobdetail_body.html',
            controller  : 'casecontroller'
        })
        .when('/caseschedule', {
            templateUrl : '/resources/views/bodytemplates/case_schedule_main.html',
            controller  : 'casecontroller'
        })
        .when('/caseschedule/:param1', {
            templateUrl : '/resources/views/bodytemplates/case_schedule_main.html',
            controller  : 'casecontroller'
        })
        .when('/listOfCases', {
            templateUrl : '/resources/views/bodytemplates/listOfCases.html',
            controller  : 'casecontroller'
        })
        .when('/listOfCasesInQueue', {
            templateUrl : '/resources/views/bodytemplates/listOfCasesInQueue_body.html',
            controller  : 'casecontroller'
        })
        
        
        // route for the module truck
        .when('/truckregistration', {
            templateUrl : '/resources/views/bodytemplates/truck_registration_body.html',
            controller  : 'truckcontroller'
        }).when('/listOfTrucks', {
            templateUrl : '/resources/views/bodytemplates/truck_list_body.html',
            controller  : 'truckcontroller'
        })
        
        // route for the module driver
	    .when('/driverregistration', {
	        templateUrl : '/resources/views/bodytemplates/driver_personaldetails.html',
	        controller  : 'drivercontroller'
	    }).when('/driverpunchtimings', {
	        templateUrl : '/resources/views/bodytemplates/driver_punchTimings_body.html',
	        controller  : 'drivercontroller'
	    }).when('/savedriverconatctdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_contactDetails_body.html',
	        controller  : 'drivercontroller'
	    }).when('/savedriveremergencyconatctdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_emergencyConDetails_body.html',
	        controller  : 'drivercontroller'
	    }).when('/savedriverworkexperience', {
	        templateUrl : '/resources/views/bodytemplates/driver_workExperience_body.html',
	        controller  : 'drivercontroller'
	    }).when('/savedriverworkdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_workDetails_body.html',
	        controller  : 'drivercontroller'
	    }).when('/updatedriverpersonaldetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_personalDetails_update_body.html',
	        controller  : 'drivercontroller'
	    }).when('/updatedrivercontactdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_contactDetails_update_body.html',
	        controller  : 'drivercontroller'
	    }).when('/updatedriveremergencycontactdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_emergencyConDetails_update_body.html',
	        controller  : 'drivercontroller'
	    }).when('/updatedriverworkexperience', {
	        templateUrl : '/resources/views/bodytemplates/driver_workExperience_update_body.html',
	        controller  : 'drivercontroller'
	    }).when('/updatedriverworkdetails', {
	        templateUrl : '/resources/views/bodytemplates/driver_workDetails_update_body.html',
	        controller  : 'drivercontroller'
	    }).when('/listOfDrivers', {
	        templateUrl : '/resources/views/bodytemplates/listOfDrivers.html',
	        controller  : 'drivercontroller'
	    })
    
	    //access denied
	    .when('/accessdenied', {
	        templateUrl : '/resources/views/bodytemplates/accessDenied.html',
	        controller  : 'mainController'
	    });
}).run( function($rootScope, $location,$http,localStorage) {
	   // register listener to watch route changes
	   $rootScope.$on( "$locationChangeStart", function(event, next, current) {
		          var inputObject={};
				  inputObject.userId = localStorage.getData("userId");
				  var url = $location.absUrl();
				  url = url.split("#/")[1];
				  
				  
				 if(url != null){
					 if(url.indexOf('/') > 0){
						  url = url.split("/")[0];
					  }
					  if(url.length>0 && url.indexOf('accessdenied') < 0){
						  inputObject.htmlPage = url;
				          $http.post(SERVICEADDRESS + '/user/access', JSON.stringify(inputObject)).
				          success(function(returnJson) {
				          //console.log("returnJson - "+JSON.stringify(returnJson));
				        	 
				            	if(!returnJson.isAccessible){
				            		 $location.path( "/accessdenied");
				            	}
				            		 event.preventDefault();
				            	
				          });
					  }
				  }
	   });
	});

