
function registrationController($scope, $http) {
	$scope.isServiceValidationExist = false;

	$scope.rolesList = [];
	//method to fetch security questions
	$scope.getRolesList = function getRolesList() {
		$http({
	        method: 'GET',
	        url: SERVICEADDRESS + '/user/getroles'
	    }).success(function(returnJson){
	    	
		    $scope.rolesList = returnJson.userRolesList;
            
       
		}).error(function(returnErrorJson, status, headers, config) {
					console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
		});//end of http post call
	};
}//registrationController end

var registerAppModule = angular.module("TRACREGISTER", []);
registerAppModule.controller('registrationController', registrationController);
