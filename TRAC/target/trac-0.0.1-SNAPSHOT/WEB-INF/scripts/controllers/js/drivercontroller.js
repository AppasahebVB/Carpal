

TRACAPP.config(function (datepickerConfig) {
    datepickerConfig.showWeeks = false;
});

TRACAPP.controller("drivercontroller", function($scope, $http, localStorage,$compile,$timeout,$location,$route,dataService,$filter) {
	 $scope.datePattern=/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i;
	
	//display name or email of user
	$scope.displayName = localStorage.getData("displayName");
	
	$scope.isValidated = true; //scope variable to check against service message
	$scope.isDriverPrviousExpUpdated = false;
	
    $scope.accessToken = localStorage.getData("accessToken"); //access token
    $scope.userId = localStorage.getData("userId"); //user id
    $scope.currentRowId = 1; 
    $scope.maxLengthValidCount = 0;
    $scope.previopusWorkExpArray = [];
    $scope.deletedPreviopusWorkExpArray =[];
    $scope.view = '/resources/views/bodytemplates/driver_personalDetails_body.html';
    $scope.listOfDriversView = '/resources/views/bodytemplates/driver_listofDrivers_body.html';
    
    angular.isUndefinedOrNull = function(val) {
		return angular.isUndefined(val) || val === null;
	};
    
	//common headers for all http posts
    $http.defaults.headers.post["Content-Type"] = "application/json";
    $http.defaults.headers.common['Authorization'] = "bearer " + $scope.accessToken;
    
  
    /**
     ng-options Values for DriverWorkDetails Screen in Driver Module
    */
    $scope.shifts = [];
    $scope.trucks = [];
    $scope.managers = [];
    $scope.modules = [];
   
   // $scope.days = ['Sunday', 'Monday', 'TuesDay','WednesDay','ThrusDay','FriDay','SaturDay'];
    $scope.days = [{"ID":1,"dayName":"Sunday"},{"ID":2,"dayName":"Monday"},{"ID":3,"dayName":"Tuesday"},{"ID":4,"dayName":"Wednesday"},{"ID":5,"dayName":"Thursday"},{"ID":6,"dayName":"Friday"},{"ID":7,"dayName":"Saturday"}];
    $scope.shirtsizes = ['S','M','L','XL','XXL','XXXL' ];
    $scope.trousersizes = ['28','30', '32','34','36','38','40','42','44','46'];
    $scope.shoessize = ['06','07','08','09','10','11'];
    $scope.shirts = ['01','02','03','04','05','06','07','08','09','10'];
    $scope.trouers =  ['01','02','03','04','05','06','07','08','09','10'];
    $scope.shoes =  ['01','02','03','04','05','06','07','08','09','10'];
    
    
	$scope.canUpdatePersonalDetails = true;
	$scope.canUpdateConatctDetails = true;
	$scope.canUpdateEmergencyContact = true;
	$scope.canUpdateDriverPrviousExp = true;
	
	 $scope.previopusWorkExpIdsArray = [];
	 $scope.listOfPermissions = [];
	 
	//the model returns a promise and THEN items
	 dataService.getItems().then(function(items) {
	        $scope.listOfPermissions=items;
	 }, function (status) {
        console.log(status);
    });
	 
	 $timeout(function(){
         $scope.canLoadNow = true;
    }, 1000);

    /**
	 * To validate date
	 * it returns true / false
	 * 
	 * @param  date1,date2
	 * @return true/false
	 */
    
	 $scope.compareDate = function (firstDate,secondDate){
	   	 if(!angular.isUndefinedOrNull(firstDate) && !angular.isUndefinedOrNull(secondDate)){
	   		
	   		 if(new Date(firstDate) < new Date(secondDate)){
					return true;
				}
	   		 
	   	 }
	   	return false;
	   };//end
   
  
  
    /**
     * Driver Personal Details Function
     */

    $scope.saveDriverPersonalDetails = function saveDriverPersonalDetails(driver,isUpdateRequired) {
    	
    	var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register driver";
			return false;
		}
		
		 
		
    	var driverObject ={};
    	var isValid = $scope.tracForm.$valid;
    	var serviceUrl;
    	
    	if(!isUpdateRequired){
    		serviceUrl = SERVICEADDRESS + '/driver/savePersonalDetailsOfDriver';
    	}else{
    		serviceUrl = SERVICEADDRESS + '/driver/editPersonalDetailsOfDriver';
    		driverObject.driverId = $scope.driverId;
    		driverObject.createdBy = $scope.userId;
    	}
    	
    	
		
		
		if(!angular.isUndefinedOrNull(driver)){
		
			driverObject.licenseNoExpiredAt = driver.licenseNoExpiredAt;
			driverObject.internationalLicenseExpiredAt = driver.internationalLicenseExpiredAt;
			driverObject.passportIssuedAt = driver.passportIssuedAt;
			driverObject.visaIssuedAt = driver.visaIssuedAt;
			driverObject.passportExpiredAt = driver.passportExpiredAt;
			driverObject.visaExpiredAt = driver.visaExpiredAt;
			driverObject.healthInsuranceCardExpiryAt = driver.healthInsuranceCardExpiryAt;
			driverObject.governmentIdNumber = driver.governmentIdNumber;
			driverObject.nationality = driver.nationality;
		    
			
			
	        /* if(!angular.isUndefinedOrNull(driverObject.passportIssuedAt)){
				var passportIssuedAt = driverObject.passportIssuedAt .split('-');
				var passportIssuedAtMonth = parseInt(passportIssuedAt[1]) - 1;
				passportIssuedAtMonth = "0"+passportIssuedAtMonth;
				passportIssuedAt = new Date(passportIssuedAt[2], passportIssuedAtMonth, passportIssuedAt[0]); //Year, Month, Date
				
			}*/

			/*if(!angular.isUndefinedOrNull(driverObject.passportExpiredAt) ){
				var passportExpiredAt = driverObject.passportExpiredAt.split('-');
				var passportExpiredAtMonth = parseInt(passportExpiredAt[1]) - 1;
				passportIssuedAtMonth = "0"+passportExpiredAtMonth;
				passportExpiredAt = new Date(passportExpiredAt[2], passportExpiredAtMonth, passportExpiredAt[0]);
			}

			if(!angular.isUndefinedOrNull(driverObject.visaIssuedAt)){
				var visaIssuedAt = driverObject.visaIssuedAt .split('-');
				var visaIssuedAtMonth = parseInt(visaIssuedAt[1]) - 1;
				passportIssuedAtMonth = "0"+visaIssuedAtMonth;
				visaIssuedAt = new Date(visaIssuedAt[2], visaIssuedAtMonth, visaIssuedAt[0]); //Year, Month, Date
			}
			if(!angular.isUndefinedOrNull(driverObject.visaExpiredAt)){
				var visaExpiredAt = driverObject.visaExpiredAt .split('-');
				var visaExpiredAtMonth = parseInt(visaExpiredAt[1]) - 1;
				passportIssuedAtMonth = "0"+visaExpiredAtMonth;
				visaExpiredAt = new Date(visaExpiredAt[2], visaExpiredAtMonth, visaExpiredAt[0]); //Year, Month, Date
			}*/
			
			if(driverObject.visaIssuedAt<driverObject.passportIssuedAt)
			{
				$scope.isVisaIssuedAtValid =true;
				$scope.errorMessage = "Visa Issued Date cannot be before Passport Issued Date.";
				return false;
			}
			if(driverObject.passportExpiredAt<driverObject.passportIssuedAt)
			{
				$scope.isPassportExpiredAtValid =true;
				$scope.errorMessage = "Passport Expired Date cannot be before Passport Issued Date.";
				return false;
			}
			
			else if(driverObject.visaExpiredAt<driverObject.visaIssuedAt)
			{
				$scope.isVisaExpiredAtValid = true;
				$scope.errorMessage = "Visa Expired date cannot be before Visa Issued Date.";
				return false;
			}
			else{//All Validations Done
				$scope.errorMessage = ""; 
				$scope.isPassportIssuedAtValid = false;
				$scope.isPassportExpiredAtValid = false;
				$scope.isVisaIssuedAtValid = false;
				$scope.isVisaExpiredAtValid = false;

	        if(isValid){
	        	$scope.driver.driverName = driver.driverFirstName + " " + driver.driverLastName;
	        	driverObject.driverName = driver.driverFirstName + " " + driver.driverLastName;
	        	driverObject.driverFirstName = driver.driverFirstName;
				driverObject.driverLastName = driver.driverLastName;
				driverObject.licenseNo = driver.licenseNo;
				driverObject.internationalLicenseNo =driver.internationalLicenseNo;
				driverObject.passportNo = driver.passportNo;
				driverObject.visaNo = driver.visaNo;
				driverObject.healthInsuranceCardNo = driver.healthInsuranceCardNo;
				driverObject.userId = $scope.userId;
				 
				  console.log("driverObject json " + JSON.stringify(driverObject));
				 
	       $http.post(serviceUrl, JSON.stringify(driverObject)).
	        success(function(returnJson) {
	
		            if (returnJson.isSuccess) { 
		            	$scope.isValidated = true;
		            	$scope.canUpdatePersonalDetails = false;
		            	if(!isUpdateRequired){
		            		$scope.driverId = returnJson.driverID;
		            		 $scope.getCountriesFroDriver();
		            	}
		                $scope.view = '/resources/views/bodytemplates/driver_contactDetails_body.html';
		            	//$location.path('/savedriverconatctdetails');
		                $("html, body").animate({ scrollTop: 0 }, 50);
		            } else {
		            	$scope.isValidated = false;
		                $scope.serviceMessage = returnJson.serviceMessage;
		            }
	
		        }).error(function(returnErrorJson, status, headers, config) {
		
		        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.saveDriverPersonalDetails(driver); 
				     }//end of if
		           
		
		        }); //error

	         }//if end
	        
			}//end of validation if
		
		}//end of isUndefinedOrNull
			
    };//end of saveDriverPersonalDetails

    /**
     * Driver ContactDetails Function
     */

    $scope.saveDriverContactDetails = function saveDriverContactDetails(driver,isUpdateRequired) {
    	
    	var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register driver";
			return false;
		}
		
    	var isValid = $scope.tracForm.$valid;
    	
    	var serviceUrl;
    	
    /*	 var enteredCountryName = $('#country').val();
  	   if(!angular.isUndefinedOrNull(enteredCountryName)){
  		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
  		   if(indexLoc  >= 0){
	  			 if(angular.isUndefinedOrNull($scope.isoCode)){
	  			   $scope.isoCode = $scope.countryJson[indexLoc].ISOCode;
	  			 }
  			   $scope.isCountrySelected = false;
  		   }else{
  				$scope.isCountrySelected = true;
  				return false;
  		   }
  		}*/
		
    	
    	if(!isUpdateRequired){
    		serviceUrl = SERVICEADDRESS + '/driver/saveContactDetailsOfDriver';
    	}else{
    		serviceUrl = SERVICEADDRESS + '/driver/editContactDetailsOfDriver';
    	}
    	 var contactDetailsinput = {
            userId: $scope.userId,
            driverId: $scope.driverId,
            phone: driver.phone,
            area: driver.area,
            building: driver.building,
            email: driver.email,
            street: driver.street,
            landmark: driver.landmark,
            postBox: driver.postBox,
            country: $scope.isoCode,
            city: driver.city
        };
        
        console.log("emergencyContactDetailsInput json " + JSON.stringify(contactDetailsinput));
         if(isValid){
	        	$http.post(serviceUrl, JSON.stringify(contactDetailsinput)).
			        success(function(returnJson) {
			            
			            if (returnJson.isSuccess) {
			            	$scope.isValidated = true;
			            	$scope.canUpdateConatctDetails = false;
			                $scope.view = '/resources/views/bodytemplates/driver_emergencyConDetails_body.html';
			                $("html, body").animate({ scrollTop: 0 }, 50);
			               // $location.path('/savedriveremergencyconatctdetails');
			            } else {
			            	$scope.isValidated = false;
			                $scope.serviceMessage = returnJson.serviceMessage;
			                //alert(returnJson.serviceMessage);
			            }
			
			        }).error(function(returnErrorJson, status, headers, config) {
			        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.saveDriverContactDetails(driver); 
						}
			            
		
			        }); //Error
        }

    };


    /**
     * Driver EmergencyContact Details
     */


    $scope.saveDriverEmergencyContactDetails = function saveDriverEmergencyContactDetails(driver,isUpdateRequired) {
    	
    	var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register driver";
			return false;
		}
		
    	var isValid = $scope.tracForm.$valid;
    	
    	var serviceUrl;
    	
    	 /*var enteredCountryName = $('#eCountry').val();
    	   if(!angular.isUndefinedOrNull(enteredCountryName)){
    		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
    		   if(indexLoc  >= 0){
    			   if(angular.isUndefinedOrNull($scope.isoCode)){
    	  			   $scope.eIsoCode = $scope.countryJson[indexLoc].ISOCode;
    	  			 }
    			   $scope.isECountrySelected = false;
    		   }else{
    				$scope.isECountrySelected = true;
    				return false;
    		   }
    		}*/
  		
    	
    	if(!isUpdateRequired){
    		serviceUrl = SERVICEADDRESS + '/driver/saveOrEditEmergencyContactDetailsOfDriver';
    	}else{
    		serviceUrl = SERVICEADDRESS + '/driver/saveOrEditEmergencyContactDetailsOfDriver';
    	}
        

        var emergencyContactDetailsInput = {
            userId: $scope.userId,
            driverId: $scope.driverId,
            eContactName: driver.eContactName,
            phone: driver.ePhone,
            area: driver.eArea,
            building: driver.eBuilding,
            email: driver.eEmail,
            street: driver.eStreet,
            landmark: driver.eLandmark,
            postBox: driver.ePostBox,
            country: $scope.eIsoCode,
            city: driver.eCity
        };
        
        console.log("emergencyContactDetailsInput json " + JSON.stringify(emergencyContactDetailsInput));
        
        if(isValid){
        $http.post(serviceUrl, JSON.stringify(emergencyContactDetailsInput)).
        success(function(returnJson) {

            if (returnJson.isSuccess) {
            	$scope.isValidated = true;
            	$scope.canUpdateEmergencyContact = false;
            	if(!isUpdateRequired){
            		$scope.previopusWorkExpArray.push({"experienceId": 0,"isNew":false,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
            	}
            	$scope.view = '/resources/views/bodytemplates/driver_workExperience_body.html';
            	 $("html, body").animate({ scrollTop: 0 }, 50);
            	 // $location.path('/savedriverworkexperience');
            } else {
            	$scope.isValidated = false;
                $scope.serviceMessage = returnJson.serviceMessage;
            }

        }).error(function(returnErrorJson, status, headers, config) {
        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveDriverEmergencyContactDetails(driver); 
			}

        }); //error
        }

    };

    /**
     * Driver WorkExperience Details
     */

    $scope.saveDriverWorkExperince = function saveDriverWorkExperince(driver,isUpdateRequired) {
    	
    	var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register driver";
			return false;
		}
		
    	 var isValid = $scope.tracForm.$valid;
    	 var isDateValid = false;
    	 var isMaxLengthValid = false;
    	 var serviceUrl;
    	 var noOfIds = $scope.previopusWorkExpArray.length;
    	 var noOfDeletedIds = $scope.deletedPreviopusWorkExpArray.length;
    	 var previousExpObject =[];
    	
    	
     	if(!isUpdateRequired){
     		serviceUrl = SERVICEADDRESS + '/driver/saveDriverWorkExperience';
     	}else{
     		serviceUrl = SERVICEADDRESS + '/driver/editDriverWorkExperience';
     		
     	}
     	
    	for(var i = 0,j=0;i < noOfIds; i++){
        
        	var previopusWorkExpObject = {};
         	var dateOfJoining = $scope.previopusWorkExpArray[i].dateOfJoining;
        	var dateOfRelieving = $scope.previopusWorkExpArray[i].dateOfRelieving;
          	var totalWorkExperience = $scope.previopusWorkExpArray[i].totalWorkExperience;
           	var relatedWorkExperience = $scope.previopusWorkExpArray[i].relatedWorkExperience;
           	var companyName = $scope.previopusWorkExpArray[i].companyName;
           	var previousdesignation = $scope.previopusWorkExpArray[i].previousdesignation;
           	
           	previopusWorkExpObject.totalWorkExperience = totalWorkExperience; 
	  		previopusWorkExpObject.relatedWorkExperience = relatedWorkExperience;
	  		previopusWorkExpObject.companyName = companyName;
	  		previopusWorkExpObject.previousdesignation = previousdesignation;
	  		previopusWorkExpObject.dateOfJoining = dateOfJoining;
	  		previopusWorkExpObject.dateOfRelieving = dateOfRelieving;
	  		previopusWorkExpObject.isDeleted = false;
	  		previopusWorkExpObject.userId = $scope.userId;
	  	    previopusWorkExpObject.driverId = $scope.driverId;
	  				
	  				
           	if(totalWorkExperience.length > 25)
           		isMaxLengthValid = true;
           		
           	if(relatedWorkExperience.length > 25)
           		isMaxLengthValid = true;
           			
           	if(companyName.length > 25)
           		isMaxLengthValid = true;
           				
           	if(previousdesignation.length > 25)
           		isMaxLengthValid = true;
  			
  			if(dateOfRelieving < dateOfJoining)
			{
				$("#dateOfRelievingValidation"+i).show();
				j++;
			}
			
			if(j == 0){
				isDateValid = true;
			}else{
				isDateValid = false;
			}
			if(isUpdateRequired){
				previopusWorkExpObject.experienceId = $scope.previopusWorkExpIdsArray[i];
				console.log("previopusWorkExpObject.experienceId - "+previopusWorkExpObject.experienceId);
				if(!angular.isUndefinedOrNull(previopusWorkExpObject.experienceId))
					previopusWorkExpObject.isNew = false;
				else
					previopusWorkExpObject.isNew = true;
				
			}
				
	  		previousExpObject.push(previopusWorkExpObject);
		}
				
        if(isValid && isDateValid && !isMaxLengthValid){
					
        	  for(var i = 0;i < noOfDeletedIds; i++){
        	  		 var deletedObjects =  $scope.deletedPreviopusWorkExpArray[i];
        	  		var previopusWorkExpObject = {};
        	  		previopusWorkExpObject.experienceId = deletedObjects.experienceId;
        	  		previopusWorkExpObject.isNew = false;
        	  		previopusWorkExpObject.isDeleted = true;
        	  		previopusWorkExpObject.totalWorkExperience = ''; 
        	  		previopusWorkExpObject.relatedWorkExperience = '';
        	  		previopusWorkExpObject.companyName = '';
        	  		previopusWorkExpObject.previousdesignation = '';
        	  		previopusWorkExpObject.dateOfJoining = '';
        	  		previopusWorkExpObject.dateOfRelieving = '';
        	  		previousExpObject.push(previopusWorkExpObject);
        	  	 }
					//driverPreviousWorkExp.tracFormList =  $scope.previopusWorkExpArray;
					console.log("driverPreviousWorkExp json " + angular.toJson(previousExpObject));
					
				      $http.post(serviceUrl, angular.toJson(previousExpObject)).
				        success(function(returnJson) {
				
				            if (returnJson.isSuccess) {
				            	 $scope.isValidated = true;
				            	 
				            	 if(noOfDeletedIds > 0){
					            	//	$scope.previopusWorkExpArray.splice($scope.previopusWorkExpArray.length - 1,1);
					            		$scope.deletedPreviopusWorkExpArray = [];
				            	 }
				            	 
				            	 $scope.isDriverPrviousExpUpdated = true;
				            	 $scope.previopusWorkExpIdsArray= returnJson.experienceIds;
				            		console.log("previopusWorkExpIdsArray json " + angular.toJson($scope.previopusWorkExpIdsArray));
				            	 $scope.canUpdateDriverPrviousExp = false;
				            	if(!isUpdateRequired){
				            		$scope.getShiftAndTruckList();
				            	}
				            	$scope.view = '/resources/views/bodytemplates/driver_workDetails_body.html';
				            	 $("html, body").animate({ scrollTop: 0 }, 50);
				            	// $location.path('/savedriverworkdetails');
				            } else {
				            	$scope.isValidated = false;
				                $scope.serviceMessage = returnJson.serviceMessage;
				                //alert(returnJson.serviceMessage);
				            }
				
				        }).error(function(returnErrorJson, status, headers, config) {
				        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.saveDriverWorkExperince(driver); 
							}
				          
				
				        }); //error
        
				}//is Valid Close
				
		

    };

    /**
     * Driver Present Work Details
     */

    $scope.saveDriverPresentExperience = function saveDriverPresentExperience(driver,isUpdateRequired) {
    	
    	var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register driver";
			return false;
		}
		
    	var isValid = $scope.tracForm.$valid;
    	var driverPresentWorkExp = {};
        console.log("driver json " + JSON.stringify(driver));

        var serviceUrl;
    	
    	if(!isUpdateRequired){
    		serviceUrl = SERVICEADDRESS + '/driver/savePresentWorkExperienceOfDriver';
    	}else{
    		serviceUrl = SERVICEADDRESS + '/driver/editPresentWorkExperienceOfDriver';
    	}
    	
    	
        driverPresentWorkExp.driverId = $scope.driverId;
        driverPresentWorkExp.userId = $scope.userId;
        driverPresentWorkExp.userName = driver.userName;
        driverPresentWorkExp.driverName = driver.driverName;
        driverPresentWorkExp.employeeId = driver.employeeId;
        driverPresentWorkExp.dateOfJoining = driver.presentDateOfJoining;
        driverPresentWorkExp.presentdesignation = driver.presentdesignation;
        driverPresentWorkExp.preferedWorkTimings = driver.preferedWorkTimings;
        driverPresentWorkExp.preferedDayOff = driver.preferedDayOff;
        driverPresentWorkExp.reportingTo = driver.reportingTo;
        driverPresentWorkExp.shirtSize = driver.shirtSize;
        driverPresentWorkExp.trouserSize = driver.trouserSize;
        driverPresentWorkExp.shoeSize = driver.shoeSize;
        driverPresentWorkExp.noOfShirtsReceived = driver.noOfShirtsReceived;
        driverPresentWorkExp.noOfTrousersReceived = driver.noOfTrousersReceived;
        driverPresentWorkExp.noOfShoeReceived = driver.noOfShoeReceived;
        driverPresentWorkExp.preferedTruckId = driver.preferedTruckId;
        
        console.log("driverPresentWorkExp json " + JSON.stringify(driverPresentWorkExp));
        if(isValid){
    	    
    	    var formattedDateOfJoining = $filter('date')(driver.presentDateOfJoining,'yyyy/MM/dd');
			console.log("formattedDateOfJoining - "+formattedDateOfJoining);
			driverPresentWorkExp.formattedDateOfJoining = formattedDateOfJoining;
    	    
        $http.post(serviceUrl, JSON.stringify(driverPresentWorkExp)).
        success(function(returnJson) {

            if (returnJson.isSuccess) {
            	//$scope.canShowPersonalDetails = false;
            	$scope.isValidated = true;
            	//window.location.href = HOSTADDRESS+"/driverlistofdrivers.html";
            	 $location.path('/listOfDrivers');
            	 
            } else {
            	$scope.isValidated = false;
                $scope.serviceMessage = returnJson.serviceMessage;
            }

        }).error(function(returnErrorJson, status, headers, config) {
        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveDriverPresentExperience(driver); 
			}
            return $scope.reject(returnJson);

        }); //error
        }

    };

    
    /**
     *List of drivers
     */

    $scope.getListOfDrivers = function getListOfDrivers() {
       
    	
    	$http({
	        method: 'GET',
	        url: SERVICEADDRESS + '/driver/fetchDriversInfo'
	    }).success(function(returnJson){
	    	console.log("returnJson - "+ JSON.stringify(returnJson));
	    	  if (returnJson.isSuccess) {
	                $scope.listOfDrivers = returnJson.drivers;
	               
	            }
       
	    	  setTimeout(function(){ 
	  			App.init(); // Init layout and core plugins
	  			Plugins.init(); // Init all plugins
	  			FormComponents.init(); // Init all form-specific plugins
	  		  },1000);
	    	  
	    	  $("html, body").animate({ scrollTop: 0 }, 50);
		}).error(function(returnErrorJson, status, headers, config) {
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.getListOfDrivers(); 
			}
		});//end of http post call
            

    };
    
    /* * * * * * * * * * * *
	 * get list of roles
	 * 
	 * * * * * * * * * * * */
    $scope.getShiftAndTruckList = function() {
    	console.log(" driverId - "+$scope.driverId);
    	
    	
			$http({
		        method: 'GET',
		        url: SERVICEADDRESS + '/driver/gettrucksandshiftsmanagers'
		    }).success(function(returnJson){
		    	$scope.driver.driverId = $scope.driverId;
		    	$('#driverId').val($scope.driverId); 
			    $scope.shifts = returnJson.shiftList;
			    $scope.trucks = returnJson.truckList;
			    $scope.managers = returnJson.managers;
			    
			   
			}).error(function(returnErrorJson, status, headers, config) {
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.getInitList(); 
				}
			});//end of http post call
		
	};
    
	 /* * * * * * * * * * * *
	 * get list of roles
	 * 
	 * * * * * * * * * * * */
    $scope.isFormStateValid = function isFormStateValid() {
    	var driverId = $scope.driverId;
    	console.log("isUndefined - "+angular.isUndefined(driverId));
    	if(angular.isUndefined(driverId) || driverId == null){
    		
    		$scope.serviceMessage = "Please fill the personal details of driver";
    		return true;
    	}else{
    		return false;
    	}
    };
    
  
  
   /* * * * * * * * * * * *
	 * delete driver
	 * 
	 * * * * * * * * * * * */
   $scope.deleteDriver = function deleteDriver(driverID,currentRow) {
	  console.log("driverID - "+driverID);
	   console.log("currentRow - "+currentRow);
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Delete');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Delete a Driver";
			return false;
		}
	   
	   var driverJson ={};
	   driverJson.driverId = driverID;
	      
	   
	   $http.post(SERVICEADDRESS + '/driver/deleteDriverInfo', JSON.stringify(driverJson)).
        success(function(returnJson) {

            if (returnJson.isSuccess) {
            	   $('#driverRow'+currentRow).remove();
            } 

        }).error(function(returnErrorJson, status, headers, config) {
        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveDriverPresentExperience(driver); 
			}
            return $scope.reject(returnJson);

        }); //error
     
   };
   
   /* * * * * * * * * * * *
	 * edit driver details
	 * 
	 * * * * * * * * * * * */
   $scope.editDriverDetails = function editDriverDetails(driverID) {
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
	   var driverJson ={};
	   $scope.driverId=driverID;
	   driverJson.driverId = driverID;
	   if(!angular.isUndefinedOrNull($scope.previopusWorkExpIdsArray)){
		   $scope.previopusWorkExpIdsArray.splice(0);   
		   $scope.previopusWorkExpArray.splice(0);
	   }
	   
	   
	   $http.post(SERVICEADDRESS + '/driver/getFullDriverDetails', JSON.stringify(driverJson)).
        success(function(returnJson) {

            if (returnJson.isSuccess) {
            	$scope.driver=returnJson.driverInfo[0];
            	var poBox = returnJson.driverInfo[0].postBox;
            	var ePoBox = returnJson.driverInfo[0].ePostBox;
            	
            	$scope.driver.postBox = poBox.toString();
            	$scope.driver.ePostBox = ePoBox.toString();
           
            	$scope.isoCode = returnJson.driverInfo[0].countryISO;
            	$scope.eIsoCode = returnJson.driverInfo[0].eCountryISO;
            	$scope.currentRowId = returnJson.driverInfo.length;
            	$scope.previousRowId = returnJson.driverInfo.length;
            	
            	if($scope.currentRowId == 0)
          		  $scope.previopusWorkExpArray.push({"experienceId":'',"isNew":false,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
            	
            	
            	for(var i =0; i < returnJson.driverExpList.length; i++){
            		 var driverExperienceObject ={};
            		driverExperienceObject.experienceId = returnJson.driverExpList[i].exprerienceID;
            		driverExperienceObject.totalWorkExperience = returnJson.driverExpList[i].totalExperience;
            		driverExperienceObject.relatedWorkExperience = returnJson.driverExpList[i].relatedExpirence;
            		driverExperienceObject.companyName = returnJson.driverExpList[i].companyName;
            		driverExperienceObject.previousdesignation = returnJson.driverExpList[i].designation;
            		driverExperienceObject.dateOfJoining = returnJson.driverExpList[i].doj;
            		driverExperienceObject.dateOfRelieving = returnJson.driverExpList[i].dor;
            		driverExperienceObject.isNew = false;
            		driverExperienceObject.isDeleted = false;
            		driverExperienceObject.userId = $scope.userId;
            		driverExperienceObject.driverId =$scope.driverId;
            		$scope.previopusWorkExpIdsArray.push(returnJson.driverExpList[i].exprerienceID);
            		$scope.previopusWorkExpArray.push(driverExperienceObject);
            		
            	}
            	
            	
            	$scope.driverExperienceList=returnJson.driverInfo[0];
            	
            	var shoeSize = parseInt($scope.driver.shoeSize);
            	var trouserSize = parseInt($scope.driver.trouserSize);
            	var noOfShoeReceived = parseInt($scope.driver.noOfShoeReceived);
            	var noOfTrousersReceived = parseInt($scope.driver.noOfTrousersReceived);
            	var noOfShirtsReceived = parseInt($scope.driver.noOfShirtsReceived);
            
            	if(shoeSize < 10)
            		$scope.driver.shoeSize = "0"+shoeSize.toString();
            	else
            		$scope.driver.shoeSize = shoeSize.toString();
            	
            	if(trouserSize < 10)
            		$scope.driver.trouserSize = "0"+trouserSize.toString();
            	else
            		$scope.driver.trouserSize = trouserSize.toString();
            	
            	if(noOfShoeReceived < 10)
            		$scope.driver.noOfShoeReceived = "0"+noOfShoeReceived.toString();
            	else
            		$scope.driver.noOfShoeReceived = noOfShoeReceived.toString();
            	
            	if(noOfTrousersReceived < 10 )
            		$scope.driver.noOfTrousersReceived = "0"+noOfTrousersReceived.toString();
            	else
            		$scope.driver.noOfTrousersReceived = noOfTrousersReceived.toString();
            	
            	if(noOfShirtsReceived < 10)
            		$scope.driver.noOfShirtsReceived = "0"+noOfShirtsReceived.toString();
            	else
            		$scope.driver.noOfShirtsReceived = noOfShirtsReceived.toString();
            	
          	
      			console.log("$scope.driver"+ JSON.stringify($scope.driver));
      			
      			
            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_personalDetails_update_body.html';
            	
            	//$location.path('/updatedriverpersonaldetails');
            	   
            } 

        }).error(function(returnErrorJson, status, headers, config) {
        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveDriverPresentExperience(driver); 
			}
            

        }); //error
   };
   
   $scope.updateDriverPersonalDetails = function updateDriverPersonalDetails(driver) {
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
   	
	   var isValid = $scope.tracForm.$valid;
	   var driverObject = {};
	   
   		$scope.errorMessage;
   		$scope.isPassportIssuedAtValid = false;
		$scope.isPassportExpiredAtValid = false;
		$scope.isVisaIssuedAtValid = false;
		$scope.isVisaExpiredAtValid = false;
		$scope.isDateofJoiningAtValid = false;
		$scope.isDateofRelievingAtValid = false;
		
   	
   		if(!angular.isUndefinedOrNull(driver)){
			$scope.driver.driverName = driver.driverFirstName + " " + driver.driverLastName;
			driverObject.driverFirstName = driver.driverFirstName;
			driverObject.driverLastName = driver.driverLastName;
			driverObject.licenseNo = driver.licenseNo;
			driverObject.passportNo = driver.passportNo;
			driverObject.visaNo = driver.visaNo;
			driverObject.healthInsuranceCardNo = driver.healthInsuranceCardNo;
			driverObject.licenseNoExpiredAt = driver.licenseNoExpiredAt;
			driverObject.passportIssuedAt = driver.passportIssuedAt;
			driverObject.visaIssuedAt = driver.visaIssuedAt;
			driverObject.passportExpiredAt = driver.passportExpiredAt;
			driverObject.visaExpiredAt = driver.visaExpiredAt;
			driverObject.healthInsuranceCardExpiryAt = driver.healthInsuranceCardExpiryAt;
			driverObject.governmentIdNumber = driver.governmentIdNumber;
			driverObject.nationality = driver.nationality;
			driverObject.userId = $scope.userId;
			driverObject.driverId = $scope.driverId;
	   		driverObject.createdBy = $scope.userId;
	   	    
	   		if(driverObject.visaIssuedAt<driverObject.passportIssuedAt)
			{
				$scope.isVisaIssuedAtValid =true;
				$scope.errorMessage = "Visa Issued Date cannot be before Passport Issued Date.";
				return false;
			}else if(driverObject.passportExpiredAt<driverObject.passportIssuedAt)
			{
				$scope.isPassportExpiredAtValid =true;
				$scope.errorMessage = "Passport Expired Date cannot be before Passport Issued Date.";
			}
			
			else if(driverObject.visaExpiredAt<driverObject.visaIssuedAt)
			{
				$scope.isVisaExpiredAtValid = true;
				$scope.errorMessage = "Visa Expired date cannot be before Visa Issued Date.";
			}
			else{//All Validations Done
				$scope.errorMessage = ""; 
				$scope.isPassportIssuedAtValid = false;
				$scope.isPassportExpiredAtValid = false;
				$scope.isVisaIssuedAtValid = false;
				$scope.isVisaExpiredAtValid = false;

	      
	      
	        console.log("driver json " + JSON.stringify(driverObject));
	
	        if(isValid){
	        $http.post(SERVICEADDRESS + '/driver/editPersonalDetailsOfDriver', JSON.stringify(driverObject)).
	        success(function(returnJson) {
	
		            if (returnJson.isSuccess) { 
		            	$scope.isValidated = true;
		            	$scope.canUpdatePersonalDetails = false;
		            	$scope.getCountriesFroDriver();
		            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_contactDetails_update_body.html';
		            	 $("html, body").animate({ scrollTop: 0 }, 50);
		            	//$location.path('/updatedrivercontactdetails');
		            } else {
		            	$scope.isValidated = false;
		                $scope.serviceMessage = returnJson.serviceMessage;
		            }
	
		        }).error(function(returnErrorJson, status, headers, config) {
		
		        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.saveDriverPersonalDetails(driver); 
				     }//end of if
		           
		
		        }); //error
	        
	         }//if end
	        
			}//end of validation if
		
		}//end of isUndefinedOrNull
   };
   
   $scope.updateDriverContactDetails = function updateDriverContactDetails(driver) {
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
	   var isValid = $scope.tracForm.$valid;
	  /* var enteredCountryName = $('#country').val();
	   if(!angular.isUndefinedOrNull(enteredCountryName)){
		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
		   if(indexLoc  >= 0){
			   $scope.isCountrySelected = false;
		   }else{
				$scope.isCountrySelected = true;
				return false;
		   }
		}
   	*/
      
       
       var contactDetailsinput = {
           userId: $scope.userId,
           driverId: $scope.driverId,
           phone: driver.phone,
           area: driver.area,
           building: driver.building,
           email: driver.email,
           street: driver.street,
           landmark: driver.landmark,
           postBox: driver.postBox,
           country: $scope.isoCode,
           city: driver.city
       };
       
       
       console.log("driver json " + JSON.stringify(contactDetailsinput));
        if(isValid){
		   $http.post(SERVICEADDRESS + '/driver/editContactDetailsOfDriver', JSON.stringify(contactDetailsinput)).
	        success(function(returnJson) {
	
	            if (returnJson.isSuccess) {
	            	$scope.isValidated = true;
	            	//$scope.driver=returnJson.driverObject;
	            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_emergencyConDetails_update_body.html';
	            	 $("html, body").animate({ scrollTop: 0 }, 50);
	            	//$location.path('/updatedriveremergencycontactdetails');
	            } else{
	            	$scope.isValidated = false;
	            	$scope.serviceMessage = returnJson.serviceMessage;
	            }
	
	        }).error(function(returnErrorJson, status, headers, config) {
	        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.saveDriverPresentExperience(driver); 
				}
	            return $scope.reject(returnJson);
	
	        }); //error
       }//end of if loop
   };
   
   $scope.updateDriverEmergencyContactDetails = function updateDriverEmergencyContactDetails(driver) {
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
		
	   var isValid = $scope.tracForm.$valid;
   	
	   /*var enteredCountryName = $('#eCountry').val();
	   if(!angular.isUndefinedOrNull(enteredCountryName)){
		   var indexLoc= $scope.getIndexOf($scope.countryJson,enteredCountryName,"Name");
		   if(indexLoc  >= 0){
			   $scope.isECountrySelected = false;
		   }else{
				$scope.isECountrySelected = true;
				return false;
		   }
		}*/
     //  console.log("driver json " + JSON.stringify(driver));

       var emergencyContactDetailsInput = {
           userId: $scope.userId,
           driverId: $scope.driverId,
           eContactName: driver.eContactName,
           phone: driver.ePhone,
           area: driver.eArea,
           building: driver.eBuilding,
           email: driver.eEmail,
           street: driver.eStreet,
           landmark: driver.eLandmark,
           postBox: driver.ePostBox,
           country: $scope.eIsoCode,
           city: driver.eCity
       };
       
       console.log("driver json " + JSON.stringify(emergencyContactDetailsInput));
       if(isValid){
		   $http.post(SERVICEADDRESS + '/driver/saveOrEditEmergencyContactDetailsOfDriver', JSON.stringify(emergencyContactDetailsInput)).
	        success(function(returnJson) {
	
	            if (returnJson.isSuccess) {
	            	$scope.isValidated = true;
	            	console.log("previopusWorkExpArray - "+$scope.previopusWorkExpArray);
	            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_workExperience_update_body.html';
	            	 $("html, body").animate({ scrollTop: 0 }, 50);
	            	//$location.path('/updatedriverworkexperience');
	            	
	            } else{
	            	$scope.isValidated = false;
	            	$scope.serviceMessage = returnJson.serviceMessage;
	            }
	
	        }).error(function(returnErrorJson, status, headers, config) {
	        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.saveDriverPresentExperience(driver); 
				}
	           
	        }); //error
       }
   };
   
   
   $scope.updateDriverPreviousExpDetails = function updateDriverPreviousExpDetails(driver) {
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
	   console.log(" $scope.previopusWorkExpArray =- "+ $scope.previopusWorkExpArray);
	 var isValid = $scope.tracForm.$valid;
	 var isMaxLengthValid = false;
  	 var isDateValid = false;
  	 var noOfDeletedIds = $scope.deletedPreviopusWorkExpArray.length;
  	var noOfIds = $scope.previopusWorkExpArray.length;
  	var previousExpObject =[];
  	 
  	 
      for(var i = 0,j=0;i < noOfIds; i++){
    	  	
    	    var previopusWorkExpObject = {};
	       	var dateOfJoining = $scope.previopusWorkExpArray[i].dateOfJoining;
	      	var dateOfRelieving = $scope.previopusWorkExpArray[i].dateOfRelieving;
        	var totalWorkExperience = $scope.previopusWorkExpArray[i].totalWorkExperience;
         	var relatedWorkExperience = $scope.previopusWorkExpArray[i].relatedWorkExperience;
         	var companyName = $scope.previopusWorkExpArray[i].companyName;
         	var previousdesignation = $scope.previopusWorkExpArray[i].previousdesignation;
         	
         	previopusWorkExpObject.totalWorkExperience = totalWorkExperience; 
	  		previopusWorkExpObject.relatedWorkExperience = relatedWorkExperience;
	  		previopusWorkExpObject.companyName = companyName;
	  		previopusWorkExpObject.previousdesignation = previousdesignation;
	  		previopusWorkExpObject.dateOfJoining = dateOfJoining;
	  		previopusWorkExpObject.dateOfRelieving = dateOfRelieving;
	  		previopusWorkExpObject.isDeleted = false;
	  		previopusWorkExpObject.userId = $scope.userId;
	  	    previopusWorkExpObject.driverId = $scope.driverId;
	  				
           	
           	if(totalWorkExperience.length > 25)
           		isMaxLengthValid = true;
           		
           	if(relatedWorkExperience.length > 25)
           		isMaxLengthValid = true;
           			
           	if(companyName.length > 25)
           		isMaxLengthValid = true;
           				
           	if(previousdesignation.length > 25)
           		isMaxLengthValid = true;
           	
         	if(dateOfRelieving < dateOfJoining)
			{
				$("#dateOfRelievingValidation"+i).show();
				j++;
			}
			
			
			if(j == 0){
				isDateValid = true;
			}else{
				isDateValid = false;
			}
			
			previopusWorkExpObject.experienceId = $scope.previopusWorkExpIdsArray[i];
			console.log("experienceId - "+ previopusWorkExpObject.experienceId);
			if(!angular.isUndefinedOrNull(previopusWorkExpObject.experienceId)){
				$scope.previopusWorkExpArray[i].isNew = false;
				previopusWorkExpObject.isNew = false;
			}else{
				previopusWorkExpObject.isNew = true;
			}
			
			previousExpObject.push(previopusWorkExpObject);
		}
	
      console.log("isMaxLengthValid - "+isMaxLengthValid );
      if(isValid && isDateValid && !isMaxLengthValid){
    	     for(var i = 0;i < noOfDeletedIds; i++){
    	  		 var deletedObjects =  $scope.deletedPreviopusWorkExpArray[i];
    	  		var previopusWorkExpObject = {};
    	  		previopusWorkExpObject.experienceId = deletedObjects.experienceId;
    	  		previopusWorkExpObject.isNew = false;
    	  		previopusWorkExpObject.isDeleted = true;
    	  		previopusWorkExpObject.totalWorkExperience = ''; 
    	  		previopusWorkExpObject.relatedWorkExperience = '';
    	  		previopusWorkExpObject.companyName = '';
    	  		previopusWorkExpObject.previousdesignation = '';
    	  		previopusWorkExpObject.dateOfJoining = '';
    	  		previopusWorkExpObject.dateOfRelieving = '';
    	  		previousExpObject.push(previopusWorkExpObject);
    	  	 }
    	  
			//driverPreviousWorkExp.tracFormList =  $scope.previopusWorkExpArray;
			console.log("driverPreviousWorkExp json " + angular.toJson(previousExpObject));
	   
			   $http.post(SERVICEADDRESS + '/driver/editDriverWorkExperience', angular.toJson(previousExpObject)).
		        success(function(returnJson) {
		
		            if (returnJson.isSuccess) {
		            	$scope.isValidated = true;
		            	 $scope.previopusWorkExpIdsArray= returnJson.experienceIds;
		            	console.log("previopusWorkExpIdsArray json " + angular.toJson($scope.previopusWorkExpIdsArray));
		            	
		            	 if(noOfDeletedIds > 0){
			            		$scope.deletedPreviopusWorkExpArray = [];
		            	 }
		            	
		            	$scope.getShiftAndTruckList();
		            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_workDetails_update_body.html';
		            	 $("html, body").animate({ scrollTop: 0 }, 50);
		            	//$location.path('/updatedriverworkdetails');
		            	
		            	   
		            } else{
		            	$scope.isValidated = false;
		            	$scope.serviceMessage = returnJson.serviceMessage;
		            }
		
		        }).error(function(returnErrorJson, status, headers, config) {
		        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.saveDriverPresentExperience(driver); 
					}
		            
		        }); //error
      }
   };
   
  
   
   $scope.updateDriverPresentExpDetails = function updateDriverPresentExpDetails(driver) {
	   
	   var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'driver','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update Driver";
			return false;
		}
		
		
	 var isValid = $scope.tracForm.$valid;
  	 var driverPresentWorkExp = {};
     console.log("driver json " + JSON.stringify(driver));

      driverPresentWorkExp.driverId = $scope.driverId;
      driverPresentWorkExp.employeeId = driver.employeeId;
      driverPresentWorkExp.userId = $scope.userId;
      driverPresentWorkExp.userName = driver.userName;
      driverPresentWorkExp.driverName = driver.driverName;
      driverPresentWorkExp.dateOfJoining = driver.presentDateOfJoining;
      driverPresentWorkExp.presentdesignation = driver.presentdesignation;
      driverPresentWorkExp.preferedWorkTimings = driver.preferedWorkTimings;
      driverPresentWorkExp.preferedDayOff = driver.preferedDayOff;
      driverPresentWorkExp.reportingTo = driver.reportingTo;
      driverPresentWorkExp.shirtSize = driver.shirtSize;
      driverPresentWorkExp.trouserSize = driver.trouserSize;
      driverPresentWorkExp.shoeSize = driver.shoeSize;
      driverPresentWorkExp.noOfShirtsReceived = driver.noOfShirtsReceived;
      driverPresentWorkExp.noOfTrousersReceived = driver.noOfTrousersReceived;
      driverPresentWorkExp.noOfShoeReceived = driver.noOfShoeReceived;
      driverPresentWorkExp.preferedTruckId = driver.preferedTruckId;
      
      console.log("driverPresentWorkExp json " + JSON.stringify(driverPresentWorkExp));
      if(isValid){
    	  
    	/*  var presentDateOfJoining =  driver.presentDateOfJoining;
  		var day = presentDateOfJoining.getDate();
  	    var monthIndex = presentDateOfJoining.getMonth();
  	    var year = presentDateOfJoining.getFullYear();
  	    var formattedDateOfJoining = year+"/"+monthIndex+"/"+day;*/
  	  var formattedDateOfJoining = $filter('date')(driver.presentDateOfJoining,'yyyy/MM/dd');
  	    /*console.log("day - "+day);
  	    console.log("monthIndex - "+monthIndex);
  	    console.log("year - "+year);*/
  	  
  	console.log("console.log("+formattedDateOfJoining);
  	    driverPresentWorkExp.formattedDateOfJoining = formattedDateOfJoining;
		   $http.post(SERVICEADDRESS + '/driver/editPresentWorkExperienceOfDriver', JSON.stringify(driverPresentWorkExp)).
	        success(function(returnJson) {
	
	            if (returnJson.isSuccess) {
	            	$scope.isValidated = true;
	            	$scope.listOfDriversView = '/resources/views/bodytemplates/driver_listofDrivers_body.html';
	            	
	            } else{
	            	$scope.isValidated = false;
	            	$scope.serviceMessage = returnJson.serviceMessage;
	            }
	
	        }).error(function(returnErrorJson, status, headers, config) {
	        	var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.saveDriverPresentExperience(driver); 
				}
	          
	
	        }); //error
      }
   };
   
   
   $scope.previousSession = function(viewToBeDisplayed) {
		$scope.view = viewToBeDisplayed;
	};
	
	
	$scope.previousSessionForUpdateScreen = function(viewToBeDisplayed) {
	 
		$scope.listOfDriversView = viewToBeDisplayed;
		 $("html, body").animate({ scrollTop: 0 }, "slow");
	};
   
	   
   /* * * * * * * * * * * *
	 * get list of countries
	 * 
	 * * * * * * * * * * * */
    $scope.getCountriesFroDriver = function getCountriesFroDriver() {
    	$scope.countryNameList = [];
	    $scope.countryIsoList = [];
    	$http.get(SERVICEADDRESS+'/countries/fetch').success(function(returnJson){
    		var countryJson = returnJson.tableResultList;
    		$scope.countryJson = returnJson.tableResultList;
		    for(var i=0;i<countryJson.length;i++){
		    	$scope.countryNameList.push(countryJson[i].Name);	
				$scope.countryIsoList.push(countryJson[i].ISOCode);
			}
		    console.log("countryList "+ $scope.countryNameList);
		    console.log("countryList "+ $scope.countryIsoList);
		}).error(function(returnErrorJson, status, headers, config) {
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.getCountriesFroDriver(); 
				
			}
		});//end of http post call
    };
    
	   
    /* * * * * * * * * * * *
 	 * fucntion to serach json and returm value
 	 * 
 	 * * * * * * * * * * * */
    $scope.getValueFromJson =  function (jsonToSearch, comparableValue, prop,valueToGet) {
		var l = jsonToSearch.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = jsonToSearch[k][prop];
			var searcValue = comparableValue;
			if (indexValue == searcValue) {
				return jsonToSearch[k][valueToGet];
			}
		}
		return false;
	};
	
	 //json serach to find out location where the value resides
	$scope.getIndexOf =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop].trim().toString();
			var searcValue = coparableValue.trim().toString();
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	
	 /* * * * * * * * * * * *
 	 * logout of user
 	 * 
 	 * * * * * * * * * * * */
    $scope.logoutUser = function logoutUser() {
    	$http.defaults.headers.common['Authorization'] = "bearer "+ accessToken + " " +  userId;
		//service call
		$http.post(SERVICEADDRESS+'/logout').
		  success(function(returnJson) {
			    	
			    	if(returnJson.isLoggedOut){
			    		localStorage.clearStorage();
			    		window.location.href = HOSTADDRESS+"/login.html";
			    	
			    	}else{
			    		
			    	}
			    	
		  }).error(function(returnErrorJson, status, headers, config) {
			  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.logoutUser(); 
				}
		 });//error
		  
	  };//logoutUser function end
	
	  
	  $scope.redirectToUrl = function(urlToBeRedirected) {
		  window.location.href = HOSTADDRESS+urlToBeRedirected;
	  };
	

	$scope.today = function() {
		$scope.dt = new Date();
	
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	
	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
		 
	};
	
	
	$scope.toggleMin();

	$scope.open = function($event) {
		
		$event.preventDefault();
		$event.stopPropagation();
		console.log(" $scope.opened", $scope.opened);
		$scope.opened = true;
	};
	
	$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
	};

	
	$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy', 'yyyy-MM-dd'];
	$scope.format = $scope.formats[5];
	$scope.yearformat = $scope.formats[4];
	
	
	$scope.deleteCurrentExpRow = function(currentRowId) {
		  $scope.deletedRowIds.push(currentRowId);
		 $('#dynamicRow'+currentRowId).remove();
	};
	
	$scope.addExprow  = function(currentRowId) {
		  $scope.previopusWorkExpArray.push({"experienceId":0,"isNew":true,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
		  $scope.currentRowId = $scope.currentRowId + 1;
	};
	
	$scope.addNewExpRow  = function(currentRowId) {
		if($scope.canUpdateDriverPrviousExp)
		  $scope.previopusWorkExpArray.push({"experienceId":0,"isNew":false,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
		else
			$scope.previopusWorkExpArray.push({"experienceId":0,"isNew":true,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
		
		
		  $scope.currentRowId = $scope.currentRowId + 1;
	};
	
	$scope.removeNewDriverPreviousExp  = function(currentRowId,isUpdated) {
		var jsonArrayLength =  $scope.previopusWorkExpArray.length;
		if(!isUpdated){
		  $scope.previopusWorkExpArray.splice(currentRowId,1);
		}else{
		    $scope.deletedPreviopusWorkExpArray.push({"experienceId":$scope.previopusWorkExpIdsArray[currentRowId],"isDeleted":true});
		    $scope.previopusWorkExpArray.splice(currentRowId,1);
		    $scope.previopusWorkExpIdsArray.splice(currentRowId,1);
		}
	 if(jsonArrayLength == 1){
	    	$scope.previopusWorkExpArray.push({"experienceId":0,"isNew":true,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
	 }
	};

	$scope.removeDriverPreviousExp = function(index){
		var jsonArrayLength =  $scope.previopusWorkExpArray.length;
	    var prevExpObject = $scope.previopusWorkExpArray[index];
	    $scope.deletedPreviopusWorkExpArray.push({"experienceId":prevExpObject.experienceId,"isDeleted":true});
	    $scope.previopusWorkExpArray.splice(index,1);
	    $scope.previopusWorkExpIdsArray.splice(index,1);
	    if(jsonArrayLength == 1){
	    	$scope.previopusWorkExpArray.push({"experienceId":0,"isNew":true,"isDeleted":false,"totalWorkExperience":'', "relatedWorkExperience":'', "companyName":'', "previousdesignation":'', "dateOfJoining":'', "dateOfRelieving":'',"userId": $scope.userId,"driverId": $scope.driverId});
	    }
	};
	
	$scope.initDriver= function(){
		
		$scope.errorMessage='';
    	$scope.isPassportIssuedAtValid = false;
		$scope.isPassportExpiredAtValid = false;
		$scope.isVisaIssuedAtValid = false;
		$scope.isVisaExpiredAtValid = false;
		$scope.isDateofJoiningAtValid = false;
		$scope.isDateofRelievingAtValid = false;
		 if(angular.isUndefinedOrNull($scope.driver))
			 	$scope.driver= {};
		 $("html, body").animate({ scrollTop: 0 }, 50);
	};
	

	//json serach to access permissions
		$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
			var l = arr.length,
			k = 0;
			for (k = 0; k < l; k = k + 1) {
				var indexValue = arr[k][prop1].trim().toLowerCase();
				var searcValue = comparableValue.trim().toLowerCase();
				if (indexValue.indexOf(searcValue) >= 0) {
					//var canAccess = arr[k][prop2];
					return arr[k][prop2];
				}
			}
			return -1;
		};//end
	
}); //DriverController End