package trac.beans.userbean;

import java.io.Serializable;

/** Stores the UserLocation related information
 * 
 */
public class UserLocation implements Serializable{
	
	
	private static final long serialVersionUID = 8064693276386644124L;
	
	private int userId;
	private String email;
	private String userLocationLatitude;
	private String userLocationLongitude;
	private String userLocationName;
	private Object userLocationList;
	private String userLocationCity;
	private String userLocationCountryCode;
	private String userLocationState;
	private boolean isSuccess;
	private String serviceMessage;
	private int serviceCode;
	private String detailAddress;
	private String locationId;
	

	
	private String area;
	private String street;
	private String building;
	private String landmark;
	private String poBox;
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the userLocationLatitude
	 */
	public synchronized String getUserLocationLatitude() {
		return userLocationLatitude;
	}
	/**
	 * @param userLocationLatitude the userLocationLatitude to set
	 */
	public synchronized void setUserLocationLatitude(String userLocationLatitude) {
		this.userLocationLatitude = userLocationLatitude;
	}
	/**
	 * @return the userLocationLongitude
	 */
	public synchronized String getUserLocationLongitude() {
		return userLocationLongitude;
	}
	/**
	 * @param userLocationLongitude the userLocationLongitude to set
	 */
	public synchronized void setUserLocationLongitude(String userLocationLongitude) {
		this.userLocationLongitude = userLocationLongitude;
	}
	/**
	 * @return the isSuccess
	 */
	public synchronized boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public synchronized void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public synchronized String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public synchronized void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the serviceCode
	 */
	public synchronized int getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public synchronized void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	/**
	 * @return the userLocationName
	 */
	public String getUserLocationName() {
		return userLocationName;
	}
	/**
	 * @param userLocationName the userLocationName to set
	 */
	public void setUserLocationName(String userLocationName) {
		this.userLocationName = userLocationName;
	}
	public Object getUserLocationList() {
		return userLocationList;
	}
	public void setUserLocationList(Object userLocationList) {
		this.userLocationList = userLocationList;
	}
	/**
	 * @return the userLocationCity
	 */
	public String getUserLocationCity() {
		return userLocationCity;
	}
	/**
	 * @param userLocationCity the userLocationCity to set
	 */
	public void setUserLocationCity(String userLocationCity) {
		this.userLocationCity = userLocationCity;
	}
	/**
	 * @return the userLocationCountry
	 */
	public String getUserLocationCountryCode() {
		return userLocationCountryCode;
	}
	/**
	 * @param userLocationCountry the userLocationCountry to set
	 */
	public void setUserLocationCountryCode(String userLocationCountryCode) {
		this.userLocationCountryCode = userLocationCountryCode;
	}
	/**
	 * @return the userLocationState
	 */
	public String getUserLocationState() {
		return userLocationState;
	}
	
	/**
	 * @param userLocationState the userLocationState to set
	 */
	public void setUserLocationState(String userLocationState) {
		this.userLocationState = userLocationState;
	}
	public String getDetailAddress() {
		return detailAddress;
	}
	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the building
	 */
	public String getBuilding() {
		return building;
	}
	/**
	 * @param building the building to set
	 */
	public void setBuilding(String building) {
		this.building = building;
	}
	/**
	 * @return the landmark
	 */
	public String getLandmark() {
		return landmark;
	}
	/**
	 * @param landmark the landmark to set
	 */
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	/**
	 * @return the poBox
	 */
	public String getPoBox() {
		return poBox;
	}
	/**
	 * @param poBox the poBox to set
	 */
	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
	

}
