package trac.beans.jdbcteamplatebean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class JDBCConnection {
	/** Stores the database connection
	 * 
	 */
	@Autowired
	private static JdbcTemplate jdbcTemplate;

	public static JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		System.out.println("jdbcTemplate - "+ jdbcTemplate);
		JDBCConnection.jdbcTemplate = jdbcTemplate;
	}

}
