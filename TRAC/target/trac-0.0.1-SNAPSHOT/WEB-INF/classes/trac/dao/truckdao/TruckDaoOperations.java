package trac.dao.truckdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.truckbean.Truck;
//import trac.beans.userbean.UserLocation;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.CommonUtil;
import trac.util.DBUtil;
import trac.util.utilinterface.CommonUtilInterface;
import trac.util.utilinterface.DBUtilInterface;

/**
 *  Truck related database Operations are handled in this class
 *  
 */
public class TruckDaoOperations {


	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	private CommonUtilInterface commonUtil = new CommonUtil();

	/**
	 * This method is to insert the truck details into database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */


	public List<HashMap<String, Object>> registerTuckInfo(Truck truckDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_INSERT_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean("issuccess")){

						responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
						responseMap.put(ResponseConstantsOfTruck.VIN, rs.getString("vin"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDID, rs.getLong("TruckBrandID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKMODELID, rs.getLong("TruckModelID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKCODE, rs.getString("TruckCode"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_DETAILS_SAVED);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Vin Number already Exists.");
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});



		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iTruckCode","iDateOfPurchase","itruckRegistrationDate","iPlateCode","itruckRegistrationNo","iVin","iAcquiredDate","iTruckBrandID","iModelYear","iColor","iTotalCost","iTruckModelID","iAvailableHours","iTrucknickname","iINSCompanyName","iPolicyIssuedAt","iPolicyExpiredAt","iReRegistrationDueAt",
		"iservicesArray","iPolicyNumber"};
		Object[] inParamaterValues = {truckDetails.getUserId(),truckDetails.getTruckCode(),
				truckDetails.getDateOfPurchase(),truckDetails.getRegistrationDate(),
				truckDetails.getPlateCode(),truckDetails.getPlateNumber(),
				truckDetails.getVin(),truckDetails.getDateOfAcquiring(),truckDetails.getTruckBrandID(),truckDetails.getModelYear(),truckDetails.getTruckColor(),truckDetails.getTotalCost(),truckDetails.getTruckModelID(),truckDetails.getAvailableHoursPerDay(),truckDetails.getTruckNickName(),truckDetails.getInsCompanyName(),truckDetails.getTruckPolicyIssuedDate(),truckDetails.getTruckPolicyExpiryDate(),truckDetails.getTruckReRegistrationDueDate(),
				truckDetails.getServicesProvided(),truckDetails.getTruckPolicyNumber()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


	/**
	 * This method is to update the truck details in database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */

	public List<HashMap<String, Object>> editTruckInfo(Truck truckDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_UPDATE_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					System.out.println("Truck Update IsSuccess = "+rs.getBoolean(1));
					if(rs.getBoolean(1)){
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_DETAILS_UPDATED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);

					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}


					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

				//IN parameters for stored procedure
				String[] inParamaters = {"iuserID","iTruckID","iTruckCode","iDateOfPurchase","itruckRegistrationDate","iPlateCode","itruckRegistrationNo","iVin","iAcquiredDate","iTruckBrandID","iModelYear","iColor","iTotalCost","iTruckModelID","iAvailableHours","iTrucknickname","iINSCompanyName","iPolicyIssuedAt","iPolicyExpiredAt","iReRegistrationDueAt","iservicesArray","iPolicyNumber"};
				Object[] inParamaterValues = {truckDetails.getUserId(),truckDetails.getTruckId(),truckDetails.getTruckCode(),truckDetails.getDateOfPurchase(),truckDetails.getRegistrationDate(),truckDetails.getPlateCode(),truckDetails.getPlateNumber(),truckDetails.getVin(),truckDetails.getDateOfAcquiring(),truckDetails.getTruckBrandID(),truckDetails.getModelYear(),truckDetails.getTruckColor(),truckDetails.getTotalCost(),truckDetails.getTruckModelID(),truckDetails.getAvailableHoursPerDay(),truckDetails.getTruckNickName(),truckDetails.getInsCompanyName(),truckDetails.getTruckPolicyIssuedDate(),truckDetails.getTruckPolicyExpiryDate(),truckDetails.getTruckReRegistrationDueDate(),truckDetails.getServicesProvided(),truckDetails.getTruckPolicyNumber()};

				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				@SuppressWarnings("unchecked")
				List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
				return returnjson;
	}


	/**
	 * This method is to delete the truck details from database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */


	public List<HashMap<String, Object>> unRegisterTruck(int truckId,int userId) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_REMOVE_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					//if(isSuccess){ 
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
					/*}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_UNREGISTRATION_FAILED);
					}*/
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iTruckID","iUserId"};
		Object[] inParamaterValues = {truckId,userId};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
	
	
	/**
	 * This method is to update the truck activation/deactivation 
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */


	public List<HashMap<String, Object>> updateTruckActivation(int truckId,int userId) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_ACTIVATION_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					boolean isSuccess = rs.getBoolean("issuccess");
				
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						boolean isActive = rs.getBoolean("isActive");
						responseMap.put("isActive", isActive);
						if(isActive)
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_ACTIVATION);
						else
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_ACTIVATION);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Truck Activation/Deactivation failed");
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iTruckID","iUserId"};
		Object[] inParamaterValues = {truckId,userId};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}



	/**
	 * This method is to get the truck details from database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true with trucks list or false
	 */


	public List<HashMap<String, Object>> getTruckInfo(Truck truckDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_FETCH_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{

					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					
					responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKCODE, rs.getString("TruckCode"));
					responseMap.put(ResponseConstantsOfTruck.DATEOFPURCHASE, rs.getString("DateOfPurchase"));
					responseMap.put(ResponseConstantsOfTruck.REGISTRATIONDATE, rs.getString("TruckRegistrationDate"));
					responseMap.put(ResponseConstantsOfTruck.PLATECODE, rs.getString("plateCode"));
					responseMap.put(ResponseConstantsOfTruck.PLATENUMBER, rs.getString("TruckRegistrationNo"));
					responseMap.put(ResponseConstantsOfTruck.VIN, rs.getString("vin"));
					responseMap.put(ResponseConstantsOfTruck.DATEOFACQUIRING, rs.getString("acquiredDate"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDID, rs.getInt("TruckBrandID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDNAME, rs.getString("brand"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKMODELID, rs.getInt("TruckModelID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKMODELNAME, rs.getString("model"));
					responseMap.put(ResponseConstantsOfTruck.MODELYEAR, rs.getString("ModelYear"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKCOLOR, rs.getString("color"));
					responseMap.put(ResponseConstantsOfTruck.TOTALCOST, rs.getString("totalCost"));
					responseMap.put(ResponseConstantsOfTruck.REREGISTRATIONDUEAT, rs.getString("ReRegistrationDueAt"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKNICKNAME, rs.getString("trucknickname"));
					responseMap.put(ResponseConstantsOfTruck.AVAILABLEHOURSPERDAY, rs.getString("AvailableHours"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKPOLICYID, rs.getString("TruckPolicyID"));
					responseMap.put(ResponseConstantsOfTruck.INSCOMPANYNAME, rs.getString("insCompanyName"));
					responseMap.put(ResponseConstantsOfTruck.POLICYNUMBER, rs.getString("policyNumber"));
					responseMap.put(ResponseConstantsOfTruck.POLICYISSUEDAT, rs.getString("IssuedAt"));
					responseMap.put(ResponseConstantsOfTruck.POLICYEXPIREDAT, rs.getString("ExpiredAt"));
					responseMap.put(ResponseConstantsOfTruck.REREGISTRATIONDUEAT, rs.getString("ReRegistrationDueAt"));
					responseMap.put(ResponseConstantsOfTruck.SERVICESPROVIDED, rs.getString("servcices"));
					responseMap.put("lastcaseAttendedTime", rs.getString("lastcaseAttendedTime"));
					responseMap.put("isActive", rs.getBoolean("isActive"));

					int policyExpiryCode= commonUtil.dateComparator(rs.getString("ExpiredAt"));
					int reRegExpiryCode= commonUtil.dateComparator(rs.getString("ReRegistrationDueAt"));
					System.out.println(rs.getString("ExpiredAt")+"  "+policyExpiryCode);
					System.out.println(rs.getString("ReRegistrationDueAt")+"  "+reRegExpiryCode);
					responseMap.put(ResponseConstantsOfTruck.COLOR_CODE, commonUtil.getColorCode(policyExpiryCode,reRegExpiryCode));
					System.out.println(ResponseConstantsOfTruck.COLOR_CODE+"  "+commonUtil.getColorCode(policyExpiryCode,reRegExpiryCode));
				
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"ifromValue","itoValue"};
		Object[] inParamaterValues = {truckDetails.getFromValue(),truckDetails.getToValue()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
	
	/**
	 *This method is used to get the all truck brands list of Corresponding Model.
	 *
	 *@param userid
	 *@spname usp_truck_truckBrands_select
	 * @return userid, List of Truck Brands
	 */
	public Object getTruckBrands() {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.GET_TRUCK_BRAND_FETCH_SPNAME)
		.returningResultSet("truckBrandsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
	
					responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDNAME, rs.getString("brandName").trim());
					responseMap.put(ResponseConstantsOfTruck.TRUCKBRANDID, rs.getInt("truckBrandID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKMODELID, rs.getLong("truckModelID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKMODELNAME, rs.getString("modelName").trim());
	
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
			});

		//IN parameters for stored procedure
		Map<String, Object> inParamMap = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource(inParamMap);

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(in);
		
		
		System.out.println("truck brands list - "+simpleJdbcCallResult.get("truckBrandsList"));
		return simpleJdbcCallResult.get("truckBrandsList");
	}

	
	
	
	/**
	 * This method is to get the truck details from database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true with trucks list or false
	 */


	public List<HashMap<String, Object>> getTruckInfo() {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_INFO_FETCH_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{

					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKCODE, rs.getString("TruckCode"));
					
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
		});


		//executing stored procedure
				Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
	
	/**
	 * This method is to insert the truck expense details into database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */

	public List<HashMap<String, Object>> registerTuckExpenseInfo(Truck truckDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_EXPENSE_INFO_INSERT_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean("issuccess")){
						responseMap.put("expenseId", rs.getInt("ID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("truckID"));
						responseMap.put("expenseType", rs.getInt("expenseType"));
						responseMap.put("expenseValue", rs.getString("expenseValue"));
						responseMap.put("paidTo", rs.getString("paidTo"));
						responseMap.put("expenseTime", rs.getString("expenseTime"));
						responseMap.put("paymentType", rs.getInt("paymentType"));
						responseMap.put("receiptNumber", rs.getString("receiptNumber"));
						responseMap.put("comments", rs.getString("comments"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","itruckID","iexpenseType","iexpenseValue","iCurrency","ipaidTo","iexpenseTime","ipaymentType","ireceiptNumber","icomments"};
		Object[] inParamaterValues = {truckDetails.getUserId(),truckDetails.getTruckId(),
				truckDetails.getExpenseType(),truckDetails.getExpenseValue(),truckDetails.getCurrency(),
				truckDetails.getPaidTo(),truckDetails.getExpenseTime(),
				truckDetails.getPaymentType(),truckDetails.getReceiptNumber(),truckDetails.getComments()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


	/**
	 * This method is to update the truck expense details in database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true or false
	 */

	public List<HashMap<String, Object>> editTruckExpenseInfo(Truck truckDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_EXPENSE_INFO_UPDATE_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					System.out.println("Truck Update IsSuccess = "+rs.getBoolean(1));
					if(rs.getBoolean(1)){
						responseMap.put("expenseId", rs.getInt("ID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("truckID"));
						responseMap.put("expenseType", rs.getInt("expenseType"));
						responseMap.put("expenseValue", rs.getString("expenseValue"));
						responseMap.put("paidTo", rs.getString("paidTo"));
						responseMap.put("expenseTime", rs.getString("expenseTime"));
						responseMap.put("paymentType", rs.getInt("paymentType"));
						responseMap.put("receiptNumber", rs.getString("receiptNumber"));
						responseMap.put("comments", rs.getString("comments"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);

					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}


					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

				//IN parameters for stored procedure
				String[] inParamaters = {"iuserID","iExpenseID","itruckID","iexpenseType","iexpenseValue","iCurrency","ipaidTo","iexpenseTime","ipaymentType","ireceiptNumber","icomments"};
				Object[] inParamaterValues = {truckDetails.getUserId(),truckDetails.getExpenseID(),truckDetails.getTruckId(),
				truckDetails.getExpenseType(),truckDetails.getExpenseValue(),truckDetails.getCurrency(),
				truckDetails.getPaidTo(),truckDetails.getExpenseTime(),
				truckDetails.getPaymentType(),truckDetails.getReceiptNumber(),truckDetails.getComments()};
				
				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				@SuppressWarnings("unchecked")
				List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
				return returnjson;
	}
	
	/**
	 * This method is to get the truck expense details from database
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  truckDetails
	 * @return true with trucks list or false
	 */


	public List<HashMap<String, Object>> getTruckexpenseInfo(Truck truckDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.TRUCK_EXPENSE_INFO_FETCH_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{

					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean(1)){
						responseMap.put("expenseId", rs.getInt("ID"));
						responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("truckID"));
						responseMap.put("truckNickName", rs.getString("trucknickname"));
						responseMap.put("expenseType", rs.getInt("expenseType"));
						responseMap.put("expenseValue", rs.getString("expenseValue"));
						responseMap.put("currency", rs.getString("currency"));
						responseMap.put("paidTo", rs.getString("paidTo"));
						responseMap.put("expenseTime", rs.getString("expenseTime"));
						responseMap.put("paymentType", rs.getInt("paymentType"));
						responseMap.put("receiptNumber", rs.getString("receiptNumber"));
						responseMap.put("comments", rs.getString("comments"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);

					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}

					
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});
		

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iSearchBy","iexpenseTimeFrom","iexpenseTimeTo"};
		Object[] inParamaterValues = {truckDetails.getUserId(),truckDetails.getSearchBy(),
		truckDetails.getExpenseTimeFrom(),
		truckDetails.getExpenseTimeTo()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
}
