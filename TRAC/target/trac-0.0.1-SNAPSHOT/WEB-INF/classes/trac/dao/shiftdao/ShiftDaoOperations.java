package trac.dao.shiftdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.shiftbean.Shift;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfShift;
import trac.constants.sqlqueryconstants.ShiftSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.CommonUtil;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.DBUtilInterface;

/**
 *  Shifts related database Operations are handled in this class
 *  
 */

public class ShiftDaoOperations {


	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	public static UtilFunctions utility = new UtilFunctions();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();

	/**
	 * This method is used to insert the shift Details
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  shiftTimings
	 * @return true or false
	 */

	public List<HashMap<String, Object>> insertShiftInfo(int userId,int shiftDayId,ArrayList<LinkedHashMap<String, Object>> shifttimingsList) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.SHIFT_INFO_INSERT_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean("issuccess")){
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DETAILS_SAVED);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfShift.SHIFT_DETAILS_SAVE_FAILED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			} 
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iShiftDayId","iShitDetailsArray","iShiftTimingsCount"};
		Object[] inParamaterValues = {userId,shiftDayId,new CommonUtil().buildShiftTimingsArray(shifttimingsList),shifttimingsList.size()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		
		return returnjson;
	}


	/**
	 * This method is used to edit the shift Details
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  shiftTimings
	 * @return true or false
	 */

	public List<HashMap<String, Object>> updateShiftInfo(Shift shiftTimings) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.SHIFT_INFO_UPDATE_SPNAME).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean("issuccess")){
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DETAILS_UPDATED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);

					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DETAILS_UPDATE_FAILED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
		});

				//IN parameters for stored procedure
				String[] inParamaters = {"iuserID","iShiftID","iShiftName","iStartTime","iEndTime","iFormattedStartTime","iFormattedEndTime","iVehiclesNeeded","iDriversNeeded"};
				Object[] inParamaterValues = {shiftTimings.getUserId(),shiftTimings.getShiftId(),shiftTimings.getShiftName(),shiftTimings.getStartTime(),shiftTimings.getEndTime(),
						shiftTimings.getFormattedStartTime(),shiftTimings.getFormattedEndTime(),shiftTimings.getVehiclesCount(),shiftTimings.getDriversCount()};


				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				@SuppressWarnings("unchecked")
				List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
				return returnjson;
	}
	
	/**
	 * This method is used to edit the shift Details
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  shiftTimings
	 * @return true or false
	 */

	public List<HashMap<String, Object>> updateDriverSchedule(Shift shiftTimings) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SCHEDULE_UPDATE).returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					if(rs.getBoolean("issuccess")){
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DETAILS_UPDATED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DETAILS_UPDATE_FAILED);
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				});;

				//IN parameters for stored procedure
				String[] inParamaters = {"iuserID","iCurrentShiftId","iCurrentDayId","iSelectedShiftId","iDriverId","isDayOff","isDayOffSelected","iShiftDate","isAvailableForShift"};
				Object[] inParamaterValues = {shiftTimings.getUserId(),shiftTimings.getCurrentShiftId(),shiftTimings.getCurrentDayId(),shiftTimings.getSelectedShiftId(),shiftTimings.getDriverId(),shiftTimings.getIsDayOff() == 1 ? true : false,shiftTimings.getIsDayOffSelected() == 1 ? true : false,shiftTimings.getShiftDate(),shiftTimings.getIsAvailableForShift()};


				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				@SuppressWarnings("unchecked")
				List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
				return returnjson;
	}

	/**
	 * This method is used to delete the shift Details
	 * it returns List<HashMap<String, Object>> object with success or failure 
	 * 
	 * @param  shiftId
	 * @return true or false
	 */
	
	public List<HashMap<String, Object>> deleteShiftInfo(int shiftId) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.SHIFT_INFO_DELETE_SPNAME)
		.returningResultSet("returnedResult",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();

					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DELETED);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfShift.SHIFT_DELETION_FAILED);
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iShiftID"};
		Object[] inParamaterValues = {shiftId};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


	/**
	 * This method is used to get the shifts Details
	 * it returns List<HashMap<String, Object>> object with success or failure
	 * 
	 * @return List<Shift> object with shifts
	 */

	public List<Map<String, Object>> getShiftInfoByDay(int shiftDay) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
	
		//Shift shiftDetails = new Shift();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				ShiftSqlQueryConstants.SELECT_SHIFTS_BY_DAY,shiftDay);

		/*List<Shift> shiftsList= new ArrayList<Shift>(); 
		for (Map<String, Object> row : rows) {
			Shift shiftDetails = new Shift();
			shiftDetails.setShiftId((int)row.get("id"));
			shiftDetails.setShiftName((String)row.get("description"));
			shiftsList.add(shiftDetails);
		}*/

		return rows;
	}
	
	/**
	 * This method is used to get the shifts Details
	 * it returns List<HashMap<String, Object>> object with success or failure
	 * 
	 * @return List<Shift> object with shifts
	 */

	public List<Shift> getShiftInfo() {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		
		//Shift shiftDetails = new Shift();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				ShiftSqlQueryConstants.SELECT_SHIFTS);

		List<Shift> shiftsList= new ArrayList<Shift>(); 
		for (Map<String, Object> row : rows) {
			Shift shiftDetails = new Shift();
			shiftDetails.setShiftId((int)row.get("id"));
			shiftDetails.setShiftName((String)row.get("description"));
			shiftsList.add(shiftDetails);
		}

		return shiftsList;
	}
	
	
	/**
	 * This method is used to get the shifts Details
	 * it returns List<HashMap<String, Object>> object with success or failure
	 * 
	 * @return List<Shift> object with shifts
	 */

	public List<Shift> getListOfShifts() {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
	
		//Shift shiftDetails = new Shift();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				ShiftSqlQueryConstants.LIST_OF_SHIFTS);
		

		List<Shift> shiftsList= new ArrayList<Shift>(); 
		for (Map<String, Object> row : rows) {
			Shift shiftDetails = new Shift();
			shiftDetails.setShiftId((int)row.get("ID"));
			shiftDetails.setShiftName((String)row.get("ShiftID"));
			shiftDetails.setShiftDay((String)row.get("shiftDay"));
			shiftDetails.setStartTime((String)row.get("StartTime"));
			shiftDetails.setEndTime((String)row.get("EndTime"));
			shiftDetails.setVehiclesCount((int)row.get("VehiclesNeeded"));
			shiftDetails.setDriversCount((int)row.get("DriversNeeded"));
			shiftsList.add(shiftDetails);
		}

		return shiftsList;
	}



	/**
	 * This method is used to get the shifts schedule of drivers
	 * it returns List<HashMap<String, Object>> object with success or failure
	 * 
	 * @return List<Shift> object with shifts
	 * @throws ParseException 
	 */
	//Map<Integer,Object> driverShiftMap = new HashMap<Integer, Object>();

	@SuppressWarnings({ "unchecked" })
	public List<Object>  getScheduleOfDrivers(Shift shift) throws ParseException {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		String stratingDateOfMonth = utility.startAndEndDateOfCurrentMonth();
		List<HashMap<String, Object>> driverScheduleList1 = new ArrayList<HashMap<String, Object>>();
		List<HashMap<String, Object>> finalListOfShifts = new ArrayList<HashMap<String,Object>>();
		HashMap<String, Object> listOfDates = new HashMap<String, Object>();
		Calendar cal = Calendar.getInstance();
		Boolean isLooper = false;
		String driverName = null,truckName = null,shiftDate = null;
		List<Integer> daysList = null;
		List<String> formattedDateList,dateList = null;
		int preferedVehicleID = 0,dayOffId = 0,totalNoOfDays = 0;
			if(shift.getDatesToFetch() == 4){ //to get current month
				listOfDates = (HashMap<String, Object>) utility.getCurrentMonthInfo(utility.startDateOfCurrentMonth());
			}else if(shift.getDatesToFetch() == 3){ //to get c
				listOfDates = (HashMap<String, Object>) utility.getNextPrevSevenDays(utility.startDateOfWeek(),"",shift.getDatesToFetch());
			}else if(shift.getDatesToFetch() == 5){ //to get last month
				listOfDates = (HashMap<String, Object>) utility.getNextPrevSevenDays(utility.startDateOfLastWeek(),"",shift.getDatesToFetch());
			}else if(shift.getDatesToFetch() == 6){ //to get last month
				listOfDates = (HashMap<String, Object>) utility.getPreviousMonthInfo(utility.startDateOfLastMonth());
			}else{
				listOfDates = (HashMap<String, Object>) utility.getNextPrevSevenDays(shift.getStartDate(),shift.getEndDate(),shift.getDatesToFetch());
			}
		
		formattedDateList = (List<String>) listOfDates.get("startAndEndDateList");
		dateList =  (List<String>) listOfDates.get("datesList");
		daysList =  (List<Integer>) listOfDates.get("daysList");
		shift.setStartDate(formattedDateList.get(0));
		shift.setEndDate(formattedDateList.get(1));
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.FETCH_SCHEDULE_DETAILS).returningResultSet("driverscheduleInfo", new RowMapper<Map<String,Object>>()
						{
					@Override
					public Map<String,Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						try{
							Map<String,Object> responseMap = new HashMap<String, Object>();
							String shiftDate = rs.getString("shiftDate");
							
													
							responseMap.put("driverId", rs.getInt("driverID"));
							responseMap.put("shiftsPerDriver", rs.getInt("shiftsPerDriver"));
							responseMap.put("driverName", rs.getString("driverFirstName") + " "+ rs.getString("driverLastName"));
							responseMap.put("truckName", rs.getString("TruckCode"));
							responseMap.put("preferedVehicleID",rs.getInt("preferedVehicleID"));
							responseMap.put("shiftDay",rs.getInt("shiftDay"));
							responseMap.put("shiftDate",shiftDate);
							responseMap.put("shiftName",rs.getString("shiftName"));
							responseMap.put("shiftId",rs.getInt("workingShiftID"));
							responseMap.put("dayName",rs.getString("dayName"));
							responseMap.put("isDayOff",rs.getBoolean("isDayOff"));
							responseMap.put("isAvailableForShift",rs.getBoolean("isAvailableForShift"));
							responseMap.put("startTime",rs.getString("startTime"));
							responseMap.put("endTime",rs.getString("endTime"));
							responseMap.put("timeDifference",rs.getInt("timeDifference"));
							
							
							return responseMap;	
						}
						catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
						}
					}
						}).returningResultSet("listOfShifts1",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
								
									responseMap.put(ResponseConstantsOfShift.SHIFTID,rs.getInt("ID"));
									responseMap.put(ResponseConstantsOfShift.SHIFTDAY,rs.getInt("shiftDay"));
									responseMap.put(ResponseConstantsOfShift.SHIFTNAME,rs.getString("shiftName"));
										
									return responseMap;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						}).returningResultSet("listOfShifts2",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
								
									responseMap.put(ResponseConstantsOfShift.SHIFTID,rs.getInt("ID"));
									responseMap.put(ResponseConstantsOfShift.SHIFTDAY,rs.getInt("shiftDay"));
									responseMap.put(ResponseConstantsOfShift.SHIFTNAME,rs.getString("shiftName"));
										
									return responseMap;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						}).returningResultSet("dayWiseShiftCount1",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									
									responseMap.put("shiftCountPerDay",rs.getString("shiftCountPerDay"));
									responseMap.put("shiftDay",rs.getString("shiftDay"));
										
									return responseMap;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						}).returningResultSet("dayWiseShiftCount2",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
								
									responseMap.put("shiftCountPerDay",rs.getString("shiftCountPerDay"));
									responseMap.put("shiftDay",rs.getString("shiftDay"));
									
										
									return responseMap;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						}).returningResultSet("listOfDifferentShiftNames",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> listOfShifts  = new HashMap<String, Object>();
									listOfShifts.put("shiftId",rs.getString("shiftId"));
									listOfShifts.put("shiftName",rs.getString("shiftName"));
									return listOfShifts;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						}).returningResultSet("isDriversExist",
								new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try {
									HashMap<String, Object> driverCheckList = new HashMap<String, Object>();
									int driverCount = rs.getInt("isDriversExist");
									int isDriverScheduleGenarated = rs.getInt("isDriverScheduleGenarated");
									
									if(driverCount > 0 )
										driverCheckList.put("isDriversExist",true);
									else
										driverCheckList.put("isDriversExist",false);
									
									driverCheckList.put("isDriverScheduleGenarated", isDriverScheduleGenarated);
									
									return driverCheckList;
								} catch (SQLException ex) {
									return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
											ex.getMessage());
								}
							}
						});
						
		
	
		
		// IN parameters for stored procedure

		String[] inParamaters = {"iuserID","iDay","iStartDate","iEndDate","isStartingDayOfAMonth","iStartDateOfMonth"};
		Object[] inParamaterValues = {shift.getUserId(),cal.get(Calendar.DAY_OF_WEEK),shift.getStartDate(),shift.getEndDate(),
				UtilFunctions.isTodayStartingDayOfAMonth(),
				stratingDateOfMonth};		
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		
		HashMap<String, Object> driverScheduleMap = new HashMap<String, Object>();
		
		List<HashMap<String, Object>> driverscheduleInfo = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("driverscheduleInfo");
		
		List<HashMap<String, Object>> updatedDriverscheduleInfo = new ArrayList<HashMap<String,Object>>();
		
		
		
		List<HashMap<String, Object>> listOfShifts = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("listOfShifts1");
		List<HashMap<String, Object>> listOfShifts2 = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("listOfShifts2");
		if(listOfShifts2 != null){
			if(listOfShifts2.size() > 0)
				listOfShifts.addAll(listOfShifts2);
		}
		List<HashMap<String, Object>> dayWiseShiftCountList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("dayWiseShiftCount1");
		List<HashMap<String, Object>> dayWiseShiftCountList2 = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("dayWiseShiftCount2");
		if(dayWiseShiftCountList2 != null){
		if(dayWiseShiftCountList2.size() > 0)
			dayWiseShiftCountList.addAll(dayWiseShiftCountList2);
		}
		List<HashMap<String, Object>> listOfDifferentShiftNames = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("listOfDifferentShiftNames");
		
		List<HashMap<String, Object>> isDriversExist = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("isDriversExist");
		
		//List<Integer> dayList = (List<Integer>) listOfDates.get("daysList");
		HashMap<String, Object> dayWiseShiftCountMap = null;
		
		List<HashMap<String, Object>> dayWiseShiftList = new ArrayList<HashMap<String, Object>>();
		
		List<Object> responseList = new ArrayList<Object>();
		int tempDriverId = -1,driverId = -1,subIndex = 0,canAdd = 0,shiftId = -1,totalHours = 0;
		String tempShiftDate = null,dayName = null;
		//boolean canAdd = true;
		for(HashMap<String, Object> row : driverscheduleInfo){
			Map<String,Object> driverShiftList = new HashMap<String, Object>();
			shiftDate = (String) row.get("shiftDate");
			driverId = (int) row.get("driverId");
			dayName = (String) row.get("dayName");
			shiftId = (int) row.get("shiftId");
			
			boolean isDayOff = (boolean) row.get("isDayOff");
			boolean isDatesAdded = false;
			
			System.out.println("indexOf - "+dateList.indexOf(tempShiftDate));
			if(driverId != tempDriverId && tempDriverId != -1){//reinitialize 
				int indexOfdate = dateList.indexOf(tempShiftDate) + 1;
				for(int i = indexOfdate; i < dateList.size(); i++){
					int dayId = daysList.get(i);
					isDatesAdded = true;
				    driverShiftList = new HashMap<String, Object>();
					driverShiftList.put("isWorkingOnthatDate",false);
					driverShiftList.put("isAvailableForShift",false);
					driverShiftList.put("isDayOff",false);
					driverShiftList.put("dayName","");
					driverShiftList.put("shiftDay",dayId);
					driverShiftList.put("shiftName","");
					driverShiftList.put("shiftId","");
					driverShiftList.put("shiftDate",dateList.get(i));
					driverScheduleList1.add((HashMap<String, Object>) driverShiftList);
						
				}
				driverScheduleMap.put("driverId",  tempDriverId);
				driverScheduleMap.put("driverName", driverName);
				driverScheduleMap.put("truckName", truckName);
				driverScheduleMap.put("preferedVehicleID",preferedVehicleID);
				driverScheduleMap.put("totalNoOfDays",totalNoOfDays);
				driverScheduleMap.put("totalHours",totalHours);
				driverScheduleMap.put("shiftschedule",driverScheduleList1);
				updatedDriverscheduleInfo.add(driverScheduleMap);
				driverScheduleMap = new HashMap<String, Object>(); 
				driverScheduleList1 = new ArrayList<HashMap<String, Object>>();
				isLooper = false;
				subIndex = 0;
				totalNoOfDays = 0;
				totalHours = 0;
				
			}
			
			if(driverId != tempDriverId || tempDriverId  == -1 && subIndex == 0){ //to add missing dates
				for(String dates : dateList){
					if(UtilFunctions.formatDateAndCompare(shiftDate,dates)){
						int dayId = daysList.get(subIndex);
						
						 isDatesAdded = true;
						    driverShiftList = new HashMap<String, Object>();
							driverShiftList.put("isWorkingOnthatDate",false);
							driverShiftList.put("isAvailableForShift",false);
							driverShiftList.put("isShiftGenerated",false);
							driverShiftList.put("isDayOff",false);
							driverShiftList.put("dayName","");
							driverShiftList.put("shiftDay",dayId);
							driverShiftList.put("shiftName","");
							driverShiftList.put("shiftId",shiftId);
							driverShiftList.put("shiftDate",dates);
							
							driverScheduleList1.add((HashMap<String, Object>) driverShiftList);
							totalNoOfDays = 0;
							
						}
						subIndex++;
				}
			}
			
			if(isDatesAdded){
				driverShiftList = new HashMap<String, Object>();
				isDatesAdded = false;
			}
			
			
			if(!isLooper){ //gathering info
				driverName =  (String) row.get("driverName");
				truckName = (String) row.get("truckName");
				preferedVehicleID = (int) row.get("preferedVehicleID");
				isLooper = true;
			}
			
			boolean isAvailableForShift = (boolean) row.get("isAvailableForShift");
			
			if(isDayOff){//loop to add day off only once in the list
				dayOffId = (int) row.get("shiftDay");
				canAdd++;
			}else{
				if(isAvailableForShift)
					totalNoOfDays++;
				canAdd = 0;
			}
			
			
				driverShiftList.put("isWorkingOnthatDate",true);
				driverShiftList.put("isAvailableForShift",isAvailableForShift);
				driverShiftList.put("isShiftGenerated",true);
				driverShiftList.put("isDayOff",isDayOff);
				driverShiftList.put("dayName",row.get("dayName"));
				driverShiftList.put("shiftDay",row.get("shiftDay"));
				driverShiftList.put("shiftName",row.get("shiftName"));
				driverShiftList.put("shiftId",row.get("shiftId"));
				driverShiftList.put("startTime",row.get("startTime"));
				driverShiftList.put("endTime",row.get("endTime"));
				driverShiftList.put("shiftDate",shiftDate); 
				driverScheduleList1.add((HashMap<String, Object>) driverShiftList);
			
				if(!isDayOff){
					totalHours = totalHours + (int) row.get("timeDifference");
				}
			tempDriverId = driverId;
			tempShiftDate = shiftDate +" "+dayName;
			//looper++;
		}
		
		
		
		if(driverScheduleList1.size() > 0 && driverScheduleMap.size() == 0 && driverId != -1){
			int indexOfdate = dateList.indexOf(tempShiftDate) + 1;
			for(int i = indexOfdate; i < dateList.size(); i++){
				Map<String,Object> driverShiftList = new HashMap<String, Object>();
					int dayId = daysList.get(i);
				
					 driverShiftList = new HashMap<String, Object>();
				    driverShiftList = new HashMap<String, Object>();
					driverShiftList.put("isWorkingOnthatDate",false);
					driverShiftList.put("isAvailableForShift",false);
					driverShiftList.put("isDayOff",false);
					driverShiftList.put("dayName","");
					driverShiftList.put("shiftDay",dayId);
					driverShiftList.put("shiftName","");
					driverShiftList.put("shiftId","");
					driverShiftList.put("shiftDate",dateList.get(i));
					driverScheduleList1.add((HashMap<String, Object>) driverShiftList);

			}
			
			
			driverScheduleMap.put("driverId", tempDriverId);
			driverScheduleMap.put("driverName", driverName);
			driverScheduleMap.put("truckName", truckName);
			driverScheduleMap.put("preferedVehicleID",preferedVehicleID);
			driverScheduleMap.put("dayOffId",dayOffId);
			driverScheduleMap.put("shiftschedule",driverScheduleList1);
			driverScheduleMap.put("totalNoOfDays",totalNoOfDays);
			driverScheduleMap.put("totalHours",totalHours);
			updatedDriverscheduleInfo.add(driverScheduleMap);
			driverScheduleMap = null; 
			driverScheduleList1 = null;
			totalNoOfDays = 0;
			totalHours = 0;
		}
		
		boolean isFound = false;
		int weekLoop = 0,i=0;
		for (Integer temp : daysList) {
			
			dayWiseShiftCountMap = new HashMap<String, Object>();
			dayWiseShiftCountMap.put("date", dateList.get(i));
			dayWiseShiftCountMap.put("shiftDay", temp);
			dayWiseShiftCountMap.put("shiftCountPerDay", dayWiseShiftCountList.get(weekLoop).get("shiftCountPerDay"));
			HashMap<String, Object> shiftmap = null;
			
			for(HashMap<String, Object> tempMap : listOfShifts){
				if(temp == tempMap.get("shiftDay")){
					isFound = true;
					shiftmap = new HashMap<String, Object>();
					shiftmap.put("shiftName",tempMap.get("shiftName"));
					shiftmap.put("shiftId",tempMap.get("shiftId"));
					shiftmap.put("shiftDay",temp);
					finalListOfShifts.add(shiftmap);
				}
				if(isFound && temp!= tempMap.get("shiftDay")){
					break;
				}
			}
		
			if(weekLoop == 6){
				weekLoop = 0;
			}else{
				weekLoop++;
			}
			dayWiseShiftList.add(dayWiseShiftCountMap);
			isFound = false;
			i++;
		}
		boolean isDriverExist = (boolean) isDriversExist.get(0).get("isDriversExist"),isScheduleGenarated = true;
		int isDriverScheduleGenarated = (int) isDriversExist.get(0).get("isDriverScheduleGenarated");
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		if( (utility.isMonthStartingDay() || isDriverScheduleGenarated == 0)  && isDriverExist){
				isScheduleGenarated = false;
		}
		resultMap.put("isScheduleGenarated", isScheduleGenarated);
		System.out.println("updatedDriverscheduleInfo - "+updatedDriverscheduleInfo.size());
		responseList.add(updatedDriverscheduleInfo);
		responseList.add(finalListOfShifts);
		responseList.add(listOfDates);
		responseList.add(dayWiseShiftList);
		responseList.add(listOfDifferentShiftNames);
		responseList.add(resultMap);
	
		return responseList;
		
	}
	
	
	
	/**
	 * This method is used to genarate shifts schedule of drivers
	 * it returns List<HashMap<String, Object>> object with success or failure
	 * 
	 * @return List<Shift> object with shifts
	 * @throws ParseException 
	 */
		@SuppressWarnings("unchecked")
		public HashMap<String, Object>  genarateScheduleOfDrivers(Shift shift) {
			String stratingDateOfMonth = utility.startAndEndDateOfCurrentMonth();
			
		//registering jdbc connection and stored procedure to execute
				SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(SPNameConstants.GENARATE_SCHEDULE_DETAILS).returningResultSet("resultList", new RowMapper<Map<String,Object>>()
								{
							@Override
							public Map<String,Object> mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								Map<String,Object> responseMap = new HashMap<String, Object>();
														
								responseMap.put("isSuccess", true);
								responseMap.put("serviceMessage", "Schedule genarated successfully");
								
								return responseMap;
							}
					});
				
				// IN parameters for stored procedure

				String[] inParamaters = {"iuserID","startDay","noOfDays","iStrtDateOfMonth"};
				System.out.println("startDay - "+new UtilFunctions().getDay(Calendar.getInstance()));
				System.out.println("startDay - "+utility.startAndEndDateOfAMonth().get(0));
				System.out.println("no of days - "+new UtilFunctions().getNoOfDaysLeftInMonth(utility.startAndEndDateOfAMonth().get(0)));
				Object[] inParamaterValues = {shift.getUserId(),new UtilFunctions().getDay(new UtilFunctions().instanceOfStartDayOfMonth()),
						new UtilFunctions().getNoOfDaysLeftInMonth(utility.startAndEndDateOfAMonth().get(0)),stratingDateOfMonth};		
				
				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				List<HashMap<String, Object>> resultList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("resultList");
				if(resultList == null)
					return new HashMap<String, Object>();
				else
					return resultList.get(0);
		
	}

}
