
package trac.dao.dashboarddao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.dashboardbean.Dashboard;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.sqlqueryconstants.CommonSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;

import com.mysql.jdbc.ResultSetMetaData;


/**
 *  Reports related database Operations are handled in this class
 *  
 */
public class DashboardDaoOperations {


	public static JdbcTemplate jdbcTemplate = null;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	List<Map<String, String>> reportMetadataList;
	/**
	 * This method is used to execute the reports query
	 * @param dahsboard
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public Dashboard getReportInfo(
			Dashboard dahsboard) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		List<Map<String, Object>> reportDataList = null;
		List<Map<String, Object>> fieldsToBeDisplayed = dahsboard.getFieldsToBeDisplayed();
		List<Map<String, Object>> filteredList = new ArrayList<Map<String,Object>>();
		try {
			reportDataList = jdbcTemplate.queryForList(dahsboard.getTableSqlQuery());
		

		if(fieldsToBeDisplayed!=null && fieldsToBeDisplayed.size()>0){
		for(Map<String, Object> reportobject:reportDataList){
			Map<String, Object> newObject = new HashMap<String, Object>();
			for(Map<String, Object> filteredobject:fieldsToBeDisplayed){
			 
			for (Map.Entry<String, Object> entry : reportobject.entrySet())
			{
			   if(filteredobject.get("selectedColumnName").equals(entry.getKey())){
			   	newObject.put(entry.getKey(), entry.getValue());
			   }
			}
		}
			filteredList.add(newObject);
			reportDataList = filteredList;
		}
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
			
		// Condition to Check if record is available 
		if (reportDataList!=null && reportDataList.size() > 0 && reportDataList != null){
			
			dahsboard.setTableResultList(reportDataList);
			dahsboard.setTableRecordFound(true);
			dahsboard.setSuccess(true);

		}else{
			dahsboard.setServiceMessage("No records found");
			dahsboard.setTableRecordFound(false);
			dahsboard.setSuccess(false);
		}//end of else if

		return dahsboard;
	}//end of getReportInfo


	/**
	 * This method is get reports columns names with respective datatype 
	 * @param dahsboard
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	
	public Dashboard getReportMetaDataInfo(Dashboard dahsboard) {

		System.out.println("getReportTofetch - "+dahsboard.getReportTofetch());

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		String sqlQuery = null;

		if(dahsboard.getReportTofetch()!=null)
		{
			if(dahsboard.getReportTofetch().equalsIgnoreCase("trucks")){
				sqlQuery = CommonSqlQueryConstants.GET_TRUCK_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("operations")){
				sqlQuery = CommonSqlQueryConstants.GET_OPERATIONS_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("attendance")){
				sqlQuery = CommonSqlQueryConstants.GET_ATTENDANCE_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("expenses")){
				sqlQuery = CommonSqlQueryConstants.GET_TRUCK_EXPENSES;
			}

			
		   sqlQuery = sqlQuery.concat(" LIMIT 1");
		}


		List<Map<String, String>> resultSet = null;
		if(sqlQuery!=null)
			resultSet = jdbcTemplate.query(sqlQuery,new ResultSetExtractor<List<Map<String, String>>>() {

				@Override
				public List<Map<String, String>>  extractData(ResultSet rs) throws SQLException, DataAccessException {

					ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
					int columnCount = rsmd.getColumnCount();

					List<Map<String, String>> reportMetadataList = new ArrayList<Map<String, String>>();
					for(int i = 1 ; i <= columnCount ; i++){
						Map<String, String> columnInfo = new HashMap<String, String>();
						String columnName = rsmd.getColumnLabel(i);
						String columnType = rsmd.getColumnTypeName(i);
						columnInfo.put("coulmnName",columnName);
						columnInfo.put("coulmnDisplayName",StringUtils.join(
							    StringUtils.splitByCharacterTypeCamelCase(columnName),
							    ' '
							));
						
						if(columnType!=null){
							
							if(columnName.equalsIgnoreCase("AttendanceDate") || columnName.equalsIgnoreCase("AttendanceTime") || columnName.equalsIgnoreCase("DateOfPurchase") ||
							   columnName.equalsIgnoreCase("TruckRegistrationDate") || columnName.equalsIgnoreCase("DateOfAcquiring") ||
							   columnName.equalsIgnoreCase("PolicyIssueDate") || columnName.equalsIgnoreCase("PolicyExpiryDate") || columnName.equalsIgnoreCase("ReRegistrationDueDate") ||
							    columnName.equalsIgnoreCase("CalledTime") || columnName.equalsIgnoreCase("AssignedTime") ||
							   columnName.equalsIgnoreCase("DispatchedTime") || columnName.equalsIgnoreCase("CheckedInTime") || columnName.equalsIgnoreCase("CompletedTime") || columnName.equalsIgnoreCase("ExpenseTime"))
								columnInfo.put("coulmnType","DATE");
							else if(columnName.equalsIgnoreCase("ModelYear") ||
									   columnName.equalsIgnoreCase("VehcileModelYear"))
										columnInfo.put("coulmnType","YEAR");
							else if(columnName.equalsIgnoreCase("ExpenseType") || columnName.equalsIgnoreCase("PaymentType") || columnName.equalsIgnoreCase("currencyType"))
								columnInfo.put("coulmnType","VARCHAR");
							else if(columnType.equalsIgnoreCase("FLOAT"))
								columnInfo.put("coulmnType","INT");
							else 
								columnInfo.put("coulmnType",columnType);
						}
						System.out.println("originalcoulmnName :"+rsmd.getColumnName(i) +",coulmnName :"+columnName + ",coulmnType :"+rsmd.getColumnTypeName(i));
						reportMetadataList.add(columnInfo);
					}

					return reportMetadataList;
				}
			});

		// TODO Auto-generated method stub
		if(resultSet!=null && resultSet.size() > 0){
			dahsboard.setSuccess(true);
			dahsboard.setTableMetaDataList(resultSet);
		}else{
			dahsboard.setSuccess(false);
		}

		return dahsboard;
	}


	/**
	 * This method is get reports data
	 * @param dahsboard
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	
	
	public Dashboard getReportDataInfo(Dashboard dahsboard) {

		System.out.println("getReportTofetch - "+dahsboard.getReportTofetch());

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		String sqlQuery = null;

		if(dahsboard.getReportTofetch()!=null)
		{
			if(dahsboard.getReportTofetch().equalsIgnoreCase("trucks")){
				sqlQuery = CommonSqlQueryConstants.GET_TRUCK_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("operations")){
				sqlQuery = CommonSqlQueryConstants.GET_OPERATIONS_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("attendance")){
				sqlQuery = CommonSqlQueryConstants.GET_ATTENDANCE_REPORT_COLUMNS;
			}else if(dahsboard.getReportTofetch().equalsIgnoreCase("expenses")){
				sqlQuery = CommonSqlQueryConstants.GET_TRUCK_EXPENSES;
			}
		}

		List<Map<String, Object>> resultSet = null;
		if(sqlQuery!=null){
			try {
				resultSet = jdbcTemplate
						.queryForList(sqlQuery);
			} catch (DataAccessException e) {
				
				e.printStackTrace();
			}
		}

		// TODO Auto-generated method stub
		if(resultSet!=null && resultSet.size() > 0){
			dahsboard.setSuccess(true);
			dahsboard.setTableResultList(resultSet);
		}else{
			dahsboard.setSuccess(false);
		}

		return dahsboard;
	}
	
	
	/**
	 * This method is used to save report details of user
	 * 
	 * @param reportdetails
	 * @spname usp_report_insert
	 * @return success or failure
	 *
	 */
	public Object saveReportInfo(Dashboard dashboard) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_INSERT_SPNAME)
		.returningResultSet("response",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					responseMap.put(CommonResponseConstants.ISSUCCESS,rs.getBoolean("issuccess"));
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ireportName","iReportTable","ireportQuery","icolumnNames","iWhereClause"};
		Object[] inParamaterValues = new Object[]{dashboard.getUserId(),dashboard.getReportName(),dashboard.getReportTable(),dashboard.getReportSqlQuery(),dashboard.getColumnNames(),dashboard.getWhereClause()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("response");

		return returnjson;

	}
	
	
	/**
	 * This method is used to update report details of user
	 * 
	 * @param reportdetails
	 * @spname usp_report_update
	 * @return success or failure
	 *
	 */
	public Object updateReportInfo(Dashboard dashboard) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_UPDATE_SPNAME)
		.returningResultSet("response",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					responseMap.put(CommonResponseConstants.ISSUCCESS,rs.getBoolean("issuccess"));
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ireportId","ireportName","ireportQuery","icolumnNames","iWhereClause"};
		Object[] inParamaterValues = new Object[]{dashboard.getUserId(),dashboard.getReportId(),dashboard.getReportName(),dashboard.getReportSqlQuery(),dashboard.getColumnNames(),dashboard.getWhereClause()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("response");

		return returnjson;

	}

	/**
	 * This method is used to delete report 
	 * 
	 * @param reportdetails
	 * @spname usp_report_update
	 * @return success or failure
	 *
	 */
	public Object deleteReportInfo(Dashboard dashboard) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_DELETE_SPNAME)
		.returningResultSet("response",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					responseMap.put(CommonResponseConstants.ISSUCCESS,rs.getBoolean("issuccess"));
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ireportId"};
		Object[] inParamaterValues = new Object[]{dashboard.getUserId(),dashboard.getReportId()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("response");

		return returnjson;

	}
	
	
	/**
	 * This method is used to fetch report details of user
	 * 
	 * @param reportdetails
	 * @spname usp_report_fetch
	 * @return success or failure
	 *
	 */
	public Object fetchReportInfo(Dashboard dashboard) {
		 reportMetadataList= new ArrayList<Map<String, String>>();
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.REPORT_FETCH_SPNAME)
		.returningResultSet("response",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if(rs.getBoolean("issuccess")){
						 responseMap.put(CommonResponseConstants.ISSUCCESS,true);
						 responseMap.put("reportName",rs.getString("reportName"));
						 responseMap.put("reportTableName",rs.getString("reportTableName"));
						 responseMap.put("reportId",rs.getInt("reportId"));
						 responseMap.put("reportQuery",rs.getString("reportQuery"));
						 responseMap.put("columnNames",rs.getString("columnNames"));
						 responseMap.put("whereClause",rs.getString("whereClause"));
						 
				    }else{
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iReportId"};
		Object[] inParamaterValues = new Object[]{dashboard.getUserId(),dashboard.getReportId()};	
		
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("response");
		
        if(dashboard.getReportId()>0) {
			if(returnjson!=null && returnjson.size()>0) {
				HashMap<String, Object> resultMap = returnjson.get(0);
					
				String sqlQuery = null;

				if(resultMap.get("reportTableName")!=null)
				{
					if(((String)resultMap.get("reportTableName")).equalsIgnoreCase("trucks")){
						sqlQuery = CommonSqlQueryConstants.GET_TRUCK_REPORT_COLUMNS;
					}else if(((String)resultMap.get("reportTableName")).equalsIgnoreCase("operations")){
						sqlQuery = CommonSqlQueryConstants.GET_OPERATIONS_REPORT_COLUMNS;
					}else if(((String)resultMap.get("reportTableName")).equalsIgnoreCase("attendance")){
						sqlQuery = CommonSqlQueryConstants.GET_ATTENDANCE_REPORT_COLUMNS;
					}else if(((String)resultMap.get("reportTableName")).equalsIgnoreCase("expenses")){
						sqlQuery = CommonSqlQueryConstants.GET_TRUCK_EXPENSES;
					}

					
				   sqlQuery = sqlQuery.concat(" LIMIT 1");
				}


				List<Map<String, String>> resultSet = null;
				if(sqlQuery!=null)
					resultSet = jdbcTemplate.query(sqlQuery,new ResultSetExtractor<List<Map<String, String>>>() {

						@Override
						public List<Map<String, String>>  extractData(ResultSet rs) throws SQLException, DataAccessException {

							ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
							int columnCount = rsmd.getColumnCount();

							
							for(int i = 1 ; i <= columnCount ; i++){
								//for converting spaces to Camel case columnnames
								Map<String, String> columnInfo = new HashMap<String, String>();
								String columnName = rsmd.getColumnLabel(i);
								String columnType = rsmd.getColumnTypeName(i);
								columnInfo.put("coulmnName",columnName);
								columnInfo.put("coulmnDisplayName",StringUtils.join(
									    StringUtils.splitByCharacterTypeCamelCase(columnName),
									    ' '
									));
							
								if(columnType!=null){
									if(columnName.equalsIgnoreCase("AttendanceDate") || columnName.equalsIgnoreCase("AttendanceTime") || columnName.equalsIgnoreCase("DateOfPurchase") ||
											   columnName.equalsIgnoreCase("TruckRegistrationDate") || columnName.equalsIgnoreCase("DateOfAcquiring") ||
											   columnName.equalsIgnoreCase("PolicyIssueDate") || columnName.equalsIgnoreCase("PolicyExpiryDate") || columnName.equalsIgnoreCase("ReRegistrationDueDate") ||
											    columnName.equalsIgnoreCase("CalledTime") || columnName.equalsIgnoreCase("AssignedTime") ||
											   columnName.equalsIgnoreCase("DispatchedTime") || columnName.equalsIgnoreCase("CheckedInTime") || columnName.equalsIgnoreCase("CompletedTime") || columnName.equalsIgnoreCase("ExpenseTime"))
												columnInfo.put("coulmnType","DATE");
									else if(columnName.equalsIgnoreCase("ModelYear") ||
											   columnName.equalsIgnoreCase("VehcileModelYear"))
												columnInfo.put("coulmnType","YEAR");
									else if(columnName.equalsIgnoreCase("ExpenseType") || columnName.equalsIgnoreCase("PaymentType") || columnName.equalsIgnoreCase("currencyType"))
										columnInfo.put("coulmnType","VARCHAR");
									else if(columnType.equalsIgnoreCase("FLOAT"))
										columnInfo.put("coulmnType","INT");
									else 
										columnInfo.put("coulmnType",columnType);
								}
								System.out.println("originalcoulmnName :"+rsmd.getColumnName(i) +",coulmnName :"+columnName + ",coulmnType :"+rsmd.getColumnTypeName(i));
								reportMetadataList.add(columnInfo);
							}

							return reportMetadataList;
						}
					});

				
				
				
				
			}
		}
        List<Object> resultList = new ArrayList<Object>();
		resultList.add(returnjson);
		resultList.add(reportMetadataList);
		return resultList;

	}

	
	
}
