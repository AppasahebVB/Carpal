package trac.dao.vehicledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import trac.beans.jdbcteamplatebean.JDBCConnection;

//import trac.beans.userbean.UserLocation;
import trac.beans.vehcilebean.Vehicle;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.sqlqueryconstants.VehicleSqlConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.CommonUtil;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

/**
 *  Vehicle related database Operations are handled in this class
 *  
 */

public class VehicleDaoOperations {


	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	
	/**
	 * This method is used to register the details of first time user 
	 * it returns user object contains all related info  of user
	 *
	 * @param  userName 
	 * @spname  usp_Vehicle_Vehicles_Insert
	 * @return Users plain java object contains registered user info
	 * @author rpenta     
	 */	
	public List<Object> registerVehcileInfo(Vehicle vehicleDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_REGISTRATION_SPNAME).returningResultSet("registerstatus",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
						HashMap<String, Object> responseMap = new HashMap<String, Object>();
		
						if(rs.getBoolean("issuccess")){
		
							responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
							responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHICLE_PIC_URL, rs.getString("vehiclePicURL"));
							
							if(rs.getInt("isFavorite") == 1)
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
							else
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle registred successfully.");
		
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Vehicle already registered");
							responseMap.put(CommonResponseConstants.ISSUCCESS, false);
						}
						return responseMap;
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
			}).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
					{
				@Override
				public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
					try{
						Map<String,Object> customerLocationMap = new HashMap<String, Object>();

						
						customerLocationMap.put("serviceLocation", rs.getString("Description"));
						
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLat"));
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
						//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

						// if registerd through web
						customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));
						
						customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
						customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE, rs.getInt("serviceTimeTypeID"));
						customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, rs.getString("serviceTime"));
						


						if(rs.getInt("isServiceLocation") == 1)
							customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
						else
							customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

						if(rs.getInt("isDestinationLocation") == 1)
							customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
						else
							customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

						return customerLocationMap;
					}catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
					});

		//IN parameters for stored procedure
		//String[] inParamaters = {"iuserID","ivehicleName","iChassis","iBrandModelID","ifavorite","ivehicleModelID","ivehicleRegisterNo","ivehicleModelYear"};
		String[] inParamaters = {"iuserID","isCustomer","icustomerUID","imemberShipStart","imemberShipEnd","ivehicleName","iChassis","ivehicleRegisterNo","icountryISO","icity","iplateCode","imemberShipTypeId","iBrandModelID","ivehicleModelID","ivehicleModelYear","icolor","idateOfPurchase","iregistrationDate","imileage","ifavorite"};
		//Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getVehicleName(),vehicleDetails.getVin(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getIsFavorite(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getVehicleModelYear()};
		Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getIsCustomer(),vehicleDetails.getCustomerId(),vehicleDetails.getMemberShipStart(),vehicleDetails.getMemberShipEnd(),vehicleDetails.getVehicleName(),vehicleDetails.getVin(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getCountryISO(),vehicleDetails.getCity(),vehicleDetails.getPlateCode(),vehicleDetails.getMemberShipTypeId(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleModelYear(),vehicleDetails.getColor(),vehicleDetails.getDateOfPurchase(),vehicleDetails.getRegistrationDate(),vehicleDetails.getMileage(),vehicleDetails.getIsFavorite()};

	    // executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehiclesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("registerstatus");
		List<HashMap<String, Object>> locationList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		
		List<Object> responseList = new ArrayList<Object>();
		if(!(vehiclesList!=null && vehiclesList.size()>0)){
			vehiclesList = new ArrayList<HashMap<String, Object>>();
			vehiclesList.add(new HashMap<String, Object>()); 
		}
		if(!(locationList!=null && locationList.size()>0)){
			locationList = new ArrayList<HashMap<String, Object>>();
		}
		responseList.add(vehiclesList);
		responseList.add(locationList);
		return responseList;
	}


	/**
	 * This method is used to edit the vehicleDetails of Existing User
	 * it returns user object contains all related info  of user
	 * 
	 * @param  userId,vehicleDetails
	 * @spname  usp_Vehicle_Vehicles_Update
	 * @return true or false
	 */

	@SuppressWarnings("unchecked")
	public List<Object> editVehcileInfo(final Vehicle vehicleDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_UPDATE_SPNAME).returningResultSet("updateStatus",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
	
					System.out.println("Vehicle Update IsSuccess = "+rs.getBoolean(1));
					if(rs.getBoolean(1)){
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						responseMap.put(ResponseConstantsOfVehicle.VEHICLE_FILENAME, rs.getString("vehicleFileNameOnCloud"));
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, rs.getBoolean("isFavorite"));
						responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
						responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
						responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
						responseMap.put("vehicleId", rs.getInt("vehicleID"));
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
	
					return responseMap;
				}catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
			}).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
					{
				@Override
				public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
					try{
						Map<String,Object> customerLocationMap = new HashMap<String, Object>();

						
						customerLocationMap.put("serviceLocation", rs.getString("Description"));
						
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE, rs.getString("GPSLocationLat"));
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
						//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

						// if registerd through web
						customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));
						
						customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
						customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE, rs.getInt("serviceTimeTypeID"));
						customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, rs.getString("serviceTime"));
						


						if(rs.getInt("isServiceLocation") == 1)
							customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
						else
							customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

						if(rs.getInt("isDestinationLocation") == 1)
							customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
						else
							customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

						return customerLocationMap;
					}catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
					});

				System.out.println("Vehicle UVIN = "+vehicleDetails.getVin());
								
				//IN parameters for stored procedure
				//String[] inParamaters = {"iuserID","ivehicleName","iVehicleId","iChassis","iBrandModelID","ifavorite","ivehicleModelID","ivehicleRegisterNo","ivehicleModelYear"};
				//Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getVehicleName(),vehicleDetails.getVehicleId(),vehicleDetails.getVin(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getIsFavorite(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getVehicleModelYear()};
				
				String[] inParamaters = {"iuserID","icustomerUID","iVehicleId","imemberShipStart","imemberShipEnd","ivehicleName","iChassis","ivehicleRegisterNo","icountryISO","icity","iplateCode","imemberShipTypeId","iBrandModelID","ivehicleModelID","ivehicleModelYear","icolor","idateOfPurchase","iregistrationDate","imileage","ifavorite"};
				Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getCustomerId(),vehicleDetails.getVehicleId(),vehicleDetails.getMemberShipStart(),vehicleDetails.getMemberShipEnd(),vehicleDetails.getVehicleName(),vehicleDetails.getVin(),vehicleDetails.getVehicleRegisterNo(),vehicleDetails.getCountryISO(),vehicleDetails.getCity(),vehicleDetails.getPlateCode(),vehicleDetails.getMemberShipTypeId(),vehicleDetails.getVehicleBrandId(),vehicleDetails.getVehicleModelId(),vehicleDetails.getVehicleModelYear(),vehicleDetails.getColor(),vehicleDetails.getDateOfPurchase(),vehicleDetails.getRegistrationDate(),vehicleDetails.getMileage(),vehicleDetails.getIsFavorite()};

				
				//executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
				System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

				List<HashMap<String, Object>> vehiclesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("updateStatus");
				List<HashMap<String, Object>> locationList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
				
				List<Object> responseList = new ArrayList<Object>();
				if(!(vehiclesList!=null && vehiclesList.size()>0)){
					vehiclesList = new ArrayList<HashMap<String, Object>>();
					vehiclesList.add(new HashMap<String, Object>()); 
				}
				if(!(locationList!=null && locationList.size()>0)){
					locationList = new ArrayList<HashMap<String, Object>>();
				}
				responseList.add(vehiclesList);
				responseList.add(locationList);
				return responseList;
				
	}


	/**
	 * This method is used to get the vehicleDetails of Existing User
	 * it returns user object contains all related info  of user
	 * 
	 * @param  userId
	 * @spname  usp_Vehicle_Vehicles_Select
	 * @return userId And registered vehicle details
	 * @author rpenta
	 */
	public List<HashMap<String, Object>> getVehcileInfo(Vehicle vehicleDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_FETCH_SPNAME)
		.returningResultSet("vehicleDetailsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					
					responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
					if(rs.getInt("isFavorite") == 1)
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
					else
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
	
					//if registerd through web
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPTYPEID, rs.getInt("memberShipTypeId"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPSTART, rs.getString("memberShipStart"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPEND, rs.getString("memberShipEnd"));
					responseMap.put(ResponseConstantsOfVehicle.COUNTRYISO, rs.getString("countryISO"));
					responseMap.put(ResponseConstantsOfVehicle.CITY, rs.getString("city"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
					responseMap.put(ResponseConstantsOfVehicle.COLOR, rs.getString("color"));
					responseMap.put(ResponseConstantsOfVehicle.DATEOFPURCHASE, rs.getString("dateOfPurchase"));
					responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
					responseMap.put(ResponseConstantsOfVehicle.MILEAGE, rs.getString("mileage"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
					responseMap.put(ResponseConstantsOfVehicle.DISPLAY_ORDER, rs.getString("displayOrder"));
	
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		//IN parameters for stored procedure
		/*String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {vehicleDetails.getUserId()};*/
		
		String[] inParamaters = {"iuserID","icustomerUID"};
		Object[] inParamaterValues = {vehicleDetails.getUserId(),vehicleDetails.getCustomerId()};

		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		List<HashMap<String, Object>> vehiclesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleDetailsList");
		return vehiclesList;
	}

	/**
	 *This method is used to get the all vehicle brands list of Corresponding Model.
	 *
	 *@param userid
	 *@spname usp_vehicles_vehicleBrands_select
	 * @return userid, List of Vehicle Brands
	 */

public List<Object> getVehicleBrands() {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.GET_VEHICLE_BRAND_FETCH_SPNAME)
		.returningResultSet("vehicleBrandsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
	
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName").trim());
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getLong("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName").trim());
	
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
			}).returningResultSet("vinmodelsList",  new RowMapper<HashMap<String, Object>>()
					{
				@Override
				public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
				{
					try{
						HashMap<String, Object> responseMap = new HashMap<String, Object>();
		
						responseMap.put("vincountryCode", rs.getString("vincountryCode").trim());
						responseMap.put("manufacturer", rs.getString("manufacturer").trim());
						responseMap.put("manufacturerCountry", rs.getString("manufacturerCountry"));
						
		
						return responseMap;
					}catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				    }
				}
				}).returningResultSet("vinmodelyearList",  new RowMapper<HashMap<String, Object>>()
						{
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					{
						try{
							HashMap<String, Object> responseMap = new HashMap<String, Object>();
			
							responseMap.put("vinmodelCode", rs.getString("vinmodelCode").trim());
							responseMap.put("vinModelYear", rs.getString("vinModelYear"));
							
							return responseMap;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					    }
					}
					});

		//IN parameters for stored procedure
		Map<String, Object> inParamMap = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		
		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(in);
		List<Object> vehicleBrandsList 					= (List<Object>) simpleJdbcCallResult.get("vehicleBrandsList");
		List<HashMap<String, Object>> vinmodelsList 	= (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vinmodelsList");
		List<HashMap<String, Object>> vinmodelyearList  = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vinmodelyearList");
		
		List<Object> responseList = new ArrayList<Object>();
		responseList.add(vehicleBrandsList!=null ? vehicleBrandsList : new ArrayList<HashMap<String, Object>>());
		responseList.add(vinmodelsList!=null ? vinmodelsList : new ArrayList<HashMap<String, Object>>());
		responseList.add(vinmodelyearList!=null ? vinmodelyearList : new ArrayList<HashMap<String, Object>>());
		
		return responseList;
	}

	/**
	 *This method is used to update the vehicle image that was uploaded.
	 *
	 *@param vehicleId,vehcilePicUrl
	 *@spname 
	 * @return 1 if updated or else 0 if not updated
	 */
	public int updateVehcilePic(int vehicleId, String vehcilePicUrl, String vehicleFileNameOnCloud) {
		       //Jdbc Connection
				jdbcTemplate = JDBCConnection.getJdbcTemplate();
				System.out.println("jdbcTemplate - "+jdbcTemplate);
				
				String dynamicSqlQuery = VehicleSqlConstants.UPDATE_VEHICLE_PIC;
				return jdbcTemplate.update(dynamicSqlQuery,vehcilePicUrl,vehicleFileNameOnCloud,vehicleId);
				
	}
	
	/**
	 * unregister vehicle Info
	 * 
	 * @param vehicleId
	 * @spname usp_Vehicle_Vehicles_Delete
	 * @return 1 for success or 0 for failure
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>>  deleteVehilceInfo(Vehicle vehicleDetails) {
			//getting jdbc connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - "+jdbcTemplate);
	
			//registering jdbc connection and stored procedure to execute
			SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(SPNameConstants.VEHICLE_UNREGISTER_SPNAME)
			.returningResultSet("vehicleunregister",  new RowMapper<HashMap<String, Object>>()
				{
				@Override
				public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					{
						try{
							HashMap<String, Object> responseMap = new HashMap<String, Object>();
					
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, rs.getBoolean("issuccess"));
							return responseMap;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
						}
					}
			});
	
			//IN parameters for stored procedure
			String[] inParamaters = {"iVehicleID","icustomerUID","iuserID"};
			Object[] inParamaterValues = {vehicleDetails.getVehicleId(),vehicleDetails.getCustomerId(),vehicleDetails.getUserId()};
	
			// executing stored procedure
			Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
	
	
	
			System.out.println("vehicleunregister "+simpleJdbcCallResult.get("vehicleunregister"));
			return (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleunregister");
		}
	
	
	/**
	 * update vehicle Display Order
	 * 
	 * @param vehicles
	 * @spname usp_Vehicle_DisplayOrder_update
	 * @return 1 for success or 0 for failure
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>>  updateVehcileDisplayOrder(ArrayList<LinkedHashMap<String, Object>> vehicles) {
			//getting jdbc connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - "+jdbcTemplate);
	
			//registering jdbc connection and stored procedure to execute
			SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(SPNameConstants.VEHICLE_DISPLAY_ORDER_SPNAME)
			.returningResultSet("vehicleunregister",  new RowMapper<HashMap<String, Object>>()
				{
				@Override
				public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					{
						try{
							HashMap<String, Object> responseMap = new HashMap<String, Object>();
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, rs.getBoolean("issuccess"));
							return responseMap;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
						}
					}
			});
	
			//IN parameters for stored procedure
			String[] inParamaters = {"ivehiclesArray","ivehiclesCount"};
			Object[] inParamaterValues = {new CommonUtil().buildVehicleDisplayOrderArray(vehicles),vehicles.size()};
	
			// executing stored procedure
			Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
	
	
	
			System.out.println("vehicleunregister "+simpleJdbcCallResult.get("vehicleunregister"));
			return (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleunregister");
		}
	
}
