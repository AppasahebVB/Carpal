package trac.dao.driverdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


import trac.beans.driverbean.Driver;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.sqlqueryconstants.DriverSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.CommonUtil;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.CommonUtilInterface;
import trac.util.utilinterface.DBUtilInterface;

/**
 * 
 * DriverSqlOperations inserts,updates and fetches the Driver related Information from the Database
 * @author arokalla
 * @version 1.0
 * @since   2014-11-27 
 */

public class DriverSqlOperations {

	public static JdbcTemplate jdbcTemplate = null;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonUtilInterface commonUtil = new CommonUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	int id = 1;

	/**
	 * This method is used to insert Personal details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> insertPersonalDetails(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PERSONAL_DETAILS_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_PERSONAL_DETAILS_SAVED);
					}else{
						if(rs.findColumn("serviceMessage")>0 && rs.getString("serviceMessage")!=null){
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverFirstName","iDriverLastName","iDriverLicenceNo","iLicenceExpiryDate","iNationality","iGovernemntIdNumber","iPassportNo","iPassportIssuedDate","iVisaNumber","iVisaIssuedDate","iPassportExpiryDate","iVisaExpiryDate","iHealthInsuranceCardNo","iHealthInsuranceCardExpirydate"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverFirstName(),driverDetails.getDriverLastName(),driverDetails.getLicenseNo(),driverDetails.getLicenseNoExpiredAt()
				,driverDetails.getNationality(),driverDetails.getGovernmentIdNumber(),driverDetails.getPassportNo(),
				driverDetails.getPassportIssuedAt(),driverDetails.getVisaNo(),driverDetails.getVisaIssuedAt(),driverDetails.getPassportExpiredAt(),
				driverDetails.getVisaExpiredAt(),driverDetails.getHealthInsuranceCardNo(),driverDetails.getHealthInsuranceCardExpiryAt()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


	/**
	 * This method is used to update Personal details of Driver in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> updatePersonalDetails(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PERSONAL_DETAILS_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{

						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_PERSONAL_DETAILS_UPDATED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID","iuserID","iDriverFirstName","iDriverLastName","iDriverLicenceNo","iLicenceExpiryDate","iNationality","iGovernemntIdNumber","iPassportNo","iPassportIssuedDate","iVisaNumber","iVisaIssuedDate","iPassportExpiryDate","iVisaExpiryDate","iHealthInsuranceCardNo","iHealthInsuranceCardExpirydate"};
		Object[] inParamaterValues = {driverDetails.getDriverId(),driverDetails.getUserId(),driverDetails.getDriverFirstName(),driverDetails.getDriverLastName(),driverDetails.getLicenseNo(),driverDetails.getLicenseNoExpiredAt()
				,driverDetails.getNationality(),driverDetails.getGovernmentIdNumber(),driverDetails.getPassportNo(),
				driverDetails.getPassportIssuedAt(),driverDetails.getVisaNo(),driverDetails.getVisaIssuedAt(),driverDetails.getPassportExpiredAt(),
				driverDetails.getVisaExpiredAt(),driverDetails.getHealthInsuranceCardNo(),driverDetails.getHealthInsuranceCardExpiryAt()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to fetch Personal details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchPersonalDetails(
			Driver driverDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PERSONAL_DETAILS_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID,rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_FIRSTNAME,rs.getString("driverFirstName"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_LASTNAME,rs.getString("driverLastName"));
							responseMap.put(ResponseConstantsOfDriver.LICENSE_NO,rs.getString("licenseNo"));
							responseMap.put(ResponseConstantsOfDriver.LICENSENO_EXPIRED_AT,rs.getString("licenseNoExpiredAt"));
							responseMap.put(ResponseConstantsOfDriver.NATIONALITY,rs.getString("nationality"));
							responseMap.put(ResponseConstantsOfDriver.GOVTIDNO,rs.getString("governmentIdNumber"));
							responseMap.put(ResponseConstantsOfDriver.PASSPORT_NO,rs.getString("passportNo"));
							responseMap.put(ResponseConstantsOfDriver.PASSPORT_EXPIRED_AT,rs.getString("passportExpiredAt"));
							responseMap.put(ResponseConstantsOfDriver.VISA_NO,rs.getString("visaNo"));
							responseMap.put(ResponseConstantsOfDriver.VISA_EXPIREDAT,rs.getString("visaExpiredAt"));
							responseMap.put(ResponseConstantsOfDriver.HEALTH_INSURANCE_CARD_NO,rs.getString("insuranceCardID"));
							responseMap.put(ResponseConstantsOfDriver.HEALTH_INSURANCE_CARD_EXPIRY_AT,rs.getString("insuranceExpiredAt"));
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to insert Contact details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> insertContactInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_CONTACT_DETAILS_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_CONTACT_DETAILS_SAVED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage") );
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverID","iPhone","iEmail","iArea","iStreet","iBuilding","iLandMark","iCity","iPOBox","iCountryISO"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.getPhone(),driverDetails.getEmail(),
				driverDetails.getArea(),driverDetails.getStreet(),driverDetails.getBuilding(),driverDetails.getLandmark(),
				driverDetails.getCity(),driverDetails.getPostBox(),driverDetails.getCountry()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}


	/**
	 * This method is used to update Contact details of Driver in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> updateContactInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_CONTACT_DETAILS_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_CONTACT_DETAILS_UPDATED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverID","iPhone","iEmail","iArea","iStreet","iBuilding","iLandMark","iCity","iPOBox","iCountryISO"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.getPhone(),driverDetails.getEmail(),
				driverDetails.getArea(),driverDetails.getStreet(),driverDetails.getBuilding(),driverDetails.getLandmark(),
				driverDetails.getCity(),driverDetails.getPostBox(),driverDetails.getCountry()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to fetch ContactInfo details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchContactInfo(
			Driver driverDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_CONTACT_DETAILS_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID,rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.PHONE,rs.getString("phone"));
							responseMap.put(ResponseConstantsOfDriver.EMAIL,rs.getString("email"));
							responseMap.put(ResponseConstantsOfDriver.AREA,rs.getString("area"));
							responseMap.put(ResponseConstantsOfDriver.STREET,rs.getString("street"));
							responseMap.put(ResponseConstantsOfDriver.BUILDING,rs.getString("building"));
							responseMap.put(ResponseConstantsOfDriver.LANDMARK,rs.getString("landmark"));
							responseMap.put(ResponseConstantsOfDriver.CITY,rs.getString("city"));
							responseMap.put(ResponseConstantsOfDriver.POSTBOX,rs.getString("poBox"));
							responseMap.put(ResponseConstantsOfDriver.COUNTRY,rs.getString("countryISO"));

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}


	/**
	 * This method is used to insert Emergency Contact details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> insertorupdateEmergencyContactInfo(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_EMERGENCY_CONTACT_DETAILS_INSERTORUPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage") );
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverID","iContactName","iPhone","iEmail","iArea","iStreet","iBuilding","iLandMark","iCity","iPOBox","iCountryISO"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.geteContactName(),driverDetails.getPhone(),driverDetails.getEmail(),
				driverDetails.getArea(),driverDetails.getStreet(),driverDetails.getBuilding(),driverDetails.getLandmark(),
				driverDetails.getCity(),driverDetails.getPostBox(),driverDetails.getCountry()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}




	/**
	 * This method is used to fetch EmergencyContact details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchEmergencyContactInfo(
			Driver driverDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_EMERGENCY_CONTACT_DETAILS_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID,rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.CONTACT_PERSON_NAME,rs.getString("eContactName"));
							responseMap.put(ResponseConstantsOfDriver.PHONE,rs.getString("ePhone"));
							responseMap.put(ResponseConstantsOfDriver.EMAIL,rs.getString("eEmail"));
							responseMap.put(ResponseConstantsOfDriver.AREA,rs.getString("eArea"));
							responseMap.put(ResponseConstantsOfDriver.STREET,rs.getString("eStreet"));
							responseMap.put(ResponseConstantsOfDriver.BUILDING,rs.getString("eBuilding"));
							responseMap.put(ResponseConstantsOfDriver.LANDMARK,rs.getString("eLandmark"));
							responseMap.put(ResponseConstantsOfDriver.CITY,rs.getString("eCity"));
							responseMap.put(ResponseConstantsOfDriver.POSTBOX,rs.getString("ePOBox"));
							responseMap.put(ResponseConstantsOfDriver.COUNTRY,rs.getString("eCountryISO"));

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to insert Previous Experience details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> insertExperienceInfo(ArrayList<Driver> driverDetailsList) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_EXPERIENCE_DETAILS_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
			{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
					try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
							if(isSuccess){ 
								responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_SAVED);
								responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
								String insertedIds = rs.getString("insertedIds");
								List<String> insertedIdsArray = new ArrayList<String>();
								if(insertedIds.length()>0)
									insertedIdsArray = Arrays.asList(insertedIds.split(","));
								
									
								responseMap.put("experienceIds", insertedIdsArray);
								
							}else{
								responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_FAILED);
								responseMap.put(CommonResponseConstants.ISSUCCESS, false);
							}
						
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
			});

		//IN parameters for stored procedure
		String[] inParamaters = {"idriversArray","iexperienceCount"};
		Object[] inParamaterValues = {commonUtil.buildPreviousExperienceArray(driverDetailsList),driverDetailsList.size()};
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

		}


	/**
	 * This method is used to update Previous Experience details of Driver in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> updateExperienceInfo(ArrayList<Driver> driverDetailsList) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		int driverId = 0;
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_EXPERIENCE_DETAILS_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
						try{
							Map<String,Object> responseMap = new HashMap<String, Object>();
							boolean isSuccess = rs.getBoolean("issuccess");
							System.out.print("is success value"+isSuccess);
								if(isSuccess){ 
									responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_UPDATED);
									responseMap.put(CommonResponseConstants.ISSUCCESS, true);
									
									String insertedIds = rs.getString("experienceIds");
									List<String> insertedIdsArray = new ArrayList<String>();
									if(insertedIds.length()>0)
										insertedIdsArray = Arrays.asList(insertedIds.split(","));
									
										
									responseMap.put("experienceIds", insertedIdsArray);
									
									
								}else{
									responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_UPDATE_FAILED);
									responseMap.put(CommonResponseConstants.ISSUCCESS, false);
								}
							
							return responseMap;	
						}
						catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
						}
					
			}
			
				});

		
		//IN parameters for stored procedure
		String[] inParamaters = {"idriversArray","iexperienceCount","iDriverId"};
		if(driverDetailsList.size() > 0){
		 driverId= driverDetailsList.get(0).getDriverId();
		}
		Object[] inParamaterValues = {commonUtil.buildPreviousExperienceUpdateArray(driverDetailsList),driverDetailsList.size(),driverId};
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to fetch Experience details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchExperienceInfo(
			Driver driverDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_EXPERIENCE_DETAILS_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			//iDriverID,iTotalExperience,iRelatedExperience,iCompanyName,iDesignation,iDoj,iDor,Now(),iuserID
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID,rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.TOTAL_WORKEXPERIENCE,rs.getString("totalExperience"));
							responseMap.put(ResponseConstantsOfDriver.RELATED_WORKEXPERIENCE,rs.getString("relatedExpirence"));
							responseMap.put(ResponseConstantsOfDriver.COMPANY_NAME,rs.getString("companyName"));
							responseMap.put(ResponseConstantsOfDriver.PREVIOUS_DESIGNATION,rs.getString("designation"));
							responseMap.put(ResponseConstantsOfDriver.DATE_OF_JOINING,rs.getString("doj"));
							responseMap.put(ResponseConstantsOfDriver.DATE_OF_RELIEVING,rs.getString("dor"));
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
	/**
	 * This method is used to insert Present Experience details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> insertPresentExperienceInfo(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PRESENT_EXPERIENCE_DETAILS_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
					}
			}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverID","iEmployeeId","iDateOfJoin","iFormattedDateOfJoin","iDesignation","iWorkingShiftID","iPreferredDayOffID","iReportingTo","iShirtSize","iShoeSize","iTrouserSize",
				"iShoesReceived","iShirtsReceived","iTrousersReceived","iPreferedVehicleID","iUserName","isValid","startDay","noOfDays"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.getEmployeeId(),driverDetails.getDateOfJoining(),driverDetails.getFormattedDateOfJoining(),driverDetails.getPresentdesignation(),driverDetails.getPreferedWorkTimings(),
				driverDetails.getPreferedDayOff(),driverDetails.getReportingTo(),driverDetails.getShirtSize(),driverDetails.getShoeSize(),driverDetails.getTrouserSize(),driverDetails.getNoOfShoeReceived(),
				driverDetails.getNoOfShirtsReceived(),driverDetails.getNoOfTrousersReceived(),driverDetails.getPreferedTruckId(),driverDetails.getUserName(),1,new UtilFunctions().getDay(UtilFunctions.getCalenderObject(driverDetails.getFormattedDateOfJoining())),new UtilFunctions().getNoOfDaysLeftInMonth(UtilFunctions.getCalenderObject(driverDetails.getFormattedDateOfJoining()))};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to update Present Experience details of Driver in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> updatePresentExperienceInfo(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PRESENT_EXPERIENCE_DETAILS_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iDriverID","iEmployeeId","iDateOfJoin","iDesignation","iWorkingShiftID","iPreferredDayOffID","iReportingTo","iShirtSize","iShoeSize","iTrouserSize",
				"iShoesReceived","iShirtsReceived","iTrousersReceived","iPreferedVehicleID","iUserName"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.getEmployeeId(),driverDetails.getDateOfJoining(),driverDetails.getPresentdesignation(),driverDetails.getPreferedWorkTimings(),
				driverDetails.getPreferedDayOff(),driverDetails.getReportingTo(),driverDetails.getShirtSize(),driverDetails.getShoeSize(),driverDetails.getTrouserSize(),driverDetails.getNoOfShoeReceived(),
				driverDetails.getNoOfShirtsReceived(),driverDetails.getNoOfTrousersReceived(),driverDetails.getPreferedTruckId(),driverDetails.getUserName()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to fetch Present Experience details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> fetchPresentExperienceInfo(
			Driver driverDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_PRESENT_EXPERIENCE_DETAILS_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID,rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.EMPLOYEEID,rs.getInt("employeeId"));
							responseMap.put(ResponseConstantsOfDriver.REPORTING_TO,rs.getString("reportingTo"));
							responseMap.put(ResponseConstantsOfDriver.PREFERED_DAYOFF,rs.getString("preferredDayOffID"));
							responseMap.put(ResponseConstantsOfDriver.TROUSER_SIZE,rs.getString("trouserSize"));
							responseMap.put(ResponseConstantsOfDriver.SHIRT_SIZE,rs.getString("shirtSize"));
							responseMap.put(ResponseConstantsOfDriver.SHOE_SIZE,rs.getString("shoeSize"));
							
							responseMap.put(ResponseConstantsOfDriver.NO_OF_SHIRTS_RECEIVED,rs.getString("shirtsReceived"));
							responseMap.put(ResponseConstantsOfDriver.NO_OF_SHOE_RECEIVED,rs.getString("shoesReceived"));
							responseMap.put(ResponseConstantsOfDriver.NO_OF_TROUSERS_RECEIVED,rs.getString("trousersReceived"));
							responseMap.put(ResponseConstantsOfTruck.TRUCKID,rs.getString("preferedVehicleID"));
							
							
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}



	/**
	 * This method is used to get the list of Available Drivers from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getDriverShiftInfo() {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFTINFO_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.LICENSENO_EXPIRED_AT, rs.getString("licenseNoExpiredAt"));
							/*responseMap.put(ResponseConstantsOfTruck.TRUCKNAME, rs.getString("TruckName"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_STATUS, rs.getString("driverStatus"));*/
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverFirstName"));
							responseMap.put(ResponseConstantsOfDriver.PHONE, rs.getString("phone"));
							/*responseMap.put(ResponseConstantsOfDriver.SHIFT_NAME, rs.getString("ShiftName"));*/

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
							responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						}

						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	
	/**
	 * This method is used to get the list of Available Drivers from database and return the success or failure information
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getAllDrivers() {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_INFO_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							String licenseNoExpiredAt = rs.getString("licenseNoExpiredAt");
							responseMap.put(ResponseConstantsOfDriver.ID, id++);
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.LICENSENO_EXPIRED_AT, licenseNoExpiredAt);
							/*responseMap.put(ResponseConstantsOfTruck.TRUCKNAME, rs.getString("TruckName"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_STATUS, rs.getString("driverStatus"));*/
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							responseMap.put(ResponseConstantsOfDriver.PHONE, rs.getString("phone"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_NAME, rs.getString("shiftName"));
							 responseMap.put("lastcaseAttendedTime", rs.getString("lastcaseAttendedTime"));
							int licenceExpiryCode= commonUtil.dateComparator(licenseNoExpiredAt);
							System.out.println(licenseNoExpiredAt +"  "+licenceExpiryCode);
							responseMap.put(ResponseConstantsOfTruck.COLOR_CODE, commonUtil.getColorCodeForLicenceExpire(licenceExpiryCode));
							
						}else{
							//responseMap.put(CommonResponseConstants.ISSUCCESS, false);
							//responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_FOUND);
							return null;
						}

						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
					}
			}
				});

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute();
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
		id = 1;
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	
	/**
	 * This method is used to  delete the Driver Information from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> deleteDriverInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_INFO_DELETE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_DETAILS_DELETED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
				}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"idriverId"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);	
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}


	
	
	/**
	 * This method is used to get the list of Available Drivers with respective shift schedules from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getShiftSchedule(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_SCHEDULE_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.LICENSENO_EXPIRED_AT, rs.getString("licenseNoExpiredAt"));
							responseMap.put(ResponseConstantsOfTruck.TRUCKCODE, rs.getString("TruckCode"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_STATUS, rs.getString("driverStatus"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							responseMap.put(ResponseConstantsOfDriver.PHONE, rs.getString("phone"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_NAME, rs.getString("ShiftName"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_SCHEDULED_ON, rs.getString("ScheduledOn"));

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
							responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						}

						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iScheduleStartOn","iScheduleEndOn"};
		Object[] inParamaterValues = {driverDetails.getScheduledOn(),driverDetails.getScheduleEndOn()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);	
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to  save the Driver Shift Information in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> saveDriverShiftInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INFO_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						/*if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_SAVED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_SAVE_FAILED);
						}*/
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iShiftID","iDriverID","iScheduledOn","iScheduleTypeID","iScheduleStatusID","iuserID"};
		Object[] inParamaterValues = {driverDetails.getShiftId(),driverDetails.getDriverId(),driverDetails.getScheduledOn(),driverDetails.getScheduleTypeID(),driverDetails.getScheduleStatusID(),driverDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);	
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to  update the Driver Shift Information in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> updateDriverShiftInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INFO_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_UPDATED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iShiftID","iShiftScheduleID","iDriverID","iScheduledOn","iScheduleTypeID","iScheduleStatusID","iuserID"};
		Object[] inParamaterValues = {driverDetails.getShiftId(),driverDetails.getShiftScheduleId(),driverDetails.getDriverId(),driverDetails.getScheduledOn(),driverDetails.getScheduleTypeID(),driverDetails.getScheduleStatusID(),driverDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);	
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	/**
	 * This method is used to  delete the Driver Shift Information from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> deleteDriverShiftInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INFO_DELETE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_DELETED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iShiftScheduleID"};
		Object[] inParamaterValues = {driverDetails.getShiftScheduleId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);	
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}




	/**
	 * This method is used to  get Shift In Out Timings of Driver from  database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getShiftINOUTTimmingsOfDriver(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INOUT_INFO_FETCH_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{

						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess)
						{ 
							responseMap.put(ResponseConstantsOfDriver.PUNCHID, rs.getInt("punchID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_ID, rs.getInt("shiftId"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_IN_TIME, rs.getString("punchIn"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_OUT_TIME, rs.getString("punchOut"));
						}else
						{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iScheduledOn"};
		Object[] inParamaterValues = {driverDetails.getScheduledOn()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to insert shift InOut details of Driver into database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> insertShiftInOutInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INOUT_INFO_SAVE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					if(isSuccess){ 
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_PUNCH_DETAILS_SAVED);
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PUNCH_DETAILS_FAILED );
					}
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iUserID","iDriverID","iPunchIn"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getDriverId(),driverDetails.getPunchInTime()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}


	/**
	 * This method is used to update Shift InOut details of Driver in database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public List<HashMap<String, Object>> updateShiftInOutInfo(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INOUT_INFO_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_PUNCH_DETAILS_UPDATED);
						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});


		//IN parameters for stored procedure
		String[] inParamaters = {"iUserID","iPunchOut","iPunchID"};
		Object[] inParamaterValues = {driverDetails.getUserId(),driverDetails.getPunchOutTime(),driverDetails.getPunchId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to  get Shift In Out Timings of Driver By ShiftId from  database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getShiftINOUTTimmingsOfDriverByShiftId(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INOUT_INFO_FETCH_BY_SHIFTID_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{

						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess)
						{ 
							responseMap.put(ResponseConstantsOfDriver.PUNCHID, rs.getInt("punchID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_ID, rs.getInt("shiftId"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_IN_TIME, rs.getString("punchIn"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_OUT_TIME, rs.getString("punchOut"));
						}else
						{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iShiftID","iScheduledOn"};
		Object[] inParamaterValues = {driverDetails.getShiftId(),driverDetails.getScheduledOn()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

	/**
	 * This method is used to  get Shift In Out Timings of Driver By Driver Details from  database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> getShiftINOUTTimmingsOfDriverByDetails(
			Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.DRIVER_SHIFT_INOUT_INFO_FETCH_BY_DETAILS_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{

						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess)
						{ 
							responseMap.put(ResponseConstantsOfDriver.PUNCHID, rs.getInt("punchID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							responseMap.put(ResponseConstantsOfDriver.SHIFT_ID, rs.getInt("shiftId"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_IN_TIME, rs.getString("punchIn"));
							responseMap.put(ResponseConstantsOfDriver.PUNCH_OUT_TIME, rs.getString("punchOut"));
						}else
						{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
						}
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
					}
			}
				});
		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID","iDriverName","iScheduledOn"};
		Object[] inParamaterValues = {driverDetails.getDriverId(),driverDetails.getDriverName(),driverDetails.getScheduledOn()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}
	
	

	/**
	 * This method is used to get the list of Available Drivers from database and return the success or failure information
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	
	@SuppressWarnings("unchecked")
	public List<Object>  getFullDriverInfoById(Driver driverDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.GET_FULL_DRIVER_INFO).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{

						Map<String,Object> responseMap = new HashMap<String, Object>();
							responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put("driverFirstName", rs.getString("driverFirstName"));
							responseMap.put("driverLastName", rs.getString("driverLastName"));
							responseMap.put("licenseNo", rs.getString("licenseNo"));
							responseMap.put("licenseNoExpiredAt", rs.getString("licenseNoExpiredAt"));
							responseMap.put("passportNo", rs.getString("passportNo"));
							responseMap.put("passportExpiredAt", rs.getString("passportExpiredAt"));
							responseMap.put("passportIssuedAt", rs.getString("passportIssuedAt"));
							responseMap.put("governmentIdNumber", rs.getString("governmentIdNumber"));
							responseMap.put("nationality", rs.getString("nationality"));
							responseMap.put("visaNo", rs.getString("visaNo"));
							responseMap.put("visaIssuedAt", rs.getString("visaIssuedAt"));
							responseMap.put("visaExpiredAt", rs.getString("visaExpiredAt"));
							responseMap.put("healthInsuranceCardNo", rs.getString("healthInsuranceCardNo"));
							responseMap.put("healthInsuranceCardExpiryAt", rs.getString("healthInsuranceCardExpiryAt"));
							responseMap.put("phone", rs.getString("phone"));
							responseMap.put("email", rs.getString("email"));
							responseMap.put("area", rs.getString("area"));
							responseMap.put("street", rs.getString("street"));
							responseMap.put("building", rs.getString("building"));
							responseMap.put("landmark", rs.getString("landmark"));
							responseMap.put("city", rs.getString("city"));
							responseMap.put("postBox", rs.getString("postBox"));
							responseMap.put("countryISO", rs.getString("countryISO"));
							responseMap.put("country", rs.getString("country"));
							
							responseMap.put("eContactName", rs.getString("eContactName"));
							responseMap.put("ePhone", rs.getString("ePhone"));
							responseMap.put("eEmail", rs.getString("eEmail"));
							responseMap.put("eArea", rs.getString("eArea"));
							responseMap.put("eStreet", rs.getString("eStreet"));
							responseMap.put("eBuilding", rs.getString("eBuilding"));
							responseMap.put("eLandmark", rs.getString("eLandmark"));
							responseMap.put("eCity", rs.getString("eCity"));
							responseMap.put("ePostBox", rs.getString("ePostBox"));
							responseMap.put("eCountryISO", rs.getString("eCountryISO"));
							responseMap.put("eCountry", rs.getString("eCountry"));
							
							responseMap.put("employeeId", rs.getString("employeeId"));
							responseMap.put("presentDateOfJoining", rs.getString("presentDateOfJoining"));
							responseMap.put("presentdesignation", rs.getString("presentdesignation"));
							responseMap.put("preferedShiftTimings", rs.getInt("preferedShiftTimings"));
							responseMap.put("preferedWorkTimings", rs.getInt("preferedWorkTimings"));
							responseMap.put("preferedDayOff", rs.getInt("preferedDayOff"));
							responseMap.put("reportingManager", rs.getString("reportingManager"));
							responseMap.put("reportingTo", rs.getInt("reportingTo"));
							responseMap.put("userName", rs.getString("userName"));
							responseMap.put("reportingUserId", rs.getString("reportingUserId"));
							responseMap.put("shirtSize", rs.getString("shirtSize"));
							responseMap.put("shoeSize", rs.getString("shoeSize"));
							responseMap.put("trouserSize", rs.getString("trouserSize"));
							responseMap.put("noOfShoeReceived", rs.getString("noOfShoeReceived"));
							responseMap.put("noOfShirtsReceived", rs.getString("noOfShirtsReceived"));
							responseMap.put("noOfTrousersReceived", rs.getString("noOfTrousersReceived"));
							responseMap.put("preferedTruck", rs.getString("preferedTruck"));
							responseMap.put("preferedTruckId", rs.getInt("preferedTruckId"));
							responseMap.put("truckDriverId", rs.getInt("truckDriverId"));
						
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
					}
			}
				}).returningResultSet("driverPrevExpResult", new RowMapper<Map<String,Object>>()
						{
					@Override
					public Map<String,Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {try{

								Map<String,Object> responseMap = new HashMap<String, Object>();
								
									responseMap.put("exprerienceID", rs.getString("exprerienceID"));
									responseMap.put("totalExperience", rs.getString("totalExperience"));
									responseMap.put("relatedExpirence", rs.getString("relatedExpirence"));
									responseMap.put("companyName", rs.getString("companyName"));
									responseMap.put("designation", rs.getString("designation"));
									responseMap.put("doj", rs.getString("doj"));
									responseMap.put("dor", rs.getString("dor"));
									
								return responseMap;	
							}
							catch(SQLException ex){
								return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
							}
					}
						});
		//IN parameters for stored procedure
		String[] inParamaters = {"iDriverID"};
		Object[] inParamaterValues = {driverDetails.getDriverId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> driverPrevExpResultList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("driverPrevExpResult");
		
		//HashMap<String, Object> driverDetailsMap = new HashMap<String, Object>();	
		List<Object> responseList = new ArrayList<Object>();
		/*if(returnjson!=null && returnjson.size()>0){
			driverDetailsMap.putAll(returnjson.get(0));
		}*/
		
		responseList.add(returnjson);
		responseList.add(driverPrevExpResultList);
		return responseList;

	}


}
