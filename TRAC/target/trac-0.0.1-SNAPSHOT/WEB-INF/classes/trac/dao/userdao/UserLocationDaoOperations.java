package trac.dao.userdao;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.userbean.UserLocation;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;

/**
 *  User Location related database Operations are handled in this class
 *  
 */

public class UserLocationDaoOperations {
	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	/**
	 * This Method is used to add the userLocation details of registered user.
	 * it returns userLocations object contains all related info  of user.
	 * 
	 * @author rpenta
	 * @param  userId
	 * @spname usp_Location_Locations_Insert
	 * @return true or false
	 */
	public List<HashMap<String, Object>> addUserLocation(UserLocation userLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USERLOC_REGISTRATION_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
						Map<String,Object> userLocationMap = new HashMap<String, Object>();
		
						System.out.print("is map");
						System.out.print("is success value"+rs.getBoolean("issuccess"));
						if(rs.getBoolean("issuccess")){ 
							userLocationMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location added successfully");
							userLocationMap.put(CommonResponseConstants.ISSUCCESS, true);
							userLocationMap.put(CommonResponseConstants.USERID, rs.getString("userID"));
							userLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getString("locationID"));
							userLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
							userLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
							userLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
						}else{
							userLocationMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location adding failed");
							userLocationMap.put(CommonResponseConstants.ISSUCCESS, false);
						}
						return userLocationMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iLatitude","iLongitude","iLocationName","iaddress","icity","istate","icountryISO"};
		Object[] inParamaterValues = {userLocationDetails.getUserId(),userLocationDetails.getUserLocationLatitude(),userLocationDetails.getUserLocationLongitude(),userLocationDetails.getUserLocationName(),userLocationDetails.getDetailAddress(),userLocationDetails.getUserLocationCity(),userLocationDetails.getUserLocationState(),userLocationDetails.getUserLocationCountryCode()};
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		
			return returnjson;
	}


	/**
	 * This method is used to update the userLocation details of existing user.
	 * it returns userLocations object contains all related info  of user.
	 * 
	 * @param userLocationDetails
	 * @spname usp_Location_Locations_Update
	 * @return true or false
	 */
	public List<HashMap<String, Object>> updateUserLocation(UserLocation userLocationDetails) {
		
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USERLOC_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();	
		
						boolean isSuccess = (boolean)rs.getBoolean("issuccess");
							
						if(isSuccess){
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location updated successfully");
							responseMap.put(CommonResponseConstants.ISSUCCESS, true);

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location updation failed");
							responseMap.put(CommonResponseConstants.ISSUCCESS, false);

						}
						return responseMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","iLatitude","iLongtitude","iLocationName","iaddress","iLocID","icity","istate","icountryISO"};
		Object[] inParamaterValues = {userLocationDetails.getUserId(),userLocationDetails.getUserLocationLatitude(),userLocationDetails.getUserLocationLongitude(),userLocationDetails.getUserLocationName(),userLocationDetails.getDetailAddress(),userLocationDetails.getLocationId(),userLocationDetails.getUserLocationCity(),userLocationDetails.getUserLocationState(),userLocationDetails.getUserLocationCountryCode()};		
		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
		
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		
			return returnjson;
	

	}
	
	
	/** 
	 * This Method is used to getLocationDetails of Existing User.
	 *  if true userlocation plain java object contains fetched user info or false
	 * 
	 * @param userID 
	 * @spname usp_Location_Locations_Select
	 * @return locationID , Plain userlocations java Object
	 *
	 * 
	 */
	public Object getUserLocation(UserLocation userLocationDetails) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall  simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USERLOC_FETCH_SPNAME)
		.withoutProcedureColumnMetaDataAccess()
		.declareParameters(new SqlParameter("iuserID",java.sql.Types.INTEGER))
		.returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum){
				try{
					Map<String,Object> userLocationMap = new HashMap<String, Object>();
	
					userLocationMap.put(ResponseConstantsOfUserLocation.USERLOCATIONLATITUDE, (rs.getString("GPSLocationLat") == null) ? "" : rs.getString("GPSLocationLat"));
					userLocationMap.put(ResponseConstantsOfUserLocation.USERLOCATIONLONGITUDE, (rs.getString("GPSLocationLong") == null) ? "" : rs.getString("GPSLocationLong"));
					userLocationMap.put(ResponseConstantsOfUserLocation.USERLOCATIONNAME, (rs.getString("Description") == null) ? "" : rs.getString("Description"));
					userLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS,  (rs.getString("address") == null) ? "" : rs.getString("address"));
					userLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID,(rs.getString("locationID") == null) ? "" : rs.getString("locationID"));
					userLocationMap.put(ResponseConstantsOfUserLocation.CITY, (rs.getString("city") == null) ? "" : rs.getString("city"));
					userLocationMap.put(ResponseConstantsOfUserLocation.STATE, (rs.getString("state") == null) ? "" : rs.getString("state"));
					userLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, (rs.getString("countryName") == null) ? "" : rs.getString("countryName"));
					userLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYCODE, (rs.getString("countryCode") == null) ? "" : rs.getString("countryCode"));
	
					return userLocationMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			    }
			}
		});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userLocationDetails.getUserId()};
		
		//Execute stopred procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
		
		System.out.println("userLocations - "+simpleJdbcCallResult);
		System.out.println("userLocations - "+simpleJdbcCallResult.get("returnedResult"));
		return simpleJdbcCallResult.get("returnedResult");

	}

	/** *unregister user Location Info * 
	 * *@param userLocationDetails 
	 * *@spname usp_Location_Locations__Delete 
	 * * @return 1 for success or 0 for failure */ 
	public List<HashMap<String, Object>> deleteLocationInfo(UserLocation userLocationDetails) { 
		//getting jdbc connection 
		jdbcTemplate = JDBCConnection.getJdbcTemplate(); 
		System.out.println("jdbcTemplate - "+jdbcTemplate); 
		//registering jdbc connection and stored procedure to execute 
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).
				withProcedureName(SPNameConstants.USERLOC_UNREGISTER_SPNAME).
				returningResultSet("locationunregister", new RowMapper<HashMap<String, Object>>() { 
					@Override public HashMap<String, Object> mapRow(ResultSet rs, int rowNum) { 
						try{ 
							HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage")); 
							responseMap.put(CommonResponseConstants.ISSUCCESS, rs.getBoolean("issuccess")); 
							return responseMap; 
						}catch(SQLException ex){ 
							return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime"); 
							} 
						} 
					}); 
		//IN parameters for stored procedure 
		String[] inParamaters = {"iLocID","iuserID"}; 
		Object[] inParamaterValues = {userLocationDetails.getLocationId(),userLocationDetails.getUserId()}; 
		// executing stored procedure 
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall); 
		System.out.println("locationunregister"+simpleJdbcCallResult.get("locationunregister")); 
		return (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationunregister"); 
	}
	

}
