package trac.oauth.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class CustomUserAuthenticationToken extends AbstractAuthenticationToken{
	private static final long serialVersionUID = -1092219614309982278L;
    private final Object principal;
    private Object credentials;
 
    /**
     * This method is used to generate the token for user register first time.
     * @param principal
     * @param credentials
     * @param authorities
     * @return token
     */
    public CustomUserAuthenticationToken(Object principal, Object credentials,
            Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        super.setAuthenticated(true);
    }
 
    public Object getCredentials() {
        return this.credentials;
    }
 
    public Object getPrincipal() {
        return this.principal;
    }
}
