package trac.constants.cloudconstants;

/**
 *  Constants for file Bucket Names 
 *
 */
public class CloudConstantsInfo {

	 public static final String USER_BUCKETNAME ="userprofileimages";
	 public static final String VEHICLE_BUCKETNAME ="vehicleregisteredpics";

}
