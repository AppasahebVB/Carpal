package trac.constants.resturlconstants;

public class RestUrlConstantsOfRoadAssistance {

	/**
	 * This Class is used to place the All RestUrlConstantsOf casesBean
	 * @author gatla
	 *
	 */
	public static final String REGISTER_CASE = "/roadassistance/registercase";
	public static final String FETCH_CASE_INFO = "/roadassistance/fetchcase";
	public static final String FETCH_LIST_OF_SERVICES = "/roadassistance/listofservices";
	public static final String UPDATE_CASE_STATUS = "/roadassistance/updatecasestatus";

	public static final String FETCH_CASE_FAULTS_SERVICES = "/caseRegistration/fetchcasefaultservices";

	public static final String REGISTER_CUSTOMER_CASE = "/caseRegistration/saveCaseInfo";
	public static final String SEARCH_CUSTOMER_DETAILS = "/customer/search";
	public static final String SEARCH_VEHICLE_DETAILS = "/vehicle/search";
	
	public static final String SAVE_CASE_COMMENT = "/case/saveComment";
	public static final String FETCH_CUSTOMER_CASE_INFO = "/caseRegistration/fetchCaseInfo";
	public static final String EDIT_CUSTOMER_CASE_INFO = "/caseRegistration/editCaseInfo";

	public static final String FETCH_NEAREST_AVAILABLE_DRIVERS ="/assigningCase/fetchNearestAvailableDrivers";
	public static final String ASSIGNCASE_TO_DRIVERS ="/assigningCase/assigningCaseToDriver";

	public static final String SAVE_CASE_PREREQUISITE = "/preRequestieJobDetails/addPreRequestieJobDetails";
	public static final String CASE_ADD_ON_JOB = "/caseOnJob/addOnJobDetails";

	public static final String LIST_SCHEDULE_CASES = "/caseSchedule/listAllCases";
	public static final String UPDATE_CASE_ARRIVAL_TIME = "/caseSchedule/updateCaseArrivalTime";

	public static final String LIST_ALL_CASES = "/cases/listAllCases";

	public static final String FETCH_CUSTOMER_CASE_DETAILS_INFO = "/cases/fetchCaseDetailedInfo";
	
	
	public static final String LIST_ALL_CASES_IN_QUE = "/cases/listAllCasesinQue";//userId
	public static final String ASSIGN_CASE_TO_SELF = "/cases/assigncasetoself";//userId,caseId
	
	public static final String FETCH_CASES_COUNT = "/cases/casescount";
	public static final String UPDATE_FAULT = "/cases/updatefault";
}
