package trac.constants.resturlconstants;

/**
 *  report services url paths
 *
 */

public class RestUrlConstantOfDashboard {
	public static final String REPORT_INFO = "/dashboard/report";
	public static final String REPORT_METADATA = "/dashboard/reportmetadata";
	public static final String REPORT_DATA = "/dashboard/reportdata";
	public static final String REPORT_SAVE_DATA = "/dashboard/savereport";
	public static final String REPORT_FETCH_DATA = "/dashboard/fetchreport";
	public static final String REPORT_UPDATE_DATA = "/dashboard/updatereport";
	public static final String REPORT_DELETE_DATA = "/dashboard/deletereport";
}
