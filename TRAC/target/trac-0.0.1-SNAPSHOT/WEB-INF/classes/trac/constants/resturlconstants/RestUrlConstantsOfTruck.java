package trac.constants.resturlconstants;

/**
 *  truck services url paths
 *
 */

public class RestUrlConstantsOfTruck {

	public static final String REGISTER_TRUCK_INFO = "/truck/register";
	public static final String UPDATE_TRUCK_INFO= "/truck/update";
	public static final String GET_TRUCK_INFO= "/truck/gettruckinfo";
	public static final String GET_TRUCK_BRANDS= "/truck/gettruckbrands";
	public static final String ACTIVATE_BY_DEACTIVATE_TRUCK_INFO= "/truck/activation";
	public static final String UNREGISTER_TRUCK_INFO= "/truck/unregistertruck";
	public static final String FETCH_SERVICES_TRUCK_BRANDS= "/truck/getservicesandbrands";
	
	public static final String REGISTER_TRUCK_EXPENSE_INFO = "/truck/expenseregister";
	public static final String UPDATE_TRUCK_EXPENSE_INFO= "/truck/expenseupdate";
	public static final String GET_TRUCK_EXPENSE_INFO= "/truck/gettruckexpenseinfo";
}
