package trac.services.controllers.report;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trac.beans.userbean.Users;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.resturlconstants.RestUrlConstantsOfReports;
import trac.dao.reportsdao.ReportsSqlOperations;
import trac.services.controllers.driver.DriverController;
import trac.services.interfaces.report.ReportsInterface;

@Controller
public class ReportsController implements ReportsInterface{

	private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
	public ReportsSqlOperations reportdaoOperations = new ReportsSqlOperations();
	
	/**
	 * All Operations Specific to a case
	 * URL: /reports/operations
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfReports.OPERATIONS_REPORT_URL, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public HashMap<String, Object> getOperationsReport(@RequestBody Users userDetails) {
		logger.info("IN "+ RestUrlConstantsOfReports.OPERATIONS_REPORT_URL + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = reportdaoOperations.fetchOpertaionsData(userDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			
			if(resultMap!=null && (boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
			resultMap = new HashMap<String, Object>();	
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfCaseDetails.CASES_LIST,responseListMap);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Data Found.");
		}

		return resultMap;
	}

	/**
	 *  Attendance Report of Drivers
	 * URL: /reports/attendance
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfReports.ATTENDANCE_REPORT_URL, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public HashMap<String, Object> getAttendanceReport(@RequestBody Users userDetails) {
		logger.info("IN "+ RestUrlConstantsOfReports.ATTENDANCE_REPORT_URL + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = reportdaoOperations.fetchAttendanceData(userDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			if(resultMap!=null && (boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS,responseListMap);}
			
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Drivers Found.");
		}

		return resultMap;
	}

	/**
	 *  Trucks Information Report
	 * URL: /reports/trucks
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfReports.TRUCKS_REPORT_URL, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public HashMap<String, Object> getTruckeReport(@RequestBody Users userDetails) {
		logger.info("IN "+ RestUrlConstantsOfReports.TRUCKS_REPORT_URL + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = reportdaoOperations.fetchTrucksData(userDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			if(resultMap!=null && (boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfTruck.TRUCKLIST,responseListMap);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Trucks Found.");
		}

		return resultMap;
	}

	/**
	 * All Expenses related to Truck
	 * URL:/reports/expenses
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfReports.EXPENSES_REPORT_URL, method = RequestMethod.POST,consumes="application/json",produces="application/json")

	public HashMap<String, Object> getExpensesReport(Users userDetails) {
		// TODO Auto-generated method stub
		return null;
	}

}
