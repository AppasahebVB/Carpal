package trac.services.controllers.roadassistance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.resturlconstants.RestUrlConstantsOfRoadAssistance;
import trac.dao.casesdao.CaseRelatedSqlOperations;
import trac.services.interfaces.roadassistance.CaseRegisterInterface;

@Controller
public class CaseRegisterImplController implements CaseRegisterInterface {

	private static final Logger logger = LoggerFactory.getLogger(CaseRegisterImplController.class);
	private CaseRelatedSqlOperations sqlOperations = new CaseRelatedSqlOperations();


	/**
	 * This Function is Used To Registered case Details of Registered User.
	 * URL: /roadassistance/registercase
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.REGISTER_CASE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerCaseDetails(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.REGISTER_CASE + " service");
		
		List<HashMap<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap;
		try {
			responseList = sqlOperations.registerCaseInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(responseList!=null && responseList.size()>0){
			responseMap = responseList.get(0);
		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Case Registration Failed.");
		
		}
		return responseMap;
		
	}

	/**
	 * This Function is Used To Fetch case Details of Registered Vehicle.
	 * URL: /roadassistance/fetchcase
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CASE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchCaseDetails(@RequestBody CaseDetails caseDetails) {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_CASE_INFO + " service");
		List<HashMap<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getCaseInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(responseList!=null && responseList.size()>0){
			responseMap.put(ResponseConstantsOfCaseDetails.SERVICEHISTORY,responseList); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Cases Registered");
		}
		
		return responseMap;
	}

	/**
	 * This Function is Used To Fetch list of services for mobile users.
	 * URL: /roadassistance/listofservices
	 * METHOD: GET
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_LIST_OF_SERVICES, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody List<Map<String, Object>> fetchServicesList() {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_LIST_OF_SERVICES + " service");
		List<Map<String, Object>> servicesList = sqlOperations.getServiceList();
		
		// TODO Auto-generated method stub
		return servicesList;
	}
	
	
	/**
	 * This Function is Used To update case status of Registered User.
	 * URL: /roadassistance/updatecasestatus
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.UPDATE_CASE_STATUS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateCaseStatus(@RequestBody CaseDetails caseDetails) {
		List<HashMap<String, Object>> responseList =null;
		HashMap<String, Object> responseMap;
		try {
			responseList = sqlOperations.updateCaseStatusById(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(responseList!=null && responseList.size()>0){
			responseMap = responseList.get(0);
		}else{
			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating case Failed.");
		}
		return responseMap;
	}

	
	

}
