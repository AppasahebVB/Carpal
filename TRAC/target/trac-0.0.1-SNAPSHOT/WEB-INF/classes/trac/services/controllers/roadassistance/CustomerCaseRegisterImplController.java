package trac.services.controllers.roadassistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.resturlconstants.RestUrlConstantsOfRoadAssistance;
import trac.dao.casesdao.CustomerCaseRelatedSqlOperations;
import trac.services.interfaces.roadassistance.CustomerCaseRegisterInterface;
import trac.util.CommonUtil;

@Controller
public class CustomerCaseRegisterImplController implements CustomerCaseRegisterInterface {

	private static final Logger logger = LoggerFactory.getLogger(CustomerCaseRegisterImplController.class);
	private CustomerCaseRelatedSqlOperations sqlOperations = new CustomerCaseRelatedSqlOperations();
	private CommonUtil commonUtil = new CommonUtil();

	/**
	 * This method is Used To Registered case Details of Registered Customer.
	 * URL: /caseRegistration/saveCaseInfo
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.REGISTER_CUSTOMER_CASE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerCustomerCaseDetails(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.REGISTER_CUSTOMER_CASE + " service");
		List<HashMap<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap;
		try {

			responseList = sqlOperations.registerCaseInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap = new HashMap<String, Object>();
			responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,responseList); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance registered successfully.");
		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance Registration Failed.");

		}
		return responseMap;
	}
	
	/**
	 * This method is Used To search customer details of Registered Customer.
	 * URL: /customer/autoCompleteSearch
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.SEARCH_CUSTOMER_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> searchForCustomer(@RequestBody Customer  customerDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.SEARCH_CUSTOMER_DETAILS + " service");
		List<Map<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap;
		try {

			responseList = sqlOperations.searchForCustomer(customerDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap = new HashMap<String, Object>();
			responseMap.put(ResponseConstantsOfCaseDetails.SEARCH_CUST_LIST,responseList); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			//responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance registered successfully.");
		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			//responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance Registration Failed.");

		}
		return responseMap;
	}
	
	
	/**
	 * This method is Used To search vehicle details of Registered Customer.
	 * URL: /customer/autoCompleteSearch
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.SEARCH_VEHICLE_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> searchForVehicle(@RequestBody Customer  customerDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.SEARCH_VEHICLE_DETAILS + " service");
		List<Map<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap;
		try {

			responseList = sqlOperations.searchForVehicle(customerDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap = new HashMap<String, Object>();
			responseMap.put(ResponseConstantsOfCaseDetails.SEARCH_VEHICLE_LIST,responseList); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			//responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance registered successfully.");
		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			//responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance Registration Failed.");

		}
		return responseMap;
	}

	/**
	 * This method is Used To save case comments.
	 * URL: /caseRegistration/saveCaseInfo
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.SAVE_CASE_COMMENT, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveCaseComment(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside registerCaseDetails");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.SAVE_CASE_COMMENT + " service");
		List<HashMap<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap;
		try {

			responseList = sqlOperations.saveCaseComment(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Comments saved successfully.");
			responseMap.put("commentsList", responseList);
		}else{
			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Comments Failed.");

		}
		return responseMap;
	}
	/**
	 * This method is Used To Fetch sub case Details of Customer .
	 * URL: /caseRegistration/fetchCaseInfo
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CUSTOMER_CASE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchCustomerCaseDetails(@RequestBody CaseDetails caseDetails) {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_CUSTOMER_CASE_INFO + " service");

		List<HashMap<String, Object>> responseList = null ;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getCaseInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,responseList); 
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Cases Registered");
		}

		return responseMap;
	}



	/**
	 * This method is Used To edit sub case information like adding caseregisterd services for a fault.
	 * URL: /caseRegistration/editCaseInfo
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.EDIT_CUSTOMER_CASE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateCustomerCaseDetails(@RequestBody HashMap<String, Object> requestBody) {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.EDIT_CUSTOMER_CASE_INFO + " service");
		List<HashMap<String, Object>> responseList =null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.updateCaseDetails((int)requestBody.get("subcaseId"),(String)requestBody.get("caseServiceDescription"),(int)requestBody.get("userId"),(ArrayList<LinkedHashMap<String, Object>>)requestBody.get("caseServicesList"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			if((boolean) responseList.get(0).get(CommonResponseConstants.ISSUCCESS)){
				responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,responseList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Assistance Updated Successfully.");
			}else{
				responseMap = responseList.get(0);
			}

		}else{

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating Assistance Failed.");
		}
		return responseMap;
	}



	/**
	 * This method is Used To Get All faults and services of cases.
	 * URL :/roadassistance/fetchcasefaultservices
	 * 
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CASE_FAULTS_SERVICES, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> caseFaultAndServices() {
		logger.info("IN " + RestUrlConstantsOfRoadAssistance.FETCH_CASE_FAULTS_SERVICES+ " service");
		// fetching case faults and service in TRAC DB
		List<HashMap<String, Object>> responseList = null;
		try {
			responseList = (List<HashMap<String, Object>>) sqlOperations.getCaseFaultAndServices();
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if (responseList != null && responseList.size() > 0) {

			ArrayList<Object> faultServicesList = commonUtil.processCaseFaultServices(responseList);

			returnObject.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICES_LIST,faultServicesList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		} else {
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"No Fautls or services found.");

		}
		return returnObject;

	}


	/**
	 * This method is Used To faetch the nearest avaialble drivers
	 * URL :/assigningCase/fetchNearestAvailableDrivers
	 * 
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_NEAREST_AVAILABLE_DRIVERS, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchAvailableDrivers(@RequestBody CaseDetails caseDetails) {
		logger.info("IN " + RestUrlConstantsOfRoadAssistance.FETCH_NEAREST_AVAILABLE_DRIVERS+ " service");
		// fetching case faults and service in TRAC DB
		List<HashMap<String, Object>> responseList = null;
		HashMap<String, Object> returnObject = new HashMap<String, Object>();
		String serviceLocationLatitude  = caseDetails.getServiceLocationLatitude();
		String serviceLocationLongitude = caseDetails.getServiceLocationLongitude();
		if(serviceLocationLatitude == null || serviceLocationLatitude.length()==0){
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Vehicle Latitude Location not found.");
		}

		else if(serviceLocationLongitude == null || serviceLocationLongitude.length()==0){
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Vehicle Longitude Location not found.");
		}
		else{
			try {
				responseList = (List<HashMap<String, Object>>) sqlOperations.getAvailableDrivers(caseDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (responseList != null && responseList.size() > 0) {
				List<HashMap<String, Object>> driversList = commonUtil.sortTrucksByDistance(serviceLocationLatitude,serviceLocationLongitude,responseList);
				if(driversList!=null && driversList.size()>0){
					returnObject.put(ResponseConstantsOfDriver.DRIVERS,driversList);
					returnObject.put(CommonResponseConstants.ISSUCCESS, true);
				}
				else{
					returnObject.put(CommonResponseConstants.ISSUCCESS, false);
					returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"No Drivers are Available.");
				}
			} else {
				returnObject.put(CommonResponseConstants.ISSUCCESS, false);
				returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"No Drivers are Available.");

			}
		}
		return returnObject;

	}
	/**
	 *Assign case to driver
	 * URL :/assigningCase/assigningCaseToDriver
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.ASSIGNCASE_TO_DRIVERS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> assignCaseToDriver(@RequestBody CaseDetails caseDetails){
		logger.info("IN " + RestUrlConstantsOfRoadAssistance.ASSIGNCASE_TO_DRIVERS+ " service");
		// fetching case faults and service in TRAC DB
		List<Object> responseMapList = null;
		try {
			responseMapList = (List<Object>) sqlOperations.assignCaseToDriver(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if (responseMapList != null && responseMapList.size() > 0) {
			returnObject = new HashMap<String, Object>();
			List<HashMap<String, Object>> assignList =  (List<HashMap<String, Object>>) responseMapList.get(0);
			List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseMapList.get(1);
			if(assignList!=null && assignList.size()>0){
			returnObject = assignList.get(0);
			returnObject.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO, subcasesList);
			}else{

				returnObject.put(CommonResponseConstants.ISSUCCESS, false);
				returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Assigning Case Failed.");

			
			}
			
			
		} else {
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE,"Assigning Case Failed.");

		}
		return returnObject;

	}


	/**
	 * This method is Used To save case Prerequisite details.
	 * URL: /preRequestieJobDetails/addPreRequestieJobDetails
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.SAVE_CASE_PREREQUISITE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveCasePrerequisite(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside saveCasePrerequisite");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.SAVE_CASE_PREREQUISITE + " service");
		List<Object> responseList = null ;
		HashMap<String, Object> responseMap =new HashMap<String, Object>();
		try {

			responseList = sqlOperations.insertCasePrerequisite(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){

			HashMap<String, Object> casePrerequisiteResult = ((List<HashMap<String, Object>>)responseList.get(0)).get(0);
			HashMap<String, Object> caseDetailsResult = ((List<HashMap<String, Object>>)responseList.get(1)).get(0);
			List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseList.get(2);
			List<HashMap<String, Object>> locationList =  (List<HashMap<String, Object>>) responseList.get(3);
			List<HashMap<String, Object>> casefaultsservicesList =  (List<HashMap<String, Object>>) responseList.get(4);

			if(casePrerequisiteResult.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Saving Case Prerequisite failed.");
			}
			else if(casePrerequisiteResult.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) casePrerequisiteResult.get(CommonResponseConstants.ISSUCCESS)){
				responseMap = casePrerequisiteResult;
			}
			else if(caseDetailsResult.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Case Not Found.");
			}else if(caseDetailsResult.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) caseDetailsResult.get(CommonResponseConstants.ISSUCCESS)){
				responseMap = caseDetailsResult;
			}else{
				responseMap = casePrerequisiteResult;
				responseMap.put("casedetails",caseDetailsResult);
				responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,subcasesList);
				responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLIST, locationList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				ArrayList<Object> faultServicesList = commonUtil.processCaseFaultServices(casefaultsservicesList);
				responseMap.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICES_LIST,faultServicesList);

			}


		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Case Prerequisite failed.");

		}
		return responseMap;
	}


	/**
	 * This method is Used To save case On Job details.
	 * URL: /caseOnJob/addOnJobDetails
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.CASE_ADD_ON_JOB, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveCaseOnJob(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside saveCasePrerequisite");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.CASE_ADD_ON_JOB + " service");
		List<Object> responseList = null ;
		HashMap<String, Object> responseMap = null;
		try {
			responseList = sqlOperations.insertCaseOnJob(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			//responseMap = responseList.get(0);
			List<HashMap<String, Object>> caseonJobResponseList =  (List<HashMap<String, Object>>) responseList.get(0);
			//List<HashMap<String, Object>> caseOnJobResult =  (List<HashMap<String, Object>>) responseList.get(0);
			List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseList.get(1);
			//responseMap.put("", caseOnJobResult);
			//responseMap.put("subcasesInfo", caseonJobresponseList);
			if(caseonJobResponseList != null && caseonJobResponseList.size()>0){
				responseMap = caseonJobResponseList.get(0);
				responseMap.put("subcasesInfo", subcasesList);
			}
		}else{

			responseMap = new HashMap<String, Object>();
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Case on job details failed.");

		}
		return responseMap;
	}

	/**
	 * method is Used To get case schedule details.
	 * URL: /caseOnJob/addOnJobDetails
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.LIST_SCHEDULE_CASES, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> getCaseScheduleInfo(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside saveCasePrerequisite");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.LIST_SCHEDULE_CASES + " service");
		List<Object> responseList = null ;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getCaseScheduleInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(responseList!=null && responseList.size()>0){
			List<HashMap<String, Object>> casesList =  (List<HashMap<String, Object>>) responseList.get(0);
			List<HashMap<String, Object>> casestatusList =  (List<HashMap<String, Object>>) responseList.get(1);
			List<HashMap<String, Object>> driverstatusesList =  (List<HashMap<String, Object>>) responseList.get(2);
			List<HashMap<String, Object>> casescountList =  (List<HashMap<String, Object>>) responseList.get(3);
			if(casesList.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Cases found.");
				responseMap.put("casescountresult", casescountList.get(0));
			}else{

				responseMap.put(ResponseConstantsOfCaseDetails.CASES_LIST,casesList);
				responseMap.put("casestatusList", casestatusList);
				responseMap.put("driverstatusesList", driverstatusesList);
				responseMap.put("casescountresult", casescountList.get(0));
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}

		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Fetching case schedules failed.");
		}
		return responseMap;
	}
	/**
	 * method is Used To update case schedule details.
	 * URL: /caseSchedule/updateCaseArrivalTime
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.UPDATE_CASE_ARRIVAL_TIME, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editCaseScheduleInfo(@RequestBody CaseDetails  caseDetails) {
		System.out.println("Inside editCaseScheduleInfo");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.UPDATE_CASE_ARRIVAL_TIME + " service");
		List<Object> responseList = null ;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			//ArrayList<LinkedHashMap<String,Object>> caseDetailsList=(ArrayList<LinkedHashMap<String,Object>>) caseDetails;
			responseList = sqlOperations.updateCaseSchedule(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(responseList!=null && responseList.size()>0){
			
			List<HashMap<String, Object>> returnedList =  (List<HashMap<String, Object>>) responseList.get(0);
			List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseList.get(1);
			//responseMap.put("", caseOnJobResult);
			if(returnedList != null && returnedList.size() > 0){
				responseMap = (HashMap<String, Object>) returnedList.get(0);
				responseMap.put("subcasesInfo", subcasesList);
				}
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating cases failed.");
		}
		return responseMap;
	}

	/**
	 * method is Used To get case schedule details.
	 * URL: /caseOnJob/addOnJobDetails
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.LIST_ALL_CASES, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getAllCaseInfo(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside getAllCaseInfo");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.LIST_ALL_CASES + " service");
		List<Object> responseList = null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getAllCaseInfo(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			List<HashMap<String, Object>> casesList =  (List<HashMap<String, Object>>) responseList.get(0);
			List<HashMap<String, Object>> casescountList =  (List<HashMap<String, Object>>) responseList.get(1);
			if(casesList.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Cases found.");
				responseMap.put("casescountresult", casescountList.get(0));
			}else{

				responseMap.put(ResponseConstantsOfCaseDetails.CASES_LIST,casesList);
				responseMap.put("casescountresult", casescountList.get(0));
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}

		}else{

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Fetching all cases failed.");

		}
		return responseMap;
	}

	/**
	 * method is Used To get cases in que.
	 * URL: /cases/listAllCasesinQue
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.LIST_ALL_CASES_IN_QUE, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getAllCaseInfoInQue(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside getAllCaseInfoInQue");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.LIST_ALL_CASES_IN_QUE + " service");
		List<HashMap<String, Object>> responseList = null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getCasesInQue(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			boolean isSuccess = (boolean) responseList.get(0).get(CommonResponseConstants.ISSUCCESS);
			if(isSuccess){
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				responseMap.put(ResponseConstantsOfCaseDetails.CASES_LIST, responseList);
			}else{
				responseMap=responseList.get(0);
			}
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "No cases Found.");
		}
		return responseMap;
	}



	/**
	 * method is Used To assign case to que.
	 * URL: /cases/addCasetoQue
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.ASSIGN_CASE_TO_SELF, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> assignCasetoSelf(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside /cases/addCasetoQue");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.ASSIGN_CASE_TO_SELF + " service");
		List<HashMap<String, Object>> responseList = null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.assigncasetoself(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap= responseList.get(0);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Adding case to que failed.");
		}
		return responseMap;
	}



	/**
	 * This method is Used To Fetch Details of Customer Case .
	 * URL: /cases/fetchCaseDetailedInfo
	 * METHOD: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CUSTOMER_CASE_DETAILS_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchCustomerCaseDetailsInfo(@RequestBody CaseDetails caseDetails) {
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_CUSTOMER_CASE_DETAILS_INFO + " service");
		List<Object> responseList =null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();//getting return object

		//Fetch Details of Customer Case  details in TRAC DB
		try {
			responseList = sqlOperations.getCustomerCaseDetails(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		} 

		if(responseList!=null && responseList.size()>0){

			HashMap<String, Object> caseDetailsResult = ((List<HashMap<String, Object>>)responseList.get(0)).get(0);
			List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseList.get(1);
			List<HashMap<String, Object>> locationList =  (List<HashMap<String, Object>>) responseList.get(2);
			List<HashMap<String, Object>> casefaultsservicesList =  (List<HashMap<String, Object>>) responseList.get(3);

			if(caseDetailsResult.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Case Not Found.");
			}else if(caseDetailsResult.get(CommonResponseConstants.ISSUCCESS)!=null && !(boolean) caseDetailsResult.get(CommonResponseConstants.ISSUCCESS)){
				responseMap = caseDetailsResult;
			}else{
				responseMap.put("casedetails",caseDetailsResult);
				responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,subcasesList);
				responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLIST, locationList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				ArrayList<Object> faultServicesList = commonUtil.processCaseFaultServices(casefaultsservicesList);
				responseMap.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICES_LIST,faultServicesList);

			}

		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Case Not Found.");
		}

		return responseMap;
	}



	/**
	 * method is Used To get cases count details que,pending,closed.
	 * URL: /cases/casescount
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.FETCH_CASES_COUNT, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getCasesCount(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside getCasesCount");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.FETCH_CASES_COUNT + " service");
		List<HashMap<String, Object>> responseList = null;
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseList = sqlOperations.getCasesCount(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseList!=null && responseList.size()>0){
			responseMap = responseList.get(0);
		}else{

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Cases found.");

		}
		return responseMap;
	}
	

/**
	 * method is Used To update fault id of subcase
	 * URL: /cases/updatefault
	 * METHOD: POST
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfRoadAssistance.UPDATE_FAULT, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> updateFault(@RequestBody CaseDetails caseDetails) {
		System.out.println("Inside updateFault");
		logger.info("IN "+ RestUrlConstantsOfRoadAssistance.UPDATE_FAULT + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseMap = sqlOperations.updateFaultBySubCaseId(caseDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

	
		return responseMap;
	}
	

}
