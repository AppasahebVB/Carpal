package trac.services.interfaces.users;

import java.util.HashMap;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gmr.web.multipart.GMultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.userbean.SecurityQuestions;
import trac.beans.userbean.UserRoles;
import trac.beans.userbean.Users;

public interface UserInterface {
	  //public @ResponseBody Object registerUserDetails(@RequestBody Users userDetails);
	  public @ResponseBody Object userRegistration(@RequestParam("file") GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response,@RequestHeader ("host") String hostName);
	  //public @ResponseBody Object updateUserDetails(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> fetchUserDetails(@RequestBody Users userDetails,@RequestHeader ("host") String hostName) throws Exception;
	  public @ResponseBody HashMap<String, Object> checkForUnique(@RequestBody Users userDetails);
	  public @ResponseBody Object getSecurityInfo(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> validateSecInfo(@RequestBody Users userDetails);
	  public @ResponseBody SecurityQuestions getSecurityQuestionsList();
	  public @ResponseBody UserRoles getRolesList();
	  public @ResponseBody boolean sendMail();
	  public @ResponseBody Object validateRegistration(); 
	  public @ResponseBody Object changePassword(Users userDetails);
	  public @ResponseBody Object updateUserDetails(GMultipartFile gMultiPartData, Users userDetails,HttpServletRequest request, HttpServletResponse response);
	  public @ResponseBody Object resetPassword(@RequestBody Users userDetails);
	  public @ResponseBody Object updatePassword(@RequestBody Users userDetails);
	  public @ResponseBody Object createRole(@RequestBody HashMap<String, Object> requestBody);
	  public @ResponseBody Object updateRole(@RequestBody HashMap<String, Object> requestBody);
	  public @ResponseBody HashMap<String, Object> getModulesList() ;
	  public @ResponseBody HashMap<String, Object> getUserModules(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> canAccess(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> getUserPermissions(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> getRoleDetails(@RequestBody Users userDetails);
	  public @ResponseBody HashMap<String, Object> updateRoleStatus(@RequestBody Users userDetails);
	//  public @ResponseBody Object updateCustomerContactDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,UserLocation userLocation,HttpServletRequest request,HttpServletResponse response);
//	  public @ResponseBody Object saveCustomerContactDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,UserLocation userLocation,HttpServletRequest request,HttpServletResponse response);
}
