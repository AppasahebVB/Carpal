package trac.services.interfaces.report;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.userbean.Users;

public interface ReportsInterface {
	
	public @ResponseBody HashMap<String, Object> getOperationsReport(@RequestBody Users userDetails);
	public @ResponseBody HashMap<String, Object> getAttendanceReport(@RequestBody Users userDetails);
	public @ResponseBody HashMap<String, Object> getTruckeReport(@RequestBody Users userDetails);
	public @ResponseBody HashMap<String, Object> getExpensesReport(@RequestBody Users userDetails);

}
