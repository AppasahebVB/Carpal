package trac.services.interfaces.vehicle;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.vehcilebean.Vehicle;

public interface CustomerVehicleInterface {
	 public @ResponseBody HashMap<String, Object> registerCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails);
	 public @ResponseBody HashMap<String, Object> updateCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails);
	 public @ResponseBody HashMap<String, Object> fetchCustomerVehicleDetails(@RequestBody Vehicle vechicleDetails);
	
}
