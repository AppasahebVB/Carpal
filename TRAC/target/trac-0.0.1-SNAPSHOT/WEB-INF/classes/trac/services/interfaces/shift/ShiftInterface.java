package trac.services.interfaces.shift;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.shiftbean.Shift;

public interface ShiftInterface {
	public @ResponseBody HashMap<String, Object> registerShiftTimings(@RequestBody HashMap<String, Object> requestBody);
	public @ResponseBody HashMap<String, Object> updateShiftTimings(@RequestBody Shift shiftTimings);
	public @ResponseBody HashMap<String, Object> deleteShiftTimings(@RequestBody Shift shiftTimings);
	public @ResponseBody HashMap<String, Object> fetchShiftTimings();
	public @ResponseBody HashMap<String, Object> getShiftScheduleInfo(Shift shift);
	public @ResponseBody HashMap<String, Object> getListOfShifts(@RequestBody Shift shiftDetails);
	public @ResponseBody HashMap<String, Object> updateDriverSchedule(@RequestBody Shift shiftDetails);
	public @ResponseBody HashMap<String, Object> genarateShiftSchedule(@RequestBody Shift shift);
}
