package trac.services.interfaces.roadassistance;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import trac.beans.casesbean.CaseDetails;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface CaseRegisterInterface {
	
	public @ResponseBody HashMap<String, Object> registerCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> fetchCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody List<Map<String, Object>> fetchServicesList();
	 public @ResponseBody HashMap<String, Object> updateCaseStatus(@RequestBody CaseDetails caseDetails);
	 

	 //public @ResponseBody HashMap<String, Object> caseFaultAndServices();
}
