package trac.util.utilinterface;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import trac.beans.driverbean.Driver;

public interface CommonUtilInterface {
	public ArrayList<Object> processVehicleBrands(ArrayList<Object> vehicleBrandModelsList);
	public ArrayList<Object> processTruckBrands(ArrayList<Object> truckBrandModelsList);
	public ArrayList<Object> processCaseFaultServices(List<HashMap<String, Object>> responseServicesList);
	public String getEarlierDate(Date yourDate);
	public int dateComparator(String dateToCompare);
	public String getColorCode(int policyExpiryCode,int reRegExpiryCode);
	public String getColorCodeForLicenceExpire(int policyExpiryCode);
	public String buildPreviousExperienceArray(ArrayList<Driver> driverDetailsList);
	public String buildPreviousExperienceUpdateArray(ArrayList<Driver> driverDetailsList);
	public String buildSchedulecasesArray(ArrayList<LinkedHashMap<String, Object>> caseDetailsList);
	public String buildShiftTimingsArray(ArrayList<LinkedHashMap<String, Object>> shifttimingsList);
	public String buildModuleAccesTypeArray(ArrayList<LinkedHashMap<String, Object>> accesssTypeList);
}
