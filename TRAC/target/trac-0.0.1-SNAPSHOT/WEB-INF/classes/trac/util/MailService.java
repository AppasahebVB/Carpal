package trac.util;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class MailService {
	
	private static MailSender mailSender;
	private static String fromEmail;
	private static String password;
	private static String decryptedPassword;
	

	@Autowired
	public void setMailSender(MailSender mailSender) {
		MailService.mailSender = mailSender;
	}
	
	@Autowired
	public void setFromEmail(String fromEmail) {
		MailService.fromEmail = fromEmail;
	}
	
	@Autowired
	public void setPassword(String password) {
		MailService.password = password;
	}
	
	
	/*public void sendMail(String to, String subject, String msg) {
	
		SimpleMailMessage message = new SimpleMailMessage();
		
		Properties props = new Properties();
		 	  	 props.put("mail.smtp.starttls.enable", "true"); // added this line  
			     props.put("mail.smtp.host", "smtp.gmail.com");  
			     props.put("mail.smtp.port", "587");  
			     props.put("mail.smtp.auth", "true"); 
			     
	        Session mailSession = Session.getDefaultInstance(props,  
		    	    new javax.mail.Authenticator() {
		    	       protected PasswordAuthentication getPasswordAuthentication() {  
		    	       return new PasswordAuthentication(fromEmail,password);  
		    	   }  
		   }); 
	        
		if(mailSession != null){
			message.setFrom(fromEmail);
			message.setTo(to);
			message.setSubject(subject);
			message.setText(msg);
			//mailSender.setJavaMailProperties(props);
			try{
				mailSender.send(message);
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}
		
	}*/
	
	public void sendMail(String to, String subject, String msgBody) {
		
		 	Properties props = new Properties();
		 	  	 props.put("mail.smtp.starttls.enable", "true"); // added this line  
			     props.put("mail.smtp.host", "smtp.gmail.com");  
			     props.put("mail.smtp.port", "587");  
			     props.put("mail.smtp.auth", "true"); 
			     
			     //basicTextEncryptor.setPassword(password);
			    // String encrypted = EncryptionUtil.encrypt(basicTextEncryptor,password);
			    // MailService.decryptedPassword  =  EncryptionUtil.decrypt(password);
			   //  System.out.println("encrypted - "+encrypted);
			     
			     Session session = Session.getDefaultInstance(props,  
		    	    new javax.mail.Authenticator() {
		    	       protected PasswordAuthentication getPasswordAuthentication() {  
		    	       return new PasswordAuthentication(fromEmail.trim(),MailService.decryptedPassword);  
		    	   }  
			     }); 
		    
	      
	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress(fromEmail.trim(), "TRAC Admin"));
	            msg.addRecipient(Message.RecipientType.TO,new InternetAddress(to.trim(), "Mr. User"));
	            msg.setSubject(subject);
	            msg.setText(msgBody);
	            Transport.send(msg);
	            

	        } catch (AddressException e) {
	        	System.out.println(e.getMessage());
	        } catch (MessagingException e) {
	        	System.out.println(e.getMessage());
	        } catch (Exception e) {
	        	System.out.println(e.getMessage());
	        }
		
	}
	
	
	/**
	   * Create mail session.
	   * 
	   * @return mail session, may not be null.
	   */
	public Session createSession()
	  {
		  Properties props = new Properties();
			 
	   /* props.put("mail.smtps.host", 587);
	    props.put("mail.smtps.auth", "true");
	    props.put("mail.smtp.starttls.enable", "true");*/
	    
	     props.put("mail.smtp.starttls.enable", "true"); // added this line  
	     props.put("mail.smtp.host", "smtp.gmail.com");  
	     props.put("mail.smtp.port", "587");  
	     props.put("mail.smtp.auth", "true"); 
	  /*   props.put("mail.smtp.socketFactory.port", "465");  
	     props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
	     props.put("mail.smtp.socketFactory.fallback", "false");*/
	    
/*
	    Authenticator auth = null;
	    if (password != null && fromEmail != null)
	    {
	      auth = new Authenticator()
	      {
	        protected PasswordAuthentication getPasswordAuthentication()
	        {
	          return new PasswordAuthentication(fromEmail, password);
	        }
	      };
	    }*/
	   // Session session = Session.getInstance(props, auth);
	    
	    Session session = Session.getDefaultInstance(props,  
	    	    new javax.mail.Authenticator() {
	    	       protected PasswordAuthentication getPasswordAuthentication() {  
	    	       return new PasswordAuthentication(fromEmail,password);  
	    	   }  
	   }); 
	    
	    return session;
	  }
}
