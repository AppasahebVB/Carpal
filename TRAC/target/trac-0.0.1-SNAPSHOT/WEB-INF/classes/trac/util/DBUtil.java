package trac.util;



import java.util.HashMap;
import java.util.Map;


import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.utilinterface.DBUtilInterface;


public class DBUtil implements DBUtilInterface{
	private Map<String, Object> inParamMap = null;
	private SqlParameterSource inParams = null;
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	
	/**Executes Stored Procedure with the specified parameters
	 */
	@Override
	public Map<String,Object> executeStoredProcedure(String[] inParamaters,Object[] inParamValues,SimpleJdbcCall simpleJdbcCall){
		
		inParamMap = new HashMap<String, Object>();
		
		for(int i = 0; i < inParamaters.length;i++){
			inParamMap.put(inParamaters[i], inParamValues[i] == null ? "" :inParamValues[i]);
			System.out.println(inParamaters[i] + " = " + inParamValues[i] == null ? "" :inParamValues[i]);
		}
		
		inParams = new MapSqlParameterSource(inParamMap);

	
		try{
			//executing stored procedure
			return  simpleJdbcCall.execute(inParams);
		  }catch (BadSqlGrammarException ex) {
			return  exceptionHandling.customGenericExceptionJson(ex.getSQLException().getErrorCode(),ex.getMessage());
		  } catch (UncategorizedSQLException ex) {
			  return  exceptionHandling.customGenericExceptionJson(ex.getSQLException().getErrorCode(),ex.getMessage());
		  }catch (EmptyResultDataAccessException ex) { 
			  return  exceptionHandling.customGenericExceptionJson(500,ex.getMessage());
		  }catch (DataAccessException ex) { 
			  return  exceptionHandling.customGenericExceptionJson(500,ex.getMessage());
		  }catch (Exception ex) {
		      ex.printStackTrace();
		    return  null;
		}//end of try catch
		
	}//end of method

}//end of main class
