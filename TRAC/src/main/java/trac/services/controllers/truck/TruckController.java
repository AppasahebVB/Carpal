package trac.services.controllers.truck;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.truckbean.Truck;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.resturlconstants.RestUrlConstantsOfTruck;
import trac.dao.casesdao.CaseRelatedSqlOperations;
import trac.dao.truckdao.TruckDaoOperations;
import trac.services.interfaces.truck.TruckInterface;
import trac.util.CommonUtil;
import trac.util.utilinterface.CommonUtilInterface;

@Controller
public class TruckController implements TruckInterface{
	private static final Logger logger = LoggerFactory.getLogger(TruckController.class);
	private TruckDaoOperations sqlOperations = new TruckDaoOperations();
	private CaseRelatedSqlOperations casesqlOperations;
    private CommonUtilInterface commonUtil = new CommonUtil();
	/**
	 * This method is to register truck details
	 * @param truckDetails
	 * @serviceurl /truck/register
	 * @return success (or) failure
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.REGISTER_TRUCK_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerTruckDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.REGISTER_TRUCK_INFO + " service");
		System.out.println("in "+  RestUrlConstantsOfTruck.REGISTER_TRUCK_INFO  +" service");
		List<HashMap<String, Object>> responseListMap = null;
		try {
			responseListMap = sqlOperations.registerTuckInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_DETAILS_SAVE_FAILED);
		}


		return resultMap;
	}

	/**
	 * This method is to update truck details
	 * @param truckDetails
	 * @serviceurl /truck/update
	 * @return success (or) failure
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.UPDATE_TRUCK_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateTruckDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.UPDATE_TRUCK_INFO + " service");
		List<HashMap<String, Object>> responseListMap = null;
		try {
			responseListMap = sqlOperations.editTruckInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is to unregister truck details
	 * @param truckDetails
	 * @serviceurl /truck/unregistertrcuk
	 * @return success (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.UNREGISTER_TRUCK_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> unRegisterTruck(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.UNREGISTER_TRUCK_INFO + " service");
		List<HashMap<String, Object>> responseListMap = null ;
		try {
			responseListMap = sqlOperations.unRegisterTruck(truckDetails.getTruckId(),truckDetails.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_UNREGISTRATION_FAILED);
		}
		return resultMap;
	}
	
	/**
	 * This method is to activate/deactivate truck 
	 * @param truckDetails
	 * @serviceurl /truck/activation
	 * @return success (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.ACTIVATE_BY_DEACTIVATE_TRUCK_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateTruckActivation(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.ACTIVATE_BY_DEACTIVATE_TRUCK_INFO + " service");
		List<HashMap<String, Object>> responseListMap = null ;
		try {
			responseListMap = sqlOperations.updateTruckActivation(truckDetails.getTruckId(),truckDetails.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_UNREGISTRATION_FAILED);
		}
		return resultMap;
	}

	/**
	 * This method is to get all truck details
	 * @param truckDetails
	 * @serviceurl /truck/gettruckinfo
	 * @return success with trucks list (or) failure
	 */
	
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.GET_TRUCK_INFO, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchTruckDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.GET_TRUCK_INFO + " service");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> trucksList = null;
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		try {
			trucksList = (List<HashMap<String, Object>>) sqlOperations.getTruckInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(trucksList!=null && trucksList.size()>0){
			returnObject.put(ResponseConstantsOfTruck.TRUCKLIST, trucksList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCKS_NOT_FOUND);

		}
		return returnObject;

	}

	/**
	 * This Function is Used To Get All Truck Brands and case Services .
	 * URL : /truck/getservicesandbrands
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.FETCH_SERVICES_TRUCK_BRANDS, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchTruckServicesAndBrandDetails() {
		logger.info("IN " + RestUrlConstantsOfTruck.FETCH_SERVICES_TRUCK_BRANDS+ " service");
		// registering vehicle details in TRAC DB

		casesqlOperations = new CaseRelatedSqlOperations();

		ArrayList<Object> vehicleBrandModelsList = (ArrayList<Object>) sqlOperations
				.getTruckBrands();
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		List<Map<String, Object>> servicesList = casesqlOperations.getServiceList();


		if(servicesList!=null && servicesList.size()>0){
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			returnObject.put("services", servicesList);
			
			if (vehicleBrandModelsList != null && vehicleBrandModelsList.size() > 0) {
				ArrayList<Object> brandsList = commonUtil.processTruckBrands(vehicleBrandModelsList);
				returnObject.put(ResponseConstantsOfTruck.TRUCKBRANDLIST,brandsList);
			}

			
		}else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, "No Services found");
		}



		return returnObject;

	}



	/**
	 * This method is to register truck expense details
	 * @param truckDetails
	 * @serviceurl /truck/expenseregister
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.REGISTER_TRUCK_EXPENSE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerTruckExpenseDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.REGISTER_TRUCK_EXPENSE_INFO + " service");
		System.out.println("in "+  RestUrlConstantsOfTruck.REGISTER_TRUCK_EXPENSE_INFO  +" service");
		List<HashMap<String, Object>> responseListMap = null;
		try {
			responseListMap = sqlOperations.registerTuckExpenseInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			if((boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put("truckExpenses", responseListMap);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_EXPENSE_DETAILS_SAVED);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_EXPENSE_DETAILS_SAVE_FAILED);
		}


		return resultMap;
	}

	/**
	 * This method is to update truck expense details
	 * @param truckDetails
	 * @serviceurl /truck/expenseupdate
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.UPDATE_TRUCK_EXPENSE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateTruckExpenseDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.UPDATE_TRUCK_EXPENSE_INFO + " service");
		List<HashMap<String, Object>> responseListMap = null;
		try {
			responseListMap = sqlOperations.editTruckExpenseInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashMap<String, Object> resultMap = null;
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			if((boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put("truckExpenses", responseListMap);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_EXPENSE_DETAILS_UPDATED);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfTruck.TRUCK_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}



	/**
	 * This method is to get all truck Expense details
	 * @param truckDetails
	 * @serviceurl /truck/gettruckexpenseinfo
	 * @return success with trucks list (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfTruck.GET_TRUCK_EXPENSE_INFO, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchTruckExpenseDetails(@RequestBody Truck truckDetails) {
		logger.info("IN "+ RestUrlConstantsOfTruck.GET_TRUCK_EXPENSE_INFO + " service");
	
		List<HashMap<String, Object>> responseListMap = null;
		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		try {
			responseListMap = (List<HashMap<String, Object>>) sqlOperations.getTruckexpenseInfo(truckDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			if((boolean) resultMap.get(CommonResponseConstants.ISSUCCESS)){
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put("truckExpenses", responseListMap);
			}
		}
		else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCK_EXPENSES_NOT_FOUND);

		}
		return resultMap;

	}
}
