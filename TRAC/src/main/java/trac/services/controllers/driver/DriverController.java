package trac.services.controllers.driver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.driverbean.Driver;
import trac.beans.shiftbean.Shift;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfShift;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.resturlconstants.RestUrlConstantsOfDriver;
import trac.dao.driverdao.DriverSqlOperations;
import trac.dao.shiftdao.ShiftDaoOperations;
import trac.dao.truckdao.TruckDaoOperations;
import trac.dao.userdao.UserSqlOperations;
import trac.services.interfaces.driver.DriverInterface;


@Controller
public class DriverController implements DriverInterface{

	private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
	public DriverSqlOperations driverdaoOperations = new DriverSqlOperations();
	/**
	 * This method is  to save Driver Personal Details
	 *   
	 * @param driverDetails
	 * @serviceurl /driver/savePersonalDetailsOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVER_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> savePersonalDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_DRIVER_INFO + " service");
		System.out.println("in "+  RestUrlConstantsOfDriver.SAVE_DRIVER_INFO  +" service");
	
		HashMap<String, Object> resultMap = null;
		String licenseNo = driverDetails.getLicenseNo();
		String passportNo = driverDetails.getPassportNo();

		if(licenseNo!=null && licenseNo.length()>0 && passportNo!=null && passportNo.length()>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertPersonalDetails(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PERSONAL_DETAILS_FAILED);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PERSONAL_DETAILS_MISSING);
		}

		return resultMap;
	}

	/**
	 * This method is  to edit Driver Personal Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/editPersonalDetailsOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.EDIT_DRIVER_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editPersonalDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.EDIT_DRIVER_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();
		int  userId       = driverDetails.getCreatedBy();

		if(driverId>0 && userId >0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.updatePersonalDetails(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PERSONAL_DETAILS_UPDATE_FAILED);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PERSONAL_IDDETAILS_MISSING);
		}

		return resultMap;
	}

	/**
	 * This method is  to fetch Driver Personal Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchPersonalDetailsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVER_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchPersonalDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVER_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();

		if(driverId>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.fetchPersonalDetails(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is  to save Driver Contact Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/saveContactDetailsOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVER_CONTACT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveContactDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_DRIVER_CONTACT_INFO + " service");

		HashMap<String, Object> resultMap = null;

		List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertContactInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_CONTACT_DETAILS_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is  to edit Driver Contact Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/editContactDetailsOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.EDIT_DRIVER_CONTACT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editContactDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.EDIT_DRIVER_CONTACT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.updateContactInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_CONTACT_DETAILS__UPDATE_FAILED);
		}

		return resultMap;

	}

	/**
	 * This method is  to fetch Driver Contact Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchContactDetailsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVER_CONTACT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchContactDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVER_CONTACT_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();

		if(driverId>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.fetchContactInfo(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is  to save/edit Driver Emergency Contact Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/saveOrEditEmergencyContactDetailsOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_EDIT_DRIVER_EMERGENCY_CONTACT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveOrEditEmergencyContactDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_EDIT_DRIVER_EMERGENCY_CONTACT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertorupdateEmergencyContactInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_EMERGENCY_CONTACT_INFO_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is  to save Driver Previous Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/saveDriverWorkExperience
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVER_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveDriverWorkExperience(@RequestBody ArrayList<Driver> driverDetailsList) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_DRIVER_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertExperienceInfo(driverDetailsList);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_FAILED);
			}
		
		return resultMap;
	}

	/**
	 * This method is  to edit Driver Previous Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/editDriverWorkExperience
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.EDIT_DRIVER_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editDriverWorkExperience(@RequestBody ArrayList<Driver> driverDetailsList) {
		logger.info("IN "+ RestUrlConstantsOfDriver.EDIT_DRIVER_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.updateExperienceInfo(driverDetailsList);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_EXPERIENCE_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is  to fetch Driver Previous Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchDriverWorkExperience
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVER_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchDriverWorkExperience(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVER_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();

		if(driverId>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.fetchExperienceInfo(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is to fetch Driver Emergency Contact Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchEmergencyContactDetailsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVER_EMERGENCY_CONTACT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchEmergencyContactDetailsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVER_EMERGENCY_CONTACT_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();

		if(driverId>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.fetchEmergencyContactInfo(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is  to save Driver Present Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/savePresentWorkExperienceOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVER_PRESENT_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> savePresentWorkExperienceOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_DRIVER_PRESENT_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertPresentExperienceInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PRESENT_EXPERIENCE_DETAILS_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is  to edit Driver Present Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/editPresentWorkExperienceOfDriver
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.EDIT_DRIVER_PRESENT_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> editPresentWorkExperienceOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.EDIT_DRIVER_PRESENT_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.updatePresentExperienceInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PRESENT_EXPERIENCE_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}


	/**
	 * This method is  to fetch Driver Present Experience Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchPresentWorkExperienceOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVER_PRESENT_EXPERIENCE_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchPresentWorkExperienceOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVER_PRESENT_EXPERIENCE_INFO + " service");
		HashMap<String, Object> resultMap = null;

		int driverId      = driverDetails.getDriverId();

		if(driverId>0){
			List<HashMap<String, Object>> responseListMap = driverdaoOperations.fetchPresentExperienceInfo(driverDetails);
			if(responseListMap!=null && responseListMap.size()>0){
				resultMap = responseListMap.get(0);
			}else{
				resultMap = new HashMap<String, Object>();
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
			}
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is  to fetch Driver Shift Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/serachDriverShiftInfo
	 * @return success with driver shifts List (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SERACH_DRIVERSHIFT_INFO, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> serachDriverShiftInfo() {
		logger.info("IN "+ RestUrlConstantsOfDriver.SERACH_DRIVERSHIFT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getDriverShiftInfo();
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS,responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
		}

		return resultMap;
	}

	/**
	 * This method is  to fetch Driver Shift Schedule
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchShiftSchedule
	 * @return success with driver shifts schedules List (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVERSHIFT_SCHEDULES_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchShiftSchedule(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVERSHIFT_SCHEDULES_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getShiftSchedule(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS,responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_NOT_FOUND);
		}

		return resultMap;
	}


	/**
	 * This method is  to save Driver Shift Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/saveDriverShiftInfo
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_DRIVERSHIFT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveDriverShiftInfo(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_DRIVERSHIFT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.saveDriverShiftInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_SAVE_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is to edit Driver Shift Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/editDriverShiftInfo
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.EDIT_DRIVERSHIFT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateDriverShiftInfo(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.EDIT_DRIVERSHIFT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.updateDriverShiftInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}


	/**
	 * This method is to delete Driver Shift Details
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/deleteDriverShiftInfo
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.DELETE_DRIVERSHIFT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> deleteDriverShiftInfo(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.DELETE_DRIVERSHIFT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.deleteDriverShiftInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_SHIFT_DETAILS_DELETE_FAILED);
		}

		return resultMap;
	}


	/**
	 * This method is to get all Drivers with Punched InOut Timings
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchShiftINOUTTimmingsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_ALLSHIFT_INOUT_TIMMINGS_OFDRIVER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_ALLSHIFT_INOUT_TIMMINGS_OFDRIVER + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getShiftINOUTTimmingsOfDriver(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS, responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
		}

		return resultMap;
	}

	/**
	 * This method is to save Punched InOut Timings of Driver
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/saveShiftINOUTTimmingsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.SAVE_SHIFT_INOUT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.SAVE_SHIFT_INOUT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.insertShiftInOutInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PUNCH_DETAILS_FAILED);
		}

		return resultMap;
	}

	/**
	 * This method is to update Punched InOut Timings of Driver
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/updateShiftINOUTTimmingsOfDriver
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.UPDATE_SHIFT_INOUT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.UPDATE_SHIFT_INOUT_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.updateShiftInOutInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_PUNCH_DETAILS_UPDATE_FAILED);
		}

		return resultMap;
	}

	/**
	 *  This method is to get drivers based on shiftId on a selected date
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchShiftINOUTTimmingsOfDriverByShift
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_SHIFTID, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriverByShift(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_SHIFTID + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getShiftINOUTTimmingsOfDriverByShiftId(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS, responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
		}

		return resultMap;
	}


	/**
	 * This method is to get drivers based on driverId or driverName on a selected date
	 * 
	 * @param driverDetails
	 * @serviceurl /driver/fetchShiftINOUTTimmingsOfDriverByDetails
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_DRIVER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriverByDetails(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_SHIFT_INOUT_TIMMINGS_OFDRIVER_BY_DRIVER + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getShiftINOUTTimmingsOfDriverByDetails(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS, responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_LOGGEDIN);
		}

		return resultMap;
	}

	/**
	 * This Function is Used To Get All Trucks and Shifts And Managers.
	 * URL : /driver/gettrucksandshiftsmanagers
	 * 
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_TRUCKS_SHIFTS_MANAGERS, method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> fetchShiftsAndTrucksAndManagers() {
		logger.info("IN " + RestUrlConstantsOfDriver.FETCH_TRUCKS_SHIFTS_MANAGERS+ " service");
		// registering vehicle details in TRAC DB

		List<HashMap<String, Object>> trucksList = (List<HashMap<String, Object>>) new TruckDaoOperations().getTruckInfo();
		HashMap<String, Object> returnObject = new HashMap<String, Object>();

		if(trucksList!=null && trucksList.size()>0){
			
			returnObject.put(ResponseConstantsOfTruck.TRUCKLIST, trucksList);
			returnObject.put(CommonResponseConstants.ISSUCCESS, true);
			
			List<Shift> shiftsList = (List<Shift>) new ShiftDaoOperations().getShiftInfo();
			if(shiftsList!=null && shiftsList.size()>0){
			returnObject.put(ResponseConstantsOfShift.SHIFTLIST, shiftsList);
			}
			
			List<Map<String, Object>> managersList =  new UserSqlOperations().fetchManagers();
			if(managersList!=null && managersList.size()>0){
				returnObject.put("managers", managersList);
			}
		
		}
		else{
			returnObject.put(CommonResponseConstants.ISSUCCESS, false);
			returnObject.put(CommonResponseConstants.SERVICEMESSAGE, ResponseConstantsOfTruck.TRUCKS_NOT_FOUND);

		}
		return returnObject;
	}

	/**
	 * This method is  to fetch all Drivers
	 * 
	 * @serviceurl /driver/fetchDriversInfo
	 * @return success (or) failure
	 */

	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.FETCH_DRIVERS_INFO, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getAllDriversInfo() {
		logger.info("IN "+ RestUrlConstantsOfDriver.FETCH_DRIVERS_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.getAllDrivers();
		
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = new HashMap<String, Object>();
			
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(ResponseConstantsOfDriver.DRIVERS,responseListMap);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVERS_NOT_FOUND);
		}

		return resultMap;
	}

	
	
	/**
	 * This method is to delete Driver Details
	 * 
	 * @param driverDetails 
	 * @serviceurl /driver/deleteDriverInfo
	 * @return success (or) failure
	 */


	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.DELETE_DRIVER_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> deleteDriverInfo(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.DELETE_DRIVER_INFO + " service");
		HashMap<String, Object> resultMap = null;
		List<HashMap<String, Object>> responseListMap = driverdaoOperations.deleteDriverInfo(driverDetails);
		if(responseListMap!=null && responseListMap.size()>0){
			resultMap = responseListMap.get(0);
		}else{
			resultMap = new HashMap<String, Object>();
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE,ResponseConstantsOfDriver.DRIVER_NOT_FOUND);
		}

		return resultMap;
	}
	
	/**
	 * This method is to delete Driver Details
	 * 
	 * @param driverDetails 
	 * @serviceurl /driver/getFullDriverDetails
	 * @return success (or) failure
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfDriver.GET_DRIVER_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@SuppressWarnings("unchecked")
	public @ResponseBody HashMap<String,Object> getDriverInfoById(@RequestBody Driver driverDetails) {
		logger.info("IN "+ RestUrlConstantsOfDriver.GET_DRIVER_DETAILS + " service");
		
		List<Object> driverDetailsInfo = null; //return json variable
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
		
		try {
			driverDetailsInfo = driverdaoOperations.getFullDriverInfoById(driverDetails);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if(driverDetailsInfo!=null && driverDetailsInfo.size()>0){
			List<HashMap<String, Object>> driverDetailsMap = (List<HashMap<String, Object>>)driverDetailsInfo.get(0);
			List<HashMap<String, Object>> driverExpList =  (List<HashMap<String, Object>>) driverDetailsInfo.get(1);
			
			if(driverDetailsMap.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Driver Details found.");
			}else{
				responseMap.put(ResponseConstantsOfDriver.DRIVER_INFO,driverDetailsMap);
				responseMap.put("driverExpList", driverExpList);
				responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}

		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "CustomerId/Mobile Number doesn't Exist.");
		}
		
		return responseMap;
	}




}
