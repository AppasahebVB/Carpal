package trac.services.controllers.users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import trac.beans.userbean.SecurityQuestions;
import trac.beans.userbean.UserRoles;
import trac.beans.userbean.Users;
import trac.constants.cloudconstants.CloudConstantsInfo;
import trac.constants.resturlconstants.RestUrlConstantsOfUser;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUser;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.dao.userdao.UserSqlOperations;
import trac.services.interfaces.users.UserInterface;
import trac.util.CloudStorageService;
import trac.util.CustomHashing;
import trac.util.CustomHttpURLConnection;
import trac.util.MailService;
import trac.util.UtilFunctions;
import trac.util.utilinterface.SpringByCryptoHashing;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.gmr.web.multipart.GMultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class UserController implements UserInterface{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private UserSqlOperations sqlOperations = new UserSqlOperations();
	private CloudStorageService cloudService = new CloudStorageService(); // object that have cloud related info
	private SpringByCryptoHashing hashing = new CustomHashing();
	private UtilFunctions utility = new UtilFunctions();

	/**
	 * This method is used to send email
	 * @return success and failure
	 */
	
	@Override
	@RequestMapping(value = "/user/sendmail", method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody boolean sendMail() {
		logger.info("send mail.");
		//MailService.sendMail("agkreddy@gmail.com","hii","hiiiiiiiiiii");
		return true;
	}
	
	
	/**
	 * This method is used to upload the Multipart image file and register user 
	 * 
	 * @param gMultiPartData
	 * @param userDetails
	 * @param request
	 * @serviceurl /user/register
	 * @return success and failure
	 */

	@RequestMapping(value = RestUrlConstantsOfUser.CREATE_USER, method = RequestMethod.POST)
	public @ResponseBody Object userRegistration(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response,@RequestHeader ("host") String hostName) {
		
		logger.info("create user.");
		System.out.println("in "+ RestUrlConstantsOfUser.CREATE_USER +" service");
		
		Object returnJson; //return json variable
		String profilePicURL = null; //to hold image url of cloud
		String userFileNameOnCloud = null;
		int updateCount = 0;
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
		
		//encrypting the password
		userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
		
		//Condition to check User Registered Throw Mobile or web   (1= mobile,2= web)		
		if(userDetails.getDeviceTypeId() == 2){
			userDetails.setRoleIds("6");
			userDetails.setFromWeb(false);
		}else{
			userDetails.setFromWeb(false);
		}
		
		     if(userDetails.getPassword()!=null && userDetails.getPassword().length()>0
				&&
				userDetails.getEmail()!=null && userDetails.getEmail().length()>0
				&&
				userDetails.getPhone()!=null && userDetails.getPhone().length()>0){
			
			
			
			//calling addUserDetails method to save user details
			returnJson = sqlOperations.addUserDetails(userDetails);
		
			@SuppressWarnings("unchecked")
			HashMap<String, Object> returnedUserDetails = (HashMap<String, Object>) returnJson;
			
			//check whether is there any multipart content exist 
			if(returnedUserDetails != null){
				if((boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS) && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null){
						if (gMultiPartData.getSize() != 0) {
							userFileNameOnCloud = new Integer((int)returnedUserDetails.get(CommonResponseConstants.USERID)).toString() + UtilFunctions.getDate();
							//Blob file = new Blob(gMultiPartData.getBytes());
							profilePicURL = cloudService.uploadImagesToCloudBucket(gMultiPartData,userFileNameOnCloud,null,response,CloudConstantsInfo.USER_BUCKETNAME);
							if(profilePicURL.length() > 0)
								updateCount = sqlOperations.updateProfilePic((int)returnedUserDetails.get(CommonResponseConstants.USERID),profilePicURL,userFileNameOnCloud);
							 //userDetails.setProfilePicURL(profilePicURL);
							if(updateCount == 1)
								returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,profilePicURL);
							else
								returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,null);
						}
						if(!userDetails.isFromWeb()){
							userDetails.setUserId((int)returnedUserDetails.get(CommonResponseConstants.USERID));
							responseMap = sqlOperations.authGenaration(userDetails,hostName);
							returnedUserDetails.putAll(responseMap);
						}
				}
			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Internal server error");
				returnJson = responseMap;
			}
		}
		else{
	
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Email/Phone doesn't Exist.");
			returnJson = responseMap;
		}
		
		    
	
		return returnJson;
	}
	
	/**
	 * This method is Used To Edit UserDetails of Registered User. 
	 * URL: /userprofile/update
	 * Method: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.EDIT_USER, method = RequestMethod.POST)
	public @ResponseBody Object updateUserDetails(@RequestParam(value = "file",required=false) GMultipartFile gMultiPartData,Users userDetails,HttpServletRequest request,HttpServletResponse response) {
		logger.info("edit user.");
		String profilePicURL = null;
		String oldFileName = null;
		boolean isLoginExist = false;
		String userFileNameOnCloud = new Integer(userDetails.getUserId()).toString() + UtilFunctions.getDate();
		Object returnJson;
		
		HashMap<String, Object> returnedUserDetails = null;
		String  isImageChanged = request.getParameter("isImageChanged");

		//user update if details are not null
		if(userDetails.getEmail()!=null && userDetails.getEmail().length()>0
				||
		   userDetails.getPhone()!=null && userDetails.getPhone().length()>0){
			
			//password encyption if password not null
			if(userDetails.getPassword()!=null && userDetails.getPassword().length()>0 )
				userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
			
			//updating the details by calling update dao method
			returnJson = sqlOperations.editUserDetails(userDetails);
			
        	returnedUserDetails = (HashMap<String, Object>) returnJson;
	    }else{
	    	returnedUserDetails = new HashMap<String, Object>();
	    	returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
	    	
	    }
		
		//is email/phone already exist
		isLoginExist = (boolean)returnedUserDetails.get(CommonResponseConstants.ISLOGINEXIST);
		
		//check whether is there any multipart content exist 
		if(isImageChanged.equals("true") && ServletFileUpload.isMultipartContent(request) && gMultiPartData != null && !isLoginExist){
			System.out.println("in gMultiPartData if");
				if (gMultiPartData.getSize() != 0) {
					System.out.println("in gMultiPartData size if");
					if(returnedUserDetails.containsKey(ResponseConstantsOfUser.CLOUD_PIC_FILENAME) && (String)returnedUserDetails.get(ResponseConstantsOfUser.CLOUD_PIC_FILENAME) != null) //condition to check whether the key exist in map
						oldFileName = returnedUserDetails.get(ResponseConstantsOfUser.CLOUD_PIC_FILENAME).toString();
					
					profilePicURL = cloudService.uploadImagesToCloudBucket(gMultiPartData,userFileNameOnCloud,oldFileName,response,CloudConstantsInfo.USER_BUCKETNAME);
					
					int updateCount =sqlOperations.updateProfilePic(userDetails.getUserId(),profilePicURL,userFileNameOnCloud);
					 //userDetails.setProfilePicURL(profilePicURL);
					if(updateCount == 1){
						System.out.println("in updateCount if");
						returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, true);
						returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "User updated successfully");
						returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,profilePicURL);
					}else{
						System.out.println("in updateCount if");
						returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "Problem updating image");
						returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
						returnedUserDetails.put(ResponseConstantsOfUser.PROFILEPICURL,null);
					}
				}else{
					System.out.println("in gMultiPartData size else");
					returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, returnedUserDetails.get(CommonResponseConstants.SERVICEMESSAGE));
					returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, true);
				}
				
		}else if(!(boolean)returnedUserDetails.get(CommonResponseConstants.ISSUCCESS)){
			System.out.println("in gMultiPartData else");
			if(isLoginExist)
				returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, returnedUserDetails.get(CommonResponseConstants.SERVICEMESSAGE));
			else
				returnedUserDetails.put(CommonResponseConstants.SERVICEMESSAGE, "Nothing to update");
			
			returnedUserDetails.put(CommonResponseConstants.ISSUCCESS, false);
		}
		return returnedUserDetails;
	}


/**
	 * This method is Used To Validate User Details of Registered User By Using Email And Phone.
	 * URL: /user/validate
	 * Method: POST
	 * @return Token , Service Message
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.VALIDATE_USER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchUserDetails(@RequestBody Users userDetails,@RequestHeader ("host") String hostName) throws Exception {
		logger.info("in "+ RestUrlConstantsOfUser.VALIDATE_USER +" service");
   
		//http url connection to get accesstoken 
		CustomHttpURLConnection httpUrlConnection = new CustomHttpURLConnection();
		Users returnUserDetails = null;
		int roleId = 0;
		
		//fetching userdetails if exists
		if((userDetails.getLoginId()!=null && userDetails.getLoginId().length()>0) && userDetails.getPassword()!=null && userDetails.getPassword().length()>0 ){
			 returnUserDetails = sqlOperations.getUserDetails(userDetails);
		}
		int rowUpdatedCount = 0;


		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(returnUserDetails != null ){
			System.out.println("RetryCount - "+returnUserDetails.getRetryCount());
			if(returnUserDetails.getIsActive() == 0){
				responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,false);
			}
			else if(returnUserDetails.getRetryCount() < 3){
				responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS, hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword()));
			}else{
				responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS, false);
			}
			
			
			//Generate Token If Credentials is Valid
			if((boolean) responseMap.get(ResponseConstantsOfUser.ISVALIDCREDENTIALS)){
				
				//getting accesstoken
				//Users tempUserDetails = httpUrlConnection.sendPost("http://"+hostName+"/oauth/token?username="+returnUserDetails.getUserId()+"&password="+userDetails.getPassword()+"&client_id=mysupplycompany&client_secret=mycompanykey&grant_type=password");
				Users tempUserDetails = httpUrlConnection.serviceCall("http://"+hostName+"/oauth/token?username="+returnUserDetails.getUserId()+"&password="+userDetails.getPassword()+"&client_id=trac&client_secret=trac&grant_type=password");
				
				//condition to send role id only to web application
				if(roleId != 6)
					responseMap.put(CommonResponseConstants.ROLEID,returnUserDetails.getRoleId());
				
				responseMap.put(CommonResponseConstants.EMAIL,returnUserDetails.getEmail());
				responseMap.put(CommonResponseConstants.PHONE,returnUserDetails.getPhone());
				responseMap.put(ResponseConstantsOfUser.HASHEDPASSWORD,"");
				responseMap.put(CommonResponseConstants.ACCESSTOKEN,tempUserDetails.getAccessToken());
				responseMap.put(CommonResponseConstants.REFRESHTOKEN,tempUserDetails.getRefreshToken());
				responseMap.put(CommonResponseConstants.EXPIRESIN,tempUserDetails.getExpiresIn());
				responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,returnUserDetails.getProfilePicURL());
				responseMap.put(CommonResponseConstants.SERVICECODE,600);
				responseMap.put(CommonResponseConstants.ISSUCCESS,true);
				responseMap.put(CommonResponseConstants.USERID, returnUserDetails.getUserId());
				responseMap.put(ResponseConstantsOfUser.DISPLAYNAME, returnUserDetails.getDisplayName());
				
				
				if(returnUserDetails.getApplicationVersionNo()!=null && !returnUserDetails.getApplicationVersionNo().equalsIgnoreCase(userDetails.getApplicationVersionNo())){
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"App version changed,please update");
				}

			}else{
				if(returnUserDetails.getIsActive() == 0){
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Role is not active, please contact admin");
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					return responseMap;
				}
				else if(returnUserDetails.getRetryCount() < 3)
					rowUpdatedCount = sqlOperations.updateUserLoginAttempts(returnUserDetails.getUserId());
				
				//Condition to check If User login multiple times then display message
				if(rowUpdatedCount == 0 && returnUserDetails.getRetryCount() >= 3){
					System.out.println("if isMultipleLoginFailed - ");
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Multiple logins failed, please conatct admin");
					responseMap.put(CommonResponseConstants.SERVICECODE,602);
				}else{
					System.out.println("else isMultipleLoginFailed - ");

					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Invalid User/Password");
					responseMap.put(CommonResponseConstants.SERVICECODE,601);
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				}
				responseMap.put(ResponseConstantsOfUser.HASHEDPASSWORD,"");

			}
		}else{


			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Invalid User/Password");
			responseMap.put(CommonResponseConstants.SERVICECODE,601);
			responseMap.put(CommonResponseConstants.ISSUCCESS,false);
		}

		return responseMap;
	}

	/**
	 * This method is Used To Check the Uniqueness of user along the application
	 * URL: /user/unique
	 * METHOD: POST
	 * @return 
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.UNIQUE_USER, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody  HashMap<String, Object> checkForUnique(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		boolean isSuccess =  sqlOperations.checkForUnique(userDetails.getUserName(),userDetails.getEmail());

		responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

		return responseMap;
	}
	/**
	 * This method is Used To Fetch SecurityQuestion When the user forget password.
	 * URL: /user/checkemail
	 * @return secQuestion
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_CHECK1, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object getSecurityInfo(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.SECURITY_CHECK1 +" service");
		
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 
		//condition to check if email and phone number is exit then return security question else email/phone does't exist.
		String randomPassword = UtilFunctions.generateRandomPassword();
		
		//HashMap<String, Object> response =  (HashMap<String, Object>) sqlOperations.fetchSecurityQuestion(userDetails.getLoginId(),userDetails.getLoginId());
		HashMap<String, Object> response =  (HashMap<String, Object>) sqlOperations.userValidation(userDetails.getLoginId(),userDetails.getLoginId(),randomPassword);
		if((boolean) response.get(CommonResponseConstants.ISSUCCESS)){
			new MailService().sendMail((String) response.get(CommonResponseConstants.EMAIL),"Email with password assistance sent successfully","Please use the "+randomPassword+" inorder to genarate new password");
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put(CommonResponseConstants.USERID, (int)response.get(CommonResponseConstants.USERID));
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Email with password assistance sent successfully.");
		}
		else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Email/Phone doesn't Exist.");
		}

		return responseMap;

	}
	/**
	 * This method is Used To validate security Answer to provided by user when he tries option to get forget password
	 * URL: /user/checksecurity
	 * @return userID
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_CHECK2, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> validateSecInfo(@RequestBody Users userDetails) {

		/*logger.info("in "+ RestUrlConstantsOfUser.SECURITY_CHECK2 +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		//HashMap<String, Object> responseMap =  sqlOperations.validateSecurityAnswer(userDetails);
		HashMap<String, Object> responseMap =  sqlOperations.validateTemporaryPassword(userDetails);*/
		logger.info("in "+ RestUrlConstantsOfUser.CHANGE_PASSWORD +" service");
		HashMap<String, Object> returnJson =  new HashMap<String,Object>();
		//Object returnJson = null;
		boolean isPasswordCorrect = false;
		
		Users returnUserDetails = sqlOperations.getUserDetailsById(userDetails.getUserId(),userDetails.isUpdateFlag());
		System.out.println("isTempPasswordChange - "+userDetails.isUpdateFlag());
		if(userDetails.isUpdateFlag()){
			isPasswordCorrect = hashing.decryption(userDetails.getTempPassword(),returnUserDetails.getHashedPassword());
		}else{
			isPasswordCorrect = hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword());
		}
		
		if(isPasswordCorrect){
			userDetails.setHashedPassword(hashing.encryption(userDetails.getNewPassword()));
			returnJson  =  (HashMap<String, Object>) sqlOperations.updatePassword(userDetails,SPNameConstants.CHANGE_PASSWORD_SPNAME);
		}else{
			returnJson.put(CommonResponseConstants.ISSUCCESS, false);
			returnJson.put(CommonResponseConstants.SERVICEMESSAGE, "Password not matching");
		}

		return returnJson;

		//return responseMap;
	}
	/**
	 * This method is used to get the list of security questions of list
	 * URL: /user/getsecurityinfo
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.SECURITY_QUESTIONS, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody SecurityQuestions getSecurityQuestionsList() {

		logger.info("in "+ RestUrlConstantsOfUser.SECURITY_QUESTIONS +" service");


		System.out.println("in "+ RestUrlConstantsOfUser.UNIQUE_USER +" service");
		SecurityQuestions secQuestions =  sqlOperations.fetchSecurityInfoList();
		
		
		return secQuestions;

	}
	
	/**
	 * This method is used to get the list of roles
	 * URL: /user/getroles
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_LIST_OF_ROLES, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody UserRoles getRolesList() {

		logger.info("in "+ RestUrlConstantsOfUser.GET_LIST_OF_ROLES +" service");
		List<Map<String, Object>> userRolesMap =  sqlOperations.fetchListOfRoles();
		UserRoles userRoles =new UserRoles();
		userRoles.setUserRolesList(userRolesMap);
		return userRoles;

	}
	
	/**
	 * This method is used to get Verification code before completion of registration. 
	 * URL: /user/securitycheck
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_CAPTCHA, method = RequestMethod.GET,consumes="application/json",produces="application/json")
	public @ResponseBody Object validateRegistration() {
		logger.info("get captcha");
		Map<String,Object> returnJson = new HashMap<String,Object>();
		//Generate captcha string
		String randomCaptcha = utility.generateCaptchaString();
		returnJson.put("captchaCode", randomCaptcha);
		return returnJson;
	}
	/**
	 * This method is used to reset the password of user.
	 * URL: /user/resetpassword
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.RESET_PASSWORD, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object changePassword(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.RESET_PASSWORD +" service");
		System.out.println("in "+ RestUrlConstantsOfUser.RESET_PASSWORD +" service");

		userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
		Object returnJson =  sqlOperations.updatePassword(userDetails,SPNameConstants.RESET_PASSWORD_SPNAME);

		return returnJson;

	}
	
	/**
	 * This method is used to get change password of user.
	 * URL: /user/changepassword
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.CHANGE_PASSWORD, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object resetPassword(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.CHANGE_PASSWORD +" service");
		Map<String, Object> returnJson =  new HashMap<String,Object>();
		//Object returnJson = null;
		boolean isPasswordCorrect = false;
		
		Users returnUserDetails = sqlOperations.getUserDetailsById(userDetails.getUserId(),userDetails.isUpdateFlag());
		System.out.println("isTempPasswordChange - "+userDetails.isUpdateFlag());
		if(userDetails.isUpdateFlag()){
			isPasswordCorrect = hashing.decryption(userDetails.getTempPassword(),returnUserDetails.getHashedPassword());
		}else{
			isPasswordCorrect = hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword());
		}
		
		if(isPasswordCorrect){
			userDetails.setHashedPassword(hashing.encryption(userDetails.getNewPassword()));
			returnJson  =  (Map<String, Object>) sqlOperations.updatePassword(userDetails,SPNameConstants.CHANGE_PASSWORD_SPNAME);
		}else{
			returnJson.put(CommonResponseConstants.ISSUCCESS, false);
			returnJson.put(CommonResponseConstants.SERVICEMESSAGE, "Password not matching");
		}

		return returnJson;

	}
	
	/**
	 * This method is used to update old password with new password
	 * URL: /password/update
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.UPDATE_PASSWORD, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object updatePassword(@RequestBody Users userDetails) {
		logger.info("in "+ RestUrlConstantsOfUser.UPDATE_PASSWORD +" service");
		Map<String, Object> returnJson =  new HashMap<String,Object>();
		/*//Object returnJson = null;
		boolean isPasswordCorrect = false;
		
		Users returnUserDetails = sqlOperations.getUserDetailsById(userDetails.getUserId(),userDetails.isUpdateFlag());
		System.out.println("isTempPasswordChange - "+userDetails.isUpdateFlag());
		if(userDetails.isUpdateFlag()){
			isPasswordCorrect = hashing.decryption(userDetails.getTempPassword(),returnUserDetails.getHashedPassword());
		}else{
			isPasswordCorrect = hashing.decryption(userDetails.getPassword(),returnUserDetails.getHashedPassword());
		}
		*/
			userDetails.setHashedPassword(hashing.encryption(userDetails.getPassword()));
			returnJson  =  (Map<String, Object>) sqlOperations.setPassword(userDetails,SPNameConstants.UPDATE_PASSWORD_SPNAME);
		/*}else{
			returnJson.put(CommonResponseConstants.ISSUCCESS, false);
			returnJson.put(CommonResponseConstants.SERVICEMESSAGE, "Password not matching");
		}
*/
		return returnJson;

	}
	
	/**
	 * This method is used to insert new role
	 * URL: /user/role
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.CREATE_ROLE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object createRole(@RequestBody HashMap<String, Object> requestBody) {
		logger.info("IN "+ RestUrlConstantsOfUser.CREATE_ROLE + " service");
		HashMap<String, Object> resultMap = null;
		//(List<HashMap<String, Object>>)  sqlOperations.createUserRole(requestBody);
		
		try {
			resultMap = (HashMap<String, Object>) sqlOperations.createUserRole(requestBody);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	* update Role .
	* URL: /user/role
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.UPDATE_ROLE, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Object updateRole(@RequestBody HashMap<String, Object> requestBody) {
		logger.info("IN "+ RestUrlConstantsOfUser.UPDATE_ROLE + " service");
		HashMap<String, Object> resultMap = null;
		//(List<HashMap<String, Object>>)  sqlOperations.createUserRole(requestBody);
		
		try {
			resultMap = (HashMap<String, Object>) sqlOperations.updateUserRole(requestBody);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	/**
	* get modules list
	* URL: /user/getmodules
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_LIST_OF_MODULES, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getModulesList() {

		logger.info("in "+ RestUrlConstantsOfUser.GET_LIST_OF_MODULES +" service");
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> userModulesMap =  sqlOperations.fetchListOfModules();
		if(userModulesMap!=null){
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put("modules", userModulesMap);
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Modules Found.");
		}
		return resultMap;

	}
	
	/**
	* get modules list
	* URL: /user/getmodules
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_USER_MODULES, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getUserModules(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.GET_USER_MODULES +" service");
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		List<String> userModulesMap =  sqlOperations.fetchUserRoleModules(userDetails);
		
	//	boolean isAccessible = sqlOperations.isPageAccessible(userDetails);
		
		if(userModulesMap!=null){
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put("modules", userModulesMap);
		//	resultMap.put("isAccessible", isAccessible);
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Modules Found.");
		}
		return resultMap;

	}
	
	/**
	* get user permissions
	* URL: /user/permissions
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_PERMISSIONS, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getUserPermissions(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.GET_PERMISSIONS +" service");
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> userModulesMap =  sqlOperations.getPermisssions(userDetails);
		
	//	boolean isAccessible = sqlOperations.isPageAccessible(userDetails);
		
		if(userModulesMap != null){
			if(userModulesMap.size() > 0){
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put("permissions", userModulesMap);
			}else{
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Permissions Found.");
			}
		//	resultMap.put("isAccessible", isAccessible);
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "No Permissions Found.");
		}
		return resultMap;

	}
	
	/**
	* check accessibility by user
	* URL: /user/access
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.USER_ACCESS, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> canAccess(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.USER_ACCESS +" service");
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		boolean isAccessible = sqlOperations.isPageAccessible(userDetails);
		
		
		resultMap.put(CommonResponseConstants.ISSUCCESS, true);
		resultMap.put("isAccessible", isAccessible);
	
		return resultMap;

	}
	
	/**
	* get all roles
	* URL: /user/access
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.GET_ROLES_INFO, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> getRoleDetails(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.GET_ROLES_INFO +" service");
		List<Map<String, Object>> resultMap = new ArrayList<Map<String, Object>>();
		HashMap<String, Object> returnMap = new HashMap<String, Object>();;
		resultMap = sqlOperations.getRoleInfo(userDetails);
		
		if(resultMap.size() > 0){
			returnMap.put("isSuccess", true);
			returnMap.put("listOfRoles", resultMap);
		}else{
			returnMap.put("isSuccess", false);
		}
		
		
	
		return returnMap;

	}
	
	
	/**
	* update Role status
	* URL: /user/updateRoleStatus
	*/
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.UPDATE_ROLE_STATUS, method = RequestMethod.POST,produces="application/json")
	public @ResponseBody HashMap<String, Object> updateRoleStatus(@RequestBody Users userDetails) {

		logger.info("in "+ RestUrlConstantsOfUser.UPDATE_ROLE_STATUS +" service");
		
		HashMap<String, Object> resultMap =  sqlOperations.updateUserRoleStatus(userDetails);
		
		return resultMap;

	}
	
	
	/**
	 * This method is used to get the list of users
	 * URL: /user/getusers
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.USERS_LIST, method = RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getListOfUsers(@RequestParam int userId) {

		logger.info("in "+ RestUrlConstantsOfUser.USERS_LIST +" service");
		List<Map<String, Object>> listOfUsers =  sqlOperations.fetchListOfUsers();
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		if(listOfUsers!=null){
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put("listOfUsers", listOfUsers);
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, true);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "Users not yet created.");
		}
		return resultMap;

	}
	
	
	/**
	 * This method is used to activate/deactivate user
	 * URL: /user/activation
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.USERS_ACTIVATION, method = RequestMethod.PUT,produces="application/json")
	public @ResponseBody HashMap<String, Object> userActivation(@RequestParam int userId) {

		logger.info("in "+ RestUrlConstantsOfUser.USERS_LIST +" service");
		List<HashMap<String, Object>> returnList =  sqlOperations.userActivation(userId);
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		if(returnList!=null){
			boolean isSuccess = (boolean) returnList.get(0).get("isSuccess");
			if(isSuccess) {
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put("isActive", returnList.get(0).get("isActive"));
			}else {
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE, returnList.get(0).get(CommonResponseConstants.SERVICEMESSAGE));
			}
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "Unable to remove User");
		}
		
		return resultMap;

	}
	
	/**
	 * This method is used to disable the user
	 * URL: /user/remove
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfUser.USERS_REMOVE, method = RequestMethod.PUT,produces="application/json")
	public @ResponseBody HashMap<String, Object> disableUser(@RequestParam int userId) {

		logger.info("in "+ RestUrlConstantsOfUser.USERS_LIST +" service");
		List<HashMap<String, Object>> returnList =  sqlOperations.disableUser(userId);
		HashMap<String, Object> resultMap = new HashMap<String, Object>(userId);
		  
		if(returnList!=null){
			boolean isSuccess = (boolean) returnList.get(0).get("isSuccess");
			if(isSuccess) {
				resultMap.put(CommonResponseConstants.ISSUCCESS, true);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "User removed successfully");
			}else {
				resultMap.put(CommonResponseConstants.ISSUCCESS, false);
				resultMap.put(CommonResponseConstants.SERVICEMESSAGE, returnList.get(0).get(CommonResponseConstants.SERVICEMESSAGE));
			}
		}else{
			resultMap.put(CommonResponseConstants.ISSUCCESS, false);
			resultMap.put(CommonResponseConstants.SERVICEMESSAGE, "Unable to remove User");
		}
		return resultMap;

	}
	

  
}
