package trac.services.controllers.dashboard;


import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.dashboardbean.Dashboard;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.resturlconstants.RestUrlConstantOfDashboard;
import trac.dao.dashboarddao.DashboardDaoOperations;
import trac.services.controllers.driver.DriverController;

@Controller
public class DashboardController {
	
	private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
	public DashboardDaoOperations dashboardDaoOperations = new DashboardDaoOperations();
	
	/**
	 * This method is to get ReportDetails
	 * @param dahsboard
	 * @serviceurl /dashboard/report
	 * @return success (or) failure
	 */

	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_INFO, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Dashboard getReportDetails(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_INFO + " service");
		
		return dashboardDaoOperations.getReportInfo(dahsboard);
		
	}
	
	/**
	 * This method is to get Report metadata information
	 * @param dahsboard
	 * @serviceurl /dashboard/reportmetadata
	 * @return success (or) failure
	 */

	
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_METADATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Dashboard getReportMetaData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_METADATA + " service");
		
	   return dashboardDaoOperations.getReportMetaDataInfo(dahsboard);
		
	}
	
	/**
	 * This method is to get Report data
	 * @param dahsboard
	 * @serviceurl /dashboard/reportdata
	 * @return success (or) failure
	 */

	
	
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_DATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody Dashboard getReportData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_DATA + " service");
		
	   return dashboardDaoOperations.getReportDataInfo(dahsboard);
		
	}
	
	/**
	 * This method is to get Report metadata information
	 * @param dahsboard
	 * @serviceurl /dashboard/reportmetadata
	 * @return success (or) failure
	 */

	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_SAVE_DATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveReportData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_SAVE_DATA + " service");
		
		List<HashMap<String, Object>> reportsList = (List<HashMap<String, Object>>) dashboardDaoOperations.saveReportInfo(dahsboard);
	    HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(reportsList!=null && reportsList.size()>0){
			return reportsList.get(0);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving report Failed.");
		}
		return responseMap;
		
	}

	/**
	 * This method is to update saved report data
	 * @param dahsboard
	 * @serviceurl /dashboard/updatereport
	 * @return success (or) failure
	 */

	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_UPDATE_DATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateReportData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_UPDATE_DATA + " service");
		
		List<HashMap<String, Object>> reportsList = (List<HashMap<String, Object>>) dashboardDaoOperations.updateReportInfo(dahsboard);
	    HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(reportsList!=null && reportsList.size()>0){
			return reportsList.get(0);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating report Failed.");
		}
		return responseMap;
	}
	
	/**
	 * This method is to delete saved report data
	 * @param dahsboard
	 * @serviceurl /dashboard/deletereport
	 * @return success (or) failure
	 */
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_DELETE_DATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> deleteReportData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_DELETE_DATA + " service");
		
		List<HashMap<String, Object>> reportsList = (List<HashMap<String, Object>>) dashboardDaoOperations.deleteReportInfo(dahsboard);
	    HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(reportsList!=null && reportsList.size()>0){
			return reportsList.get(0);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Deleting report Failed.");
		}
		return responseMap;
	}
	

	/**
	 * This method is to fetch saved report
	 * @param dahsboard
	 * @serviceurl /dashboard/fetchreport
	 * @return success (or) failure
	 */
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantOfDashboard.REPORT_FETCH_DATA, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchReportData(@RequestBody Dashboard dahsboard) {
		logger.info("IN "+ RestUrlConstantOfDashboard.REPORT_FETCH_DATA + " service");
		
		List<Object> resultList = (List<Object>) dashboardDaoOperations.fetchReportInfo(dahsboard);
	    HashMap<String, Object> responseMap = new HashMap<String, Object>();
		if(resultList!=null && resultList.size()>0){
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put("reportMetadataList", resultList.get(1));
			responseMap.put("reportsList", resultList.get(0));
			
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "No reports saved.");
		}
		return responseMap;
	
	}


}
