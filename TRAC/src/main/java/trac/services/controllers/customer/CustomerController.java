package trac.services.controllers.customer;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.customerbean.Customer;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.resturlconstants.RestUrlConstantsOfCustomer;
import trac.dao.customerdao.CustomerDaoOperations;
import trac.services.interfaces.customer.CustomerInterface;
import trac.util.CommonUtil;


@Controller
public class CustomerController implements CustomerInterface{
	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	private CustomerDaoOperations sqlOperations = new CustomerDaoOperations();

	/**
	 * This method is used to register customer with contact details from web
	 * 
	 * @param customerDetails
	 * @param customerRequest
	 * @serviceurl /customer/saveCustomerContactDetails
	 * @return success and failure
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfCustomer.SAVE_USER_CONTACT_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveCustomerContactDetails(@RequestBody Customer customerRequest) {

		logger.info("create customer.");
		System.out.println("in "+ RestUrlConstantsOfCustomer.SAVE_USER_CONTACT_DETAILS +" service");

		List<HashMap<String, Object>> responseMapList = null; //return json variable
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 

		if(customerRequest!=null && customerRequest.getMobileNumber()!=null && customerRequest.getMobileNumber().length()>0){

			//calling customerDetails function to save customer details along with location details through web 
			try {
				responseMapList = (List<HashMap<String, Object>>) sqlOperations.addUserWithLocationDetails(customerRequest);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(responseMapList!=null && responseMapList.size()>0){
				responseMap = responseMapList.get(0);

			}else{

				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Customer Details Failed.");

			}

		}
		else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Provide Mobile Number.");
		}


		return responseMap;
	}

	/**
	 * This Function is Used To Edit customerDetails and LocationDetails of Registered customer through web 
	 * URL: /customer/editCustomerContactDetails
	 * Method: POST
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.EDIT_USER_CONTACT_DETAILS, method = RequestMethod.POST, consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> updateCustomerContactDetails(@RequestBody Customer customerRequest) {
		logger.info("edit customer.");

		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 

		//user update if details are not null
		if(customerRequest.getCustomerId()!=null && customerRequest.getCustomerId().length()>0
				||
				customerRequest.getMobileNumber()!=null && customerRequest.getMobileNumber().length()>0){

			//updating the details by calling update dao function
			List<HashMap<String, Object>> responseMapList = null;
			try {
				responseMapList = (List<HashMap<String, Object>>) sqlOperations.editUserWithLocationDetails(customerRequest);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(responseMapList!=null && responseMapList.size()>0){
				responseMap = responseMapList.get(0);		
			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Editing Customer Details Failed.");
			}


		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Provide Customer Id or Mobile Number.");
		}


		return responseMap;
	}

	/**
	 * This Function is used to fetch user with contact details from web
	 * @param customerRequest
	 * @serviceurl /customer/fetchCustomerContactDetails
	 * @return success and failure
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_USER_CONTACT_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchCustomerContactDetails(@RequestBody Customer customerRequest) {

		System.out.println("in "+ RestUrlConstantsOfCustomer.FETCH_USER_CONTACT_DETAILS +" service");

		List<Object> responseMapList = null; //return json variable
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 


		if(customerRequest.getSearchBy()!=null && customerRequest.getSearchBy().length()>0){

			//calling fetchUserWithLocationDetails function to fetch customer details along with location details through web 
			try {
				responseMapList = (List<Object>) sqlOperations.fetchCustomerWithLocationDetails(customerRequest);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(responseMapList!=null && responseMapList.size()>0){
				HashMap<String, Object> customer = ((List<HashMap<String, Object>>)responseMapList.get(0)).get(0);
				List<HashMap<String, Object>> vehcilesList =  (List<HashMap<String, Object>>) responseMapList.get(1);
				if(!(boolean)customer.get(CommonResponseConstants.ISSUCCESS)){
					responseMap = customer;
				}else{
					responseMap.put("customer",customer);
					responseMap.put("vehicles", vehcilesList);
					responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "CustomerId/Mobile Number doesn't Exist.");
			}

		}
		return responseMap;
	}

	/**
	 * This Function is used to fetch the case deatils
	 * @param customerRequest
	 * @serviceurl /case/details
	 * @return success and failure
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_CASE_DETAILS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchCaseDetails(@RequestBody Customer customerRequest) {

		System.out.println("in "+ RestUrlConstantsOfCustomer.FETCH_CASE_DETAILS +" service");

		List<Object> responseMapList = null; //return json variable
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 


		if(customerRequest.getSearchBy()!=null && customerRequest.getSearchBy().length()>0){

			//calling fetchUserWithLocationDetails function to fetch customer details along with location details through web 
			try {
				responseMapList = (List<Object>) sqlOperations.fetchCaseDetails(customerRequest);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(responseMapList!=null && responseMapList.size()>0){
				HashMap<String, Object> customer = ((HashMap<String, Object>)responseMapList.get(0));
				List<HashMap<String, Object>> vehcilesList =  (List<HashMap<String, Object>>) responseMapList.get(1);
				List<HashMap<String, Object>> vehcileserviceLocationsList =  (List<HashMap<String, Object>>) responseMapList.get(2);
				List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseMapList.get(3);
				List<HashMap<String, Object>> faultservicesList =  (List<HashMap<String, Object>>) responseMapList.get(4);
				List<HashMap<String, Object>> assistanceStatusList =  (List<HashMap<String, Object>>) responseMapList.get(5);
				ArrayList<Object> vehicleBrandModelsList =  (ArrayList<Object>) responseMapList.get(6);
				List<HashMap<String, Object>> commentsList =  (List<HashMap<String, Object>>) responseMapList.get(7);
				if(customer.isEmpty()){
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Customer found.");
				}else{
					responseMap.put("customer",customer);
					responseMap.put("vehicles", vehcilesList);
					responseMap.put("vehicleservicelocations", vehcileserviceLocationsList);
					responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,subcasesList);
					ArrayList<Object> faultServicesList = new CommonUtil().processCaseFaultServices(faultservicesList);
					responseMap.put("faultservices",faultServicesList);
					responseMap.put("assistanceStatusList",assistanceStatusList);
					ArrayList<Object> brandsList = new CommonUtil().processVehicleBrands((ArrayList<Object>)vehicleBrandModelsList);
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEBRANDSLIST,brandsList);
					responseMap.put("commentsList",commentsList);
					responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "CustomerId/Mobile Number doesn't Exist.");
			}

		}
		return responseMap;
	}


	/**
	 * This Function is used to fetch teh case deatils
	 * 
	 * @param userDetails
	 * @param request
	 * @serviceurl /case/details
	 * @return success and failure
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_CASE_DETAILS_BY_Id, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchCaseDetailsById(@RequestBody Customer customerRequest) {

		System.out.println("in "+ RestUrlConstantsOfCustomer.FETCH_CASE_DETAILS_BY_Id +" service");

		List<Object> responseMapList = null; //return json variable
		HashMap<String, Object> responseMap = new HashMap<String, Object>(); 


		if(customerRequest.getSearchBy()!=null && customerRequest.getSearchBy().length()>0){

			//calling fetchUserWithLocationDetails function to fetch customer details along with location details through web 
			try {
				responseMapList = (List<Object>) sqlOperations.fetchCaseDetailsById(customerRequest);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(responseMapList!=null && responseMapList.size()>0){
				HashMap<String, Object> customer = ((HashMap<String, Object>)responseMapList.get(0));
				List<HashMap<String, Object>> subcasesList =  (List<HashMap<String, Object>>) responseMapList.get(1);
				List<HashMap<String, Object>> faultservicesList =  (List<HashMap<String, Object>>) responseMapList.get(2);
				List<HashMap<String, Object>> commentsList =  (List<HashMap<String, Object>>) responseMapList.get(3);
				List<HashMap<String, Object>> brandsList =  (List<HashMap<String, Object>>) responseMapList.get(4);
				if(customer.isEmpty()){
					responseMap.put(CommonResponseConstants.ISSUCCESS,false);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"No Customer found.");
				}else{
					responseMap.put("customer",customer);
					responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO,subcasesList);
					responseMap.put("commentsList",commentsList);
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEBRANDSLIST,brandsList);
					ArrayList<Object> faultServicesList = new CommonUtil().processCaseFaultServices(faultservicesList);
					responseMap.put("faultservices",faultServicesList);
					
					responseMap.put(CommonResponseConstants.ISSUCCESS, true);
				}

			}else{
				responseMap.put(CommonResponseConstants.ISSUCCESS, false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "CustomerId/Mobile Number doesn't Exist.");
			}

		}
		return responseMap;
	}
	
	/**
	* This method is used to save customer satisfaction
	* 
	* @param customerRequest
	* @serviceurl /customerStaisfaction/saveCustomerReview
	* @return success and failure
	*/

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestUrlConstantsOfCustomer.SAVE_CUSTOMER_SATISFACTION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> saveCustomerSatisfaction(@RequestBody Customer customerRequest) {

	System.out.println("in "+ RestUrlConstantsOfCustomer.SAVE_CUSTOMER_SATISFACTION +" service");

	List<HashMap<String, Object>> responseMapList = null; //return json variable
	HashMap<String, Object> responseMap = new HashMap<String, Object>(); 


	try {
		responseMapList = (List<HashMap<String, Object>>) sqlOperations.saveCustomerSatisfaction(customerRequest);
	} catch (Exception e) {
		e.printStackTrace();
	}
	if(responseMapList!=null && responseMapList.size()>0){
		responseMap = responseMapList.get(0);
	}else{

		responseMap.put(CommonResponseConstants.ISSUCCESS, false);
		responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Customer Satisfaction Failed.");
	}



	return responseMap;
	}


}
