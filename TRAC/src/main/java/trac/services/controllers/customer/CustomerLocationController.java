package trac.services.controllers.customer;


import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.resturlconstants.RestUrlConstantsOfCustomer;
import trac.dao.customerdao.CustomerLocationDaoOperations;
import trac.services.interfaces.customer.CustomerLocationInterface;



@Controller
public class CustomerLocationController implements CustomerLocationInterface {

	private static final Logger logger = LoggerFactory.getLogger(CustomerLocationController.class);
	private CustomerLocationDaoOperations sqlOperations = new CustomerLocationDaoOperations();
	/**
	 * This Function is used to Register location details of registered customer.
	 * URL:/serviceLocation/saveVehicleServiceLocation
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.REGISTER_CUSTOMER_SERVICELOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerCustomerLocationDetails(
			@RequestBody CaseDetails customerLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.REGISTER_CUSTOMER_SERVICELOCATION + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		List<Object> returnList = null;
		try {
			returnList =  sqlOperations.addcustomerLocation(customerLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(returnList !=null && returnList.size()>0)
		{
			
			HashMap<String, Object> locationResult = ((List<HashMap<String, Object>>)returnList.get(0)).get(0);
			List<HashMap<String, Object>> casesResult =  (List<HashMap<String, Object>>) returnList.get(1);
			if(locationResult.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Customer not found.");
			}else{
			responseMap.put("location",locationResult);
			responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO, casesResult);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer location added successfully");
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}
		}else{

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Customer Location Failed.");
		}
		return responseMap;
	}
	/**
	 * This Function is used to Edit location details of registered customer.
	 * URL: /serviceLocation/editVehicleServiceLocation
	 */
	@SuppressWarnings( "unchecked" )
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.EDIT_CUSTOMER_SERVICELOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> updateCustomerLocationDetails(
			@RequestBody CaseDetails customerLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.EDIT_CUSTOMER_SERVICELOCATION + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		List<Object> returnList = null;
		try {
			returnList =  sqlOperations.updateCustomerLocation(customerLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(returnList !=null && returnList.size()>0)
		{
			HashMap<String, Object> locationResult = ((List<HashMap<String, Object>>)returnList.get(0)).get(0);
			List<HashMap<String, Object>> casesResult =  (List<HashMap<String, Object>>) returnList.get(1);
			/*if(locationResult.get(CommonResponseConstants.ISSUCCESS)!=null && !((boolean) locationResult.get(CommonResponseConstants.ISSUCCESS))){
				responseMap = locationResult;
			}*/
			if(locationResult.isEmpty()){
				responseMap.put(CommonResponseConstants.ISSUCCESS,false);
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Customer location not found.");
			}else{
			responseMap.put("location",locationResult);
			responseMap.put(ResponseConstantsOfCaseDetails.SUBCASES_INFO, casesResult);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer location updated successfully");
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			}
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating Customer Location Failed.");
		}
		return responseMap;
	}
	/**
	 * This Function is used to get registered location details of registered customer.
	 * URL: /serviceLocation/fetchVehicleServiceLocation
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_CUSTOMER_SERVICELOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchCustomerLocationDetails(
			@RequestBody CaseDetails customerLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.FETCH_CUSTOMER_SERVICELOCATION + " service");
		HashMap<String, Object> responseMap;

		List<HashMap<String, Object>> returnList = null;

		try {
			returnList = (List<HashMap<String, Object>>) sqlOperations.getCustomerLocation(customerLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		responseMap = new HashMap<String, Object>();
		//condition to check objectsize is not null then return customerid and location object
		if(returnList!=null && returnList.size()>0){
			responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID, customerLocationDetails.getCustomerId());
			responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLIST, returnList);
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer Location not Found.");

		}

		return responseMap;
	}

	
	/**
	 * This Function is used to get registered destination location details of registered customer.
	 * URL: /serviceLocation/fetchVehicleServiceLocation
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_CUSTOMER_DESTINATIONLOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> fetchCustomerDestinationLocationDetails(
			@RequestBody CaseDetails customerLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.FETCH_CUSTOMER_DESTINATIONLOCATION + " service");
		HashMap<String, Object> responseMap;

		List<HashMap<String, Object>> returnList = null;

		try {
			returnList = (List<HashMap<String, Object>>) sqlOperations.getCustomerDestinationLocation(customerLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		responseMap = new HashMap<String, Object>();
		//condition to check objectsize is not null then return customerid and location object
		if(returnList!=null && returnList.size()>0){
			responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID, customerLocationDetails.getCustomerId());
			responseMap.put(ResponseConstantsOfCustomer.DESTLOCATIONLIST, returnList);
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
		}
		else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer Destination Location not Found.");

		}

		return responseMap;
	}
	

	/**
	 * This Function is used to Register destination location details of registered user.
	 * URL:/serviceLocation/saveUserDestinationLocation
	 */
	@SuppressWarnings("unchecked")
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.REGISTER_USER_DESTINATIONLOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> registerUserDestinationLocationDetails(
			@RequestBody CaseDetails userLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.REGISTER_USER_DESTINATIONLOCATION + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> returnList = null;
		try {
			returnList =  sqlOperations.adduserDestinationLocation(userLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(returnList !=null && returnList.size()>0)
		{
			responseMap = returnList.get(0);
			
		}else{

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Saving Customer Destination Location Failed.");
		}
		return responseMap;
	}
	
	/**
	 * This Function is used to Edit location details of registered customer.
	 * URL: /serviceLocation/editUserDestinationLocation
	 */
	@SuppressWarnings( "unchecked" )
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.EDIT_USER_DESTINATIONLOCATION, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody
	HashMap<String, Object> updateUserDestinationLocationDetails(
			@RequestBody CaseDetails customerLocationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.EDIT_USER_DESTINATIONLOCATION + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> returnList = null;
		try {
			returnList =  sqlOperations.updateUserDestinationLocation(customerLocationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(returnList !=null && returnList.size()>0)
		{
			responseMap = returnList.get(0);
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Updating Customer Location Failed.");
		}
		return responseMap;
	}
	

	/**
	 * This Function is used to fetch Vehicle Service Locations.
	 * URL: /serviceLocation/fetchServiceLocation
	 */
	@Override
	@RequestMapping(value = RestUrlConstantsOfCustomer.FETCH_VEHICLE_SERVICE_lOCATIONS, method = RequestMethod.POST,consumes="application/json",produces="application/json")
	public @ResponseBody HashMap<String, Object> fetchVehicleServiceLocation(@RequestBody Customer locationDetails) {
		logger.info("IN "+ RestUrlConstantsOfCustomer.FETCH_VEHICLE_SERVICE_lOCATIONS + " service");
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> returnList = null;
		try {
			returnList =  sqlOperations.getVehicleServiceLocations(locationDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(returnList !=null && returnList.size()>0)
		{
			responseMap.put(CommonResponseConstants.ISSUCCESS, true);
			responseMap.put("vehicleServiceLocations", returnList);
			
		}else{
			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Locations not found.");
		}
		return responseMap;
	}
}
