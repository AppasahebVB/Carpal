package trac.services.interfaces.roadassistance;


import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;

public interface CustomerCaseRegisterInterface {
	
	 public @ResponseBody HashMap<String, Object> registerCustomerCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> fetchCustomerCaseDetails(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> updateCustomerCaseDetails(@RequestBody HashMap<String, Object> requestBody);
	 public @ResponseBody HashMap<String, Object> searchForCustomer(@RequestBody Customer  customerDetails);
	 public @ResponseBody HashMap<String, Object> searchForVehicle(@RequestBody Customer  customerDetails);
	 
	 public @ResponseBody HashMap<String, Object> caseFaultAndServices();
	 
	 public @ResponseBody HashMap<String, Object> fetchAvailableDrivers(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> assignCaseToDriver(@RequestBody CaseDetails caseDetails);
	 
	 public @ResponseBody HashMap<String, Object> saveCasePrerequisite(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> saveCaseOnJob(@RequestBody CaseDetails caseDetails);
	 
	 public @ResponseBody HashMap<String, Object> getCaseScheduleInfo(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> editCaseScheduleInfo(@RequestBody CaseDetails  caseDetails);
	 
	 public @ResponseBody HashMap<String, Object> getAllCaseInfo(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> fetchCustomerCaseDetailsInfo(@RequestBody CaseDetails caseDetails);
	 
	 public @ResponseBody HashMap<String, Object> getAllCaseInfoInQue(CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> assignCasetoSelf(CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> getCasesCount(@RequestBody CaseDetails caseDetails);
	 public @ResponseBody HashMap<String, Object> updateFault(@RequestBody CaseDetails caseDetails);
	 
	 public @ResponseBody HashMap<String, Object> saveCaseComment(@RequestBody CaseDetails caseDetails);
}

