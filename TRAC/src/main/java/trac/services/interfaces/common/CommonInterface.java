package trac.services.interfaces.common;

import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.commonbean.Common;

public interface CommonInterface {
	public @ResponseBody Common getListOfCountries(Common commonBean);
}
