package trac.services.interfaces.customer;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;

public interface CustomerLocationInterface {

	public @ResponseBody HashMap<String, Object> registerCustomerLocationDetails(@RequestBody CaseDetails customerLocationDetails);
	public @ResponseBody HashMap<String, Object> updateCustomerLocationDetails(@RequestBody CaseDetails customerLocationDetails);
	public @ResponseBody HashMap<String, Object> fetchCustomerLocationDetails(@RequestBody CaseDetails customerLocationDetails);
	public @ResponseBody HashMap<String, Object> fetchCustomerDestinationLocationDetails(@RequestBody CaseDetails customerLocationDetails);
	public @ResponseBody HashMap<String, Object> updateUserDestinationLocationDetails(@RequestBody CaseDetails customerLocationDetails);
	public @ResponseBody HashMap<String, Object> registerUserDestinationLocationDetails(@RequestBody CaseDetails userLocationDetails);
	public @ResponseBody HashMap<String, Object> fetchVehicleServiceLocation(@RequestBody Customer locationDetails);

}
