package trac.services.interfaces.customer;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.customerbean.Customer;

public interface CustomerInterface {
	
	  public @ResponseBody HashMap<String, Object> saveCustomerContactDetails(@RequestBody Customer customerRequest);
	  public @ResponseBody HashMap<String, Object> updateCustomerContactDetails(@RequestBody Customer customerRequest);
	  public @ResponseBody HashMap<String, Object> fetchCustomerContactDetails(@RequestBody Customer customerRequest);
	  public @ResponseBody HashMap<String, Object> saveCustomerSatisfaction(@RequestBody Customer customerRequest);
	  public @ResponseBody HashMap<String, Object> fetchCaseDetails(@RequestBody Customer customerRequest);
}
