package trac.services.interfaces.driver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.driverbean.Driver;

public interface DriverInterface {
	public @ResponseBody HashMap<String, Object> savePersonalDetailsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> editPersonalDetailsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchPersonalDetailsOfDriver(Driver driverDetails);

	public @ResponseBody HashMap<String, Object> saveContactDetailsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> editContactDetailsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchContactDetailsOfDriver(Driver driverDetails);

	public @ResponseBody HashMap<String, Object> saveOrEditEmergencyContactDetailsOfDriver(@RequestBody Driver driverDetails);
//	public @ResponseBody HashMap<String, Object> editEmergencyContactDetailsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchEmergencyContactDetailsOfDriver(Driver driverDetails);

	public @ResponseBody HashMap<String, Object> saveDriverWorkExperience(@RequestBody ArrayList<Driver> driverDetailsList);
	public @ResponseBody HashMap<String, Object> editDriverWorkExperience(@RequestBody ArrayList<Driver> driverDetailsList);
	public @ResponseBody HashMap<String, Object> fetchDriverWorkExperience(Driver driverDetails);

	public @ResponseBody HashMap<String, Object> savePresentWorkExperienceOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> editPresentWorkExperienceOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchPresentWorkExperienceOfDriver(Driver driverDetails);

	public @ResponseBody HashMap<String, Object> serachDriverShiftInfo();
	public @ResponseBody HashMap<String, Object> saveDriverShiftInfo(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> updateDriverShiftInfo(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> deleteDriverShiftInfo(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchShiftSchedule(@RequestBody Driver driverDetails);

	public @ResponseBody HashMap<String, Object> saveShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> updateShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriver(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriverByShift(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> fetchShiftINOUTTimmingsOfDriverByDetails(@RequestBody Driver driverDetails);
	
	public @ResponseBody HashMap<String, Object> fetchShiftsAndTrucksAndManagers();
	
	public @ResponseBody HashMap<String, Object> getAllDriversInfo();
	
	public @ResponseBody HashMap<String,Object> getDriverInfoById(@RequestBody Driver driverDetails);
	public @ResponseBody HashMap<String, Object> deleteDriverInfo(@RequestBody Driver driverDetails);
	

}
