package trac.services.interfaces.truck;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import trac.beans.truckbean.Truck;

public interface TruckInterface {
	public @ResponseBody HashMap<String, Object> registerTruckDetails(@RequestBody Truck truckDetails);
	public @ResponseBody HashMap<String, Object> updateTruckDetails(@RequestBody Truck truckDetails);
	public @ResponseBody HashMap<String, Object> unRegisterTruck(@RequestBody Truck truckDetails);
	public @ResponseBody HashMap<String, Object> fetchTruckDetails(@RequestBody Truck truckDetails);
	public @ResponseBody HashMap<String, Object> fetchTruckServicesAndBrandDetails();
	
	public @ResponseBody HashMap<String, Object> fetchTruckExpenseDetails(Truck truckDetails);
	public @ResponseBody HashMap<String, Object> registerTruckExpenseDetails(Truck truckDetails);
	public @ResponseBody HashMap<String, Object> updateTruckExpenseDetails(Truck truckDetails);
	public @ResponseBody HashMap<String, Object> updateTruckActivation(@RequestBody Truck truckDetails);
}
