package trac.util.utilinterface;

public interface SpringByCryptoHashing {
	public String encryption(String stringToBeHashed);
	public boolean decryption(String actualString,String hashedString);
}
