package trac.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import trac.beans.userbean.Users;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;




public class CustomHttpURLConnection {

	// HTTP GET request
	public String sendGet(String urlToGET) throws Exception {

		
		URL obj = new URL(urlToGET);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + urlToGET);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());
		return response.toString();
	}
	
	
	// HTTP POST request
		public Users sendPostUsingHttpClient(String urlToPost) throws Exception {
			Map<String,Object> map = new HashMap<String,Object>();
			ObjectMapper mapper = new ObjectMapper();
			Users tempUser = new Users();
			HttpClient client =  HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(urlToPost);
		 
			// add header
			post.setHeader("Connection", "keep-alive");
			post.setHeader("Content-Type", "application/json");
			
			post.setEntity(new UrlEncodedFormEntity(null));
		 
			HttpResponse response = client.execute(post);
			System.out.println("Response Code : " 
		                + response.getStatusLine().getStatusCode());
			 if(response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(
				        new InputStreamReader(response.getEntity().getContent()));
			 
				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				
				
				map = mapper.readValue(result.toString(), 
		     		    new TypeReference<HashMap<String,Object>>(){});
		     		tempUser.setAccessToken((String)map.get("access_token"));
		     		tempUser.setExpiresIn((int)map.get("expires_in"));
		     		tempUser.setRefreshToken((String)map.get("refresh_token"));
		      
		             reader.close();
			   }
	       // writer.close();
	        return tempUser;

		}

	// HTTP POST request
	public Users sendPost(String urlToPost) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		ObjectMapper mapper = new ObjectMapper();
		Users tempUser = new Users();
		URL obj = new URL(urlToPost);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		  String message = URLEncoder.encode("my message", "UTF-8");

		// add reuqest header
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
	
		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
        writer.write("message=" + message);
        writer.close();

        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	 BufferedReader reader = new BufferedReader(new InputStreamReader(obj.openStream()));
             //StringBuffer res = new StringBuffer();
             String jsonString =  reader.readLine();
            /* while ((line = reader.readLine()) != null) {
            	 System.out.println("line = "+line);
                 res.append(line);
             }*/
           //convert JSON string to Map
     		map = mapper.readValue(jsonString, 
     		    new TypeReference<HashMap<String,Object>>(){});
     		tempUser.setAccessToken((String)map.get("access_token"));
     		tempUser.setExpiresIn((int)map.get("expires_in"));
     		tempUser.setRefreshToken((String)map.get("refresh_token"));
      
             reader.close();


        } 

        writer.close();
        return tempUser;

	}
	
	// HTTP POST request
		public Users serviceCall(String urlToPost) {
			Users tempUser = new Users();
			try{
			RestTemplate restTemplate = new RestTemplate();
			((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setReadTimeout(1000*30);
			
			
			Map<String,Object> response = restTemplate.getForObject(urlToPost,Map.class);
				tempUser.setAccessToken((String)response.get("access_token"));
	     		tempUser.setExpiresIn((int)response.get("expires_in"));
	     		tempUser.setRefreshToken((String)response.get("refresh_token"));
			}catch(Exception exception){
				System.out.println(exception.getMessage());
				return tempUser;
			}
			 return tempUser;

		}

}
