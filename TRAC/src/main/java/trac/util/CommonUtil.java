package trac.util;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Message;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import trac.beans.driverbean.Driver;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.util.utilinterface.CommonUtilInterface;

public class CommonUtil implements CommonUtilInterface{

	
	public static final String DISTANCE_MATRIX_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?";
	public static final String API_KEY = "AIzaSyCT0OZHt0fbdY-6Q-098YDI6PYQlWtreJM";

	/*
	 *Building Json Array of vehicle brands and models
	 *@param vehicleBrandModelsList
	 *@return list of vehicle brands and models
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Object> processVehicleBrands(ArrayList<Object> vehicleBrandModelsList) {

		if (vehicleBrandModelsList != null && vehicleBrandModelsList.size() > 0) {

			try {
				String brandName = (String) ((HashMap<String, Object>) vehicleBrandModelsList.get(0)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME);

				ArrayList<Object> brandsList = new ArrayList<Object>();
				HashMap<String, Object> modelsMap = new HashMap<String, Object>();

				ArrayList<Object> subModelsList = new ArrayList<Object>();

				int brandId = 0;
				int modelsSize = vehicleBrandModelsList.size();
				int modelId = 0;
				for (modelId = 0; modelId < modelsSize; modelId++) {
					brandName = (String) ((HashMap<String, Object>) vehicleBrandModelsList.get(modelId)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME);
					if (modelId != modelsSize - 1
							&& brandName.equalsIgnoreCase((String) ((HashMap<String, Object>) vehicleBrandModelsList.get(modelId + 1)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME))) {
						subModelsList.add(brandId, vehicleBrandModelsList.get(modelId));
						brandId++;
					} else {
						subModelsList.add(brandId, vehicleBrandModelsList.get(modelId));
						modelsMap = new HashMap<String, Object>();
						modelsMap.put("brandModels", subModelsList);
						modelsMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME,brandName);
						modelsMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID,((HashMap<String, Object>) vehicleBrandModelsList.get(modelId)).get(ResponseConstantsOfVehicle.VEHCICLEBRANDID));
						brandsList.add(modelsMap);
						subModelsList = new ArrayList<Object>();
						brandId = 0;
					}
				}
				return brandsList;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;	
	}


	/*
	 *Building Json Array of case faults and services
	 *@param truckBrandModelsList
	 *@return list case faults and services
	 */

	public ArrayList<Object> processCaseFaultServices(
			List<HashMap<String, Object>> responseServicesList) {

		if (responseServicesList != null && responseServicesList.size() > 0) {

			try {
				String faultName = (String) ((HashMap<String, Object>) responseServicesList.get(0)).get(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC);

				ArrayList<Object> faultsList = new ArrayList<Object>();
				HashMap<String, Object> servicesMap = new HashMap<String, Object>();

				ArrayList<Object> faultServicesList = new ArrayList<Object>();

				int faultId = 0;
				int servicesSize = responseServicesList.size();
				int serviceId = 0;
				for (serviceId = 0; serviceId < servicesSize; serviceId++) {
					faultName = (String) ((HashMap<String, Object>) responseServicesList.get(serviceId)).get(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC);
					if (serviceId != servicesSize - 1
							&& faultName.equalsIgnoreCase((String) ((HashMap<String, Object>) responseServicesList.get(serviceId + 1)).get(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC))) {
						faultServicesList.add(faultId, responseServicesList.get(serviceId));
						faultId++;
					} else {
						faultServicesList.add(faultId, responseServicesList.get(serviceId));
						servicesMap = new HashMap<String, Object>();
						servicesMap.put("faultServicesList", faultServicesList);
						servicesMap.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,faultName);
						servicesMap.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,((HashMap<String, Object>) responseServicesList.get(serviceId)).get(ResponseConstantsOfCaseDetails.CASE_FAULT_ID));
						servicesMap.put("checked", false);
						servicesMap.put("isDisabled", false);
						faultsList.add(servicesMap);
						faultServicesList = new ArrayList<Object>();
						faultId = 0;
					}
				}
				return faultsList;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;	
	}

	/*
	 *Building Truck brands and models list
	 *@param truckBrandModelsList
	 *@return list of truck brands and models
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Object> processTruckBrands(ArrayList<Object> truckBrandModelsList) {

		if (truckBrandModelsList != null && truckBrandModelsList.size() > 0) {

			try {
				String brandName = (String) ((HashMap<String, Object>) truckBrandModelsList.get(0)).get(ResponseConstantsOfTruck.TRUCKBRANDNAME);

				ArrayList<Object> brandsList = new ArrayList<Object>();
				HashMap<String, Object> modelsMap = new HashMap<String, Object>();

				ArrayList<Object> subModelsList = new ArrayList<Object>();

				int brandId = 0;
				int modelsSize = truckBrandModelsList.size();
				int modelId = 0;
				for (modelId = 0; modelId < modelsSize; modelId++) {
					brandName = (String) ((HashMap<String, Object>) truckBrandModelsList.get(modelId)).get(ResponseConstantsOfTruck.TRUCKBRANDNAME);
					if (modelId != modelsSize - 1
							&& brandName.equalsIgnoreCase((String) ((HashMap<String, Object>) truckBrandModelsList.get(modelId + 1)).get(ResponseConstantsOfTruck.TRUCKBRANDNAME))) {
						subModelsList.add(brandId, truckBrandModelsList.get(modelId));
						brandId++;
					} else {
						subModelsList.add(brandId, truckBrandModelsList.get(modelId));
						modelsMap = new HashMap<String, Object>();
						modelsMap.put("brandModels", subModelsList);
						modelsMap.put(ResponseConstantsOfTruck.TRUCKBRANDNAME,brandName);
						modelsMap.put(ResponseConstantsOfTruck.TRUCKBRANDID,((HashMap<String, Object>) truckBrandModelsList.get(modelId)).get(ResponseConstantsOfTruck.TRUCKBRANDID));
						brandsList.add(modelsMap);
						subModelsList = new ArrayList<Object>();
						brandId = 0;
					}
				}
				return brandsList;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;	
	}

	/*
	 *Returns date of next fifteenth day
	 */
	
	@Override
	public String getEarlierDate(Date yourDate) {
		// get Calendar instance
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));

		cal.setTime(yourDate);

		
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+15);

		// convert to date
		/*Date myDate = cal.getTime();*/
		return dateFormat.format(cal.getTime());
	}

	/*
	 * returns by date between 15 days,before or after
	 */
	
	
	@Override
	public int dateComparator(String yourDate) {
		try{

			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			sdf1.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
			Date minDate = sdf.parse(getCurrentFormattedDate());
			Date maxDate =  sdf.parse(getEarlierDate(sdf.parse(getCurrentFormattedDate())));
			Date dateToCompare = sdf1.parse(yourDate);

			System.out.println("compareTo minDate - "+dateToCompare.compareTo(minDate));
			System.out.println("compareTo maxDate - "+dateToCompare.compareTo(maxDate));
			if(dateToCompare.compareTo(minDate) >= 0 && dateToCompare.compareTo(maxDate) <= 0){
				return 1;
			}else if(dateToCompare.compareTo(minDate) <= 0) {
				return 0;
			}else{
				return -1;
			}

		}catch(ParseException ex){
			ex.printStackTrace();
			return -1;
		}

	}

	/*
	 * returns formated date time
	 */

	public String getCurrentFormattedDate() {
		DateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		//get current date time with Date()
		

		//get current date time with Calendar()
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		return dateFormat.format(cal.getTime());
	}

	/*
	 * returns color code by policy Expiry and registration Expiry
	 */
	
	
	public String getColorCode(int policyExpiryCode,int reRegExpiryCode){
		String colorCode = null;
		if(policyExpiryCode == 1 && reRegExpiryCode == 1){
			colorCode = "#0000FF";
		}else if(policyExpiryCode == 0 && reRegExpiryCode == 1){
			colorCode = "#FF0000";
		}else if(policyExpiryCode == -1 && reRegExpiryCode == 1){
			colorCode = "#0000FF";
		}else if(policyExpiryCode == 1 && reRegExpiryCode == -1){
			colorCode = "#0000FF";
		}else if(policyExpiryCode == 0 && reRegExpiryCode == -1){
			colorCode = "#FF0000";
		}else if(policyExpiryCode == -1 && reRegExpiryCode == -1){
			colorCode = "#000";
		}else if(policyExpiryCode == 1 && reRegExpiryCode == 0){
			colorCode = "#FF0000";
		}else if(policyExpiryCode == 0 && reRegExpiryCode == 0){
			colorCode = "#FF0000";
		}else if(policyExpiryCode == -1 && reRegExpiryCode == 0){
			colorCode = "#FF0000";
		}
		return colorCode;
	}

	/*
	 * returns color code by licence Expiry
	 */
	
	public String getColorCodeForLicenceExpire(int licenceExpiryCode){
		String colorCode = null;
		if(licenceExpiryCode == 1){
			colorCode = "#0000FF";
		}else if(licenceExpiryCode == 0){
			colorCode = "#FF0000";
		}else if(licenceExpiryCode == -1){
			colorCode = "#000";
		}
		return colorCode;
	}
	/*
	 *Building array of experience values for inserting into database in bulk
	 *@param driverDetailsList
	 *@return arrayString
	 */

	@Override
	public String buildPreviousExperienceArray(ArrayList<Driver> driverDetailsList){
		String driversArray="";
		if(driverDetailsList!=null && driverDetailsList.size()>0){
			int size = driverDetailsList.size();
			for (int detailId=0;detailId<size;detailId++) {
				Driver driverDetails = driverDetailsList.get(detailId);
				StringBuffer sb = new StringBuffer();
				sb.append(driverDetails.getDriverId()).append(",");
				sb.append("#").append(driverDetails.getTotalWorkExperience()).append("#,");
				sb.append("#").append(driverDetails.getRelatedWorkExperience()).append("#,");
				sb.append("#").append(driverDetails.getCompanyName()).append("#,");
				sb.append("#").append(driverDetails.getPreviousdesignation()).append("#,");
				sb.append("#").append(driverDetails.getDateOfJoining()).append("#,");
				sb.append("#").append(driverDetails.getDateOfRelieving()).append("#,");
				sb.append(driverDetails.getUserId());

				driversArray = driversArray + sb.toString();

				if(detailId!=size-1){
					driversArray = driversArray + "$$";
				}
			}
		}
		return driversArray;
	}


	/*
	 *Building array of experience values for updating into database in bulk
	 *@param driverDetailsList
	 *@return arrayString
	 */

	@Override
	public String buildPreviousExperienceUpdateArray(ArrayList<Driver> driverDetailsList){
		String driversArray="";
		if(driverDetailsList!=null && driverDetailsList.size()>0){
			int size = driverDetailsList.size();
			for (int detailId=0;detailId<size;detailId++) {
				Driver driverDetails = driverDetailsList.get(detailId);
				StringBuffer sb = new StringBuffer();
				sb.append(driverDetails.getDriverId()).append(",");
				sb.append("#").append(driverDetails.getTotalWorkExperience()).append("#,");
				sb.append("#").append(driverDetails.getRelatedWorkExperience()).append("#,");
				sb.append("#").append(driverDetails.getCompanyName()).append("#,");
				sb.append("#").append(driverDetails.getPreviousdesignation()).append("#,");
				sb.append("#").append(driverDetails.getDateOfJoining()).append("#,");
				sb.append("#").append(driverDetails.getDateOfRelieving()).append("#,");
				sb.append(driverDetails.getUserId()).append(",").append(driverDetails.getExperienceId()).append(",");
				sb.append(driverDetails.getIsNew()).append(",").append(driverDetails.getIsDeleted());

				driversArray = driversArray + sb.toString();

				if(detailId!=size-1){
					driversArray = driversArray + "$$";
				}
			}
		}
		return driversArray;
	}

	/*
	 *Building array of subcase services values for inserting/updating/deleting into database in bulk
	 *@param caseServicesList
	 *@return arrayString
	 */

	public String buildSubcaseServicesArray(ArrayList<LinkedHashMap<String, Object>> caseServicesList){
		String servicesArray="";
		if(caseServicesList!=null && caseServicesList.size()>0){
			int size = caseServicesList.size();
			for (int caseServiceId=0;caseServiceId<size;caseServiceId++) {
				LinkedHashMap<String, Object> serviceDetails = (LinkedHashMap<String, Object>) caseServicesList.get(caseServiceId);
				StringBuffer sb = new StringBuffer();
				sb.append(serviceDetails.get("caseServiceId")).append(",");
				sb.append(serviceDetails.get("action")).append(",").append(serviceDetails.get("casefaultRegserviceID"));

				servicesArray = servicesArray + sb.toString();

				if(caseServiceId!=size-1){
					servicesArray = servicesArray + "$$";
				}
			}
		}
		return servicesArray;
	}

	/*
	*calculating distance and time for the available trucks and sorting based on lowest time
	*@param caseServicesList
	*@return arrayString
	*/

	@SuppressWarnings("unchecked")
			public List<HashMap<String, Object>>  sortTrucksByDistance(String serviceLocationLatitude,String serviceLocationLongitude,List<HashMap<String, Object>> responseList){
			for(Iterator<HashMap<String, Object>> iterator=responseList.iterator();iterator.hasNext();){
		
			HashMap<String, Object> location = (HashMap<String, Object>) iterator.next();
		
			StringBuilder builder = new StringBuilder();
			builder.append(CommonUtil.DISTANCE_MATRIX_URL);
			builder.append("origins=");
			builder.append(serviceLocationLatitude).append(",");
			builder.append(serviceLocationLongitude);
			builder.append("&destinations=");
			builder.append(location.get(ResponseConstantsOfDriver.LATITUDE)).append(",");
			builder.append(location.get(ResponseConstantsOfDriver.LONGITUDE));
			builder.append("&key=");
			builder.append(CommonUtil.API_KEY);
		
			try {	
					Map<String,Object> map = new HashMap<String,Object>();
					ObjectMapper mapper = new ObjectMapper();
					
				 //  String formattedString = CommonUtil.DISTANCE_MATRIX_URL+ URLEncoder.encode( builder.toString() , "UTF8" )  ;
					
					String response = new CustomHttpURLConnection().sendGet(builder.toString());
				
					map = mapper.readValue(response, new TypeReference<HashMap<String,Object>>(){});
				
					if(((String) map.get("status")).equalsIgnoreCase("OK")){
							location.put("locationname", (((ArrayList<HashMap<String, Object>>) map.get("destination_addresses")).get(0)));
							List<HashMap<String, Object>> rows = (List<HashMap<String, Object>>) map.get("rows");
						
							for(HashMap<String, Object> row : rows){
							List<HashMap<String, Object>> elements = (List<HashMap<String, Object>>) ((HashMap<String, Object>) row).get("elements");
							for(HashMap<String, Object> element : elements){
						
							Object distance = ((HashMap<String, Object>) element).get("distance");
							if(distance!=null){
								Object distancetext= ((HashMap<String, Object>) distance).get("text");
								Object distancevalue= ((HashMap<String, Object>) distance).get("value");
							
								location.put("distancetext", distancetext);
								location.put("distancevalue", distancevalue);
							}
						
							Object duration = ((HashMap<String, Object>) element).get("duration");
							if(duration!=null){
								Object durationtext= ((HashMap<String, Object>) duration).get("text");
								Object durationvalue= ((HashMap<String, Object>) duration).get("value");
							
								location.put("durationtext", durationtext);
								location.put("durationvalue", durationvalue);
							}
						
							}
							}//end of rows for
						
					}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		
			}
			Iterator<HashMap<String, Object>> iterator=responseList.iterator();
			while(iterator.hasNext()){
				HashMap<String, Object> location = (HashMap<String, Object>) iterator.next();
				if(!location.containsKey("distancevalue") && !location.containsKey("durationvalue")){
					iterator.remove();
				}
			}
		
		
			Collections.sort(responseList, new Comparator<HashMap<String,Object>>() {
			
				@Override
				public int compare(HashMap<String, Object> map1,
				HashMap<String, Object> map2) {
			
				String firstdurationvalue = String.valueOf(map1.get("durationvalue"));
				String seconddurationvalue = String.valueOf(map2.get("durationvalue"));
					if(firstdurationvalue!=null && seconddurationvalue!=null)
						return firstdurationvalue.compareTo(seconddurationvalue);
					else
						return 0;
				}
			
			});
			return responseList;
	}

	/*
	*Building array of experience values for updating into database in bulk
	*@param driverDetailsList
	*@return arrayString
	*/
	@Override
	public String buildSchedulecasesArray(ArrayList<LinkedHashMap<String,Object>> caseDetailsList){
	String casesArray="";
	if(caseDetailsList!=null && caseDetailsList.size()>0){
		int size = caseDetailsList.size();
			for (int detailId=0;detailId<size;detailId++) {
				LinkedHashMap<String, Object> caseDetails = caseDetailsList.get(detailId);
				  StringBuffer sb = new StringBuffer();
				  sb.append("#").append(UtilFunctions.getCurrentDate()).append(" ").append(caseDetails.get("actualarrival")!=null ? caseDetails.get("actualarrival") : "").append("#,");
				  sb.append(caseDetails.get("schedulestatusID")!=null ? caseDetails.get("schedulestatusID") : "").append(",");
				  sb.append(caseDetails.get("caseId")).append(",");
				  sb.append(caseDetails.get("userId"));
				  casesArray = casesArray + sb.toString();
				 
				  if(detailId!=size-1){
					  casesArray = casesArray + "$$";
				  }
			}
	}
	return casesArray;
	}
	
	
	/*
	*Building array of shiftTimings to insert into database in bulk
	*@param shifttimingsList
	*@return arrayString
	*/

	@Override
	public String buildShiftTimingsArray(ArrayList<LinkedHashMap<String, Object>> shifttimingsList){
		String shifttimingsArray="";
			if(shifttimingsList!=null && shifttimingsList.size()>0){
				int size = shifttimingsList.size();
				for (int shiftDetailId=0;shiftDetailId<size;shiftDetailId++) {
					LinkedHashMap<String, Object> shiftDetails = shifttimingsList.get(shiftDetailId);
					StringBuffer sb = new StringBuffer();
					sb.append(shiftDetails.get("Id")).append(",");
					sb.append("#").append(shiftDetails.get("shiftName")).append("#,");
					sb.append("#").append(shiftDetails.get("startTime")).append("#,");
					sb.append("#").append(shiftDetails.get("endTime")).append("#,");
					sb.append("#").append(shiftDetails.get("formattedStartTime")).append("#,");
					sb.append("#").append(shiftDetails.get("formattedEndTime")).append("#,");
					sb.append(shiftDetails.get("vehiclesNeeded")).append(",");
					sb.append(shiftDetails.get("driversNeeded")).append(",");
					sb.append(shiftDetails.get("shiftRWD"));
					 //1-insert 2-update 3-delete
					shifttimingsArray = shifttimingsArray + sb.toString();
				 
					if(shiftDetailId!=size-1){
						shifttimingsArray = shifttimingsArray + "$$";
					}
				}
			}
		return shifttimingsArray;
	}
	
	
	/*
	*Building array of shiftTimings to insert into database in bulk
	*@param shifttimingsList
	*@return arrayString
	*/

	@Override
	public String buildModuleAccesTypeArray(ArrayList<LinkedHashMap<String, Object>> moduleList){
		String moduleArray="";
		if(moduleList!=null && moduleList.size()>0){
			int size = moduleList.size();
			for (int i=0;i<size;i++) {
				LinkedHashMap<String, Object> moduleDetails = moduleList.get(i);
				 StringBuffer sb = new StringBuffer();
				 sb.append(moduleDetails.get("id")).append(",");
				 boolean canView = (boolean) moduleDetails.get("canView");
				 boolean canAdd = (boolean) moduleDetails.get("canAdd");
				 boolean canEdit = (boolean) moduleDetails.get("canEdit");
				 boolean canDelete = (boolean) moduleDetails.get("canDelete");
					 sb.append(canView).append(",");
					 sb.append(canAdd).append(",");
					 sb.append(canEdit).append(",");
					 sb.append(canDelete).append("");
					 
					 moduleArray = moduleArray + sb.toString();
				 
				 if(i!=size-1){
					 moduleArray = moduleArray + "$$";
				 }
			}
		}
		return moduleArray;
	}
	
	public void sendMail(String msgBody,String subject,String toEmail){
		// ...
		// Create some properties and get the default Session.
		Properties props = System.getProperties(); 
		props.put("mail.host", "smtp.google.com");
		props.put("mail.transport.protocol", "smtp");
		Session session = Session.getDefaultInstance(props, null);


		try {
		    Message msg = new MimeMessage(session);
		    msg.setFrom(new InternetAddress("admin@example.com", "TRAC Admin"));
		    msg.addRecipient(Message.RecipientType.TO, new InternetAddress("toEmail", "Mr. User"));
		    msg.setSubject(subject);
		    msg.setText(msgBody);
		    Transport.send(msg);

		} catch (AddressException e) {
		    // ...
		} catch (MessagingException e) {
		    // ...
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	/*
	 *Building array of vehicles for updating display order
	 *@param vehicelsList
	 *@return arrayString
	 */

	public String buildVehicleDisplayOrderArray(ArrayList<LinkedHashMap<String, Object>> vehicelsList){
		String displayOrderArray="";
		if(vehicelsList!=null && vehicelsList.size()>0){
			int size = vehicelsList.size();
			for (int vehicle=0;vehicle<size;vehicle++) {
				LinkedHashMap<String, Object> vehicleDetails = (LinkedHashMap<String, Object>) vehicelsList.get(vehicle);
				StringBuffer sb = new StringBuffer();
				sb.append(vehicleDetails.get("displayOrder")).append(",");
				sb.append(vehicleDetails.get("vehicleId"));

				displayOrderArray = displayOrderArray + sb.toString();

				if(vehicle!=size-1){
					displayOrderArray = displayOrderArray + "$$";
				}
			}
		}
		return displayOrderArray;
	}


}
