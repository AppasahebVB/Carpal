package trac.dao.customerdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


import trac.beans.customerbean.Customer;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfUser;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.DBUtilInterface;

/**
 *  Web User(Customer) related database Operations are handled in this class
 *  @methods editUserWithLocationDetails,fetchCustomerWithLocationDetails,fetchCaseDetailsById,fetchCaseDetails,saveCustomerSatisfaction
 */


public class CustomerDaoOperations {

	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	boolean issuccess;
	/**
	 * This method is used to register the details of customer
	 * customerDetails,location object contains customer details&adress
	 * 
	 * @param customerDetails,location
	 * @spname usp_Customer_Registration_Insert
	 * @return success or failure
	 *
	 */
	public Object addUserWithLocationDetails(Customer customerRequest) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_REGISTRATION_SPNAME)
		.returningResultSet("customerresponse",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID,rs.getString("customerUID"));
						responseMap.put(ResponseConstantsOfUserLocation.LOCATIONID,rs.getInt("locationId"));
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Customer Registered Successfully.");
					} else {
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Customer already Registered/Invalid Data");
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"imobileNumber","iemailID","ifirstName","ilastName","icreatedBy",
				"iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"};


		Object[] inParamaterValues = new Object[]{customerRequest.getMobileNumber(),customerRequest.getEmailId(),customerRequest.getFirstName(),customerRequest.getLastName(),customerRequest.getUpdatedBy(),
				customerRequest.getCustomerLocationLatitude(),customerRequest.getCustomerLocationLongitude(),customerRequest.getCustomerLocationName(),customerRequest.getArea(),customerRequest.getStreet(),customerRequest.getBuilding(),customerRequest.getLandmark(),customerRequest.getCustomerLocationCity(),customerRequest.getPoBox(),"",customerRequest.getCustomerLocationCountryCode()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");

		return returnjson;

	}

	/**
	 * This method is used to edit the customerDetails,locationdetails of customer
	 * 
	 * 
	 * @param customerDetails,location
	 * @spname usp_Customer_Registration_Update
	 * @return true or false
	 * @see
	 */
	public Object editUserWithLocationDetails(Customer customerRequest) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_UPDATION_SPNAME)
		.returningResultSet("customerresponse",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum)  {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID,rs.getString("customerUID"));
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					} else {
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"isCustomer","icustomerUID","iupdatedBy","imobileNumber","iemailID","ifirstName","ilastName",
				"iLocID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"};
		
		if(!customerRequest.getIsCustomer() && customerRequest.getLocationId().length() == 0){
			customerRequest.setLocationId("0");
		}

		Object[] inParamaterValues = {customerRequest.getIsCustomer(),customerRequest.getCustomerId(),customerRequest.getUpdatedBy(),customerRequest.getMobileNumber(),customerRequest.getEmailId(),customerRequest.getFirstName(),customerRequest.getLastName(),
				customerRequest.getLocationId(),customerRequest.getCustomerLocationLatitude(),customerRequest.getCustomerLocationLongitude(),customerRequest.getCustomerLocationName(),customerRequest.getArea(),customerRequest.getStreet(),customerRequest.getBuilding(),customerRequest.getLandmark(),customerRequest.getCustomerLocationCity(),customerRequest.getPoBox(),"",customerRequest.getCustomerLocationCountryCode()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");

		return returnjson;

	}

	
	/**
	 * This method is used to fetch the customerDetails,locationdetails of customer
	 * 
	 * 
	 * @param customerDetails
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */


	public Object fetchCustomerWithLocationDetails(Customer customerRequest) {

		
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_FETCH_SPNAME)
		.returningResultSet("customerresponse",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					issuccess = rs.getBoolean("issuccess");
					if (issuccess) {
						
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID,rs.getString("customerUID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMEREMAILID,rs.getString("emailID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERMOBILE,rs.getString("mobileNumber"));
						responseMap.put(ResponseConstantsOfUser.FIRSTNAME,rs.getString("firstName"));
						responseMap.put(ResponseConstantsOfUser.LASTNAME,rs.getString("lastName"));

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE,rs.getString("GPSLocationLat"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE,rs.getString("GPSLocationLong"));

						responseMap.put("customerLocationName",rs.getString("AddressTitle"));
						responseMap.put(ResponseConstantsOfUserLocation.STREET,rs.getString("Street"));
						responseMap.put(ResponseConstantsOfUserLocation.AREA,rs.getString("Area"));
						responseMap.put(ResponseConstantsOfUserLocation.BUILDING,rs.getString("Building"));
						responseMap.put(ResponseConstantsOfUserLocation.LANDMARK,rs.getString("LandMark"));
						responseMap.put("customerLocationCity",rs.getString("City"));
						responseMap.put("poBox",rs.getString("POBox"));
						responseMap.put("customerLocationCountryName",rs.getString("countryName"));
						responseMap.put("customerLocationCountryCode",rs.getString("CountryISO"));
						responseMap.put("customerLocationId",rs.getString("LocID"));
						//responseMap.put("serviceMessage",rs.getString("serviceMessage"));

						responseMap.put("isExisting",true);
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
					}else{
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						responseMap.put("serviceMessage",rs.getString("serviceMessage"));
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
		}).returningResultSet("vehicleDetailsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if(issuccess){
					responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
					if(rs.getInt("isFavorite") == 1)
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
					else
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);
					
					
					responseMap.put("brandAndModel", rs.getString("brandName")+","+rs.getString("modelName")+","+rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));

					//if registerd through web
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPTYPEID, rs.getInt("memberShipTypeId"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPSTART, rs.getString("memberShipStart"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPEND, rs.getString("memberShipEnd"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLECOUNTRYCODE, rs.getString("countryISO"));
					responseMap.put(ResponseConstantsOfVehicle.COUNTRYNAME, rs.getString("countryName"));
					responseMap.put(ResponseConstantsOfVehicle.CITY, rs.getString("city"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
					responseMap.put(ResponseConstantsOfVehicle.COLOR, rs.getString("color"));
					responseMap.put(ResponseConstantsOfVehicle.DATEOFPURCHASE, rs.getString("dateOfPurchase"));
					responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
					responseMap.put(ResponseConstantsOfVehicle.MILEAGE, rs.getString("mileage"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));

					}
					return responseMap;
					
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"isearchBy"};

		Object[] inParamaterValues = new Object[]{customerRequest.getSearchBy()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> customersList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehiclesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleDetailsList");
		List<Object> responseList = new ArrayList<Object>();
		if(!(customersList!=null && customersList.size()>0)){
			customersList = new ArrayList<HashMap<String, Object>>();
			customersList.add(new HashMap<String, Object>()); 
		}
		if(!(vehiclesList!=null && vehiclesList.size()>0)){
			vehiclesList = new ArrayList<HashMap<String, Object>>();
		}
		responseList.add(customersList);
		responseList.add(vehiclesList);
		return responseList;
	}

	
	/**
	 * This method is used to fetch the customerDetails,locationdetails of customer
	 * 
	 * 
	 * @param customerDetails
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */


	public Object fetchCaseDetailsById(Customer customerRequest) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.FETCH_CASE_DETAILS_BY_ID)
		.returningResultSet("customerresponse",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID,rs.getString("customerUID"));
						
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID,rs.getString("vcaseID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMEREMAILID,rs.getString("emailID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERMOBILE,rs.getString("mobileNumber"));
						responseMap.put(ResponseConstantsOfUser.FIRSTNAME,rs.getString("firstName"));
						responseMap.put(ResponseConstantsOfUser.LASTNAME,rs.getString("lastName"));
						responseMap.put("isDestinationLocationSet",rs.getBoolean("isDestinationLocationSet"));
					
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE,rs.getString("GPSLocationLat"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE,rs.getString("GPSLocationLong"));

						responseMap.put("customerLocationName",rs.getString("AddressTitle"));
						responseMap.put(ResponseConstantsOfUserLocation.STREET,rs.getString("Street"));
						responseMap.put(ResponseConstantsOfUserLocation.AREA,rs.getString("Area"));
						responseMap.put(ResponseConstantsOfUserLocation.BUILDING,rs.getString("Building"));
						responseMap.put(ResponseConstantsOfUserLocation.LANDMARK,rs.getString("LandMark"));
						responseMap.put("customerLocationCity",rs.getString("City"));
						responseMap.put("poBox",rs.getString("POBox"));
						responseMap.put("customerLocationCountryName",rs.getString("countryName"));
						responseMap.put("customerLocationCountryCode",rs.getString("CountryISO"));
						responseMap.put("customerLocationId",rs.getString("LocID"));


						responseMap.put("isExisting",true);
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
					}else{
						return new HashMap<String, Object>();
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
		}).returningResultSet("vehicleDetails",  new RowMapper<HashMap<String, Object>>()
						{
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					{
						try{
							HashMap<String, Object> responseMap = new HashMap<String, Object>();

							responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
							if(rs.getInt("isFavorite") == 1)
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
							else
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);


							responseMap.put("brandAndModel", rs.getString("brandName")+","+rs.getString("modelName")+","+rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getString("modelName"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("vehicleModelID"));
							responseMap.put("selectedModelId", rs.getString("vehicleModelID"));
							responseMap.put("selectedBrandId", rs.getString("vehicleBrandID"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("vehicleBrandID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID,rs.getString("brandName"));
							responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));

							//if registerd through web
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPTYPEID, rs.getInt("memberShipTypeId"));
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPSTART, rs.getString("memberShipStart"));
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPEND, rs.getString("memberShipEnd"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLECOUNTRYCODE, rs.getString("countryISO"));
							responseMap.put("vehcileCountryISO", rs.getString("countryName"));
							responseMap.put(ResponseConstantsOfVehicle.CITY, rs.getString("city"));
							responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
							responseMap.put(ResponseConstantsOfVehicle.COLOR, rs.getString("color"));
							responseMap.put(ResponseConstantsOfVehicle.DATEOFPURCHASE, rs.getString("dateOfPurchase"));
							responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
							responseMap.put(ResponseConstantsOfVehicle.MILEAGE, rs.getString("mileage"));
							responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));


							return responseMap;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
						}
					}
						}).returningResultSet("vehicleServiceLocationDetails", new RowMapper<Map<String,Object>>()
										{
									@Override
									public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
										try{
											Map<String,Object> customerLocationMap = new HashMap<String, Object>();

                                            
											customerLocationMap.put("serviceLocationName", rs.getString("Description"));

											customerLocationMap.put("serviceLocationLatitude", rs.getString("GPSLocationLat"));
											customerLocationMap.put("serviceLocationLongitude", rs.getString("GPSLocationLong"));
											customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
											//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
											customerLocationMap.put("serviceLocationCity", rs.getString("city"));
											/*customerLocationMap.put("serviceLocationCity", rs.getString("state"));*/
											customerLocationMap.put("serviceLocationCountryName", rs.getString("countryName"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

											// if registerd through web
											customerLocationMap.put("serviceLocationArea", rs.getString("Area"));
											customerLocationMap.put("serviceLocationStreet", rs.getString("Street"));
											customerLocationMap.put("serviceLocationBuilding", rs.getString("Building"));
											customerLocationMap.put("serviceLocationLandmark", rs.getString("LandMark"));
											customerLocationMap.put("serviceLocationpoBox", rs.getString("POBox"));
											customerLocationMap.put("saveForReference", rs.getBoolean("isReferencedInFuture"));
											
											customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
											customerLocationMap.put("serviceRequiredTypeId", rs.getInt("serviceTimeTypeID"));
											
											String serviceTime = rs.getString("serviceTime");
											customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, serviceTime);
											if(serviceTime!=null && serviceTime.length() > 0){
												customerLocationMap.put("hh", UtilFunctions.dateToHoursMin(serviceTime)[0]);
												customerLocationMap.put("mm", UtilFunctions.dateToHoursMin(serviceTime)[1]);
											}


											if(rs.getInt("isServiceLocation") == 1)
												customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
											else
												customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

											if(rs.getInt("isDestinationLocation") == 1)
												customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
											else
												customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

											return customerLocationMap;
										}catch(SQLException ex){
											return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
										}
									}
										}).returningResultSet("caseDetails",
												new RowMapper<Map<String, Object>>() {
											@Override
											public Map<String, Object> mapRow(ResultSet rs,int rowNum){
												try{
													Boolean isSuccess = rs.getBoolean("isSuccess");
													Map<String, Object> returnResult = new HashMap<String, Object>();

													if (isSuccess) {

														returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
														returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
														returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
														returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
														returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
														returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
														returnResult.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("FaultId"));
														returnResult.put("isTowDetailsUpdated", rs.getBoolean("isCaseOnJob"));
														returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

														if(rs.getString("FaultServiceName")==null)
															returnResult.put("isSelected",false);
														else
															returnResult.put("isSelected",true);	

														returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
														returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
														returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
														returnResult.put("faultName", rs.getString("FaultName"));
														returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
														returnResult.put(CommonResponseConstants.ISSUCCESS, true);

														returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));
														returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));

														returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
														
														String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
														returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
														returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
														returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
														long differenceTime = 0;
														long countDownMilliSeconds=0;
														if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
															
															 if(lastStatusTime.length() > 0) {
																 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
															     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
															 }
															     
															
														}
														returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

														returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
														returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
														returnResult.put("lastStatusTime",lastStatusTime);
														returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
														returnResult.put("differenceTime",differenceTime );

														

													}else{ 
														returnResult.put(CommonResponseConstants.ISSUCCESS, false);
														returnResult.put(CommonResponseConstants.SERVICEMESSAGE, "Case not found.");
													}

													return returnResult;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
												}
											}
										}).returningResultSet("casefaultsservices",
												new RowMapper<Map<String, Object>>() {
											@Override
											public Map<String, Object> mapRow(ResultSet rs,int rowNum){
												try{
													Map<String, Object> returnResult = new HashMap<String, Object>();
													returnResult.put("checked",false);
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICE_ID,rs.getInt("faultServiceId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,rs.getInt("faultId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_ID,rs.getInt("serviceId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,rs.getString("faultdesc"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_DESC,rs.getString("servicedesc"));
													returnResult.put("isDisabled", false);
													return returnResult;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
												}
											}
										}).returningResultSet("vehicleBrandsList",  new RowMapper<HashMap<String, Object>>(){
											@Override
											public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
											{
												try{
													HashMap<String, Object> responseMap = new HashMap<String, Object>();
									
													responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName").trim());
													responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
													responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getLong("vehicleModelID"));
													responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName").trim());
													
													return responseMap;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
											    }
											}
										}).returningResultSet("commentsList",  new RowMapper<HashMap<String, Object>>(){
											@Override
											public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
											{
												try{
													String userName = rs.getString("userName");
													if(userName == null || userName.length() ==0)
														userName = rs.getString("displayName");
													HashMap<String, Object> responseMap = new HashMap<String, Object>();
													responseMap.put("commentId", rs.getString("commentId"));
													responseMap.put("caseId", rs.getInt("caseId"));
													responseMap.put("createdBy", userName);
													responseMap.put(ResponseConstantsOfCustomer.CASECOMMENTS, rs.getString(ResponseConstantsOfCustomer.CASECOMMENTS));
													responseMap.put("createdAt", rs.getString("createdAt"));
													return responseMap;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
											    }
											}
										});

		// IN parameters for stored procedure
		String[] inParamaters = {"isearchBy","icaseId","isCustomer"};

		Object[] inParamaterValues = new Object[]{"",customerRequest.getSearchBy(),customerRequest.getIsCustomer()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> customersList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleDetails =  (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleDetails");;
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleServiceLocationDetails = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleServiceLocationDetails");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> faultservicesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casefaultsservices");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> commentsList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("commentsList");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleBrandsList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleBrandsList");

		

		HashMap<String, Object> caseDetailsMap = new HashMap<String, Object>();	
		List<Object> responseList = new ArrayList<Object>();
		if(customersList!=null && customersList.size()>0){
			caseDetailsMap.putAll(customersList.get(0));
		}
		if(vehicleDetails!=null && vehicleDetails.size()>0){
			caseDetailsMap.putAll(vehicleDetails.get(0));
		}
		if(vehicleServiceLocationDetails!=null && vehicleServiceLocationDetails.size()>0){
			caseDetailsMap.putAll(vehicleServiceLocationDetails.get(0));
		}
		responseList.add(caseDetailsMap);
		responseList.add(subcasesList);
		responseList.add(faultservicesList);
		responseList.add(commentsList);
		responseList.add(vehicleBrandsList);
		
		return responseList;
	}

	
	/**
	 * This method is used to fetch the customerDetails,locationdetails of customer
	 * 
	 * 
	 * @param customerDetails
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */


	public Object fetchCaseDetails(Customer customerRequest) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.FETCH_CASE_DETAILS)
		.returningResultSet("customerresponse",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID,rs.getString("customerUID"));
						/*responseMap.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString(ResponseConstantsOfCustomer.CASECOMMENTS));*/
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID,rs.getString("vcaseID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMEREMAILID,rs.getString("emailID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERMOBILE,rs.getString("mobileNumber"));
						responseMap.put(ResponseConstantsOfUser.FIRSTNAME,rs.getString("firstName"));
						responseMap.put(ResponseConstantsOfUser.LASTNAME,rs.getString("lastName"));
						responseMap.put("isDestinationLocationSet",rs.getBoolean("isDestinationLocationSet"));
					
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE,rs.getString("GPSLocationLat"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE,rs.getString("GPSLocationLong"));

						responseMap.put("customerLocationName",rs.getString("AddressTitle"));
						responseMap.put(ResponseConstantsOfUserLocation.STREET,rs.getString("Street"));
						responseMap.put(ResponseConstantsOfUserLocation.AREA,rs.getString("Area"));
						responseMap.put(ResponseConstantsOfUserLocation.BUILDING,rs.getString("Building"));
						responseMap.put(ResponseConstantsOfUserLocation.LANDMARK,rs.getString("LandMark"));
						responseMap.put("customerLocationCity",rs.getString("City"));
						responseMap.put("poBox",rs.getString("POBox"));
						responseMap.put("customerLocationCountryName",rs.getString("countryName"));
						responseMap.put("customerLocationCountryCode",rs.getString("CountryISO"));
						responseMap.put("customerLocationId",rs.getString("LocID"));

						System.out.println("customerresponse");

						responseMap.put("isExisting",true);
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
					}else{
						return new HashMap<String, Object>();
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
		}).returningResultSet("vehicleDetailsList",  new RowMapper<HashMap<String, Object>>()
				{
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
			{
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
                    
					responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
					if(rs.getInt("isFavorite") == 1)
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
					else
						responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);


					responseMap.put("brandAndModel", rs.getString("brandName")+","+rs.getString("modelName")+","+rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getInt("vehicleModelID"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
					responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
					responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));

					//if registerd through web
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPTYPEID, rs.getInt("memberShipTypeId"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPSTART, rs.getString("memberShipStart"));
					responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPEND, rs.getString("memberShipEnd"));
					responseMap.put(ResponseConstantsOfVehicle.VEHICLECOUNTRYCODE, rs.getString("countryISO"));
					responseMap.put(ResponseConstantsOfVehicle.COUNTRYNAME, rs.getString("countryName"));
					responseMap.put(ResponseConstantsOfVehicle.CITY, rs.getString("city"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
					responseMap.put(ResponseConstantsOfVehicle.COLOR, rs.getString("color"));
					responseMap.put(ResponseConstantsOfVehicle.DATEOFPURCHASE, rs.getString("dateOfPurchase"));
					responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
					responseMap.put(ResponseConstantsOfVehicle.MILEAGE, rs.getString("mileage"));
					responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));

					System.out.println("vehicleDetailsList");
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
				}
			}
				}).returningResultSet("vehicleDetails",  new RowMapper<HashMap<String, Object>>()
						{
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
					{
						try{
							HashMap<String, Object> responseMap = new HashMap<String, Object>();

							responseMap.put(CommonResponseConstants.VEHICLEID, rs.getInt("vehicleID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCILEPIC, rs.getString("vehiclePicURL"));
							if(rs.getInt("isFavorite") == 1)
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, true);
							else
								responseMap.put(ResponseConstantsOfVehicle.FAVORITE, false);


							responseMap.put("brandAndModel", rs.getString("brandName")+","+rs.getString("modelName")+","+rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getString("modelName"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("vehicleModelID"));
							responseMap.put("selectedModelId", rs.getString("vehicleModelID"));
							responseMap.put("selectedBrandId", rs.getString("vehicleBrandID"));
							//responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("vehicleBrandID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLENAME, rs.getString("vehicleName"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEMODELYEAR, rs.getString("modelYear"));
							responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID,rs.getString("brandName"));
							responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));

							//if registerd through web
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPTYPEID, rs.getInt("memberShipTypeId"));
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPSTART, rs.getString("memberShipStart"));
							responseMap.put(ResponseConstantsOfVehicle.MEMBERSHIPEND, rs.getString("memberShipEnd"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLECOUNTRYCODE, rs.getString("countryISO"));
							responseMap.put("vehcileCountryISO", rs.getString("countryName"));
							responseMap.put(ResponseConstantsOfVehicle.CITY, rs.getString("city"));
							responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));
							responseMap.put(ResponseConstantsOfVehicle.COLOR, rs.getString("color"));
							responseMap.put(ResponseConstantsOfVehicle.DATEOFPURCHASE, rs.getString("dateOfPurchase"));
							responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
							responseMap.put(ResponseConstantsOfVehicle.MILEAGE, rs.getString("mileage"));
							responseMap.put(ResponseConstantsOfVehicle.PLATECODE, rs.getString("plateCode"));

							System.out.println("vehicleDetails");
							return responseMap;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
						}
					}
						}).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
								{
							@Override
							public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
								try{
									Map<String,Object> customerLocationMap = new HashMap<String, Object>();


									customerLocationMap.put("serviceLocation", rs.getString("Description"));

									customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE, rs.getString("GPSLocationLat"));
									customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
									customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
									//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));
									customerLocationMap.put("saveForReference", rs.getBoolean("isReferencedInFuture"));

									// if registerd through web
									customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
									customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

									customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
									customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE, rs.getInt("serviceTimeTypeID"));
									customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, rs.getString("serviceTime"));



									if(rs.getInt("isServiceLocation") == 1)
										customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
									else
										customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

									if(rs.getInt("isDestinationLocation") == 1)
										customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
									else
										customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

									System.out.println("locationResult");
									
									return customerLocationMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
								}
							}
								}).returningResultSet("vehicleServiceLocationDetails", new RowMapper<Map<String,Object>>()
										{
									@Override
									public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
										try{
											Map<String,Object> customerLocationMap = new HashMap<String, Object>();

                                            
											customerLocationMap.put("serviceLocationName", rs.getString("Description"));

											customerLocationMap.put("serviceLocationLatitude", rs.getString("GPSLocationLat"));
											customerLocationMap.put("serviceLocationLongitude", rs.getString("GPSLocationLong"));
											customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
											//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
											customerLocationMap.put("serviceLocationCity", rs.getString("city"));
											/*customerLocationMap.put("serviceLocationCity", rs.getString("state"));*/
											customerLocationMap.put("serviceLocationCountryName", rs.getString("countryName"));
											customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

											// if registerd through web
											customerLocationMap.put("serviceLocationArea", rs.getString("Area"));
											customerLocationMap.put("serviceLocationStreet", rs.getString("Street"));
											customerLocationMap.put("serviceLocationBuilding", rs.getString("Building"));
											customerLocationMap.put("serviceLocationLandmark", rs.getString("LandMark"));
											customerLocationMap.put("serviceLocationpoBox", rs.getString("POBox"));
											customerLocationMap.put("saveForReference", rs.getBoolean("isReferencedInFuture"));
											
											customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
											customerLocationMap.put("serviceRequiredTypeId", rs.getInt("serviceTimeTypeID"));
											
											String serviceTime = rs.getString("serviceTime");
											customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, serviceTime);
											if(serviceTime!=null && serviceTime.length() > 0){
												customerLocationMap.put("hh", (serviceTime.length() > 0) ? UtilFunctions.dateToHoursMin(serviceTime)[0] :"");
												customerLocationMap.put("mm", (serviceTime.length() > 0) ? UtilFunctions.dateToHoursMin(serviceTime)[1] :"");
											}


											if(rs.getInt("isServiceLocation") == 1)
												customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
											else
												customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

											if(rs.getInt("isDestinationLocation") == 1)
												customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
											else
												customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

											System.out.println("vehicleServiceLocationDetails");
											
											return customerLocationMap;
										}catch(SQLException ex){
											return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
										}
									}
										}).returningResultSet("caseDetails",
												new RowMapper<Map<String, Object>>() {
											@Override
											public Map<String, Object> mapRow(ResultSet rs,int rowNum){
												try{
													Boolean isSuccess = rs.getBoolean("isSuccess");
													Map<String, Object> returnResult = new HashMap<String, Object>();

													if (isSuccess) {

														returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
														returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
														returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
														returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
														returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
														returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
														returnResult.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("FaultId"));
														returnResult.put("isTowDetailsUpdated", rs.getBoolean("isCaseOnJob"));
														returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

														if(rs.getString("FaultServiceName")==null)
															returnResult.put("isSelected",false);
														else
															returnResult.put("isSelected",true);	

														returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
														returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
														returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
														returnResult.put("faultName", rs.getString("FaultName"));
														returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
														returnResult.put(CommonResponseConstants.ISSUCCESS, true);

														returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));
														returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));

														returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
														
														
														
														returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
														returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
														returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
														returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
														String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");

														long differenceTime = 0;
														long countDownMilliSeconds=0;
														if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
															 if(lastStatusTime.length() > 0) {
																 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
															     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
															 }
														}
														returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

														returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
														returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
														returnResult.put("lastStatusTime",lastStatusTime);
														returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
														returnResult.put("differenceTime",differenceTime );
														
													}else{ 
														returnResult.put(CommonResponseConstants.ISSUCCESS, false);
														returnResult.put(CommonResponseConstants.SERVICEMESSAGE, "Case not found.");
													}

													return returnResult;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
												}
											}
										}).returningResultSet("casefaultsservices",
												new RowMapper<Map<String, Object>>() {
											@Override
											public Map<String, Object> mapRow(ResultSet rs,int rowNum){
												try{
													Map<String, Object> returnResult = new HashMap<String, Object>();
													returnResult.put("checked",false);
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICE_ID,rs.getInt("faultServiceId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,rs.getInt("faultId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_ID,rs.getInt("serviceId"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,rs.getString("faultdesc"));
													returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_DESC,rs.getString("servicedesc"));
													returnResult.put("isDisabled", false);
													
													return returnResult;
												}catch(SQLException ex){
													return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
												}
											}
										}).returningResultSet("assistanceStatusList", new RowMapper<Map<String,Object>>()
														{
													@Override
													public Map<String,Object> mapRow(ResultSet rs, int rowNum)
															throws SQLException {try{
																Map<String,Object> responseMap = new HashMap<String, Object>();
																responseMap.put("schedulestatusID", rs.getInt("ID"));
																responseMap.put("schedulestatus", rs.getString("Description"));
																
																return responseMap;	
															}
															catch(SQLException ex){
																return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
															}
													}
									}).returningResultSet("vehicleBrandsList",  new RowMapper<HashMap<String, Object>>(){
										@Override
										public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
										{
											try{
												HashMap<String, Object> responseMap = new HashMap<String, Object>();
								
												responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDNAME, rs.getString("brandName").trim());
												responseMap.put(ResponseConstantsOfVehicle.VEHCICLEBRANDID, rs.getInt("vehicleBrandID"));
												responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELID, rs.getLong("vehicleModelID"));
												responseMap.put(ResponseConstantsOfVehicle.VEHCICLEMODELNAME, rs.getString("modelName").trim());
												
												return responseMap;
											}catch(SQLException ex){
												return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
										    }
										}
									}).returningResultSet("commentsList",  new RowMapper<HashMap<String, Object>>(){
										@Override
										public HashMap<String, Object> mapRow(ResultSet rs, int rowNum)
										{
											try{
												String userName = rs.getString("userName");
												if(userName == null || userName.length() ==0)
													userName = rs.getString("displayName");
												HashMap<String, Object> responseMap = new HashMap<String, Object>();
												responseMap.put("commentId", rs.getString("commentId"));
												responseMap.put("caseId", rs.getInt("caseId"));
												responseMap.put("createdBy", userName);
												responseMap.put(ResponseConstantsOfCustomer.CASECOMMENTS, rs.getString(ResponseConstantsOfCustomer.CASECOMMENTS));
												responseMap.put("createdAt", rs.getString("createdAt"));
												return responseMap;
											}catch(SQLException ex){
												return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
										    }
										}
									});


		// IN parameters for stored procedure
		String[] inParamaters = {"isearchBy","isCustomer"};

		Object[] inParamaterValues = new Object[]{customerRequest.getSearchBy(),customerRequest.getIsCustomer()};	

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> customersList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehiclesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleDetailsList");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleDetails =  (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleDetails");;
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleServiceLocationList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleServiceLocationDetails = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleServiceLocationDetails");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> faultservicesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casefaultsservices");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> assistanceStatusList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("assistanceStatusList");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> vehicleBrandsList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("vehicleBrandsList");
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> commentsList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("commentsList");


		HashMap<String, Object> caseDetailsMap = new HashMap<String, Object>();	
		List<Object> responseList = new ArrayList<Object>();
		if(customersList!=null && customersList.size()>0){
			caseDetailsMap.putAll(customersList.get(0));
		}
		if(vehicleDetails!=null && vehicleDetails.size()>0){
			caseDetailsMap.putAll(vehicleDetails.get(0));
		}
		if(vehicleServiceLocationDetails!=null && vehicleServiceLocationDetails.size()>0){
			caseDetailsMap.putAll(vehicleServiceLocationDetails.get(0));
		}
		responseList.add(caseDetailsMap);
		responseList.add(vehiclesList);
		responseList.add(vehicleServiceLocationList);
		responseList.add(subcasesList);
		responseList.add(faultservicesList);
		responseList.add(assistanceStatusList);
		responseList.add(vehicleBrandsList);
		responseList.add(commentsList);
		
		return responseList;
	}

	

	/**
	* This method is used to save the customer satisfaction
	* 
	* 
	* @param customerRequest
	* @spname usp_case_save_satisfaction
	* @return success or failure
	*
	*/
	public Object saveCustomerSatisfaction(Customer customerRequest) {

	// getting jdbc connection
	jdbcTemplate = JDBCConnection.getJdbcTemplate();

	// registering jdbc connection and stored procedure to execute
	SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
	.withProcedureName(SPNameConstants.CASE_SAVE_SATISFACTION_SPNAME)
	.returningResultSet("customerresponse",
	new RowMapper<HashMap<String, Object>>() {
	@Override
	public HashMap<String, Object> mapRow(ResultSet rs,
		int rowNum){
	
			try{
				HashMap<String, Object> responseMap = new HashMap<String, Object>();
				responseMap.put(CommonResponseConstants.ISSUCCESS,rs.getBoolean("issuccess"));
				responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("servicemessage"));
				return responseMap;
			}catch(SQLException ex){
				return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			}
		}
	});

	// IN parameters for stored procedure
	String[] inParamaters = {"iuserID","iCaseID","iCustomerCaseResponse","iDriverResponse","iServicesProvided","iOverallRating","iSuggestions"};

	Object[] inParamaterValues = new Object[]{customerRequest.getUserId(),customerRequest.getCaseId(),customerRequest.getCustomerCareResponse(),customerRequest.getDriverResponse(),customerRequest.getServicesProvided(),customerRequest.getOverallRating(),customerRequest.getSuggestions()};	

	// executing stored procedure
	Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

	@SuppressWarnings("unchecked")
	List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("customerresponse");

	return returnjson;

	}
}
