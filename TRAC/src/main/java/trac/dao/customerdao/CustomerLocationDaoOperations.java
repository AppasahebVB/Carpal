package trac.dao.customerdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.DBUtilInterface;

/**
 *  Web User(Customer) Location related database Operations are handled in this class
 *  
 */

public class CustomerLocationDaoOperations {
	public static JdbcTemplate jdbcTemplate;
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	/**
	 * This Method is used to add the customerLocation details of registered customer.
	 * it returns customerLocation object contains all related info  of customer.
	 * 
	 * @param  customerLocationDetails
	 * @spname usp_Customer_Location_Insert
	 * @return true or false
	 */
	@SuppressWarnings("unchecked")
	public List<Object> addcustomerLocation(final CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMERLOC_REGISTRATION_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					Map<String,Object> customerLocationMap = new HashMap<String, Object>();

					System.out.print("is success value"+rs.getBoolean("issuccess"));
					if(rs.getBoolean("issuccess")){ 
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERID, rs.getString("icustomerUID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getString("LocID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));

						customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getString("vactivecaseID"));
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASE_NUMBER, rs.getString("vactivecaseNumber"));
						
						if(customerLocationDetails.getServiceRequiredTypeId()==2){
							String serviceRequiredTime = customerLocationDetails.getServiceRequiredTime();
							if(serviceRequiredTime!=null){
							customerLocationMap.put("isScheduledToday", UtilFunctions.isToday(serviceRequiredTime));
							}
							}else{
							customerLocationMap.put("isScheduledToday", false);
							}
						

					}else{
						return new HashMap<String, Object>();
					}
					return customerLocationMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				}).returningResultSet("caseDetails",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,int rowNum){
						try{
							Boolean isSuccess = rs.getBoolean("isSuccess");
							Map<String, Object> returnResult = new HashMap<String, Object>();

							if (isSuccess) {

								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("FaultId"));
								
								returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

								if(rs.getString("FaultServiceName")==null)
									returnResult.put("isSelected",false);
								else
									returnResult.put("isSelected",true);	

								returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("caseregisterdtime"));
								returnResult.put("faultName", rs.getString("FaultName"));
								returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
								returnResult.put(CommonResponseConstants.ISSUCCESS, true);

								returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));


								returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
							

								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
								returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

								returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("lastStatusTime",lastStatusTime);
								returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
								returnResult.put("differenceTime",differenceTime );




							}else{ 
								returnResult.put(CommonResponseConstants.ISSUCCESS, false);
								returnResult.put(CommonResponseConstants.SERVICEMESSAGE, "Case not found.");
							}

							return returnResult;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
						}
					}
				});



		String caseId = new UtilFunctions().getUserIdService("WEBCASE",
				1, ""+customerLocationDetails.getCustomerId());

		//IN parameters for stored procedure

		String[] inParamaters = {"iuserID","isCustomer","icustomerUID","ivehicleID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO",
				"icaseNumber","ipriorityId","iserviceTimeTypeId","iserviceTime","iReferencedInFuture","iDestinationLocation"};
		Object[] inParamaterValues = {customerLocationDetails.getUserId(),customerLocationDetails.getIsCustomer(),customerLocationDetails.getCustomerId(),customerLocationDetails.getVehicleId(),customerLocationDetails.getServiceLocationLatitude(),customerLocationDetails.getServiceLocationLongitude(),customerLocationDetails.getCustomerLocationName(),customerLocationDetails.getArea(),customerLocationDetails.getStreet(),customerLocationDetails.getBuilding(),customerLocationDetails.getLandmark(),customerLocationDetails.getCustomerLocationCity(),customerLocationDetails.getPoBox(),customerLocationDetails.getCustomerLocationState(),customerLocationDetails.getCustomerLocationCountryCode(),
				caseId,customerLocationDetails.getPriorityId(),customerLocationDetails.getServiceRequiredTypeId(),customerLocationDetails.getServiceRequiredTime(),customerLocationDetails.isSaveForReference(),customerLocationDetails.getIsDestinationLocation()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		List<HashMap<String, Object>> locationResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		List<Object> responseList = new ArrayList<Object>();

		if(!(locationResult!=null && locationResult.size()>0)){
			locationResult = new ArrayList<HashMap<String, Object>>();
			locationResult.add(new HashMap<String, Object>()); 
		}
		if(!(subcasesList!=null && subcasesList.size()>0)){
			subcasesList = new ArrayList<HashMap<String, Object>>();
		}
		responseList.add(locationResult);
		responseList.add(subcasesList);
		return responseList;

	}


	/**
	 * This method is used to update the customerLocation details of existing customer.
	 * it returns userLocations object contains all related info  of customer.
	 * 
	 * @param customerLocationDetails
	 * @spname usp_Customer_Location_Update
	 * @return true or false
	 */
	@SuppressWarnings("unchecked")
	public List<Object> updateCustomerLocation(final CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMERLOC_UPDATE_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();	

					boolean isSuccess = (boolean)rs.getBoolean("issuccess");

					if(isSuccess){


						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID, rs.getString("icustomerUID"));
						responseMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getString("LocID"));
						responseMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						responseMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						responseMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));

						responseMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						responseMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						responseMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						responseMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						responseMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getString("vactivecaseID"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASE_NUMBER, rs.getString("vactivecaseNumber"));



						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "Customer location updated successfully");
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						
						if(customerLocationDetails.getServiceRequiredTypeId()==2){
							String serviceRequiredTime = customerLocationDetails.getServiceRequiredTime();
							if(serviceRequiredTime!=null){
								responseMap.put("isScheduledToday", UtilFunctions.isToday(serviceRequiredTime));
							}
							}else{
								responseMap.put("isScheduledToday", false);
							}

					}else{/*
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));*/
						return new HashMap<String, Object>();
					}
					return responseMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				}).returningResultSet("caseDetails",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,int rowNum){
						try{
							Boolean isSuccess = rs.getBoolean("isSuccess");
							Map<String, Object> returnResult = new HashMap<String, Object>();

							if (isSuccess) {

								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("FaultId"));
								
								returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

								if(rs.getString("FaultServiceName")==null)
									returnResult.put("isSelected",false);
								else
									returnResult.put("isSelected",true);	

								returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("caseregisterdtime"));
								returnResult.put("faultName", rs.getString("FaultName"));
								returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
								returnResult.put(CommonResponseConstants.ISSUCCESS, true);

								returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));


								returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
								

								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
								returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

								returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("lastStatusTime",lastStatusTime);
								returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
								returnResult.put("differenceTime",differenceTime );

							}else{ 
								returnResult.put(CommonResponseConstants.ISSUCCESS, false);
								returnResult.put(CommonResponseConstants.SERVICEMESSAGE, "Case not found.");
							}

							return returnResult;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
						}
					}
				});

		String caseId = new UtilFunctions().getUserIdService("WEBCASE",
				1, ""+customerLocationDetails.getCustomerId());
		
		
		String[] inParamaters = {"iuserID","icustomerUID","iLocID","ivehicleID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO",
				"icaseNumber","ipriorityId","iserviceTimeTypeId","iserviceTime","iReferencedInFuture","iDestinationLocation","isCustomer"};
		Object[] inParamaterValues = {customerLocationDetails.getUserId(),customerLocationDetails.getCustomerId(),customerLocationDetails.getLocationId(),customerLocationDetails.getVehicleId(),customerLocationDetails.getServiceLocationLatitude(),customerLocationDetails.getServiceLocationLongitude(),customerLocationDetails.getCustomerLocationName(),customerLocationDetails.getArea(),customerLocationDetails.getStreet(),customerLocationDetails.getBuilding(),customerLocationDetails.getLandmark(),customerLocationDetails.getCustomerLocationCity(),customerLocationDetails.getPoBox(),customerLocationDetails.getCustomerLocationState(),customerLocationDetails.getCustomerLocationCountryCode(),
				caseId,customerLocationDetails.getPriorityId(),customerLocationDetails.getServiceRequiredTypeId(),customerLocationDetails.getServiceRequiredTime(),customerLocationDetails.isSaveForReference(),customerLocationDetails.getIsDestinationLocation(),customerLocationDetails.getIsCustomer()};

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		List<HashMap<String, Object>> locationResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		List<Object> responseList = new ArrayList<Object>();

		if(!(locationResult!=null && locationResult.size()>0)){
			locationResult = new ArrayList<HashMap<String, Object>>();
			locationResult.add(new HashMap<String, Object>()); 
		}
		if(!(subcasesList!=null && subcasesList.size()>0)){
			subcasesList = new ArrayList<HashMap<String, Object>>();
		}
		responseList.add(locationResult);
		responseList.add(subcasesList);
		return responseList;

	}


	/** 
	 * This Method is used to getLocationDetails of Existing Customer.
	 *  if true customerlocation plain java object contains fetched customer info or false
	 * 
	 * @param customerUId 
	 * @spname usp_Customer_Location_Select
	 * @return customerlocation details
	 *
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> getCustomerLocation(CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMERLOC_FETCH_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					Map<String,Object> customerLocationMap = new HashMap<String, Object>();

					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLat"));
					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
					//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));

					// if registerd through web
					customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));


					if(rs.getInt("isServiceLocation") == 1)
						customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
					else
						customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

					if(rs.getInt("isDestinationLocation") == 1)
						customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
					else
						customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

					return customerLocationMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});



		//IN parameters for stored procedure
		String[] inParamaters = {"icustomerUID"};
		Object[] inParamaterValues = {customerLocationDetails.getCustomerId()};

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		return (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");

	}
	
	
	/** 
	 * This Method is used to getDestincationLocationDetails of Existing Customer.
	 *  if true customerlocation plain java object contains fetched customer info or false
	 * 
	 * @param customerUId 
	 * @spname usp_Customer_DestinationLocation_Select
	 * @return customerlocation details
	 *
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> getCustomerDestinationLocation(CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_DEST_LOC_FETCH_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					Map<String,Object> customerLocationMap = new HashMap<String, Object>();

					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLat"));
					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
					customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
					//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYCODE, rs.getString("countryCode"));
					// if registerd through web
					customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
					customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));


					if(rs.getInt("isServiceLocation") == 1)
						customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
					else
						customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

					if(rs.getInt("isDestinationLocation") == 1)
						customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
					else
						customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

					return customerLocationMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});



		//IN parameters for stored procedure
		String[] inParamaters = {"icustomerUID"};
		Object[] inParamaterValues = {customerLocationDetails.getCustomerId()};

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		return (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");

	}
	
	/**
	 * This Method is used to add the userDestinationLocation details of registered user.
	 * it returns List<HashMap<String, Object>> object contains success or failure.
	 * 
	 * @param  customerLocationDetails
	 * @spname usp_User_DestinationLocation_Insert
	 * @return true or false
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> adduserDestinationLocation(final CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USERDESTINATIONLOC_REGISTRATION_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					Map<String,Object> customerLocationMap = new HashMap<String, Object>();

					System.out.print("is success value"+rs.getBoolean("issuccess"));
					if(rs.getBoolean("issuccess")){ 
						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERID, rs.getString("vuserID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getString("LocID"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));

						customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

						customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getString("icaseID"));
						
						
					}else{
						customerLocationMap.put(CommonResponseConstants.ISSUCCESS, false);
						customerLocationMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
					}
					return customerLocationMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		//IN parameters for stored procedure
		String[] inParamaters = {"iuserID","icaseID","ivehicleID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"};
		Object[] inParamaterValues = {customerLocationDetails.getUserId(),customerLocationDetails.getCaseId(),customerLocationDetails.getVehicleId(),customerLocationDetails.getServiceLocationLatitude(),customerLocationDetails.getServiceLocationLongitude(),customerLocationDetails.getCustomerLocationName(),customerLocationDetails.getArea(),customerLocationDetails.getStreet(),customerLocationDetails.getBuilding(),customerLocationDetails.getLandmark(),customerLocationDetails.getCustomerLocationCity(),customerLocationDetails.getPoBox(),customerLocationDetails.getCustomerLocationState(),customerLocationDetails.getCustomerLocationCountryCode()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);

		List<HashMap<String, Object>> locationResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		
		return locationResult;

	}
	
	

	/**
	 * This method is used to update the userDestination location details of existing customer.
	 * it returns userLocations object contains all related info  of user.
	 * 
	 * @param customerLocationDetails
	 * @spname usp_User_DestinationLocation_Update
	 * @return true or false
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> updateUserDestinationLocation(final CaseDetails customerLocationDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USERDESTINATIONLOC_UPDATE_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();	

					boolean isSuccess = (boolean)rs.getBoolean("issuccess");

					if(isSuccess){

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERID, rs.getString("vuserID"));
						responseMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getString("LocID"));
						responseMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
						responseMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
						responseMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));

						responseMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
						responseMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
						responseMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
						responseMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
						responseMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

					

						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location updated successfully");
						responseMap.put(CommonResponseConstants.ISSUCCESS, true);
						
					
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, "User location not found.");
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
					return responseMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		
		String[] inParamaters = {"iuserID","icaseID","iLocID","ivehicleID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"
		};
		Object[] inParamaterValues = {customerLocationDetails.getUserId(),customerLocationDetails.getCaseId(),customerLocationDetails.getLocationId(),customerLocationDetails.getVehicleId(),customerLocationDetails.getServiceLocationLatitude(),customerLocationDetails.getServiceLocationLongitude(),customerLocationDetails.getCustomerLocationName(),customerLocationDetails.getArea(),customerLocationDetails.getStreet(),customerLocationDetails.getBuilding(),customerLocationDetails.getLandmark(),customerLocationDetails.getCustomerLocationCity(),customerLocationDetails.getPoBox(),customerLocationDetails.getCustomerLocationState(),customerLocationDetails.getCustomerLocationCountryCode()};

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		List<HashMap<String, Object>> locationResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		
		return locationResult;

	}

/**
	 * This method is used to fetch the vehicle servicelocations
	 * 
	 * @param customerLocationDetails
	 * @spname usp_User_DestinationLocation_Update
	 * @return true or false
	 */
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> getVehicleServiceLocations(Customer customerLocations) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.VEHICLE_SERVICE_LOCATIONS_FETCH_SPNAME).returningResultSet("locationResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum) {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();	

					boolean isSuccess = (boolean)rs.getBoolean("issuccess");

					if(isSuccess){

						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE, rs.getString("GPSLocationLat"));
						responseMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
						responseMap.put("casecount", rs.getString("casecount"));
						responseMap.put("CaseId", rs.getString("CaseId"));
						
					}else{
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, false);
					}
					return responseMap;	
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
				});

		
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {customerLocations.getUserId()};

		//executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);


		List<HashMap<String, Object>> locationResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("locationResult");
		
		return locationResult;

	}

	

}
