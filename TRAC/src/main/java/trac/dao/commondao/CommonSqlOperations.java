package trac.dao.commondao;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import trac.beans.commonbean.Common;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.sqlqueryconstants.CommonSqlQueryConstants;

/**
 *  Common database Operations are handled in this class
 *  @methods getListOfCountries
 */


public class CommonSqlOperations {

	public static JdbcTemplate jdbcTemplate = null;
	
	/**
	 * This method is used to fetch country details of Driver from database and return the success or failure information
	 * @param driverDetails
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */
	public Common getListOfCountries(
			Common commonBean) {
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		
		List<Map<String, Object>> row = jdbcTemplate.queryForList(CommonSqlQueryConstants.GET_COUNTRY_LIST); //list for table
		
			// Condition to Check if record is available 
		if (row.size() > 0 && row != null){
			commonBean.setTableResultList(row);
			commonBean.setSuccess(true);
			
		}else{
			commonBean.setServiceMessage("No records found");
			
		}//end of else if
		
		
	
	    return commonBean;
	}//end of getReportInfo
}
