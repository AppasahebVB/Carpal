package trac.dao.casesdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.casesbean.CaseDetails;
import trac.beans.customerbean.Customer;
import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfCaseDetails;
import trac.constants.responseconstants.ResponseConstantsOfCustomer;
import trac.constants.responseconstants.ResponseConstantsOfDriver;
import trac.constants.responseconstants.ResponseConstantsOfTruck;
import trac.constants.responseconstants.ResponseConstantsOfUserLocation;
import trac.constants.responseconstants.ResponseConstantsOfVehicle;
import trac.constants.sqlqueryconstants.CustomerSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;
import trac.util.CommonUtil;
import trac.util.DBUtil;
import trac.util.UtilFunctions;
import trac.util.utilinterface.DBUtilInterface;
/**
 *   Case Module Web User database Operations are handled in this class
 *  @methods searchForCustomer,searchForVehicle,updateCaseDetails,getCaseFaultAndServices,getAvailableDrivers,assignCaseToDriver,insertCasePrerequisite,
	insertCaseOnJob,getCaseScheduleInfo,updateCaseSchedule,getAllCaseInfo,getCasesInQue,assigncasetoself,getCustomerCaseDetails,getCasesCount,
	updateFaultBySubCaseId,saveCaseComment
	
	
 */
public class CustomerCaseRelatedSqlOperations {
	public static JdbcTemplate jdbcTemplate;
	public static UtilFunctions utility = new UtilFunctions();
	private DBUtilInterface dbUtilities = new DBUtil();
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();

	/**
	 * This Method is used to registered caseDetails of the Existing customer.
	 * @spname usp_customer_cases_Insert
	 * @param caseDetails
	 * @return if true returns casedetails plain java object,or returns false
	 */
	public List<HashMap<String, Object>> registerCaseInfo(CaseDetails caseDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_CASE_REGISTRATION_SPNAME)
		.returningResultSet("caseRegisterStatus",
				new RowMapper<Map<String, Object>>() {
			@Override
			public Map<String, Object> mapRow(ResultSet rs,int rowNum){
				try{
					Boolean isSuccess = rs.getBoolean("isSuccess");
					Map<String, Object> returnResult = new HashMap<String, Object>();

					if (isSuccess) {

						returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID,rs.getInt("caseInfoID"));
						returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
						returnResult.put("subCaseNumber", rs.getString("subCaseNumber"));
						returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
						returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, returnResult.get("caseRegisteredServiceID")!=null?returnResult.get("caseRegisteredServiceID"):"");
						returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription")!=null?rs.getString("faultDescription"):"");
						returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION, rs.getString("serviceDescription")!=null ? rs.getString("serviceDescription"):"");
						returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS, rs.getString("caseComments")!=null?rs.getString("caseComments"):"");
						returnResult.put(ResponseConstantsOfCustomer.CREATEDBY, rs.getInt("createdBy"));
						returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("caseregisterdtime"));
						if(rs.getString("FaultServiceName")==null)
							returnResult.put("isSelected",false);
						else
							returnResult.put("isSelected",true);	
						returnResult.put("FaultServiceName",  (rs.getString("FaultServiceName") == null) ? "" : rs.getString("FaultServiceName"));
						returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));
						returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
						returnResult.put("faultName", rs.getString("FaultName"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
						returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
						returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
						String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
						returnResult.put("faultId", rs.getInt("caseRegisteredFaultID"));
						
						long differenceTime = 0;
						long countDownMilliSeconds=0;
						if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
							 if(lastStatusTime.length() > 0) {
								 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime); //countdown
							     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
							 }
						}
						
						returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);
						returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
						returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
						returnResult.put("lastStatusTime",lastStatusTime);
						returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
						returnResult.put("differenceTime",differenceTime );

					} else {
						returnResult.put(CommonResponseConstants.ISSUCCESS, false);
						returnResult.put(CommonResponseConstants.SERVICEMESSAGE,"CaseID not found.");
					}

					return returnResult;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		//iStatus - New,Assigned

		String[] inParamaters = {"icaseId","icaseNumber","icaseRegisteredFaultID","ifaultDescription","icaseComments","iuserID"};

		Object[] inParamaterValues = {caseDetails.getCaseId(),caseDetails.getCaseNumber(),caseDetails.getCaseFaultId(),caseDetails.getCaseFaultDescription(),caseDetails.getCaseNotes(),caseDetails.getUserId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseRegisterStatus");
		//return Json
		return returnjson;

	}

	/**
	 * This method is used to get the case details of registered customer of registered vehicle.
	 * 
	 * @param caseDetails
	 * @return list of map objects with cases information 
	 */
	public List<HashMap<String, Object>> getCaseInfo(CaseDetails caseDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				CustomerSqlQueryConstants.FETCH_SUBCASES,caseDetails.getCaseId());
		List<HashMap<String, Object>> caseDetailsList = new ArrayList<HashMap<String, Object>>();
		for(Map<String, Object> row : rows){
			HashMap<String, Object> caseDetail = new HashMap<String, Object>();
			caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID,(int) row.get("caseInfoID"));
			caseDetail.put("caseID",(int) row.get("caseID"));
			caseDetail.put("subCaseNumber",(String) row.get("subCaseNumber"));
			caseDetail.put("caseRegisteredFaultID",(int) row.get("caseRegisteredFaultID"));

			caseDetail.put("caseRegisteredServiceID",(int) row.get("caseRegisteredServiceID"));

			caseDetail.put("faultDescription",(String) row.get("faultDescription"));

			caseDetail.put("serviceDescription",(String) row.get("serviceDescription")==null?"":row.get("serviceDescription"));

			caseDetail.put("caseComments",(String) row.get("caseComments"));
			caseDetail.put("createdBy",Integer.parseInt((String) row.get("createdBy")));
			caseDetail.put("createdAt",(String) row.get("createdAt"));

			caseDetailsList.add(caseDetail);
		}
		return caseDetailsList;
	}
	
	/**
	 * This method is used to get the customer details of registered customer.
	 * 
	 * @param customerDetails
	 * @return  list of map objects with matching customers information  
	 */
	public List<Map<String, Object>> searchForCustomer(Customer customerDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		String sqlQueryToExecute = CustomerSqlQueryConstants.CUSTOMER_AUTOCOMPLETE_QUERY + "'"+ customerDetails.getSearchBy()+"%' OR customerUID LIKE "+"'"+ customerDetails.getSearchBy()+"%'";
		
		System.out.println("sqlQueryToExecute - " + sqlQueryToExecute);
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlQueryToExecute);
		
		sqlQueryToExecute = null;
		return rows;
	}
	
	/**
	 * This method is used to get the vehicle details of registered customer .
	 * 
	 * @param customerDetails
	 * @return list of map objects with matching vehicle information   
	 */
	public List<Map<String, Object>> searchForVehicle(Customer customerDetails) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		String sqlQueryToExecute = CustomerSqlQueryConstants.VEHICLE_AUTOCOMPLETE_QUERY +  " where cv.customerUID='"+customerDetails.getCustomerId()+"' AND (v.VIN LIKE '%"+ customerDetails.getSearchBy()+"' OR registrationNo LIKE "+"'%"+ customerDetails.getSearchBy()+"')";
		
		System.out.println("sqlQueryToExecute - " + sqlQueryToExecute);
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlQueryToExecute);
		
		sqlQueryToExecute = null;
		return rows;
	}

	
	/**
	 * This Method is used to update case details of Existing customer.
	 * @spname usp_customer_caseinfo_update
	 * @param caseDetails
	 * @return if true returns casedetails plain java object,or returns false
	 */
	public List<HashMap<String, Object>> updateCaseDetails(int subcaseId,String caseServiceDescription,int userId,ArrayList<LinkedHashMap<String, Object>> caseServicesList)  {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CUSTOMER_CASE_DETAILS_UPDATE_SPNAME)
		.returningResultSet("caseDetails",
				new RowMapper<Map<String, Object>>() {
			@Override
			public Map<String, Object> mapRow(ResultSet rs,int rowNum){
				try{
					Boolean isSuccess = rs.getBoolean("isSuccess");
					Map<String, Object> returnResult = new HashMap<String, Object>();

					if (isSuccess) {

						returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
						returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
						returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
						returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
						returnResult.put("faultId", rs.getInt("caseRegisteredFaultID"));
						returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
						returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
						returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

						if(rs.getString("FaultServiceName")==null)
							returnResult.put("isSelected",false);
						else
							returnResult.put("isSelected",true);
						
						//returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));
						returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
						returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
						returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
						returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
						returnResult.put("faultName", rs.getString("FaultName"));
						returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
						returnResult.put(CommonResponseConstants.ISSUCCESS, true);
						
						returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
						String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
						returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
						returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
						returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
						returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
						long differenceTime = 0;
						long countDownMilliSeconds=0;
						if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
							
							 if(lastStatusTime.length() > 0) {
								 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
							     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
							 }
							     
							
						}
						returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

						returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
						returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
						returnResult.put("lastStatusTime",lastStatusTime);
						returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
						returnResult.put("differenceTime",differenceTime );

					}else{ 
						returnResult.put(CommonResponseConstants.ISSUCCESS, false);
						returnResult.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
					}

					return returnResult;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		//String[] inParamaters = {"icaseInfoID","icaseRegisteredServiceID","iserviceDescription","iupdatedBy"};
		String[] inParamaters = {"isubcaseId","icaseServiceDescription","iuserID","iserviceArray","iservicescount"};
		Object[] inParamaterValues = {subcaseId,caseServiceDescription,userId,new CommonUtil().buildSubcaseServicesArray(caseServicesList),caseServicesList.size()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		//return Json
		return returnjson;

	}
	

	/**
	 * This Method is used to fetch Case Faults and Services
	 * @spname usp_case_registered_faults_services
	 * @return if true returns success or failure with faults and respective services
	 */

	public List<HashMap<String, Object>>  getCaseFaultAndServices() {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FAULTS_SERVICES_SPNAME)
		.returningResultSet("casefaultsservices",
				new RowMapper<Map<String, Object>>() {
			@Override
			public Map<String, Object> mapRow(ResultSet rs,int rowNum){
				try{
					Map<String, Object> returnResult = new HashMap<String, Object>();

					returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICE_ID,rs.getInt("faultServiceId"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,rs.getInt("faultId"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_ID,rs.getInt("serviceId"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,rs.getString("faultdesc"));
					returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_DESC,rs.getString("servicedesc"));

					return returnResult;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
		});

		Map<String, Object> simpleJdbcCallResult =  simpleJdbcCall.execute();
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casefaultsservices");
		//return Json
		return returnjson;

	}
	
	/**
	 * Fetch Available driver who logged in today
	 *  @spname usp_cases_fetch_available_driver
	 * @return List<HashMap<String, Object>> with the success or failure information with list of drivers
	 */

	public List<HashMap<String, Object>> getAvailableDrivers(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FETCH_AVAILABLE_DRIVERS_SPNAME).returningResultSet("assignedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();

					responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKNICKNAME, rs.getString("trucknickname"));
					responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
					responseMap.put(ResponseConstantsOfTruck.TRUCKDRIVERID, rs.getInt("TruckDriverID"));
					responseMap.put(ResponseConstantsOfDriver.PUNCH_IN_TIME, rs.getString("punchIn"));
					responseMap.put(ResponseConstantsOfDriver.LATITUDE, rs.getString("GPSLocationLat"));
					responseMap.put(ResponseConstantsOfDriver.LONGITUDE, rs.getString("GPSLocationLong"));
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
				}).returningResultSet("unassignedResult", new RowMapper<Map<String,Object>>()
						{
					@Override
					public Map<String,Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						try{
							Map<String,Object> responseMap = new HashMap<String, Object>();

							//responseMap.put(ResponseConstantsOfDriver.DRIVERID, rs.getInt("driverID"));
							responseMap.put(ResponseConstantsOfTruck.TRUCKID, rs.getInt("TruckID"));
							responseMap.put(ResponseConstantsOfTruck.TRUCKNICKNAME, rs.getString("trucknickname"));
							//responseMap.put(ResponseConstantsOfDriver.DRIVER_NAME, rs.getString("driverName"));
							//responseMap.put(ResponseConstantsOfTruck.TRUCKDRIVERID, rs.getInt("TruckDriverID"));
							//responseMap.put(ResponseConstantsOfDriver.PUNCH_IN_TIME, rs.getString("punchIn"));
							responseMap.put(ResponseConstantsOfDriver.LATITUDE, rs.getString("GPSLocationLat"));
							responseMap.put(ResponseConstantsOfDriver.LONGITUDE, rs.getString("GPSLocationLong"));
							return responseMap;	
						}
						catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
						}
					}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"assistanceServiceId","iservicecaseID","isubcaseId"};
		Object[] inParamaterValues = {caseDetails.getAssistanceId(),caseDetails.getCaseId(),caseDetails.getSubcaseId()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> assignedResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("assignedResult");
		List<HashMap<String, Object>> unassignedResult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("unassignedResult");
		
		if(unassignedResult!=null && unassignedResult.size()>0)
		assignedResult.addAll(unassignedResult);
		
		return assignedResult;

	}
	

	/**
	 * Assign case to driver
	 *  @spname usp_cases_assign_case_to_driver
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<Object> assignCaseToDriver(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_ASSIGN_CASE_TO_DRIVER_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				}).returningResultSet("caseDetails",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,int rowNum){
						try{
							Boolean isSuccess = rs.getBoolean("isSuccess");
							Map<String, Object> returnResult = new HashMap<String, Object>();

							if (isSuccess) {

								returnResult.put("isCustomer", rs.getBoolean("isCustomer"));
								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								returnResult.put("subCaseNumber",rs.getString("subCaseNumber"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
								returnResult.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("FaultId"));
								
								returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

								if(rs.getString("FaultServiceName")==null)
									returnResult.put("isSelected",false);
								else
									returnResult.put("isSelected",true);	

								returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
								returnResult.put("faultName", rs.getString("FaultName"));
								returnResult.put("FaultServiceName", rs.getString("FaultServiceName"));
								returnResult.put(CommonResponseConstants.ISSUCCESS, true);

								returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));

								returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
								
								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
								
								returnResult.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
								returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
							
								
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

								returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("lastStatusTime",lastStatusTime);
								returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
								returnResult.put("differenceTime",differenceTime );




							}else{ 
								returnResult.put(CommonResponseConstants.ISSUCCESS, false);
								returnResult.put(CommonResponseConstants.SERVICEMESSAGE, "Case not found.");
							}

							return returnResult;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
						}
					}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"itruckDriverID","iservicecaseID","icaseinfoID","idurationvalue","iuserID"};
		Object[] inParamaterValues = {caseDetails.getTruckDriverId(),caseDetails.getCaseId(),caseDetails.getSubcaseId(),caseDetails.getDurationValue(),caseDetails.getUserId()};			
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseDetails");
		List<Object> responseList = new ArrayList<Object>();
		responseList.add(returnjson);
		responseList.add(subcasesList);
		return responseList;

	}
	

	/**
	 * save case prerequisite
	 * @spname usp_case_prerequisite_Insert
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	@SuppressWarnings("unchecked")
	public List<Object> insertCasePrerequisite(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_SAVE_CASE_PREREQUISITE_SPNAME).returningResultSet("caseprerequisite", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
					Map<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					System.out.print("is success value"+isSuccess);
					responseMap.put("newSubCaseId", rs.getInt("vinsertedSubCaseId"));
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
					responseMap.put(ResponseConstantsOfCaseDetails.ISCASECLOSED, rs.getBoolean("isCaseClosed"));
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					return responseMap;	
				}
				catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
				}
			}
				}).returningResultSet("casedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,
							int rowNum) {
						try {
							HashMap<String, Object> responseMap = new HashMap<String, Object>();
							if (rs.getBoolean(1)) {
								responseMap.put("customerId",rs.getString("customerUID"));
								responseMap.put("vehicleId",rs.getString("vehicleID"));
								responseMap.put("customerName",rs.getString("customerName"));
								responseMap.put("mobileNumber",rs.getString("mobileNumber"));
								responseMap.put("vin",rs.getString("vin"));
								responseMap.put("brandModelReg",rs.getString("brandModelReg"));
								responseMap.put("plateCode",rs.getString("plateCode"));
								responseMap.put("serviceLocationName",rs.getString("customerLocationName"));
								responseMap.put("serviceLocationLatitude",rs.getString("GPSLocationLat"));
								responseMap.put("serviceLocationLongitude",rs.getString("GPSLocationLong"));
								responseMap.put("area",rs.getString("area"));
								responseMap.put("street",rs.getString("street"));
								responseMap.put("caseId", rs.getInt("caseID"));
								responseMap.put("caseNumber", rs.getString("caseNumber"));
								
							} else {
								responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
								responseMap.put(CommonResponseConstants.ISSUCCESS,false);
							}
							return responseMap;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				})
				.returningResultSet("subcasedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,int rowNum) {
						try {
							HashMap<String, Object> caseDetail = new HashMap<String, Object>();
							if (rs.getBoolean(1)) {
								caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								caseDetail.put("subCaseNumber",rs.getString("subCaseNumber"));
								caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								caseDetail.put("faultId", rs.getInt("caseRegisteredFaultID"));
								caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
								caseDetail.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
								caseDetail.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

								if(rs.getString("FaultServiceName")==null)
									caseDetail.put("isSelected",false);
								else
									caseDetail.put("isSelected",true);	
								
								caseDetail.put("isAssigned", rs.getBoolean("iscaseAssigned"));
								caseDetail.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
								caseDetail.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
								caseDetail.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
								caseDetail.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
								caseDetail.put("faultName", rs.getString("FaultName"));
								caseDetail.put("FaultServiceName", rs.getString("FaultServiceName"));
								caseDetail.put(CommonResponseConstants.ISSUCCESS, true);
								
								caseDetail.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
								
								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
								
								caseDetail.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								caseDetail.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
								caseDetail.put("oldScheduleStatusId", rs.getString("statusID"));
								caseDetail.put("isAssigned", rs.getBoolean("iscaseAssigned"));
								caseDetail.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								caseDetail.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

								caseDetail.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								caseDetail.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								caseDetail.put("lastStatusTime",lastStatusTime);
								caseDetail.put("countDownMilliSeconds",countDownMilliSeconds);
								caseDetail.put("differenceTime",differenceTime );

							}
							return caseDetail;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				})
				.returningResultSet("locationResult",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,
							int rowNum) {
						try {
							Map<String,Object> customerLocationMap = new HashMap<String, Object>();


							customerLocationMap.put("serviceLocation", rs.getString("Description"));

							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE, rs.getString("GPSLocationLat"));
							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
							//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

							// if registerd through web
							customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

							/*customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
							customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE, rs.getInt("serviceTimeTypeID"));
							customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, rs.getString("serviceTime"));
*/


							if(rs.getInt("isServiceLocation") == 1)
								customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
							else
								customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

							if(rs.getInt("isDestinationLocation") == 1)
								customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
							else
								customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

							return customerLocationMap;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				}).returningResultSet("casefaultsservices",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,int rowNum){
						try{
							Map<String, Object> returnResult = new HashMap<String, Object>();
							returnResult.put("checked",false);
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICE_ID,rs.getInt("faultServiceId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,rs.getInt("faultId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_ID,rs.getInt("serviceId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,rs.getString("faultdesc"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_DESC,rs.getString("servicedesc"));

							return returnResult;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
						}
					}
				});


		// IN parameters for stored procedure
		String[] inParamaters = {"iuserId","isubcaseId","iVehicleAvailable","ivehicleAvailableComments","iServiceAccepted","iserviceAcceptedComments",
				"iReported","iReportedSameIssue","ireportedComments","iAttendedSolved","iEmergency","iemergencyComments","iNoDentsScratches","identscomments"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getSubcaseId(),caseDetails.getIsVehicleAvailable(),caseDetails.getVehicleAvailableComments(),caseDetails.getIsServiceAccepted(),caseDetails.getServiceAcceptedComments(),
				caseDetails.getIsReported(),caseDetails.getIsReportedSameIssue(),caseDetails.getReportedComments(),caseDetails.getIsAttendedSolved(),caseDetails.getIsEmergency(),caseDetails.getEmergencyComments(),caseDetails.getIsNoDentsScratches(),caseDetails.getDentscomments()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("caseprerequisite");

		List<HashMap<String, Object>> caseDetailsList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("casedetails");
		List<HashMap<String, Object>> subcasedetailsList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("subcasedetails");
		List<HashMap<String, Object>> locationList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("locationResult");
		List<HashMap<String, Object>> casefaultsservicesList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("casefaultsservices");

		List<Object> responseList = new ArrayList<Object>();
		if (!(caseDetailsList != null && caseDetailsList.size() > 0)) {
			caseDetailsList = new ArrayList<HashMap<String, Object>>();
			caseDetailsList.add(new HashMap<String, Object>());
		}
		if (!(subcasedetailsList != null && subcasedetailsList.size() > 0)) {
			subcasedetailsList = new ArrayList<HashMap<String, Object>>();
		}
		if (!(locationList != null && locationList.size() > 0)) {
			locationList = new ArrayList<HashMap<String, Object>>();
		}
		if (!(casefaultsservicesList != null && casefaultsservicesList.size() > 0)) {
			casefaultsservicesList = new ArrayList<HashMap<String, Object>>();
		}

		responseList.add(returnjson);
		responseList.add(caseDetailsList);
		responseList.add(subcasedetailsList);
		responseList.add(locationList);
		responseList.add(casefaultsservicesList);
		return responseList;

	}

	
	/**
	 * save case on job details
     * @spname usp_case_on_jobdetails_Insert
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<Object> insertCaseOnJob(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_SAVE_CASE_ON_JOB_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");

						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				}).returningResultSet("subcasedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,int rowNum) {
						try {
							HashMap<String, Object> caseDetail = new HashMap<String, Object>();
							if (rs.getBoolean(1)) {

								caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								caseDetail.put("subCaseNumber",rs.getString("subCaseNumber"));
								caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								caseDetail.put("faultId", rs.getInt("caseRegisteredFaultID"));
								caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
								caseDetail.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
								caseDetail.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

								if(rs.getString("FaultServiceName")==null)
									caseDetail.put("isSelected",false);
								else
									caseDetail.put("isSelected",true);	

								caseDetail.put("isAssigned", rs.getBoolean("iscaseAssigned"));
								caseDetail.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
								caseDetail.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
								caseDetail.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
								caseDetail.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
								caseDetail.put("faultName", rs.getString("FaultName"));
								caseDetail.put("FaultServiceName", rs.getString("FaultServiceName"));
								caseDetail.put(CommonResponseConstants.ISSUCCESS, true);
								
								caseDetail.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
							
								caseDetail.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								caseDetail.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								caseDetail.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
								caseDetail.put("oldScheduleStatusId", rs.getString("statusID"));
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								caseDetail.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);
								caseDetail.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								caseDetail.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								caseDetail.put("lastStatusTime",lastStatusTime);
								caseDetail.put("countDownMilliSeconds",countDownMilliSeconds);
								caseDetail.put("differenceTime",differenceTime );

							}
							return caseDetail;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserId","isubcaseId","iCaseId","iDentsScratches","identscomments","iDriverAbletoResolve","idriverComments","iRequiredServiceProvided","iserviceComments","idispatchedTimeFromLoc","iTowRequested","iarrivalTimeAtLoc"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getSubcaseId(),caseDetails.getCaseId(),caseDetails.getIsNoDentsScratches(),caseDetails.getDentscomments(),caseDetails.getIsDriverAbletoResolve(),caseDetails.getDriverComments(),caseDetails.getIsRequiredServiceProvided(),caseDetails.getServiceComments(),caseDetails.getDispatchedTimeFromLoc(),caseDetails.getIsTowRequested(),caseDetails.getArrivalTimeAtLoc()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		List<Object> responseList = new ArrayList<Object>();
		
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> subcasedetails = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("subcasedetails");
		responseList.add(returnjson);
		responseList.add(subcasedetails);
		return responseList;

	}
	
	/**
	 * Fetch running case schedule Info 
	 * @spname usp_case_fetch_schedule
	 * @return List<HashMap<String, Object>> with the success or failure information with list of running cases
	 */

	@SuppressWarnings("unchecked")
	public List<Object> getCaseScheduleInfo(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FETCH_SCHEDULE_SPNAME).returningResultSet("casesResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						if(isSuccess){ 
							boolean isBeforeToday = false;
							if(rs.getInt("serviceTimeTypeID")==2){
								String serviceRequiredTime = rs.getString("serviceTime");
								if(serviceRequiredTime!=null){
									isBeforeToday =UtilFunctions.isBeforeToday(serviceRequiredTime);
								}
							}else{
								isBeforeToday = true;
							}

							responseMap.put(ResponseConstantsOfCaseDetails.CASEID, rs.getInt("servicecaseID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfCaseDetails.OWNER_NAME, rs.getString("ownername"));
							responseMap.put(ResponseConstantsOfCaseDetails.OWNER_PHONE_NO, rs.getString("ownerphoneno"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
														responseMap.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
							responseMap.put("oldScheduleStatusId", rs.getString("statusID"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS, rs.getString("statusName"));
							responseMap.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLATITUDE, rs.getString("GPSLocationLat"));
							responseMap.put(ResponseConstantsOfCaseDetails.SERVICELOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
							responseMap.put(CommonResponseConstants.USERID, rs.getInt("iuserID"));
							responseMap.put("isCustomer", rs.getBoolean("isCustomer"));
						
						}else{
							
							responseMap = new HashMap<String, Object>();
						}

						

						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				}).returningResultSet("casestatusesResult", new RowMapper<Map<String,Object>>()
						{
					@Override
					public Map<String,Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {try{
								Map<String,Object> responseMap = new HashMap<String, Object>();
								responseMap.put("schedulestatusID", rs.getInt("ID"));
								responseMap.put("schedulestatus", rs.getString("Description"));
								return responseMap;	
							}
							catch(SQLException ex){
								return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
							}
					}
						}).returningResultSet("driverstatusesResult", new RowMapper<Map<String,Object>>()
								{
							@Override
							public Map<String,Object> mapRow(ResultSet rs, int rowNum)
									throws SQLException {try{
										Map<String,Object> responseMap = new HashMap<String, Object>();
										responseMap.put("schedulestatusID", rs.getInt("ID"));
										responseMap.put("schedulestatus", rs.getString("Description"));
										return responseMap;	
									}
									catch(SQLException ex){
										return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
									}
							}
								}).returningResultSet("casescountresult", new RowMapper<Map<String,Object>>()
								{
							@Override
							public Map<String,Object> mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								try{
										Map<String,Object> responseMap = new HashMap<String, Object>();
										boolean isSuccess = rs.getBoolean("issuccess");
										if(isSuccess){
											responseMap.put(CommonResponseConstants.ISSUCCESS, true);
											responseMap.put("casesinquecount", rs.getInt("casesinque"));
											responseMap.put("pendingcasescount", rs.getInt("pendingcases"));
											responseMap.put("closedcasescount", rs.getInt("closedcases"));

										}else{
											responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
											responseMap.put(CommonResponseConstants.ISSUCCESS, false);
										}


										return responseMap;	
									}
									catch(SQLException ex){
										return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
									}
							 }
								});


		// IN parameters for stored procedure
		String[] inParamaters = {"iTypeId","iuserId"};
		Object[] inParamaterValues = {caseDetails.getTypeId(),caseDetails.getUserId()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		
		List<HashMap<String, Object>> casesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casesResult");
		List<HashMap<String, Object>> subcasesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("subcasesResult");
		List<HashMap<String, Object>> casestatusesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casestatusesResult");
		List<HashMap<String, Object>> driverstatusesList = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("driverstatusesResult");
		List<HashMap<String, Object>> casescountresult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casescountresult");
		
		List<Object> responseList = new ArrayList<Object>();
		List<HashMap<String, Object>> maincasesList = new ArrayList<HashMap<String,Object>>();
		if(!(casesList!=null && casesList.size()>0)){
			casesList = new ArrayList<HashMap<String, Object>>();
		}else{
			//For removing scheduled cases 
			Iterator<HashMap<String, Object>> iterator=casesList.iterator();
			while(iterator.hasNext()){
				
				
				HashMap<String, Object> caseDetail = (HashMap<String, Object>) iterator.next();
				
				if(caseDetail.get("isBeforeToday")!=null && !(Boolean)caseDetail.get("isBeforeToday")){
					iterator.remove();
				}
				
				
				List<HashMap<String, Object>> subcases = new ArrayList<HashMap<String,Object>>();;
				if(subcasesList!=null){//adding subcases to main case
					
					Iterator<HashMap<String, Object>> subiterator=subcasesList.iterator();
					while(subiterator.hasNext()){
						HashMap<String, Object> subcaseDetail = (HashMap<String, Object>) subiterator.next();
					
						if(subcaseDetail!=null && subcaseDetail.get("caseId") == caseDetail.get("caseId")){
							
							subcases.add(subcaseDetail);
						}
					}
					
				}
				caseDetail.put("subcaseslist", subcases);
				maincasesList.add(caseDetail);
				
			}
		}
		
		if(!(casestatusesList!=null && casestatusesList.size()>0)){
			casestatusesList = new ArrayList<HashMap<String, Object>>();
		}
		
		if(!(driverstatusesList!=null && driverstatusesList.size()>0)){
			driverstatusesList = new ArrayList<HashMap<String, Object>>();
		}
		
		responseList.add(maincasesList); 
		responseList.add(casestatusesList);
		responseList.add(driverstatusesList);
		responseList.add(casescountresult);
		return responseList;

	}
	
	
	/**
	 * Update case status
	 * @spname usp_cases_schedule_update
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	@SuppressWarnings("unchecked")
	public List<Object> updateCaseSchedule(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_SCHEDULE_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						
						responseMap.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME, rs.getString("actualtArrival"));
						responseMap.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getInt("statusID"));
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
		}).returningResultSet("subcasedetails",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,int rowNum) {
				try {
					HashMap<String, Object> caseDetail = new HashMap<String, Object>();
					if (rs.getBoolean(1)) {
						caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
						caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
						caseDetail.put("subCaseNumber",rs.getString("subCaseNumber"));
						caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
						caseDetail.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, rs.getInt("caseRegisteredServiceID"));
						caseDetail.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription"));
						caseDetail.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION,rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));

						if(rs.getString("FaultServiceName")==null)
							caseDetail.put("isSelected",false);
						else
							caseDetail.put("isSelected",true);	

						caseDetail.put(ResponseConstantsOfCustomer.CASECOMMENTS,rs.getString("caseComments"));
						caseDetail.put(ResponseConstantsOfCustomer.CREATEDBY,Integer.parseInt((String) rs.getString("createdBy")));
						caseDetail.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("createdAt"));
						caseDetail.put(ResponseConstantsOfCustomer.FAULTID, rs.getInt("caseRegisteredFaultID"));
						caseDetail.put("faultName", rs.getString("FaultName"));
						caseDetail.put("FaultServiceName", rs.getString("FaultServiceName"));
						caseDetail.put(CommonResponseConstants.ISSUCCESS, true);
						
						caseDetail.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
						caseDetail.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
						String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
						

						
						caseDetail.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);
						caseDetail.put(ResponseConstantsOfCaseDetails.EXPECTEDARRIVAL, rs.getString("formattedExpectedArrival"));
						caseDetail.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
						caseDetail.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
						caseDetail.put("oldScheduleStatusId", rs.getString("statusID"));
						caseDetail.put("isAssigned", rs.getBoolean("iscaseAssigned"));
						caseDetail.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
						long differenceTime = 0;
						long countDownMilliSeconds=0;
						if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
							
							 if(lastStatusTime.length() > 0) {
								 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
							     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
							 }
							     
							
						}
						

						caseDetail.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
						caseDetail.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
						caseDetail.put("lastStatusTime",lastStatusTime);
						caseDetail.put("countDownMilliSeconds",countDownMilliSeconds);
						caseDetail.put("differenceTime",differenceTime );

					}
					return caseDetail;
				} catch (SQLException ex) {
					return exceptionHandling.customGenericExceptionJson(
							new Integer(ex.getErrorCode()),
							ex.getLocalizedMessage());
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","icaseID","isubcaseID","ischedulestatusID","iactualtarrival"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getCaseId(),caseDetails.getSubcaseId(),caseDetails.getCaseStatusId(),caseDetails.getArrivalTimeAtLoc()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		List<Object> returnList = new ArrayList<Object>();
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> subcasedetails = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("subcasedetails");
		
		returnList.add(returnjson);
		returnList.add(subcasedetails);
		return returnList;

	}

	

	/**
	 * Fetch all case Info 
	 * @spname usp_case_fetch_allcases
	 * @return List<HashMap<String, Object>> with the success or failure information with list of all cases
	 */

	@SuppressWarnings("unchecked")
	public List<Object> getAllCaseInfo(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FETCH_CASES_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();

						responseMap.put("isCustomer", rs.getBoolean("isCustomer"));
						responseMap.put(ResponseConstantsOfCaseDetails.CASEID, rs.getInt("servicecaseID"));
						responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
						responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
						responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
						responseMap.put(ResponseConstantsOfCaseDetails.CALLER_NAME, rs.getString("callername"));
						responseMap.put(CommonResponseConstants.PHONE, rs.getString("mobileNumber"));
						responseMap.put(ResponseConstantsOfCaseDetails.CREATOR_NAME, rs.getString("creator"));
						responseMap.put(ResponseConstantsOfCaseDetails.CASESTATUS_ID, rs.getString("statusID"));
						responseMap.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
						responseMap.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("createdAt"));

						responseMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));


						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
	}).returningResultSet("casescountresult", new RowMapper<Map<String,Object>>()
						{
					@Override
					public Map<String,Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						try{
								Map<String,Object> responseMap = new HashMap<String, Object>();
									responseMap.put(CommonResponseConstants.ISSUCCESS, true);
									responseMap.put("casesinquecount", rs.getInt("casesinque"));
									responseMap.put("pendingcasescount", rs.getInt("pendingcases"));
									responseMap.put("closedcasescount", rs.getInt("closedcases"));

								return responseMap;	
							}
							catch(SQLException ex){
								return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
							}
					 }
						});;

		// IN parameters for stored procedure
		String[] inParamaters = {"iTypeId","iuserId","iStartIndex","iEndIndex"};
		Object[] inParamaterValues = {caseDetails.getTypeId(),caseDetails.getUserId(),caseDetails.getStartIndex(),caseDetails.getEndIndex()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		
		
		List<Object> responseList = new ArrayList<Object>();
		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> casescountresult = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("casescountresult");
		responseList.add(returnjson);
		responseList.add(casescountresult);
		return responseList;

	}

	
	/**
	 * Fetch all cases in que 
	 * @spname usp_case_fetch_casesinque
	 * @return List<HashMap<String, Object>> with the success or failure information with list of all cases
	 */

	public List<HashMap<String, Object>> getCasesInQue(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FETCH_QUE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						if(isSuccess){
							responseMap.put(ResponseConstantsOfCaseDetails.CASEID, rs.getInt("servicecaseID"));
							responseMap.put(ResponseConstantsOfVehicle.VEHICLEREGISTERNO, rs.getString("registrationNo"));
							responseMap.put(ResponseConstantsOfVehicle.REGISTRATIONDATE, rs.getString("registrationDate"));
							responseMap.put(ResponseConstantsOfVehicle.VIN, rs.getString("vin"));
							responseMap.put(ResponseConstantsOfCaseDetails.CALLER_NAME, rs.getString("callername"));
							responseMap.put(CommonResponseConstants.PHONE, rs.getString("mobileNumber"));
							responseMap.put(ResponseConstantsOfCaseDetails.CREATOR_NAME, rs.getString("creator"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
							responseMap.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("createdAt"));
							responseMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
								String caseCreatedTime = (rs.getString("createdAt") == null) ? "" : rs.getString("createdAt");
								System.out.println("caseCreatedTime = " + caseCreatedTime);
							if(!caseCreatedTime.isEmpty()){
								long differenceMilliseconds = UtilFunctions.getDifferenceArrivalMilliseconds(rs.getString("createdAt"));
								responseMap.put("differnceTime", differenceMilliseconds);
								System.out.println("differnceTime = " + differenceMilliseconds);
							}

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						}

						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserId"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getCaseId()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}
	

	/**
	 * Assign case to que
	 * @return List<HashMap<String, Object>> with the success or failure information
	 */

	public List<HashMap<String, Object>> assigncasetoself(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_ASSIGN_CASE_SELF_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						System.out.print("is success value"+isSuccess);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","icaseID"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getCaseId()};	
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}

	
	/**
	 * This method is used to fetch customer cases details service locations
	 * 
	 * @param userId
	 *            ,vehicleDetails
	 * @spname usp_Vehicle_Vehicles_Update
	 * @return true or false
	 */

	@SuppressWarnings("unchecked")
	public List<Object> getCustomerCaseDetails(CaseDetails caseDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(
				SPNameConstants.CASE_FETCH_CASEDETAILS_SPNAME)
				.returningResultSet("casedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,
							int rowNum) {
						try {
							HashMap<String, Object> responseMap = new HashMap<String, Object>();
							if (rs.getBoolean(1)) {
								responseMap.put("customerId",rs.getString("customerUID"));
								responseMap.put("customerName",rs.getString("customerName"));
								responseMap.put("mobileNumber",rs.getString("mobileNumber"));
								responseMap.put("vin",rs.getString("vin"));
								responseMap.put("brandModelReg",rs.getString("brandModelReg"));
								responseMap.put("plateCode",rs.getString("plateCode"));
								responseMap.put("serviceLocationName",rs.getString("customerLocationName"));
							/*	responseMap.put("serviceLocationLatitude",rs.getString("GPSLocationLat"));
								responseMap.put("serviceLocationLongitude",rs.getString("GPSLocationLong"));*/
								responseMap.put("area",rs.getString("area"));
								responseMap.put("street",rs.getString("street"));
							} else {
								responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
								responseMap.put(CommonResponseConstants.ISSUCCESS,false);
							}
							return responseMap;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				})
				.returningResultSet("subcasedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,int rowNum) {
						try {
							HashMap<String, Object> caseDetail = new HashMap<String, Object>();
							if (rs.getBoolean(1)) {

								caseDetail.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID, rs.getInt("caseInfoID"));
								caseDetail.put("caseID", rs.getInt("caseID"));
								caseDetail.put("subCaseNumber",rs.getString("subCaseNumber"));
								caseDetail.put("caseRegisteredFaultID", rs.getInt("caseRegisteredFaultID"));
								caseDetail.put("caseRegisteredServiceID", rs.getInt("caseRegisteredServiceID"));
								caseDetail.put("faultDescription", rs.getString("faultDescription"));
								caseDetail.put("serviceDescription",rs.getString("serviceDescription")==null?"":rs.getString("serviceDescription"));
								caseDetail.put("isSelected",false);
								caseDetail.put("caseComments",rs.getString("caseComments"));
								caseDetail.put("createdBy",Integer.parseInt((String) rs.getString("createdBy")));
								caseDetail.put("createdAt",rs.getString("createdAt"));
								caseDetail.put("faultName", rs.getString("FaultName"));

							}
							return caseDetail;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				})
				.returningResultSet("locationResult",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,
							int rowNum) {
						try {
							Map<String,Object> customerLocationMap = new HashMap<String, Object>();


							customerLocationMap.put("serviceLocation", rs.getString("Description"));

							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLATITUDE, rs.getString("GPSLocationLat"));
							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONLONGITUDE, rs.getString("GPSLocationLong"));
							customerLocationMap.put(ResponseConstantsOfCustomer.CUSTOMERLOCATIONNAME, rs.getString("Description"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.DETAILADDRESS, rs.getString("address"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.LOCATIONID, rs.getInt("LocID"));
							//				customerLocationMap.put(ResponseConstantsOfVehicle.VEHICLE_ID, rs.getInt("LocID"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.CITY, rs.getString("city"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.STATE, rs.getString("state"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.COUNTRYNAME, rs.getString("countryName"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.SERVICELOCATIONCOUNTRYCODE, rs.getString("countryCode"));

							// if registerd through web
							customerLocationMap.put(ResponseConstantsOfUserLocation.AREA, rs.getString("Area"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.STREET, rs.getString("Street"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.BUILDING, rs.getString("Building"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.LANDMARK, rs.getString("LandMark"));
							customerLocationMap.put(ResponseConstantsOfUserLocation.POBOX, rs.getString("POBox"));

							//customerLocationMap.put(ResponseConstantsOfCaseDetails.PRIORITY_ID, rs.getInt("priorityID"));
						//	customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TYPE, rs.getInt("serviceTimeTypeID"));
							//customerLocationMap.put(ResponseConstantsOfCaseDetails.SERVICE_REQUIRED_TIME, rs.getString("serviceTime"));



							if(rs.getInt("isServiceLocation") == 1)
								customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, true);
							else
								customerLocationMap.put(ResponseConstantsOfCustomer.ISSERVICELOCATION, false);

							if(rs.getInt("isDestinationLocation") == 1)
								customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, true);
							else
								customerLocationMap.put(ResponseConstantsOfCustomer.ISDESTINATIONLOCATION, false);

							return customerLocationMap;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				}).returningResultSet("casefaultsservices",
						new RowMapper<Map<String, Object>>() {
					@Override
					public Map<String, Object> mapRow(ResultSet rs,int rowNum){
						try{
							Map<String, Object> returnResult = new HashMap<String, Object>();
							returnResult.put("checked",false);
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_SERVICE_ID,rs.getInt("faultServiceId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_ID,rs.getInt("faultId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_ID,rs.getInt("serviceId"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_FAULT_DESC,rs.getString("faultdesc"));
							returnResult.put(ResponseConstantsOfCaseDetails.CASE_SERVICE_DESC,rs.getString("servicedesc"));

							return returnResult;
						}catch(SQLException ex){
							return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
						}
					}
				});

		// IN parameters for stored procedure
		String[] inParamaters = { "iuserID", "icaseID" };
		Object[] inParamaterValues = { caseDetails.getUserId(),caseDetails.getCaseId() };

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues,simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		List<HashMap<String, Object>> caseDetailsList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("casedetails");
		List<HashMap<String, Object>> subcasedetailsList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("subcasedetails");
		List<HashMap<String, Object>> locationList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("locationResult");
		List<HashMap<String, Object>> casefaultsservicesList = (List<HashMap<String, Object>>) simpleJdbcCallResult
				.get("casefaultsservices");

		List<Object> responseList = new ArrayList<Object>();
		if (!(caseDetailsList != null && caseDetailsList.size() > 0)) {
			caseDetailsList = new ArrayList<HashMap<String, Object>>();
			caseDetailsList.add(new HashMap<String, Object>());
		}
		if (!(subcasedetailsList != null && subcasedetailsList.size() > 0)) {
			subcasedetailsList = new ArrayList<HashMap<String, Object>>();
		}
		if (!(locationList != null && locationList.size() > 0)) {
			locationList = new ArrayList<HashMap<String, Object>>();
		}
		if (!(casefaultsservicesList != null && casefaultsservicesList.size() > 0)) {
			casefaultsservicesList = new ArrayList<HashMap<String, Object>>();
		}

		responseList.add(caseDetailsList);
		responseList.add(subcasedetailsList);
		responseList.add(locationList);
		responseList.add(casefaultsservicesList);
		return responseList;
	}

	
	/**
	 * Fetch cases count
	 * @return List<HashMap<String, Object>> with the success or failure information with cases count
	 */

	public List<HashMap<String, Object>> getCasesCount(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FETCH_CASES_COUNT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						if(isSuccess){
							responseMap.put(CommonResponseConstants.ISSUCCESS, true);
							responseMap.put("casesinquecount", rs.getInt("casesinque"));
							responseMap.put("pendingcasescount", rs.getInt("pendingcases"));
							responseMap.put("closedcasescount", rs.getInt("closedcases"));

						}else{
							responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
							responseMap.put(CommonResponseConstants.ISSUCCESS, false);
						}


						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserId"};
		Object[] inParamaterValues = {caseDetails.getUserId()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;

	}
	
	/** 
	 * update fault by subcaseId
	 * @return List<HashMap<String, Object>> with the success or failure information with cases count
	 */

	@SuppressWarnings("unchecked")
	public HashMap<String,Object> updateFaultBySubCaseId(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.CASE_FAULT_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {try{
						Map<String,Object> returnResult = new HashMap<String, Object>();
						boolean isSuccess = rs.getBoolean("issuccess");
						returnResult.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
						returnResult.put(CommonResponseConstants.ISSUCCESS, isSuccess);
						return returnResult;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
			}
		}).returningResultSet("subcasedetails",
						new RowMapper<HashMap<String, Object>>() {
					@Override
					public HashMap<String, Object> mapRow(ResultSet rs,int rowNum) {
						try {
							HashMap<String, Object> returnResult = new HashMap<String, Object>();
							boolean isSuccess = rs.getBoolean("isSuccess");
							
							if (isSuccess) {

								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_SUBCASEID,rs.getInt("caseInfoID"));
								returnResult.put(ResponseConstantsOfCustomer.CUSTOMER_CASEID, rs.getInt("caseID"));
								returnResult.put("subCaseNumber", rs.getString("subCaseNumber"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDFAULTID, rs.getInt("caseRegisteredFaultID"));
								returnResult.put(ResponseConstantsOfCustomer.CASEREGISTEREDSERVICEID, returnResult.get("caseRegisteredServiceID")!=null?returnResult.get("caseRegisteredServiceID"):"");
								returnResult.put(ResponseConstantsOfCustomer.FAULTDESCRIPTION, rs.getString("faultDescription")!=null?rs.getString("faultDescription"):"");
								returnResult.put(ResponseConstantsOfCustomer.SERVICEDESCRIPTION, rs.getString("serviceDescription")!=null ? rs.getString("serviceDescription"):"");
								returnResult.put(ResponseConstantsOfCustomer.CASECOMMENTS, rs.getString("caseComments")!=null?rs.getString("caseComments"):"");
								returnResult.put(ResponseConstantsOfCustomer.CREATEDBY, rs.getInt("createdBy"));
								returnResult.put(ResponseConstantsOfCustomer.CREATEDAT,rs.getString("caseregisterdtime"));
								if(rs.getString("FaultServiceName")==null)
									returnResult.put("isSelected",false);
								else
									returnResult.put("isSelected",true);	
								returnResult.put("FaultServiceName",  (rs.getString("FaultServiceName") == null) ? "" : rs.getString("FaultServiceName"));
								returnResult.put("isAssigned", rs.getBoolean("iscaseAssigned"));
								returnResult.put("isCaseClosedByDifferentIssue", rs.getBoolean("isCaseClosedByDifferentIssue"));
								returnResult.put("faultName", rs.getString("FaultName"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASE_SCHEDULE_STATUS_ID, rs.getString("statusID"));
								returnResult.put(ResponseConstantsOfCaseDetails.STATUS_ID, rs.getString("statusID"));
								returnResult.put("oldScheduleStatusId", rs.getString("statusID"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASESTATUS, rs.getString("casestatus"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEREGISTERDTIME, rs.getString("caseregisterdtime"));
								returnResult.put(ResponseConstantsOfCaseDetails.CASEASSIGNEDTIME, rs.getString("caseassignedtime"));
								
								returnResult.put("faultId", rs.getInt("caseRegisteredFaultID"));
								String lastStatusTime = (rs.getString("lastStatusTime") == null) ? "" : rs.getString("lastStatusTime");
																
								long differenceTime = 0;
								long countDownMilliSeconds=0;
								if(rs.getString("statusID")!=null && Integer.parseInt(rs.getString("statusID"))>=4 && lastStatusTime != null){
									
									 if(lastStatusTime.length() > 0) {
										 countDownMilliSeconds = UtilFunctions.getDifferenceMilliseconds(lastStatusTime);//countdown
									     differenceTime = UtilFunctions.getDifferenceArrivalMilliseconds(lastStatusTime);
									 }
									     
									
								}
								returnResult.put(ResponseConstantsOfCaseDetails.ACTUALARRIVALTIME,lastStatusTime);

								returnResult.put("dateToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("timeToSet",UtilFunctions.getCurrentDateByTimeZone());
								returnResult.put("lastStatusTime",lastStatusTime);
								returnResult.put("countDownMilliSeconds",countDownMilliSeconds);
								returnResult.put("differenceTime",differenceTime );

							}
							return returnResult;
						} catch (SQLException ex) {
							return exceptionHandling.customGenericExceptionJson(
									new Integer(ex.getErrorCode()),
									ex.getLocalizedMessage());
						}
					}
				});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","isubcaseId","iFaultId","icaseId"};
		Object[] inParamaterValues = {caseDetails.getUserId(),caseDetails.getSubcaseId(),caseDetails.getCaseFaultId(),caseDetails.getCaseId()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		List<HashMap<String, Object>> subcasedetails = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("subcasedetails");
		
		HashMap<String,Object> returnResultMap = new HashMap<String,Object>();
		
		if (returnjson != null && returnjson.size() > 0) {
			returnResultMap.putAll(returnjson.get(0));
		}
		if ((boolean) returnResultMap.get("isSuccess")) {
			returnResultMap.put("subcasesInfo", subcasedetails);
		}
		return returnResultMap;

	}

	
	
	public List<HashMap<String, Object>> saveCaseComment(CaseDetails caseDetails) {

		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);

		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USP_SAVE_CASE_COMMENT).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
				{
			@Override
			public Map<String,Object> mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				try{
						Map<String,Object> responseMap = new HashMap<String, Object>();
						String userName = rs.getString("userName");
						if(userName == null || userName.length() ==0)
							userName = rs.getString("displayName");
						responseMap.put("commentId", rs.getString("commentId"));
						responseMap.put("caseId", rs.getInt("caseId"));
						responseMap.put("createdBy", userName);
						responseMap.put(ResponseConstantsOfCustomer.CASECOMMENTS, rs.getString(ResponseConstantsOfCustomer.CASECOMMENTS));
						responseMap.put("createdAt", rs.getString("createdAt"));
						return responseMap;	
					}
					catch(SQLException ex){
						return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
					}
				}
			});

		// IN parameters for stored procedure
		String[] inParamaters = {"iCaseId","iDescription","iUserId"};
		Object[] inParamaterValues = {caseDetails.getCaseId(),caseDetails.getCaseComments(),caseDetails.getUserId()};		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
		return returnjson;
	}

}
