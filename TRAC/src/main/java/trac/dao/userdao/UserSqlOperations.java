package trac.dao.userdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import trac.beans.jdbcteamplatebean.JDBCConnection;
import trac.beans.userbean.SecurityQuestions;
import trac.beans.userbean.UserLocation;
import trac.beans.userbean.Users;

import trac.constants.responseconstants.CommonResponseConstants;
import trac.constants.responseconstants.ResponseConstantsOfUser;
import trac.constants.sqlqueryconstants.CommonSqlQueryConstants;
import trac.constants.sqlqueryconstants.UserSqlQueryConstants;
import trac.constants.storedprocedureconstants.SPNameConstants;
import trac.customexception.customexceptionhandling.CommonExceptionHandlingClass;

import trac.util.CommonUtil;
import trac.util.CustomHashing;
import trac.util.CustomHttpURLConnection;
import trac.util.DBUtil;
import trac.util.utilinterface.DBUtilInterface;
import trac.util.utilinterface.SpringByCryptoHashing;

/**
 *  User related database Operations are handled in this class
 *  
 */

public class UserSqlOperations {

	public static JdbcTemplate jdbcTemplate;
	private SpringByCryptoHashing hashing = new CustomHashing();
	private DBUtilInterface dbUtilities = new DBUtil();
	//private CommonUtilInterface commonUtil = new CommonUtil();
	
	private CommonExceptionHandlingClass exceptionHandling = new CommonExceptionHandlingClass();
	
	/**
	 * This method is used to register the details of first time user it returns
	 * user object contains all related info of user
	 * 
	 * @param userName
	 * @spname usp_Registration_Users_Insert
	 * @return Users plain java object contains registered user info
	 * @author rpenta
	 */
	public Object addUserDetails(Users userDetails) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		Object[] inParamaterValues = null;
		

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_REGISTRATION_SPNAME)
				.returningResultSet("userRegisterStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum){
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									if (rs.getBoolean("issuccess")) {
										responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
								
										responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
										responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
										responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
										responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
										responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,true);
										responseMap.put(CommonResponseConstants.ISSUCCESS,true);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User Registered Successfully.");
									} else {
										responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,false);
										responseMap.put(CommonResponseConstants.ISSUCCESS,false);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User already Registered/Invalid Data");
									}
	
									return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
							    }
							}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"iFirstName","iLastName","iemail","imobileNumber","iemail","ipwdHash","iuserName","ideviceTypeID","iapplicationVersion","isecurityQuestionID","isecurityQuestion","isecurityQuestionHash","iRoleIDS","icreatedBy","iDisplayName","isTermsAgreed","iIsdCode"};
		if(userDetails.getRoleIds().contains("6"))
			inParamaterValues = new Object[]{userDetails.getFirstName(),userDetails.getLastName(),userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getUserName(),userDetails.getDeviceTypeId(),userDetails.getApplicationVersionNo(),userDetails.getSecurityQuestionId(),userDetails.getSecurityQuestion(),hashing.encryption(userDetails.getSecurityAnswer()),userDetails.getRoleIds(),userDetails.getUserId(),userDetails.getDisplayName(),0,userDetails.getIsdCode()};
		else
			inParamaterValues = new Object[]{userDetails.getFirstName(),userDetails.getLastName(),userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),"",3,"",0,"","",userDetails.getRoleIds(),userDetails.getUserId(),userDetails.getDisplayName(),0,0};		
		
	    // executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userRegisterStatus");
		if(returnjson != null){
			if(returnjson.size() > 0)
				return returnjson.get(0);
			else
				return null;
		}else{
			return null;
		}
		
	}

	/**
	 * This method is used to fetch the details of user it returns user object
	 * contains all related info of user
	 * 
	 * @param userDetails
	 * @return Users plain java object contains fetched user info
	 * @see
	 */
	public Users getUserDetails(Users userDetails) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		Users tempUserDetails = new Users();
		String sqlQueryToExecute = null;
		if(!userDetails.isFromWeb()){
			sqlQueryToExecute = UserSqlQueryConstants.MOBILE_SELECT_USER;
		}else{
			sqlQueryToExecute = UserSqlQueryConstants.WEB_SELECT_USER;
		}
			
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				sqlQueryToExecute, userDetails.getLoginId(),
				userDetails.getLoginId());
		
		for (Map<String, Object> row : rows) {
			tempUserDetails.setUserId((int) row.get("userID"));
			tempUserDetails.setEmail((String) row.get("email"));
			tempUserDetails.setPhone((String) row.get("phone"));
			tempUserDetails.setRoleId((int) row.get("roleId"));
			tempUserDetails.setHashedPassword((String) row.get("pwdHash"));
			tempUserDetails.setRetryCount((int) row.get("passwordRetryCount"));
			tempUserDetails.setApplicationVersionNo((String) row.get("applicationVersionNo"));
			tempUserDetails.setProfilePicURL((String) row.get("profilePicData"));
			tempUserDetails.setDisplayName((String) row.get("displayName"));
			tempUserDetails.setIsActive((int) row.get("active"));
		}
		// Condition to Check if record is available return userdetails
		if (rows.size() > 0 && rows != null)
			return tempUserDetails;
		else
			return null;
	}
	
	
	public HashMap<String, Object> authGenaration(Users userDetails,String hostName){
		    //http url connection to get accesstoken 
			CustomHttpURLConnection httpUrlConnection = new CustomHttpURLConnection();
			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			Users tempUserDetails = httpUrlConnection.serviceCall("http://"+hostName+"/oauth/token?username="+userDetails.getUserId()+"&password="+userDetails.getPassword()+"&client_id=trac&client_secret=trac&grant_type=password");
			
			//condition to send role id only to web application
			responseMap.put(CommonResponseConstants.ACCESSTOKEN,tempUserDetails.getAccessToken());
			responseMap.put(CommonResponseConstants.REFRESHTOKEN,tempUserDetails.getRefreshToken());
			responseMap.put(CommonResponseConstants.EXPIRESIN,tempUserDetails.getExpiresIn());
			responseMap.put(CommonResponseConstants.SERVICECODE,600);
			responseMap.put(CommonResponseConstants.ISSUCCESS,true);
			
			return responseMap;
	}
	/**
	 * This method is used to fetch the details of user it returns user object
	 * contains all related info of user
	 * 
	 * @param userName
	 * @return Users plain java object contains fetched user info
	 * @see
	 */
	public Users getUserDetailsById(int userId,boolean isTempPasswordChange) {
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		Users tempUserDetails = new Users();
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				UserSqlQueryConstants.SELECT_USER_BY_ID, userId);
		
		for (Map<String, Object> row : rows) {
						if(isTempPasswordChange)
							tempUserDetails.setHashedPassword((String) row.get("tempPwdHash"));
						else
							tempUserDetails.setHashedPassword((String) row.get("pwdHash"));
		}
		// Condition to Check if record is available return userdetails
		if (rows.size() > 0 && rows != null)
			return tempUserDetails;
		else
			return null;
	}

	/**
	 * This method is used to check for uniqueness of user along the application
	 * it returns true if user doesn't exists else false
	 * 
	 * @param userName
	 * @param emailId
	 * @return true/false
	 * @see
	 */
	public boolean checkForUnique(String userName, String emailId) {

		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.UNIQUE_USERNAME;
		int rowCount = 0;
		boolean isUserNameExists = false;

		if (userName != null && userName.length() > 0) {
			dynamicSqlQuery = dynamicSqlQuery + " and userName = ? ";
			isUserNameExists = true;
		}
		if (emailId != null && emailId.length() > 0) {
			dynamicSqlQuery = dynamicSqlQuery + " and email = ?";

			if (isUserNameExists) {
				rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, userName,
						emailId);
			} else {
				rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, emailId);
			}
		} else {
			rowCount = jdbcTemplate.queryForInt(dynamicSqlQuery, userName);
		}

		if (rowCount > 0)
			return true;
		else
			return false;
	}

	/**
	 * This method is used to edit the details of user it returns 0/1 based on
	 * updation
	 * 
	 * @param Users
	 *            user plain java object
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */
	public Object editUserDetails(Users userDetails) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_UPDATE_SPNAME)
				.returningResultSet("userUpdateStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum)  {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									if (rs.getBoolean("issuccess")) {
										responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
										responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
										responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
										responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
										responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
										responseMap.put(ResponseConstantsOfUser.CLOUD_PIC_FILENAME,rs.getString("userFileNameOnCloud"));
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
										responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
										responseMap.put(CommonResponseConstants.ISSUCCESS,true);
									} else {
										responseMap.put(CommonResponseConstants.ISSUCCESS,false);
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
										responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
									}
									return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});
		
		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","imobileNumber","iemail","ipwdHash","iDisplayName","iIsdCode"};
		Object[] inParamaterValues = {userDetails.getUserId(),userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getDisplayName(),userDetails.getIsdCode()};
		
		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

			System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		
		@SuppressWarnings("unchecked")
		List<Users> returnjson = (List<Users>) simpleJdbcCallResult.get("userUpdateStatus");

		return returnjson.get(0);

	}

	/**
	 * This method is used to fetch the security question of user when he tries
	 * option to retrieve forgot password using email option it returns security
	 * question if email matches or else returns null
	 * 
	 * @param email
	 *            email of user
	 * @spname usp_Registration_SecurityQuestion_Users_Select
	 * @return security question
	 * @see
	 */
	public Object fetchSecurityQuestion(String email, String phone) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_SECURITYQUESTION_SPNAME)
				.returningResultSet("userUpdateStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									/*System.out.println("count" + rowNum);
									System.out.println("Description"+ rs.getString("Description"));*/
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									Boolean isSuccess = rs.getBoolean(CommonResponseConstants.ISSUCCESS);
									responseMap.put(CommonResponseConstants.ISSUCCESS,isSuccess);
									
									if(isSuccess){
										responseMap.put(CommonResponseConstants.EMAIL, rs.getString(CommonResponseConstants.EMAIL));
									}
									// Condition To Check Description is not null then return security question
									/*if (rs.getString("Description") != null) {
										responseMap.put(ResponseConstantsOfUser.SECURITYQUESTION,
														rs.getString("Description"));
										responseMap.put(CommonResponseConstants.ISSUCCESS,
												true);
									} else
										responseMap.put(CommonResponseConstants.ISSUCCESS,
												false);*/
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});

		// IN parameters for stored procedure
		
		String[] inParamaters = {"iemail","iphone"};
		Object[] inParamaterValues = {email,phone};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userUpdateStatus");
		/*if (returnjson.size() == 0) {
			HashMap<String, Object> responseMap = new HashMap<String, Object>();

			responseMap.put(CommonResponseConstants.ISSUCCESS, false);
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Email/Phone doesn't Exist.");
			returnjson.add(responseMap);
		}*/
		return returnjson.get(0);
	}

	/**
	 * This method is used to fetch the security question of user when he tries
	 * option to retrieve forgot password using email option it returns security
	 * question if email matches or else returns null
	 * 
	 * @param email
	 *            email of user
	 * @spname usp_Registration_SecurityQuestion_Users_Select
	 * @return security question
	 * @see
	 */
	public Object userValidation(String email, String phone,String randomPassword) {
		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_SECURITYQUESTION_SPNAME)
				.returningResultSet("userUpdateStatus",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									Boolean isSuccess = rs.getBoolean(CommonResponseConstants.ISSUCCESS);
									responseMap.put(CommonResponseConstants.ISSUCCESS,isSuccess);
									
									if(isSuccess){
										responseMap.put(CommonResponseConstants.EMAIL, rs.getString(CommonResponseConstants.EMAIL));
										responseMap.put(CommonResponseConstants.USERID, rs.getInt(CommonResponseConstants.USERID));
									}
									
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});

		// IN parameters for stored procedure
		
		String[] inParamaters = {"iemail","iphone","iRandomPassword"};
		Object[] inParamaterValues = {email,phone,hashing.encryption(randomPassword)};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userUpdateStatus");
		
		return returnjson.get(0);
	}
	/**
	 * This method is used to validate the security answer provided by user when
	 * he tries option to retrieve forgot password it returns true if securtiy
	 * answer matches or else returns false
	 * 
	 * @param Users
	 *            user plain java object
	 * @return userid , true (or) false
	 * @see
	 */

	public HashMap<String, Object> validateSecurityAnswer(Users userDetails) {
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		// get JDBC Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		String hashedSecAnswer = null;
		String userId = null;
		boolean isSuccess = false;
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(
				UserSqlQueryConstants.GET_SEC_ANSWER, userDetails.getLoginId(),
				userDetails.getLoginId());

		for (Map<String, Object> row : rows) {
			hashedSecAnswer = row.get("securityQuestionHash").toString();
			userId = row.get("userID").toString();
		}

		if (rows.size() > 0) {
			isSuccess = hashing.decryption(userDetails.getSecurityAnswer(),hashedSecAnswer);
			int updateCount = jdbcTemplate.update(
					UserSqlQueryConstants.UPDATE_SEC_CHECK, 1,
					userDetails.getLoginId(), userDetails.getLoginId());
			System.out.println("updateCount - " + updateCount);
		}

		if (rows.size() == 0) {
			isSuccess = false;
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Invalid email id/phone");
		}

		if (isSuccess) {
			responseMap.put(CommonResponseConstants.USERID, userId);
		} else {
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,
					"Invalid security answer");
		}

		responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

		return responseMap;

	}
	
	/**
	 * This method is used to validate the security answer provided by user when
	 * he tries option to retrieve forgot password it returns true if securtiy
	 * answer matches or else returns false
	 * 
	 * @param Users
	 *            user plain java object
	 * @return userid , true (or) false
	 * @see
	 */

	public HashMap<String, Object> validateTemporaryPassword(Users userDetails) {
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		// get JDBC Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		String hashedSecAnswer = null;
		HashMap<String, Object> resultMap = null;
		String userId = null;
		boolean isSuccess = false;
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_TEMP_PWD_SPNAME)
				.returningResultSet("userDetails",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									Boolean isSuccess = rs.getBoolean(CommonResponseConstants.ISSUCCESS);
									responseMap.put(CommonResponseConstants.ISSUCCESS,isSuccess);
									
									if(isSuccess){
										responseMap.put("passwordHash", rs.getString("passwordHash"));
									}else{
										responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
									}
									
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"iemail","iphone"};
		Object[] inParamaterValues = {userDetails.getLoginId(),userDetails.getLoginId()};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userDetails");
		
		if(returnjson != null){
		   resultMap = returnjson.get(0);
		  isSuccess = (Boolean) resultMap.get("isSuccess");
			 if(isSuccess){
			  hashedSecAnswer = resultMap.get("passwordHash").toString();
			  isSuccess = hashing.decryption(userDetails.getPassword(),hashedSecAnswer);
			 }
		}
			
		if (isSuccess) {
			responseMap.put(CommonResponseConstants.USERID, userId);
		} else {
			responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"Invalid security answer/password expired");
		}

		responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);

		return responseMap;

	}

	/**
	 * This method is used to update the retry count if user provides incorrect
	 * password for multiple times it returns 1 if passwordRetryCount column
	 * updates successfully or else returns 0
	 * 
	 * @param userName
	 *            unique user name to search for record to update
	 * @return 1 (or) 0
	 * @see
	 */
	public int updateUserLoginAttempts(int userID) {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.UPDATE_LOGIN_ATTEMPTS;
		return jdbcTemplate.update(dynamicSqlQuery, userID);

	}

	/**
	 * This method is used to update the profile pic of user it returns 1 if
	 * profile pic column updates successfully or else returns 0
	 * 
	 * @param userID
	 *            unique userID to search for record to update
	 * @param profilePicURL
	 *            profile pic to be updated in user table
	 * @return 1 (or) 0
	 * @see
	 */
	public int updateProfilePic(int userID, String profilePicURL,String userFileNameOnCloud) {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		String dynamicSqlQuery = UserSqlQueryConstants.UPDATE_PROFILE_PIC;
		return jdbcTemplate.update(dynamicSqlQuery, profilePicURL,userFileNameOnCloud, userID);

	}

	/**
	 * This method is used to get the list of security questions list it returns
	 * list of securityQuestions
	 * 
	 * @param
	 * @return list of securityQuestions
	 * @see
	 */
	public SecurityQuestions fetchSecurityInfoList() {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String secSqlQuery = UserSqlQueryConstants.GET_SEC_QUESTIONS;
		String isdSqlQuery = UserSqlQueryConstants.GET_COUNTRY_ISD_LIST;
		List<Map<String, Object>> securityQuestionList = jdbcTemplate.queryForList(secSqlQuery);
		List<Map<String, Object>> countryIsdList = jdbcTemplate.queryForList(isdSqlQuery);
		SecurityQuestions secQuestions = new SecurityQuestions();

		secQuestions.setSecurityQuestionList(securityQuestionList);
		secQuestions.setCountryIsdList(countryIsdList);
		return secQuestions;
	}
	
	/**
	 * This method is used to get the list of security questions list it returns
	 * list of securityQuestions
	 * 
	 * @param
	 * @return list of securityQuestions
	 * @see
	 */
	public List<Map<String, Object>> fetchListOfRoles() {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.GET_ROLES;
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList(dynamicSqlQuery);

		return rows;
	}
	
	/**
	 * This method is used to get the list of security questions list it returns
	 * list of securityQuestions
	 * 
	 * @param
	 * @return list of securityQuestions
	 * @see
	 */
	public List<Map<String, Object>> fetchListOfUsers() {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("GET_USERS_LIST - " + UserSqlQueryConstants.GET_USERS_LIST);
		String dynamicSqlQuery = UserSqlQueryConstants.GET_USERS_LIST;
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList(dynamicSqlQuery);

		return rows;
	}
	
	/**
	 * This method is used to activate/deactivate user
	 * 
	 * 
	 * @param
	 * @return 
	 * @see
	 */
	public List<HashMap<String, Object>> userActivation(int userId) {
	jdbcTemplate = JDBCConnection.getJdbcTemplate(); //Jdbc object
		
		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_ACTIVATION)
				.returningResultSet("userDetails",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									Boolean isSuccess = rs.getBoolean(CommonResponseConstants.ISSUCCESS);
									responseMap.put(CommonResponseConstants.ISSUCCESS,isSuccess);
									
									if(isSuccess){
										responseMap.put("isActive", rs.getBoolean("isActive"));
									}
									
									responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
									
									
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
							    }
							}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userId};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userDetails");
		

		return returnjson;
	}

	
	/**
	 * This method is used to disable the user
	 *
	 * 
	 * @param
	 * @return 
	 * @see
	 */
	public List<HashMap<String, Object>> disableUser(int userId) {
		
		jdbcTemplate = JDBCConnection.getJdbcTemplate(); //Jdbc object
		
		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(SPNameConstants.USER_REMOVE)
				.returningResultSet("userDetails",
						new RowMapper<HashMap<String, Object>>() {
							@Override
							public HashMap<String, Object> mapRow(ResultSet rs,
									int rowNum) {
								try{
									HashMap<String, Object> responseMap = new HashMap<String, Object>();
									Boolean isSuccess = rs.getBoolean(CommonResponseConstants.ISSUCCESS);
									responseMap.put(CommonResponseConstants.ISSUCCESS,isSuccess);
									responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
									
								   return responseMap;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
							    }
							}
						});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID"};
		Object[] inParamaterValues = {userId};
		

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userDetails");
		

		return returnjson;
	}



	/**
	 * This method is used to update the password of user it returns true/false
	 * 
	 * @param userName
	 * @spname usp_Users_Password_Update
	 * @return Users plain java object contains registered user info
	 * @see
	 */

	public Object updatePassword(Users userDetails,String spName) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(spName)
				.returningResultSet("passwordUpdateStatus",
						new RowMapper<Map<String, Object>>() {
							@Override
							public Map<String, Object> mapRow(ResultSet rs,int rowNum) {
								try{
									Map<String, Object> returnJson = new HashMap<String, Object>();
	
									if (rs.getBoolean(CommonResponseConstants.ISSUCCESS)) {
										returnJson.put(CommonResponseConstants.ISSUCCESS, true);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,"Password changed successfully");
									} else {
										returnJson.put(CommonResponseConstants.ISSUCCESS, false);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,"Problem updating password");
									}
									return returnJson;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
							    }
							}
						});
		

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","ipwdHash"};
		Object[] inParamaterValues = {userDetails.getUserId(),userDetails.getHashedPassword()};
		

		// executing stored procedure
	    Map<String, Object> simpleJdbcCallResult = (HashMap<String, Object>) dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult.get("passwordUpdateStatus"));

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("passwordUpdateStatus");
		
		if(returnjson !=  null)
			return returnjson.get(0);
		else
			return null;
	}
	
	
	/**
	 * This method is used to update the password of user it returns true/false
	 * 
	 * @param userName
	 * @spname usp_Users_Password_Update
	 * @return Users plain java object contains registered user info
	 * @see
	 */

	public Object setPassword(Users userDetails,String spName) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(spName)
				.returningResultSet("passwordUpdateStatus",
						new RowMapper<Map<String, Object>>() {
							@Override
							public Map<String, Object> mapRow(ResultSet rs,int rowNum) {
								try{
									Map<String, Object> returnJson = new HashMap<String, Object>();
	
									if (rs.getBoolean(CommonResponseConstants.ISSUCCESS)) {
										returnJson.put(CommonResponseConstants.ISSUCCESS, true);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString(CommonResponseConstants.SERVICEMESSAGE));
									} else {
										returnJson.put(CommonResponseConstants.ISSUCCESS, false);
										returnJson.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString(CommonResponseConstants.SERVICEMESSAGE));
									}
									return returnJson;
								}catch(SQLException ex){
									return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getMessage());
							    }
							}
						});
		

		// IN parameters for stored procedure
		String[] inParamaters = {"emailOrPhone","ipwdHash"};
		Object[] inParamaterValues = {userDetails.getUniqueIdentity(),userDetails.getHashedPassword()};
		

		// executing stored procedure
	    Map<String, Object> simpleJdbcCallResult = (HashMap<String, Object>) dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
		
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);
		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult.get("passwordUpdateStatus"));

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("passwordUpdateStatus");
		
		if(returnjson !=  null)
			return returnjson.get(0);
		else
			return null;
	}
	/**
	 * This method is used to edit the details,locationdetails of user registered through web it returns 0/1 based on
	 * updation
	 * 
	 * @param Users
	 *            user plain java object
	 * @spname usp_Registration_Users_Update
	 * @return true or false
	 * @see
	 */
	public Object editUserWithLocationDetails(Users userDetails,UserLocation location ) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USER_UPDATE_WITH_LOCATION_SPNAME)
		.returningResultSet("userUpdateStatus",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum)  {
				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {
						responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
						responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
						responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
						responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
						responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
						responseMap.put(ResponseConstantsOfUser.CLOUD_PIC_FILENAME,rs.getString("userFileNameOnCloud"));
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
					} else {
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
						responseMap.put(CommonResponseConstants.ISLOGINEXIST,rs.getBoolean("isLoginExist"));
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,rs.getString("serviceMessage"));
					}
					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
		});

		// IN parameters for stored procedure
		String[] inParamaters = {"iuserID","imobileNumber","iemail","ipwdHash","iDisplayName",
				"iLocID","iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"};
		
		Object[] inParamaterValues = {userDetails.getUserId(),userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getDisplayName(),
				location.getLocationId(),location.getUserLocationLatitude(),location.getUserLocationLongitude(),location.getUserLocationName(),location.getArea(),location.getStreet(),location.getBuilding(),location.getLandmark(),location.getUserLocationCity(),location.getPoBox(),location.getUserLocationState(),location.getUserLocationCountryCode()};

		// executing stored procedure
		Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

		System.out.println("simpleJdbcCallResult = " + simpleJdbcCallResult);

		@SuppressWarnings("unchecked")
		List<Users> returnjson = (List<Users>) simpleJdbcCallResult.get("userUpdateStatus");

		return returnjson.get(0);

	}
	
	
	/**
	 * This method is used to register the details of first time user from web it returns
	 * user object contains all related info of user
	 * 
	 * @param userName
	 * @spname usp_Registration_Users_Insert
	 * @return Users plain java object contains registered user info
	 * @author rpenta
	 */
	public Object addUserWithLocationDetails(Users userDetails,UserLocation location) {

		// getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();

		// registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USER_REGISTRATION_WITH_LOCATION_SPNAME)
		.returningResultSet("userRegisterStatus",
				new RowMapper<HashMap<String, Object>>() {
			@Override
			public HashMap<String, Object> mapRow(ResultSet rs,
					int rowNum){

				try{
					HashMap<String, Object> responseMap = new HashMap<String, Object>();
					if (rs.getBoolean("issuccess")) {
						responseMap.put(CommonResponseConstants.USERID,rs.getInt("userID"));
						responseMap.put(ResponseConstantsOfUser.PROFILEPICURL,rs.getString("profilePicData"));
						responseMap.put(CommonResponseConstants.EMAIL,rs.getString("email"));
						responseMap.put(CommonResponseConstants.PHONE,rs.getString("phone"));
						responseMap.put(ResponseConstantsOfUser.DISPLAYNAME,rs.getString("displayName"));
						responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,true);
						responseMap.put(CommonResponseConstants.ISSUCCESS,true);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User Registered Successfully.");
					} else {
						responseMap.put(ResponseConstantsOfUser.ISVALIDCREDENTIALS,false);
						responseMap.put(CommonResponseConstants.ISSUCCESS,false);
						responseMap.put(CommonResponseConstants.SERVICEMESSAGE,"User already Registered/Invalid Data");
					}

					return responseMap;
				}catch(SQLException ex){
					return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
				}
			}
		});
		
		// IN parameters for stored procedure
				String[] inParamaters = {"imobileNumber","iemail","ipwdHash","iuserName","ideviceTypeID","iapplicationVersion","isecurityQuestionID","isecurityQuestion","isecurityQuestionHash","iRoleID","icreatedBy","iDisplayName",
		"iLatitude","iLongitude","iLocationName","iArea","iStreet","iBuilding","iLandmark","icity","iPoBox","istate","icountryISO"};
				 
				Object[] inParamaterValues = new Object[]{userDetails.getPhone(),userDetails.getEmail(),userDetails.getHashedPassword(),userDetails.getUserName(),userDetails.getDeviceTypeId(),userDetails.getApplicationVersionNo(),1,"","",userDetails.getRoleId(),userDetails.getUserId(),userDetails.getDisplayName(),
						location.getUserLocationLatitude(),location.getUserLocationLongitude(),location.getUserLocationName(),location.getArea(),location.getStreet(),location.getBuilding(),location.getLandmark(),location.getUserLocationCity(),location.getPoBox(),location.getUserLocationState(),location.getUserLocationCountryCode()};	

				// executing stored procedure
				Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);

				@SuppressWarnings("unchecked")
				List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("userRegisterStatus");

				return returnjson.get(0);
	}
	
	/**
	 * This method is used to get the list of managers with role Id '7'
	 * list of securityQuestions
	 * 
	 * @param
	 * @return list of managers
	 * @see
	 */
	public List<Map<String, Object>> fetchManagers() {
		// Jdbc Connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - " + jdbcTemplate);
		String dynamicSqlQuery = UserSqlQueryConstants.GET_MANAGERS;
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList(dynamicSqlQuery);

		return rows;
	}
	
	/**
	* create userRole
	* @param userid
	* @return List<HashMap<String, Object>> with the success or failure information
	*/
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> createUserRole(HashMap<String, Object>  userDetails ) {
	
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		String[] inParamaters;
		Object[] inParamaterValues;
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USER_ROLES_INSERT_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
		{
		@Override
		public Map<String,Object> mapRow(ResultSet rs, int rowNum)
		throws SQLException {
			try{
				   HashMap<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
				return responseMap;	
			}catch(SQLException ex){
				return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			}
		}
	});
	
		inParamaters = new String[] {"iuserId","iRoleName","iDisplayName","iDisplayDescription","iModuleList"};
		inParamaterValues = new Object[]  {userDetails.get("userId"),userDetails.get("roleName"),userDetails.get("displayName"),userDetails.get("roleDescription"),new CommonUtil().buildModuleAccesTypeArray((ArrayList<LinkedHashMap<String, Object>>) userDetails.get("moduleList"))};
	
	// executing stored procedure
	Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
	
	System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
	
	List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
	
	HashMap<String, Object> resultMap = new HashMap<String, Object>();
	resultMap = (HashMap<String, Object>) returnjson.get(0);
	
	return resultMap;
}
	
	/**
	* create userRole
	* @param userid
	* @return List<HashMap<String, Object>> with the success or failure information
	*/
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> updateUserRole(HashMap<String, Object>  userDetails ) {
	
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		String[] inParamaters;
		Object[] inParamaterValues;
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USER_ROLES_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
		{
		@Override
		public Map<String,Object> mapRow(ResultSet rs, int rowNum)
		throws SQLException {
			try{
				   HashMap<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
				return responseMap;	
			}catch(SQLException ex){
				return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),"Service is down.Please try after sometime");
			}
		}
	});
		
	ArrayList<LinkedHashMap<String, Object>> moduleList = (ArrayList<LinkedHashMap<String, Object>>) userDetails.get("moduleList");
	inParamaters = new String[] {"iuserId","iRoleId","iRoleName","iDisplayName","iDisplayDescription","iModuleList","iModuleCount"};
	inParamaterValues = new Object[]  {userDetails.get("userId"),userDetails.get("roleId"),userDetails.get("roleName"),userDetails.get("displayName"),userDetails.get("roleDescription"),new CommonUtil().buildModuleAccesTypeArray(moduleList),moduleList.size()};
	
	// executing stored procedure
	Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
	
	System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
	
	List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
	
	HashMap<String, Object> resultMap = new HashMap<String, Object>();
	
	resultMap = (HashMap<String, Object>) returnjson.get(0);
	
	return resultMap;
}

	/**
	* get Modules
	* @param
	* @return list of modules
	* @see
	*/
	public List<Map<String, Object>> fetchListOfModules() {
			// Jdbc Connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - " + jdbcTemplate);
			String dynamicSqlQuery = UserSqlQueryConstants.GET_MODULES;
			List<Map<String, Object>> rows = null;
			List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
			Map<String, Object> returnMap = null;
			try {
				rows = jdbcTemplate.queryForList(dynamicSqlQuery);
				
				for(Map<String, Object> row : rows){
					returnMap = new HashMap<String, Object>();
					returnMap.put("id",(int)row.get("id"));
					returnMap.put("moduleName",(String)row.get("moduleName"));
					returnMap.put("canView",false);
					returnMap.put("canAdd",false);
					returnMap.put("canEdit",false);
					returnMap.put("canDelete",false);
					returnList.add(returnMap);
				}
				
				
			} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			
			return returnList;
	}
	
	/**
	* get Modules
	* @param
	* @return list of modules
	* @see
	*/
	public List<String> fetchUserRoleModules(Users userdetails) {
			// Jdbc Connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - " + jdbcTemplate);
			String dynamicSqlQuery = CommonSqlQueryConstants.GET_ROLE_MODULES;
			List<Map<String, Object>> rows = null;
			
			try {
				rows = jdbcTemplate.queryForList(dynamicSqlQuery,userdetails.getUserId());
				
			} catch (DataAccessException e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<String> listOfModules = new ArrayList<String>();
			for(Map<String, Object> row : rows){
				boolean canView = (boolean) row.get("View");
				if(canView)
					listOfModules.add(((String) row.get("moduleName")).toLowerCase());
			}
			
			return listOfModules;
	}
	
	/**
	* get Modules
	* @param
	* @return list of modules
	* @see
	*/
	public List<Map<String, Object>> getPermisssions(Users userdetails) {
			// Jdbc Connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - " + jdbcTemplate);
			String dynamicSqlQuery = CommonSqlQueryConstants.GET_PERMISSIONS_LIST;
			List<Map<String, Object>> rows = null;
			
			try {
				rows = jdbcTemplate.queryForList(dynamicSqlQuery,userdetails.getUserId());
				
			} catch (DataAccessException e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return rows;
	}
	
	
	/**
	* get Modules
	* @param
	* @return list of modules
	* @see
	*/
	public boolean isPageAccessible(Users userdetails) {
			// Jdbc Connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			
			int rowCount= 0;
			try {
				rowCount = jdbcTemplate.queryForInt(CommonSqlQueryConstants.GET_MODULES_COUNT,userdetails.getUserId(),userdetails.getHtmlPage());
				
			} catch (DataAccessException e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(rowCount == 0)
				return false;
			else
				return true;
						
			
	}
	
	/**
	* get Modules
	* @param
	* @return list of modules
	* @see
	*/
	public List<Map<String, Object>> getRoleInfo(Users userdetails) {
			// Jdbc Connection
			jdbcTemplate = JDBCConnection.getJdbcTemplate();
			System.out.println("jdbcTemplate - " + jdbcTemplate);
			String dynamicSqlQuery = CommonSqlQueryConstants.GET_ROLE_DETAILS;
			List<Map<String, Object>> rows = null;
			List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
			Map<String, Object> returnMap = null;
			
			try {
				rows = jdbcTemplate.queryForList(dynamicSqlQuery,userdetails.getRoleId());
				
				for(Map<String, Object> row : rows){
					returnMap = new HashMap<String, Object>();
					returnMap.put("id",(int)row.get("id"));
					returnMap.put("moduleName",(String)row.get("moduleName"));
					returnMap.put("canView",(boolean)row.get("View"));
					returnMap.put("canAdd",(boolean)row.get("Add"));
					returnMap.put("canEdit",(boolean)row.get("Edit"));
					returnMap.put("canDelete",(boolean)row.get("Delete"));
					returnList.add(returnMap);
				}
				
			} catch (DataAccessException e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return returnList;
	}
	


/**
	* update userRole Status
	* @param userid
	* @return List<HashMap<String, Object>> with the success or failure information
	*/
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> updateUserRoleStatus(Users userDetails) {
	
		//getting jdbc connection
		jdbcTemplate = JDBCConnection.getJdbcTemplate();
		System.out.println("jdbcTemplate - "+jdbcTemplate);
		String[] inParamaters;
		Object[] inParamaterValues;
		
		//registering jdbc connection and stored procedure to execute
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName(SPNameConstants.USER_ROLE_STATUS_UPDATE_SPNAME).returningResultSet("returnedResult", new RowMapper<Map<String,Object>>()
		{
		@Override
		public Map<String,Object> mapRow(ResultSet rs, int rowNum)
		throws SQLException {
			try{
				   HashMap<String,Object> responseMap = new HashMap<String, Object>();
					boolean isSuccess = rs.getBoolean("issuccess");
					responseMap.put(CommonResponseConstants.ISSUCCESS, isSuccess);
					responseMap.put(CommonResponseConstants.SERVICEMESSAGE, rs.getString("serviceMessage"));
				return responseMap;	
			}catch(SQLException ex){
				return  exceptionHandling.customGenericExceptionJson(new Integer(ex.getErrorCode()),ex.getLocalizedMessage());
			}
		}
	});
		
	
	inParamaters = new String[] {"iuserID","iRoleID","isActive"};
	inParamaterValues = new Object[]  {userDetails.getUserId(),userDetails.getRoleId(),userDetails.getIsActive()};
	
	// executing stored procedure
	Map<String, Object> simpleJdbcCallResult = dbUtilities.executeStoredProcedure(inParamaters, inParamaterValues, simpleJdbcCall);
	
	System.out.println("simpleJdbcCallResult = "+simpleJdbcCallResult);
	
	List<HashMap<String, Object>> returnjson = (List<HashMap<String, Object>>) simpleJdbcCallResult.get("returnedResult");
	
	HashMap<String, Object> resultMap = new HashMap<String, Object>();
	
	resultMap = (HashMap<String, Object>) returnjson.get(0);
	
	return resultMap;
}
}