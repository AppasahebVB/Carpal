package trac.beans.customerbean;

/**Stores Customer related information
 * 
 */

public class Customer {
	
	
	private int userId;
	private String searchBy;
	private String customerId;
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private String emailId;
	private String updatedBy;
	
	private String customerLocationLatitude;
	private String customerLocationLongitude;
	private String customerLocationName;
	private Object userLocationList;
	private String customerLocationCity;
	private String customerLocationCountryCode;
	private String customerLocationState;
	private boolean isSuccess;
	private String serviceMessage;
	private String locationId;
	
	private String area;
	private String street;
	private String building;
	private String landmark;
	private String poBox;
	
	 private int customerCareResponse;
     private int driverResponse;
     private int servicesProvided;
     private int overallRating;
     private String suggestions;
     private int caseId;
	
	private boolean saveForReference;
	
	private String isDestinationLocation;
	private int assistanceId;
	private boolean isCustomer;
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	
	public boolean getIsCustomer() { 
		return isCustomer; 
		} 
	
	/** * @param isCustomer the isCustomer to set */
	public void setIsCustomer(boolean isCustomer) { 
		this.isCustomer = isCustomer; 
	}
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the customerLocationLatitude
	 */
	public String getCustomerLocationLatitude() {
		return customerLocationLatitude;
	}
	/**
	 * @param customerLocationLatitude the customerLocationLatitude to set
	 */
	public void setCustomerLocationLatitude(String customerLocationLatitude) {
		this.customerLocationLatitude = customerLocationLatitude;
	}
	/**
	 * @return the customerLocationLongitude
	 */
	public String getCustomerLocationLongitude() {
		return customerLocationLongitude;
	}
	/**
	 * @param customerLocationLongitude the customerLocationLongitude to set
	 */
	public void setCustomerLocationLongitude(String customerLocationLongitude) {
		this.customerLocationLongitude = customerLocationLongitude;
	}
	/**
	 * @return the customerLocationName
	 */
	public String getCustomerLocationName() {
		return customerLocationName;
	}
	/**
	 * @param customerLocationName the customerLocationName to set
	 */
	public void setCustomerLocationName(String customerLocationName) {
		this.customerLocationName = customerLocationName;
	}
	/**
	 * @return the userLocationList
	 */
	public Object getUserLocationList() {
		return userLocationList;
	}
	/**
	 * @param userLocationList the userLocationList to set
	 */
	public void setUserLocationList(Object userLocationList) {
		this.userLocationList = userLocationList;
	}
	/**
	 * @return the customerLocationCity
	 */
	public String getCustomerLocationCity() {
		return customerLocationCity;
	}
	/**
	 * @param customerLocationCity the customerLocationCity to set
	 */
	public void setCustomerLocationCity(String customerLocationCity) {
		this.customerLocationCity = customerLocationCity;
	}
	/**
	 * @return the customerLocationCountryCode
	 */
	public String getCustomerLocationCountryCode() {
		return customerLocationCountryCode;
	}
	/**
	 * @param customerLocationCountryCode the customerLocationCountryCode to set
	 */
	public void setCustomerLocationCountryCode(String customerLocationCountryCode) {
		this.customerLocationCountryCode = customerLocationCountryCode;
	}
	/**
	 * @return the customerLocationState
	 */
	public String getCustomerLocationState() {
		return customerLocationState;
	}
	/**
	 * @param customerLocationState the customerLocationState to set
	 */
	public void setCustomerLocationState(String customerLocationState) {
		this.customerLocationState = customerLocationState;
	}
	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}
	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}
	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the building
	 */
	public String getBuilding() {
		return building;
	}
	/**
	 * @param building the building to set
	 */
	public void setBuilding(String building) {
		this.building = building;
	}
	/**
	 * @return the landmark
	 */
	public String getLandmark() {
		return landmark;
	}
	/**
	 * @param landmark the landmark to set
	 */
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	/**
	 * @return the poBox
	 */
	public String getPoBox() {
		return poBox;
	}
	/**
	 * @param poBox the poBox to set
	 */
	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
	/**
	 * @return the saveForReference
	 */
	public boolean isSaveForReference() {
		return saveForReference;
	}
	/**
	 * @param saveForReference the saveForReference to set
	 */
	public void setSaveForReference(boolean saveForReference) {
		this.saveForReference = saveForReference;
	}
	/**
	 * @return the isDestinationLocation
	 */
	public String getIsDestinationLocation() {
		return isDestinationLocation;
	}
	/**
	 * @param isDestinationLocation the isDestinationLocation to set
	 */
	public void setIsDestinationLocation(String isDestinationLocation) {
		this.isDestinationLocation = isDestinationLocation;
	}
	/**
	 * @return the customerCaseResponse
	 */
	public int getCustomerCareResponse() {
		return customerCareResponse;
	}
	/**
	 * @param customerCaseResponse the customerCaseResponse to set
	 */
	public void setCustomerCareResponse(int customerCareResponse) {
		this.customerCareResponse = customerCareResponse;
	}
	/**
	 * @return the driverResponse
	 */
	public int getDriverResponse() {
		return driverResponse;
	}
	/**
	 * @param driverResponse the driverResponse to set
	 */
	public void setDriverResponse(int driverResponse) {
		this.driverResponse = driverResponse;
	}
	/**
	 * @return the servicesProvided
	 */
	public int getServicesProvided() {
		return servicesProvided;
	}
	/**
	 * @param servicesProvided the servicesProvided to set
	 */
	public void setServicesProvided(int servicesProvided) {
		this.servicesProvided = servicesProvided;
	}
	/**
	 * @return the overallRating
	 */
	public int getOverallRating() {
		return overallRating;
	}
	/**
	 * @param overallRating the overallRating to set
	 */
	public void setOverallRating(int overallRating) {
		this.overallRating = overallRating;
	}
	/**
	 * @return the suggestions
	 */
	public String getSuggestions() {
		return suggestions;
	}
	/**
	 * @param suggestions the suggestions to set
	 */
	public void setSuggestions(String suggestions) {
		this.suggestions = suggestions;
	}
	/**
	 * @return the caseId
	 */
	public int getCaseId() {
		return caseId;
	}
	/**
	 * @param caseId the caseId to set
	 */
	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}

	/**
	* @return the assistanceServiceId
	*/
	public int getAssistanceId() {
	return assistanceId;
	}
	/**
	* @param assistanceServiceId the assistanceServiceId to set
	*/
	public void setAssistanceId(int assistanceId) {
	this.assistanceId = assistanceId;
	}
	
	
}
