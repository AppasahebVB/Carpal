package trac.beans.truckbean;

import java.io.Serializable;


/** Stores the Truck related information
 * 
 */

public class Truck implements Serializable{

	private static final long serialVersionUID = -2107587111242700732L;

	private int userId;
	private int truckId;
	private String vin;
	private Long truckBrandID;

	private Long truckModelID;
	private String truckCode;
	private String truckBrandName;
	private String truckModelName;

	private Object isSuccess;
	private String serviceMessage;

	private String dateOfPurchase;
	private String registrationDate;
	private String plateCode;
	private String plateNumber;
	private String dateOfAcquiring;
	private String modelYear;
	private String truckColor;
	private String totalCost;
	private String servicesProvided;
	private int availableHoursPerDay;
	private String truckNickName;

	private String truckType;
	private String lastCaseAttended;
	private int truckpolicyNo;
	private String truckPolicyNumber;
	private String insCompanyName;
	private String truckPolicyIssuedDate;
	private String truckPolicyExpiryDate;
	private String truckReRegistrationDueDate;
	
	private int fromValue;
	
	private int toValue;
	
	
	//Truck Expense Details
		private int expenseID;
		private int expenseType;
		private String expenseValue;
		private String paidTo;
		private String expenseTime;
		private int paymentType;
		private String receiptNumber;
		private String comments;
		private String currency;
		private String expenseTimeTo;
		private String expenseTimeFrom;
		private String searchBy;
		
		
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTruckId() {
		return truckId;
	}
	public void setTruckId(int truckId) {
		this.truckId = truckId;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public Long getTruckBrandID() {
		return truckBrandID;
	}
	public void setTruckBrandID(Long truckBrandID) {
		this.truckBrandID = truckBrandID;
	}
	public Long getTruckModelID() {
		return truckModelID;
	}
	public void setTruckModelID(Long truckModelID) {
		this.truckModelID = truckModelID;
	}
	
	public String getTruckBrandName() {
		return truckBrandName;
	}
	public void setTruckBrandName(String truckBrandName) {
		this.truckBrandName = truckBrandName;
	}
	public String getTruckModelName() {
		return truckModelName;
	}
	public void setTruckModelName(String truckModelName) {
		this.truckModelName = truckModelName;
	}
	public Object getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Object isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getServiceMessage() {
		return serviceMessage;
	}
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}
	public String getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getPlateCode() {
		return plateCode;
	}
	public void setPlateCode(String plateCode) {
		this.plateCode = plateCode;
	}
	public String getPlateNumber() {
		return plateNumber;
	}
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	public String getDateOfAcquiring() {
		return dateOfAcquiring;
	}
	public void setDateOfAcquiring(String dateOfAcquiring) {
		this.dateOfAcquiring = dateOfAcquiring;
	}
	public String getModelYear() {
		return modelYear;
	}
	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}
	public String getTruckColor() {
		return truckColor;
	}
	public void setTruckColor(String truckColor) {
		this.truckColor = truckColor;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getServicesProvided() {
		return servicesProvided;
	}
	public void setServicesProvided(String servicesProvided) {
		this.servicesProvided = servicesProvided;
	}
	public int getAvailableHoursPerDay() {
		return availableHoursPerDay;
	}
	public void setAvailableHoursPerDay(int availableHoursPerDay) {
		this.availableHoursPerDay = availableHoursPerDay;
	}
	public String getTruckType() {
		return truckType;
	}
	public void setTruckType(String truckType) {
		this.truckType = truckType;
	}
	public String getLastCaseAttended() {
		return lastCaseAttended;
	}
	public void setLastCaseAttended(String lastCaseAttended) {
		this.lastCaseAttended = lastCaseAttended;
	}
	
	public int getTruckpolicyNo() {
		return truckpolicyNo;
	}
	public void setTruckpolicyNo(int truckpolicyNo) {
		this.truckpolicyNo = truckpolicyNo;
	}
	
	public String getTruckPolicyIssuedDate() {
		return truckPolicyIssuedDate;
	}
	public void setTruckPolicyIssuedDate(String truckPolicyIssuedDate) {
		this.truckPolicyIssuedDate = truckPolicyIssuedDate;
	}
	public String getTruckPolicyExpiryDate() {
		return truckPolicyExpiryDate;
	}
	public void setTruckPolicyExpiryDate(String truckPolicyExpiryDate) {
		this.truckPolicyExpiryDate = truckPolicyExpiryDate;
	}
	/**
	 * @return the truckCode
	 */
	public String getTruckCode() {
		return truckCode;
	}
	/**
	 * @param truckCode the truckCode to set
	 */
	public void setTruckCode(String truckCode) {
		this.truckCode = truckCode;
	}
	/**
	 * @return the truckPolicyNumber
	 */
	public String getTruckPolicyNumber() {
		return truckPolicyNumber;
	}
	/**
	 * @param truckPolicyNumber the truckPolicyNumber to set
	 */
	public void setTruckPolicyNumber(String truckPolicyNumber) {
		this.truckPolicyNumber = truckPolicyNumber;
	}
	/**
	 * @return the insCompanyName
	 */
	public String getInsCompanyName() {
		return insCompanyName;
	}
	/**
	 * @param insCompanyName the insCompanyName to set
	 */
	public void setInsCompanyName(String insCompanyName) {
		this.insCompanyName = insCompanyName;
	}
	public String getTruckReRegistrationDueDate() {
		return truckReRegistrationDueDate;
	}
	public void setTruckReRegistrationDueDate(String truckReRegistrationDueDate) {
		this.truckReRegistrationDueDate = truckReRegistrationDueDate;
	}
	public String getTruckNickName() {
		return truckNickName;
	}
	public void setTruckNickName(String truckNickName) {
		this.truckNickName = truckNickName;
	}
	/**
	 * @return the fromValue
	 */
	public int getFromValue() {
		return fromValue;
	}
	/**
	 * @param fromValue the fromValue to set
	 */
	public void setFromValue(int fromValue) {
		this.fromValue = fromValue;
	}
	/**
	 * @return the toValue
	 */
	public int getToValue() {
		return toValue;
	}
	/**
	 * @param toValue the toValue to set
	 */
	public void setToValue(int toValue) {
		this.toValue = toValue;
	}
	/**
	 * @return the expenseID
	 */
	public int getExpenseID() {
		return expenseID;
	}
	/**
	 * @param expenseID the expenseID to set
	 */
	public void setExpenseID(int expenseID) {
		this.expenseID = expenseID;
	}
	/**
	 * @return the expenseType
	 */
	public int getExpenseType() {
		return expenseType;
	}
	/**
	 * @param expenseType the expenseType to set
	 */
	public void setExpenseType(int expenseType) {
		this.expenseType = expenseType;
	}
	/**
	 * @return the expenseValue
	 */
	public String getExpenseValue() {
		return expenseValue;
	}
	/**
	 * @param expenseValue the expenseValue to set
	 */
	public void setExpenseValue(String expenseValue) {
		this.expenseValue = expenseValue;
	}
	/**
	 * @return the paidTo
	 */
	public String getPaidTo() {
		return paidTo;
	}
	/**
	 * @param paidTo the paidTo to set
	 */
	public void setPaidTo(String paidTo) {
		this.paidTo = paidTo;
	}
	/**
	 * @return the expenseTime
	 */
	public String getExpenseTime() {
		return expenseTime;
	}
	/**
	 * @param expenseTime the expenseTime to set
	 */
	public void setExpenseTime(String expenseTime) {
		this.expenseTime = expenseTime;
	}
	/**
	 * @return the paymentType
	 */
	public int getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the receiptNumber
	 */
	public String getReceiptNumber() {
		return receiptNumber;
	}
	/**
	 * @param receiptNumber the receiptNumber to set
	 */
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the expenseTimeTo
	 */
	public String getExpenseTimeTo() {
		return expenseTimeTo;
	}
	/**
	 * @param expenseTimeTo the expenseTimeTo to set
	 */
	public void setExpenseTimeTo(String expenseTimeTo) {
		this.expenseTimeTo = expenseTimeTo;
	}
	/**
	 * @return the expenseTimeFrom
	 */
	public String getExpenseTimeFrom() {
		return expenseTimeFrom;
	}
	/**
	 * @param expenseTimeFrom the expenseTimeFrom to set
	 */
	public void setExpenseTimeFrom(String expenseTimeFrom) {
		this.expenseTimeFrom = expenseTimeFrom;
	}
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
