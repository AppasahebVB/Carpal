package trac.beans.dashboardbean;

import java.util.List;
import java.util.Map;

public class Dashboard {
	
	/** Stores report information
	 * 
	 */
	
	private int userId;
	private int reportId;
	private String reportName;
	private String reportTable;
	private String reportTofetch;
	private String tableSqlQuery;
	private String reportSqlQuery;
	private String columnNames;
	private String whereClause;
	private boolean isSuccess;
	private boolean isMetaData;
	private boolean isTableRecordFound;
	private boolean isReportRecordFound;
	private String serviceMessage;
	private List<Map<String, Object>>  tableResultList;
	private List<Map<String, Object>>  reportResultList;
	private List<Map<String, String>>  tableMetaDataList;
	
	private List<Map<String, Object>> fieldsToBeDisplayed;

	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the reportId
	 */
	public int getReportId() {
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the reportTable
	 */
	public String getReportTable() {
		return reportTable;
	}

	/**
	 * @param reportTable the reportTable to set
	 */
	public void setReportTable(String reportTable) {
		this.reportTable = reportTable;
	}

	/**
	* @return the fieldsToBeDisplayed
	*/
	public List<Map<String, Object>> getFieldsToBeDisplayed() {
		return fieldsToBeDisplayed;
	}
	
	/**
	* @param fieldsToBeDisplayed the fieldsToBeDisplayed to set
	*/
	public void setFieldsToBeDisplayed(List<Map<String, Object>> fieldsToBeDisplayed) {
		this.fieldsToBeDisplayed = fieldsToBeDisplayed;
	}

	/**
	 * @return the sqlQuery
	 */
	public String getTableSqlQuery() {
		return tableSqlQuery;
	}

	/**
	 * @param sqlQuery the sqlQuery to set
	 */
	public void setTableSqlQuery(String tableSqlQuery) {
		this.tableSqlQuery = tableSqlQuery;
	}

	/**
	 * @return the resultList
	 */
	public List<Map<String, Object>> getTableResultList() {
		return tableResultList;
	}

	/**
	 * @param resultList the resultList to set
	 */
	public void setTableResultList(List<Map<String, Object>> tableResultList) {
		this.tableResultList = tableResultList;
	}

	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	/**
	 * @return the serviceMessage
	 */
	public String getServiceMessage() {
		return serviceMessage;
	}

	/**
	 * @param serviceMessage the serviceMessage to set
	 */
	public void setServiceMessage(String serviceMessage) {
		this.serviceMessage = serviceMessage;
	}

	/**
	 * @return the reportSqlQuery
	 */
	public String getReportSqlQuery() {
		return reportSqlQuery;
	}

	/**
	 * @param reportSqlQuery the reportSqlQuery to set
	 */
	public void setReportSqlQuery(String reportSqlQuery) {
		this.reportSqlQuery = reportSqlQuery;
	}

	/**
	 * @return the reportResultList
	 */
	public List<Map<String, Object>> getReportResultList() {
		return reportResultList;
	}

	/**
	 * @param reportResultList the reportResultList to set
	 */
	public void setReportResultList(List<Map<String, Object>> reportResultList) {
		this.reportResultList = reportResultList;
	}

	/**
	 * @return the isTableRecordFound
	 */
	public boolean isTableRecordFound() {
		return isTableRecordFound;
	}

	/**
	 * @param isTableRecordFound the isTableRecordFound to set
	 */
	public void setTableRecordFound(boolean isTableRecordFound) {
		this.isTableRecordFound = isTableRecordFound;
	}

	/**
	 * @return the isReportRecordFound
	 */
	public boolean isReportRecordFound() {
		return isReportRecordFound;
	}

	/**
	 * @param isReportRecordFound the isReportRecordFound to set
	 */
	public void setReportRecordFound(boolean isReportRecordFound) {
		this.isReportRecordFound = isReportRecordFound;
	}

	/**
	 * @return the reportTofetch
	 */
	public String getReportTofetch() {
		return reportTofetch;
	}

	/**
	 * @param reportTofetch the reportTofetch to set
	 */
	public void setReportTofetch(String reportTofetch) {
		this.reportTofetch = reportTofetch;
	}

	/**
	 * @return the tableMetaDataList
	 */
	public List<Map<String, String>> getTableMetaDataList() {
		return tableMetaDataList;
	}

	/**
	 * @param tableMetaDataList the tableMetaDataList to set
	 */
	public void setTableMetaDataList(List<Map<String, String>> tableMetaDataList) {
		this.tableMetaDataList = tableMetaDataList;
	}

	

	/**
	 * @return the isMetaData
	 */
	public boolean isMetaData() {
		return isMetaData;
	}

	/**
	 * @param isMetaData the isMetaData to set
	 */
	public void setMetaData(boolean isMetaData) {
		this.isMetaData = isMetaData;
	}

	public String getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String columnNames) {
		this.columnNames = columnNames;
	}

	/**
	 * @return the whereClause
	 */
	public String getWhereClause() {
		return whereClause;
	}

	/**
	 * @param whereClause the whereClause to set
	 */
	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}
	
	
}
