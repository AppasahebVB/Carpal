package trac.constants.storedprocedureconstants;

/**
 *  All Stored Procedure Names are placed in this class
 *
 */

public class SPNameConstants {
	
	//user related sp names
	public static final String CHANGE_PASSWORD_SPNAME = "usp_Users_Chnage_Password";
	public static final String RESET_PASSWORD_SPNAME = "usp_Users_Password_Update";
	public static final String UPDATE_PASSWORD_SPNAME = "usp_Users_Set_Password";
	public static final String USER_REGISTRATION_SPNAME = "usp_Registration_Users_Insert";
	public static final String USER_UPDATE_SPNAME = "usp_Registration_Users_Update";
	public static final String USER_SECURITYQUESTION_SPNAME = "usp_Registration_SecurityQuestion_Users_Select";
	public static final String USER_TEMP_PWD_SPNAME = "usp_randomPassword_verfication";
	public static final String USER_ACTIVATION = "usp_user_activation";
	public static final String USER_REMOVE = "usp_user_disable";
	
	
	
	public static final String USER_REGISTRATION_WITH_LOCATION_SPNAME = "usp_Registration_Users_Location_Insert";
	public static final String USER_UPDATE_WITH_LOCATION_SPNAME = "usp_Registration_Users_Location_Update";
	public static final String USER_ROLES_INSERT_SPNAME = "usp_role_insert";
	public static final String USER_ROLES_UPDATE_SPNAME = "usp_role_update";
	public static final String USER_ROLE_STATUS_UPDATE_SPNAME = "usp_role_status_update";
	
	public static final String USER_ROLES_MODULE_INSERT_SPNAME = "usp_role_module_insert";
	
	public static final String GET_MODULES = "select * from lkp_modules";


	//user location related sp names
	public static final String USERLOC_REGISTRATION_SPNAME = "usp_Location_Locations_Insert";
	public static final String USERLOC_UPDATE_SPNAME = "usp_Location_Locations_Update";
	public static final String USERLOC_FETCH_SPNAME = "usp_Location_Locations_Select";
	public static final String USERLOC_UNREGISTER_SPNAME = "usp_Location_Locations__Delete";
	
	//vehicle related sp names
	public static final String VEHICLE_REGISTRATION_SPNAME = "usp_Vehicle_Vehicles_Insert";
	public static final String VEHICLE_UPDATE_SPNAME = "usp_Vehicle_Vehicles_Update";
	public static final String VEHICLE_FETCH_SPNAME = "usp_Vehicle_Vehicles_Select";
	public static final String GET_VEHICLE_BRAND_FETCH_SPNAME = "usp_vehicles_vehicleBrands_select";
	public static final String VEHICLE_UNREGISTER_SPNAME  = "usp_Vehicle_Vehicles_Delete";
	
	//case related sp names
	public static final String CASE_REGISTRATION_SPNAME = "usp_RoadAssassinate_cases_Insert";
	public static final String CASE_HISTORY_FETCH_SPNAME = "usp_ServiceHistoryAPI_Select";
	public static final String CASE_STATUS_UPDATE_SPNAME = "usp_CaseStatus_Update";
	public static final String CASE_FAULTS_SERVICES_SPNAME = "usp_case_registered_faults_services";
	
	public static final String CASE_FETCH_SCHEDULE_SPNAME = "usp_case_fetch_schedule";
	public static final String CASE_SCHEDULE_UPDATE_SPNAME = "usp_cases_schedule_update";
	
	
	//driver related sp names
		public static final String DRIVER_PERSONAL_DETAILS_INSERT_SPNAME = "usp_driver_details_insert";
		public static final String DRIVER_PERSONAL_DETAILS_UPDATE_SPNAME = "usp_driver_details_update";
		public static final String DRIVER_PERSONAL_DETAILS_FETCH_SPNAME  = "usp_driver_details_fetch";

		public static final String DRIVER_CONTACT_DETAILS_INSERT_SPNAME = "usp_driver_contact_details_insert";
		public static final String DRIVER_CONTACT_DETAILS_UPDATE_SPNAME = "usp_driver_contact_details_update";
		public static final String DRIVER_CONTACT_DETAILS_FETCH_SPNAME = "usp_driver_contact_details_fetch";

		public static final String DRIVER_EMERGENCY_CONTACT_DETAILS_INSERTORUPDATE_SPNAME = "usp_driver_emergency_contact_details_insertorupdate";
		public static final String DRIVER_EMERGENCY_CONTACT_DETAILS_FETCH_SPNAME = "usp_driver_emergency_contact_details_fetch";

		public static final String DRIVER_EXPERIENCE_DETAILS_INSERT_SPNAME = "usp_driver_experience_details_bulk_insert";
		public static final String DRIVER_EXPERIENCE_DETAILS_UPDATE_SPNAME = "usp_driver_experience_details_update";
		public static final String DRIVER_EXPERIENCE_DETAILS_FETCH_SPNAME  = "usp_driver_experience_details_fetch";

		public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_INSERT_SPNAME = "usp_driver_present_experience_details_insert";
		public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_UPDATE_SPNAME = "usp_driver_present_experience_details_update";
		public static final String DRIVER_PRESENT_EXPERIENCE_DETAILS_FETCH_SPNAME  = "usp_driver_present_experience_details_fetch";
		
		 public static final String DRIVER_INFO_FETCH_SPNAME   = "usp_driver_info_fetch";
		 public static final String GET_FULL_DRIVER_INFO   = "usp_driver_complete_details";
		 
         public static final String DRIVER_INFO_DELETE_SPNAME = "usp_driver_delete";

		public static final String DRIVER_SHIFT_INFO_INSERT_SPNAME = "usp_driver_shift_info_insert";
		public static final String DRIVER_SHIFT_INFO_UPDATE_SPNAME = "usp_driver_shift_info_update";
		public static final String DRIVER_SHIFT_INFO_DELETE_SPNAME = "usp_driver_shift_info_delete";
		public static final String DRIVER_SHIFTINFO_FETCH_SPNAME   = "usp_driver_shift_info_fetch";
		public static final String DRIVER_SHIFT_SCHEDULE_FETCH_SPNAME   = "usp_driver_shift_schedule";

		//Shift InOut Timings
		public static final String DRIVER_SHIFT_INOUT_INFO_SAVE_SPNAME    = "usp_driver_punch_insert";
		public static final String DRIVER_SHIFT_INOUT_INFO_UPDATE_SPNAME  = "usp_driver_punch_update";
		public static final String DRIVER_SHIFT_INOUT_INFO_FETCH_SPNAME   = "usp_driver_punch_info_fetch";
		public static final String DRIVER_SHIFT_INOUT_INFO_FETCH_BY_SHIFTID_SPNAME   = "usp_driver_punch_info_by_shift";
		public static final String DRIVER_SHIFT_INOUT_INFO_FETCH_BY_DETAILS_SPNAME   = "usp_driver_punch_info_by_Driver";
		
		//get driver info
		public static final String GET_DRIVER_INFO_SPNAME   = "usp_driver_details_by_driverId";


		//Shift related sp names
		public static final String SHIFT_INFO_INSERT_SPNAME = "usp_shift_insert";
		public static final String SHIFT_INFO_UPDATE_SPNAME = "usp_shift_update";
		public static final String SHIFT_INFO_DELETE_SPNAME = "usp_shift_delete";

		//truck related spnames
		public static final String TRUCK_INFO_INSERT_SPNAME = "usp_truck_Insert";
		public static final String TRUCK_INFO_UPDATE_SPNAME = "usp_truck_Update";
		public static final String TRUCK_INFO_ACTIVATION_SPNAME = "usp_truck_activation";
		public static final String TRUCK_INFO_REMOVE_SPNAME = "usp_truck_remove";
		
		public static final String TRUCK_INFO_FETCH_SPNAME  = "usp_truck_Fetch";
		public static final String GET_TRUCK_BRAND_FETCH_SPNAME  = "usp_truck_truckBrands_select";
		
		public static final String TRUCK_INFO_FETCH_WITH_RANGE_SPNAME  = "usp_truck_Fetch_With_Range";
		public static final String FETCH_TRUCK_SHIFT_MANAGER_SPNAME  = "usp_fetch_shift_manager_truck";
		
		
		//customer case related sp names
		public static final String CUSTOMER_CASE_REGISTRATION_SPNAME = "usp_customer_cases_Insert";
		public static final String CUSTOMER_CASE_HISTORY_FETCH_SPNAME = "usp_customer_cases_fetch";
		public static final String CUSTOMER_CASE_DETAILS_UPDATE_SPNAME = "usp_customer_caseinfo_update";
		
		//assigning case
		public static final String CASE_FETCH_AVAILABLE_DRIVERS_SPNAME = "usp_cases_fetch_available_driver";
		public static final String CASE_ASSIGN_CASE_TO_DRIVER_SPNAME = "usp_cases_assign_case_to_driver";
		public static final String CASE_SAVE_CASE_PREREQUISITE_SPNAME = "usp_case_prerequisite_Insert";
		public static final String CASE_SAVE_CASE_ON_JOB_SPNAME = "usp_case_on_jobdetails_Insert";
		
		public static final String CASE_FETCH_CASES_SPNAME = "usp_case_fetch_allcases";
		
		//customer location related sp names
		public static final String CUSTOMERLOC_REGISTRATION_SPNAME = "usp_Customer_Location_Insert";
	    public static final String CUSTOMERLOC_UPDATE_SPNAME = "usp_Customer_Location_Update";
		public static final String CUSTOMERLOC_FETCH_SPNAME = "usp_Customer_Location_Select";
		public static final String CUSTOMER_DEST_LOC_FETCH_SPNAME = "usp_Customer_DestinationLocation_Select";
		
		public static final String CUSTOMER_REGISTRATION_SPNAME = "usp_Customer_Registration_Insert";
		public static final String CUSTOMER_UPDATION_SPNAME = "usp_Customer_Registration_Update";
		public static final String CUSTOMER_FETCH_SPNAME = "usp_customer_fetch";
		public static final String FETCH_CASE_DETAILS = "usp_fetch_case_details";
		public static final String FETCH_CASE_DETAILS_BY_ID = "usp_fetch_case_details_by_caseId";
		public static final String CASE_SAVE_SATISFACTION_SPNAME = "usp_case_save_satisfaction";
			
		public static final String CASE_FETCH_CASEDETAILS_SPNAME = "usp_customer_fetch_casedetails";
		public static final String CASE_FETCH_QUE_SPNAME = "usp_case_fetch_casesinque"; 
		public static final String CASE_ASSIGN_CASE_SELF_SPNAME = "usp_case_assign_self";
		
		public static final String CASE_FETCH_CASES_COUNT_SPNAME = "usp_cases_fetch_count";
		public static final String CASE_FAULT_UPDATE_SPNAME = "usp_CaseFault_Update";
		
		
		public static final String USERDESTINATIONLOC_REGISTRATION_SPNAME = "usp_User_DestinationLocation_Insert";
		public static final String USERDESTINATIONLOC_UPDATE_SPNAME = "usp_User_DestinationLocation_Update";
		
		//shift schedule
		public static final String FETCH_SCHEDULE_DETAILS = "usp_shift_schedule_of_driver";
		public static final String GENARATE_SCHEDULE_DETAILS = "usp_genarate_schedule";
		public static final String DRIVER_SCHEDULE_UPDATE = "usp_update_schedule_of_driver";
		
		//report 
		public static final String REPORT_OPERATIONS = "usp_report_operations";
		public static final String REPORT_ATTENDANCE = "usp_report_attendance";
		public static final String REPORT_TRUCK_INFO = "usp_report_truck";
		
		public static final String VEHICLE_DISPLAY_ORDER_SPNAME ="usp_Vehicle_DisplayOrder_update";
	
		public static final String TRUCK_EXPENSE_INFO_INSERT_SPNAME = "usp_truck_ExpenseEntryForm_Insert";
		public static final String TRUCK_EXPENSE_INFO_UPDATE_SPNAME = "usp_truck_ExpenseEntryForm_Update";
		public static final String TRUCK_EXPENSE_INFO_FETCH_SPNAME  = "usp_truck_ExpenseEntryForm_FetchByDate";
		
		public static final String USP_SAVE_CASE_COMMENT  = "usp_save_case_comment";
		
		public static final String REPORT_INSERT_SPNAME = "usp_report_insert";
		public static final String REPORT_UPDATE_SPNAME = "usp_report_update";
		public static final String REPORT_DELETE_SPNAME = "usp_report_delete";
		public static final String REPORT_FETCH_SPNAME = "usp_report_fetch";
		
		public static final String VEHICLE_SERVICE_LOCATIONS_FETCH_SPNAME = "usp_vehicle_service_locations";

}
