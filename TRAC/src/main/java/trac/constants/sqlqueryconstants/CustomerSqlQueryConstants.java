package trac.constants.sqlqueryconstants;

/**
 *  Customer Sql Query Constants are placed in this class
 *
 */


public class CustomerSqlQueryConstants {

	public static final String FETCH_SUBCASES = "SELECT * from caseinfo WHERE caseID=?";
	public static final String CUSTOMER_AUTOCOMPLETE_QUERY = "SELECT CONCAT(mobileNumber,\",\",CONCAT(firstName,\" \",lastName)) as customerNameNumber FROM trac.customers where mobileNumber LIKE ";
	public static final String VEHICLE_AUTOCOMPLETE_QUERY = "SELECT CONCAT(vin,\",\",lbrands.Description,\",\",lmodels.Description,\",\",IF(length(modelYear) = 5,modelYear,DATE_FORMAT(modelYear,'%Y'))) AS vehicleDetails" 
							+" FROM trac.vehicles v LEFT JOIN lkp_vehiclebrands_loc lbrands ON lbrands.ID = v.vehicleBrandID"  
							+" LEFT JOIN lkp_vehiclemodels_loc lmodels ON lmodels.ID = v.vehicleModelID"
							+" LEFT JOIN customervehicles cv ON cv.vehicleID = v.vehicleID ";


}
