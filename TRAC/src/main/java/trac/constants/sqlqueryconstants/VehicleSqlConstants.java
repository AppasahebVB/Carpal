package trac.constants.sqlqueryconstants;

/**
 *  Vehicle Sql Query Constants are placed in this class
 *
 */

public class VehicleSqlConstants {

	 public static final String UPDATE_VEHICLE_PIC =  "UPDATE vehicles SET vehiclePicURL = ?,vehicleFileNameOnCloud = ? WHERE vehicleID = ?";
}
