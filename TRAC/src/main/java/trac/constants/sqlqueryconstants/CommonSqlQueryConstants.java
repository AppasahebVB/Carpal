package trac.constants.sqlqueryconstants;

/**
 *  Common Sql Query Constants are placed in this class
 *
 */

public class CommonSqlQueryConstants {
	public static final String GET_COUNTRY_LIST =  "SELECT ISOCode,Name FROM `trac`.`lkp_countries_loc` where LocaleID = 1 and ISOCode != '00'";
	
	public static final String GET_ROLE_DETAILS =  "SELECT rolemodule.id,modules.id as moduleId,modules.moduleName,secroles.RoleID,secroles.Displayname,secroles.Displayname,secroles.RoleName,"+
			" rolemodule.View,rolemodule.Add,rolemodule.Edit,"+
			" rolemodule.Delete from rolemoduleassignment rolemodule "+
			" join lkp_secroles_loc secroles on rolemodule.roleId = secroles.RoleID "+
			" join lkp_modules modules on modules.id = rolemodule.moduleId "+
			" where rolemodule.roleId = ?";
	
	public static final String GET_ROLE_MODULES =  "SELECT userrole.userID,userrole.roleID,secroles_loc.displayName,moduleassignment.moduleId,modules.moduleName,moduleassignment.View,moduleassignment.Add,moduleassignment.Edit,moduleassignment.Delete from usersroles userrole join"+
	" lkp_secroles_loc secroles_loc on  secroles_loc.RoleID = userrole.roleID"+
	" left join rolemoduleassignment moduleassignment on secroles_loc.RoleID = moduleassignment.roleId"+ 
	" join lkp_modules modules on moduleassignment.moduleId = modules.id"+
	" where userID = ? group by modules.id";
	
	public static final String GET_PERMISSIONS_LIST =  "SELECT moduleassignment.moduleId,modules.moduleName,moduleassignment.View"+
			" ,moduleassignment.Add,moduleassignment.Edit,moduleassignment.Delete from usersroles userrole join"+
			" lkp_secroles_loc secroles_loc on  secroles_loc.RoleID = userrole.roleID"+
			" left join rolemoduleassignment moduleassignment on secroles_loc.RoleID = moduleassignment.roleId"+
			" join lkp_modules modules on moduleassignment.moduleId = modules.id"+
			" where userID = ? group by modules.id";
	
	public static final String GET_MODULES_COUNT =  "SELECT count(*) from usersroles userrole "+
		" left join rolemoduleassignment moduleassignment on userrole.roleID = moduleassignment.roleId "+
		" join lkp_module_html_mapping htmlmapping on moduleassignment.moduleId = htmlmapping.moduleId "+
		" where userrole.userID = ? and htmlmapping.htmlPage = ?";
	
	public static final String GET_TRUCK_REPORT_COLUMNS = "Select ttrucks.TruckCode,ttrucks.DateOfPurchase,ttrucks.TruckRegistrationDate,ttrucks.PlateCode,"
			+"ttrucks.TruckRegistrationNo,ttrucks.Vin,ttrucks.AcquiredDate as DateOfAcquiring,truckbrands.Description as Brand,truckmodels.Description as Model,ttrucks.ModelYear,ttrucks.Color,"
			+"ttrucks.TotalCost,tpolicies.Description as InsCompanyName,tpolicies.policyNumber as PolicyNumber,tpolicies.IssuedAt AS PolicyIssueDate,tpolicies.ExpiredAt AS PolicyExpiryDate,ttrucks.ReRegistrationDueAt AS ReRegistrationDueDate,"
			+"ttrucks.Trucknickname as TruckNickName,ttrucks.AvailableHours AS AvailableHrsPerDay,GROUP_CONCAT(truckfaults.Description) as ServiceProvided"
			+" from trucks ttrucks"
			+" JOIN truckpolicies tpolicies"
			+" ON tpolicies.TruckID = ttrucks.TruckID"
			+" JOIN lkp_truckbrands_loc truckbrands"
			+" ON truckbrands.ID = ttrucks.TruckBrandID"
			+" JOIN lkp_truckmodels_loc truckmodels"
			+" ON truckmodels.ID = ttrucks.TruckModelID"
			+" JOIN (select truckservices.truckID,casefaults.Description,casefaults.ID as serviceID from truckregisteredservices as truckservices"
			+" JOIN lkp_mobile_service_faults casefaults"
			+" ON truckservices.caseFaultId =casefaults.ID order by truckID) as truckfaults"
			+" ON truckfaults.truckID = ttrucks.TruckID group by ttrucks.TruckID";
	
	
	public static final String GET_OPERATIONS_REPORT_COLUMNS = "SELECT CONCAT(d.driverFirstName,\" \",d.driverLastName) AS DriverName,t.Trucknickname,IF(u.firstName IS NULL,IFNULL(u.displayName,\"\"),CONCAT(IFNULL(u.firstName,\"\"),\" \",IFNULL(u.lastName,\"\"))) AS DispatcherName,"
            +"stypes.Description AS SourceType,CONCAT(c.firstName,\" \",c.lastName) as CustomerName,c.MobileNumber,loc.AddressTitle as LocationName,loc.Area AS LocationArea,loc.Street AS LocationStreet,loc.Building AS LocationBuilding,"
			+"loc.City AS LocationCity,v.VehicleName,v.PlateCode AS VehcilePlateCode,v.Vin AS vehicleVin,lbrands.Description AS VehiclebrandName,lmodels.Description AS VehicleModelName,v.ModelYear AS VehcileModelYear,v.Color AS VehicleColor,"
            +"cinfo.CreatedAt AS CalledTime,(SELECT createdAt FROM trac.caseschedulestatus where caseinfoID=cinfo.caseInfoID AND statusID=4 ORDER BY createdAt LIMIT 1) AS AssignedTime,"
            + "(SELECT createdAt FROM trac.caseschedulestatus where caseinfoID=cinfo.caseInfoID AND statusID=7 ORDER BY createdAt LIMIT 1) AS DispatchedTime,"
            + "(SELECT createdAt FROM trac.caseschedulestatus where caseinfoID=cinfo.caseInfoID AND statusID=10 ORDER BY createdAt LIMIT 1) AS CheckedInTime,"
            + "(SELECT createdAt FROM trac.caseschedulestatus where caseinfoID=cinfo.caseInfoID AND statusID IN (15,17) ORDER BY createdAt LIMIT 1) AS CompletedTime,"
            + "cinfo.caseID,cinfo.caseInfoID AS AssistanceId,GROUP_CONCAT(servicesTable.faultName) AS FaultNames,GROUP_CONCAT(servicesTable.serviceName) AS ServiceNames,lstatus.Description as Casestatus,"
            + "dloc.AddressTitle as DestinationLocationName,dloc.Area AS DestinationArea,dloc.Street AS DestinationStreet,dloc.Building AS DestinationBuilding,dloc.City AS DestinationCity,"
            + "r.DriverResponse AS DriverComments,r.CustomerCaseResponse AS CustomerComments"
            + " from caseinfo cinfo LEFT JOIN truckdrivers tdrivers ON cinfo.truckDriverID=tdrivers.TruckDriverID"
            + " LEFT JOIN servicecases scases ON cinfo.caseId = scases.servicecaseID"
            + " LEFT JOIN drivers d ON tdrivers.DriverID = d.driverID"
            + " LEFT JOIN trucks t ON tdrivers.TruckID = t.TruckID"
            + " LEFT JOIN users u ON scases.CreatedBy = u.userID"
            + " LEFT JOIN lkp_sys_sourcetypes stypes ON scases.sourceTypeID = stypes.ID "
            + " LEFT JOIN customers c ON scases.userID = c.customerID"
            + " LEFT JOIN locations loc ON scases.locID = loc.LocID"
            + " LEFT JOIN vehicles v ON scases.vehicleID = v.vehicleID"
            + " LEFT JOIN lkp_vehiclebrands_loc lbrands ON v.vehicleBrandID = lbrands.ID"
            + " LEFT JOIN lkp_vehiclemodels_loc lmodels ON v.vehicleModelID = lmodels.ID"
            + " LEFT JOIN lkp_casestatuses_loc lstatus ON cinfo.statusID = lstatus.ID"
            + " LEFT JOIN locations dloc ON scases.destinationLocID = dloc.LocID"
            + " LEFT JOIN ratings r ON scases.servicecaseID = r.CaseID"
            + " LEFT JOIN (SELECT * FROM (SELECT c.caseID,c.caseInfoID,c.caseRegisteredFaultID,lcasefaults.Description as faultName,c.createdAt"
            + " from caseinfo c JOIN"
            + " lkp_sys_caseregisteredfaults lcasefaults ON lcasefaults.ID = c.caseregisteredFaultID"
            + ") AS table1"
            + " JOIN "
            + "(SELECT cfservices.subcaseId,cfservices.caseRegisteredServiceID,lregservices.Description AS serviceName FROM caseinfo c JOIN casefaultservices cfservices "
            + " ON c.caseInfoID = cfservices.subcaseId "
            + " JOIN lkp_sys_caseregisteredservices lregservices "
            + " ON cfservices.caseRegisteredServiceID = lregservices.ID) AS table2"
            + " ON table1.caseInfoID = table2.subcaseId) AS servicesTable "
            + " ON cinfo.caseInfoID = servicesTable.subcaseId"
            + " GROUP BY cinfo.caseInfoID ORDER BY scases.createdAt DESC";
	
	
	public static final String GET_ATTENDANCE_REPORT_COLUMNS  ="SELECT CONCAT(d.driverFirstName,\" \",d.driverLastName) AS DriverName,e.EmployeeId,"
			+ "CONCAT('Shift ',dschedule.shiftId) AS ShiftName,days.Description AS ShiftDay,"
			+ "DATE(dptimings.punchIn) AS AttendanceDate,TIME(dptimings.punchIn) AS AttendanceTime "
			+ " From employee e LEFT JOIN drivers d "
			+ " ON e.ID = d.employeeId"
			+ " LEFT JOIN "
			+ " driverpunchtimings dptimings "
			+ " ON dptimings.driverID = d.driverID "
			+ " LEFT JOIN driverdaywiseschedule dschedule ON dptimings.shiftScheduleID = dschedule.ID"
			+ " LEFT JOIN lkp_sys_shift_days days ON dschedule.dayId = days.ID";
	
	
	//public static final String GET_TRUCK_EXPENSES = "SELECT Trucknickname,totalCost as OriginalCost FROM trac.trucks";
	
	//public static final String GET_TRUCK_EXPENSES = "SELECT t.Trucknickname,te.TruckID,te.ExpenseType,te.ExpenseValue,te.currency AS currencyType,te.PaidTo,te.ExpenseTime,te.PaymentType ,te.ReceiptNumber,te.Comments FROM trac.truck_expenses te LEFT JOIN trac.trucks t ON te.truckId = t.TruckID";
	public static final String GET_TRUCK_EXPENSES = "SELECT ttrucks.Trucknickname,ttrucks.TruckID,letypes.expenseDescription AS ExpenseType,te.ExpenseValue,lctypes.currecnyDescription AS currencyType,te.PaidTo,te.ExpenseTime,lptypes.paymentDescription AS PaymentType ,te.ReceiptNumber,te.Comments FROM"
			+" trac.truck_expenses te LEFT JOIN trac.trucks ttrucks ON te.truckId = ttrucks.TruckID"
			+" LEFT JOIN lkp_truck_currencytypes lctypes ON te.currency = lctypes.currecnyId"
			+" LEFT JOIN lkp_truck_paymenttypes lptypes ON te.PaymentType = lptypes.paymentId"
			+" LEFT JOIN lkp_truck_expensetypes letypes ON te.ExpenseType = letypes.expenseId";

}
