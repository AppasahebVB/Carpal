package trac.constants.sqlqueryconstants;

/**
 *  Logout Sql Query are placed in this class
 *
 */

public class LogoutSqlQueryConstants {

	public static final String GET_USERIN_AUTH_TABLE =  "select * from oauth_access_token where user_name=?";
}
