package trac.constants.responseconstants;

/**
 *  Constants for Vehicle response 
 *
 */


public class ResponseConstantsOfVehicle {
	

	public static final String VIN = "vin";
	public static final String VEHCICLEBRANDID = "vehicleBrandId";
	public static final String VEHCICLEMODELID = "vehicleModelId";
	public static final String VEHCILEPIC = "vehiclePic";
	public static final String VEHCICLEBRANDNAME = "vehicleBrandName";
	public static final String VEHCICLEMODELNAME = "vehicleModelName";
	public static final String VEHCICLENAME = "vehicleName";
	public static final String FAVORITE = "isFavorite";
	public static final String VEHICLELIST = "vehicleList";
	public static final String VEHICLEBRANDSLIST = "vehicleBrandsList";
	public static final String VEHICLEMODELSLIST = "vehicleModelsList";
	public static final String VEHICLE_PIC_URL = "vehiclePicUrl";
	public static final String VEHICLE_FILENAME = "vehicleFileNameOnCloud";
	public static final String VEHICLEREGISTERNO = "vehicleRegisterNo";
	public static final String VEHICLEMODELYEAR = "vehicleModelYear";
	public static final String VEHICLECOUNTRYCODE = "vehcileCountryISOCode";
	
	public static final String  MEMBERSHIPSTART = "memberShipStart";
	public static final String  MEMBERSHIPEND   = "memberShipEnd";
	public static final String  COUNTRYISO      = "countryISO";
	public static final String  COUNTRYNAME      = "countryName";
	public static final String CITY        = "city" ;
	public static final String  PLATECODE       = "plateCode";
	public static final String MEMBERSHIPTYPEID= "memberShipTypeId";
	public static final String  COLOR           = "color";
	public static final String  DATEOFPURCHASE  = "dateOfPurchase";
	public static final String  REGISTRATIONDATE= "registrationDate";
	public static final String  MILEAGE         = "mileage";
	public static final String   DISPLAY_ORDER = "displayOrder";
}
