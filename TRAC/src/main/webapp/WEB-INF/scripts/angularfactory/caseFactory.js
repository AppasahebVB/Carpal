TRACAPP.factory('casedDetailsObject', function ($rootScope) {
    var mem = {};
 
    return {
        store: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});



TRACAPP.service('SearchService', function ($resource, $http, $q) {
	return {
	    // Get Async list of disciplines
		    getCustomerDetails: function ( inputParams) {
		      
		    // create deferring result
		    var deferred = $q.defer();
		    
			     $http.post(SERVICEADDRESS + '/customer/search', JSON.stringify(inputParams)).
		        success(function(returnJson) {
		        
		            if (returnJson.isSuccess) {
		            	deferred.resolve(returnJson.searchList);
		           
		            	/*$scope.searchList.splice(0);
		            	console.log("returnJson.searchList"+ returnJson.searchList);
		            	$scope.searchList =  returnJson.searchList;
		            	$scope.$apply();*/
		      		}else{
		      			 return deferred.promise;
		      		}
		        }).error(function(returnErrorJson, status, headers, config) {
		        	deferred.reject("No DATA FOUND"); 
		        }); //error
			     return deferred.promise;
		    }
		};
	});