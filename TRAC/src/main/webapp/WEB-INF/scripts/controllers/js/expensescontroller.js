

TRACAPP.controller("expensesController", function($scope,$http,localStorage,$location,$timeout,$route,dataService) {
	    $scope.displayName = localStorage.getData("displayName");
	    $scope.accessToken = localStorage.getData("accessToken"); //access token
	    $scope.userId = localStorage.getData("userId"); //user id
		$scope.filename = "";
		
	    $scope.accessTypeList = [{"ID":1,"Description":"View"},{"ID":2,"Description":"Add"},{"ID":3,"Description":"Edit"},{"ID":4,"Description":"Delete"}];
	    
	    $scope.expensesTypeList = [{"ID":1,"Description":"Accessories price"},{"ID":2,"Description":" Truck Cost"},{"ID":3,"Description":"Maintenance Cost"}];
	    
	    $scope.paymentTypeList = [{"ID":1,"Description":"Cash"},{"ID":2,"Description":"Card"},{"ID":3,"Description":"Cheque"}];
	    
	    $scope.currencyTypeList = [{"ID":1,"Description":"USD"},{"ID":2,"Description":"Dhirams"},{"ID":3,"Description":"Indian Rupee"}];
   
	    
	    angular.isUndefinedOrNull = function(val) {
			return angular.isUndefined(val) || val === null;
		};
	    
		//common headers for all http posts
	    $http.defaults.headers.post["Content-Type"] = "application/json";
	    $http.defaults.headers.common['Authorization'] = "bearer " + $scope.accessToken;
	  

	    //the model returns a promise and THEN items
		 dataService.getItems().then(function(items) {
		        $scope.listOfPermissions=items;
		 }, function (status) {
	        console.log(status);
	    });
	    
	    
	    $scope.init = function init() {
	    	 $scope.isValidated = true;
			 $scope.onSuccess = false;
			$scope.trucks = [];
			$scope.canShow = false;
			$scope.slideToTop();
	    	$scope.trucksList(-1,-1);
		  
	    };
	    
	    /**
	     *to slide page view to top
	     */
	    $scope.slideToTop = function slideToTop(){
	  	  $("html, body").animate({ scrollTop: 0 }, 1000);
	    };
	    
	    $scope.initList = function initList(truckexpenses) {
	    	$scope.ifRecordsFound = true;
	     	$scope.isValidated = true;
			$scope.onSuccess = false;
	    	$scope.trucks = [];
			$scope.listOfTruckExpenses = [];
			$scope.listOfTruckExpensesForCSV = [];
			$scope.canShow = false;
			$scope.slideToTop();
			$scope.trucksList(-1,-1);
			$scope.getLisOfExpenses(truckexpenses);
	    	
	    };
	    
	    $scope.redirectToList =  function redirectToList() {
	    	
	    	$scope.canShow = false;
	    };
	    
		/**
		 * To fetch trucks through service 
		 * 
		 * 
		 * @return selection array
		 */

		$scope.trucksList =  function (firstValue,toValue) {
			var truck ={};
			truck.fromValue = firstValue;
			truck.toValue = toValue;
			$scope.trucks = [];
			
			$http.post(SERVICEADDRESS+'/truck/gettruckinfo',JSON.stringify(truck)).
			success(function(returnJson) {

			
				if(returnJson.isSuccess){
					$scope.trucks = returnJson.truckList;
				}else{
					$scope.serviceMessage = returnJson.serviceMessage;
				}
				
				console.log(JSON.stringify(returnJson));

			}).error(function(returnErrorJson, status, headers, config) {
				console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.trucksList(firstValue,toValue); 
				}
			});//error
		};//getTrucksList function end
		
		 /* * * * * * * * * * * *
	 	 * TO save expenses
	 	 * 
	 	 * * * * * * * * * * * */
	   $scope.registerTruckExpenses = function registerTruckExpenses(truckexpenses) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Expenses','moduleName','Add');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Add Expenses ";
				return false;
			}
			
			var isValid = $scope.tracForm.$valid;
			var inputObject  = {};
			inputObject.userId = parseInt($scope.userId);
			inputObject.truckId = truckexpenses.truckNickName;
			inputObject.expenseType = truckexpenses.expensesType;
			inputObject.expenseValue = truckexpenses.expensesValue;
			inputObject.paidTo = truckexpenses.paidTo;
			inputObject.expenseTime = truckexpenses.dateTimeOfExpenses;
			inputObject.currency = truckexpenses.currencyType;
			inputObject.paymentType = truckexpenses.paymentType;
			inputObject.receiptNumber = truckexpenses.recipientNumber;
			inputObject.comments = truckexpenses.expensesComments;
			
			console.log("Expenses json "+angular.toJson(inputObject));
			if(isValid){
						$http.post(SERVICEADDRESS+'/truck/expenseregister',angular.toJson(inputObject)).
						success(function(returnJson) {
							if(returnJson.isSuccess){
								 $scope.isValidated = true;
								 $scope.onSuccess = true;
								 $scope.serviceMessage = returnJson.serviceMessage;
								 $scope.truckexpenses.truckNickName ='';
								 $scope.truckexpenses.expensesType = '';
								 $scope.truckexpenses.expensesValue = '';
								 $scope.truckexpenses.currencyType = '';
								 $scope.truckexpenses.paidTo = '';
								 $scope.truckexpenses.paymentType = '';
								 $scope.truckexpenses.recipientNumber = '';
								 $scope.truckexpenses.dateTimeOfExpenses ='';
								 $scope.truckexpenses.expensesComments = '';
									
								 
								 $timeout(function(){
									 $scope.onSuccess = false;
							     }, 10000);
							}else{
								$scope.isValidated = false;
								$scope.serviceMessage = returnJson.serviceMessage;
								return false;
							}

						}).error(function(returnErrorJson, status, headers, config) {
							console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
							var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.registerTruckExpenses(truckexpenses) ; 
							}
							return false;
						});//error
				
		      }//end of isValid
};//end of service call

		
/* * * * * * * * * * * *
	 * TO update expenses
	 * 
	 * * * * * * * * * * * */
$scope.updateExpenses = function updateExpenses(truckexpenses,index) {
			
			var canWrite = $scope.getAccessPermissions($scope.listOfPermissions,'Expenses','moduleName','Edit');
			if(!canWrite){
				$scope.isValidated = false;
				$scope.serviceMessage = "You don't have permissions to Update Expenses";
				return false;
			}
			
			var isValid = $scope.tracForm.$valid;
			var inputObject  = {};
			inputObject.expenseID = truckexpenses.expenseId;
			inputObject.userId = parseInt($scope.userId);
			inputObject.truckId = truckexpenses.truckNickName;
			inputObject.expenseType = truckexpenses.expensesType;
			inputObject.expenseValue = truckexpenses.expensesValue;
			inputObject.paidTo = truckexpenses.paidTo;
			inputObject.expenseTime = truckexpenses.dateTimeOfExpenses;
			inputObject.paymentType = truckexpenses.paymentType;
			inputObject.receiptNumber = truckexpenses.recipientNumber;
			inputObject.comments = truckexpenses.expensesComments;
			
			inputObject.currency = truckexpenses.currencyType;
			
			console.log("Expenses json "+angular.toJson(inputObject));
			if(isValid){
						$http.post(SERVICEADDRESS+'/truck/expenseupdate',angular.toJson(inputObject)).
						success(function(returnJson) {
							if(returnJson.isSuccess){
								$scope.isValidated = true;
								$scope.onSuccess = false;
								$scope.canShow = false;
								
								$scope.listOfTruckExpenses[index].truckId = truckexpenses.truckNickName;
								$scope.listOfTruckExpenses[index].truckNickName = $scope.getValueOf($scope.trucks,truckexpenses.truckNickName,"truckId","truckNickName");
								$scope.listOfTruckExpenses[index].expenseType = truckexpenses.expensesType;
								$scope.listOfTruckExpenses[index].expenseValue = truckexpenses.expensesValue;
								$scope.listOfTruckExpenses[index].paidTo = truckexpenses.paidTo;
								$scope.listOfTruckExpenses[index].expenseTime = truckexpenses.dateTimeOfExpenses;
								$scope.listOfTruckExpenses[index].paymentType = truckexpenses.paymentType;
								$scope.listOfTruckExpenses[index].receiptNumber = truckexpenses.recipientNumber;
								$scope.listOfTruckExpenses[index].comments = truckexpenses.expensesComments;
								
								
							}else{
								$scope.isValidated = false;
								$scope.serviceMessage = returnJson.serviceMessage;
								return false;
							}

						}).error(function(returnErrorJson, status, headers, config) {
							console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
							var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
							if(isFreshTokenGenarated){
								$scope.updateExpenses(truckexpenses,index); 
							}
							return false;
						});//error
				
		      }//end of isValid
		};//update role function end
		
		$scope.getTruckExpenseDetails = function getTruckExpenseDetails(truckexpenses,index) {
			
			var inputObject = {};
			inputObject.expenseId = truckexpenses.expenseId;
			inputObject.truckNickName = truckexpenses.truckId;
			inputObject.expensesType = truckexpenses.expenseType;
			inputObject.expensesValue = truckexpenses.expenseValue;
			inputObject.paidTo = truckexpenses.paidTo;
			inputObject.dateTimeOfExpenses = truckexpenses.expenseTime;
			inputObject.paymentType = truckexpenses.paymentType;
			inputObject.recipientNumber = truckexpenses.receiptNumber;
			inputObject.expensesComments = truckexpenses.comments;
			inputObject.currencyType = parseInt(truckexpenses.currency);
			$scope.truckexpenses = inputObject;
			$scope.canShow = true;
			$scope.expenseIndex = index;
		};
		
		/*******
		 * get list of modules
		 *
		*******/
	    
	    $scope.getLisOfExpenses = function getLisOfExpenses(truckexpenses) {
	    	var inputObject  = {};
			inputObject.userId = parseInt($scope.userId);
			if(!angular.isUndefinedOrNull(truckexpenses)){
				inputObject.searchBy = truckexpenses.searchBy;
			}else{
				inputObject.searchBy = '';
			}
			
				inputObject.expenseTimeTo = !angular.isUndefinedOrNull($('#expenseTimeTo').val()) ? new Date($('#expenseTimeTo').val()) : '';
				inputObject.expenseTimeFrom = !angular.isUndefinedOrNull($('#expenseTimeFrom').val()) ? new Date($('#expenseTimeFrom').val()) : '';
			
			console.log("Expenses json "+angular.toJson(inputObject));
	    
			$http.post(SERVICEADDRESS+'/truck/gettruckexpenseinfo',angular.toJson(inputObject)).
				success(function(returnJson) {
					if(returnJson.isSuccess){
						$scope.listOfTruckExpenses = returnJson.truckExpenses;
						$scope.ifRecordsFound = true;
						if(!angular.isUndefinedOrNull($scope.listOfTruckExpenses)){
							for(var i =0; i< $scope.listOfTruckExpenses.length;i++){
								var expensesObject ={};
								expensesObject.truckNickName = $scope.listOfTruckExpenses[i].truckNickName;
								expensesObject.receiptNumber = $scope.listOfTruckExpenses[i].receiptNumber;
								expensesObject.expenseType = $scope.getValueOf($scope.expensesTypeList, $scope.listOfTruckExpenses[i].expenseType,"ID","Description");
								expensesObject.expenseValue = $scope.listOfTruckExpenses[i].expenseValue;
								expensesObject.expenseTime = $scope.listOfTruckExpenses[i].expenseTime;
								expensesObject.paymentType = $scope.getValueOf($scope.paymentTypeList, $scope.listOfTruckExpenses[i].paymentType,"ID","Description");
								expensesObject.currency = $scope.listOfTruckExpenses[i].currency;
								expensesObject.currencyType = $scope.getValueOf($scope.currencyTypeList, $scope.listOfTruckExpenses[i].currency,"ID","Description");
								
								$scope.listOfTruckExpensesForCSV.push(expensesObject);
							}
						}
					}else{
						$scope.ifRecordsFound = false;
					}
				  }).error(function(returnErrorJson, status, headers, config) {
					var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.getLisOfExpenses(truckexpenses);
					}
					
				});//end of http post call
			
		}; //End getModules function
		
		
		 /*******
		 * get list of modules
		 *
		*******/
	    
	    $scope.getLisOfModules = function getLisOfModules() {
	    	
	    	$http.get(SERVICEADDRESS+'/user/getmodules').
			success(function(returnJson) {
					$scope.listOfModules = returnJson.modules;
					console.log("getmodules" + JSON.stringify(returnJson));
				  }).error(function(returnErrorJson, status, headers, config) {
					var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.getLisOfModules();
					}
					
				});//end of http post call
			
		}; //End getModules function
		
		
		$scope.getHeader = function () {
			$scope.filename = "expenses_" + new Date().getTime()/1000;
	    	var headersArray = [];
	    	headersArray.push("Truck NickName");
	    	headersArray.push("Receipt Number");
	    	headersArray.push("Expenses Type");
	    	headersArray.push("Expense Cost");
	    	headersArray.push("Date");
	    	headersArray.push("Payment Type");
	    	return headersArray;
	    };

		
		
		 //json serach to find out location where the value resides
		$scope.getValueOf =  function (arr, comparableValue, proptosearch,propToget) {
			var l = arr.length,
			k = 0;
			for (k = 0; k < l; k = k + 1) {
				var indexValue = parseInt(arr[k][proptosearch]);
				var searcValue = parseInt(comparableValue);
				if (indexValue == searcValue) {
					return arr[k][propToget];
				}
			}
			return "";
		};//end

		
		   //json serach to find out location where the value resides
			$scope.getIndexOf =  function (arr, coparableValue, prop) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop].trim().toString();
					var searcValue = coparableValue.trim().toString();
					if (indexValue.indexOf(searcValue) >= 0) {
						return k;
					}
				}
				return -1;
			};//end

			//json serach to find out location where the value resides
			$scope.getIndexById =  function (arr, coparableValue, prop) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop];
					var searcValue = coparableValue;
					if (indexValue == searcValue >= 0) {
						return k;
					}
				}
				return -1;
			};//end

			//json serach to access permissions
			$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
				var l = arr.length,
				k = 0;
				for (k = 0; k < l; k = k + 1) {
					var indexValue = arr[k][prop1].trim().toLowerCase();
					var searcValue = comparableValue.trim().toLowerCase();
					if (indexValue.indexOf(searcValue) >= 0) {
						//var canAccess = arr[k][prop2];
						return arr[k][prop2];
					}
				}
				return -1;
			};//end
	    	
		 
	    
	    /* * * * * * * * * * * *
	 	 * logout of user
	 	 * 
	 	 * * * * * * * * * * * */
	    $scope.logoutUser = function logoutUser() {
	    	$http.defaults.headers.common['Authorization'] = "bearer "+ $scope.accessToken + " " +  $scope.userId;
			
	    	//service call
			$http.post(SERVICEADDRESS+'/logout').
			  success(function(returnJson) {
				    	
				    	if(returnJson.isLoggedOut){
				    		localStorage.clearStorage();
				    		window.location.href = HOSTADDRESS+"/login.html";
				    	}
			  }).error(function(returnErrorJson, status, headers, config) {
				  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					if(isFreshTokenGenarated){
						$scope.logoutUser(); 
					}
			 });//error
			  
		  };//logoutUser function end
		  
		  

			$scope.today = function() {
				$scope.dt = new Date();
			};//end
			
			$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};//end

			// Disable weekend selection
			$scope.disabled = function(date, mode) {
				return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
			};//end

			
			
			$scope.toggleMin = function() {
				$scope.minDate = $scope.minDate ? null : new Date();
			};//end

			$scope.toggleMin();

			$scope.open = function($event) {
				$event.preventDefault();
				$event.stopPropagation();
				$scope.opened = true;
			};//end
			

			$scope.dateOptions = {
					'formatYear': 'yy',
					'startingDay': 1
			};//end
			
			
			$scope.datepickerYearOptions = {
					'formatYear': 'yyyy',
					'startingDay' : 0,
					'minMode':'year'
				    
			};//end

			$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy','yyyy-MM-dd'];
			$scope.format = $scope.formats[5];
			$scope.yearformat = $scope.formats[4];
			
	      
}); //mainController function end
