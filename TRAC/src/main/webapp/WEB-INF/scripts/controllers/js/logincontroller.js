//var loginAppModule =  angular.module("TRACLOGIN", []).controller('loginController', loginController);
var TRACAPP =  angular.module("TRACAPP", ['ui.bootstrap']);

TRACAPP.controller("loginController", function($scope,$http,localStorage) {
	
	/*function loginController($scope,$http,$localStorage) {*/
		$scope.isServiceValidationExist = false;
		var isLoggedIn = localStorage.getData("isLoggedIn");
		
		
		if(isLoggedIn != "true" ) //set only when isLoggedIn  not equals to true
			localStorage.setData("isLoggedIn", false);
		
		
		  $scope.signinUser = function signinUser(user) {
			  user.fromWeb = true;
			     var isValid = $scope.userForm.$valid;
			     var displayName = null;
			  	console.log("user json = "+JSON.stringify(user));
			  	console.log("isValid = "+isValid);
			  	if(isValid){
			    $http.defaults.headers.post["Content-Type"] = "application/json";
					$http.post(SERVICEADDRESS+'/user/validate',JSON.stringify(user)).
					  success(function(returnJson) {
						    	
						    	if(returnJson.isSuccess){
						    		localStorage.setData("userId" , returnJson.userId);
						    		displayName = returnJson.displayName;
						    		if(displayName != null && displayName.length > 0)
						    			localStorage.setData("displayName" , returnJson.displayName);
						    		else
						    			localStorage.setData("displayName" , returnJson.email);
						    		
						    		localStorage.setData("isLoggedIn", true);
						    		localStorage.setData("roleId" , returnJson.roleId);
						    		localStorage.setData("accessToken", returnJson.accessToken);
						    		localStorage.setData("refreshToken", returnJson.refreshToken);
						    		
						    		window.location.href = HOSTADDRESS+"/index.html#/caseschedule";
						    	}else{
						    		$scope.isServiceValidationExist = true;
						    		$scope.serviceMessage = returnJson.serviceMessage;
						    		//alert(returnJson.serviceMessage);
						    	}
						    	
					  }).error(function(returnErrorJson, status, headers, config) {
						  if(returnErrorJson.length > 0)
							  inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					 });//error
				};//isvalid condition
		  };//signinUser function end
		  
		/*  $scope.forgotPasswordCheck = function forgotPasswordCheck(user) {
			  var isValid = $scope.userForm.$valid;
			    console.log("user json = "+JSON.stringify(user));
			  	console.log("isValid = "+isValid);
			  	if(isValid){
			    $http.defaults.headers.post["Content-Type"] = "application/json";
					$http.post(SERVICEADDRESS+'/user/validate',JSON.stringify(user)).
					  success(function(returnJson) {
						    	
						    	if(returnJson.isSuccess){
						    		localStorage.setData("userId" , returnJson.userId);
						    		displayName = returnJson.displayName;
						    		if(displayName != null && displayName.length > 0)
						    			localStorage.setData("displayName" , returnJson.displayName);
						    		else
						    			localStorage.setData("displayName" , returnJson.email);
						    		
						    		localStorage.setData("isLoggedIn", true);
						    		localStorage.setData("roleId" , returnJson.roleId);
						    		localStorage.setData("accessToken", returnJson.accessToken);
						    		localStorage.setData("refreshToken", returnJson.refreshToken);
						    		
						    		window.location.href = HOSTADDRESS+"/index.html#/caseschedule";
						    	}else{
						    		$scope.isServiceValidationExist = true;
						    		$scope.serviceMessage = returnJson.serviceMessage;
						    		//alert(returnJson.serviceMessage);
						    	}
						    	
					  }).error(function(returnErrorJson, status, headers, config) {
						  if(returnErrorJson.length > 0)
							  inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
					 });//error
				};//isvalid condition
		  };*/
}); //signinUser function end



