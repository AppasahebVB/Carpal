
var TRACAPP =  angular.module("TRACAPP", ['ui.bootstrap','googlechart','ngRoute','ngSanitize', 'ngCsv']);

TRACAPP.controller("mainController", ["$scope", "$http","$modal","localStorage","$compile","$filter","$location","$timeout","$route","$routeParams","dataService", function($scope,$http,$modal,localStorage,$compile,$filter,$location,$timeout,$route,$routeParams,dataService) {
	//common headers for all http posts
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.defaults.headers.common['Authorization'] = "bearer " + localStorage.getData("accessToken");
	
	angular.isUndefinedOrNull = function(val) {
    	return angular.isUndefined(val) || val === null;
    };

    $scope.reportview = '/resources/views/bodytemplates/reports.html';
	$scope.displayName = localStorage.getData("displayName");
	$scope.onSuccess = false;
	$scope.canOpen = false;
	$scope.canUpdate =  false;
	$scope.canShowTable = false;
	$scope.canShowReportInfo = false;
	$scope.isOperatorsValidated = true;
	$scope.themeIconSwithcer = true;
	$scope.userId = localStorage.getData("userId");
	$scope.canLoadNow = false;
	$scope.accessToken = localStorage.getData("accessToken");//accessToken
	$scope.userId = localStorage.getData("userId");//userId
	
	$scope.initialRange = 0;
	$scope.queryRange = 20;
	$scope.toRange = $scope.queryRange;
	$scope.maxSize = 10;
	$scope.maxvisiblepagesSize=5;
	$scope.reportFilterList = [];
	$scope.dynamicTD = '';
	$scope.fetchAllData = false;
	$scope.hasData = false;
	$scope.reportList = [];
	$scope.listOfPermissions = [];
	$scope.columnsArray = [];
	$scope.filename = "";
	$scope.currentPage = 1;
	$scope.processing = true;
	$scope.dateInputValue = "";
	$scope.isExportable =false;
	$scope.reportName = '';
	$scope.queryToExecute = "";
	$scope.showReportName = true;
	$scope.columnNames = '';
	 $scope.coumnNameArray = [];
	 $scope.isLimitRemoved = false;
	
	$scope.serviceMessage = '';

	$scope.expensesType = ["Accessories price","Truck Cost","Maintenance Cost"];

	$scope.currencyType = ["USD","Dhirams","India Rupee"];

	$scope.paymentType  = ["Cash","Card","Cheque"];
	
	 //the model returns a promise and THEN items
	 dataService.getItems().then(function(items) {
	        $scope.listOfPermissions=items;
	 }, function (status) {
        console.log(status);
    });

	$scope.modules = [];

	 $timeout(function(){
         $scope.canLoadNow = true;
    }, 1000);

	 
	 $scope.initMainReportView = function(){
		 $scope.initAppPlugins();
		 	 var searchParameter = $routeParams.param1;
			 if(!angular.isUndefinedOrNull(searchParameter)){
				 if(searchParameter.length > 0){
					 $scope.getReportById(searchParameter);
	             }
			 }
			 
	  		
	     };

	 $scope.menuArray = ["Create User","Reports","Cases","Truck","DRIVER","Shifts","Shift Schedule","Roles","Expenses"]; 
	 
	 $scope.subUrlArray = [{"moduleName":"Create User","subUrls":["/createuser"]},
	                       {"moduleName":"Reports","subUrls":["/reports","/listofsavedreports","/servicelocationreport"]},
	                   {"moduleName":"Cases","subUrls":["/createcase","/caseschedule","/listOfCases","/listOfCasesInQueue"]},
	                   {"moduleName":"Truck","subUrls":["/truckregistration","/listOfTrucks"]},
	                   {"moduleName":"DRIVER","subUrls":["/driverregistration","/listOfDrivers","/driverpunchtimings"]},
	                   {"moduleName":"Shifts","subUrls":["/shifttimings","/listingofshifts"]},
	                   {"moduleName":"Shift Schedule","subUrls":["/shiftschedule"]},
	                   {"moduleName":"Roles","subUrls":["/createrole","/listofroles"]}, 
	                   {"moduleName":"Expenses","subUrls":["/truckexpenses","/listoftruckexpenses","/truckexpensesupdate"]}]; 
	 
	$scope.operandsByDataType = [{"ID":1,"operand":"Equal To","value":"=","dataType" : "VARCHAR"},
	                             {"ID":2,"operand":"contains","value":"LIKE","dataType" : "VARCHAR"},
	                             {"ID":3,"operand":"Greater than","value":">","dataType" : "INT"},
	                             {"ID":4,"operand":"Less than","value":"<","dataType" : "INT"},
	                             {"ID":5,"operand":"Begins with","value":"BEGINS","dataType" : "VARCHAR"},
	                             {"ID":6,"operand":"Ends with","value":"ENDS","dataType" : "VARCHAR"},
	                             {"ID":7,"operand":"Greater than","value":">","dataType" : "DATE"},
	                             {"ID":8,"operand":"Less than","value":"<","dataType" : "DATE"},
	                             {"ID":9,"operand":"Greater than","value":">","dataType" : "YEAR"},
	                             {"ID":10,"operand":"Less than","value":"<","dataType" : "YEAR"},
	                             {"ID":11,"operand":"Equal To","value":"=","dataType" : "YEAR"},
	                             {"ID":12,"operand":"Equal To","value":"=","dataType" : "DATE"},
	                             {"ID":13,"operand":"Equal To","value":"=","dataType" : "INT"}];
	
	$scope.operandsFieldsArray = [{"ANDBYOR":'',"selectedFiled":'',"selectedOperand":'',"valueToBeApplied":'',"valueToBeAppliedDate":'','isDatePicker':false,'yearpicker':false}];

	$scope.respectivecolumnNames = [{"TruckID":"ttrucks.TruckID"},{"DateOfAcquiring":"DATE_FORMAT(ttrucks.AcquiredDate,'%Y-%m-%d')"},{"Brand":"truckbrands.Description"},{"AvailableHrsPerDay":"ttrucks.AvailableHours"},{"Model":"truckmodels.Description"},{"InsCompanyName":"tpolicies.Description"},
	                                {"PolicyExpiryDate":"DATE_FORMAT(tpolicies.ExpiredAt,'%Y-%m-%d')"},{"PolicyIssueDate":"DATE_FORMAT(tpolicies.IssuedAt,'%Y-%m-%d')"},{"ReRegistrationDueDate":"DATE_FORMAT(ttrucks.ReRegistrationDueAt,'%Y-%m-%d')"},{"TruckNickName":"ttrucks.Trucknickname"},
	                                {"DriverName":"CONCAT(d.driverFirstName,\" \",d.driverLastName)"},{"DispatcherName":"IF(u.firstName IS NULL,IFNULL(u.displayName,\"\"),CONCAT(IFNULL(u.firstName,\"\"),\" \",IFNULL(u.lastName,\"\")))"},{"SourceType":"stypes.Description"},
	                                {"CustomerName":"CONCAT(c.firstName,\" \",c.lastName)"},{"LocationName":"loc.AddressTitle"},{"LocationArea":"loc.Area"},
	                                {"LocationStreet":"loc.Street"},{"LocationBuilding":"loc.Building"},{"LocationCity":"loc.City"},
	                                {"VehiclebrandName":"lbrands.Description"},{"VehicleModelName":"lmodels.Description"},{"CalledTime":"DATE_FORMAT(scases.CreatedAt,'%Y-%m-%d')"},
	                                {"AssignedTime":"DATE_FORMAT(SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=4 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d')"},
	                                {"DispatchedTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=7 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d')"},
	                                {"CheckedInTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=9 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d')"},
	                                {"CompletedTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID IN (15,17) ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d')"},
	                                {"CaseReference":"cinfo.caseInfoID"},{"FaultNames":"servicesTable.faultName"},{"ServiceNames":"servicesTable.serviceName"},
	                                {"Casestatus":"lstatus.Description"}, {"DestinationLocationName":"dloc.AddressTitle"},{"DestinationArea":"dloc.Area"},{"DestinationStreet":"dloc.Street"},
	                                {"DestinationBuilding":"dloc.Building"},{"DestinationCity":"dloc.City"},{"DriverComments":"r.DriverResponse"},
	                                {"CustomerComments":"r.CustomerCaseResponse"},
	                                {"ShiftName":"CONCAT('Shift ',dschedule.shiftId)"},{"ShiftDay":"days.Description"},{"AttendanceDate":"DATE(dptimings.punchIn)"},{"AttendanceTime":"TIME(dptimings.punchIn)"},
	                                {"OriginalCost":"totalCost"},
	                                {"EmployeeId":"e.EmployeeId"},
	                                {"ServiceProvided":"truckfaults.Description"},{"VehicleColor":"v.Color"},
	                                {"VehcileModelYear":"v.ModelYear"},{"VehcilePlateCode":"v.PlateCode"},{"vehicleVin":"v.Vin"},
	                                {"ExpenseType":"letypes.expenseDescription"},{"currencyType":"lctypes.currecnyDescription"},{"PaymentType":"lptypes.paymentDescription"},
	                                {"AssistanceId":"cinfo.caseInfoID","caseID":"cinfo.caseID"},{"DateOfAcquiring":"DATE(AcquiredDate)"},{"DateOfPurchase":"DATE_FORMAT(DateOfPurchase,'%Y-%m-%d')"},
	                                {"TruckRegistrationDate":"DATE_FORMAT(TruckRegistrationDate,'%Y-%m-%d')"},{"PolicyExpiryDate":"DATE_FORMAT(ExpiredAt,'%Y-%m-%d')"},{"PolicyIssueDate":"DATE(IssuedAt)"},
	                                {"ReRegistrationDueAt":"DATE_FORMAT(ReRegistrationDueAt,'%Y-%m-%d')"},{"ExpenseTime":"DATE_FORMAT(ExpenseTime,'%Y-%m-%d')"},{"ModelYear":"YEAR(ModelYear)"},
	                                {"VehcileModelYear":"YEAR(VehcileModelYear)"}
	                                ]; 
	
	$scope.respectivecolumnAliasNames = [{"TruckID":"ttrucks.TruckID"},{"Brand":"truckbrands.Description as Brand"},{"DateOfAcquiring":"DATE_FORMAT(ttrucks.AcquiredDate,'%Y-%m-%d') as DateOfAcquiring"},{"AvailableHrsPerDay":"ttrucks.AvailableHours as AvailableHrsPerDay"},{"Model":"truckmodels.Description as Model"},{"InsCompanyName":"tpolicies.Description as InsCompanyName"},
	                                     {"PolicyExpiryDate":"DATE_FORMAT(tpolicies.ExpiredAt,'%Y-%m-%d') AS PolicyExpiryDate"},{"PolicyIssueDate":"DATE_FORMAT(tpolicies.IssuedAt,'%Y-%m-%d') AS PolicyIssueDate"},{"ReRegistrationDueDate":"DATE_FORMAT(ttrucks.ReRegistrationDueAt,'%Y-%m-%d') AS ReRegistrationDueDate"},{"TruckNickName":"ttrucks.Trucknickname as TruckNickName"},{"DateOfPurchase":"DATE_FORMAT(DateOfPurchase,'%Y-%m-%d') AS DateOfPurchase"},
	                                     {"ServiceProvided":"GROUP_CONCAT(truckfaults.Description) as ServiceProvided"}, {"TruckRegistrationDate":"DATE_FORMAT(TruckRegistrationDate,'%Y-%m-%d ') AS TruckRegistrationDate"},
	                                {"DriverName":"CONCAT(d.driverFirstName,\" \",d.driverLastName) as DriverName"},{"DispatcherName":"IF(u.firstName IS NULL,IFNULL(u.displayName,\"\"),CONCAT(IFNULL(u.firstName,\"\"),\" \",IFNULL(u.lastName,\"\"))) as DispatcherName"},
	                                {"SourceType":"stypes.Description as SourceType"},
	                                {"CustomerName":"CONCAT(c.firstName,\" \",c.lastName) as CustomerName"},{"LocationName":"loc.AddressTitle as LocationName"},{"LocationArea":"loc.Area as LocationArea"},
	                                {"LocationStreet":"loc.Street as LocationStreet"},{"LocationBuilding":"loc.Building as LocationBuilding"},{"LocationCity":"loc.City as LocationCity"},
	                                {"VehiclebrandName":"lbrands.Description as VehiclebrandName"},{"VehicleModelName":"lmodels.Description as VehicleModelName"},{"CalledTime":"DATE_FORMAT(scases.CreatedAt,'%Y-%m-%d') as CalledTime"},
	                                {"AssignedTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=4 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d') as AssignedTime"},
	                                {"DispatchedTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=7 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d') as DispatchedTime"},
	                                {"CheckedInTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID=9 ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d') as CheckedInTime"},
	                                {"CompletedTime":"DATE_FORMAT((SELECT createdAt FROM trac.caseschedulestatus cs where cs.caseinfoID=cinfo.caseInfoID AND cs.statusID IN (15,17) ORDER BY createdAt DESC LIMIT 0,1),'%Y-%m-%d') as CompletedTime"},
	                                {"CaseReference":"cinfo.caseInfoID as CaseReference"},{"FaultNames":"servicesTable.faultName as FaultNames"},{"ServiceNames":"GROUP_CONCAT(servicesTable.serviceName) as ServiceNames"},
	                                {"Casestatus":"lstatus.Description as Casestatus"}, {"DestinationLocationName":"dloc.AddressTitle as DestinationLocationName"},{"DestinationArea":"dloc.Area as DestinationArea"},{"DestinationStreet":"dloc.Street as DestinationStreet"},
	                                {"DestinationBuilding":"dloc.Building as DestinationBuilding"},{"DestinationCity":"dloc.City as DestinationCity"},{"DriverComments":"r.DriverResponse as DriverComments"},
	                                {"CustomerComments":"r.CustomerCaseResponse as CustomerComments"},
	                                {"ShiftName":"CONCAT('Shift ',dschedule.shiftId) as ShiftName"},{"ShiftDay":"days.Description as ShiftDay"},{"AttendanceDate":"DATE(dptimings.punchIn) as AttendanceDate"},{"AttendanceTime":"TIME(dptimings.punchIn) as AttendanceTime"},
	                                {"OriginalCost":"totalCost as OriginalCost"},
	                                {"EmployeeId":"e.EmployeeId as EmployeeId"},
	                                {"VehicleColor":"v.Color as VehicleColor"},
	                                {"VehcileModelYear":"DATE_FORMAT(v.ModelYear,'%Y') as VehcileModelYear"},{"VehcilePlateCode":"v.PlateCode as VehcilePlateCode"},{"vehicleVin":"v.Vin as vehicleVin"},
	                                {"ExpenseType":"letypes.expenseDescription as ExpenseType"},{"currencyType":"lctypes.currecnyDescription as currencyType"},{"PaymentType":"lptypes.paymentDescription as PaymentType"},
	                                {"AssistanceId":"cinfo.caseInfoID as AssistanceId","caseID":"cinfo.caseID as caseID"},{"ModelYear":"YEAR(ModelYear) as ModelYear"},
	                                {"VehcileModelYear":"YEAR(VehcileModelYear) as VehcileModelYear"},{"ExpenseTime":"DATE_FORMAT(ExpenseTime,'%Y-%m-%d')"}
	                                ];
	
	 /*******
	 * get all vehicle service location area wise to display the count on map
	 *
	*******/
	$scope.initVehicleServiceLocations = function (){
		var requestObj = {};
		requestObj.userId = $scope.userId;
		$http.post(SERVICEADDRESS+'/serviceLocation/fetchServiceLocation',JSON.stringify(requestObj)).
		success(function(returnJson) {

			if(returnJson.isSuccess){
				$scope.isValidated = true;
				$scope.serviceMessage = '';
				$scope.vehicleServiceLocations = returnJson.vehicleServiceLocations;
				if($scope.vehicleServiceLocations){
					var totalCaseCount =0;
					for(var i=0;i<$scope.vehicleServiceLocations.length;i++){
						totalCaseCount = totalCaseCount + parseInt($scope.vehicleServiceLocations[i].casecount);
						
						var locationLatitude =$scope.vehicleServiceLocations[i].locationLatitude;
						var locationLongitude =$scope.vehicleServiceLocations[i].locationLongitude;

						var locationLatLong = new google.maps.LatLng(locationLatitude, locationLongitude);
						$scope.$broadcast('place-marker2',{"locationLatitude":locationLatitude,"locationLongitude":locationLongitude,"CaseId":$scope.vehicleServiceLocations[i].CaseId,"casecount":$scope.vehicleServiceLocations[i].casecount});
					}
					$scope.$broadcast('total-casecount',{"totalCaseCount":totalCaseCount});
					
				}
				
			}else{
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
			}
			
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.initVehicleServiceLocations(); 
			}
		});//error
	};//end of service call

	$scope.getRoleId = function getRoleId() {
		return roleId;
	};

	 /*******
	 * get table metadatalist on report select
	 *
	*******/
	$scope.onReportSelect = function onReportSelect() {
	
		var inputObject = {};
		$scope.filename = $scope.trac.report + new Date().getTime()/1000;
		inputObject.reportTofetch = $scope.trac.report;
		$scope.fieldsToBeDisplayed = [];
		$scope.tableMetaDataList = [],tableMetaDataMainList = [];
		$scope.canOpen =  false; //variable to hide and display buttons
		$scope.isOperatorsValidated = true;

		$scope.reportFilterList = [];
		$scope.totalItems = 0;
		$scope.initialRange= 0;
		$scope.toRange  = $scope.queryRange;
	       
		if($scope.trac.report.length>0){

		$("#dynamicRow:not(:first)").remove();

		$("#reportRow").html("");
		$("#box2View").html("");
		console.log("---------"+JSON.stringify(inputObject));
		if($scope.columnsArray.length == 0){
				$scope.operandsFieldsArray = [{"ANDBYOR":'',"selectedFiled":'',"selectedOperand":'',"valueToBeApplied":'','isDatePicker':false,'yearpicker':false}];
		}
		
		if($scope.columnsArray.length > 0){
			
			 $scope.columnsArray.splice(0);
		
			if(!angular.isUndefinedOrNull($scope.operandsFieldsArray)){
				$scope.operandsFieldsArray.splice(0);
				$scope.operandsFieldsArray.push({"ANDBYOR":'',"selectedFiled":'',"selectedOperand":'',"valueToBeApplied":'',"valueToBeAppliedDate":'','isDatePicker':false,'yearpicker':false});
			}
			if(!angular.isUndefinedOrNull($scope.tableMetaDataList)){
				$scope.tableMetaDataList.splice(0);
			}
			
			if(!angular.isUndefinedOrNull($scope.fieldsToBeDisplayed)){
				$scope.fieldsToBeDisplayed.splice(0);
			}
			$("#reportRow").html("");
		}

		$http.post(SERVICEADDRESS + '/dashboard/reportmetadata', JSON.stringify(inputObject)).
			success(function(returnJson) {
				$scope.canUpdate =  false;
				$scope.trac.reportName = '';
				$scope.queryToExecute = '';
				$scope.canShowReportInfo = true;
				if (returnJson.success) {
					$scope.isValidated = true;
					$scope.serviceMessage = '';
					console.log( "tableMetaDataList - "+returnJson.tableMetaDataList);
					var tableMetaDataList = returnJson.tableMetaDataList;
						$scope.fetchData(false);
						$scope.tableMetaDataList = tableMetaDataList;
						$scope.tableMetaDataMainList = tableMetaDataList;
				}
	
			}).error(function(returnErrorJson, status, headers, config) {
	
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.onReportSelect(); 
				}//end of if
	
			}); //error
		}// end of if
	};//end of service call
	
	$scope.getClass = function(path) {
	    if ($location.path().substr(0, path.length) == path) {
	      return "active";
	    } 
	};
	
	$scope.isMainActive = function (moduleName) {
		
		var indexOfModule = $scope.menuArray.indexOf(moduleName);
		var subUrlArray =  $scope.subUrlArray[indexOfModule].subUrls;
		for(var i =0; i< subUrlArray.length; i++){
			var url =  $location.path();
			if(!angular.isUndefinedOrNull(url)){
				var urlArray = url.split('/');
				if(urlArray.length > 2){
					url = '/'+ urlArray[1];
				}
			}
	     if(subUrlArray[i] === url){
	    	 return true;
	     }
		}
	     return false;
	};
	
	
	$scope.isActive = function (viewLocation) {
		var url =  $location.path();
		if(!angular.isUndefinedOrNull(url)){
			var urlArray = url.split('/');
			if(urlArray.length > 2){
				url = '/'+ urlArray[1];
			}
		}
	     var active = (viewLocation === url);
	     return active;
	};
	
	$scope.redirectToUrl = function (urlToBeRedirected) {
		$location.path(urlToBeRedirected);
	};
	
	$scope.getHeader = function () {
    	var headersArray = [];
    	if($scope.reportList.length>0){
    		var reportObject = $scope.reportList[0];
    		for(property in reportObject){
    			headersArray.push(property);
    		}
    	}
    	return headersArray;
    };
    
	$scope.refreshLeftSelectedList = function refreshLeftSelectedList(){
		$scope.canOpen =  false; //variable to hide and display buttons
	};
	
	$scope.refreshRightSelectedList = function refreshRightSelectedList(){
		$scope.canOpen =  false; //variable to hide and display buttons
		
		var selectobject=document.getElementById("box2View");
		
		if(selectobject.length == 0){ //right box empty validation
			$scope.canOpen =  false; //variable to hide and display buttons
			$scope.canShowTable = false;
			$scope.hasData = false;
		}
	};


	$scope.getOperandOnFieldSelect = function getOperandOnFieldSelect(selectedFieldValue,operandsField) {
		var rowLen = $scope.operandsFieldsArray.length;

		for ( var i = 0 ; i < rowLen ; i++ ) {
			//var currentRow =  $scope.operandsFieldsArray[i];
			var isDatePicker = $scope.operandsFieldsArray[i].isDatePicker;
			var yearpicker = $scope.operandsFieldsArray[i].isDatePicker;
			if(!isDatePicker && !yearpicker){
				$scope.operandsFieldsArray[i].isDatePicker = false; 
				$scope.operandsFieldsArray[i].yearpicker = false;
			}
		}
		var selectedIndex = operandsField.selectedFiled;
		var selectedField = selectedIndex.selectedColumnName;
		var typeSelected = $scope.getValueFromJson($scope.tableMetaDataMainList,selectedField,"coulmnName","coulmnType");
		
		if(typeSelected == "INT"){
			if(!angular.isUndefinedOrNull(selectedFieldValue))
				document.getElementById('valueToBeApplied'+selectedFieldValue).type = 'number';
			
			$scope.operandsFieldsArray[selectedFieldValue].isDatePicker = false; 
			$scope.operandsFieldsArray[selectedFieldValue].yearpicker = false;
			
		}else if(typeSelected == "VARCHAR"){
			if(!angular.isUndefinedOrNull(selectedFieldValue))
				document.getElementById('valueToBeApplied'+selectedFieldValue).type = 'text';
			
			$scope.operandsFieldsArray[selectedFieldValue].isDatePicker = false; 
			$scope.operandsFieldsArray[selectedFieldValue].yearpicker = false;
		}else if(typeSelected == "DATE"){
			
			//var rowLen = $scope.operandsFieldsArray.length;
			
			$scope.operandsFieldsArray[selectedFieldValue].isDatePicker = true; 
			$scope.operandsFieldsArray[selectedFieldValue].yearpicker = false;
			/*for ( var i = 0 ; i < rowLen ; i++ ) {
				var currentRow =  $scope.operandsFieldsArray[i];
				var rowSelectedFieldName = currentRow.selectedFiled.selectedColumnName;
				if(selectedField == rowSelectedFieldName){
				$scope.operandsFieldsArray[i].isDatePicker = true; 
				$scope.operandsFieldsArray[i].yearpicker = false;
			    }
			}*/
			
		}
		else if(typeSelected == "YEAR"){
			
			//var rowLen = $scope.operandsFieldsArray.length;
			
			$scope.operandsFieldsArray[selectedFieldValue].isDatePicker = true; 
			$scope.operandsFieldsArray[selectedFieldValue].yearpicker = true;

			/*for ( var i = 0 ; i < rowLen ; i++ ) {
				var currentRow =  $scope.operandsFieldsArray[i];
				var rowSelectedFieldName = currentRow.selectedFiled.selectedColumnName;
				if(selectedField == rowSelectedFieldName){
				$scope.operandsFieldsArray[i].isDatePicker = true; 
				$scope.operandsFieldsArray[i].yearpicker = true;
			    }
			}*/
			
	
		}
		
		$scope.assignArrayFromJson($scope.operandsByDataType,typeSelected,"dataType",selectedFieldValue,operandsField.selectedOperand);
		// console.log("$scope.operandList - "+$scope.operandList);
	};
	
	

	$scope.assignArrayFromJson =  function (jsonToSearch, comparableValue, prop,selectedID,selectedValue) {

		var arrayToReturn  =[];
		var l = jsonToSearch.length,
		k = 0;
		$('#selectedOperand'+selectedID).empty().append($('<option>', { 
			value: '',
			text : 'Select Operand'
		}));
		for (k = 0; k < l; k = k + 1) {
			var jsonObject  = {};
			console.log(jsonToSearch[k]);
			console.log(jsonToSearch[k][prop]);
			var indexValue = jsonToSearch[k][prop];
			var searcValue = comparableValue;
			var fieldValue = jsonToSearch[k]["value"];
			if(selectedValue != fieldValue){
				if (indexValue == searcValue) {
					jsonObject.operand = jsonToSearch[k]["operand"];
					jsonObject.value = jsonToSearch[k]["value"];
					$('#selectedOperand'+selectedID).append($('<option>', { 
						value: jsonToSearch[k]["value"],
						text : jsonToSearch[k]["operand"]
					}));
	
					arrayToReturn.push(jsonObject);
				}
			}else{
				if (indexValue == searcValue) {
					jsonObject.operand = jsonToSearch[k]["operand"];
					jsonObject.value = jsonToSearch[k]["value"];
					$('#selectedOperand'+selectedID).append($('<option>', { 
						value: jsonToSearch[k]["value"],
						text : jsonToSearch[k]["operand"],
						selected : true
					}));
	
					arrayToReturn.push(jsonObject);
				}
			}

		}
		if(arrayToReturn.length > 0)
			return arrayToReturn;
		else
			return false;
	};

	/**
	 * enabling div which contains operands to be applied on fields
	 */
	$scope.openPopup = function openPopup() {

		var selectobject=document.getElementById("box2View");
		//holding fields selected from dual box
		$scope.fieldsToBeDisplayed = [];

		for (var i=0; i<selectobject.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
			console.log(selectobject.options[i].text+" "+selectobject.options[i].value.replace(/ +/g, ""));
			var selectedJsonObject = {}; 
			selectedJsonObject.selectID = i;
			selectedJsonObject.selectedValueToStore = selectobject.options[i].text;
			selectedJsonObject.selectedColumnName = selectobject.options[i].value.replace(/ +/g, "");
			$scope.fieldsToBeDisplayed[i] = selectedJsonObject;
		}

		console.log("fieldsToBeDisplayed value"+  JSON.stringify($scope.fieldsToBeDisplayed));
		$scope.canOpen =  true; //variable to hide and display buttons

	};
	
	$scope.fetchData = function fetchData(conditionalCheck){
		$scope.operandsFieldsArray = [{"ANDBYOR":'',"selectedFiled":'',"selectedOperand":'',"valueToBeApplied":'','isDatePicker':false,'yearpicker':false}];
		var fieldsToapply ="";
		var isValid= true;
		//not  to allow multiple clicks
		if(!$scope.processing){
			return;
		}
		$scope.processing = false;
		//not  to allow multiple clicks

		$scope.fetchAllData = true;
		$scope.dynamicTD = "";
		$scope.queryToExecute = "";
		var inputObject = {};

		var reportName = $('#report').val();

		if(reportName == "trucks"){
			$scope.queryToExecute = GET_TRUCK_REPORT_DYNAMIC_COLUMNS;
		}else if(reportName == "Operations"){
			$scope.queryToExecute = GET_OPERATIONS_REPORT_DYNAMIC_COLUMNS;
		}else if(reportName == "Attendance"){
			$scope.queryToExecute = GET_ATTENDANCE_REPORT_DYNAMIC_COLUMNS;
		}else if(reportName == "Expenses"){
			$scope.queryToExecute = GET_TRUCK_EXPENSES_DYNAMIC_COLUMNS;
		}
		
		
		var selectobject=document.getElementById("box2View");
		$scope.fieldsToBeDisplayed = [];
		
		if(selectobject.length == 0 && conditionalCheck){ //right box empty validation
			$scope.isValidated = false;
			$scope.serviceMessage = "No columns to generate report";
			$scope.processing = true;
			$scope.canOpen =  false; //variable to hide and display buttons
			$scope.canShowTable = false;
			$scope.hasData = false;
			$scope.showReportName = false;
			$scope.reportName = '';
			$scope.dynamicTD = $scope.dynamicTD +  "<td> No data found </td>";
			$("#reportRow").html($scope.dynamicTD); //binding html
			isValid = false;
			return false;
		}

		if(isValid){
			for (var i=0; i<selectobject.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
				console.log(selectobject.options[i].text+" "+selectobject.options[i].value.replace(/ +/g, ""));
				var selectedJsonObject = {}; 
				selectedJsonObject.selectID = i;
				selectedJsonObject.selectedValueToStore = selectobject.options[i].text;
				selectedJsonObject.selectedColumnName = selectobject.options[i].value.replace(/ +/g, "");
				$scope.fieldsToBeDisplayed[i] = selectedJsonObject;
				
				var filedPropery = $scope.getVlaueFromPropertiesJson($scope.respectivecolumnAliasNames,selectedJsonObject.selectedColumnName);
	            
				if(filedPropery === false) {
					
					fieldsToapply = fieldsToapply+selectedJsonObject.selectedColumnName;
				}
				else{
					
					fieldsToapply = fieldsToapply+filedPropery;
				}
				if(i != selectobject.length-1){
					fieldsToapply = fieldsToapply +",";
				}
				
			}
			$scope.queryToExecute = $scope.queryToExecute.replace('%%%',fieldsToapply);
			
			$scope.queryToExecute = $scope.queryToExecute.replace("%%","");
			$scope.queryToExecute = $scope.queryToExecute + " LIMIT "+$scope.initialRange + "," + $scope.queryRange;
			inputObject.tableSqlQuery = $scope.queryToExecute;
			console.log(inputObject.tableSqlQuery);
		
			
			inputObject.fieldsToBeDisplayed = $scope.fieldsToBeDisplayed;
			if($scope.fieldsToBeDisplayed.length>0){
				$scope.isExportable = false;
				$http.post(SERVICEADDRESS + '/dashboard/report', JSON.stringify(inputObject)).
				success(function(returnJson) {
					$scope.canShowTable = true;
					$scope.processing = true;		//not  to allow multiple clicks
					if (returnJson.success) {
						$scope.isValidated = true;
						$scope.serviceMessage = '';
						$scope.canOpen =  true; //variable to hide and display buttons
						$scope.hasData = true;
						var reportList = returnJson.tableResultList;
	                  
						$scope.reportList= $scope.reportList.concat(reportList); 
						if($scope.reportList.length>0){
							$scope.isExportable = true;
						}
						console.log("reportList" + JSON.stringify(reportList));
						$scope.totalItems = $scope.reportList.length;
						//$scope.currentPage = 1;
						refreshTableData($scope.reportList,selectobject);
						$scope.showReportName = true;
						$scope.reportName = '';
					} else { //if no data found
						if($scope.reportList.length==0){
							$scope.totalItems = 0;
							$scope.dynamicTD = $scope.dynamicTD +  "<td> No data found </td>";
							console.log("returnJson - "+ returnJson);
						}else{
							$scope.hasData = false;
						}
						console.log("More data not found");
					}
	
					if($scope.dynamicTD){
						$("#reportRow").html("");
						var temp = $compile($scope.dynamicTD)($scope);
						angular.element(document.getElementById('reportRow')).html(temp);
					}
	
				}).error(function(returnErrorJson, status, headers, config) {
					$scope.processing = true;	
					$scope.dynamicTD = $scope.dynamicTD + "<td> No data found </td>";
					$("#reportRow").html($scope.dynamicTD); //binding html
	
				}); //error
			}else{
				$scope.processing = true;	
			}
		}// end of isValid
	};//end of service call


	/**
	 * Building query based on input values
	 */
	$scope.buildQuery = function buildQuery() {

		var fieldsToapply ="";
		//not  to allow multiple clicks
		console.log("$scope.processing - "+ $scope.processing);
		if(!$scope.processing){
			return;
		}
		$scope.processing = false;
		//not  to allow multiple clicks
		$scope.fetchAllData = false;

		$scope.isOperatorsValidated = true;

		var selectedFieldArray = [];
		var selectedOperandArray = [];
		var selectedValueArray = [];
		$scope.queryToExecute = ""; //query
		var subqueryCondition = "";

		var reportName = $('#report').val();

		if($scope.tracForm.$valid){

			//list of fields
			var selectobject = document.getElementById("box2View");
			$scope.fieldsToBeDisplayed = [];

			for (var i=0; i<selectobject.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
				console.log(selectobject.options[i].text+" "+selectobject.options[i].value.replace(/ +/g, ""));
				var selectedJsonObject = {}; 
				selectedJsonObject.selectID = i;
				selectedJsonObject.selectedValueToStore = selectobject.options[i].text;
				selectedJsonObject.selectedColumnName = selectobject.options[i].value.replace(/ +/g, "");
				$scope.fieldsToBeDisplayed[i] = selectedJsonObject;
				
				var filedPropery = $scope.getVlaueFromPropertiesJson($scope.respectivecolumnAliasNames,selectedJsonObject.selectedColumnName);
	            
				if(filedPropery === false) {
					
					fieldsToapply = fieldsToapply+selectedJsonObject.selectedColumnName;
				}
				else{
					
					fieldsToapply = fieldsToapply+filedPropery;
				}
				if(i != selectobject.length-1){
					fieldsToapply = fieldsToapply +",";
				}
			}

			if($scope.fieldsToBeDisplayed.length>0){
				if(reportName == "trucks"){
					$scope.queryToExecute = GET_TRUCK_REPORT_DYNAMIC_COLUMNS;
				}else if(reportName == "Operations"){
					$scope.queryToExecute = GET_OPERATIONS_REPORT_DYNAMIC_COLUMNS;
				}else if(reportName == "Attendance"){
					$scope.queryToExecute = GET_ATTENDANCE_REPORT_DYNAMIC_COLUMNS;
				}else if(reportName == "Expenses"){
					$scope.queryToExecute = GET_TRUCK_EXPENSES_DYNAMIC_COLUMNS;
				}

				$scope.queryToExecute = $scope.queryToExecute.replace('%%%',fieldsToapply);
				
				var rowLen = $scope.operandsFieldsArray.length;

				for ( var i = 0 ; i < rowLen ; i++ ) {
					var currentRow =  $scope.operandsFieldsArray[i];
					var selectedField = currentRow.selectedFiled.selectedColumnName; 
					var selectedOperand = currentRow.selectedOperand;
					var selectedvalue = "";
					//retrieving datatype of selected column
					var typeSelected = $scope.getValueFromJson($scope.tableMetaDataList,selectedField,"coulmnName","coulmnType");
					if(typeSelected == "DATE"){
						selectedvalue = currentRow.valueToBeAppliedDate;
						selectedvalue = $filter('date')(selectedvalue, 'yyyy-MM-dd');
					}else if(typeSelected == "YEAR"){
						selectedvalue = currentRow.valueToBeAppliedmodelYear;
						selectedvalue = $filter('date')(selectedvalue, 'yyyy');
					}
					else{
						 selectedvalue = currentRow.valueToBeApplied;
					
					}
					
					var andbyor = (currentRow.ANDBYOR.length == 0 ? false:currentRow.ANDBYOR);

					if(i==0 && !andbyor && rowLen==1){
						$scope.isOperatorsValidated = true;
						break;
					}
					if(selectedField && selectedOperand && selectedvalue){

						/*if(selectedField.indexOf('GROUP_CONCAT(servicesTable.serviceName)')){
							selectedField = 'servicesTable.serviceName';
						}*/
						selectedFieldArray.push(selectedField);
						selectedOperandArray.push(selectedOperand);
						selectedValueArray.push(selectedvalue);
						
						//to apply and / or operation

						if(subqueryCondition.length==0){
							subqueryCondition = subqueryCondition + " WHERE ";

						}
						else if(subqueryCondition.length>0 && andbyor){
							subqueryCondition = subqueryCondition + " and ";
						}else if(subqueryCondition.length>0 && !andbyor){
                            subqueryCondition = subqueryCondition + " or ";
						}


						if(subqueryCondition.length>0){
							var filedPropery = $scope.getVlaueFromPropertiesJson($scope.respectivecolumnNames,selectedField);
                            
							if(filedPropery === false) {
								
								subqueryCondition = subqueryCondition + selectedField + " ";
							}
							else{
								subqueryCondition = subqueryCondition + filedPropery + " ";
							}
							
							
                             
							if(selectedOperand  == "LIKE"){
								subqueryCondition = subqueryCondition + " LIKE '%" + selectedvalue + "%'";
							}else if(selectedOperand  == "BEGINS"){
								subqueryCondition = subqueryCondition + " LIKE '" + selectedvalue + "%'";
							}else if(selectedOperand  == "ENDS"){
								subqueryCondition = subqueryCondition + " LIKE '%" + selectedvalue + "'";
							}/*else if(typeSelected == "VARCHAR"){
							      subqueryCondition = subqueryCondition + " " + selectedOperand + " " + selectedvalue;
							}*/else{
								subqueryCondition = subqueryCondition + " " + selectedOperand + " '" + selectedvalue+"'";
							}
							
						}
						$scope.isOperatorsValidated = true;
					}else{
						$scope.canShowTable = false;
						$scope.isOperatorsValidated = false;
					}
				}
				
				if($scope.showReportName){
					$scope.canShowTable = true;
					$scope.isOperatorsValidated = true;
				}

				$scope.queryToExecute = $scope.queryToExecute.replace("%%",subqueryCondition);
				console.log("queryToExecute = "+$scope.queryToExecute);

				//calling execute query method 
				if($scope.queryToExecute && $scope.isOperatorsValidated)
					$scope.executeQuery($scope.queryToExecute);
				
				
			}
			
			
		}
		$scope.processing = true;//not  to allow multiple clicks
	};//end of function
	

	/**
	 * calling  the service to execute the query built in front end
	 */
	$scope.executeQuery = function executeQuery(queryToExecute) {

		var selectobject=document.getElementById("box2View");
		var inputObject = {};
		$scope.queryToExecute = queryToExecute + " LIMIT "+$scope.initialRange + "," + $scope.queryRange;
		inputObject.tableSqlQuery = $scope.queryToExecute;
		$scope.dynamicTD = '';
		//var selectobject=document.getElementById("box2View");
		
		$scope.fieldsToBeDisplayed = [];

		for (var i=0; i<selectobject.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
			console.log(selectobject.options[i].text+" "+selectobject.options[i].value.replace(/ +/g, ""));
			var selectedJsonObject = {}; 
			selectedJsonObject.selectID = i;
			selectedJsonObject.selectedValueToStore = selectobject.options[i].text;
			selectedJsonObject.selectedColumnName = selectobject.options[i].value.replace(/ +/g, "");
			$scope.fieldsToBeDisplayed[i] = selectedJsonObject;
		}
		
		inputObject.fieldsToBeDisplayed = $scope.fieldsToBeDisplayed;
		$scope.isExportable = false;
		$http.post(SERVICEADDRESS + '/dashboard/report', JSON.stringify(inputObject)).
		success(function(returnJson) {
			$scope.canShowTable = true;

			if (returnJson.tableRecordFound) {
				$scope.isValidated = true;
				$scope.showReportName = true;
				$scope.reportName = '';
				$scope.hasData = true;
				var reportList = returnJson.tableResultList;
				$scope.reportList= $scope.reportList.concat(reportList);
				
				if($scope.reportList.length>0){
					$scope.isExportable = true;
				}
				console.log("reportList" + JSON.stringify(reportList));
				$scope.totalItems = $scope.reportList.length;
				refreshTableData($scope.reportList,selectobject);
				
			} else { //if no data found
				if($scope.reportList.length==0){
					$scope.totalItems = 0;
					$scope.dynamicTD = $scope.dynamicTD +  "<td> No data found </td>";
					console.log("returnJson - "+ returnJson);
				}else{
					$scope.hasData = false;
				}
				console.log("More data not found");
			}

			if($scope.dynamicTD){
				$("#reportRow").html("");
				var temp = $compile($scope.dynamicTD)($scope);
				angular.element(document.getElementById('reportRow')).html(temp);
				// $("#reportRow").html(dynamicTD); //binding html
			}

		}).error(function(returnErrorJson, status, headers, config) {
			$scope.processing = true;	
			$scope.dynamicTD = $scope.dynamicTD + "<td> No data found </td>";
			$("#reportRow").html($scope.dynamicTD); //binding html
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.executeQuery(queryToExecute); 
			}//end of if
		}); //error
	};	//end of service call
	
	/**
	 * calling  the service to execute the query built in front end
	 */
	$scope.executeReportQuery = function executeReportQuery(queryToExecute,columnNames) {

		$scope.columnNames = columnNames;
		var selectobject = '';
		var inputObject = {};
		
		inputObject.tableSqlQuery = queryToExecute + " LIMIT "+$scope.initialRange + "," + $scope.queryRange;
		
		console.log("queryToExecute",inputObject.tableSqlQuery);
		
		inputObject.fieldsToBeDisplayed = [];
		
		$http.post(SERVICEADDRESS + '/dashboard/report', JSON.stringify(inputObject)).
		success(function(returnJson) {
			$scope.canShowTable = true;
			
			if (returnJson.tableRecordFound) {
				$scope.isValidated = true;
				$scope.showReportName = true;
				$scope.reportName = '';
				$scope.hasData = true;
				var reportList = returnJson.tableResultList;
			

				$scope.reportList= $scope.reportList.concat(reportList); 
				if($scope.reportList.length>0){
					$scope.isExportable = true;
				}
				console.log("reportList" + JSON.stringify(reportList));
				$scope.totalItems = $scope.reportList.length;
				//$scope.currentPage = 1;
				refreshTableData($scope.reportList,selectobject);
			} else { //if no data found
				if($scope.reportList.length==0){
					$scope.totalItems = 0;
					$scope.dynamicTD = $scope.dynamicTD +  "<td> No data found </td>";
					console.log("returnJson - "+ returnJson);
				}else{
					$scope.hasData = false;
				}
				console.log("More data not found");
			}

			if($scope.dynamicTD){
				$("#reportRow").html("");
				var temp = $compile($scope.dynamicTD)($scope);
				angular.element(document.getElementById('reportRow')).html(temp);
			}

		}).error(function(returnErrorJson, status, headers, config) {

			$scope.dynamicTD = $scope.dynamicTD + "<td> No data found </td>";
			$("#reportRow").html($scope.dynamicTD); //binding html
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.executeReportQuery(queryToExecute,columnNames); 
				}//end of if
		}); //error
		
		
	};//end of service call
	
	
	/**
	 * save the report function for future use
	 */
	$scope.saveReport = function (){
		var requestObj = {};
		requestObj.userId = $scope.userId;
		requestObj.reportName = $scope.trac.reportName;
		requestObj.reportTable = $scope.trac.report;
		
		if($scope.queryToExecute.length>0 && $scope.queryToExecute.lastIndexOf("LIMIT")>-1 && !$scope.isLimitRemoved){
			console.log("queryToExecute",$scope.queryToExecute);
			console.log("limit-1",$scope.queryToExecute.lastIndexOf("LIMIT")-1);
			$scope.queryToExecute = $scope.queryToExecute.substring(0,$scope.queryToExecute.lastIndexOf("LIMIT")-1);
			$scope.isLimitRemoved = true;
		}
			
		var selectedFields = '';
		for (var i=0; i<$scope.fieldsToBeDisplayed.length; i++){//prepare selected fields
			
			selectedFields = selectedFields + $scope.fieldsToBeDisplayed[i].selectedColumnName ;
			if(i != $scope.fieldsToBeDisplayed.length-1){
				selectedFields = selectedFields +",";			}
		}
		
		
		requestObj.whereClause = JSON.stringify($scope.operandsFieldsArray);
		requestObj.columnNames = selectedFields;
		requestObj.reportSqlQuery = $scope.queryToExecute;
		
		console.log("requestObj json "+JSON.stringify(requestObj));
		
		$http.post(SERVICEADDRESS+'/dashboard/savereport',JSON.stringify(requestObj)).
		success(function(returnJson) {
			
			if(returnJson.isSuccess){
				$scope.isSuccess = true;
				$scope.isLimitRemoved = false;
				$scope.reportsList = returnJson.reportsList;
				$scope.isValidated = true;
				$scope.serviceMessage = returnJson.serviceMessage;
				$scope.onSuccess = true;
				$timeout(function(){
					$scope.onSuccess = false;
				}, 10000);
			}else{
				$scope.showReportName = true;
				$scope.reportName = '';
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
			}
			 $("html, body").animate({ scrollTop: 0 }, 1000);
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.saveReport(); 
			}
		});//error
	};//end of service call
	
	/**
	 * update the report function for future use
	 */
	$scope.updateReport = function (){
		
		var isValid = true;
		
		var requestObj = {};
		requestObj.userId = $scope.userId;
		requestObj.reportId = $scope.trac.reportId;
		requestObj.reportName = $scope.trac.reportName;
		requestObj.reportTable = $scope.trac.report;
		
		var reportName = $('#reportName').val();
		
		if(reportName.length > 20){
			$scope.isValidated = false;
			$scope.serviceMessage = "Max of 30 characters allowed for report name";
			isValid = false;
			return false;
		}
		
		
		if($scope.queryToExecute.length>0 && $scope.queryToExecute.lastIndexOf("LIMIT")>-1 && !$scope.isLimitRemoved){
			console.log("queryToExecute",$scope.queryToExecute);
			console.log("limit-1",$scope.queryToExecute.lastIndexOf("LIMIT")-1);
			$scope.queryToExecute = $scope.queryToExecute.substring(0,$scope.queryToExecute.lastIndexOf("LIMIT")-1);
			$scope.isLimitRemoved = true;
		}
			
		var selectedFields = '';
		for (var i=0; i<$scope.fieldsToBeDisplayed.length; i++){//prepare selected fields
			
			selectedFields = selectedFields + $scope.fieldsToBeDisplayed[i].selectedColumnName ;
			if(i != $scope.fieldsToBeDisplayed.length-1){
				selectedFields = selectedFields +",";			
			}
		}
		
		
		requestObj.whereClause = JSON.stringify($scope.operandsFieldsArray);
		requestObj.columnNames = selectedFields;
		requestObj.reportSqlQuery = $scope.queryToExecute;
		
		console.log("requestObj json "+JSON.stringify(requestObj));
		if(isValid){
			$http.post(SERVICEADDRESS+'/dashboard/updatereport',JSON.stringify(requestObj)).
			success(function(returnJson) {
				
				if(returnJson.isSuccess){
					$scope.isLimitRemoved = false;
					$scope.isValidated = true;
					//$scope.showReportName = false;
					$scope.serviceMessage = returnJson.serviceMessage;
					$scope.onSuccess = true;
					
					 $timeout(function(){
							$scope.onSuccess = false;
					     }, 10000);
					
				}else{
					//$scope.showReportName = true;
					$scope.isValidated = false;
					$scope.serviceMessage = returnJson.serviceMessage;
				}
				 $("html, body").animate({ scrollTop: 0 }, 1000);
				console.log(JSON.stringify(returnJson));
	
			}).error(function(returnErrorJson, status, headers, config) {
				console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
				var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.updateReport(); 
				}
			});//error
		}
	};//end of service call
	
	
	/**
	 * delete the report 
	 */
	$scope.deleteReport = function (reportId,index){
		var requestObj = {};
		requestObj.userId = $scope.userId;
		requestObj.reportId = reportId;
		
		console.log("requestObj json "+JSON.stringify(requestObj));
		
		$http.post(SERVICEADDRESS+'/dashboard/deletereport',JSON.stringify(requestObj)).
		success(function(returnJson) {
			
			if(returnJson.isSuccess){
				$scope.reportsList.splice(index,1);
				$scope.isValidated = true;
				$scope.serviceMessage = returnJson.serviceMessage;
				$scope.onSuccess = true;
				
				 $timeout(function(){
						$scope.onSuccess = false;
				     }, 10000);
				
			}else{
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
			}

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.deleteReport(); 
			}
		});//error
	};//end of service call
	
	
	
	/**
	 * to get list of reports saved
	 */
	$scope.initListOfReports = function (){
		var requestObj = {};
		requestObj.userId = $scope.userId;
		$http.post(SERVICEADDRESS+'/dashboard/fetchreport',JSON.stringify(requestObj)).
		success(function(returnJson) {

			if(returnJson.isSuccess){
				$scope.isValidated = true;
				$scope.reportsList = returnJson.reportsList;
				/*$scope.serviceMessage = returnJson.serviceMessage;
				$scope.onSuccess = true;
				
				 $timeout(function(){
						$scope.onSuccess = false;
				     }, 10000);*/
			}else{
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
				 $("html, body").animate({ scrollTop: 0 }, 1000);
			}
			
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.initListOfReports(); 
			}
		});//error
	};//end of service call
	
	
	$scope.pageCount = function () {
		return Math.ceil($scope.reportList.length / $scope.maxSize);
	};

	$scope.pageChanged = function() {

		$scope.dynamicTD = "";
		var selectobject=document.getElementById("box2View");
		refreshTableData($scope.reportList,selectobject);
		$("#reportRow").html("");
		var temp = $compile($scope.dynamicTD)($scope);
		angular.element(document.getElementById('reportRow')).html(temp);
		console.log('Page changed to: ' + $scope.currentPage);
		console.log('Page count: ' + Math.ceil($scope.reportList.length / $scope.maxSize));
		if(Math.ceil($scope.reportList.length / $scope.maxSize) == $scope.currentPage){
			if($scope.hasData){
				$scope.initialRange= $scope.toRange;
				$scope.toRange  = $scope.toRange + $scope.queryRange ;
				if($scope.fetchAllData){
					$scope.fetchData(true);
				}else{
					$scope.buildQuery();	
				}
			}
		}
	};
	
	$scope.addOperandRow = function(currentRowId){
		$scope.operandsFieldsArray.push({"ANDBYOR":'',"selectedFiled":'',"selectedOperand":'',"valueToBeApplied":'',"valueToBeAppliedDate":'','isDatePicker':false,'yearpicker':false});
	};

	$scope.deleteOperandRow = function(currentRowId){

		var jsonArrayLength =  $scope.operandsFieldsArray.length;
		if(jsonArrayLength>1){
			$scope.operandsFieldsArray.splice(currentRowId,1);
		}

	};
	$scope.PrepareJsonForchart =  function (reportResultList) {


		var columnArray = [];
		for(var i =0; i< reportResultList.length;i++){ //loop untill json array length
			var columnData = {};
			var valueJsonArray = [];

			var value ={};
			value.v= reportResultList[i].ModelYear;
			valueJsonArray.push(value);

			var value ={};
			value.v= reportResultList[i].noOfModels;
			valueJsonArray.push(value);

			columnData.c = valueJsonArray;
			columnArray.push(columnData);
		}
		if(columnArray.length > 0)
			$scope.initChart(columnArray);


	};

	$scope.getValueFromJson =  function (jsonToSearch, comparableValue, prop,valueToGet) {
		var l = jsonToSearch.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			console.log(jsonToSearch[k]);
			console.log(jsonToSearch[k][prop]);
			var indexValue = jsonToSearch[k][prop];
			var searcValue = comparableValue;
			if (indexValue == searcValue) {
				return jsonToSearch[k][valueToGet];
			}
		}
		return false;
	};

	$scope.getVlaueFromPropertiesJson = function (jsonToSearch,selectedField){
		var l = jsonToSearch.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			if(jsonToSearch[k][selectedField]){
				return jsonToSearch[k][selectedField];	
			}
		}
		return false;
	};


	

	$scope.initChart = function initChart(columnArray) {
		console.log("columnArray - "+JSON.stringify(columnArray));
		var chart1 = {};
		chart1.type = "ColumnChart";
		chart1.cssStyle = "height:400px; width:400px;";
		chart1.data = {"cols": [
		                        {id: "year", label: "Year", type: "string"},
		                        {id: "models/year", label: "Model/Year", type: "number"}
		                        ], "rows": columnArray};



		chart1.options = {
				"title": "Models per year",
				"isStacked": "true",
				"fill": 20,
				"displayExactValues": true,
				"vAxis": {
					"title": "Number of models per year", "gridlines": {"count": 6}
				},
				"hAxis": {
					"title": "Years"
				}
		};

		chart1.formatters = {};

		$scope.chart = chart1;
	};


	$scope.insertRow = function insertRow() {
		var x=document.getElementById('dynamicRow');
		var new_row = x.rows[1].cloneNode(true);
		var len = x.rows.length;

		var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
		inp1.id = "andbyor"+len;
		inp1.value = '';
		var select1 = new_row.cells[4].getElementsByTagName('select')[0];
		select1.id = "selectedFiled"+len;
		select1.setAttribute("ng-model","track.selectedFiled"+len);
		select1.setAttribute("ng-change","getOperandOnFieldSelect("+len+")");
		var select2 = new_row.cells[5].getElementsByTagName('select')[0];
		select2.id = "selectedOperand"+len;
		select2.setAttribute("ng-model","track.selectedOperand"+len);
		// select2.setAttribute("ng-options","field.operand for field in operandList"+len+" track by field.value");
		var inp2 = new_row.cells[6].getElementsByTagName('input')[0];
		inp2.id = "valueToBeApplied"+len;
		inp2.setAttribute("ng-model","track.valueToBeApplied"+len);
		inp2.value = '';

		var temp = $compile(new_row)($scope);
		angular.element( x.appendChild( new_row ));


	};

	/**
	 *get count of cases in que,pending,closed
	 */
	$scope.getCasesCount = function getCasesCount(){

		var caseDetails={};
		caseDetails.userId = $scope.userId;
		console.log("caseDetails json " + JSON.stringify(caseDetails));

		$http.post(SERVICEADDRESS + '/cases/casescount', JSON.stringify(caseDetails)).
		success(function(returnJson) {
			console.log("returnJson - "+JSON.stringify(returnJson));
			if (returnJson.isSuccess) {

				$scope.casesinquecount   = returnJson.casesinquecount;
				$scope.pendingcasescount = returnJson.pendingcasescount;
				$scope.closedcasescount  = returnJson.closedcasescount;

				$scope.isValidated = true;
				$scope.serviceMessage = '';
			}else{

				$scope.isValidated = false;
			}
		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.getCasesCount(); 
			}


		}); //error

	}; //end of getCasesInQueue

	$scope.isAccessible = true;
	/**
	 *get modules under that user
	 */
	$scope.getModulesOfUser= function getModulesOfUser(){

		var inputObject={};
		inputObject.userId = $scope.userId;

		console.log("inputObject json " + JSON.stringify(inputObject));
		$http.post(SERVICEADDRESS + '/user/modules', JSON.stringify(inputObject)).
		success(function(returnJson) {
			console.log("returnJson - "+JSON.stringify(returnJson));
			if (returnJson.isSuccess){
				$scope.modules   = returnJson.modules;
				$scope.canLoadNow = true;
				$scope.isValidated = true;
				$scope.serviceMessage = '';
			}else{
				$scope.isValidated = false;
			}
		}).error(function(returnErrorJson, status, headers, config) {
			isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.getModulesOfUser(); 
			}


		}); //error

	}; //end of getCasesInQueue

	//json serach to find out location where the value resides
	$scope.isModuleAccessible =  function (arr, coparableValue) {

		if (arr.length > 0) {
			return arr.indexOf( angular.lowercase(coparableValue));
		}
	};//end



	$scope.init = function init() {
	
		$scope.getModulesOfUser();
		$scope.getCasesCount();
	};
	
	
	$scope.initAppPlugins = function initAppPlugins() {
		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	};
	
	//json serach to access permissions
	$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop1].trim().toLowerCase();
			var searcValue = comparableValue.trim().toLowerCase();
			if (indexValue.indexOf(searcValue) >= 0) {
				//var canAccess = arr[k][prop2];
				return arr[k][prop2];
			}
		}
		return -1;
	};//end
	
	//Dispaly the table result rows
	function refreshTableData(reportList,selectobject){
		$scope.indexstart;
		$scope.indexstart = $scope.currentPage-1;

		if($scope.totalItems<$scope.currentPage*$scope.maxSize){
			$scope.reportFilterList = reportList.slice(($scope.indexstart*$scope.maxSize),$scope.totalItems);
		}else{
			$scope.reportFilterList = reportList.slice(($scope.indexstart*$scope.maxSize),$scope.currentPage*$scope.maxSize);
		}
	
		for(var i =0; i< $scope.reportFilterList.length;i++){ //loop untill json array length

			$scope.dynamicTD = $scope.dynamicTD + "<tr>"; //genarating table tr

			if(selectobject){
			for (var j=0; j<selectobject.length; j++){
				var fieldToBeFetched = selectobject.options[j].value.replace(/ +/g, ""); //column name to be search on json object 

			
				$scope.dynamicTD = $scope.dynamicTD + "<td class='td_th_alignment_nowrap'>"+($scope.reportFilterList[i][fieldToBeFetched] == null ? "" : $scope.reportFilterList[i][fieldToBeFetched] )+"</td>"; //genarating table td
				
			  }
			}
			else{
				
			if($scope.columnNames.indexOf(',')>0){
		    $scope.coumnNameArray = $scope.columnNames.split(",");
		    
		    for(var j=0;j<$scope.coumnNameArray.length;j++){
		    var fieldToBeFetched = $scope.coumnNameArray[j];
 
					
		    	$scope.dynamicTD = $scope.dynamicTD + "<td class='td_th_alignment_nowrap'>"+($scope.reportFilterList[i][fieldToBeFetched] == null ? "" : $scope.reportFilterList[i][fieldToBeFetched] )+"</td>"; //genarating table td
					
		    	}//for columns
			  }
			}
			$scope.dynamicTD = $scope.dynamicTD + "</tr>"; //end of tr
		}
	};
	
	
	function reArrangeSelect() {
	    $("#box2View").html($("#box2View option").sort(function(a, b) {
	        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
	    }));
	}
	
	/*******
	 * get report deatils by id
	 *
	*******/
	$scope.getReportById = function(reportId) {
		var requestObj = {};
		$scope.tableMetaDataList = [];
		$scope.tableMetaDataMainList = [];
		requestObj.userId = $scope.userId;
		requestObj.reportId = reportId;
		
		
		$http.post(SERVICEADDRESS+'/dashboard/fetchreport',JSON.stringify(requestObj)).
		success(function(returnJson) {

			if(returnJson.isSuccess){
				$scope.isValidated = true;
				$scope.serviceMessage = '';
				$scope.canUpdate =  true; //variable to hide and display buttons
				$scope.reportsList = returnJson.reportsList;
				$scope.trac = {};
				$scope.trac.report = $scope.reportsList[0].reportTableName;
				$scope.trac.reportName = $scope.reportsList[0].reportName;
				$scope.trac.reportId = $scope.reportsList[0].reportId;
				$scope.queryToExecute = $scope.reportsList[0].reportQuery;
				var whereClauseJson = $scope.reportsList[0].whereClause;
				$scope.operandsFieldsArray  = JSON.parse(whereClauseJson);
				

				var columnsArray = $scope.reportsList[0].columnNames;
				if(columnsArray.length > 0){
					$scope.columnsArray = columnsArray.split(',');
				}else{
					$scope.columnsArray = [];
				}
				var tableMetaDataList = returnJson.reportMetadataList;
				
				$scope.tableMetaDataMainList = returnJson.reportMetadataList;
				if($scope.columnsArray.length > 0){
					for(var i = 0 ; i < tableMetaDataList.length; i++ ){
						var columnName = tableMetaDataList[i].coulmnName;
						
						   var indexLoc = $scope.getIndexOfValue($scope.columnsArray,columnName);
				   		   if(indexLoc  < 0){
				   			   $scope.tableMetaDataList.push(tableMetaDataList[i]);
				   		   }else{
					   			$('#box2View').append($('<option>', { 
									value: columnName,
									text : tableMetaDataList[i].coulmnDisplayName,
								}));
				   		   }
						
					}
					
				}
				
				reArrangeSelect(); //to sort box2View
				
				//get report data
				$scope.executeReportQuery($scope.reportsList[0].reportQuery,$scope.reportsList[0].columnNames);
				
				
				//holding fields selected from dual box
				$scope.fieldsToBeDisplayed = [];

				$timeout(function(){
					
					var selectobject=document.getElementById("box2View");
					
					for (var i=0; i<selectobject.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
						console.log(selectobject.options[i].text+" "+selectobject.options[i].value.replace(/ +/g, ""));
						var selectedJsonObject = {}; 
						selectedJsonObject.selectID = i;
						selectedJsonObject.selectedValueToStore = selectobject.options[i].text;
						selectedJsonObject.selectedColumnName = selectobject.options[i].value.replace(/ +/g, "");
						$scope.fieldsToBeDisplayed[i] = selectedJsonObject;
					}
					for (var i=0; i<$scope.operandsFieldsArray.length; i++){//for loop to prepare json object which is used to bind fields to select boxes
						$scope.getOperandOnFieldSelect(i,$scope.operandsFieldsArray[i],true);
					}
					
				
				},1000);
				
				
				$scope.canOpen =  true; //variable to hide and display buttons
				$scope.hasData = true;
				$scope.serviceMessage = returnJson.serviceMessage;
				
			}else{
				$scope.isValidated = false;
				$scope.serviceMessage = returnJson.serviceMessage;
			}
			
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.initListOfReports(); 
			}
		});//error
	};//end
	
	$scope.changeThemeIcons = function changeThemeIcons(switcher) {
		console.log("switcher - "+ switcher);
			$scope.themeIconSwithcer= switcher;
	};
	
	$scope.logoutUser = function logoutUser() {

		$http.defaults.headers.common['Authorization'] = "bearer "+ $scope.accessToken + " " +  $scope.userId;
		//service call
		$http.post(SERVICEADDRESS+'/logout').
		success(function(returnJson) {

			if(returnJson.isLoggedOut){
				localStorage.clearStorage();
				window.location.href = HOSTADDRESS+"/login.html";

			}else{

			}

		}).error(function(returnErrorJson, status, headers, config) {
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.registerTruck($scope.truck); 
			}
		});//error

	};//logoutUser function end
	
	

	//json serach to find out location where the value resides
	$scope.getIndexOfValue =  function (arr, coparableValue) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
		
			var indexValue = arr[k].toString().trim();
			var searcValue = coparableValue.toString().trim();
			if (indexValue == searcValue) {
				return k;
			}
		}
		return -1;
	};//end
	
	$scope.today = function() {
		$scope.currentDate = new Date();
	};//end
	
	$scope.today();

	$scope.clear = function () {
		$scope.currentDate = null;
	};//end

	// Disable weekend selection
	$scope.disabledForReport = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};//end

	
	
	$scope.toggleMin = function() {
		$scope.minimumDate = $scope.minimumDate ? null : new Date();
	};//end

	$scope.toggleMin();

	$scope.openDateForReport = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.openflag = true;
	};//end
	

	$scope.dateOptions = {
			'formatYear': 'yy',
			'startingDay': 1
	};//end
	
	
	$scope.datepickerYearOptions = {
			'formatYear': 'yyyy',
			'startingDay' : 0,
			'minMode':'year'
		    
	};//end

	$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy', 'yyyy-MM-dd'];
	$scope.format = $scope.formats[5];
	$scope.yearformat = $scope.formats[4];
	$scope.timPickerformat = $scope.formats[1];
	
  
	//initializing required things
	// $scope.init();
}]); //mainController function end
