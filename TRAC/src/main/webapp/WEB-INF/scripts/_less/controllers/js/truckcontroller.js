
TRACAPP.config(function (datepickerConfig) {
	datepickerConfig.showWeeks = false;
});


TRACAPP.controller("truckcontroller", function($rootScope,$scope,$http,localStorage,$timeout,$compile,$location,$route,dataService) {

	//display name or email of user
	$scope.displayName = localStorage.getData("displayName");
	$scope.accessToken = localStorage.getData("accessToken"); //access token
	$scope.userId = localStorage.getData("userId"); //user id
	 
	$scope.brands = [];
	$scope.brandId = [];
    $scope.models = [];
	$scope.modelIds = [];
	$scope.listOfServices = [];
	$scope.modules = [];
	
	$scope.selectedBrandId;
	$scope.selectedModelId;
	
	$scope.isValidated = true;
	$scope.isServicesValid = false;
	$scope.canShow = false;
	
	$scope.isRegistrationDateValid =true;
	$scope.isDateOfAcquiringValid = true;
	$scope.isPolicyIssuedValid = true;
	$scope.isPolicyExpiryValid = true;
	$scope.isReRegistrationValid = true;
	 $scope.listOfPermissions = [];
	 
	 $timeout(function(){
         $scope.canLoadNow = true;
    }, 1000);

	$http.defaults.headers.post["Content-Type"] = "application/json";
	$http.defaults.headers.common['Authorization'] = "bearer "+ $scope.accessToken;

	 //the model returns a promise and THEN items
	 dataService.getItems().then(function(items) {
	        $scope.listOfPermissions=items;
	 }, function (status) {
       console.log(status);
   });
	
	/**
	 *  To check weather value is undefined or null
	 */
	angular.isUndefinedOrNull = function(val) {
		return angular.isUndefined(val) || val === null;
	};//end
	
  	
	//to initiaize on page load
	$scope.vehicleinit = function () {
		$scope.fetchbrandModels();
	};//end
	

	/**
	 *  To check weather atleast one check box selected
	 */
	$scope.checkboxSelected = function (object) {
		  return Object.keys(object).some(function (key) {
		    return object[key];
		  });
		};//end
	
	function setDate(object){
		object = object.split('-');
		var dateobject = new Date(object[2], object[1], object[0]); 	
		return dateobject;
	}//end
	
	function setYear(object){
		object = object.split('-');
		var dateobject = new Date(object[2], object[1]); 	
		return dateobject;
	}//end
	
    /**
	 * To compare dates
	 * it returns returnJson with success or failure data
	 * 
	 * @param  truck
	 * @return returnJson
	 */
    
	 $scope.compareDate = function (firstDate,secondDate){
	   	 if(!angular.isUndefinedOrNull(firstDate) && !angular.isUndefinedOrNull(secondDate)){
	   		
	   		 if(new Date(firstDate) < new Date(secondDate)){
					return true;
				}
	   		 
	   	 }
	   	return false;
	   };//end
   
	   $scope.getYearOfPurchase = function (){
		   $timeout(function() {
		   var dateOfPurchase =$('#dateOfPurchase').val();
		   	 if(!angular.isUndefinedOrNull(dateOfPurchase) ){
		   		var yearOfPurchase = dateOfPurchase.split('-')[2];
		   		
		   		document.getElementById('modelYear').setAttribute('max-date',yearOfPurchase);
				
		   	 }
	      		
	      	 }, 0);

		};//end

	/**
	 * To save truck details through service
	 * it returns returnJson with success or failure data
	 * 
	 * @param  truck
	 * @return returnJson
	 */

	
	//APRESH
	$scope.registerTruck = function registerTruck(truck) {
		
		var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'truck','moduleName','Add');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Register truck";
			return false;
		}
		
		$scope.serviceMessage;
		var isValid = $scope.tracForm.$valid;
		var truckObject ={};
		var arrayLength = $scope.selection.length;

		//if service not selected
		if(arrayLength == 0){
			$scope.isServicesValid = true;
			return false;
		}
		
		if(angular.isUndefinedOrNull($scope.selectedBrandId)){
			$scope.isBrandValid = true;
			return false;
		}else{
			$scope.isBrandValid = false;
		}
		
		if(angular.isUndefinedOrNull($scope.selectedModelId)){
			$scope.isModelValid = true;
			return false;
		}else{
			$scope.isModelValid = false;
		}

		
		if(!angular.isUndefinedOrNull(truck)){
			truckObject.truckCode       =  truck.truckCode;
			truckObject.plateCode     =  truck.plateCode;
			truckObject.plateNumber      = truck.plateNumber;
			truckObject.vin            =  truck.vin;
			truckObject.truckColor         = truck.truckColor;
			truckObject.totalCost         =  truck.totalCost;
			truckObject.insCompanyName    = truck.insCompanyName;
			truckObject.truckPolicyNumber    = truck.truckPolicyNumber;
			truckObject.truckNickName    = truck.truckNickName;
			truckObject.availableHoursPerDay    = truck.availableHoursPerDay;
			truckObject.dateOfPurchase       =  truck.dateOfPurchase;
			truckObject.registrationDate     =  truck.registrationDate;
			truckObject.dateOfAcquiring      = truck.dateOfAcquiring;
			truckObject.modelYear            =  truck.modelYear;
			truckObject.truckPolicyIssuedDate         =  truck.truckPolicyIssuedDate;
			truckObject.truckPolicyExpiryDate         =  truck.truckPolicyExpiryDate;
			truckObject.truckReRegistrationDueDate    =  truck.truckReRegistrationDueDate;
			
				if(isValid){
				
					truckObject.userId =  userId;
					var servicesArray ='';
					
					for(var i=0;i< arrayLength;i++)
					{
						console.log("service Id"+$scope.selection[i]);
						servicesArray = servicesArray +""+$scope.selection[i];
						if(i!=arrayLength -1){
							servicesArray = servicesArray+"," ;
						}
					}
					
					truckObject.servicesProvided = servicesArray;
					truckObject.truckBrandID = $scope.selectedBrandId; 
					truckObject.truckModelID =  $scope.selectedModelId;
					
					console.log("selectedModelId"+$scope.selectedModelId);
					console.log("selectedBrandId"+$scope.selectedBrandId);
					console.log("Truck json "+JSON.stringify(truckObject));

					$http.post(SERVICEADDRESS+'/truck/register',JSON.stringify(truckObject)).
					success(function(returnJson) {
						
						
						if(returnJson.isSuccess){
								//window.location.href = HOSTADDRESS+"/listofTrucksAvailabel.html";
								$location.path('/listOfTrucks');
						}else{
							$scope.isValidated = false;
							$scope.serviceMessage = returnJson.serviceMessage;
							return false;
						}

					}).error(function(returnErrorJson, status, headers, config) {
						console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
						var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.registerTruck($scope.truck); 
						}
						return false;
					});//error
					
				} //end of if
				

		}//end of isUndefinedOrNull


	};//registerTruck function end
	
	/**
	 * To update truck details through service
	 * it returns returnJson with success or failure data
	 * 
	 * @param  truck
	 * @return returnJson
	 */
	
	$scope.updateTruck = function updateTruck(truck) {
		
		var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'truck','moduleName','Edit');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Update truck";
			return false;
		}
		
		$scope.serviceMessage;
		var isValid = $scope.tracForm.$valid;
		var arrayLength = $scope.selection.length;
		var truckReRegistrationDueDate;
		var truckPolicyExpiryDate;
		var dateOfAcquiring;
		var truckPolicyIssuedDate;
		var registrationDate;
		var dateOfPurchase;
		 var indexLoc;
		var truckObject ={};

		//if service not selected
		if(arrayLength == 0){
			$scope.isServicesValid = true;
			return false;
		}
		
		if(!angular.isUndefinedOrNull(truck.truckBrandName) ){
			  indexLoc = $scope.getIndexOf($scope.truckBradnModelList,truck.truckBrandName,"truckBrandName");
            
            if(indexLoc >= 0){
          	  $scope.isBrandValid = false;
            }else{
				$scope.isBrandValid = true;
				return false;
            }
		}
		
		if(!angular.isUndefinedOrNull(truck.truckModelName)){
			 var modelIndexLoc = $scope.getIndexOf($scope.truckBradnModelList[indexLoc].brandModels,truck.truckModelName,"truckModelName");
           
           if(modelIndexLoc >= 0){
          	 $scope.isModelValid = false;
           }else{
				$scope.isModelValid = true;
				return false;
           }
		}
		
		
			truckObject.dateOfPurchase       = truck.dateOfPurchase;
			truckObject.registrationDate     = truck.registrationDate;
			truckObject.dateOfAcquiring      = truck.dateOfAcquiring;
			truckObject.modelYear            = truck.modelYear;
			truckObject.truckPolicyIssuedDate         =  truck.truckPolicyIssuedDate;
			truckObject.truckPolicyExpiryDate         =  truck.truckPolicyExpiryDate;
			truckObject.truckReRegistrationDueDate    =  truck.truckReRegistrationDueDate;
            
			
			if(!angular.isUndefinedOrNull(truckObject.dateOfPurchase) && truckObject.dateOfPurchase.length>0){
				var dateOfPurchaseArray = truckObject.dateOfPurchase.split('-');
				var dateOfPurchaseMonth = parseInt(dateOfPurchaseArray[1]) - 1;
				dateOfPurchaseMonth = "0"+dateOfPurchaseMonth;
				dateOfPurchase = new Date(dateOfPurchaseArray[2], dateOfPurchaseMonth, dateOfPurchaseArray[0]); //Year, Month, Date
				
			}

			if(!angular.isUndefinedOrNull(truckObject.registrationDate) && truckObject.registrationDate.length>0){
				var registrationDateArray = truckObject.registrationDate.split('-');
				var registrationDateMonth = parseInt(registrationDateArray[1]) - 1;
				registrationDateMonth = "0"+registrationDateMonth;
				registrationDate = new Date(registrationDateArray[2], registrationDateMonth, registrationDateArray[0]);
			}

			if(!angular.isUndefinedOrNull(truckObject.dateOfAcquiring) && truckObject.dateOfAcquiring.length>0){
				var dateOfAcquiringArray = truckObject.dateOfAcquiring.split('-');
				var dateOfAcquiringMonth = parseInt(dateOfAcquiringArray[1]) - 1;
				dateOfAcquiringMonth = "0"+dateOfAcquiringMonth;
				dateOfAcquiring = new Date(dateOfAcquiringArray[2], dateOfAcquiringMonth, dateOfAcquiringArray[0]); //Year, Month, Date
			}
			if(!angular.isUndefinedOrNull(truckObject.truckPolicyIssuedDate) && truckObject.truckPolicyIssuedDate.length>0){
				var truckPolicyIssuedDateArray = truckObject.truckPolicyIssuedDate.split('-');
				var truckPolicyIssuedDateMonth = parseInt(truckPolicyIssuedDateArray[1]) - 1;
				truckPolicyIssuedDateMonth = "0"+truckPolicyIssuedDateMonth;
				truckPolicyIssuedDate = new Date(truckPolicyIssuedDateArray[2], truckPolicyIssuedDateMonth, truckPolicyIssuedDateArray[0]); //Year, Month, Date
			}
			if(!angular.isUndefinedOrNull(truckObject.truckPolicyExpiryDate) && truckObject.truckPolicyExpiryDate.length>0){
				var truckPolicyExpiryDateArray = truckObject.truckPolicyExpiryDate.split('-');
				var truckPolicyExpiryDateMonth = parseInt(truckPolicyExpiryDateArray[1]) - 1;
				truckPolicyExpiryDateMonth = "0"+truckPolicyExpiryDateMonth;
				truckPolicyExpiryDate = new Date(truckPolicyExpiryDateArray[2], truckPolicyExpiryDateMonth, truckPolicyExpiryDateArray[0]); //Year, Month, Date
			}
			if(!angular.isUndefinedOrNull(truckObject.truckReRegistrationDueDate) && truckObject.truckReRegistrationDueDate.length>0){
				var truckReRegistrationDueDateArray = truckObject.truckReRegistrationDueDate.split('-');
				var truckReRegistrationDueDateMonth = parseInt(truckReRegistrationDueDateArray[1]) - 1;
				truckReRegistrationDueDateMonth = "0"+truckReRegistrationDueDateMonth;
				truckReRegistrationDueDate = new Date(truckReRegistrationDueDateArray[2], truckReRegistrationDueDateMonth, truckReRegistrationDueDateArray[0]); //Year, Month, Date
			}
			
			if(registrationDate<dateOfPurchase){
				$scope.isRegistrationDateValid =false;
				isValid =  false;
			}else{
				$scope.isRegistrationDateValid =true;
			} 
				
			if(dateOfAcquiring<dateOfPurchase){
				$scope.isDateOfAcquiringValid = false;
				isValid =  false;
			}else{
				$scope.isDateOfAcquiringValid =true;
			} 
			
			if(truckPolicyIssuedDate<registrationDate){
				$scope.isPolicyIssuedValid = false;
				isValid =  false;
			}else{
				$scope.isPolicyIssuedValid =true;
			} 
			
			if(truckPolicyExpiryDate<truckPolicyIssuedDate){
				$scope.isPolicyExpiryValid = false;
				isValid =  false;
			}else{
				$scope.isPolicyExpiryValid =true;
			} 
			
			if(truckReRegistrationDueDate<registrationDate){
				$scope.isReRegistrationValid = false;
				isValid =  false;
			}else{
				$scope.isReRegistrationValid =true;
			}
			
			if(isValid){
				
					truckObject.userId =  userId;
					var servicesArray ='';
					
					for(var i=0;i< arrayLength;i++)
					{
						console.log("service Id"+$scope.selection[i]);
						servicesArray = servicesArray +""+$scope.selection[i];
						if(i!=arrayLength -1){
							servicesArray = servicesArray+"," ;
						}
					}
					
    				truckObject.servicesProvided = servicesArray;
					truckObject.truckBrandID = $scope.selectedBrandId; 
					truckObject.truckModelID =  $scope.selectedModelId;
					truckObject.plateCode =  truck.plateCode;
					truckObject.totalCost =  truck.totalCost;
					truckObject.truckColor =  truck.truckColor;
					truckObject.insCompanyName =  truck.insCompanyName;
					truckObject.truckPolicyNumber    = truck.truckPolicyNumber;
					truckObject.truckNickName =  truck.truckNickName;
					truckObject.truckCode       =  truck.truckCode;
					truckObject.availableHoursPerDay =  truck.availableHoursPerDay;
					truckObject.vin =  truck.vin;
					truckObject.plateNumber =  truck.plateNumber;
					truckObject.truckId =  truck.truckId;
					
					
					console.log("selectedModelId"+$scope.selectedModelId);
					console.log("selectedBrandId"+$scope.selectedBrandId);
					console.log("Truck json "+JSON.stringify(truckObject));

					$http.post(SERVICEADDRESS+'/truck/update',JSON.stringify(truckObject)).
					success(function(returnJson) {
						
						if(returnJson.isSuccess){
								//window.location.href = HOSTADDRESS+"/listofTrucksAvailabel.html";
							//$location.path('/listOfTrucks');
							 $scope.isValidated = true;
							$scope.trucksList(0,50);
							  $scope.canShow = false;
						}else{
							$scope.isValidated = false;
							$scope.serviceMessage = returnJson.serviceMessage;
							return false;
						}

					}).error(function(returnErrorJson, status, headers, config) {
						console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
						var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.registerTruck($scope.truck); 
						}
						return false;
					});//error
					
				} //end of if
	
				
	};//registerTruck function end
	
	
	
	/**
	 * To deactiavte truck from further use through service
	 * it returns returnJson with success or failure data
	 * 
	 * @param  truck
	 * @return returnJson
	 */
	
	$scope.deactivateTruck = function deactivateTruck(truck,index) {
		
		var canAccess = $scope.getAccessPermissions($scope.listOfPermissions,'truck','moduleName','Delete');
		if(!canAccess){
			$scope.isValidated = false;
			$scope.serviceMessage = "You don't have permissions to Delete truck";
			return false;
		}
		
		var truckObject ={};

		truckObject.userId = $scope.userId;
		truckObject.truckId =  truck.truckId;
				
					console.log("Truck json "+JSON.stringify(truckObject));

					$http.post(SERVICEADDRESS+'/truck/unregistertruck',JSON.stringify(truckObject)).
					success(function(returnJson) {
						
						if(returnJson.isSuccess){
							 $scope.isValidated = true;
							 $scope.trucks.splice(index,1);
							  //$scope.trucksList(0,50);
							  $scope.canShow = false;
						}else{
							$scope.isValidated = false;
							$scope.serviceMessage = returnJson.serviceMessage;
							return false;
						}

					}).error(function(returnErrorJson, status, headers, config) {
						console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
						var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
						if(isFreshTokenGenarated){
							$scope.deactivateTruck(truck,index); 
						}
						return false;
					});//error
								
	};//deactivateTruck function end

	
	/**
	 * To fetch the brands available from service
	 * it returns returnJson with success or failure data
	 * 
	 * @return returnJson
	 */

	$scope.fetchbrandModels = function () {
		
		$scope.truckBradnModelList = [];
		$scope.brands = [];
		$scope.brandId = [];
		$scope.servicesList = [];

		$http.get(SERVICEADDRESS+'/truck/getservicesandbrands').
		success(function(returnJson) {

			if(returnJson.isSuccess){
				//loading list of services from DB
				$scope.servicesList = angular.fromJson(returnJson.services);

				//loading list of truck brands and models from DB
				$scope.truckBradnModelList = returnJson.truckBrandList;
				var brandModelsLength = $scope.truckBradnModelList.length;
					for(var i=0;i<brandModelsLength;i++){
						$scope.brands.push($scope.truckBradnModelList[i].truckBrandName);	
						$scope.brandId.push($scope.truckBradnModelList[i].truckBrandId);
					}
			}else{

				$scope.serviceMessage = returnJson.serviceMessage;

			}


		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));

			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.fetchbrandModels(); 
			}


		});//error

	};//fetchServices function end



	/**
	 * To prepare a list of checked services
	 * 
	 * 
	 * @return selection array
	 */

	$scope.selection=[];
//	toggle selection for a given service by id
	$scope.toggleSelection = function toggleSelection(serviceId) {
		var idx = $scope.selection.indexOf(serviceId.toString());

		// is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}

		// is newly selected
		else {
			$scope.selection.push(serviceId);
		}
	};//end



	/**
	 * To fetch trucks through service 
	 * 
	 * 
	 * @return selection array
	 */

	$scope.trucksList =  function (firstValue,toValue) {
		var truck ={};
		truck.fromValue = firstValue;
		truck.toValue = toValue;
		$scope.trucks = [];
		
		$http.post(SERVICEADDRESS+'/truck/gettruckinfo',JSON.stringify(truck)).
		success(function(returnJson) {

		
			if(returnJson.isSuccess){
				$scope.trucks = returnJson.truckList;
			}else{

				$scope.serviceMessage = returnJson.serviceMessage;
				//alert(returnJson.serviceMessage);
			}
			 setTimeout(function(){ 
					App.init(); // Init layout and core plugins
					Plugins.init(); // Init all plugins
					FormComponents.init(); // Init all form-specific plugins
				  }, 1000);
			console.log(JSON.stringify(returnJson));

		}).error(function(returnErrorJson, status, headers, config) {
			console.debug("returnErrorJson - ", JSON.stringify(returnErrorJson));
			var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
			if(isFreshTokenGenarated){
				$scope.trucksList(firstValue,toValue); 
			}
		});//error
	};//getTrucksList function end
	
	
	  $scope.logoutUser = function logoutUser() {
		  $http.defaults.headers.common['Authorization'] = "bearer "+ accessToken + " " +  userId;
		//service call
		$http.post(SERVICEADDRESS+'/logout').
		  success(function(returnJson) {
			    	
			    	if(returnJson.isLoggedOut){
			    		localStorage.clearStorage();
			    		window.location.href = HOSTADDRESS+"/login.html";
			    	
			    	}else{
			    		
			    	}
			    	
		  }).error(function(returnErrorJson, status, headers, config) {
			  var isFreshTokenGenarated = inValidateSession(status,returnErrorJson.error_description,localStorage,$http);
				if(isFreshTokenGenarated){
					$scope.logoutUser(); 
				}
		 });//error
		  
	  };//logoutUser function end
	
	  //get truck details and display it in fields
	  $scope.getTruckDetails = function (truckObject) {
		
		  $scope.canShow = true;
		
		  var updateModelyear = truckObject.modelYear ;
		  truckObject.modelYear = updateModelyear.toString();
		  //$scope.truck = truckObject;
		  
		  var updatedTotalCost = truckObject.totalCost ;
		  truckObject.totalCost = updatedTotalCost.toString();
		  $scope.truckUpdate = truckObject;
		  
		  $scope.selectedBrandId = truckObject.truckBrandID;
		  $scope.selectedModelId=truckObject.truckModelID;
		  var services = truckObject.servicesProvided;
		  $scope.selection = services.split(',');
		 
	  };//end
	  
	  
	  
	//to fetch on page load
	$scope.init = function () {
		$scope.trucksList(0,50);
		$scope.fetchbrandModels();
	};//end

	//to check the items selected previously
	$scope.checkItem = function (id) {
		   var checked = false;
		   var listOfServicesArray = $scope.selection;
		   for(var i=0; i<= listOfServicesArray.length; i++) {
		      if(id == listOfServicesArray[i]) {
		    	
		         checked = true;
		      }
		    }
		    return checked;
		};
	
	$scope.redirectingToTruck = function redirectingToTruck() {
			console.log("trucks - "+$scope.trucks);
    		$scope.view = '/resources/views/bodytemplates/driver_workDetails_body.html';
    };//end
    
  //json serach to access permissions
	$scope.getAccessPermissions =  function (arr, comparableValue, prop1, prop2) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop1].trim().toLowerCase();
			var searcValue = comparableValue.trim().toLowerCase();
			if (indexValue.indexOf(searcValue) >= 0) {
				//var canAccess = arr[k][prop2];
				return arr[k][prop2];
			}
		}
		return -1;
	};//end

	 //json serach to find out location where the value resides
	$scope.getIndexOf =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop].trim().toString();
			var searcValue = coparableValue.trim().toString();
			if (indexValue.indexOf(searcValue) >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	//json serach to find out location where the value resides
	$scope.getIndexById =  function (arr, coparableValue, prop) {
		var l = arr.length,
		k = 0;
		for (k = 0; k < l; k = k + 1) {
			var indexValue = arr[k][prop];
			var searcValue = coparableValue;
			if (indexValue == searcValue >= 0) {
				return k;
			}
		}
		return -1;
	};//end
	
	  $scope.redirectToUrl = function(urlToBeRedirected) {
		  window.location.href = HOSTADDRESS+urlToBeRedirected;
	  };
	
	
	$scope.today = function() {
		$scope.dt = new Date();
	};//end
	
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};//end

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};//end

	
	
	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};//end

	$scope.toggleMin();

	$scope.open = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = true;
		
		
	};//end
	

	$scope.dateOptions = {
			'formatYear': 'yy',
			'startingDay': 1
	};//end
	
	
	$scope.datepickerYearOptions = {
			'formatYear': 'yyyy',
			'startingDay' : 0,
			'minMode':'year'
		    
	};//end

	$scope.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy', 'yyyy-MM-dd'];
	$scope.format = $scope.formats[5];
	$scope.yearformat = $scope.formats[4];
	
   
}); //truckcontroller function end



